# modelsim2latex
Convert Waveforms from HDL-Simulator Modelsim into nice LaTeX-output by using tikz-timing.

General description
-------------------
This script is intended for the following use case: There are some nice
HDL-simulations using the MentorGraphics Modelsim. Now these simulations shall
be documented using LaTeX. By doing so, one certainly has to import some
waveforms from Modelsim into the LaTeX-document. The classic approach using the
image exporting function of Modelsim is not very nice in most cases as the
output is a pixel graphic and therefore not scalable. Much nicer would therfore
be to process the raw waveform data from Modelsim and letting LaTeX do the
formatting when building the document.

Installation
------------
Just clone this repository and make the ms2tt-script executable (if not
already). Python3 has to be installed for proper function. If desired, add path
where ms2tt-script resides to PATH-variable.

Usage
-----
First of all the waveform data has to be exported from Modelsim. To do so, run
the simulation until all desired events are visible. Now select all signals that
shall be documented via LaTeX-document and right-click on them. By selecting Add
-> To List -> Selected Items these signals are added to a list. Now make sure
you see the list and select File -> Export -> Tabular list. Save the .lst-file
somewhere.

Next step invokes the ms2tt-script. First of all start with parameter -h to see
the builtin help. All parameters should be self-describing. The simplest way of
converting the .lst-file to LaTeX-output is now: ms2tt exportedfile.lst

python3 ./ms2tt -h

			Modelsim to TikzTiming
Parse a .lst-file exported from Modelsim and output LaTeX-sources to generate
nice timing diagrams, using tikz timing.
							sh-ow

positional arguments:
  lstfile               The input .lst-file

optional arguments:
  -h, --help            show this help message and exit
  -a                    Use all available values (default: only last delta values).
  -p                    Only print all available signals.
  -o                    Output full path of signal names (default: name only).
  -g                    Grid in background of LaTeX Timing diagram.
  -m                    Generate LaTeX-output as minimum working example.
  -v                    Don't show vector width in LaTeX-output.
  -t [TRIGSIG]          Signal to configure the start trigger for.
  -e [{risedg,faledg,match}]
                        Event to trigger on (signal has to be chosen with -t) (default: risedg).
  -d [TRIGDATA]         Data to match with, for trigger-type "match".
  -i [TRIGNTH]          Take the nth trigger event (default: first).
  -f [{hex,bin,none}]   Number format in LaTeX-output for signals with vectorwidth > 1 (default: hex).
  -s [INCSIGNAMES]      Signals to use (e.g.: sig1,sig2) (default: use all existing).
  -r [EXCLSIGNAMES]     Signals to exclude (e.g.: sig1,sig2) (default: exclude none).
  -n [NSIGS]            Only use n sig changes (from trigger) / leave out last n changes (via neg. val).


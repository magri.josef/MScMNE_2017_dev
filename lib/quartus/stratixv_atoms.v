// Copyright (C) 1991-2010 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// Quartus II 10.1 Build 153 11/29/2010

// ********** PRIMITIVE DEFINITIONS **********

`timescale 1 ps/1 ps

// ***** DFFE

primitive STRATIXV_PRIM_DFFE (Q, ENA, D, CLK, CLRN, PRN, notifier);
   input D;   
   input CLRN;
   input PRN;
   input CLK;
   input ENA;
   input notifier;
   output Q; reg Q;

   initial Q = 1'b0;

    table

    //  ENA  D   CLK   CLRN  PRN  notifier  :   Qt  :   Qt+1

        (??) ?    ?      1    1      ?      :   ?   :   -;  // pessimism
         x   ?    ?      1    1      ?      :   ?   :   -;  // pessimism
         1   1   (01)    1    1      ?      :   ?   :   1;  // clocked data
         1   1   (01)    1    x      ?      :   ?   :   1;  // pessimism
 
         1   1    ?      1    x      ?      :   1   :   1;  // pessimism
 
         1   0    0      1    x      ?      :   1   :   1;  // pessimism
         1   0    x      1  (?x)     ?      :   1   :   1;  // pessimism
         1   0    1      1  (?x)     ?      :   1   :   1;  // pessimism
 
         1   x    0      1    x      ?      :   1   :   1;  // pessimism
         1   x    x      1  (?x)     ?      :   1   :   1;  // pessimism
         1   x    1      1  (?x)     ?      :   1   :   1;  // pessimism
 
         1   0   (01)    1    1      ?      :   ?   :   0;  // clocked data

         1   0   (01)    x    1      ?      :   ?   :   0;  // pessimism

         1   0    ?      x    1      ?      :   0   :   0;  // pessimism
         0   ?    ?      x    1      ?      :   ?   :   -;

         1   1    0      x    1      ?      :   0   :   0;  // pessimism
         1   1    x    (?x)   1      ?      :   0   :   0;  // pessimism
         1   1    1    (?x)   1      ?      :   0   :   0;  // pessimism

         1   x    0      x    1      ?      :   0   :   0;  // pessimism
         1   x    x    (?x)   1      ?      :   0   :   0;  // pessimism
         1   x    1    (?x)   1      ?      :   0   :   0;  // pessimism

//       1   1   (x1)    1    1      ?      :   1   :   1;  // reducing pessimism
//       1   0   (x1)    1    1      ?      :   0   :   0;
         1   ?   (x1)    1    1      ?      :   ?   :   -;  // spr 80166-ignore
                                                            // x->1 edge
         1   1   (0x)    1    1      ?      :   1   :   1;
         1   0   (0x)    1    1      ?      :   0   :   0;

         ?   ?   ?       0    0      ?      :   ?   :   0;  // clear wins preset
         ?   ?   ?       0    1      ?      :   ?   :   0;  // asynch clear

         ?   ?   ?       1    0      ?      :   ?   :   1;  // asynch set

         1   ?   (?0)    1    1      ?      :   ?   :   -;  // ignore falling clock
         1   ?   (1x)    1    1      ?      :   ?   :   -;  // ignore falling clock
         1   *    ?      ?    ?      ?      :   ?   :   -; // ignore data edges

         1   ?   ?     (?1)   ?      ?      :   ?   :   -;  // ignore edges on
         1   ?   ?       ?  (?1)     ?      :   ?   :   -;  //  set and clear

         0   ?   ?       1    1      ?      :   ?   :   -;  //  set and clear

	 ?   ?   ?       1    1      *      :   ?   :   x; // spr 36954 - at any
							   // notifier event,
							   // output 'x'
    endtable

endprimitive

primitive STRATIXV_PRIM_DFFEAS (q, d, clk, ena, clr, pre, ald, adt, sclr, sload, notifier  );
    input d,clk,ena,clr,pre,ald,adt,sclr,sload, notifier;
    output q;
    reg q;
    initial
    q = 1'b0;

    table
    ////d,clk, ena,clr,pre,ald,adt,sclr,sload,notifier: q : q'
        ? ?    ?   1   ?   ?   ?   ?    ?     ?       : ? : 0; // aclr
        ? ?    ?   0   1   ?   ?   ?    ?     ?       : ? : 1; // apre
        ? ?    ?   0   0   1   0   ?    ?     ?       : ? : 0; // aload 0
        ? ?    ?   0   0   1   1   ?    ?     ?       : ? : 1; // aload 1

        0 (01) 1   0   0   0   ?   0    0     ?       : ? : 0; // din 0
        1 (01) 1   0   0   0   ?   0    0     ?       : ? : 1; // din 1
        ? (01) 1   0   0   0   ?   1    ?     ?       : ? : 0; // sclr
        ? (01) 1   0   0   0   0   0    1     ?       : ? : 0; // sload 0
        ? (01) 1   0   0   0   1   0    1     ?       : ? : 1; // sload 1

        ? ?    0   0   0   0   ?   ?    ?     ?       : ? : -; // no asy no ena
        * ?    ?   ?   ?   ?   ?   ?    ?     ?       : ? : -; // data edges
        ? (?0) ?   ?   ?   ?   ?   ?    ?     ?       : ? : -; // ignore falling clk
        ? ?    *   ?   ?   ?   ?   ?    ?     ?       : ? : -; // enable edges
        ? ?    ?   (?0)?   ?   ?   ?    ?     ?       : ? : -; // falling asynchs
        ? ?    ?   ?  (?0) ?   ?   ?    ?     ?       : ? : -;
        ? ?    ?   ?   ?  (?0) ?   ?    ?     ?       : ? : -;
        ? ?    ?   ?   ?   0   *   ?    ?     ?       : ? : -; // ignore adata edges when not aloading
        ? ?    ?   ?   ?   ?   ?   *    ?     ?       : ? : -; // sclr edges
        ? ?    ?   ?   ?   ?   ?   ?    *     ?       : ? : -; // sload edges

        ? (x1) 1   0   0   0   ?   0    0     ?        : ? : -; // ignore x->1 transition of clock
        ? ?    1   0   0   x   ?   0    0     ?        : ? : -; // ignore x input of aload
        ? ?    ?   1   1   ?   ?   ?    ?     *       : ? : x; // at any notifier event, output x

    endtable
endprimitive

primitive STRATIXV_PRIM_DFFEAS_HIGH (q, d, clk, ena, clr, pre, ald, adt, sclr, sload, notifier  );
    input d,clk,ena,clr,pre,ald,adt,sclr,sload, notifier;
    output q;
    reg q;
    initial
    q = 1'b1;

    table
    ////d,clk, ena,clr,pre,ald,adt,sclr,sload,notifier : q : q'
        ? ?    ?   1   ?   ?   ?   ?    ?     ?        : ? : 0; // aclr
        ? ?    ?   0   1   ?   ?   ?    ?     ?        : ? : 1; // apre
        ? ?    ?   0   0   1   0   ?    ?     ?        : ? : 0; // aload 0
        ? ?    ?   0   0   1   1   ?    ?     ?        : ? : 1; // aload 1

        0 (01) 1   0   0   0   ?   0    0     ?        : ? : 0; // din 0
        1 (01) 1   0   0   0   ?   0    0     ?        : ? : 1; // din 1
        ? (01) 1   0   0   0   ?   1    ?     ?        : ? : 0; // sclr
        ? (01) 1   0   0   0   0   0    1     ?        : ? : 0; // sload 0
        ? (01) 1   0   0   0   1   0    1     ?        : ? : 1; // sload 1

        ? ?    0   0   0   0   ?   ?    ?     ?        : ? : -; // no asy no ena
        * ?    ?   ?   ?   ?   ?   ?    ?     ?        : ? : -; // data edges
        ? (?0) ?   ?   ?   ?   ?   ?    ?     ?        : ? : -; // ignore falling clk
        ? ?    *   ?   ?   ?   ?   ?    ?     ?        : ? : -; // enable edges
        ? ?    ?   (?0)?   ?   ?   ?    ?     ?        : ? : -; // falling asynchs
        ? ?    ?   ?  (?0) ?   ?   ?    ?     ?        : ? : -;
        ? ?    ?   ?   ?  (?0) ?   ?    ?     ?        : ? : -;
        ? ?    ?   ?   ?   0   *   ?    ?     ?        : ? : -; // ignore adata edges when not aloading
        ? ?    ?   ?   ?   ?   ?   *    ?     ?        : ? : -; // sclr edges
        ? ?    ?   ?   ?   ?   ?   ?    *     ?        : ? : -; // sload edges

        ? (x1) 1   0   0   0   ?   0    0     ?        : ? : -; // ignore x->1 transition of clock
        ? ?    1   0   0   x   ?   0    0     ?        : ? : -; // ignore x input of aload
        ? ?    ?   1   1   ?   ?   ?    ?     *        : ? : x; // at any notifier event, output x

    endtable
endprimitive

module stratixv_dffe ( Q, CLK, ENA, D, CLRN, PRN );
   input D;
   input CLK;
   input CLRN;
   input PRN;
   input ENA;
   output Q;
   
   wire D_ipd;
   wire ENA_ipd;
   wire CLK_ipd;
   wire PRN_ipd;
   wire CLRN_ipd;
   
   buf (D_ipd, D);
   buf (ENA_ipd, ENA);
   buf (CLK_ipd, CLK);
   buf (PRN_ipd, PRN);
   buf (CLRN_ipd, CLRN);
   
   wire   legal;
   reg 	  viol_notifier;
   
   STRATIXV_PRIM_DFFE ( Q, ENA_ipd, D_ipd, CLK_ipd, CLRN_ipd, PRN_ipd, viol_notifier );
   
   and(legal, ENA_ipd, CLRN_ipd, PRN_ipd);
   specify
      
      specparam TREG = 0;
      specparam TREN = 0;
      specparam TRSU = 0;
      specparam TRH  = 0;
      specparam TRPR = 0;
      specparam TRCL = 0;
      
      $setup  (  D, posedge CLK &&& legal, TRSU, viol_notifier  ) ;
      $hold   (  posedge CLK &&& legal, D, TRH, viol_notifier   ) ;
      $setup  (  ENA, posedge CLK &&& legal, TREN, viol_notifier  ) ;
      $hold   (  posedge CLK &&& legal, ENA, 0, viol_notifier   ) ;
 
      ( negedge CLRN => (Q  +: 1'b0)) = ( TRCL, TRCL) ;
      ( negedge PRN  => (Q  +: 1'b1)) = ( TRPR, TRPR) ;
      ( posedge CLK  => (Q  +: D)) = ( TREG, TREG) ;
      
   endspecify
endmodule     

// ***** stratixv_latch

module stratixv_latch(D, ENA, PRE, CLR, Q);
   
   input D;
   input ENA, PRE, CLR;
   output Q;
   
   reg 	  q_out;
   
   specify
      $setup (D, negedge ENA, 0) ;
      $hold (negedge ENA, D, 0) ;
      
      (D => Q) = (0, 0);
      (negedge ENA => (Q +: q_out)) = (0, 0);
      (negedge PRE => (Q +: q_out)) = (0, 0);
      (negedge CLR => (Q +: q_out)) = (0, 0);
   endspecify
   
   wire D_in;
   wire ENA_in;
   wire PRE_in;
   wire CLR_in;
   
   buf (D_in, D);
   buf (ENA_in, ENA);
   buf (PRE_in, PRE);
   buf (CLR_in, CLR);
   
   initial
      begin
	 q_out <= 1'b0;
      end
   
   always @(D_in or ENA_in or PRE_in or CLR_in)
      begin
	 if (PRE_in == 1'b0)
	    begin
	       // latch being preset, preset is active low
	       q_out <= 1'b1;
	    end
	 else if (CLR_in == 1'b0)
	    begin
	       // latch being cleared, clear is active low
	       q_out <= 1'b0;
	    end
	      else if (ENA_in == 1'b1)
		 begin
		    // latch is transparent
		    q_out <= D_in;
		 end
      end
   
   and (Q, q_out, 1'b1);
   
endmodule

// ***** stratixv_mux21

module stratixv_mux21 (MO, A, B, S);
   input A, B, S;
   output MO;
   
   wire A_in;
   wire B_in;
   wire S_in;

   buf(A_in, A);
   buf(B_in, B);
   buf(S_in, S);

   wire   tmp_MO;
   
   specify
      (A => MO) = (0, 0);
      (B => MO) = (0, 0);
      (S => MO) = (0, 0);
   endspecify

   assign tmp_MO = (S_in == 1) ? B_in : A_in;
   
   buf (MO, tmp_MO);
endmodule

// ***** stratixv_mux41

module stratixv_mux41 (MO, IN0, IN1, IN2, IN3, S);
   input IN0;
   input IN1;
   input IN2;
   input IN3;
   input [1:0] S;
   output MO;
   
   wire IN0_in;
   wire IN1_in;
   wire IN2_in;
   wire IN3_in;
   wire S1_in;
   wire S0_in;

   buf(IN0_in, IN0);
   buf(IN1_in, IN1);
   buf(IN2_in, IN2);
   buf(IN3_in, IN3);
   buf(S1_in, S[1]);
   buf(S0_in, S[0]);

   wire   tmp_MO;
   
   specify
      (IN0 => MO) = (0, 0);
      (IN1 => MO) = (0, 0);
      (IN2 => MO) = (0, 0);
      (IN3 => MO) = (0, 0);
      (S[1] => MO) = (0, 0);
      (S[0] => MO) = (0, 0);
   endspecify

   assign tmp_MO = S1_in ? (S0_in ? IN3_in : IN2_in) : (S0_in ? IN1_in : IN0_in);

   buf (MO, tmp_MO);

endmodule

// ***** stratixv_and1

module stratixv_and1 (Y, IN1);
   input IN1;
   output Y;
   
   specify
      (IN1 => Y) = (0, 0);
   endspecify
   
   buf (Y, IN1);
endmodule

// ***** stratixv_and16

module stratixv_and16 (Y, IN1);
   input [15:0] IN1;
   output [15:0] Y;
   
   specify
      (IN1 => Y) = (0, 0);
   endspecify
   
   buf (Y[0], IN1[0]);
   buf (Y[1], IN1[1]);
   buf (Y[2], IN1[2]);
   buf (Y[3], IN1[3]);
   buf (Y[4], IN1[4]);
   buf (Y[5], IN1[5]);
   buf (Y[6], IN1[6]);
   buf (Y[7], IN1[7]);
   buf (Y[8], IN1[8]);
   buf (Y[9], IN1[9]);
   buf (Y[10], IN1[10]);
   buf (Y[11], IN1[11]);
   buf (Y[12], IN1[12]);
   buf (Y[13], IN1[13]);
   buf (Y[14], IN1[14]);
   buf (Y[15], IN1[15]);
   
endmodule

// ***** stratixv_bmux21

module stratixv_bmux21 (MO, A, B, S);
   input [15:0] A, B;
   input 	S;
   output [15:0] MO; 
   
   assign MO = (S == 1) ? B : A; 
   
endmodule

// ***** stratixv_b17mux21

module stratixv_b17mux21 (MO, A, B, S);
   input [16:0] A, B;
   input 	S;
   output [16:0] MO; 
   
   assign MO = (S == 1) ? B : A; 
   
endmodule

// ***** stratixv_nmux21

module stratixv_nmux21 (MO, A, B, S);
   input A, B, S; 
   output MO; 
   
   assign MO = (S == 1) ? ~B : ~A; 
   
endmodule

// ***** stratixv_b5mux21

module stratixv_b5mux21 (MO, A, B, S);
   input [4:0] A, B;
   input       S;
   output [4:0] MO; 
   
   assign MO = (S == 1) ? B : A; 
   
endmodule

// ********** END PRIMITIVE DEFINITIONS **********


//------------------------------------------------------------------
//
// Module Name : stratixv_ff
//
// Description : STRATIXV FF Verilog simulation model 
//
//------------------------------------------------------------------
`timescale 1 ps/1 ps
  
module stratixv_ff (
    d, 
    clk, 
    clrn, 
    aload, 
    sclr, 
    sload, 
    asdata, 
    ena, 
    devclrn, 
    devpor, 
    q
    );
   
parameter power_up = "low";
parameter x_on_violation = "on";
parameter lpm_type = "stratixv_ff";

input d;
input clk;
input clrn;
input aload; 
input sclr; 
input sload; 
input asdata; 
input ena; 
input devclrn; 
input devpor; 

output q;

tri1 devclrn;
tri1 devpor;

reg q_tmp;
wire reset;
   
reg d_viol;
reg sclr_viol;
reg sload_viol;
reg asdata_viol;
reg ena_viol; 
reg violation;

reg clk_last_value;
   
reg ix_on_violation;

wire d_in;
wire clk_in;
wire clrn_in;
wire aload_in;
wire sclr_in;
wire sload_in;
wire asdata_in;
wire ena_in;
   
wire nosloadsclr;
wire sloaddata;

buf (d_in, d);
buf (clk_in, clk);
buf (clrn_in, clrn);
buf (aload_in, aload);
buf (sclr_in, sclr);
buf (sload_in, sload);
buf (asdata_in, asdata);
buf (ena_in, ena);
   
assign reset = devpor && devclrn && clrn_in && ena_in;
assign nosloadsclr = reset && (!sload_in && !sclr_in);
assign sloaddata = reset && sload_in;
   
specify

    $setuphold (posedge clk &&& nosloadsclr, d, 0, 0, d_viol) ;
    $setuphold (posedge clk &&& reset, sclr, 0, 0, sclr_viol) ;
    $setuphold (posedge clk &&& reset, sload, 0, 0, sload_viol) ;
    $setuphold (posedge clk &&& sloaddata, asdata, 0, 0, asdata_viol) ;
    $setuphold (posedge clk &&& reset, ena, 0, 0, ena_viol) ;
      
    (posedge clk => (q +: q_tmp)) = 0 ;
    (posedge clrn => (q +: 1'b0)) = (0, 0) ;
    (posedge aload => (q +: q_tmp)) = (0, 0) ;
    (asdata => q) = (0, 0) ;
      
endspecify
   
initial
begin
    violation = 'b0;
    clk_last_value = 'b0;

    if (power_up == "low")
        q_tmp = 'b0;
    else if (power_up == "high")
        q_tmp = 'b1;

    if (x_on_violation == "on")
        ix_on_violation = 1;
    else
        ix_on_violation = 0;
end
   
always @ (d_viol or sclr_viol or sload_viol or ena_viol or asdata_viol)
begin
    if (ix_on_violation == 1)
        violation = 'b1;
end
   
always @ (asdata_in or clrn_in or posedge aload_in or 
          devclrn or devpor)
begin
    if (devpor == 'b0)
        q_tmp <= 'b0;
    else if (devclrn == 'b0)
        q_tmp <= 'b0;
    else if (clrn_in == 'b0) 
        q_tmp <= 'b0;
    else if (aload_in == 'b1) 
        q_tmp <= asdata_in;
end
   
always @ (clk_in or posedge clrn_in or posedge aload_in or 
          devclrn or devpor or posedge violation)
begin
    if (violation == 1'b1)
    begin
        violation = 'b0;
        q_tmp <= 'bX;
    end
    else
    begin
        if (devpor == 'b0 || devclrn == 'b0 || clrn_in === 'b0)
            q_tmp <= 'b0;
        else if (aload_in === 'b1) 
            q_tmp <= asdata_in;
        else if (ena_in === 'b1 && clk_in === 'b1 && clk_last_value === 'b0)
        begin
            if (sclr_in === 'b1)
                q_tmp <= 'b0 ;
            else if (sload_in === 'b1)
                q_tmp <= asdata_in;
            else 
                q_tmp <= d_in;
        end
    end

    clk_last_value = clk_in;
end

and (q, q_tmp, 1'b1);

endmodule

//------------------------------------------------------------------
//
// Module Name : stratixv_lcell_comb
//
// Description : STRATIXV LCELL_COMB Verilog simulation model 
//
//------------------------------------------------------------------

`timescale 1 ps/1 ps
  
module stratixv_lcell_comb (
                             dataa, 
                             datab, 
                             datac, 
                             datad, 
                             datae, 
                             dataf, 
                             datag, 
                             cin,
                             sharein, 
                             combout, 
                             sumout,
                             cout, 
                             shareout 
                            );

input dataa;
input datab;
input datac;
input datad;
input datae;
input dataf;
input datag;
input cin;
input sharein;

output combout;
output sumout;
output cout;
output shareout;

parameter lut_mask = 64'hFFFFFFFFFFFFFFFF;
parameter shared_arith = "off";
parameter extended_lut = "off";
parameter dont_touch = "off";
parameter lpm_type = "stratixv_lcell_comb";

// sub masks
wire [15:0] f0_mask;
wire [15:0] f1_mask;
wire [15:0] f2_mask;
wire [15:0] f3_mask;

// sub lut outputs
reg f0_out;
reg f1_out;
reg f2_out;
reg f3_out;

// mux output for extended mode
reg g0_out;
reg g1_out;

// either datac or datag
reg f2_input3;

// F2 output using dataf
reg f2_f;

// second input to the adder
reg adder_input2;

// tmp output variables
reg combout_tmp;
reg sumout_tmp;
reg cout_tmp;

// integer representations for string parameters
reg ishared_arith;
reg iextended_lut;

// 4-input LUT function
function lut4;
input [15:0] mask;
input dataa;
input datab;
input datac;
input datad;
      
begin

    lut4 = datad ? ( datac ? ( datab ? ( dataa ? mask[15] : mask[14])
                                     : ( dataa ? mask[13] : mask[12]))
                           : ( datab ? ( dataa ? mask[11] : mask[10]) 
                                     : ( dataa ? mask[ 9] : mask[ 8])))
                 : ( datac ? ( datab ? ( dataa ? mask[ 7] : mask[ 6]) 
                                     : ( dataa ? mask[ 5] : mask[ 4]))
                           : ( datab ? ( dataa ? mask[ 3] : mask[ 2]) 
                                     : ( dataa ? mask[ 1] : mask[ 0])));

end
endfunction

// 5-input LUT function
function lut5;
input [31:0] mask;
input dataa;
input datab;
input datac;
input datad;
input datae;
reg e0_lut;
reg e1_lut;
reg [15:0] e0_mask;
reg [31:16] e1_mask;

      
begin

    e0_mask = mask[15:0];
    e1_mask = mask[31:16];

	 begin
        e0_lut = lut4(e0_mask, dataa, datab, datac, datad);
        e1_lut = lut4(e1_mask, dataa, datab, datac, datad);

        if (datae === 1'bX) // X propogation
        begin
            if (e0_lut == e1_lut)
            begin
                lut5 = e0_lut;
            end
            else
            begin
                lut5 = 1'bX;
            end
        end
        else
        begin
            lut5 = (datae == 1'b1) ? e1_lut : e0_lut;
        end
    end
end
endfunction

// 6-input LUT function
function lut6;
input [63:0] mask;
input dataa;
input datab;
input datac;
input datad;
input datae;
input dataf;
reg f0_lut;
reg f1_lut;
reg [31:0] f0_mask;
reg [63:32] f1_mask ;
      
begin

    f0_mask = mask[31:0];
    f1_mask = mask[63:32];

	 begin

        lut6 = mask[{dataf, datae, datad, datac, datab, dataa}];

        if (lut6 === 1'bX)
        begin
            f0_lut = lut5(f0_mask, dataa, datab, datac, datad, datae);
            f1_lut = lut5(f1_mask, dataa, datab, datac, datad, datae);
    
            if (dataf === 1'bX) // X propogation
            begin
                if (f0_lut == f1_lut)
                begin
                    lut6 = f0_lut;
                end
                else
                begin
                    lut6 = 1'bX;
                end
            end
            else
            begin
                lut6 = (dataf == 1'b1) ? f1_lut : f0_lut;
            end
        end
    end
end
endfunction

wire dataa_in;
wire datab_in;
wire datac_in;
wire datad_in;
wire datae_in;
wire dataf_in;
wire datag_in;
wire cin_in;
wire sharein_in;

buf(dataa_in, dataa);
buf(datab_in, datab);
buf(datac_in, datac);
buf(datad_in, datad);
buf(datae_in, datae);
buf(dataf_in, dataf);
buf(datag_in, datag);
buf(cin_in, cin);
buf(sharein_in, sharein);

specify

    (dataa => combout) = (0, 0);
    (datab => combout) = (0, 0);
    (datac => combout) = (0, 0);
    (datad => combout) = (0, 0);
    (datae => combout) = (0, 0);
    (dataf => combout) = (0, 0);
    (datag => combout) = (0, 0);

    (dataa => sumout) = (0, 0);
    (datab => sumout) = (0, 0);
    (datac => sumout) = (0, 0);
    (datad => sumout) = (0, 0);
    (dataf => sumout) = (0, 0);
    (cin => sumout) = (0, 0);
    (sharein => sumout) = (0, 0);

    (dataa => cout) = (0, 0);
    (datab => cout) = (0, 0);
    (datac => cout) = (0, 0);
    (datad => cout) = (0, 0);
    (dataf => cout) = (0, 0);
    (cin => cout) = (0, 0);
    (sharein => cout) = (0, 0);

    (dataa => shareout) = (0, 0);
    (datab => shareout) = (0, 0);
    (datac => shareout) = (0, 0);
    (datad => shareout) = (0, 0);

endspecify

initial
begin
    if (shared_arith == "on")
        ishared_arith = 1;
    else
        ishared_arith = 0;

    if (extended_lut == "on")
        iextended_lut = 1;
    else
        iextended_lut = 0;

    f0_out = 1'b0;
    f1_out = 1'b0;
    f2_out = 1'b0;
    f3_out = 1'b0;
    g0_out = 1'b0;
    g1_out = 1'b0;
    f2_input3 = 1'b0;
    adder_input2 = 1'b0;
    f2_f = 1'b0;
    combout_tmp = 1'b0;
    sumout_tmp = 1'b0;
    cout_tmp = 1'b0;
end

// sub masks and outputs
assign f0_mask = lut_mask[15:0];
assign f1_mask = lut_mask[31:16];
assign f2_mask = lut_mask[47:32];
assign f3_mask = lut_mask[63:48];

always @(datag_in or dataf_in or datae_in or datad_in or datac_in or 
         datab_in or dataa_in or cin_in or sharein_in)
begin

    // check for extended LUT mode
    if (iextended_lut == 1) 
        f2_input3 = datag_in;
    else
        f2_input3 = datac_in;

    f0_out = lut4(f0_mask, dataa_in, datab_in, datac_in, datad_in);
    f1_out = lut4(f1_mask, dataa_in, datab_in, f2_input3, datad_in);
    f2_out = lut4(f2_mask, dataa_in, datab_in, datac_in, datad_in);
    f3_out = lut4(f3_mask, dataa_in, datab_in, f2_input3, datad_in);

    // combout is the 6-input LUT
    if (iextended_lut == 1)
    begin
        if (datae_in == 1'b0)
        begin
            g0_out = f0_out;
            g1_out = f2_out;
        end
        else if (datae_in == 1'b1)
        begin
            g0_out = f1_out;
            g1_out = f3_out;
        end
        else
        begin
            if (f0_out == f1_out)
                g0_out = f0_out;
            else
                g0_out = 1'bX;

            if (f2_out == f3_out)
                g1_out = f2_out;
            else
                g1_out = 1'bX;
        end
    
        if (dataf_in == 1'b0)
            combout_tmp = g0_out;
        else if ((dataf_in == 1'b1) || (g0_out == g1_out))
            combout_tmp = g1_out;
        else
            combout_tmp = 1'bX;
    end
    else
        combout_tmp = lut6(lut_mask, dataa_in, datab_in, datac_in, 
                           datad_in, datae_in, dataf_in);

    // check for shareed arithmetic mode
    if (ishared_arith == 1) 
        adder_input2 = sharein_in;
    else
    begin
        f2_f = lut4(f2_mask, dataa_in, datab_in, datac_in, dataf_in);
        adder_input2 = !f2_f;
    end

    // sumout & cout
    sumout_tmp = cin_in ^ f0_out ^ adder_input2;
    cout_tmp = (cin_in & f0_out) | (cin_in & adder_input2) | 
               (f0_out & adder_input2);

end

and (combout, combout_tmp, 1'b1);
and (sumout, sumout_tmp, 1'b1);
and (cout, cout_tmp, 1'b1);
and (shareout, f2_out, 1'b1);

endmodule

// Deactivate the LEDA rule that requires uppercase letters for all
// parameter names
// leda rule_G_521_3_B off

`timescale 1 ps/1 ps

`define PRIME 1
`define SEC   0

//--------------------------------------------------------------------------
// Module Name     : stratixv_ram_block
// Description     : Main RAM module
//--------------------------------------------------------------------------

module stratixv_ram_block
    (
     portadatain,
     portaaddr,
     portawe,
     portare,
     portbdatain,
     portbaddr,
     portbwe,
     portbre,
     clk0, clk1,
     ena0, ena1,
     ena2, ena3,
     clr0, clr1,
     nerror,
     portabyteenamasks,
     portbbyteenamasks,
     portaaddrstall,
     portbaddrstall,
     devclrn,
     devpor,
     eccstatus,
     portadataout,
     portbdataout
      ,dftout
     );
// -------- GLOBAL PARAMETERS ---------
parameter operation_mode = "single_port";
parameter mixed_port_feed_through_mode = "dont_care";
parameter ram_block_type = "auto";
parameter logical_ram_name = "ram_name";

parameter init_file = "init_file.hex";
parameter init_file_layout = "none";

parameter ecc_pipeline_stage_enabled = "false";
parameter enable_ecc = "false";
parameter width_eccstatus = 2;
parameter data_interleave_width_in_bits = 1;
parameter data_interleave_offset_in_bits = 1;
parameter port_a_logical_ram_depth = 0;
parameter port_a_logical_ram_width = 0;
parameter port_a_first_address = 0;
parameter port_a_last_address = 0;
parameter port_a_first_bit_number = 0;

parameter port_a_data_out_clear = "none";

parameter port_a_data_out_clock = "none";

parameter port_a_data_width = 1;
parameter port_a_address_width = 1;
parameter port_a_byte_enable_mask_width = 1;

parameter port_b_logical_ram_depth = 0;
parameter port_b_logical_ram_width = 0;
parameter port_b_first_address = 0;
parameter port_b_last_address = 0;
parameter port_b_first_bit_number = 0;

parameter port_b_address_clear = "none";
parameter port_b_data_out_clear = "none";

parameter port_b_data_in_clock = "clock1";
parameter port_b_address_clock = "clock1";
parameter port_b_write_enable_clock = "clock1";
parameter port_b_read_enable_clock  = "clock1";
parameter port_b_byte_enable_clock = "clock1";
parameter port_b_data_out_clock = "none";

parameter port_b_data_width = 1;
parameter port_b_address_width = 1;
parameter port_b_byte_enable_mask_width = 1;

parameter port_a_read_during_write_mode = "new_data_no_nbe_read";
parameter port_b_read_during_write_mode = "new_data_no_nbe_read";
parameter power_up_uninitialized = "false";
parameter lpm_type = "stratixv_ram_block";
parameter lpm_hint = "true";
parameter connectivity_checking = "off";

parameter mem_init0 = 2048'b0;
parameter mem_init1 = 2048'b0;
parameter mem_init2 = 2048'b0;
parameter mem_init3 = 2048'b0;
parameter mem_init4 = 2048'b0;
parameter mem_init5 = 2048'b0;
parameter mem_init6 = 2048'b0;
parameter mem_init7 = 2048'b0;
parameter mem_init8 = 2048'b0;
parameter mem_init9 = 2048'b0;

parameter port_a_byte_size = 0;
parameter port_b_byte_size = 0;

parameter clk0_input_clock_enable  = "none"; // ena0,ena2,none
parameter clk0_core_clock_enable   = "none"; // ena0,ena2,none
parameter clk0_output_clock_enable = "none"; // ena0,none
parameter clk1_input_clock_enable  = "none"; // ena1,ena3,none
parameter clk1_core_clock_enable   = "none"; // ena1,ena3,none
parameter clk1_output_clock_enable = "none"; // ena1,none

parameter bist_ena = "false"; //false, true 

// SIMULATION_ONLY_PARAMETERS_BEGIN

parameter port_a_address_clear = "none";

parameter port_a_data_in_clock = "clock0";
parameter port_a_address_clock = "clock0";
parameter port_a_write_enable_clock = "clock0";
parameter port_a_byte_enable_clock = "clock0";
parameter port_a_read_enable_clock = "clock0";

// SIMULATION_ONLY_PARAMETERS_END

// -------- PORT DECLARATIONS ---------
input portawe;
input portare;
input [port_a_data_width - 1:0] portadatain;
input [port_a_address_width - 1:0] portaaddr;
input [port_a_byte_enable_mask_width - 1:0] portabyteenamasks;

input portbwe, portbre;
input [port_b_data_width - 1:0] portbdatain;
input [port_b_address_width - 1:0] portbaddr;
input [port_b_byte_enable_mask_width - 1:0] portbbyteenamasks;

input clr0,clr1;
input clk0,clk1;
input ena0,ena1;
input ena2,ena3;
input nerror;

input devclrn,devpor;
input portaaddrstall;
input portbaddrstall;
output [port_a_data_width - 1:0] portadataout;
output [port_b_data_width - 1:0] portbdataout;
output [width_eccstatus - 1:0] eccstatus;
output [8:0] dftout;

// -------- RAM BLOCK INSTANTIATION ---
generic_m20k ram_core0
(
	.portawe(portawe),
	.portare(portare),
	.portadatain(portadatain),
	.portaaddr(portaaddr),
	.portabyteenamasks(portabyteenamasks),
	.portbwe(portbwe),
	.portbre(portbre),
	.portbdatain(portbdatain),
	.portbaddr(portbaddr),
	.portbbyteenamasks(portbbyteenamasks),
	.clr0(clr0),
	.clr1(clr1),
	.clk0(clk0),
	.clk1(clk1),
	.ena0(ena0),
	.ena1(ena1),
	.ena2(ena2),
	.ena3(ena3),
	.nerror(nerror),
	.devclrn(devclrn),
	.devpor(devpor),
	.portaaddrstall(portaaddrstall),
	.portbaddrstall(portbaddrstall),
	.portadataout(portadataout),
	.portbdataout(portbdataout),
	.eccstatus(eccstatus),
	.dftout(dftout)
);
defparam ram_core0.operation_mode = operation_mode;
defparam ram_core0.mixed_port_feed_through_mode = mixed_port_feed_through_mode;
defparam ram_core0.ram_block_type = ram_block_type;
defparam ram_core0.logical_ram_name = logical_ram_name;
defparam ram_core0.init_file = init_file;
defparam ram_core0.init_file_layout = init_file_layout;
defparam ram_core0.ecc_pipeline_stage_enabled = ecc_pipeline_stage_enabled;
defparam ram_core0.enable_ecc = enable_ecc;
defparam ram_core0.width_eccstatus = width_eccstatus;
defparam ram_core0.data_interleave_width_in_bits = data_interleave_width_in_bits;
defparam ram_core0.data_interleave_offset_in_bits = data_interleave_offset_in_bits;
defparam ram_core0.port_a_logical_ram_depth = port_a_logical_ram_depth;
defparam ram_core0.port_a_logical_ram_width = port_a_logical_ram_width;
defparam ram_core0.port_a_first_address = port_a_first_address;
defparam ram_core0.port_a_last_address = port_a_last_address;
defparam ram_core0.port_a_first_bit_number = port_a_first_bit_number;
defparam ram_core0.port_a_data_out_clear = port_a_data_out_clear;
defparam ram_core0.port_a_data_out_clock = port_a_data_out_clock;
defparam ram_core0.port_a_data_width = port_a_data_width;
defparam ram_core0.port_a_address_width = port_a_address_width;
defparam ram_core0.port_a_byte_enable_mask_width = port_a_byte_enable_mask_width;
defparam ram_core0.port_b_logical_ram_depth = port_b_logical_ram_depth;
defparam ram_core0.port_b_logical_ram_width = port_b_logical_ram_width;
defparam ram_core0.port_b_first_address = port_b_first_address;
defparam ram_core0.port_b_last_address = port_b_last_address;
defparam ram_core0.port_b_first_bit_number = port_b_first_bit_number;
defparam ram_core0.port_b_address_clear = port_b_address_clear;
defparam ram_core0.port_b_data_out_clear = port_b_data_out_clear;
defparam ram_core0.port_b_data_in_clock = port_b_data_in_clock;
defparam ram_core0.port_b_address_clock = port_b_address_clock;
defparam ram_core0.port_b_write_enable_clock = port_b_write_enable_clock;
defparam ram_core0.port_b_read_enable_clock = port_b_read_enable_clock;
defparam ram_core0.port_b_byte_enable_clock = port_b_byte_enable_clock;
defparam ram_core0.port_b_data_out_clock = port_b_data_out_clock;
defparam ram_core0.port_b_data_width = port_b_data_width;
defparam ram_core0.port_b_address_width = port_b_address_width;
defparam ram_core0.port_b_byte_enable_mask_width = port_b_byte_enable_mask_width;
defparam ram_core0.port_a_read_during_write_mode = port_a_read_during_write_mode;
defparam ram_core0.port_b_read_during_write_mode = port_b_read_during_write_mode;
defparam ram_core0.power_up_uninitialized = power_up_uninitialized;
defparam ram_core0.lpm_type = lpm_type;
defparam ram_core0.lpm_hint = lpm_hint;
defparam ram_core0.connectivity_checking = connectivity_checking;
defparam ram_core0.mem_init0 = mem_init0;
defparam ram_core0.mem_init1 = mem_init1;
defparam ram_core0.mem_init2 = mem_init2;
defparam ram_core0.mem_init3 = mem_init3;
defparam ram_core0.mem_init4 = mem_init4;
defparam ram_core0.mem_init5 = mem_init5;
defparam ram_core0.mem_init6 = mem_init6;
defparam ram_core0.mem_init7 = mem_init7;
defparam ram_core0.mem_init8 = mem_init8;
defparam ram_core0.mem_init9 = mem_init9;
defparam ram_core0.port_a_byte_size = port_a_byte_size;
defparam ram_core0.port_b_byte_size = port_b_byte_size;
defparam ram_core0.clk0_input_clock_enable = clk0_input_clock_enable;
defparam ram_core0.clk0_core_clock_enable = clk0_core_clock_enable ;
defparam ram_core0.clk0_output_clock_enable = clk0_output_clock_enable;
defparam ram_core0.clk1_input_clock_enable = clk1_input_clock_enable;
defparam ram_core0.clk1_core_clock_enable = clk1_core_clock_enable;
defparam ram_core0.clk1_output_clock_enable = clk1_output_clock_enable;
defparam ram_core0.bist_ena = bist_ena;
defparam ram_core0.port_a_address_clear = port_a_address_clear;
defparam ram_core0.port_a_data_in_clock = port_a_data_in_clock;
defparam ram_core0.port_a_address_clock = port_a_address_clock;
defparam ram_core0.port_a_write_enable_clock = port_a_write_enable_clock;
defparam ram_core0.port_a_byte_enable_clock = port_a_byte_enable_clock;
defparam ram_core0.port_a_read_enable_clock = port_a_read_enable_clock;

endmodule // stratixv_ram_block

// Activate again the LEDA rule that requires uppercase letters for all
// parameter names
// leda rule_G_521_3_B on



//--------------------------------------------------------------------------
// Module Name     : stratixv_mlab_cell
// Description     : Main RAM module
//--------------------------------------------------------------------------

`timescale 1 ps/1 ps

module stratixv_mlab_cell
    (
     portadatain,
     portaaddr, 
     portabyteenamasks, 
     portbaddr,
     clk0, clk1,
     ena0, ena1,
   	 ena2,
   	 clr,
	 devclrn,
     devpor,
     portbdataout
     );
// -------- GLOBAL PARAMETERS ---------

parameter logical_ram_name = "lutram";

parameter logical_ram_depth = 0;
parameter logical_ram_width = 0;
parameter first_address = 0;
parameter last_address = 0;
parameter first_bit_number = 0;

parameter mixed_port_feed_through_mode = "new";
parameter init_file = "NONE";

parameter data_width = 20;
parameter address_width = 6;
parameter byte_enable_mask_width = 1;
parameter byte_size = 1;
parameter port_b_data_out_clock = "none";
parameter port_b_data_out_clear = "none";

parameter lpm_type = "stratixv_mlab_cell";
parameter lpm_hint = "true";

parameter mem_init0 = 640'b0; // 64x10 OR 32x20

// -------- PORT DECLARATIONS ---------
input [data_width - 1:0] portadatain;
input [address_width - 1:0] portaaddr;
input [byte_enable_mask_width - 1:0] portabyteenamasks;
input [address_width - 1:0] portbaddr;

input clk0;
input clk1;

input ena0;
input ena1;
input ena2;

input clr;

input devclrn;
input devpor;

output [data_width - 1:0] portbdataout;

generic_mlab_cell my_lutram0
(
	.portadatain(portadatain),
	.portaaddr(portaaddr),
	.portabyteenamasks(portabyteenamasks),
	.portbaddr(portbaddr),
	.clk0(clk0),
	.clk1(clk1),
	.ena0(ena0),
	.ena1(ena1),
	.ena2(ena2),
	.clr(clr),
	.devclrn(devclrn),
	.devpor(devpor),
	.portbdataout(portbdataout)
);
defparam my_lutram0.logical_ram_name = logical_ram_name;
defparam my_lutram0.logical_ram_depth = logical_ram_depth;
defparam my_lutram0.logical_ram_width = logical_ram_width;
defparam my_lutram0.first_address = first_address;
defparam my_lutram0.last_address = last_address;
defparam my_lutram0.first_bit_number = first_bit_number;
defparam my_lutram0.mixed_port_feed_through_mode = mixed_port_feed_through_mode;
defparam my_lutram0.init_file = init_file;
defparam my_lutram0.data_width = data_width;
defparam my_lutram0.address_width = address_width;
defparam my_lutram0.byte_enable_mask_width = byte_enable_mask_width;
defparam my_lutram0.byte_size = byte_size;
defparam my_lutram0.port_b_data_out_clock = port_b_data_out_clock;
defparam my_lutram0.port_b_data_out_clear = port_b_data_out_clear;
defparam my_lutram0.lpm_type = lpm_type;
defparam my_lutram0.lpm_hint = lpm_hint;
defparam my_lutram0.mem_init0 = mem_init0;


endmodule // stratixv_mlab_cell
//////////////////////////////////////////////////////////////////////////////////
//Module Name:                    stratixv_io_ibuf                                 //
//Description:                    Simulation model for STRATIXV IO Input Buffer    //
//                                                                              //
//////////////////////////////////////////////////////////////////////////////////

module stratixv_io_ibuf (
                      i,
                      ibar,
                      dynamicterminationcontrol,      
                      o
                     );

// SIMULATION_ONLY_PARAMETERS_BEGIN

parameter differential_mode = "false";
parameter bus_hold = "false";
parameter simulate_z_as = "Z";
parameter lpm_type = "stratixv_io_ibuf";

// SIMULATION_ONLY_PARAMETERS_END

//Input Ports Declaration
input i;
input ibar;
input dynamicterminationcontrol; 

//Output Ports Declaration
output o;

// Internal signals
reg out_tmp;
reg o_tmp;
wire out_val ;
reg prev_value;

specify
    (i => o)    = (0, 0);
    (ibar => o) = (0, 0);
endspecify

initial
    begin
        prev_value = 1'b0;
    end

always@(i or ibar)
    begin
        if(differential_mode == "false")
            begin
                if(i == 1'b1)
                    begin
                        o_tmp = 1'b1;
                        prev_value = 1'b1;
                    end
                else if(i == 1'b0)
                    begin
                        o_tmp = 1'b0;
                        prev_value = 1'b0;
                    end
                else if( i === 1'bz)
                    o_tmp = out_val;
                else
                    o_tmp = i;
                    
                if( bus_hold == "true")
                    out_tmp = prev_value;
                else
                    out_tmp = o_tmp;
            end
        else
            begin
                case({i,ibar})
                    2'b00: out_tmp = 1'bX;
                    2'b01: out_tmp = 1'b0;
                    2'b10: out_tmp = 1'b1;
                    2'b11: out_tmp = 1'bX;
                    default: out_tmp = 1'bX;
                endcase

        end
    end
    
assign out_val = (simulate_z_as == "Z") ? 1'bz :
                 (simulate_z_as == "X") ? 1'bx :
                 (simulate_z_as == "vcc")? 1'b1 :
                 (simulate_z_as == "gnd") ? 1'b0 : 1'bz;

pmos (o, out_tmp, 1'b0);

endmodule

//////////////////////////////////////////////////////////////////////////////////
//Module Name:                    stratixv_io_obuf                                 //
//Description:                    Simulation model for STRATIXV IO Output Buffer   //
//                                                                              //
//////////////////////////////////////////////////////////////////////////////////

module stratixv_io_obuf (
                      i,
                      oe,
                      dynamicterminationcontrol,      
                      seriesterminationcontrol,
                      parallelterminationcontrol,     
                      devoe,
                      o,
                      obar
                    );

//Parameter Declaration
parameter open_drain_output = "false";
parameter bus_hold = "false";
parameter shift_series_termination_control = "false";  
parameter sim_dynamic_termination_control_is_connected = "false"; 
parameter lpm_type = "stratixv_io_obuf";

//Input Ports Declaration
input i;
input oe;
input devoe;
input dynamicterminationcontrol; 
input [15:0] seriesterminationcontrol;
input [15:0] parallelterminationcontrol;

//Outout Ports Declaration
output o;
output obar;

//INTERNAL Signals
reg out_tmp;
reg out_tmp_bar;
reg prev_value;
wire tmp;
wire tmp_bar;
wire tmp1;
wire tmp1_bar;

tri1 devoe;

specify
    (i => o)    = (0, 0);
    (i => obar) = (0, 0);
    (oe => o)   = (0, 0);
    (oe => obar)   = (0, 0);
endspecify

initial
    begin
        prev_value = 'b0;
        out_tmp = 'bz;
    end

always@(i or oe)
    begin
        if(oe == 1'b1)
            begin
                if(open_drain_output == "true")
                    begin
                        if(i == 'b0)
                             begin
                                 out_tmp = 'b0;
                                 out_tmp_bar = 'b1;
                                 prev_value = 'b0;
                             end
                        else
                             begin
                                 out_tmp = 'bz;
                                 out_tmp_bar = 'bz;
                             end
                    end
                else
                    begin
                        if( i == 'b0)
                            begin
                                out_tmp = 'b0;
                                out_tmp_bar = 'b1;
                                prev_value = 'b0;
                            end
                        else if( i == 'b1)
                            begin
                                out_tmp = 'b1;
                                out_tmp_bar = 'b0;
                                prev_value = 'b1;
                            end
                        else
                            begin
                                out_tmp = i;
                                out_tmp_bar = i;
                            end
                    end
            end
        else if(oe == 1'b0)
            begin
                out_tmp = 'bz;
                out_tmp_bar = 'bz;
            end
        else
            begin
                out_tmp = 'bx;
                out_tmp_bar = 'bx;
            end
    end

assign tmp = (bus_hold == "true") ? prev_value : out_tmp;
assign tmp_bar = (bus_hold == "true") ? !prev_value : out_tmp_bar;
assign tmp1 = ((oe == 1'b1) && (dynamicterminationcontrol == 1'b1) && (sim_dynamic_termination_control_is_connected == "true")) ? 1'bx :(devoe == 1'b1) ? tmp : 1'bz; 
assign tmp1_bar =((oe == 1'b1) && (dynamicterminationcontrol == 1'b1)&& (sim_dynamic_termination_control_is_connected == "true")) ? 1'bx : (devoe == 1'b1) ? tmp_bar : 1'bz; 



pmos (o, tmp1, 1'b0);
pmos (obar, tmp1_bar, 1'b0);

endmodule

//////////////////////////////////////////////////////////////////////////////////
//Module Name:                    stratixv_ddio_out                                //
//Description:                    Simulation model for STRATIXV DDIO Output        //
//                                                                              //
//////////////////////////////////////////////////////////////////////////////////

module stratixv_ddio_out (
                        datainlo,
                        datainhi,
                        clk,
                        clkhi,
                        clklo,
                        muxsel,
                        ena,
                        areset,
                        sreset,
                        dataout,
                        dfflo,
                        dffhi,
                        devpor,
                        devclrn
                     );

//Parameters Declaration
parameter power_up = "low";
parameter async_mode = "none";
parameter sync_mode = "none";
parameter half_rate_mode = "false"; 
parameter use_new_clocking_model = "false";
parameter lpm_type = "stratixv_ddio_out";

//Input Ports Declaration
input datainlo;
input datainhi;
input clk;
input clkhi;
input clklo;
input muxsel;
input ena;
input areset;
input sreset;
input devpor;
input devclrn;

//Output Ports Declaration
output dataout;

//Buried Ports Declaration
output dfflo;
output [1:0] dffhi; 

tri1 devclrn;
tri1 devpor;

//Internal Signals
reg ddioreg_aclr;
reg ddioreg_adatasdata;
reg ddioreg_sclr;
reg ddioreg_sload;
reg ddioreg_prn;
reg viol_notifier;

wire dfflo_tmp;
wire dffhi_tmp;
wire mux_sel;
wire dffhi1_tmp; 
wire sel_mux_hi_in;
wire clk_hi;
wire clk_lo;
wire datainlo_tmp;
wire datainhi_tmp;
reg dinhi_tmp;
reg dinlo_tmp;
wire clk_hr; 

reg clk1;
reg clk2;

reg muxsel1;
reg muxsel2;
reg muxsel_tmp;
reg sel_mux_lo_in_tmp;
wire muxsel3;
wire clk3;
wire sel_mux_lo_in;

initial
begin
	ddioreg_aclr = 1'b1;
	ddioreg_prn = 1'b1;
	ddioreg_adatasdata = (sync_mode == "preset") ? 1'b1: 1'b0;
    ddioreg_sclr = 1'b0;
    ddioreg_sload = 1'b0;
end


assign dfflo = dfflo_tmp;
assign dffhi[0] = dffhi_tmp; 
assign dffhi[1] = dffhi1_tmp;

always@(clk)
begin
    clk1 = clk;
    clk2 <= clk1;
end

always@(muxsel)
begin
    muxsel1 = muxsel;
    muxsel2 <= muxsel1;
end

always@(dfflo_tmp)
begin
    sel_mux_lo_in_tmp <= dfflo_tmp;
end

always@(datainlo)
begin
    dinlo_tmp <= datainlo;
end

always@(datainhi)
begin
    dinhi_tmp <= datainhi;
end

always @(mux_sel) begin
   muxsel_tmp <= mux_sel;
end



always@(areset)
begin
        if(async_mode == "clear")
            begin
                ddioreg_aclr = !areset;
            end
        else if(async_mode == "preset")
            begin
                ddioreg_prn = !areset;
            end
end

always@(sreset )
begin
         if(sync_mode == "clear")
            begin
                ddioreg_sclr = sreset;
            end
        else if(sync_mode == "preset")
            begin
                ddioreg_sload = sreset;
            end
end

//DDIO HIGH Register
dffeas  ddioreg_hi(                                    
                   .d(datainhi_tmp),                   
                   .clk(clk_hi),                       
                   .clrn(ddioreg_aclr),                
                   .aload(1'b0),                       
                   .sclr(ddioreg_sclr),                
                   .sload(ddioreg_sload),              
                   .asdata(ddioreg_adatasdata),        
                   .ena(ena),                          
                   .prn(ddioreg_prn),                  
                   .q(dffhi_tmp),                      
                   .devpor(devpor),                    
                   .devclrn(devclrn)                   
                  );                                   
defparam ddioreg_hi.power_up = power_up;               


assign clk_hi = (use_new_clocking_model == "true") ?  clkhi : clk;
assign datainhi_tmp = dinhi_tmp; 

//DDIO Low Register
dffeas  ddioreg_lo(
                   .d(datainlo_tmp),
                   .clk(clk_lo),
                   .clrn(ddioreg_aclr),
                   .aload(1'b0),
                   .sclr(ddioreg_sclr),
                   .sload(ddioreg_sload),
                   .asdata(ddioreg_adatasdata),
                   .ena(ena),
                   .prn(ddioreg_prn),
                   .q(dfflo_tmp),
                   .devpor(devpor),
                   .devclrn(devclrn)
                  );
defparam ddioreg_lo.power_up = power_up;
assign clk_lo = (use_new_clocking_model == "true") ?  clklo : clk;
assign datainlo_tmp = dinlo_tmp;

//DDIO High Register
dffeas  ddioreg_hi1(                                               
                   .d(dffhi_tmp),                                  
                   .clk(!clk_hr),                                  
                   .clrn(ddioreg_aclr),                            
                   .aload(1'b0),                                   
                   .sclr(ddioreg_sclr),                            
                   .sload(ddioreg_sload),                          
                   .asdata(ddioreg_adatasdata),                    
                   .ena(ena),                                      
                   .prn(ddioreg_prn),                              
                   .q(dffhi1_tmp),                                 
                   .devpor(devpor),                                
                   .devclrn(devclrn)                               
                  );                                               
defparam ddioreg_hi1.power_up = power_up;                          
assign clk_hr = (use_new_clocking_model == "true") ?  clkhi : clk; 

//registered output selection
stratixv_mux21 sel_mux(
                    .MO(dataout),
                    .A(sel_mux_lo_in),
                    .B(sel_mux_hi_in),
                    .S(muxsel_tmp)
                   );

assign muxsel3 = muxsel2;
assign clk3 = clk2;
assign  mux_sel = (use_new_clocking_model == "true")? muxsel3 : clk3;
assign sel_mux_lo_in = sel_mux_lo_in_tmp;
assign sel_mux_hi_in = (half_rate_mode == "true") ? dffhi1_tmp : dffhi_tmp; 

endmodule

//////////////////////////////////////////////////////////////////////////////////
//Module Name:                    stratixv_ddio_oe                                 //
//Description:                    Simulation model for STRATIXV DDIO OE            //
//                                                                              //
//////////////////////////////////////////////////////////////////////////////////
module stratixv_ddio_oe (
                       oe,
                       clk,
                       ena,
                       areset,
                       sreset,
                       dataout,
                       dfflo,
                       dffhi,
                       devpor,
                       devclrn
                    );

//Parameters Declaration
parameter power_up = "low";
parameter async_mode = "none";
parameter sync_mode = "none";
parameter lpm_type = "stratixv_ddio_oe";

//Input Ports Declaration
input oe;
input clk;
input ena;
input areset;
input sreset;
input devpor;
input devclrn;

//Output Ports Declaration
output dataout;

//Buried Ports Declaration
output dfflo;
output dffhi;

tri1 devclrn;
tri1 devpor;

//Internal Signals
reg ddioreg_aclr;
reg ddioreg_prn;
reg ddioreg_adatasdata;
reg ddioreg_sclr;
reg ddioreg_sload;
reg viol_notifier;

initial
begin
	ddioreg_aclr = 1'b1;
	ddioreg_prn = 1'b1;
	ddioreg_adatasdata = 1'b0;
    ddioreg_sclr = 1'b0;
    ddioreg_sload = 1'b0;
end

wire dfflo_tmp;
wire dffhi_tmp;

always@(areset or sreset )
    begin
        if(async_mode == "clear")
            begin
                ddioreg_aclr = !areset;
                ddioreg_prn = 1'b1;
            end
        else if(async_mode == "preset")
            begin
                ddioreg_aclr = 'b1;
                ddioreg_prn = !areset;
            end
         else
            begin
                ddioreg_aclr = 'b1;
                ddioreg_prn = 'b1;
            end
            
         if(sync_mode == "clear")
            begin
                ddioreg_adatasdata = 'b0;
                ddioreg_sclr = sreset;
                ddioreg_sload = 'b0;
            end
        else if(sync_mode == "preset")
            begin
                ddioreg_adatasdata = 'b1;
                ddioreg_sclr = 'b0;
                ddioreg_sload = sreset;
            end
        else
            begin
                ddioreg_adatasdata = 'b0;
                ddioreg_sclr = 'b0;
                ddioreg_sload = 'b0;
            end
    end

//DDIO OE Register
dffeas  ddioreg_hi(
                   .d(oe),
                   .clk(clk),
                   .clrn(ddioreg_aclr),
                   .aload(1'b0),
                   .sclr(ddioreg_sclr),
                   .sload(ddioreg_sload),
                   .asdata(ddioreg_adatasdata),
                   .ena(ena),
                   .prn(ddioreg_prn),
                   .q(dffhi_tmp),
                   .devpor(devpor),
                   .devclrn(devclrn)
                );
defparam ddioreg_hi.power_up = power_up;

//DDIO Low Register
dffeas  ddioreg_lo(
                   .d(dffhi_tmp),
                   .clk(!clk),
                   .clrn(ddioreg_aclr),
                   .aload(1'b0),
                   .sclr(ddioreg_sclr),
                   .sload(ddioreg_sload),
                   .asdata(ddioreg_adatasdata),
                   .ena(ena),
                   .prn(ddioreg_prn),
                   .q(dfflo_tmp),
                   .devpor(devpor),
                   .devclrn(devclrn)
                   );
defparam ddioreg_lo.power_up = power_up;

//registered output
stratixv_mux21 or_gate(
                    .MO(dataout),
                    .A(dffhi_tmp),
                    .B(dfflo_tmp),
                    .S(dfflo_tmp)
                   );
assign dfflo = dfflo_tmp;
assign dffhi = dffhi_tmp;

endmodule

////////////////////////////////////////////////////////////////////////////////
//Module Name:                    stratixv_ddio_in                                 
//Description:                    Simulation model for STRATIXV DDIO IN            
//                                                                              
////////////////////////////////////////////////////////////////////////////////
                                                                                
                                                                                
module stratixv_ddio_in (                                                          
                      datain,                                                   
                      clk,                                                      
                      clkn,                                                     
                      ena,                                                      
                      areset,                                                   
                      sreset,                                                   
                      regoutlo,                                                 
                      regouthi,                                                 
                      dfflo,                                                    
                      devpor,                                                   
                      devclrn                                                   
                    );                                                          
                                                                                
//Parameters Declaration                                                        
parameter power_up = "low";                                                     
parameter async_mode = "none";                                                  
parameter sync_mode = "none";                                                   
parameter use_clkn = "false";                                                   
parameter lpm_type = "stratixv_ddio_in";                                           
                                                                                
//Input Ports Declaration                                                       
input datain;                                                                   
input clk;                                                                      
input clkn;                                                                     
input ena;                                                                      
input areset;                                                                   
input sreset;                                                                   
input devpor;                                                                   
input devclrn;                                                                  
                                                                                
//Output Ports Declaration                                                      
output regoutlo;                                                                
output regouthi;                                                                
                                                                                
//burried port;                                                                 
output dfflo;                                                                   
                                                                                
tri1 devclrn;                                                                   
tri1 devpor;                                                                    
                                                                                
//Internal Signals                                                              
reg ddioreg_aclr;                                                               
reg ddioreg_prn;                                                                
reg ddioreg_adatasdata;                                                         
reg ddioreg_sclr;                                                               
reg ddioreg_sload;                                                              
reg viol_notifier;                                                              
                                                                                
wire ddioreg_clk;                                                               
wire dfflo_tmp;                                                                 
wire regout_tmp_hi;                                                             
wire regout_tmp_lo;                                                             
wire dff_ena;                                                                   
                                                                                
initial                                                                         
begin                                                                           
	ddioreg_aclr = 1'b1;                                                        
	ddioreg_prn = 1'b1;                                                         
	ddioreg_adatasdata = 1'b0;                                                  
    ddioreg_sclr = 1'b0;                                                        
    ddioreg_sload = 1'b0;                                                       
end                                                                             
                                                                                
assign ddioreg_clk = (use_clkn == "false") ? !clk : clkn;                       
                                                                                
//Decode the control values for the DDIO registers                              
always@(areset or sreset )                                                      
    begin                                                                       
        if(async_mode == "clear")                                               
            begin                                                               
                ddioreg_aclr = !areset;                                         
                ddioreg_prn = 1'b1;                                             
            end                                                                 
        else if(async_mode == "preset")                                         
            begin                                                               
                ddioreg_aclr = 'b1;                                             
                ddioreg_prn = !areset;                                          
            end                                                                 
         else                                                                   
            begin                                                               
                ddioreg_aclr = 'b1;                                             
                ddioreg_prn = 'b1;                                              
            end                                                                 
                                                                                
         if(sync_mode == "clear")                                               
            begin                                                               
                ddioreg_adatasdata = 'b0;                                       
                ddioreg_sclr = sreset;                                          
                ddioreg_sload = 'b0;                                            
            end                                                                 
        else if(sync_mode == "preset")                                          
            begin                                                               
                ddioreg_adatasdata = 'b1;                                       
                ddioreg_sclr = 'b0;                                             
                ddioreg_sload = sreset;                                         
            end                                                                 
        else                                                                    
            begin                                                               
                ddioreg_adatasdata = 'b0;                                       
                ddioreg_sclr = 'b0;                                             
                ddioreg_sload = 'b0;                                            
            end                                                                 
    end                                                                         
//DDIO high Register                                                            
dffeas  ddioreg_hi(                                                             
                   .d(datain),                                                  
                   .clk(clk),                                                   
                   .clrn(ddioreg_aclr),                                         
                   .aload(1'b0),                                                
                   .sclr(ddioreg_sclr),                                         
                   .sload(ddioreg_sload),                                       
                   .asdata(ddioreg_adatasdata),                                 
                   .ena(ena),                                                   
                   .prn(ddioreg_prn),                                           
                   .q(regout_tmp_hi),                                           
                   .devpor(devpor),                                             
                   .devclrn(devclrn)                                            
                   );                                                           
defparam ddioreg_hi.power_up = power_up;                                        
                                                                                
//DDIO Low Register                                                             
dffeas  ddioreg_lo(                                                             
                   .d(datain),                                                  
                   .clk(ddioreg_clk),                                           
                   .clrn(ddioreg_aclr),                                         
                   .aload(1'b0),                                                
                   .sclr(ddioreg_sclr),                                         
                   .sload(ddioreg_sload),                                       
                   .asdata(ddioreg_adatasdata),                                 
                   .ena(ena),                                                   
                   .prn(ddioreg_prn),                                           
                   .q(dfflo_tmp),                                               
                   .devpor(devpor),                                             
                   .devclrn(devclrn)                                            
                  );                                                            
defparam ddioreg_lo.power_up = power_up;                                        
                                                                                
dffeas  ddioreg_lo1(                                                            
                    .d(dfflo_tmp),                                              
                    .clk(clk),                                                  
                    .clrn(ddioreg_aclr),                                        
                    .aload(1'b0),                                               
                    .sclr(ddioreg_sclr),                                        
                    .sload(ddioreg_sload),                                      
                    .asdata(ddioreg_adatasdata),                                
                    .ena(ena),                                                  
                    .prn(ddioreg_prn),                                          
                    .q(regout_tmp_lo),                                          
                    .devpor(devpor),                                            
                    .devclrn(devclrn)                                           
                   );                                                           
defparam ddioreg_lo1.power_up = power_up;                                       
                                                                                
                                                                                
assign regouthi = regout_tmp_hi;                                                
assign regoutlo = regout_tmp_lo;                                                
assign dfflo = dfflo_tmp;                                                       
                                                                                
endmodule                                                                       
//--------------------------------------------------------------------------
// Module Name     : stratixv_io_pad
// Description     : Simulation model for stratixv IO pad
//--------------------------------------------------------------------------

`timescale 1 ps/1 ps

module stratixv_io_pad ( 
		      padin, 
                      padout
	            );

parameter lpm_type = "stratixv_io_pad";
//INPUT PORTS
input padin; //Input Pad

//OUTPUT PORTS
output padout;//Output Pad

//INTERNAL SIGNALS
wire padin_ipd;
wire padout_opd;

//INPUT BUFFER INSERTION FOR VERILOG-XL
buf padin_buf  (padin_ipd,padin);


assign padout_opd = padin_ipd;

//OUTPUT BUFFER INSERTION FOR VERILOG-XL
buf padout_buf (padout, padout_opd);

endmodule
//////////////////////////////////////////////////////////////////////////////////
//Module Name:                    stratixv_pseudo_diff_out                          //
//Description:                    Simulation model for Stratixv Pseudo Differential //
//                                Output Buffer                                  //
//////////////////////////////////////////////////////////////////////////////////

module stratixv_pseudo_diff_out(
                             i,
                             o,
                             obar,
// ports new for StratixV
				dtcin,
				dtc,
				dtcbar,
				oein,
				oeout,
				oebout
                             );
parameter lpm_type = "stratixv_pseudo_diff_out";

input i;
output o;
output obar;
// ports new for StratixV
   
input dtcin, oein;
output dtc, dtcbar, oeout, oebout;
   

reg o_tmp;
reg obar_tmp;
reg dtc_tmp, dtcbar_tmp, oeout_tmp, oebout_tmp;

assign dtc    = dtcin;
assign dtcbar = dtcin;
assign oeout  = oein;
assign oebout = oein;
   
assign o = o_tmp;
assign obar = obar_tmp;

always@(i)
    begin
        if( i == 1'b1)
            begin
                o_tmp = 1'b1;
                obar_tmp = 1'b0;
            end
        else if( i == 1'b0)
            begin
                o_tmp = 1'b0;
                obar_tmp = 1'b1;
            end
        else
            begin
                o_tmp = i;
                obar_tmp = i;
            end
    end // always@ (i)
   
endmodule

// -----------------------------------------------------------
//
// Module Name : stratixv_bias_logic
//
// Description : STRATIXV Bias Block's Logic Block
//               Verilog simulation model
//
// -----------------------------------------------------------

`timescale 1 ps/1 ps

module stratixv_bias_logic (
    clk,
    shiftnld,
    captnupdt,
    mainclk,
    updateclk,
    capture,
    update
    );

// INPUT PORTS
input  clk;
input  shiftnld;
input  captnupdt;
    
// OUTPUTPUT PORTS
output mainclk;
output updateclk;
output capture;
output update;

// INTERNAL VARIABLES
reg mainclk_tmp;
reg updateclk_tmp;
reg capture_tmp;
reg update_tmp;

initial
begin
    mainclk_tmp <= 'b0;
    updateclk_tmp <= 'b0;
    capture_tmp <= 'b0;
    update_tmp <= 'b0;
end

    always @(captnupdt or shiftnld or clk)
    begin
        case ({captnupdt, shiftnld})
        2'b10, 2'b11 :
            begin
                mainclk_tmp <= 'b0;
                updateclk_tmp <= clk;
                capture_tmp <= 'b1;
                update_tmp <= 'b0;
            end
        2'b01 :
            begin
                mainclk_tmp <= 'b0;
                updateclk_tmp <= clk;
                capture_tmp <= 'b0;
                update_tmp <= 'b0;
            end
        2'b00 :
            begin
                mainclk_tmp <= clk;
                updateclk_tmp <= 'b0;
                capture_tmp <= 'b0;
                update_tmp <= 'b1;
            end
        default :
            begin
                mainclk_tmp <= 'b0;
                updateclk_tmp <= 'b0;
                capture_tmp <= 'b0;
                update_tmp <= 'b0;
            end
        endcase
    end

and (mainclk, mainclk_tmp, 1'b1);
and (updateclk, updateclk_tmp, 1'b1);
and (capture, capture_tmp, 1'b1);
and (update, update_tmp, 1'b1);

endmodule // stratixv_bias_logic

// -----------------------------------------------------------
//
// Module Name : stratixv_bias_generator
//
// Description : STRATIXV Bias Generator Verilog simulation model
//
// -----------------------------------------------------------

`timescale 1 ps/1 ps

module stratixv_bias_generator (
    din,
    mainclk,
    updateclk,
    capture,
    update,
    dout 
    );

// INPUT PORTS
input  din;
input  mainclk;
input  updateclk;
input  capture;
input  update;
    
// OUTPUTPUT PORTS
output dout;
    
parameter TOTAL_REG = 202;

// INTERNAL VARIABLES
reg dout_tmp;
reg generator_reg [TOTAL_REG - 1:0];
reg update_reg [TOTAL_REG - 1:0];
integer i;

initial
begin
    dout_tmp <= 'b0;
    for (i = 0; i < TOTAL_REG; i = i + 1)
    begin
        generator_reg [i] <= 'b0;
        update_reg [i] <= 'b0;
    end
end

// main generator registers
always @(posedge mainclk)
begin
    if ((capture == 'b0) && (update == 'b1)) //update main registers
    begin
        for (i = 0; i < TOTAL_REG; i = i + 1)
        begin
            generator_reg[i] <= update_reg[i];
        end
    end
end

// update registers
always @(posedge updateclk)
begin
    dout_tmp <= update_reg[TOTAL_REG - 1];

    if ((capture == 'b0) && (update == 'b0)) //shift update registers
    begin
        for (i = (TOTAL_REG - 1); i > 0; i = i - 1)
        begin
            update_reg[i] <= update_reg[i - 1];
        end
        update_reg[0] <= din; 
    end
    else if ((capture == 'b1) && (update == 'b0)) //load update registers
    begin
        for (i = 0; i < TOTAL_REG; i = i + 1)
        begin
            update_reg[i] <= generator_reg[i];
        end
    end

end

and (dout, dout_tmp, 1'b1);

endmodule // stratixv_bias_generator

// -----------------------------------------------------------
//
// Module Name : stratixv_bias_block
//
// Description : STRATIXV Bias Block Verilog simulation model
//
// -----------------------------------------------------------

`timescale 1 ps/1 ps

module stratixv_bias_block(
			clk,
			shiftnld,
			captnupdt,
			din,
			dout 
			);

// INPUT PORTS
input  clk;
input  shiftnld;
input  captnupdt;
input  din;
    
// OUTPUTPUT PORTS
output dout;
    
parameter lpm_type = "stratixv_bias_block";
    
// INTERNAL VARIABLES
reg din_viol;
reg shiftnld_viol;
reg captnupdt_viol;

wire mainclk_wire;
wire updateclk_wire;
wire capture_wire;
wire update_wire;
wire dout_tmp;

specify

    $setuphold (posedge clk, din, 0, 0, din_viol) ;
    $setuphold (posedge clk, shiftnld, 0, 0, shiftnld_viol) ;
    $setuphold (posedge clk, captnupdt, 0, 0, captnupdt_viol) ;

    (posedge clk => (dout +: dout_tmp)) = 0 ;

endspecify

stratixv_bias_logic logic_block (
                             .clk(clk),
                             .shiftnld(shiftnld),
                             .captnupdt(captnupdt),
                             .mainclk(mainclk_wire),
                             .updateclk(updateclk_wire),
                             .capture(capture_wire),
                             .update(update_wire)
                             );

stratixv_bias_generator bias_generator (
                                    .din(din),
                                    .mainclk(mainclk_wire),
                                    .updateclk(updateclk_wire),
                                    .capture(capture_wire),
                                    .update(update_wire),
                                    .dout(dout_tmp) 
                                    );

and (dout, dout_tmp, 1'b1);

endmodule // stratixv_bias_block

///////////////////////////////////////////////////////////////////////////////
//  Module Name:             stratixv_mac                                    //
//  Description:             Stratix-V MAC                                   //
///////////////////////////////////////////////////////////////////////////////

`timescale 1 ps/1 ps
module stratixv_mac(
			accumulate,
			aclr,
			ax,
			ay,
			az,
			bx,
			by,
			chainin,
			cin,
			clk,
			coefsela,
			coefselb,
			complex,
			ena,
			loadconst,
			negate,
			scanin,
			sub,

			chainout,
			cout,
			dftout,
			resulta,
			resultb,
			scanout 
		);
//PARAMETERS
parameter lpm_type = "stratixv_mac";

parameter ax_width = 16;
parameter ay_scan_in_width = 16;
parameter az_width = 1;
parameter bx_width = 16;
parameter by_width = 16;
parameter scan_out_width = 1;
parameter result_a_width = 33;
parameter result_b_width = 1;

parameter operation_mode = "m18x18_sumof2";
parameter mode_sub_location = 0;
parameter operand_source_max = "input";
parameter operand_source_may = "input";
parameter operand_source_mbx = "input";
parameter operand_source_mby = "input";
parameter preadder_subtract_a = "false";
parameter preadder_subtract_b = "false";
parameter signed_max = "false";
parameter signed_may = "false";
parameter signed_mbx = "false";
parameter signed_mby = "false";

parameter ay_use_scan_in = "false";
parameter by_use_scan_in = "false";
parameter delay_scan_out_ay = "false";
parameter delay_scan_out_by = "false";
parameter use_chainadder = "false";
parameter [5:0] load_const_value = 6'b0;

parameter signed [26:0] coef_a_0 = 0;
parameter signed [26:0] coef_a_1 = 0;
parameter signed [26:0] coef_a_2 = 0;
parameter signed [26:0] coef_a_3 = 0;
parameter signed [26:0] coef_a_4 = 0;
parameter signed [26:0] coef_a_5 = 0;
parameter signed [26:0] coef_a_6 = 0;
parameter signed [26:0] coef_a_7 = 0;
parameter signed [17:0] coef_b_0 = 0;
parameter signed [17:0] coef_b_1 = 0;
parameter signed [17:0] coef_b_2 = 0;
parameter signed [17:0] coef_b_3 = 0;
parameter signed [17:0] coef_b_4 = 0;
parameter signed [17:0] coef_b_5 = 0;
parameter signed [17:0] coef_b_6 = 0;
parameter signed [17:0] coef_b_7 = 0;

parameter ax_clock = "none";
parameter ay_scan_in_clock = "none";
parameter az_clock = "none";
parameter bx_clock = "none";
parameter by_clock = "none";
parameter coef_sel_a_clock = "none";
parameter coef_sel_b_clock = "none";
parameter sub_clock = "none";
parameter negate_clock = "none";
parameter accumulate_clock = "none";
parameter load_const_clock = "none";
parameter complex_clock = "none";
parameter output_clock = "none";

//INPUT PORTS
input	sub;
input	negate;
input	accumulate;
input	loadconst;
input	complex;
input 	cin;
input	[ax_width-1 : 0]	ax;
input	[ay_scan_in_width-1 : 0]	ay;
input	[ay_scan_in_width-1 : 0]	scanin;
input	[az_width-1 : 0]	az;
input	[bx_width-1 : 0]	bx;
input	[by_width-1 : 0]	by;
input	[2:0] coefsela;
input	[2:0] coefselb;
input	[2:0] clk;
input	[1:0] aclr;
input	[2:0] ena;
input	[63 : 0] chainin;

//OUTPUT PORTS
output	cout;
output	dftout;
output	[result_a_width-1 : 0] resulta;
output	[result_b_width-1 : 0] resultb;
output	[scan_out_width-1 : 0] scanout;
output	[63 : 0] chainout;

stratixv_mac_encrypted	mac_core0
(
	.sub(sub),
	.negate(negate),
	.accumulate(accumulate),
	.loadconst(loadconst),
	.complex(complex),
	.cin(cin),
	.ax(ax),
	.ay(ay),
	.az(az),
	.bx(bx),
	.by(by),
	.scanin(scanin),
	.coefsela(coefsela),
	.coefselb(coefselb),
	.clk(clk),
	.aclr(aclr),
	.ena(ena),
	.chainin(chainin),

	.dftout(dftout),
	.cout(cout), 
	.resulta(resulta),
	.resultb(resultb),
	.scanout(scanout),
	.chainout(chainout)
);
defparam mac_core0.ax_width = ax_width;
defparam mac_core0.ay_scan_in_width = ay_scan_in_width;
defparam mac_core0.az_width = az_width;
defparam mac_core0.bx_width = bx_width;
defparam mac_core0.by_width = by_width;
defparam mac_core0.scan_out_width = scan_out_width;
defparam mac_core0.result_a_width = result_a_width;
defparam mac_core0.result_b_width = result_b_width;

defparam mac_core0.operation_mode = operation_mode;
defparam mac_core0.mode_sub_location = mode_sub_location;
defparam mac_core0.operand_source_max = operand_source_max;
defparam mac_core0.operand_source_may = operand_source_may;
defparam mac_core0.operand_source_mbx = operand_source_mbx;
defparam mac_core0.operand_source_mby = operand_source_mby;
defparam mac_core0.preadder_subtract_a = preadder_subtract_a;
defparam mac_core0.preadder_subtract_b = preadder_subtract_b;
defparam mac_core0.signed_max = signed_max;
defparam mac_core0.signed_may = signed_may;
defparam mac_core0.signed_mbx = signed_mbx;
defparam mac_core0.signed_mby = signed_mby;

defparam mac_core0.ay_use_scan_in = ay_use_scan_in;
defparam mac_core0.by_use_scan_in = by_use_scan_in;
defparam mac_core0.delay_scan_out_ay = delay_scan_out_ay;
defparam mac_core0.delay_scan_out_by = delay_scan_out_by;
defparam mac_core0.use_chainadder = use_chainadder;
defparam mac_core0.load_const_value = load_const_value;

defparam mac_core0.coef_a_0 = coef_a_0;
defparam mac_core0.coef_a_1 = coef_a_1;
defparam mac_core0.coef_a_2 = coef_a_2;
defparam mac_core0.coef_a_3 = coef_a_3;
defparam mac_core0.coef_a_4 = coef_a_4;
defparam mac_core0.coef_a_5 = coef_a_5;
defparam mac_core0.coef_a_6 = coef_a_6;
defparam mac_core0.coef_a_7 = coef_a_7;
defparam mac_core0.coef_b_0 = coef_b_0;
defparam mac_core0.coef_b_1 = coef_b_1;
defparam mac_core0.coef_b_2 = coef_b_2;
defparam mac_core0.coef_b_3 = coef_b_3;
defparam mac_core0.coef_b_4 = coef_b_4;
defparam mac_core0.coef_b_5 = coef_b_5;
defparam mac_core0.coef_b_6 = coef_b_6;
defparam mac_core0.coef_b_7 = coef_b_7;

defparam mac_core0.ax_clock = ax_clock;
defparam mac_core0.ay_scan_in_clock = ay_scan_in_clock;
defparam mac_core0.az_clock = az_clock;
defparam mac_core0.bx_clock = bx_clock;
defparam mac_core0.by_clock = by_clock;
defparam mac_core0.coef_sel_a_clock = coef_sel_a_clock;
defparam mac_core0.coef_sel_b_clock = coef_sel_b_clock;
defparam mac_core0.sub_clock = sub_clock;
defparam mac_core0.negate_clock = negate_clock;
defparam mac_core0.accumulate_clock = accumulate_clock;
defparam mac_core0.load_const_clock = load_const_clock;
defparam mac_core0.complex_clock = complex_clock;
defparam mac_core0.output_clock = output_clock;

endmodule

`timescale 1 ps/1 ps

module    stratixv_clk_phase_select    (
    clkin,
    phasectrlin,
    phaseinvertctrl,
    clkout);

    parameter    use_phasectrlin    =    "true";
    parameter    phase_setting    =    0;
    parameter    invert_phase    =    "false";


    input    [3:0]    clkin;
    input    [1:0]    phasectrlin;
    input    phaseinvertctrl;
    output    clkout;

    stratixv_clk_phase_select_encrypted inst (
        .clkin(clkin),
        .phasectrlin(phasectrlin),
        .phaseinvertctrl(phaseinvertctrl),
        .clkout(clkout) );
    defparam inst.use_phasectrlin = use_phasectrlin;
    defparam inst.phase_setting = phase_setting;
    defparam inst.invert_phase = invert_phase;

endmodule //stratixv_clk_phase_select

`timescale 1 ps/1 ps

module    stratixv_clkena    (
    inclk,
    ena,
    testsyn,
    testswitchloopback,
    enaout,
    outclk);

    parameter    clock_type    =    "auto";
    parameter    ena_register_mode    =    "always_enabled";
    parameter    lpm_type    =    "stratixv_clkena";
    parameter    ena_register_power_up    =    "high";
    parameter    disable_mode    =    "low";


    input    inclk;
    input    ena;
    input    testsyn;
    input    testswitchloopback;
    output    enaout;
    output    outclk;

    stratixv_clkena_encrypted inst (
        .inclk(inclk),
        .ena(ena),
		.testsyn(testsyn),
        .testswitchloopback(testswitchloopback),
        .enaout(enaout),
        .outclk(outclk) );
    defparam inst.clock_type = clock_type;
    defparam inst.ena_register_mode = ena_register_mode;
    defparam inst.lpm_type = lpm_type;
    defparam inst.ena_register_power_up = ena_register_power_up;
    defparam inst.disable_mode = disable_mode;

endmodule //stratixv_clkena

`timescale 1 ps/1 ps

module    stratixv_clkselect    (
    inclk,
    clkselect,
    outclk,
    testcff,
    testswitch,
    testswitchloopbackout);

    parameter    lpm_type    =    "stratixv_clkselect";


    input    [3:0]    inclk;
    input    [1:0]    clkselect;
    input    testcff;
    input    testswitch;
    output   outclk;
    output   testswitchloopbackout;

    stratixv_clkselect_encrypted inst (
        .inclk(inclk),
        .clkselect(clkselect),
        .outclk(outclk),
        .testcff(testcff),
        .testswitch(testswitch),
        .testswitchloopbackout(testswitchloopbackout) );
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_clkselect

`timescale 1 ps/1 ps

module    stratixv_delay_chain    (
    datain,
    delayctrlin,
    dataout);

    parameter    sim_intrinsic_rising_delay    =    200;
    parameter    sim_intrinsic_falling_delay    =    200;
    parameter    sim_rising_delay_increment    =    10;
    parameter    sim_falling_delay_increment    =    10;
    parameter    lpm_type    =    "stratixv_delay_chain";


    input    datain;
    input    [6:0]    delayctrlin;
    output    dataout;

    stratixv_delay_chain_encrypted inst (
        .datain(datain),
        .delayctrlin(delayctrlin),
        .dataout(dataout) );
    defparam inst.sim_intrinsic_rising_delay = sim_intrinsic_rising_delay;
    defparam inst.sim_intrinsic_falling_delay = sim_intrinsic_falling_delay;
    defparam inst.sim_rising_delay_increment = sim_rising_delay_increment;
    defparam inst.sim_falling_delay_increment = sim_falling_delay_increment;
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_delay_chain

`timescale 1 ps/1 ps

module    stratixv_dll_offset_ctrl    (
    clk,
    offsetdelayctrlin,
    offset,
    addnsub,
    aload,
    offsetctrlout,
    offsettestout);

    parameter    use_offset    =    "false";
    parameter    static_offset    =    0;
    parameter    use_pvt_compensation    =    "false";


    input    clk;
    input    [6:0]    offsetdelayctrlin;
    input    [6:0]    offset;
    input    addnsub;
    input    aload;
    output    [6:0]    offsetctrlout;
    output    [6:0]    offsettestout;

    stratixv_dll_offset_ctrl_encrypted inst (
        .clk(clk),
        .offsetdelayctrlin(offsetdelayctrlin),
        .offset(offset),
        .addnsub(addnsub),
        .aload(aload),
        .offsetctrlout(offsetctrlout),
        .offsettestout(offsettestout) );
    defparam inst.use_offset = use_offset;
    defparam inst.static_offset = static_offset;
    defparam inst.use_pvt_compensation = use_pvt_compensation;

endmodule //stratixv_dll_offset_ctrl

`timescale 1 ps/1 ps

module    stratixv_dll    (
    aload,
    clk,
    upndnin,
    upndninclkena,
    delayctrlout,
    dqsupdate,
    offsetdelayctrlout,
    offsetdelayctrlclkout,
    upndnout,
    dffin);

    parameter    input_frequency    =    "0 MHz";
    parameter    delayctrlout_mode    =    "normal";
    parameter    jitter_reduction    =    "false";
    parameter    use_upndnin    =    "false";
    parameter    use_upndninclkena    =    "false";
    parameter    dual_phase_comparators    =    "true";
    parameter    sim_valid_lock    =    16;
    parameter    sim_valid_lockcount    =    0;
    parameter    sim_buffer_intrinsic_delay    =    175;
    parameter    sim_buffer_delay_increment    =    10;
    parameter    static_delay_ctrl    =    0;
    parameter    lpm_type    =    "stratixv_dll";
    parameter    delay_chain_length    =    8;


    input    aload;
    input    clk;
    input    upndnin;
    input    upndninclkena;
    output    [6:0]    delayctrlout;
    output    dqsupdate;
    output    [6:0]    offsetdelayctrlout;
    output    offsetdelayctrlclkout;
    output    upndnout;
    output    dffin;

    stratixv_dll_encrypted inst (
        .aload(aload),
        .clk(clk),
        .upndnin(upndnin),
        .upndninclkena(upndninclkena),
        .delayctrlout(delayctrlout),
        .dqsupdate(dqsupdate),
        .offsetdelayctrlout(offsetdelayctrlout),
        .offsetdelayctrlclkout(offsetdelayctrlclkout),
        .upndnout(upndnout),
        .dffin(dffin) );
    defparam inst.input_frequency = input_frequency;
    defparam inst.delayctrlout_mode = delayctrlout_mode;
    defparam inst.jitter_reduction = jitter_reduction;
    defparam inst.use_upndnin = use_upndnin;
    defparam inst.use_upndninclkena = use_upndninclkena;
    defparam inst.dual_phase_comparators = dual_phase_comparators;
    defparam inst.sim_valid_lock = sim_valid_lock;
    defparam inst.sim_valid_lockcount = sim_valid_lockcount;
    defparam inst.sim_buffer_intrinsic_delay = sim_buffer_intrinsic_delay;
    defparam inst.sim_buffer_delay_increment = sim_buffer_delay_increment;
    defparam inst.static_delay_ctrl = static_delay_ctrl;
    defparam inst.lpm_type = lpm_type;
    defparam inst.delay_chain_length = delay_chain_length;

endmodule //stratixv_dll

`timescale 1 ps/1 ps

module    stratixv_dqs_config    (
    datain,
    clk,
    ena,
    update,
    dqsbusoutdelaysetting,
    dqsinputphasesetting,
    dqsoutputphasesetting,
    dqoutputphasesetting,
    resyncinputphasesetting,
    enaoctcycledelaysetting,
    enainputcycledelaysetting,
    enaoutputcycledelaysetting,
    dqsenabledelaysetting,
    octdelaysetting1,
    octdelaysetting2,
    enadqsenablephasetransferreg,
    enaoctphasetransferreg,
    enaoutputphasetransferreg,
    enainputphasetransferreg,
    resyncinputphaseinvert,
    dqoutputphaseinvert,
    dqsoutputphaseinvert,
    dataout,
    dqsoutputzerophaseinvert,
    dqoutputzerophaseinvert,
    resyncinputzerophaseinvert,
    dqs2xoutputphasesetting,
    dqs2xoutputphaseinvert,
    ck2xoutputphasesetting,
    ck2xoutputphaseinvert,
    dq2xoutputphasesetting,
    dq2xoutputphaseinvert,
    postamblephasesetting,
    postamblephaseinvert,
    postamblezerophaseinvert,
    dividerphaseinvert,
    addrphasesetting,
    addrphaseinvert);

    parameter    lpm_type    =    "stratixv_dqs_config";


    input    datain;
    input    clk;
    input    ena;
    input    update;
    output    [5:0]    dqsbusoutdelaysetting;
    output    [2:0]    dqsinputphasesetting;
    output    [1:0]    dqsoutputphasesetting;
    output    [1:0]    dqoutputphasesetting;
    output    [1:0]    resyncinputphasesetting;
    output    enaoctcycledelaysetting;
    output    enainputcycledelaysetting;
    output    enaoutputcycledelaysetting;
    output    [2:0]    dqsenabledelaysetting;
    output    [3:0]    octdelaysetting1;
    output    [2:0]    octdelaysetting2;
    output    enadqsenablephasetransferreg;
    output    enaoctphasetransferreg;
    output    enaoutputphasetransferreg;
    output    enainputphasetransferreg;
    output    resyncinputphaseinvert;
    output    dqoutputphaseinvert;
    output    dqsoutputphaseinvert;
    output    dataout;
    output    dqsoutputzerophaseinvert;
    output    dqoutputzerophaseinvert;
    output    resyncinputzerophaseinvert;
    output    [1:0]    dqs2xoutputphasesetting;
    output    dqs2xoutputphaseinvert;
    output    [1:0]    ck2xoutputphasesetting;
    output    ck2xoutputphaseinvert;
    output    [1:0]    dq2xoutputphasesetting;
    output    dq2xoutputphaseinvert;
    output    [1:0]    postamblephasesetting;
    output    postamblephaseinvert;
    output    postamblezerophaseinvert;
    output    dividerphaseinvert;
    output    [1:0]    addrphasesetting;
    output    addrphaseinvert;

    stratixv_dqs_config_encrypted inst (
        .datain(datain),
        .clk(clk),
        .ena(ena),
        .update(update),
        .dqsbusoutdelaysetting(dqsbusoutdelaysetting),
        .dqsinputphasesetting(dqsinputphasesetting),
        .dqsoutputphasesetting(dqsoutputphasesetting),
        .dqoutputphasesetting(dqoutputphasesetting),
        .resyncinputphasesetting(resyncinputphasesetting),
        .enaoctcycledelaysetting(enaoctcycledelaysetting),
        .enainputcycledelaysetting(enainputcycledelaysetting),
        .enaoutputcycledelaysetting(enaoutputcycledelaysetting),
        .dqsenabledelaysetting(dqsenabledelaysetting),
        .octdelaysetting1(octdelaysetting1),
        .octdelaysetting2(octdelaysetting2),
        .enadqsenablephasetransferreg(enadqsenablephasetransferreg),
        .enaoctphasetransferreg(enaoctphasetransferreg),
        .enaoutputphasetransferreg(enaoutputphasetransferreg),
        .enainputphasetransferreg(enainputphasetransferreg),
        .resyncinputphaseinvert(resyncinputphaseinvert),
        .dqoutputphaseinvert(dqoutputphaseinvert),
        .dqsoutputphaseinvert(dqsoutputphaseinvert),
        .dataout(dataout),
        .dqsoutputzerophaseinvert(dqsoutputzerophaseinvert),
        .dqoutputzerophaseinvert(dqoutputzerophaseinvert),
        .resyncinputzerophaseinvert(resyncinputzerophaseinvert),
        .dqs2xoutputphasesetting(dqs2xoutputphasesetting),
        .dqs2xoutputphaseinvert(dqs2xoutputphaseinvert),
        .ck2xoutputphasesetting(ck2xoutputphasesetting),
        .ck2xoutputphaseinvert(ck2xoutputphaseinvert),
        .dq2xoutputphasesetting(dq2xoutputphasesetting),
        .dq2xoutputphaseinvert(dq2xoutputphaseinvert),
        .postamblephasesetting(postamblephasesetting),
        .postamblephaseinvert(postamblephaseinvert),
        .postamblezerophaseinvert(postamblezerophaseinvert),
        .dividerphaseinvert(dividerphaseinvert),
        .addrphasesetting(addrphasesetting),
        .addrphaseinvert(addrphaseinvert) );
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_dqs_config

`timescale 1 ps/1 ps

module    stratixv_dqs_delay_chain    (
    dqsin,
    dqsenable,
    delayctrlin,
    offsetctrlin,
    dqsupdateen,
    phasectrlin,
    testin,
    dffin,
    dqsbusout);

    parameter    dqs_input_frequency    =    "unused";
    parameter    dqs_phase_shift    =    0;
    parameter    use_phasectrlin    =    "false";
    parameter    phase_setting    =    0;
    parameter    dqs_offsetctrl_enable    =    "false";
    parameter    dqs_ctrl_latches_enable    =    "false";
    parameter    use_alternate_input_for_first_stage_delayctrl    =    "false";
    parameter    use_alternate_input_for_multi_stage_delayctrl    =    "false";
    parameter    sim_buffer_intrinsic_delay    =    175;
    parameter    sim_buffer_delay_increment    =    10;
    parameter    test_enable    =    "false";


    input    dqsin;
    input    dqsenable;
    input    [6:0]    delayctrlin;
    input    [6:0]    offsetctrlin;
    input    dqsupdateen;
    input    [2:0]    phasectrlin;
    input    testin;
    output    dffin;
    output    dqsbusout;

    stratixv_dqs_delay_chain_encrypted inst (
        .dqsin(dqsin),
        .dqsenable(dqsenable),
        .delayctrlin(delayctrlin),
        .offsetctrlin(offsetctrlin),
        .dqsupdateen(dqsupdateen),
        .phasectrlin(phasectrlin),
        .testin(testin),
        .dffin(dffin),
        .dqsbusout(dqsbusout) );
    defparam inst.dqs_input_frequency = dqs_input_frequency;
    defparam inst.dqs_phase_shift = dqs_phase_shift;
    defparam inst.use_phasectrlin = use_phasectrlin;
    defparam inst.phase_setting = phase_setting;
    defparam inst.dqs_offsetctrl_enable = dqs_offsetctrl_enable;
    defparam inst.dqs_ctrl_latches_enable = dqs_ctrl_latches_enable;
    defparam inst.use_alternate_input_for_first_stage_delayctrl = use_alternate_input_for_first_stage_delayctrl;
    defparam inst.use_alternate_input_for_multi_stage_delayctrl = use_alternate_input_for_multi_stage_delayctrl;
    defparam inst.sim_buffer_intrinsic_delay = sim_buffer_intrinsic_delay;
    defparam inst.sim_buffer_delay_increment = sim_buffer_delay_increment;
    defparam inst.test_enable = test_enable;

endmodule //stratixv_dqs_delay_chain

`timescale 1 ps/1 ps

module    stratixv_dqs_enable_ctrl    (
    dqsenablein,
    zerophaseclk,
    enaphasetransferreg,
    levelingclk,
    dffin,
    dffphasetransfer,
    dffextenddqsenable,
    dqsenableout);

    parameter    delay_dqs_enable_by_half_cycle    =    "false";
    parameter    add_phase_transfer_reg    =    "false";


    input    dqsenablein;
    input    zerophaseclk;
    input    enaphasetransferreg;
    input    levelingclk;
    output    dffin;
    output    dffphasetransfer;
    output    dffextenddqsenable;
    output    dqsenableout;

    stratixv_dqs_enable_ctrl_encrypted inst (
        .dqsenablein(dqsenablein),
        .zerophaseclk(zerophaseclk),
        .enaphasetransferreg(enaphasetransferreg),
        .levelingclk(levelingclk),
        .dffin(dffin),
        .dffphasetransfer(dffphasetransfer),
        .dffextenddqsenable(dffextenddqsenable),
        .dqsenableout(dqsenableout) );
    defparam inst.delay_dqs_enable_by_half_cycle = delay_dqs_enable_by_half_cycle;
    defparam inst.add_phase_transfer_reg = add_phase_transfer_reg;

endmodule //stratixv_dqs_enable_ctrl

`timescale 1 ps/1 ps

module    stratixv_duty_cycle_adjustment    (
    clkin,
    delaymode,
    delayctrlin,
    clkout);

    parameter    duty_cycle_delay_mode    =    "none";
    parameter    lpm_type    =    "stratixv_duty_cycle_adjustment";


    input    clkin;
    input    delaymode;
    input    [3:0]    delayctrlin;
    output    clkout;

    stratixv_duty_cycle_adjustment_encrypted inst (
        .clkin(clkin),
        .delaymode(delaymode),
        .delayctrlin(delayctrlin),
        .clkout(clkout) );
    defparam inst.duty_cycle_delay_mode = duty_cycle_delay_mode;
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_duty_cycle_adjustment

`timescale 1 ps/1 ps

module    stratixv_fractional_pll    (
    analogtest,
    cntnen,
    coreclkfb,
    crcm,
    crcp,
    crdltasgma,
    crdsmen,
    crfbclkdly,
    crfbclksel,
    crlckf,
    crlcktest,
    crlfc,
    crlfr,
    crlfrd,
    crlock,
    crmdirectfb,
    crmhi,
    crmlo,
    crmmddiv,
    crmprst,
    crmrdly,
    crmsel,
    crnhi,
    crnlckf,
    crnlo,
    crnrdly,
    crpcnt,
    crpfdpulsewidth,
    crrefclkdly,
    crrefclksel,
    crscnt,
    crselfrst,
    crtclk,
    crtest,
    crvcop,
    crvcophbyps,
    crvr,
    enpfd,
    lfreset,
    lvdsfbin,
    niotricntr,
    pdbvr,
    pfden,
    pllpd,
    refclkin,
    reset0,
    roc,
    shift,
    shiftdonein,
    shiften,
    up,
    vcopen,
    zdbinput,
    fbclk,
    fblvdsout,
    lock,
    mcntout,
    selfrst,
    shiftdoneout,
    tclk,
    vcoover,
    vcoph,
    vcounder);

    parameter    lpm_type    =    "stratixv_fractional_pll";
    parameter    output_clock_frequency    =    "0 ps";
    parameter    pll_chg_pump_crnt    =    10;
    parameter    pll_clkin_cmp_path    =    "nrm";
    parameter    pll_cmp_buf_dly    =    "0 ps";
    parameter    pll_dnm_phsf_cnt_sel    =    "all_c";
    parameter    pll_dsm_k    =    1;
    parameter    pll_dsm_out_sel    =    "cram";
    parameter    pll_enable    =    "true";
    parameter    pll_fbclk_cmp_path    =    "nrm";
    parameter    pll_fbclk_mux_1    =    "glb";
    parameter    pll_fbclk_mux_2    =    "fb_1";
    parameter    pll_lock_fltr_cfg    =    0;
    parameter    pll_lock_fltr_test    =    "false";
    parameter    pll_lock_win    =    "nrm";
    parameter    pll_lp_fltr_cs    =    0;
    parameter    pll_lp_fltr_rp    =    20;
    parameter    pll_m_cnt_bypass_en    =    "false";
    parameter    pll_m_cnt_coarse_dly    =    "0 ps";
    parameter    pll_m_cnt_fine_dly    =    "0 ps";
    parameter    pll_m_cnt_hi_div    =    0;
    parameter    pll_m_cnt_in_src    =    "ph_mux_clk";
    parameter    pll_m_cnt_lo_div    =    0;
    parameter    pll_m_cnt_odd_div_duty_en    =    "false";
    parameter    pll_m_cnt_ph_mux_prst    =    0;
    parameter    pll_m_cnt_prst    =    0;
    parameter    pll_mmd_div_sel    =    2;
    parameter    pll_n_cnt_bypass_en    =    "false";
    parameter    pll_n_cnt_coarse_dly    =    "0 ps";
    parameter    pll_n_cnt_fine_dly    =    "0 ps";
    parameter    pll_n_cnt_hi_div    =    1;
    parameter    pll_n_cnt_lo_div    =    1;
    parameter    pll_n_cnt_odd_div_duty_en    =    "false";
    parameter    pll_p_cnt_set    =    1;
    parameter    pll_pfd_pulse_width_min    =    "0 ps";
    parameter    pll_ref_vco_over    =    1300;
    parameter    pll_ref_vco_under    =    500;
    parameter    pll_s_cnt_set    =    1;
    parameter    pll_slf_rst    =    "true";
    parameter    pll_tclk_mux_en    =    "false";
    parameter    pll_unlock_fltr_cfg    =    0;
    parameter    pll_vco_div    =    600;
    parameter    pll_vco_ph0_en    =    "false";
    parameter    pll_vco_ph1_en    =    "false";
    parameter    pll_vco_ph2_en    =    "false";
    parameter    pll_vco_ph3_en    =    "false";
    parameter    pll_vco_ph4_en    =    "false";
    parameter    pll_vco_ph5_en    =    "false";
    parameter    pll_vco_ph6_en    =    "false";
    parameter    pll_vco_ph7_en    =    "false";
    parameter    pll_vco_rng_dt    =    "dis_en";
    parameter    pll_vt_bp_reg_div    =    1700;
    parameter    pll_vt_out    =    1650;
    parameter    pll_vt_rg_mode    =    "nrm_mode";
    parameter    pll_vt_test    =    "false";
    parameter    reference_clock_frequency    =    "0 ps";


    input    analogtest;
    input    cntnen;
    input    coreclkfb;
    input    [1:0]    crcm;
    input    [2:0]    crcp;
    input    [23:0]    crdltasgma;
    input    crdsmen;
    input    [2:0]    crfbclkdly;
    input    [1:0]    crfbclksel;
    input    [11:0]    crlckf;
    input    crlcktest;
    input    [1:0]    crlfc;
    input    [4:0]    crlfr;
    input    [5:0]    crlfrd;
    input    [3:0]    crlock;
    input    crmdirectfb;
    input    [8:0]    crmhi;
    input    [8:0]    crmlo;
    input    [1:0]    crmmddiv;
    input    [10:0]    crmprst;
    input    [4:0]    crmrdly;
    input    [1:0]    crmsel;
    input    [8:0]    crnhi;
    input    [2:0]    crnlckf;
    input    [8:0]    crnlo;
    input    [4:0]    crnrdly;
    input    [3:0]    crpcnt;
    input    crpfdpulsewidth;
    input    [2:0]    crrefclkdly;
    input    [1:0]    crrefclksel;
    input    [3:0]    crscnt;
    input    [1:0]    crselfrst;
    input    [1:0]    crtclk;
    input    [1:0]    crtest;
    input    [7:0]    crvcop;
    input    crvcophbyps;
    input    [6:0]    crvr;
    input    enpfd;
    input    lfreset;
    input    lvdsfbin;
    input    niotricntr;
    input    pdbvr;
    input    pfden;
    input    pllpd;
    input    refclkin;
    input    reset0;
    input    roc;
    input    shift;
    input    shiftdonein;
    input    shiften;
    input    up;
    input    vcopen;
    input    zdbinput;
    output    fbclk;
    output    fblvdsout;
    output    lock;
    output    mcntout;
    output    selfrst;
    output    shiftdoneout;
    output    tclk;
    output    vcoover;
    output    [7:0]    vcoph;
    output    vcounder;

    stratixv_fractional_pll_encrypted inst (
        .analogtest(analogtest),
        .cntnen(cntnen),
        .coreclkfb(coreclkfb),
        .crcm(crcm),
        .crcp(crcp),
        .crdltasgma(crdltasgma),
        .crdsmen(crdsmen),
        .crfbclkdly(crfbclkdly),
        .crfbclksel(crfbclksel),
        .crlckf(crlckf),
        .crlcktest(crlcktest),
        .crlfc(crlfc),
        .crlfr(crlfr),
        .crlfrd(crlfrd),
        .crlock(crlock),
        .crmdirectfb(crmdirectfb),
        .crmhi(crmhi),
        .crmlo(crmlo),
        .crmmddiv(crmmddiv),
        .crmprst(crmprst),
        .crmrdly(crmrdly),
        .crmsel(crmsel),
        .crnhi(crnhi),
        .crnlckf(crnlckf),
        .crnlo(crnlo),
        .crnrdly(crnrdly),
        .crpcnt(crpcnt),
        .crpfdpulsewidth(crpfdpulsewidth),
        .crrefclkdly(crrefclkdly),
        .crrefclksel(crrefclksel),
        .crscnt(crscnt),
        .crselfrst(crselfrst),
        .crtclk(crtclk),
        .crtest(crtest),
        .crvcop(crvcop),
        .crvcophbyps(crvcophbyps),
        .crvr(crvr),
        .enpfd(enpfd),
        .lfreset(lfreset),
        .lvdsfbin(lvdsfbin),
        .niotricntr(niotricntr),
        .pdbvr(pdbvr),
        .pfden(pfden),
        .pllpd(pllpd),
        .refclkin(refclkin),
        .reset0(reset0),
        .roc(roc),
        .shift(shift),
        .shiftdonein(shiftdonein),
        .shiften(shiften),
        .up(up),
        .vcopen(vcopen),
        .zdbinput(zdbinput),
        .fbclk(fbclk),
        .fblvdsout(fblvdsout),
        .lock(lock),
        .mcntout(mcntout),
        .selfrst(selfrst),
        .shiftdoneout(shiftdoneout),
        .tclk(tclk),
        .vcoover(vcoover),
        .vcoph(vcoph),
        .vcounder(vcounder) );
    defparam inst.lpm_type = lpm_type;
    defparam inst.output_clock_frequency = output_clock_frequency;
    defparam inst.pll_chg_pump_crnt = pll_chg_pump_crnt;
    defparam inst.pll_clkin_cmp_path = pll_clkin_cmp_path;
    defparam inst.pll_cmp_buf_dly = pll_cmp_buf_dly;
    defparam inst.pll_dnm_phsf_cnt_sel = pll_dnm_phsf_cnt_sel;
    defparam inst.pll_dsm_k = pll_dsm_k;
    defparam inst.pll_dsm_out_sel = pll_dsm_out_sel;
    defparam inst.pll_enable = pll_enable;
    defparam inst.pll_fbclk_cmp_path = pll_fbclk_cmp_path;
    defparam inst.pll_fbclk_mux_1 = pll_fbclk_mux_1;
    defparam inst.pll_fbclk_mux_2 = pll_fbclk_mux_2;
    defparam inst.pll_lock_fltr_cfg = pll_lock_fltr_cfg;
    defparam inst.pll_lock_fltr_test = pll_lock_fltr_test;
    defparam inst.pll_lock_win = pll_lock_win;
    defparam inst.pll_lp_fltr_cs = pll_lp_fltr_cs;
    defparam inst.pll_lp_fltr_rp = pll_lp_fltr_rp;
    defparam inst.pll_m_cnt_bypass_en = pll_m_cnt_bypass_en;
    defparam inst.pll_m_cnt_coarse_dly = pll_m_cnt_coarse_dly;
    defparam inst.pll_m_cnt_fine_dly = pll_m_cnt_fine_dly;
    defparam inst.pll_m_cnt_hi_div = pll_m_cnt_hi_div;
    defparam inst.pll_m_cnt_in_src = pll_m_cnt_in_src;
    defparam inst.pll_m_cnt_lo_div = pll_m_cnt_lo_div;
    defparam inst.pll_m_cnt_odd_div_duty_en = pll_m_cnt_odd_div_duty_en;
    defparam inst.pll_m_cnt_ph_mux_prst = pll_m_cnt_ph_mux_prst;
    defparam inst.pll_m_cnt_prst = pll_m_cnt_prst;
    defparam inst.pll_mmd_div_sel = pll_mmd_div_sel;
    defparam inst.pll_n_cnt_bypass_en = pll_n_cnt_bypass_en;
    defparam inst.pll_n_cnt_coarse_dly = pll_n_cnt_coarse_dly;
    defparam inst.pll_n_cnt_fine_dly = pll_n_cnt_fine_dly;
    defparam inst.pll_n_cnt_hi_div = pll_n_cnt_hi_div;
    defparam inst.pll_n_cnt_lo_div = pll_n_cnt_lo_div;
    defparam inst.pll_n_cnt_odd_div_duty_en = pll_n_cnt_odd_div_duty_en;
    defparam inst.pll_p_cnt_set = pll_p_cnt_set;
    defparam inst.pll_pfd_pulse_width_min = pll_pfd_pulse_width_min;
    defparam inst.pll_ref_vco_over = pll_ref_vco_over;
    defparam inst.pll_ref_vco_under = pll_ref_vco_under;
    defparam inst.pll_s_cnt_set = pll_s_cnt_set;
    defparam inst.pll_slf_rst = pll_slf_rst;
    defparam inst.pll_tclk_mux_en = pll_tclk_mux_en;
    defparam inst.pll_unlock_fltr_cfg = pll_unlock_fltr_cfg;
    defparam inst.pll_vco_div = pll_vco_div;
    defparam inst.pll_vco_ph0_en = pll_vco_ph0_en;
    defparam inst.pll_vco_ph1_en = pll_vco_ph1_en;
    defparam inst.pll_vco_ph2_en = pll_vco_ph2_en;
    defparam inst.pll_vco_ph3_en = pll_vco_ph3_en;
    defparam inst.pll_vco_ph4_en = pll_vco_ph4_en;
    defparam inst.pll_vco_ph5_en = pll_vco_ph5_en;
    defparam inst.pll_vco_ph6_en = pll_vco_ph6_en;
    defparam inst.pll_vco_ph7_en = pll_vco_ph7_en;
    defparam inst.pll_vco_rng_dt = pll_vco_rng_dt;
    defparam inst.pll_vt_bp_reg_div = pll_vt_bp_reg_div;
    defparam inst.pll_vt_out = pll_vt_out;
    defparam inst.pll_vt_rg_mode = pll_vt_rg_mode;
    defparam inst.pll_vt_test = pll_vt_test;
    defparam inst.reference_clock_frequency = reference_clock_frequency;

endmodule //stratixv_fractional_pll

`timescale 1 ps/1 ps

module    stratixv_half_rate_input    (
    datain,
    directin,
    clk,
    areset,
    dataoutbypass,
    dataout,
    dffin);

    parameter    power_up    =    "low";
    parameter    async_mode    =    "no_reset";
    parameter    use_dataoutbypass    =    "false";


    input    [1:0]    datain;
    input    directin;
    input    clk;
    input    areset;
    input    dataoutbypass;
    output    [3:0]    dataout;
    output    [1:0]    dffin;

    stratixv_half_rate_input_encrypted inst (
        .datain(datain),
        .directin(directin),
        .clk(clk),
        .areset(areset),
        .dataoutbypass(dataoutbypass),
        .dataout(dataout),
        .dffin(dffin) );
    defparam inst.power_up = power_up;
    defparam inst.async_mode = async_mode;
    defparam inst.use_dataoutbypass = use_dataoutbypass;

endmodule //stratixv_half_rate_input

`timescale 1 ps/1 ps

module    stratixv_input_phase_alignment    (
    datain,
    levelingclk,
    zerophaseclk,
    areset,
    enainputcycledelay,
    enaphasetransferreg,
    dataout,
    dffin,
    dff1t,
    dffphasetransfer);

    parameter    power_up    =    "low";
    parameter    async_mode    =    "no_reset";
    parameter    add_input_cycle_delay    =    "false";
    parameter    bypass_output_register    =    "false";
    parameter    add_phase_transfer_reg    =    "false";
    parameter    lpm_type    =    "stratixv_input_phase_alignment";


    input    datain;
    input    levelingclk;
    input    zerophaseclk;
    input    areset;
    input    enainputcycledelay;
    input    enaphasetransferreg;
    output    dataout;
    output    dffin;
    output    dff1t;
    output    dffphasetransfer;

    stratixv_input_phase_alignment_encrypted inst (
        .datain(datain),
        .levelingclk(levelingclk),
        .zerophaseclk(zerophaseclk),
        .areset(areset),
        .enainputcycledelay(enainputcycledelay),
        .enaphasetransferreg(enaphasetransferreg),
        .dataout(dataout),
        .dffin(dffin),
        .dff1t(dff1t),
        .dffphasetransfer(dffphasetransfer) );
    defparam inst.power_up = power_up;
    defparam inst.async_mode = async_mode;
    defparam inst.add_input_cycle_delay = add_input_cycle_delay;
    defparam inst.bypass_output_register = bypass_output_register;
    defparam inst.add_phase_transfer_reg = add_phase_transfer_reg;
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_input_phase_alignment

`timescale 1 ps/1 ps

module    stratixv_io_clock_divider    (
    clk,
    phaseinvertctrl,
    masterin,
    clkout,
    slaveout);

    parameter    power_up    =    "low";
    parameter    invert_phase    =    "false";
    parameter    use_masterin    =    "false";
    parameter    lpm_type    =    "stratixv_io_clock_divider";


    input    clk;
    input    phaseinvertctrl;
    input    masterin;
    output    clkout;
    output    slaveout;

    stratixv_io_clock_divider_encrypted inst (
        .clk(clk),
        .phaseinvertctrl(phaseinvertctrl),
        .masterin(masterin),
        .clkout(clkout),
        .slaveout(slaveout) );
    defparam inst.power_up = power_up;
    defparam inst.invert_phase = invert_phase;
    defparam inst.use_masterin = use_masterin;
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_io_clock_divider

`timescale 1 ps/1 ps

module    stratixv_io_config    (
    datain,
    clk,
    ena,
    update,
    outputdelaysetting1,
    outputdelaysetting2,
    padtoinputregisterdelaysetting,
    padtoinputregisterrisefalldelaysetting,
    inputclkdelaysetting,
    inputclkndelaysetting,
    dutycycledelaymode,
    dutycycledelaysetting,
    dataout);

    parameter    lpm_type    =    "stratixv_io_config";


    input    datain;
    input    clk;
    input    ena;
    input    update;
    output    [5:0] outputdelaysetting1;
    output    [4:0] outputdelaysetting2;
    output    [5:0] padtoinputregisterdelaysetting;
    output    [5:0] padtoinputregisterrisefalldelaysetting;
    output    [1:0] inputclkdelaysetting;
    output    [1:0] inputclkndelaysetting;
    output    dutycycledelaymode;
    output    [4:0] dutycycledelaysetting;
    output    dataout;

    stratixv_io_config_encrypted inst (
        .datain(datain),
        .clk(clk),
        .ena(ena),
        .update(update),
        .outputdelaysetting1(outputdelaysetting1),
        .outputdelaysetting2(outputdelaysetting2),
        .padtoinputregisterdelaysetting(padtoinputregisterdelaysetting),
        .padtoinputregisterrisefalldelaysetting(padtoinputregisterrisefalldelaysetting),
        .inputclkdelaysetting(inputclkdelaysetting),
        .inputclkndelaysetting(inputclkndelaysetting),
        .dutycycledelaymode(dutycycledelaymode),
        .dutycycledelaysetting(dutycycledelaysetting),
        .dataout(dataout) );
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_io_config

`timescale 1 ps/1 ps

module    stratixv_leveling_delay_chain    (
    clkin,
    delayctrlin,
    clkout);

    parameter    physical_clock_source    =    "dqs";
    parameter    sim_buffer_intrinsic_delay    =    175;
    parameter    sim_buffer_delay_increment    =    10;


    input    clkin;
    input    [6:0]    delayctrlin;
    output    [3:0]    clkout;

    stratixv_leveling_delay_chain_encrypted inst (
        .clkin(clkin),
        .delayctrlin(delayctrlin),
        .clkout(clkout) );
    defparam inst.physical_clock_source = physical_clock_source;
    defparam inst.sim_buffer_intrinsic_delay = sim_buffer_intrinsic_delay;
    defparam inst.sim_buffer_delay_increment = sim_buffer_delay_increment;

endmodule //stratixv_leveling_delay_chain

`timescale 1 ps/1 ps

module    stratixv_lvds_rx    (
    clock0,
    datain,
    enable0,
    dpareset,
    dpahold,
    dpaswitch,
    fiforeset,
    bitslip,
    bitslipreset,
    serialfbk,
    devclrn,
    devpor,
    dpaclkin,
    dataout,
    dpalock,
    bitslipmax,
    serialdataout,
    postdpaserialdataout,
    divfwdclk,
    dpaclkout,
    observableout);

    parameter    data_align_rollover    =    2;
    parameter    enable_dpa    =    "off";
    parameter    lose_lock_on_one_change    =    "off";
    parameter    reset_fifo_at_first_lock    =    "on";
    parameter    align_to_rising_edge_only    =    "on";
    parameter    use_serial_feedback_input    =    "off";
    parameter    dpa_debug    =    "off";
    parameter    x_on_bitslip    =    "on";
    parameter    enable_soft_cdr    =    "off";
    parameter    dpa_output_clock_phase_shift    =    0;
    parameter    enable_dpa_initial_phase_selection    =    "off";
    parameter    dpa_initial_phase_value    =    0;
    parameter    enable_dpa_align_to_rising_edge_only    =    "off";
    parameter    net_ppm_variation    =    0;
    parameter    is_negative_ppm_drift    =    "off";
    parameter    rx_input_path_delay_engineering_bits    =    2;
    parameter    lpm_type    =    "stratixv_lvds_rx";
    parameter    data_width    =    10;


    input    clock0;
    input    datain;
    input    enable0;
    input    dpareset;
    input    dpahold;
    input    dpaswitch;
    input    fiforeset;
    input    bitslip;
    input    bitslipreset;
    input    serialfbk;
    input    devclrn;
    input    devpor;
    input    [7:0]    dpaclkin;
    output    [data_width-1:0]    dataout;
    output    dpalock;
    output    bitslipmax;
    output    serialdataout;
    output    postdpaserialdataout;
    output    divfwdclk;
    output    dpaclkout;
    output    [3:0]    observableout;

    stratixv_lvds_rx_encrypted inst (
        .clock0(clock0),
        .datain(datain),
        .enable0(enable0),
        .dpareset(dpareset),
        .dpahold(dpahold),
        .dpaswitch(dpaswitch),
        .fiforeset(fiforeset),
        .bitslip(bitslip),
        .bitslipreset(bitslipreset),
        .serialfbk(serialfbk),
        .devclrn(devclrn),
        .devpor(devpor),
        .dpaclkin(dpaclkin),
        .dataout(dataout),
        .dpalock(dpalock),
        .bitslipmax(bitslipmax),
        .serialdataout(serialdataout),
        .postdpaserialdataout(postdpaserialdataout),
        .divfwdclk(divfwdclk),
        .dpaclkout(dpaclkout),
        .observableout(observableout) );
    defparam inst.data_align_rollover = data_align_rollover;
    defparam inst.enable_dpa = enable_dpa;
    defparam inst.lose_lock_on_one_change = lose_lock_on_one_change;
    defparam inst.reset_fifo_at_first_lock = reset_fifo_at_first_lock;
    defparam inst.align_to_rising_edge_only = align_to_rising_edge_only;
    defparam inst.use_serial_feedback_input = use_serial_feedback_input;
    defparam inst.dpa_debug = dpa_debug;
    defparam inst.x_on_bitslip = x_on_bitslip;
    defparam inst.enable_soft_cdr = enable_soft_cdr;
    defparam inst.dpa_output_clock_phase_shift = dpa_output_clock_phase_shift;
    defparam inst.enable_dpa_initial_phase_selection = enable_dpa_initial_phase_selection;
    defparam inst.dpa_initial_phase_value = dpa_initial_phase_value;
    defparam inst.enable_dpa_align_to_rising_edge_only = enable_dpa_align_to_rising_edge_only;
    defparam inst.net_ppm_variation = net_ppm_variation;
    defparam inst.is_negative_ppm_drift = is_negative_ppm_drift;
    defparam inst.rx_input_path_delay_engineering_bits = rx_input_path_delay_engineering_bits;
    defparam inst.lpm_type = lpm_type;
    defparam inst.data_width = data_width;

endmodule //stratixv_lvds_rx

`timescale 1 ps/1 ps

module    stratixv_lvds_tx    (
    datain,
    clock0,
    enable0,
    serialdatain,
    postdpaserialdatain,
    devclrn,
    devpor,
    dpaclkin,
    dataout,
    serialfdbkout,
    observableout);

    parameter    bypass_serializer    =    "false";
    parameter    invert_clock    =    "false";
    parameter    use_falling_clock_edge    =    "false";
    parameter    use_serial_data_input    =    "false";
    parameter    use_post_dpa_serial_data_input    =    "false";
    parameter    is_used_as_outclk    =    "false";
    parameter    tx_output_path_delay_engineering_bits    =    -1;
    parameter    enable_dpaclk_to_lvdsout    =    "off";
    parameter    lpm_type    =    "stratixv_lvds_tx";
    parameter    data_width    =    10;


    input    [data_width-1:0]    datain;
    input    clock0;
    input    enable0;
    input    serialdatain;
    input    postdpaserialdatain;
    input    devclrn;
    input    devpor;
    input    dpaclkin;
    output    dataout;
    output    serialfdbkout;
    output    [2:0]    observableout;

    stratixv_lvds_tx_encrypted inst (
        .datain(datain),
        .clock0(clock0),
        .enable0(enable0),
        .serialdatain(serialdatain),
        .postdpaserialdatain(postdpaserialdatain),
        .devclrn(devclrn),
        .devpor(devpor),
        .dpaclkin(dpaclkin),
        .dataout(dataout),
        .serialfdbkout(serialfdbkout),
        .observableout(observableout) );
    defparam inst.bypass_serializer = bypass_serializer;
    defparam inst.invert_clock = invert_clock;
    defparam inst.use_falling_clock_edge = use_falling_clock_edge;
    defparam inst.use_serial_data_input = use_serial_data_input;
    defparam inst.use_post_dpa_serial_data_input = use_post_dpa_serial_data_input;
    defparam inst.is_used_as_outclk = is_used_as_outclk;
    defparam inst.tx_output_path_delay_engineering_bits = tx_output_path_delay_engineering_bits;
    defparam inst.enable_dpaclk_to_lvdsout = enable_dpaclk_to_lvdsout;
    defparam inst.lpm_type = lpm_type;
    defparam inst.data_width = data_width;

endmodule //stratixv_lvds_tx

`timescale 1 ps/1 ps

module    stratixv_output_alignment    (
    datain,
    clk,
    areset,
    sreset,
    enaoutputcycledelay,
    ena2ndoutputcycledelay,
    enaphasetransferreg,
    dataout,
    dffin,
    dff1t,
    dff2t,
    dffphasetransfer);

    parameter    power_up    =    "low";
    parameter    async_mode    =    "none";
    parameter    sync_mode    =    "none";
    parameter    add_output_cycle_delay    =    "false";
    parameter    add_2nd_output_cycle_delay    =    "false";
    parameter    add_phase_transfer_reg    =    "false";
    parameter    bypass_output_register    =    "false";


    input    datain;
    input    clk;
    input    areset;
    input    sreset;
    input    enaoutputcycledelay;
    input    ena2ndoutputcycledelay;
    input    enaphasetransferreg;
    output    dataout;
    output    dffin;
    output    dff1t;
    output    dff2t;
    output    dffphasetransfer;

    stratixv_output_alignment_encrypted inst (
        .datain(datain),
        .clk(clk),
        .areset(areset),
        .sreset(sreset),
        .enaoutputcycledelay(enaoutputcycledelay),
        .ena2ndoutputcycledelay(ena2ndoutputcycledelay),
        .enaphasetransferreg(enaphasetransferreg),
        .dataout(dataout),
        .dffin(dffin),
        .dff1t(dff1t),
        .dff2t(dff2t),
        .dffphasetransfer(dffphasetransfer) );
    defparam inst.power_up = power_up;
    defparam inst.async_mode = async_mode;
    defparam inst.sync_mode = sync_mode;
    defparam inst.add_output_cycle_delay = add_output_cycle_delay;
    defparam inst.add_2nd_output_cycle_delay = add_2nd_output_cycle_delay;
    defparam inst.add_phase_transfer_reg = add_phase_transfer_reg;
    defparam inst.bypass_output_register = bypass_output_register;

endmodule //stratixv_output_alignment

`timescale 1 ps/1 ps

module    stratixv_pll_dll_output    (
    cclk,
    clkin,
    crsel,
    mout,
    clkout);

    parameter    lpm_type    =    "stratixv_pll_dll_output";
    parameter    pll_dll_src    =    "c_0_cnt";


    input    [17:0]    cclk;
    input    [3:0]    clkin;
    input    [4:0]    crsel;
    input    mout;
    output    clkout;

    stratixv_pll_dll_output_encrypted inst (
        .cclk(cclk),
        .clkin(clkin),
        .crsel(crsel),
        .mout(mout),
        .clkout(clkout) );
    defparam inst.lpm_type = lpm_type;
    defparam inst.pll_dll_src = pll_dll_src;

endmodule //stratixv_pll_dll_output

`timescale 1 ps/1 ps

module    stratixv_pll_dpa_output    (
    crdpaen,
    pd,
    phin,
    phout);

    parameter    lpm_type    =    "stratixv_pll_dpa_output";
    parameter    pll_vcoph_div_en    =    1;


    input    [1:0]    crdpaen;
    input    pd;
    input    [7:0]    phin;
    output    [7:0]    phout;

    stratixv_pll_dpa_output_encrypted inst (
        .crdpaen(crdpaen),
        .pd(pd),
        .phin(phin),
        .phout(phout) );
    defparam inst.lpm_type = lpm_type;
    defparam inst.pll_vcoph_div_en = pll_vcoph_div_en;

endmodule //stratixv_pll_dpa_output

`timescale 1 ps/1 ps

module    stratixv_pll_extclk_output    (
    cclk,
    clken,
    crenable,
    crextclkeninv,
    crinv,
    crsel,
    mcnt,
    niotri,
    extclk);

    parameter    lpm_type    =    "stratixv_pll_extclk_output";
    parameter    pll_extclk_cnt_src    =    "m0_cnt";
    parameter    pll_extclk_enable    =    "true";
    parameter    pll_extclk_invert    =    "false";
    parameter    pll_extclken_invert    =    "false";


    input    [17:0]    cclk;
    input    clken;
    input    crenable;
    input    crextclkeninv;
    input    crinv;
    input    [4:0]    crsel;
    input    mcnt;
    input    niotri;
    output    extclk;

    stratixv_pll_extclk_output_encrypted inst (
        .cclk(cclk),
        .clken(clken),
        .crenable(crenable),
        .crextclkeninv(crextclkeninv),
        .crinv(crinv),
        .crsel(crsel),
        .mcnt(mcnt),
        .niotri(niotri),
        .extclk(extclk) );
    defparam inst.lpm_type = lpm_type;
    defparam inst.pll_extclk_cnt_src = pll_extclk_cnt_src;
    defparam inst.pll_extclk_enable = pll_extclk_enable;
    defparam inst.pll_extclk_invert = pll_extclk_invert;
    defparam inst.pll_extclken_invert = pll_extclken_invert;

endmodule //stratixv_pll_extclk_output

`timescale 1 ps/1 ps

module    stratixv_pll_lvds_output    (
    ccout,
    crdly,
    loaden,
    lvdsclk);

    parameter    lpm_type    =    "stratixv_pll_lvds_output";
    parameter    pll_loaden_coarse_dly    =    "0 ps";
    parameter    pll_loaden_fine_dly    =    "0 ps";
    parameter    pll_lvdsclk_coarse_dly    =    "0 ps";
    parameter    pll_lvdsclk_fine_dly    =    "0 ps";


    input    [1:0]    ccout;
    input    [9:0]    crdly;
    output    loaden;
    output    lvdsclk;

    stratixv_pll_lvds_output_encrypted inst (
        .ccout(ccout),
        .crdly(crdly),
        .loaden(loaden),
        .lvdsclk(lvdsclk) );
    defparam inst.lpm_type = lpm_type;
    defparam inst.pll_loaden_coarse_dly = pll_loaden_coarse_dly;
    defparam inst.pll_loaden_fine_dly = pll_loaden_fine_dly;
    defparam inst.pll_lvdsclk_coarse_dly = pll_lvdsclk_coarse_dly;
    defparam inst.pll_lvdsclk_fine_dly = pll_lvdsclk_fine_dly;

endmodule //stratixv_pll_lvds_output

`timescale 1 ps/1 ps

module    stratixv_pll_output_counter    (
    cascadein,
    crhi,
    crlo,
    nen,
    shift,
    shiftdonei,
    shiften,
    tclk,
    up,
    vcoph,
    divclk,
    shiftdoneo);

    parameter    lpm_type    =    "stratixv_pll_output_counter";
    parameter    duty_cycle    =    50;
    parameter    output_clock_frequency    =    "0 ps";
    parameter    phase_shift    =    "0 ps";
    parameter    pll_c_cnt_bypass_en    =    "false";
    parameter    pll_c_cnt_coarse_dly    =    "0 ps";
    parameter    pll_c_cnt_fine_dly    =    "0 ps";
    parameter    pll_c_cnt_hi_div    =    3;
    parameter    pll_c_cnt_in_src    =    "ph_mux_clk";
    parameter    pll_c_cnt_lo_div    =    3;
    parameter    pll_c_cnt_odd_div_even_duty_en    =    "false";
    parameter    pll_c_cnt_ph_mux_prst    =    0;
    parameter    pll_c_cnt_prst    =    1;


    input    cascadein;
    input    [8:0]    crhi;
    input    [8:0]    crlo;
    input    nen;
    input    shift;
    input    shiftdonei;
    input    shiften;
    input    tclk;
    input    up;
    input    [7:0]    vcoph;
    output    divclk;
    output    shiftdoneo;

    stratixv_pll_output_counter_encrypted inst (
        .cascadein(cascadein),
        .crhi(crhi),
        .crlo(crlo),
        .nen(nen),
        .shift(shift),
        .shiftdonei(shiftdonei),
        .shiften(shiften),
        .tclk(tclk),
        .up(up),
        .vcoph(vcoph),
        .divclk(divclk),
        .shiftdoneo(shiftdoneo) );
    defparam inst.lpm_type = lpm_type;
    defparam inst.duty_cycle = duty_cycle;
    defparam inst.output_clock_frequency = output_clock_frequency;
    defparam inst.phase_shift = phase_shift;
    defparam inst.pll_c_cnt_bypass_en = pll_c_cnt_bypass_en;
    defparam inst.pll_c_cnt_coarse_dly = pll_c_cnt_coarse_dly;
    defparam inst.pll_c_cnt_fine_dly = pll_c_cnt_fine_dly;
    defparam inst.pll_c_cnt_hi_div = pll_c_cnt_hi_div;
    defparam inst.pll_c_cnt_in_src = pll_c_cnt_in_src;
    defparam inst.pll_c_cnt_lo_div = pll_c_cnt_lo_div;
    defparam inst.pll_c_cnt_odd_div_even_duty_en = pll_c_cnt_odd_div_even_duty_en;
    defparam inst.pll_c_cnt_ph_mux_prst = pll_c_cnt_ph_mux_prst;
    defparam inst.pll_c_cnt_prst = pll_c_cnt_prst;

endmodule //stratixv_pll_output_counter

`timescale 1 ps/1 ps

module    stratixv_pll_reconfig    (
    cntsel0,
    cr3lo,
    cr3prst,
    cr3sel,
    cr4dly,
    cr4hi,
    cr4lo,
    cr4prst,
    cr4sel,
    cr5dly,
    cr5hi,
    cntsel1,
    cr5lo,
    cr5prst,
    cr5sel,
    cr6dly,
    cr6hi,
    cr6lo,
    cr6prst,
    cr6sel,
    cr7dly,
    cr7hi,
    dprio0addr,
    cr7lo,
    cr7prst,
    cr7sel,
    cr8dly,
    cr8hi,
    cr8lo,
    cr8prst,
    cr8sel,
    cr9dly,
    cr9hi,
    dprio0byteen,
    cr9lo,
    cr9prst,
    cr9sel,
    crclkenen,
    crdll,
    crext,
    crextclkeninv,
    crextclkinv,
    crfpll0cp,
    crfpll0dpadiv,
    dprio0clk,
    crfpll0lckbypass,
    crfpll0lfc,
    crfpll0lfr,
    crfpll0lfrd,
    crfpll0lockc,
    crfpll0lockf,
    crfpll0mdirectfb,
    crfpll0mdly,
    crfpll0mhi,
    crfpll0mlo,
    dprio0din,
    crfpll0mprst,
    crfpll0msel,
    crfpll0ndly,
    crfpll0nhi,
    crfpll0nlo,
    crfpll0pfdpulsewidth,
    crfpll0selfrst,
    crfpll0tclk,
    crfpll0test,
    crfpll0unlockf,
    dprio0mdiodis,
    crfpll0vcop,
    crfpll0vcophbyps,
    crfpll0vcorangeen,
    crfpll0vr,
    crfpll1cp,
    crfpll1dpadiv,
    crfpll1lckbypass,
    crfpll1lfc,
    crfpll1lfr,
    crfpll1lfrd,
    dprio0read,
    crfpll1lockc,
    crfpll1lockf,
    crfpll1mdirectfb,
    crfpll1mdly,
    crfpll1mhi,
    crfpll1mlo,
    crfpll1mprst,
    crfpll1msel,
    crfpll1ndly,
    crfpll1nhi,
    dprio0rstn,
    crfpll1nlo,
    crfpll1pfdpulsewidth,
    crfpll1selfrst,
    crfpll1tclk,
    crfpll1test,
    crfpll1unlockf,
    crfpll1vcop,
    crfpll1vcophbyps,
    crfpll1vcorangeen,
    crfpll1vr,
    dprio0sershiftload,
    crinv,
    crlvds,
    crphaseshiftsel,
    crvcosel,
    crwrapback,
    crwrapbackmux,
    dprio0blockselect,
    dprio0dout,
    dprio1blockselect,
    dprio1dout,
    dprio0write,
    fpll0cntnen,
    fpll0enpfd,
    fpll0lfreset,
    fpll0niotricntr,
    fpll0pdbvr,
    fpll0pllpd,
    fpll0reset0,
    fpll0vcopen,
    fpll1cntnen,
    fpll1enpfd,
    dprio1addr,
    fpll1lfreset,
    fpll1niotricntr,
    fpll1pdbvr,
    fpll1pllpd,
    fpll1reset0,
    fpll1vcopen,
    iocsrdataout,
    phasedone,
    shift0,
    shift1,
    dprio1byteen,
    shiftdone0o,
    shiftdone1o,
    shiften,
    up0,
    up1,
    dprio1clk,
    dprio1din,
    dprio1mdiodis,
    dprio1read,
    dprio1rstn,
    dprio1sershiftload,
    dprio1write,
    fpll0selfrst,
    fpll1selfrst,
    iocsrclkin,
    iocsrdatain,
    ioplniotri,
    nfrzdrv,
    nreset,
    pfden,
    phaseen0,
    phaseen1,
    pllbias,
    updn0,
    updn1,
    cr0dly,
    cr0hi,
    cr0lo,
    cr0prst,
    cr0sel,
    cr10dly,
    cr10hi,
    cr10lo,
    cr10prst,
    cr10sel,
    cr11dly,
    cr11hi,
    cr11lo,
    cr11prst,
    cr11sel,
    cr12dly,
    cr12hi,
    cr12lo,
    cr12prst,
    cr12sel,
    cr13dly,
    cr13hi,
    cr13lo,
    cr13prst,
    cr13sel,
    cr14dly,
    cr14hi,
    cr14lo,
    cr14prst,
    cr14sel,
    cr15dly,
    cr15hi,
    cr15lo,
    cr15prst,
    cr15sel,
    cr16dly,
    cr16hi,
    cr16lo,
    cr16prst,
    cr16sel,
    cr17dly,
    cr17hi,
    cr17lo,
    cr17prst,
    cr17sel,
    cr1dly,
    cr1hi,
    cr1lo,
    cr1prst,
    cr1sel,
    cr2dly,
    cr2hi,
    cr2lo,
    cr2prst,
    cr2sel,
    cr3dly,
    cr3hi);

    parameter    lpm_type    =    "stratixv_pll_reconfig";


    input    [4:0]    cntsel0;
    output    [10:0]    cr3lo;
    output    [10:0]    cr3prst;
    output    [1:0]    cr3sel;
    output    [10:0]    cr4dly;
    output    [10:0]    cr4hi;
    output    [10:0]    cr4lo;
    output    [10:0]    cr4prst;
    output    [1:0]    cr4sel;
    output    [10:0]    cr5dly;
    output    [10:0]    cr5hi;
    input    [4:0]    cntsel1;
    output    [10:0]    cr5lo;
    output    [10:0]    cr5prst;
    output    [1:0]    cr5sel;
    output    [10:0]    cr6dly;
    output    [10:0]    cr6hi;
    output    [10:0]    cr6lo;
    output    [10:0]    cr6prst;
    output    [1:0]    cr6sel;
    output    [10:0]    cr7dly;
    output    [10:0]    cr7hi;
    input    [6:0]    dprio0addr;
    output    [10:0]    cr7lo;
    output    [10:0]    cr7prst;
    output    [1:0]    cr7sel;
    output    [10:0]    cr8dly;
    output    [10:0]    cr8hi;
    output    [10:0]    cr8lo;
    output    [10:0]    cr8prst;
    output    [1:0]    cr8sel;
    output    [10:0]    cr9dly;
    output    [10:0]    cr9hi;
    input    [1:0]    dprio0byteen;
    output    [10:0]    cr9lo;
    output    [10:0]    cr9prst;
    output    [1:0]    cr9sel;
    output    [3:0]    crclkenen;
    output    [9:0]    crdll;
    output    [19:0]    crext;
    output    [3:0]    crextclkeninv;
    output    [10:0]    crextclkinv;
    output    [2:0]    crfpll0cp;
    output    [1:0]    crfpll0dpadiv;
    input    dprio0clk;
    output    crfpll0lckbypass;
    output    [1:0]    crfpll0lfc;
    output    [4:0]    crfpll0lfr;
    output    [5:0]    crfpll0lfrd;
    output    [3:0]    crfpll0lockc;
    output    [11:0]    crfpll0lockf;
    output    crfpll0mdirectfb;
    output    [4:0]    crfpll0mdly;
    output    [8:0]    crfpll0mhi;
    output    [8:0]    crfpll0mlo;
    input    [15:0]    dprio0din;
    output    [10:0]    crfpll0mprst;
    output    [1:0]    crfpll0msel;
    output    [4:0]    crfpll0ndly;
    output    [8:0]    crfpll0nhi;
    output    [8:0]    crfpll0nlo;
    output    crfpll0pfdpulsewidth;
    output    [1:0]    crfpll0selfrst;
    output    [1:0]    crfpll0tclk;
    output    [1:0]    crfpll0test;
    output    [2:0]    crfpll0unlockf;
    input    dprio0mdiodis;
    output    [7:0]    crfpll0vcop;
    output    crfpll0vcophbyps;
    output    crfpll0vcorangeen;
    output    [6:0]    crfpll0vr;
    output    [2:0]    crfpll1cp;
    output    [1:0]    crfpll1dpadiv;
    output    crfpll1lckbypass;
    output    [1:0]    crfpll1lfc;
    output    [4:0]    crfpll1lfr;
    output    [5:0]    crfpll1lfrd;
    input    dprio0read;
    output    [3:0]    crfpll1lockc;
    output    [11:0]    crfpll1lockf;
    output    crfpll1mdirectfb;
    output    [4:0]    crfpll1mdly;
    output    [8:0]    crfpll1mhi;
    output    [8:0]    crfpll1mlo;
    output    [10:0]    crfpll1mprst;
    output    [1:0]    crfpll1msel;
    output    [4:0]    crfpll1ndly;
    output    [8:0]    crfpll1nhi;
    input    dprio0rstn;
    output    [8:0]    crfpll1nlo;
    output    crfpll1pfdpulsewidth;
    output    [1:0]    crfpll1selfrst;
    output    [1:0]    crfpll1tclk;
    output    [1:0]    crfpll1test;
    output    [2:0]    crfpll1unlockf;
    output    [7:0]    crfpll1vcop;
    output    crfpll1vcophbyps;
    output    crfpll1vcorangeen;
    output    [6:0]    crfpll1vr;
    input    dprio0sershiftload;
    output    [85:0]    crinv;
    output    [39:0]    crlvds;
    output    [17:0]    crphaseshiftsel;
    output    [17:0]    crvcosel;
    output    crwrapback;
    output    crwrapbackmux;
    output    dprio0blockselect;
    output    [15:0]    dprio0dout;
    output    dprio1blockselect;
    output    [15:0]    dprio1dout;
    input    dprio0write;
    output    fpll0cntnen;
    output    fpll0enpfd;
    output    fpll0lfreset;
    output    fpll0niotricntr;
    output    fpll0pdbvr;
    output    fpll0pllpd;
    output    fpll0reset0;
    output    fpll0vcopen;
    output    fpll1cntnen;
    output    fpll1enpfd;
    input    [6:0]    dprio1addr;
    output    fpll1lfreset;
    output    fpll1niotricntr;
    output    fpll1pdbvr;
    output    fpll1pllpd;
    output    fpll1reset0;
    output    fpll1vcopen;
    output    iocsrdataout;
    output    [1:0]    phasedone;
    output    shift0;
    output    shift1;
    input    [1:0]    dprio1byteen;
    output    shiftdone0o;
    output    shiftdone1o;
    output    [17:0]    shiften;
    output    up0;
    output    up1;
    input    dprio1clk;
    input    [15:0]    dprio1din;
    input    dprio1mdiodis;
    input    dprio1read;
    input    dprio1rstn;
    input    dprio1sershiftload;
    input    dprio1write;
    input    fpll0selfrst;
    input    fpll1selfrst;
    input    iocsrclkin;
    input    iocsrdatain;
    input    ioplniotri;
    input    nfrzdrv;
    input    [1:0]    nreset;
    input    pfden;
    input    phaseen0;
    input    phaseen1;
    input    pllbias;
    input    updn0;
    input    updn1;
    output    [10:0]    cr0dly;
    output    [10:0]    cr0hi;
    output    [10:0]    cr0lo;
    output    [10:0]    cr0prst;
    output    [1:0]    cr0sel;
    output    [10:0]    cr10dly;
    output    [10:0]    cr10hi;
    output    [10:0]    cr10lo;
    output    [10:0]    cr10prst;
    output    [1:0]    cr10sel;
    output    [10:0]    cr11dly;
    output    [10:0]    cr11hi;
    output    [10:0]    cr11lo;
    output    [10:0]    cr11prst;
    output    [1:0]    cr11sel;
    output    [10:0]    cr12dly;
    output    [10:0]    cr12hi;
    output    [10:0]    cr12lo;
    output    [10:0]    cr12prst;
    output    [1:0]    cr12sel;
    output    [10:0]    cr13dly;
    output    [10:0]    cr13hi;
    output    [10:0]    cr13lo;
    output    [10:0]    cr13prst;
    output    [1:0]    cr13sel;
    output    [10:0]    cr14dly;
    output    [10:0]    cr14hi;
    output    [10:0]    cr14lo;
    output    [10:0]    cr14prst;
    output    [1:0]    cr14sel;
    output    [10:0]    cr15dly;
    output    [10:0]    cr15hi;
    output    [10:0]    cr15lo;
    output    [10:0]    cr15prst;
    output    [1:0]    cr15sel;
    output    [10:0]    cr16dly;
    output    [10:0]    cr16hi;
    output    [10:0]    cr16lo;
    output    [10:0]    cr16prst;
    output    [1:0]    cr16sel;
    output    [10:0]    cr17dly;
    output    [10:0]    cr17hi;
    output    [10:0]    cr17lo;
    output    [10:0]    cr17prst;
    output    [1:0]    cr17sel;
    output    [10:0]    cr1dly;
    output    [10:0]    cr1hi;
    output    [10:0]    cr1lo;
    output    [10:0]    cr1prst;
    output    [1:0]    cr1sel;
    output    [10:0]    cr2dly;
    output    [10:0]    cr2hi;
    output    [10:0]    cr2lo;
    output    [10:0]    cr2prst;
    output    [1:0]    cr2sel;
    output    [10:0]    cr3dly;
    output    [10:0]    cr3hi;

    stratixv_pll_reconfig_encrypted inst (
        .cntsel0(cntsel0),
        .cr3lo(cr3lo),
        .cr3prst(cr3prst),
        .cr3sel(cr3sel),
        .cr4dly(cr4dly),
        .cr4hi(cr4hi),
        .cr4lo(cr4lo),
        .cr4prst(cr4prst),
        .cr4sel(cr4sel),
        .cr5dly(cr5dly),
        .cr5hi(cr5hi),
        .cntsel1(cntsel1),
        .cr5lo(cr5lo),
        .cr5prst(cr5prst),
        .cr5sel(cr5sel),
        .cr6dly(cr6dly),
        .cr6hi(cr6hi),
        .cr6lo(cr6lo),
        .cr6prst(cr6prst),
        .cr6sel(cr6sel),
        .cr7dly(cr7dly),
        .cr7hi(cr7hi),
        .dprio0addr(dprio0addr),
        .cr7lo(cr7lo),
        .cr7prst(cr7prst),
        .cr7sel(cr7sel),
        .cr8dly(cr8dly),
        .cr8hi(cr8hi),
        .cr8lo(cr8lo),
        .cr8prst(cr8prst),
        .cr8sel(cr8sel),
        .cr9dly(cr9dly),
        .cr9hi(cr9hi),
        .dprio0byteen(dprio0byteen),
        .cr9lo(cr9lo),
        .cr9prst(cr9prst),
        .cr9sel(cr9sel),
        .crclkenen(crclkenen),
        .crdll(crdll),
        .crext(crext),
        .crextclkeninv(crextclkeninv),
        .crextclkinv(crextclkinv),
        .crfpll0cp(crfpll0cp),
        .crfpll0dpadiv(crfpll0dpadiv),
        .dprio0clk(dprio0clk),
        .crfpll0lckbypass(crfpll0lckbypass),
        .crfpll0lfc(crfpll0lfc),
        .crfpll0lfr(crfpll0lfr),
        .crfpll0lfrd(crfpll0lfrd),
        .crfpll0lockc(crfpll0lockc),
        .crfpll0lockf(crfpll0lockf),
        .crfpll0mdirectfb(crfpll0mdirectfb),
        .crfpll0mdly(crfpll0mdly),
        .crfpll0mhi(crfpll0mhi),
        .crfpll0mlo(crfpll0mlo),
        .dprio0din(dprio0din),
        .crfpll0mprst(crfpll0mprst),
        .crfpll0msel(crfpll0msel),
        .crfpll0ndly(crfpll0ndly),
        .crfpll0nhi(crfpll0nhi),
        .crfpll0nlo(crfpll0nlo),
        .crfpll0pfdpulsewidth(crfpll0pfdpulsewidth),
        .crfpll0selfrst(crfpll0selfrst),
        .crfpll0tclk(crfpll0tclk),
        .crfpll0test(crfpll0test),
        .crfpll0unlockf(crfpll0unlockf),
        .dprio0mdiodis(dprio0mdiodis),
        .crfpll0vcop(crfpll0vcop),
        .crfpll0vcophbyps(crfpll0vcophbyps),
        .crfpll0vcorangeen(crfpll0vcorangeen),
        .crfpll0vr(crfpll0vr),
        .crfpll1cp(crfpll1cp),
        .crfpll1dpadiv(crfpll1dpadiv),
        .crfpll1lckbypass(crfpll1lckbypass),
        .crfpll1lfc(crfpll1lfc),
        .crfpll1lfr(crfpll1lfr),
        .crfpll1lfrd(crfpll1lfrd),
        .dprio0read(dprio0read),
        .crfpll1lockc(crfpll1lockc),
        .crfpll1lockf(crfpll1lockf),
        .crfpll1mdirectfb(crfpll1mdirectfb),
        .crfpll1mdly(crfpll1mdly),
        .crfpll1mhi(crfpll1mhi),
        .crfpll1mlo(crfpll1mlo),
        .crfpll1mprst(crfpll1mprst),
        .crfpll1msel(crfpll1msel),
        .crfpll1ndly(crfpll1ndly),
        .crfpll1nhi(crfpll1nhi),
        .dprio0rstn(dprio0rstn),
        .crfpll1nlo(crfpll1nlo),
        .crfpll1pfdpulsewidth(crfpll1pfdpulsewidth),
        .crfpll1selfrst(crfpll1selfrst),
        .crfpll1tclk(crfpll1tclk),
        .crfpll1test(crfpll1test),
        .crfpll1unlockf(crfpll1unlockf),
        .crfpll1vcop(crfpll1vcop),
        .crfpll1vcophbyps(crfpll1vcophbyps),
        .crfpll1vcorangeen(crfpll1vcorangeen),
        .crfpll1vr(crfpll1vr),
        .dprio0sershiftload(dprio0sershiftload),
        .crinv(crinv),
        .crlvds(crlvds),
        .crphaseshiftsel(crphaseshiftsel),
        .crvcosel(crvcosel),
        .crwrapback(crwrapback),
        .crwrapbackmux(crwrapbackmux),
        .dprio0blockselect(dprio0blockselect),
        .dprio0dout(dprio0dout),
        .dprio1blockselect(dprio1blockselect),
        .dprio1dout(dprio1dout),
        .dprio0write(dprio0write),
        .fpll0cntnen(fpll0cntnen),
        .fpll0enpfd(fpll0enpfd),
        .fpll0lfreset(fpll0lfreset),
        .fpll0niotricntr(fpll0niotricntr),
        .fpll0pdbvr(fpll0pdbvr),
        .fpll0pllpd(fpll0pllpd),
        .fpll0reset0(fpll0reset0),
        .fpll0vcopen(fpll0vcopen),
        .fpll1cntnen(fpll1cntnen),
        .fpll1enpfd(fpll1enpfd),
        .dprio1addr(dprio1addr),
        .fpll1lfreset(fpll1lfreset),
        .fpll1niotricntr(fpll1niotricntr),
        .fpll1pdbvr(fpll1pdbvr),
        .fpll1pllpd(fpll1pllpd),
        .fpll1reset0(fpll1reset0),
        .fpll1vcopen(fpll1vcopen),
        .iocsrdataout(iocsrdataout),
        .phasedone(phasedone),
        .shift0(shift0),
        .shift1(shift1),
        .dprio1byteen(dprio1byteen),
        .shiftdone0o(shiftdone0o),
        .shiftdone1o(shiftdone1o),
        .shiften(shiften),
        .up0(up0),
        .up1(up1),
        .dprio1clk(dprio1clk),
        .dprio1din(dprio1din),
        .dprio1mdiodis(dprio1mdiodis),
        .dprio1read(dprio1read),
        .dprio1rstn(dprio1rstn),
        .dprio1sershiftload(dprio1sershiftload),
        .dprio1write(dprio1write),
        .fpll0selfrst(fpll0selfrst),
        .fpll1selfrst(fpll1selfrst),
        .iocsrclkin(iocsrclkin),
        .iocsrdatain(iocsrdatain),
        .ioplniotri(ioplniotri),
        .nfrzdrv(nfrzdrv),
        .nreset(nreset),
        .pfden(pfden),
        .phaseen0(phaseen0),
        .phaseen1(phaseen1),
        .pllbias(pllbias),
        .updn0(updn0),
        .updn1(updn1),
        .cr0dly(cr0dly),
        .cr0hi(cr0hi),
        .cr0lo(cr0lo),
        .cr0prst(cr0prst),
        .cr0sel(cr0sel),
        .cr10dly(cr10dly),
        .cr10hi(cr10hi),
        .cr10lo(cr10lo),
        .cr10prst(cr10prst),
        .cr10sel(cr10sel),
        .cr11dly(cr11dly),
        .cr11hi(cr11hi),
        .cr11lo(cr11lo),
        .cr11prst(cr11prst),
        .cr11sel(cr11sel),
        .cr12dly(cr12dly),
        .cr12hi(cr12hi),
        .cr12lo(cr12lo),
        .cr12prst(cr12prst),
        .cr12sel(cr12sel),
        .cr13dly(cr13dly),
        .cr13hi(cr13hi),
        .cr13lo(cr13lo),
        .cr13prst(cr13prst),
        .cr13sel(cr13sel),
        .cr14dly(cr14dly),
        .cr14hi(cr14hi),
        .cr14lo(cr14lo),
        .cr14prst(cr14prst),
        .cr14sel(cr14sel),
        .cr15dly(cr15dly),
        .cr15hi(cr15hi),
        .cr15lo(cr15lo),
        .cr15prst(cr15prst),
        .cr15sel(cr15sel),
        .cr16dly(cr16dly),
        .cr16hi(cr16hi),
        .cr16lo(cr16lo),
        .cr16prst(cr16prst),
        .cr16sel(cr16sel),
        .cr17dly(cr17dly),
        .cr17hi(cr17hi),
        .cr17lo(cr17lo),
        .cr17prst(cr17prst),
        .cr17sel(cr17sel),
        .cr1dly(cr1dly),
        .cr1hi(cr1hi),
        .cr1lo(cr1lo),
        .cr1prst(cr1prst),
        .cr1sel(cr1sel),
        .cr2dly(cr2dly),
        .cr2hi(cr2hi),
        .cr2lo(cr2lo),
        .cr2prst(cr2prst),
        .cr2sel(cr2sel),
        .cr3dly(cr3dly),
        .cr3hi(cr3hi) );
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_pll_reconfig

`timescale 1 ps/1 ps

module    stratixv_pll_refclk_select    (
    extswitch,
    pllen,
    refclk,
    clk0bad,
    clk1bad,
    clkout,
    pllclksel);

    parameter    lpm_type    =    "stratixv_pll_refclk_select";
    parameter    pll_auto_clk_sw_en    =    "false";
    parameter    pll_clk_loss_edge    =    "pll_clk_loss_both_edges";
    parameter    pll_clk_loss_sw_en    =    "false";
    parameter    pll_clk_sw_dly    =    "0 ps";
    parameter    pll_manu_clk_sw_en    =    "false";
    parameter    pll_sw_refclk_src    =    "clk_0";
    parameter    reference_clock_frequency_0    =    "0 ps";
    parameter    reference_clock_frequency_1    =    "0 ps";


    input    extswitch;
    input    pllen;
    input    [1:0]    refclk;
    output    clk0bad;
    output    clk1bad;
    output    clkout;
    output    pllclksel;

    stratixv_pll_refclk_select_encrypted inst (
        .extswitch(extswitch),
        .pllen(pllen),
        .refclk(refclk),
        .clk0bad(clk0bad),
        .clk1bad(clk1bad),
        .clkout(clkout),
        .pllclksel(pllclksel) );
    defparam inst.lpm_type = lpm_type;
    defparam inst.pll_auto_clk_sw_en = pll_auto_clk_sw_en;
    defparam inst.pll_clk_loss_edge = pll_clk_loss_edge;
    defparam inst.pll_clk_loss_sw_en = pll_clk_loss_sw_en;
    defparam inst.pll_clk_sw_dly = pll_clk_sw_dly;
    defparam inst.pll_manu_clk_sw_en = pll_manu_clk_sw_en;
    defparam inst.pll_sw_refclk_src = pll_sw_refclk_src;
    defparam inst.reference_clock_frequency_0 = reference_clock_frequency_0;
    defparam inst.reference_clock_frequency_1 = reference_clock_frequency_1;

endmodule //stratixv_pll_refclk_select

`timescale 1 ps/1 ps

module    stratixv_termination_logic    (
    s2pload,
    serdata,
    scanenable,
    scanin,
    scanclk,
    scanout,
    seriesterminationcontrol,
    parallelterminationcontrol);

    parameter    lpm_type    =    "stratixv_termination_logic";
    parameter 	 a_iob_oct_test = "a_iob_oct_test_off";

    input  s2pload;
    input  serdata;
    input  scanenable;
    input  scanin;
    input  scanclk;

    output scanout;
    output [15 : 0] parallelterminationcontrol;
    output [15 : 0] seriesterminationcontrol;

    stratixv_termination_logic_encrypted inst (
        .s2pload(s2pload),
        .serdata(serdata),
        .scanenable(scanenable),
        .scanin(scanin),
        .scanclk(scanclk),
        .scanout(scanout),
        .seriesterminationcontrol(seriesterminationcontrol),
        .parallelterminationcontrol(parallelterminationcontrol));
    defparam inst.lpm_type = lpm_type;
    defparam  inst.a_iob_oct_test  = a_iob_oct_test;

endmodule //stratixv_termination_logic

`timescale 1 ps/1 ps

module    stratixv_termination    (
    rzqin,
    enserusr,
    nclrusr,
    clkenusr,
    clkusr,
    scanmode,
    serdatafromcore,
    scanclk,
    otherenser,
    serdataout,
    enserout,
    compoutrup,
    compoutrdn,
    serdatatocore,
    scanin,
    scanout);

    parameter lpm_type = "stratixv_termination";
    parameter a_oct_nclrusr_inv = "a_oct_nclrusr_inv_off";
    parameter a_oct_pwrdn = "true";
    parameter a_oct_clkdiv = "a_oct_clkdiv_20";
    parameter a_oct_intosc = "a_oct_intosc_2";
    parameter a_oct_vref = "a_oct_vref_rupm_rdnm";
    parameter a_oct_test_0 = "a_oct_test_0_off";
    parameter a_oct_test_1 = "a_oct_test_1_off";
    parameter a_oct_test_2 = "a_oct_test_2_off";
    parameter a_oct_test_3 = "a_oct_test_3_off";
    parameter a_oct_test_4 = "a_oct_test_4_off";
    parameter a_oct_test_5 = "a_oct_test_5_off";
    parameter a_oct_pllbiasen = "a_oct_pllbiasen_low";
    parameter a_oct_usermode = "false";

    input rzqin;
    input enserusr;
    input nclrusr;
    input clkenusr;
    input clkusr;
    input scanmode;
    input serdatafromcore;
    input scanclk;
    input [8 : 0] otherenser;
    output serdataout;
    output enserout;
    output compoutrup;
    output compoutrdn;
    output serdatatocore;
    output scanin;
    output scanout;

    stratixv_termination_encrypted inst (
        .rzqin(rzqin),
		.enserusr(enserusr),
    	.nclrusr(nclrusr),
    	.clkenusr(clkenusr),
    	.clkusr(clkusr),
    	.scanmode(scanmode),
    	.serdatafromcore(serdatafromcore),
    	.scanclk(scanclk),
    	.otherenser(otherenser),
    	.serdataout(serdataout),
    	.enserout(enserout),
    	.compoutrup(compoutrup),
    	.compoutrdn(compoutrdn),
    	.serdatatocore(serdatatocore),
    	.scanin(scanin),
    	.scanout(scanout));
    defparam inst.lpm_type = lpm_type;
    defparam inst.a_oct_nclrusr_inv = a_oct_nclrusr_inv;
    defparam inst.a_oct_pwrdn = a_oct_pwrdn;
    defparam inst.a_oct_clkdiv = a_oct_clkdiv;
    defparam inst.a_oct_intosc = a_oct_intosc;
    defparam inst.a_oct_vref = a_oct_vref;
    defparam inst.a_oct_test_0 = a_oct_test_0;
    defparam inst.a_oct_test_1 = a_oct_test_1;
    defparam inst.a_oct_test_2 = a_oct_test_2;
    defparam inst.a_oct_test_3 = a_oct_test_3;
    defparam inst.a_oct_test_4 = a_oct_test_4;
    defparam inst.a_oct_test_5 = a_oct_test_5;
    defparam inst.a_oct_pllbiasen = a_oct_pllbiasen;
    defparam inst.a_oct_usermode = a_oct_usermode;

endmodule //stratixv_termination

`timescale 1 ps/1 ps

module    stratixv_asmiblock    (
    dclk,
    sce,
    oe,
    data0out,
    data1out,
    data2out,
    data3out,
    data0oe,
    data1oe,
    data2oe,
    data3oe,
    data0in,
    data1in,
    data2in,
    data3in);

    parameter    lpm_type    =    "stratixv_asmiblock";


    input    dclk;
    input    sce;
    input    oe;
    input    data0out;
    input    data1out;
    input    data2out;
    input    data3out;
    input    data0oe;
    input    data1oe;
    input    data2oe;
    input    data3oe;
    output    data0in;
    output    data1in;
    output    data2in;
    output    data3in;

    stratixv_asmiblock_encrypted inst (
        .dclk(dclk),
        .sce(sce),
        .oe(oe),
        .data0out(data0out),
        .data1out(data1out),
        .data2out(data2out),
        .data3out(data3out),
        .data0oe(data0oe),
        .data1oe(data1oe),
        .data2oe(data2oe),
        .data3oe(data3oe),
        .data0in(data0in),
        .data1in(data1in),
        .data2in(data2in),
        .data3in(data3in) );
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_asmiblock

`timescale 1 ps/1 ps

module    stratixv_chipidblock    (
    clk,
    shiftnld,
    regout);

    parameter    lpm_type    =    "stratixv_chipidblock";


    input    clk;
    input    shiftnld;
    output    regout;

    stratixv_chipidblock_encrypted inst (
        .clk(clk),
        .shiftnld(shiftnld),
        .regout(regout) );
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_chipidblock

`timescale 1 ps/1 ps

module    stratixv_controller    (
    nceout);

    parameter    lpm_type    =    "stratixv_controller";


    output    nceout;

    stratixv_controller_encrypted inst (
        .nceout(nceout) );
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_controller

`timescale 1 ps/1 ps

module    stratixv_crcblock    (
    clk,
    shiftnld,
    crcerror,
    regout);

    parameter    oscillator_divider    =    256;
    parameter    lpm_type    =    "stratixv_crcblock";


    input    clk;
    input    shiftnld;
    output    crcerror;
    output    regout;

    stratixv_crcblock_encrypted inst (
        .clk(clk),
        .shiftnld(shiftnld),
        .crcerror(crcerror),
        .regout(regout) );
    defparam inst.oscillator_divider = oscillator_divider;
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_crcblock

`timescale 1 ps/1 ps

module    stratixv_jtag    (
    tms,
    tck,
    tdi,
    ntrst,
    tdoutap,
    tdouser,
    tdo,
    tmsutap,
    tckutap,
    tdiutap,
    shiftuser,
    clkdruser,
    updateuser,
    runidleuser,
    usr1user);

    parameter    lpm_type    =    "stratixv_jtag";


    input    tms;
    input    tck;
    input    tdi;
    input    ntrst;
    input    tdoutap;
    input    tdouser;
    output    tdo;
    output    tmsutap;
    output    tckutap;
    output    tdiutap;
    output    shiftuser;
    output    clkdruser;
    output    updateuser;
    output    runidleuser;
    output    usr1user;

    stratixv_jtag_encrypted inst (
        .tms(tms),
        .tck(tck),
        .tdi(tdi),
        .ntrst(ntrst),
        .tdoutap(tdoutap),
        .tdouser(tdouser),
        .tdo(tdo),
        .tmsutap(tmsutap),
        .tckutap(tckutap),
        .tdiutap(tdiutap),
        .shiftuser(shiftuser),
        .clkdruser(clkdruser),
        .updateuser(updateuser),
        .runidleuser(runidleuser),
        .usr1user(usr1user) );
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_jtag

`timescale 1 ps/1 ps

module    stratixv_prblock    (
    clk,
    corectl,
    prrequest,
    data,
    externalrequest,
    error,
    ready,
    done);

    parameter    lpm_type    =    "stratixv_prblock";


    input    clk;
    input    corectl;
    input    prrequest;
    input    [15:0]    data;
    output    externalrequest;
    output    error;
    output    ready;
    output    done;

    stratixv_prblock_encrypted inst (
        .clk(clk),
        .corectl(corectl),
        .prrequest(prrequest),
        .data(data),
        .externalrequest(externalrequest),
        .error(error),
        .ready(ready),
        .done(done) );
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_prblock

`timescale 1 ps/1 ps

module    stratixv_rublock    (
    clk,
    shiftnld,
    captnupdt,
    regin,
    rsttimer,
    rconfig,
    regout);

    parameter    sim_init_watchdog_value    =    0;
    parameter    sim_init_status    =    0;
    parameter    sim_init_config_is_application    =    "false";
    parameter    sim_init_watchdog_enabled    =    "false";
    parameter    lpm_type    =    "stratixv_rublock";


    input    clk;
    input    shiftnld;
    input    captnupdt;
    input    regin;
    input    rsttimer;
    input    rconfig;
    output    regout;

    stratixv_rublock_encrypted inst (
        .clk(clk),
        .shiftnld(shiftnld),
        .captnupdt(captnupdt),
        .regin(regin),
        .rsttimer(rsttimer),
        .rconfig(rconfig),
        .regout(regout) );
    defparam inst.sim_init_watchdog_value = sim_init_watchdog_value;
    defparam inst.sim_init_status = sim_init_status;
    defparam inst.sim_init_config_is_application = sim_init_config_is_application;
    defparam inst.sim_init_watchdog_enabled = sim_init_watchdog_enabled;
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_rublock

`timescale 1 ps/1 ps

module    stratixv_tsdblock    (
    clk,
    ce,
    clr,
    tsdcalo,
    tsdcaldone);

    parameter    clock_divider_enable    =    "on";
    parameter    clock_divider_value    =    40;
    parameter    sim_tsdcalo    =    0;
    parameter    lpm_type    =    "stratixv_tsdblock";


    input    clk;
    input    ce;
    input    clr;
    output    [7:0]    tsdcalo;
    output    tsdcaldone;

    stratixv_tsdblock_encrypted inst (
        .clk(clk),
        .ce(ce),
        .clr(clr),
        .tsdcalo(tsdcalo),
        .tsdcaldone(tsdcaldone) );
    defparam inst.clock_divider_enable = clock_divider_enable;
    defparam inst.clock_divider_value = clock_divider_value;
    defparam inst.sim_tsdcalo = sim_tsdcalo;
    defparam inst.lpm_type = lpm_type;

endmodule //stratixv_tsdblock

`timescale 1 ps/1 ps

module stratixv_read_fifo (
                           datain,
                           wclk,
                           we,
                           rclk,
                           re,
                           areset,
                           plus2,
                           dataout
                          );

    parameter use_half_rate_read = "false";


    input [1:0] datain; 
    input wclk;
    input we;
    input rclk;
    input re;
    input areset;
    input plus2;

    output [3:0]dataout;

    stratixv_read_fifo_encrypted inst (
    	.datain(datain),
        .wclk(wclk),
        .we(we),
        .rclk(rclk),
        .re(re),
        .areset(areset),
        .plus2(plus2),
        .dataout(dataout));
    defparam inst.use_half_rate_read = use_half_rate_read;
 
endmodule //stratixv_read_fifo

`timescale 1 ps/1 ps

module stratixv_read_fifo_read_enable (
                           re,
                           rclk,
                           plus2,
                           areset,
                           reout,
                           plus2out
                          );

    parameter use_stalled_read_enable = "false";

    input re;
    input rclk;
    input plus2;
    input areset;

    output reout;
    output plus2out;

    stratixv_read_fifo_read_enable_encrypted inst (
    	.re(re),
    	.rclk(rclk),
    	.plus2(plus2),
    	.areset(areset),
    	.reout(reout),
    	.plus2out(plus2out));
    	
    defparam inst.use_stalled_read_enable = use_stalled_read_enable;
 
endmodule //stratixv_read_fifo_read_enable
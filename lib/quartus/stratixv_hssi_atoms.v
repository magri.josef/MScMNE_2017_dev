// Copyright (C) 1991-2010 Altera Corporation
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, Altera MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs for
// use exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// Quartus II 10.1 Build 153 11/29/2010
`timescale 1 ps/1 fs

module    stratixv_channel_pll    (
    clk270eye,
    clk90eye,
    crurstb,
    deeye,
    doeye,
    earlyeios,
    extclk,
    lpbkpreen,
    ltd,
    ltr,
    occalen,
    pciel,
    pciem,
    pciesw,
    ppmlock,
    refclk,
    rstn,
    rxp,
    sd,
    ck0pd,
    ck180pd,
    ck270pd,
    ck90pd,
    clk270bcdr,
    clk270bdes,
    clk90bcdr,
    clk90bdes,
    clkcdr,
    clklow,
    decdr,
    deven,
    docdr,
    dodd,
    fref,
    pdof,
    pfdmodelock,
    rxlpbdp,
    rxlpbp,
    rxplllock,
    txpllhclk,
    txrlpbk);

    parameter    reference_clock_frequency    =    "0 ps";
    parameter    output_clock_frequency    =    "0 ps";
    parameter    bbpd_salatch_offset_ctrl_clk0    =    "clk0_offset_0mv";
    parameter    bbpd_salatch_offset_ctrl_clk180    =    "clk180_offset_0mv";
    parameter    bbpd_salatch_offset_ctrl_clk270    =    "clk270_offset_0mv";
    parameter    bbpd_salatch_offset_ctrl_clk90    =    "clk90_offset_0mv";
    parameter    bbpd_salatch_sel    =    "normal";
    parameter    bypass_cp_rgla    =    "false";
    parameter    cdr_atb_select    =    "atb_disable";
    parameter    cgb_clk_enable    =    "false";
    parameter    charge_pump_current_test    =    "enable_ch_pump_normal";
    parameter    clklow_fref_to_ppm_div_sel    =    1;
    parameter    clock_monitor    =    "lpbk_data";
    parameter    eye_monitor_bbpd_data_ctrl    =    "cdr_data";
    parameter    fast_lock_mode    =    "false";
    parameter    fb_sel    =    "vcoclk";
   	parameter    gpon_lock2ref_ctrl    =    "lck2ref";
	parameter    hs_levshift_power_supply_setting    =    1;
    parameter    ignore_phslock    =    "false";
    parameter    l_counter_pd_clock_disable    =    "false";
    parameter    m_counter    =    25;
    parameter    pcie_freq_control    =    "pcie_100mhz";
    parameter    pd_charge_pump_current_ctrl    =    5;
    parameter    pd_l_counter    =    1;
    parameter    pfd_charge_pump_current_ctrl    =    20;
    parameter    pfd_l_counter    =    1;
    parameter    powerdown    =    "false";
    parameter    ref_clk_div    =    1;
    parameter    regulator_volt_inc    =    "volt_inc_0pct";
    parameter    replica_bias_ctrl    =    "true";
    parameter    reverse_serial_lpbk    =    "false";
    parameter    ripple_cap_ctrl    =    "none";
    parameter    rxpll_pd_bw_ctrl    =    300;
    parameter    rxpll_pfd_bw_ctrl    =    3200;
    parameter    txpll_hclk_driver_enable    =    "false";


    input    clk270eye;
    input    clk90eye;
    input    crurstb;
    input    deeye;
    input    doeye;
    input    earlyeios;
    input    extclk;
    input    lpbkpreen;
    input    ltd;
    input    ltr;
    input    occalen;
    input    pciel;
    input    pciem;
    input    [1:0]    pciesw;
    input    ppmlock;
    input    refclk;
    input    rstn;
    input    rxp;
    input    sd;
    output    ck0pd;
    output    ck180pd;
    output    ck270pd;
    output    ck90pd;
    output    clk270bcdr;
    output    clk270bdes;
    output    clk90bcdr;
    output    clk90bdes;
    output    clkcdr;
    output    clklow;
    output    decdr;
    output    deven;
    output    docdr;
    output    dodd;
    output    fref;
    output    [3:0]    pdof;
    output    pfdmodelock;
    output    rxlpbdp;
    output    rxlpbp;
    output    rxplllock;
    output    txpllhclk;
    output    txrlpbk;

    stratixv_channel_pll_encrypted inst (
        .clk270eye(clk270eye),
        .clk90eye(clk90eye),
        .crurstb(crurstb),
        .deeye(deeye),
        .doeye(doeye),
        .earlyeios(earlyeios),
        .extclk(extclk),
        .lpbkpreen(lpbkpreen),
        .ltd(ltd),
        .ltr(ltr),
        .occalen(occalen),
        .pciel(pciel),
        .pciem(pciem),
        .pciesw(pciesw),
        .ppmlock(ppmlock),
        .refclk(refclk),
        .rstn(rstn),
        .rxp(rxp),
        .sd(sd),
        .ck0pd(ck0pd),
        .ck180pd(ck180pd),
        .ck270pd(ck270pd),
        .ck90pd(ck90pd),
        .clk270bcdr(clk270bcdr),
        .clk270bdes(clk270bdes),
        .clk90bcdr(clk90bcdr),
        .clk90bdes(clk90bdes),
        .clkcdr(clkcdr),
        .clklow(clklow),
        .decdr(decdr),
        .deven(deven),
        .docdr(docdr),
        .dodd(dodd),
        .fref(fref),
        .pdof(pdof),
        .pfdmodelock(pfdmodelock),
        .rxlpbdp(rxlpbdp),
        .rxlpbp(rxlpbp),
        .rxplllock(rxplllock),
        .txpllhclk(txpllhclk),
        .txrlpbk(txrlpbk) );
    defparam inst.reference_clock_frequency = reference_clock_frequency;
    defparam inst.output_clock_frequency = output_clock_frequency;
    defparam inst.bbpd_salatch_offset_ctrl_clk0 = bbpd_salatch_offset_ctrl_clk0;
    defparam inst.bbpd_salatch_offset_ctrl_clk180 = bbpd_salatch_offset_ctrl_clk180;
    defparam inst.bbpd_salatch_offset_ctrl_clk270 = bbpd_salatch_offset_ctrl_clk270;
    defparam inst.bbpd_salatch_offset_ctrl_clk90 = bbpd_salatch_offset_ctrl_clk90;
    defparam inst.bbpd_salatch_sel = bbpd_salatch_sel;
    defparam inst.bypass_cp_rgla = bypass_cp_rgla;
    defparam inst.cdr_atb_select = cdr_atb_select;
    defparam inst.cgb_clk_enable = cgb_clk_enable;
    defparam inst.charge_pump_current_test = charge_pump_current_test;
    defparam inst.clklow_fref_to_ppm_div_sel = clklow_fref_to_ppm_div_sel;
    defparam inst.clock_monitor = clock_monitor;
    defparam inst.eye_monitor_bbpd_data_ctrl = eye_monitor_bbpd_data_ctrl;
    defparam inst.fast_lock_mode = fast_lock_mode;
    defparam inst.fb_sel = fb_sel;
    defparam inst.gpon_lock2ref_ctrl = gpon_lock2ref_ctrl;
    defparam inst.hs_levshift_power_supply_setting = hs_levshift_power_supply_setting;
    defparam inst.ignore_phslock = ignore_phslock;
    defparam inst.l_counter_pd_clock_disable = l_counter_pd_clock_disable;
    defparam inst.m_counter = m_counter;
    defparam inst.pcie_freq_control = pcie_freq_control;
    defparam inst.pd_charge_pump_current_ctrl = pd_charge_pump_current_ctrl;
    defparam inst.pd_l_counter = pd_l_counter;
    defparam inst.pfd_charge_pump_current_ctrl = pfd_charge_pump_current_ctrl;
    defparam inst.pfd_l_counter = pfd_l_counter;
    defparam inst.powerdown = powerdown;
    defparam inst.ref_clk_div = ref_clk_div;
    defparam inst.regulator_volt_inc = regulator_volt_inc;
    defparam inst.replica_bias_ctrl = replica_bias_ctrl;
    defparam inst.reverse_serial_lpbk = reverse_serial_lpbk;
    defparam inst.ripple_cap_ctrl = ripple_cap_ctrl;
    defparam inst.rxpll_pd_bw_ctrl = rxpll_pd_bw_ctrl;
    defparam inst.rxpll_pfd_bw_ctrl = rxpll_pfd_bw_ctrl;
    defparam inst.txpll_hclk_driver_enable = txpll_hclk_driver_enable;

endmodule //stratixv_channel_pll

`timescale 1 ps/1 ps

module stratixv_hssi_10g_rx_pcs
(
	bercount,
	errorblockcount,
	pcsstatus,
	randomerrorcount,
	prbserrorlatch,
	txpmaclk,
	rxpmaclk,
	pmaclkdiv33txorrx,
	rxpmadatavalid,
	hardresetn,
	rxpldclk,
	rxpldrstn,
	refclkdig,
	rxalignen,
	rxalignclr,
	rxrden,
	rxdisparityclr,
	rxclrerrorblockcount,
	rxclrbercount,
	rxbitslip,
	rxprbserrorclr,
	rxclkout,
	rxclkiqout,
	rxdatavalid,
	rxfifoempty,
	rxfifopartialempty,
	rxfifopartialfull,
	rxfifofull,
	rxalignval,
	rxblocklock,
	rxsyncheadererror,
	rxhighber,
	rxframelock,
	rxrdpossts,
	rxrdnegsts,
	rxskipinserted,
	rxrxframe,
	rxpayloadinserted,
	rxsyncworderror,
	rxscramblererror,
	rxskipworderror,
	rxdiagnosticerror,
	rxmetaframeerror,
	rxcrc32error,
	rxdiagnosticstatus,
	rxdata,
	rxcontrol,
	accumdisparity,
	loopbackdatain,
	rxpmadata,
	rxtestdata,
	syncdatain
); 

// parameter declaration and default value assignemnt
parameter prot_mode = "disable_mode";	//Valid values: DISABLE_MODE|TENG_BASER_MODE|INTERLAKEN_MODE|SFIS_MODE|TENG_SDI_MODE|BASIC_MODE|TEST_PRBS_MODE|TEST_PRP_MODE
parameter sup_mode = "full_mode";	//Valid values: FULL_MODE|STRETCH_MODE|ENGR_MODE
parameter dis_signal_ok = "dis_signal_ok_dis";	//Valid values: DIS_SIGNAL_OK_DIS|DIS_SIGNAL_OK_EN
parameter gb_rx_idwidth = "idwidth_32";	//Valid values: IDWIDTH_40|IDWIDTH_32|IDWIDTH_20
parameter gb_rx_odwidth = "odwidth_66";	//Valid values: ODWIDTH_32|ODWIDTH_40|ODWIDTH_50|ODWIDTH_67|ODWIDTH_64|ODWIDTH_66
parameter bit_reverse = "bit_reverse_dis";	//Valid values: BIT_REVERSE_DIS|BIT_REVERSE_EN
parameter gb_sel_mode = "internal";	//Valid values: INTERNAL|EXTERNAL
parameter lpbk_mode = "lpbk_dis";	//Valid values: LPBK_DIS|LPBK_EN
parameter test_mode = "test_off";	//Valid values: TEST_OFF|PSEUDO_RANDOM|PRBS_31|PRBS_23|PRBS_9|PRBS_7
parameter blksync_bypass = "blksync_bypass_dis";	//Valid values: BLKSYNC_BYPASS_DIS|BLKSYNC_BYPASS_EN
parameter blksync_pipeln = "blksync_pipeln_dis";	//Valid values: BLKSYNC_PIPELN_DIS|BLKSYNC_PIPELN_EN
parameter blksync_knum_sh_cnt_prelock = "int";	//Valid values: 63
parameter blksync_knum_sh_cnt_postlock = "int";	//Valid values: 63
parameter blksync_enum_invalid_sh_cnt = "int";	//Valid values: 15
parameter blksync_bitslip_wait_cnt = "int";	//Valid values: 1
parameter bitslip_wait_cnt_user = "int";	//Valid values: 1
parameter blksync_bitslip_type = "bitslip_comb";	//Valid values: BITSLIP_COMB|BITSLIP_REG
parameter blksync_bitslip_wait_type = "bitslip_match";	//Valid values: BITSLIP_MATCH|BITSLIP_CNT
parameter dispchk_bypass = "dispchk_bypass_dis";	//Valid values: DISPCHK_BYPASS_DIS|DISPCHK_BYPASS_EN
parameter dispchk_rd_level = "dispchk_rd_level_min";	//Valid values: DISPCHK_RD_LEVEL_MIN|DISPCHK_RD_LEVEL_MAX|DISPCHK_RD_LEVEL_USER_SETTING
parameter dispchk_rd_level_user = "int";	//Valid values: 96
parameter dispchk_pipeln = "dispchk_pipeln_dis";	//Valid values: DISPCHK_PIPELN_DIS|DISPCHK_PIPELN_EN
parameter descrm_bypass = "descrm_bypass_en";	//Valid values: DESCRM_BYPASS_DIS|DESCRM_BYPASS_EN
parameter descrm_mode = "async";	//Valid values: ASYNC|SYNC
parameter frmsync_bypass = "frmsync_bypass_dis";	//Valid values: FRMSYNC_BYPASS_DIS|FRMSYNC_BYPASS_EN
parameter frmsync_pipeln = "frmsync_pipeln_dis";	//Valid values: FRMSYNC_PIPELN_DIS|FRMSYNC_PIPELN_EN
parameter frmsync_mfrm_length = "int";	//Valid values: 5
parameter frmsync_mfrm_length_user = "int";	//Valid values: 2048
parameter frmsync_knum_sync = "int";	//Valid values: 4
parameter frmsync_enum_sync = "int";	//Valid values: 4
parameter frmsync_enum_scrm = "int";	//Valid values: 3
parameter frmsync_flag_type = "all_framing_words";	//Valid values: ALL_FRAMING_WORDS|LOCATION_ONLY
parameter dec_64b66b_10g_mode = "dec_64b66b_10g_mode_en";	//Valid values: DEC_64B66B_10G_MODE_EN|DEC_64B66B_10G_MODE_DIS
parameter dec_64b66b_rxsm_bypass = "dec_64b66b_rxsm_bypass_dis";	//Valid values: DEC_64B66B_RXSM_BYPASS_DIS|DEC_64B66B_RXSM_BYPASS_EN
parameter rx_sm_bypass = "rx_sm_bypass_dis";	//Valid values: RX_SM_BYPASS_DIS|RX_SM_BYPASS_EN
parameter rx_sm_pipeln = "rx_sm_pipeln_dis";	//Valid values: RX_SM_PIPELN_DIS|RX_SM_PIPELN_EN
parameter rx_sm_hiber = "rx_sm_hiber_en";	//Valid values: RX_SM_HIBER_EN|RX_SM_HIBER_DIS
parameter ber_xus_timer_window = "int";	//Valid values: 19530
parameter ber_bit_err_total_cnt = "int";	//Valid values: 15
parameter crcchk_bypass = "crcchk_bypass_dis";	//Valid values: CRCCHK_BYPASS_DIS|CRCCHK_BYPASS_EN
parameter crcchk_pipeln = "crcchk_pipeln_dis";	//Valid values: CRCCHK_PIPELN_DIS|CRCCHK_PIPELN_EN
parameter crcflag_pipeln = "crcflag_pipeln_dis";	//Valid values: CRCFLAG_PIPELN_DIS|CRCFLAG_PIPELN_EN
parameter crcchk_init = "crcchk_init_user_setting";	//Valid values: CRCCHK_INIT_USER_SETTING
parameter crcchk_init_user = 32'b11111111111111111111111111111111;	//Valid values: 
parameter crcchk_inv = "crcchk_inv_dis";	//Valid values: CRCCHK_INV_DIS|CRCCHK_INV_EN
parameter force_align = "force_align_dis";	//Valid values: FORCE_ALIGN_DIS|FORCE_ALIGN_EN
parameter align_del = "align_del_en";	//Valid values: ALIGN_DEL_DIS|ALIGN_DEL_EN
parameter control_del = 8'b11110000;	//Valid values: 8
parameter rxfifo_mode = "phase_comp";	//Valid values: PHASE_COMP|CLK_COMP|REGISTER_MODE|GENERIC
parameter master_clk_sel = "master_rx_pma_clk";	//Valid values: MASTER_RX_PMA_CLK|MASTER_TX_PMA_CLK|MASTER_REFCLK_DIG
parameter rd_clk_sel = "rd_rx_pma_clk";	//Valid values: RD_RX_PLD_CLK|RD_RX_PMA_CLK|RD_REFCLK_DIG
parameter gbexp_clken = "gbexp_clk_dis";	//Valid values: GBEXP_CLK_DIS|GBEXP_CLK_EN
parameter prbs_clken = "prbs_clk_dis";	//Valid values: PRBS_CLK_DIS|PRBS_CLK_EN
parameter blksync_clken = "blksync_clk_dis";	//Valid values: BLKSYNC_CLK_DIS|BLKSYNC_CLK_EN
parameter dispchk_clken = "dispchk_clk_dis";	//Valid values: DISPCHK_CLK_DIS|DISPCHK_CLK_EN
parameter descrm_clken = "descrm_clk_dis";	//Valid values: DESCRM_CLK_DIS|DESCRM_CLK_EN
parameter frmsync_clken = "frmsync_clk_dis";	//Valid values: FRMSYNC_CLK_DIS|FRMSYNC_CLK_EN
parameter dec64b66b_clken = "dec64b66b_clk_dis";	//Valid values: DEC64B66B_CLK_DIS|DEC64B66B_CLK_EN
parameter ber_clken = "ber_clk_dis";	//Valid values: BER_CLK_DIS|BER_CLK_EN
parameter rand_clken = "rand_clk_dis";	//Valid values: RAND_CLK_DIS|RAND_CLK_EN
parameter crcchk_clken = "crcchk_clk_dis";	//Valid values: CRCCHK_CLK_DIS|CRCCHK_CLK_EN
parameter wrfifo_clken = "wrfifo_clk_dis";	//Valid values: WRFIFO_CLK_DIS|WRFIFO_CLK_EN
parameter rdfifo_clken = "rdfifo_clk_dis";	//Valid values: RDFIFO_CLK_DIS|RDFIFO_CLK_EN
parameter rxfifo_pempty = "pempty_default";	//Valid values:   //chestan:10g_rx
parameter rxfifo_pfull = "pfull_default";	//Valid values:   //chestan:10g_rx
parameter rxfifo_full = "full_default";	//Valid values:  //chestan:10g_rx
parameter rxfifo_empty = "pempty_default";	//Valid values:  //chestan:10g_rx
parameter bitslip_mode = "bitslip_dis";	//Valid values: BITSLIP_DIS|BITSLIP_EN
parameter fast_path = "fast_path_dis";	//Valid values: FAST_PATH_DIS|FAST_PATH_EN
parameter stretch_num_stages = "zero_stage";	//Valid values: ZERO_STAGE|ONE_STAGE|TWO_STAGE|THREE_STAGE
parameter stretch_en = "stretch_en";	//Valid values: STRETCH_EN|STRETCH_DIS
parameter iqtxrx_clkout_sel = "iq_rx_clk_out";	//Valid values: IQ_RX_CLK_OUT|IQ_RX_PMA_CLK_DIV33
//chestan: manually added accoding to atmgen
parameter channel_number =  0 ;
parameter frmgen_diag_word =  064'b0000000000000000011001000000000000000000000000000000000000000000 ;
parameter frmgen_scrm_word =  064'b0000000000000000001010000000000000000000000000000000000000000000 ;
parameter frmgen_skip_word =  064'b0000000000000000000111100001111000011110000111100001111000011110 ;
parameter frmgen_sync_word =  064'b0000000000000000011110001111011001111000111101100111100011110110 ;
parameter test_bus_mode = "tx";

//input and output port declaration
output [ 5:0 ] bercount;
output [ 7:0 ] errorblockcount;
output [ 0:0 ] pcsstatus;
output [ 15:0 ] randomerrorcount;
output [ 0:0 ] prbserrorlatch;
input [ 0:0 ] txpmaclk;
input [ 0:0 ] rxpmaclk;
input [ 0:0 ] pmaclkdiv33txorrx;
input [ 0:0 ] rxpmadatavalid;
input [ 0:0 ] hardresetn;
input [ 0:0 ] rxpldclk;
input [ 0:0 ] rxpldrstn;
input [ 0:0 ] refclkdig;
input [ 0:0 ] rxalignen;
input [ 0:0 ] rxalignclr;
input [ 0:0 ] rxrden;
input [ 0:0 ] rxdisparityclr;
input [ 0:0 ] rxclrerrorblockcount;
input [ 0:0 ] rxclrbercount;
input [ 0:0 ] rxbitslip;
input [ 0:0 ] rxprbserrorclr;
output [ 0:0 ] rxclkout;
output [ 0:0 ] rxclkiqout;
output [ 0:0 ] rxdatavalid;
output [ 0:0 ] rxfifoempty;
output [ 0:0 ] rxfifopartialempty;
output [ 0:0 ] rxfifopartialfull;
output [ 0:0 ] rxfifofull;
output [ 0:0 ] rxalignval;
output [ 0:0 ] rxblocklock;
output [ 0:0 ] rxsyncheadererror;
output [ 0:0 ] rxhighber;
output [ 0:0 ] rxframelock;
output [ 0:0 ] rxrdpossts;
output [ 0:0 ] rxrdnegsts;
output [ 0:0 ] rxskipinserted;
output [ 0:0 ] rxrxframe;
output [ 0:0 ] rxpayloadinserted;
output [ 0:0 ] rxsyncworderror;
output [ 0:0 ] rxscramblererror;
output [ 0:0 ] rxskipworderror;
output [ 0:0 ] rxdiagnosticerror;
output [ 0:0 ] rxmetaframeerror;
output [ 0:0 ] rxcrc32error;
output [ 1:0 ] rxdiagnosticstatus;
output [ 63:0 ] rxdata;
output [ 9:0 ] rxcontrol;
output [ 8:0 ] accumdisparity;
input [ 39:0 ] loopbackdatain;
input [ 39:0 ] rxpmadata;
output [ 19:0 ] rxtestdata;
output [ 0:0 ] syncdatain;

stratixv_hssi_10g_rx_pcs_encrypted inst (
.bercount(bercount),
.errorblockcount(errorblockcount),
.pcsstatus(pcsstatus),
.randomerrorcount(randomerrorcount),
.prbserrorlatch(prbserrorlatch),
.txpmaclk(txpmaclk),
.rxpmaclk(rxpmaclk),
.pmaclkdiv33txorrx(pmaclkdiv33txorrx),
.rxpmadatavalid(rxpmadatavalid),
.hardresetn(hardresetn),
.rxpldclk(rxpldclk),
.rxpldrstn(rxpldrstn),
.refclkdig(refclkdig),
.rxalignen(rxalignen),
.rxalignclr(rxalignclr),
.rxrden(rxrden),
.rxdisparityclr(rxdisparityclr),
.rxclrerrorblockcount(rxclrerrorblockcount),
.rxclrbercount(rxclrbercount),
.rxbitslip(rxbitslip),
.rxprbserrorclr(rxprbserrorclr),
.rxclkout(rxclkout),
.rxclkiqout(rxclkiqout),
.rxdatavalid(rxdatavalid),
.rxfifoempty(rxfifoempty),
.rxfifopartialempty(rxfifopartialempty),
.rxfifopartialfull(rxfifopartialfull),
.rxfifofull(rxfifofull),
.rxalignval(rxalignval),
.rxblocklock(rxblocklock),
.rxsyncheadererror(rxsyncheadererror),
.rxhighber(rxhighber),
.rxframelock(rxframelock),
.rxrdpossts(rxrdpossts),
.rxrdnegsts(rxrdnegsts),
.rxskipinserted(rxskipinserted),
.rxrxframe(rxrxframe),
.rxpayloadinserted(rxpayloadinserted),
.rxsyncworderror(rxsyncworderror),
.rxscramblererror(rxscramblererror),
.rxskipworderror(rxskipworderror),
.rxdiagnosticerror(rxdiagnosticerror),
.rxmetaframeerror(rxmetaframeerror),
.rxcrc32error(rxcrc32error),
.rxdiagnosticstatus(rxdiagnosticstatus),
.rxdata(rxdata),
.rxcontrol(rxcontrol),
.accumdisparity(accumdisparity),
.loopbackdatain(loopbackdatain),
.rxpmadata(rxpmadata),
.rxtestdata(rxtestdata),
.syncdatain(syncdatain)
);

defparam inst.prot_mode = prot_mode;
defparam inst.sup_mode = sup_mode;
defparam inst.dis_signal_ok = dis_signal_ok;
defparam inst.gb_rx_idwidth = gb_rx_idwidth;
defparam inst.gb_rx_odwidth = gb_rx_odwidth;
defparam inst.bit_reverse = bit_reverse;
defparam inst.gb_sel_mode = gb_sel_mode;
defparam inst.lpbk_mode = lpbk_mode;
defparam inst.test_mode = test_mode;
defparam inst.blksync_bypass = blksync_bypass;
defparam inst.blksync_pipeln = blksync_pipeln;
defparam inst.blksync_knum_sh_cnt_prelock = blksync_knum_sh_cnt_prelock;
defparam inst.blksync_knum_sh_cnt_postlock = blksync_knum_sh_cnt_postlock;
defparam inst.blksync_enum_invalid_sh_cnt = blksync_enum_invalid_sh_cnt;
defparam inst.blksync_bitslip_wait_cnt = blksync_bitslip_wait_cnt;
defparam inst.bitslip_wait_cnt_user = bitslip_wait_cnt_user;
defparam inst.blksync_bitslip_type = blksync_bitslip_type;
defparam inst.blksync_bitslip_wait_type = blksync_bitslip_wait_type;
defparam inst.dispchk_bypass = dispchk_bypass;
defparam inst.dispchk_rd_level = dispchk_rd_level;
defparam inst.dispchk_rd_level_user = dispchk_rd_level_user;
defparam inst.dispchk_pipeln = dispchk_pipeln;
defparam inst.descrm_bypass = descrm_bypass;
defparam inst.descrm_mode = descrm_mode;
defparam inst.frmsync_bypass = frmsync_bypass;
defparam inst.frmsync_pipeln = frmsync_pipeln;
defparam inst.frmsync_mfrm_length = frmsync_mfrm_length;
defparam inst.frmsync_mfrm_length_user = frmsync_mfrm_length_user;
defparam inst.frmsync_knum_sync = frmsync_knum_sync;
defparam inst.frmsync_enum_sync = frmsync_enum_sync;
defparam inst.frmsync_enum_scrm = frmsync_enum_scrm;
defparam inst.frmsync_flag_type = frmsync_flag_type;
defparam inst.dec_64b66b_10g_mode = dec_64b66b_10g_mode;
defparam inst.dec_64b66b_rxsm_bypass = dec_64b66b_rxsm_bypass;
defparam inst.rx_sm_bypass = rx_sm_bypass;
defparam inst.rx_sm_pipeln = rx_sm_pipeln;
defparam inst.rx_sm_hiber = rx_sm_hiber;
defparam inst.ber_xus_timer_window = ber_xus_timer_window;
defparam inst.ber_bit_err_total_cnt = ber_bit_err_total_cnt;
defparam inst.crcchk_bypass = crcchk_bypass;
defparam inst.crcchk_pipeln = crcchk_pipeln;
defparam inst.crcflag_pipeln = crcflag_pipeln;
defparam inst.crcchk_init = crcchk_init;
defparam inst.crcchk_init_user = crcchk_init_user;
defparam inst.crcchk_inv = crcchk_inv;
defparam inst.force_align = force_align;
defparam inst.align_del = align_del;
defparam inst.control_del = control_del;
defparam inst.rxfifo_mode = rxfifo_mode;
defparam inst.master_clk_sel = master_clk_sel;
defparam inst.rd_clk_sel = rd_clk_sel;
defparam inst.gbexp_clken = gbexp_clken;
defparam inst.prbs_clken = prbs_clken;
defparam inst.blksync_clken = blksync_clken;
defparam inst.dispchk_clken = dispchk_clken;
defparam inst.descrm_clken = descrm_clken;
defparam inst.frmsync_clken = frmsync_clken;
defparam inst.dec64b66b_clken = dec64b66b_clken;
defparam inst.ber_clken = ber_clken;
defparam inst.rand_clken = rand_clken;
defparam inst.crcchk_clken = crcchk_clken;
defparam inst.wrfifo_clken = wrfifo_clken;
defparam inst.rdfifo_clken = rdfifo_clken;
defparam inst.rxfifo_pempty = rxfifo_pempty;
defparam inst.rxfifo_pfull = rxfifo_pfull;
defparam inst.rxfifo_full = rxfifo_full;
defparam inst.rxfifo_empty = rxfifo_empty;
defparam inst.bitslip_mode = bitslip_mode;
defparam inst.fast_path = fast_path;
defparam inst.stretch_num_stages = stretch_num_stages;
defparam inst.stretch_en = stretch_en;
defparam inst.iqtxrx_clkout_sel = iqtxrx_clkout_sel;
defparam inst.frmgen_diag_word = frmgen_diag_word;
defparam inst.frmgen_scrm_word = frmgen_scrm_word;
defparam inst.frmgen_skip_word = frmgen_skip_word;
defparam inst.frmgen_sync_word = frmgen_sync_word;
defparam inst.test_bus_mode = test_bus_mode;
defparam inst.channel_number = channel_number;

endmodule	//stratixv_hssi_10g_rx_pcs

`timescale 1 ps/1 ps

module stratixv_hssi_10g_tx_pcs
(
	txpmaclk,
	pmaclkdiv33lc,
	hardresetn,
	txpldclk,
	txpldrstn,
	refclkdig,
	txdatavalid,
	txbitslip,
	txdiagnosticstatus,
	txwordslip,
	txbursten,
	txdisparityclr,
	txclkout,
	txclkiqout,
	txfifoempty,
	txfifopartialempty,
	txfifopartialfull,
	txfifofull,
	txframe,
	txburstenexe,
	txwordslipexe,
	distupindv,
	distdwnindv,
	distupinwren,
	distdwninwren,
	distupinrden,
	distdwninrden,
	distupoutdv,
	distdwnoutdv,
	distupoutwren,
	distdwnoutwren,
	distupoutrden,
	distdwnoutrden,
	txtestdata,
	txdata,
	txcontrol,
	loopbackdataout,
	txpmadata,
	syncdatain
); 

// parameter declaration and default value assignemnt
parameter prot_mode = "disable_mode";	//Valid values: DISABLE_MODE|TENG_BASER_MODE|INTERLAKEN_MODE|SFIS_MODE|TENG_SDI_MODE|BASIC_MODE|TEST_PRBS_MODE|TEST_PRP_MODE|TEST_RPG_MODE
parameter sup_mode = "full_mode";	//Valid values: FULL_MODE|STRETCH_MODE|ENGR_MODE
parameter ctrl_plane_bonding = "individual";	//Valid values: INDIVIDUAL|CTRL_MASTER|CTRL_SLAVE_ABV|CTRL_SLAVE_BLW
parameter master_clk_sel = "master_tx_pma_clk";	//Valid values: MASTER_TX_PMA_CLK|MASTER_REFCLK_DIG
parameter wr_clk_sel = "wr_tx_pma_clk";	//Valid values: WR_TX_PLD_CLK|WR_TX_PMA_CLK|WR_REFCLK_DIG
parameter wrfifo_clken = "wrfifo_clk_dis";	//Valid values: WRFIFO_CLK_DIS|WRFIFO_CLK_EN
parameter rdfifo_clken = "rdfifo_clk_dis";	//Valid values: RDFIFO_CLK_DIS|RDFIFO_CLK_EN
parameter frmgen_clken = "frmgen_clk_dis";	//Valid values: FRMGEN_CLK_DIS|FRMGEN_CLK_EN
parameter crcgen_clken = "crcgen_clk_dis";	//Valid values: CRCGEN_CLK_DIS|CRCGEN_CLK_EN
parameter enc64b66b_txsm_clken = "enc64b66b_txsm_clk_dis";	//Valid values: ENC64B66B_TXSM_CLK_DIS|ENC64B66B_TXSM_CLK_EN
parameter scrm_clken = "scrm_clk_dis";	//Valid values: SCRM_CLK_DIS|SCRM_CLK_EN
parameter dispgen_clken = "dispgen_clk_dis";	//Valid values: DISPGEN_CLK_DIS|DISPGEN_CLK_EN
parameter prbs_clken = "prbs_clk_dis";	//Valid values: PRBS_CLK_DIS|PRBS_CLK_EN
parameter sqwgen_clken = "sqwgen_clk_dis";	//Valid values: SQWGEN_CLK_DIS|SQWGEN_CLK_EN
parameter gbred_clken = "gbred_clk_dis";	//Valid values: GBRED_CLK_DIS|GBRED_CLK_EN
parameter gb_tx_idwidth = "idwidth_50";	//Valid values: IDWIDTH_32|IDWIDTH_40|IDWIDTH_50|IDWIDTH_67|IDWIDTH_64|IDWIDTH_66
parameter gb_tx_odwidth = "odwidth_32";	//Valid values: ODWIDTH_40|ODWIDTH_32|ODWIDTH_20
parameter txfifo_mode = "phase_comp";	//Valid values: PHASE_COMP|CLK_COMP|REGISTER_MODE|GENERIC
parameter txfifo_pempty = "pempty_default";	//Valid values: 
parameter txfifo_pfull = "pfull_default";	//Valid values: 
parameter txfifo_empty = "empty_default";	//Valid values: 
parameter txfifo_full = "full_default";	//Valid values: 
parameter frmgen_bypass = "frmgen_bypass_dis";	//Valid values: FRMGEN_BYPASS_DIS|FRMGEN_BYPASS_EN
parameter frmgen_pipeln = "frmgen_pipeln_dis";	//Valid values: FRMGEN_PIPELN_DIS|FRMGEN_PIPELN_EN
parameter frmgen_mfrm_length = "frmgen_mfrm_length_min";	//Valid values: FRMGEN_MFRM_LENGTH_MIN|FRMGEN_MFRM_LENGTH_MAX|FRMGEN_MFRM_LENGTH_USER_SETTING
parameter frmgen_mfrm_length_user = "int";	//Valid values: 5
parameter frmgen_pyld_ins = "frmgen_pyld_ins_dis";	//Valid values: FRMGEN_PYLD_INS_DIS|FRMGEN_PYLD_INS_EN
parameter sh_err = "sh_err_dis";	//Valid values: SH_ERR_DIS|SH_ERR_EN
parameter frmgen_burst = "frmgen_burst_dis";	//Valid values: FRMGEN_BURST_DIS|FRMGEN_BURST_EN
parameter frmgen_wordslip = "frmgen_wordslip_dis";	//Valid values: FRMGEN_WORDSLIP_DIS|FRMGEN_WORDSLIP_EN
parameter crcgen_bypass = "crcgen_bypass_dis";	//Valid values: CRCGEN_BYPASS_DIS|CRCGEN_BYPASS_EN
parameter crcgen_init = "crcgen_init_user_setting";	//Valid values: CRCGEN_INIT_USER_SETTING
parameter crcgen_init_user = 32'b11111111111111111111111111111111;	//Valid values: 
parameter crcgen_inv = "crcgen_inv_dis";	//Valid values: CRCGEN_INV_DIS|CRCGEN_INV_EN
parameter crcgen_err = "crcgen_err_dis";	//Valid values: CRCGEN_ERR_DIS|CRCGEN_ERR_EN
parameter enc_64b66b_10g_mode = "enc_64b66b_10g_mode_en";	//Valid values: ENC_64B66B_10G_MODE_EN|ENC_64B66B_10G_MODE_DIS
parameter enc_64b66b_txsm_bypass = "enc_64b66b_txsm_bypass_dis";	//Valid values: ENC_64B66B_TXSM_BYPASS_DIS|ENC_64B66B_TXSM_BYPASS_EN
parameter tx_sm_bypass = "tx_sm_bypass_dis";	//Valid values: TX_SM_BYPASS_DIS|TX_SM_BYPASS_EN
parameter tx_sm_pipeln = "tx_sm_pipeln_dis";	//Valid values: TX_SM_PIPELN_DIS|TX_SM_PIPELN_EN
parameter scrm_bypass = "scrm_bypass_dis";	//Valid values: SCRM_BYPASS_DIS|SCRM_BYPASS_EN
parameter test_mode = "test_off";	//Valid values: TEST_OFF|PSEUDO_RANDOM|SQ_WAVE|PRBS_31|PRBS_23|PRBS_9|PRBS_7
parameter pseudo_random = "all_0";	//Valid values: ALL_0|TWO_LF
parameter pseudo_seed_a = "pseudo_seed_a_user_setting";	//Valid values: PSEUDO_SEED_A_USER_SETTING
parameter pseudo_seed_a_user = 58'b1111111111111111111111111111111111111111111111111111111111;	//Valid values: 
parameter pseudo_seed_b = "pseudo_seed_b_user_setting";	//Valid values: PSEUDO_SEED_B_USER_SETTING
parameter pseudo_seed_b_user = 58'b1111111111111111111111111111111111111111111111111111111111;	//Valid values: 
parameter bit_reverse = "bit_reverse_dis";	//Valid values: BIT_REVERSE_DIS|BIT_REVERSE_EN
parameter scrm_seed = "scram_seed_user_setting";	//Valid values: SCRAM_SEED_MIN|SCRAM_SEED_MAX|SCRAM_SEED_USER_SETTING
parameter scrm_seed_user = 58'b1111111111111111111111111111111111111111111111111111111111;	//Valid values: 58
parameter scrm_mode = "async";	//Valid values: ASYNC|SYNC
parameter dispgen_bypass = "dispgen_bypass_dis";	//Valid values: DISPGEN_BYPASS_DIS|DISPGEN_BYPASS_EN
parameter dispgen_err = "dispgen_err_dis";	//Valid values: DISPGEN_ERR_DIS|DISPGEN_ERR_EN
parameter dispgen_pipeln = "dispgen_pipeln_dis";	//Valid values: DISPGEN_PIPELN_DIS|DISPGEN_PIPELN_EN
parameter gb_sel_mode = "internal";	//Valid values: INTERNAL|EXTERNAL
parameter sq_wave = "sq_wave_4";	//Valid values: SQ_WAVE_4|SQ_WAVE_5|SQ_WAVE_6|SQ_WAVE_8|SQ_WAVE_10
parameter bitslip_en = "bitslip_dis";	//Valid values: BITSLIP_DIS|BITSLIP_EN
parameter fastpath = "fastpath_dis";	//Valid values: FASTPATH_DIS|FASTPATH_EN
parameter distup_bypass_pipeln = "distup_bypass_pipeln_dis";	//Valid values: DISTUP_BYPASS_PIPELN_DIS|DISTUP_BYPASS_PIPELN_EN
parameter distup_master = "distup_master_en";	//Valid values: DISTUP_MASTER_EN|DISTUP_MASTER_DIS
parameter distdwn_bypass_pipeln = "distdwn_bypass_pipeln_dis";	//Valid values: DISTDWN_BYPASS_PIPELN_DIS|DISTDWN_BYPASS_PIPELN_EN
parameter distdwn_master = "distdwn_master_en";	//Valid values: DISTDWN_MASTER_EN|DISTDWN_MASTER_DIS
parameter compin_sel = "compin_master";	//Valid values: COMPIN_MASTER|COMPIN_SLAVE_TOP|COMPIN_SLAVE_BOT|COMPIN_DEFAULT
parameter comp_cnt = "comp_cnt_00";	//Valid values: COMP_CNT_00|COMP_CNT_02|COMP_CNT_04|COMP_CNT_06|COMP_CNT_08|COMP_CNT_0A|COMP_CNT_0C|COMP_CNT_0E|COMP_CNT_10|COMP_CNT_12|COMP_CNT_14|COMP_CNT_16|COMP_CNT_18
parameter indv = "indv_en";	//Valid values: INDV_EN|INDV_DIS
parameter stretch_num_stages = "zero_stage";	//Valid values: ZERO_STAGE|ONE_STAGE|TWO_STAGE|THREE_STAGE
parameter stretch_en = "stretch_en";	//Valid values: STRETCH_EN|STRETCH_DIS
parameter iqtxrx_clkout_sel = "iq_tx_pma_clk";	//Valid values: IQ_TX_PMA_CLK|IQ_TX_PMA_CLK_DIV33

//manually added parameter
parameter channel_number = 0;
parameter frmgen_sync_word = 64'h78F678F678F6;
parameter frmgen_scrm_word = 64'h280000000000;
parameter frmgen_skip_word = 64'h1E1E1E1E1E1E;
parameter frmgen_diag_word = 64'h640000000000;
parameter test_bus_mode = "tx";

parameter lpm_type = "stratixv_hssi_10g_tx_pcs"; //added accoding to atmgen



//input and output port declaration
input [ 0:0 ] txpmaclk;
input [ 0:0 ] pmaclkdiv33lc;
input [ 0:0 ] hardresetn;
input [ 0:0 ] txpldclk;
input [ 0:0 ] txpldrstn;
input [ 0:0 ] refclkdig;
input [ 0:0 ] txdatavalid;
input [ 6:0 ] txbitslip;
input [ 1:0 ] txdiagnosticstatus;
input [ 0:0 ] txwordslip;
input [ 0:0 ] txbursten;
input [ 0:0 ] txdisparityclr;
output [ 0:0 ] txclkout;
output [ 0:0 ] txclkiqout;
output [ 0:0 ] txfifoempty;
output [ 0:0 ] txfifopartialempty;
output [ 0:0 ] txfifopartialfull;
output [ 0:0 ] txfifofull;
output [ 0:0 ] txframe;
output [ 0:0 ] txburstenexe;
output [ 0:0 ] txwordslipexe;
input [ 0:0 ] distupindv;
input [ 0:0 ] distdwnindv;
input [ 0:0 ] distupinwren;
input [ 0:0 ] distdwninwren;
input [ 0:0 ] distupinrden;
input [ 0:0 ] distdwninrden;
output [ 0:0 ] distupoutdv;
output [ 0:0 ] distdwnoutdv;
output [ 0:0 ] distupoutwren;
output [ 0:0 ] distdwnoutwren;
output [ 0:0 ] distupoutrden;
output [ 0:0 ] distdwnoutrden;
output [ 19:0 ] txtestdata;
input [ 63:0 ] txdata;
input [ 8:0 ] txcontrol;
output [ 39:0 ] loopbackdataout;
output [ 39:0 ] txpmadata;
output [ 0:0 ] syncdatain;

stratixv_hssi_10g_tx_pcs_encrypted inst (
.txpmaclk(txpmaclk),
.pmaclkdiv33lc(pmaclkdiv33lc),
.hardresetn(hardresetn),
.txpldclk(txpldclk),
.txpldrstn(txpldrstn),
.refclkdig(refclkdig),
.txdatavalid(txdatavalid),
.txbitslip(txbitslip),
.txdiagnosticstatus(txdiagnosticstatus),
.txwordslip(txwordslip),
.txbursten(txbursten),
.txdisparityclr(txdisparityclr),
.txclkout(txclkout),
.txclkiqout(txclkiqout),
.txfifoempty(txfifoempty),
.txfifopartialempty(txfifopartialempty),
.txfifopartialfull(txfifopartialfull),
.txfifofull(txfifofull),
.txframe(txframe),
.txburstenexe(txburstenexe),
.txwordslipexe(txwordslipexe),
.distupindv(distupindv),
.distdwnindv(distdwnindv),
.distupinwren(distupinwren),
.distdwninwren(distdwninwren),
.distupinrden(distupinrden),
.distdwninrden(distdwninrden),
.distupoutdv(distupoutdv),
.distdwnoutdv(distdwnoutdv),
.distupoutwren(distupoutwren),
.distdwnoutwren(distdwnoutwren),
.distupoutrden(distupoutrden),
.distdwnoutrden(distdwnoutrden),
.txtestdata(txtestdata),
.txdata(txdata),
.txcontrol(txcontrol),
.loopbackdataout(loopbackdataout),
.txpmadata(txpmadata),
.syncdatain(syncdatain)
);

defparam inst.prot_mode = prot_mode;
defparam inst.sup_mode = sup_mode;
defparam inst.ctrl_plane_bonding = ctrl_plane_bonding;
defparam inst.master_clk_sel = master_clk_sel;
defparam inst.wr_clk_sel = wr_clk_sel;
defparam inst.wrfifo_clken = wrfifo_clken;
defparam inst.rdfifo_clken = rdfifo_clken;
defparam inst.frmgen_clken = frmgen_clken;
defparam inst.crcgen_clken = crcgen_clken;
defparam inst.enc64b66b_txsm_clken = enc64b66b_txsm_clken;
defparam inst.scrm_clken = scrm_clken;
defparam inst.dispgen_clken = dispgen_clken;
defparam inst.prbs_clken = prbs_clken;
defparam inst.sqwgen_clken = sqwgen_clken;
defparam inst.gbred_clken = gbred_clken;
defparam inst.gb_tx_idwidth = gb_tx_idwidth;
defparam inst.gb_tx_odwidth = gb_tx_odwidth;
defparam inst.txfifo_mode = txfifo_mode;
defparam inst.txfifo_pempty = txfifo_pempty;
defparam inst.txfifo_pfull = txfifo_pfull;
defparam inst.txfifo_empty = txfifo_empty;
defparam inst.txfifo_full = txfifo_full;
defparam inst.frmgen_bypass = frmgen_bypass;
defparam inst.frmgen_pipeln = frmgen_pipeln;
defparam inst.frmgen_mfrm_length = frmgen_mfrm_length;
defparam inst.frmgen_mfrm_length_user = frmgen_mfrm_length_user;
defparam inst.frmgen_pyld_ins = frmgen_pyld_ins;
defparam inst.sh_err = sh_err;
defparam inst.frmgen_burst = frmgen_burst;
defparam inst.frmgen_wordslip = frmgen_wordslip;
defparam inst.crcgen_bypass = crcgen_bypass;
defparam inst.crcgen_init = crcgen_init;
defparam inst.crcgen_init_user = crcgen_init_user;
defparam inst.crcgen_inv = crcgen_inv;
defparam inst.crcgen_err = crcgen_err;
defparam inst.enc_64b66b_10g_mode = enc_64b66b_10g_mode;
defparam inst.enc_64b66b_txsm_bypass = enc_64b66b_txsm_bypass;
defparam inst.tx_sm_bypass = tx_sm_bypass;
defparam inst.tx_sm_pipeln = tx_sm_pipeln;
defparam inst.scrm_bypass = scrm_bypass;
defparam inst.test_mode = test_mode;
defparam inst.pseudo_random = pseudo_random;
defparam inst.pseudo_seed_a = pseudo_seed_a;
defparam inst.pseudo_seed_a_user = pseudo_seed_a_user;
defparam inst.pseudo_seed_b = pseudo_seed_b;
defparam inst.pseudo_seed_b_user = pseudo_seed_b_user;
defparam inst.bit_reverse = bit_reverse;
defparam inst.scrm_seed = scrm_seed;
defparam inst.scrm_seed_user = scrm_seed_user;
defparam inst.scrm_mode = scrm_mode;
defparam inst.dispgen_bypass = dispgen_bypass;
defparam inst.dispgen_err = dispgen_err;
defparam inst.dispgen_pipeln = dispgen_pipeln;
defparam inst.gb_sel_mode = gb_sel_mode;
defparam inst.sq_wave = sq_wave;
defparam inst.bitslip_en = bitslip_en;
defparam inst.fastpath = fastpath;
defparam inst.distup_bypass_pipeln = distup_bypass_pipeln;
defparam inst.distup_master = distup_master;
defparam inst.distdwn_bypass_pipeln = distdwn_bypass_pipeln;
defparam inst.distdwn_master = distdwn_master;
defparam inst.compin_sel = compin_sel;
defparam inst.comp_cnt = comp_cnt;
defparam inst.indv = indv;
defparam inst.stretch_num_stages = stretch_num_stages;
defparam inst.stretch_en = stretch_en;
defparam inst.iqtxrx_clkout_sel = iqtxrx_clkout_sel;
defparam inst.frmgen_sync_word = frmgen_sync_word;
defparam inst.frmgen_scrm_word = frmgen_scrm_word;
defparam inst.frmgen_skip_word = frmgen_skip_word;
defparam inst.frmgen_diag_word = frmgen_diag_word;
defparam inst.test_bus_mode = test_bus_mode;
defparam inst.lpm_type = lpm_type;
defparam inst.channel_number = channel_number;

endmodule	//stratixv_hssi_10g_tx_pcs

`timescale 1 ps/1 ps

module    stratixv_hssi_8g_pcs_aggregate    (
    refclkdig,
    scanmoden,
    scanshiftn,
    txpmaclk,
    rcvdclkch0,
    rcvdclkch1,
    hardrst,
    txpcsrst,
    rxpcsrst,
    dprioagg,
    rcvdclkout,
    rcvdclkouttop,
    rcvdclkoutbot,
    rdenablesynctopch1,
    txdatatctopch1,
    txctltctopch1,
    syncstatustopch1,
    rdaligntopch1,
    aligndetsynctopch1,
    fifordintopch1,
    alignstatussynctopch1,
    cgcomprddintopch1,
    cgcompwrintopch1,
    delcondmetintopch1,
    fifoovrintopch1,
    latencycompintopch1,
    insertincompleteintopch1,
    decdatatopch1,
    decctltopch1,
    decdatavalidtopch1,
    runningdisptopch1,
    txdatatstopch1,
    txctltstopch1,
    fiforstrdqdtopch1,
    endskwqdtopch1,
    endskwrdptrstopch1,
    alignstatustopch1,
    alignstatussync0topch1,
    fifordoutcomp0topch1,
    cgcomprddalltopch1,
    cgcompwralltopch1,
    delcondmet0topch1,
    insertincomplete0topch1,
    fifoovr0topch1,
    latencycomp0topch1,
    rxdatarstopch1,
    rxctlrstopch1,
    rdenablesynctopch0,
    txdatatctopch0,
    txctltctopch0,
    syncstatustopch0,
    rdaligntopch0,
    aligndetsynctopch0,
    fifordintopch0,
    alignstatussynctopch0,
    cgcomprddintopch0,
    cgcompwrintopch0,
    delcondmetintopch0,
    fifoovrintopch0,
    latencycompintopch0,
    insertincompleteintopch0,
    decdatatopch0,
    decctltopch0,
    decdatavalidtopch0,
    runningdisptopch0,
    txdatatstopch0,
    txctltstopch0,
    fiforstrdqdtopch0,
    endskwqdtopch0,
    endskwrdptrstopch0,
    alignstatustopch0,
    alignstatussync0topch0,
    fifordoutcomp0topch0,
    cgcomprddalltopch0,
    cgcompwralltopch0,
    delcondmet0topch0,
    insertincomplete0topch0,
    fifoovr0topch0,
    latencycomp0topch0,
    rxdatarstopch0,
    rxctlrstopch0,
    rdenablesyncch2,
    txdatatcch2,
    txctltcch2,
    syncstatusch2,
    rdalignch2,
    aligndetsyncch2,
    fifordinch2,
    alignstatussyncch2,
    cgcomprddinch2,
    cgcompwrinch2,
    delcondmetinch2,
    fifoovrinch2,
    latencycompinch2,
    insertincompleteinch2,
    decdatach2,
    decctlch2,
    decdatavalidch2,
    runningdispch2,
    txdatatsch2,
    txctltsch2,
    fiforstrdqdch2,
    endskwqdch2,
    endskwrdptrsch2,
    alignstatusch2,
    alignstatussync0ch2,
    fifordoutcomp0ch2,
    cgcomprddallch2,
    cgcompwrallch2,
    delcondmet0ch2,
    insertincomplete0ch2,
    fifoovr0ch2,
    latencycomp0ch2,
    rxdatarsch2,
    rxctlrsch2,
    rdenablesyncch1,
    txdatatcch1,
    txctltcch1,
    syncstatusch1,
    rdalignch1,
    aligndetsyncch1,
    fifordinch1,
    alignstatussyncch1,
    cgcomprddinch1,
    cgcompwrinch1,
    delcondmetinch1,
    fifoovrinch1,
    latencycompinch1,
    insertincompleteinch1,
    decdatach1,
    decctlch1,
    decdatavalidch1,
    runningdispch1,
    txdatatsch1,
    txctltsch1,
    fiforstrdqdch1,
    endskwqdch1,
    endskwrdptrsch1,
    alignstatusch1,
    alignstatussync0ch1,
    fifordoutcomp0ch1,
    cgcomprddallch1,
    cgcompwrallch1,
    delcondmet0ch1,
    insertincomplete0ch1,
    fifoovr0ch1,
    latencycomp0ch1,
    rxdatarsch1,
    rxctlrsch1,
    rdenablesyncch0,
    txdatatcch0,
    txctltcch0,
    syncstatusch0,
    rdalignch0,
    aligndetsyncch0,
    fifordinch0,
    alignstatussyncch0,
    cgcomprddinch0,
    cgcompwrinch0,
    delcondmetinch0,
    fifoovrinch0,
    latencycompinch0,
    insertincompleteinch0,
    decdatach0,
    decctlch0,
    decdatavalidch0,
    runningdispch0,
    txdatatsch0,
    txctltsch0,
    fiforstrdqdch0,
    endskwqdch0,
    endskwrdptrsch0,
    alignstatusch0,
    alignstatussync0ch0,
    fifordoutcomp0ch0,
    cgcomprddallch0,
    cgcompwrallch0,
    delcondmet0ch0,
    insertincomplete0ch0,
    fifoovr0ch0,
    latencycomp0ch0,
    rxdatarsch0,
    rxctlrsch0,
    rdenablesyncbotch2,
    txdatatcbotch2,
    txctltcbotch2,
    syncstatusbotch2,
    rdalignbotch2,
    aligndetsyncbotch2,
    fifordinbotch2,
    alignstatussyncbotch2,
    cgcomprddinbotch2,
    cgcompwrinbotch2,
    delcondmetinbotch2,
    fifoovrinbotch2,
    latencycompinbotch2,
    insertincompleteinbotch2,
    decdatabotch2,
    decctlbotch2,
    decdatavalidbotch2,
    runningdispbotch2,
    txdatatsbotch2,
    txctltsbotch2,
    fiforstrdqdbotch2,
    endskwqdbotch2,
    endskwrdptrsbotch2,
    alignstatusbotch2,
    alignstatussync0botch2,
    fifordoutcomp0botch2,
    cgcomprddallbotch2,
    cgcompwrallbotch2,
    delcondmet0botch2,
    insertincomplete0botch2,
    fifoovr0botch2,
    latencycomp0botch2,
    rxdatarsbotch2,
    rxctlrsbotch2);

    parameter    xaui_sm_operation    =    "en_xaui_sm";
    parameter    dskw_sm_operation    =    "dskw_xaui_sm";
    parameter    data_agg_bonding    =    "agg_disable";
    parameter    prot_mode_tx    =    "pipe_g1_tx";
    parameter    pcs_dw_datapath    =    "sw_data_path";
    parameter    dskw_control    =    "dskw_write_control";
    parameter    refclkdig_sel    =    "dis_refclk_dig_sel";


    input    [0:0]    refclkdig;
    input    [0:0]    scanmoden;
    input    [0:0]    scanshiftn;
    input    [0:0]    txpmaclk;
    input    [0:0]    rcvdclkch0;
    input    [0:0]    rcvdclkch1;
    input    [0:0]    hardrst;
    input    [0:0]    txpcsrst;
    input    [0:0]    rxpcsrst;
    input    [63:0]    dprioagg;
    output    [0:0]    rcvdclkout;
    output    [0:0]    rcvdclkouttop;
    output    [0:0]    rcvdclkoutbot;
    input    [0:0]    rdenablesynctopch1;
    input    [7:0]    txdatatctopch1;
    input    [0:0]    txctltctopch1;
    input    [0:0]    syncstatustopch1;
    input    [1:0]    rdaligntopch1;
    input    [1:0]    aligndetsynctopch1;
    input    [0:0]    fifordintopch1;
    input    [0:0]    alignstatussynctopch1;
    input    [1:0]    cgcomprddintopch1;
    input    [1:0]    cgcompwrintopch1;
    input    [0:0]    delcondmetintopch1;
    input    [0:0]    fifoovrintopch1;
    input    [0:0]    latencycompintopch1;
    input    [0:0]    insertincompleteintopch1;
    input    [7:0]    decdatatopch1;
    input    [0:0]    decctltopch1;
    input    [0:0]    decdatavalidtopch1;
    input    [1:0]    runningdisptopch1;
    output    [7:0]    txdatatstopch1;
    output    [0:0]    txctltstopch1;
    output    [0:0]    fiforstrdqdtopch1;
    output    [0:0]    endskwqdtopch1;
    output    [0:0]    endskwrdptrstopch1;
    output    [0:0]    alignstatustopch1;
    output    [0:0]    alignstatussync0topch1;
    output    [0:0]    fifordoutcomp0topch1;
    output    [0:0]    cgcomprddalltopch1;
    output    [0:0]    cgcompwralltopch1;
    output    [0:0]    delcondmet0topch1;
    output    [0:0]    insertincomplete0topch1;
    output    [0:0]    fifoovr0topch1;
    output    [0:0]    latencycomp0topch1;
    output    [7:0]    rxdatarstopch1;
    output    [0:0]    rxctlrstopch1;
    input    [0:0]    rdenablesynctopch0;
    input    [7:0]    txdatatctopch0;
    input    [0:0]    txctltctopch0;
    input    [0:0]    syncstatustopch0;
    input    [1:0]    rdaligntopch0;
    input    [1:0]    aligndetsynctopch0;
    input    [0:0]    fifordintopch0;
    input    [0:0]    alignstatussynctopch0;
    input    [1:0]    cgcomprddintopch0;
    input    [1:0]    cgcompwrintopch0;
    input    [0:0]    delcondmetintopch0;
    input    [0:0]    fifoovrintopch0;
    input    [0:0]    latencycompintopch0;
    input    [0:0]    insertincompleteintopch0;
    input    [7:0]    decdatatopch0;
    input    [0:0]    decctltopch0;
    input    [0:0]    decdatavalidtopch0;
    input    [1:0]    runningdisptopch0;
    output    [7:0]    txdatatstopch0;
    output    [0:0]    txctltstopch0;
    output    [0:0]    fiforstrdqdtopch0;
    output    [0:0]    endskwqdtopch0;
    output    [0:0]    endskwrdptrstopch0;
    output    [0:0]    alignstatustopch0;
    output    [0:0]    alignstatussync0topch0;
    output    [0:0]    fifordoutcomp0topch0;
    output    [0:0]    cgcomprddalltopch0;
    output    [0:0]    cgcompwralltopch0;
    output    [0:0]    delcondmet0topch0;
    output    [0:0]    insertincomplete0topch0;
    output    [0:0]    fifoovr0topch0;
    output    [0:0]    latencycomp0topch0;
    output    [7:0]    rxdatarstopch0;
    output    [0:0]    rxctlrstopch0;
    input    [0:0]    rdenablesyncch2;
    input    [7:0]    txdatatcch2;
    input    [0:0]    txctltcch2;
    input    [0:0]    syncstatusch2;
    input    [1:0]    rdalignch2;
    input    [1:0]    aligndetsyncch2;
    input    [0:0]    fifordinch2;
    input    [0:0]    alignstatussyncch2;
    input    [1:0]    cgcomprddinch2;
    input    [1:0]    cgcompwrinch2;
    input    [0:0]    delcondmetinch2;
    input    [0:0]    fifoovrinch2;
    input    [0:0]    latencycompinch2;
    input    [0:0]    insertincompleteinch2;
    input    [7:0]    decdatach2;
    input    [0:0]    decctlch2;
    input    [0:0]    decdatavalidch2;
    input    [1:0]    runningdispch2;
    output    [7:0]    txdatatsch2;
    output    [0:0]    txctltsch2;
    output    [0:0]    fiforstrdqdch2;
    output    [0:0]    endskwqdch2;
    output    [0:0]    endskwrdptrsch2;
    output    [0:0]    alignstatusch2;
    output    [0:0]    alignstatussync0ch2;
    output    [0:0]    fifordoutcomp0ch2;
    output    [0:0]    cgcomprddallch2;
    output    [0:0]    cgcompwrallch2;
    output    [0:0]    delcondmet0ch2;
    output    [0:0]    insertincomplete0ch2;
    output    [0:0]    fifoovr0ch2;
    output    [0:0]    latencycomp0ch2;
    output    [7:0]    rxdatarsch2;
    output    [0:0]    rxctlrsch2;
    input    [0:0]    rdenablesyncch1;
    input    [7:0]    txdatatcch1;
    input    [0:0]    txctltcch1;
    input    [0:0]    syncstatusch1;
    input    [1:0]    rdalignch1;
    input    [1:0]    aligndetsyncch1;
    input    [0:0]    fifordinch1;
    input    [0:0]    alignstatussyncch1;
    input    [1:0]    cgcomprddinch1;
    input    [1:0]    cgcompwrinch1;
    input    [0:0]    delcondmetinch1;
    input    [0:0]    fifoovrinch1;
    input    [0:0]    latencycompinch1;
    input    [0:0]    insertincompleteinch1;
    input    [7:0]    decdatach1;
    input    [0:0]    decctlch1;
    input    [0:0]    decdatavalidch1;
    input    [1:0]    runningdispch1;
    output    [7:0]    txdatatsch1;
    output    [0:0]    txctltsch1;
    output    [0:0]    fiforstrdqdch1;
    output    [0:0]    endskwqdch1;
    output    [0:0]    endskwrdptrsch1;
    output    [0:0]    alignstatusch1;
    output    [0:0]    alignstatussync0ch1;
    output    [0:0]    fifordoutcomp0ch1;
    output    [0:0]    cgcomprddallch1;
    output    [0:0]    cgcompwrallch1;
    output    [0:0]    delcondmet0ch1;
    output    [0:0]    insertincomplete0ch1;
    output    [0:0]    fifoovr0ch1;
    output    [0:0]    latencycomp0ch1;
    output    [7:0]    rxdatarsch1;
    output    [0:0]    rxctlrsch1;
    input    [0:0]    rdenablesyncch0;
    input    [7:0]    txdatatcch0;
    input    [0:0]    txctltcch0;
    input    [0:0]    syncstatusch0;
    input    [1:0]    rdalignch0;
    input    [1:0]    aligndetsyncch0;
    input    [0:0]    fifordinch0;
    input    [0:0]    alignstatussyncch0;
    input    [1:0]    cgcomprddinch0;
    input    [1:0]    cgcompwrinch0;
    input    [0:0]    delcondmetinch0;
    input    [0:0]    fifoovrinch0;
    input    [0:0]    latencycompinch0;
    input    [0:0]    insertincompleteinch0;
    input    [7:0]    decdatach0;
    input    [0:0]    decctlch0;
    input    [0:0]    decdatavalidch0;
    input    [1:0]    runningdispch0;
    output    [7:0]    txdatatsch0;
    output    [0:0]    txctltsch0;
    output    [0:0]    fiforstrdqdch0;
    output    [0:0]    endskwqdch0;
    output    [0:0]    endskwrdptrsch0;
    output    [0:0]    alignstatusch0;
    output    [0:0]    alignstatussync0ch0;
    output    [0:0]    fifordoutcomp0ch0;
    output    [0:0]    cgcomprddallch0;
    output    [0:0]    cgcompwrallch0;
    output    [0:0]    delcondmet0ch0;
    output    [0:0]    insertincomplete0ch0;
    output    [0:0]    fifoovr0ch0;
    output    [0:0]    latencycomp0ch0;
    output    [7:0]    rxdatarsch0;
    output    [0:0]    rxctlrsch0;
    input    [0:0]    rdenablesyncbotch2;
    input    [7:0]    txdatatcbotch2;
    input    [0:0]    txctltcbotch2;
    input    [0:0]    syncstatusbotch2;
    input    [1:0]    rdalignbotch2;
    input    [1:0]    aligndetsyncbotch2;
    input    [0:0]    fifordinbotch2;
    input    [0:0]    alignstatussyncbotch2;
    input    [1:0]    cgcomprddinbotch2;
    input    [1:0]    cgcompwrinbotch2;
    input    [0:0]    delcondmetinbotch2;
    input    [0:0]    fifoovrinbotch2;
    input    [0:0]    latencycompinbotch2;
    input    [0:0]    insertincompleteinbotch2;
    input    [7:0]    decdatabotch2;
    input    [0:0]    decctlbotch2;
    input    [0:0]    decdatavalidbotch2;
    input    [1:0]    runningdispbotch2;
    output    [7:0]    txdatatsbotch2;
    output    [0:0]    txctltsbotch2;
    output    [0:0]    fiforstrdqdbotch2;
    output    [0:0]    endskwqdbotch2;
    output    [0:0]    endskwrdptrsbotch2;
    output    [0:0]    alignstatusbotch2;
    output    [0:0]    alignstatussync0botch2;
    output    [0:0]    fifordoutcomp0botch2;
    output    [0:0]    cgcomprddallbotch2;
    output    [0:0]    cgcompwrallbotch2;
    output    [0:0]    delcondmet0botch2;
    output    [0:0]    insertincomplete0botch2;
    output    [0:0]    fifoovr0botch2;
    output    [0:0]    latencycomp0botch2;
    output    [7:0]    rxdatarsbotch2;
    output    [0:0]    rxctlrsbotch2;

    stratixv_hssi_8g_pcs_aggregate_encrypted inst (
        .refclkdig(refclkdig),
        .scanmoden(scanmoden),
        .scanshiftn(scanshiftn),
        .txpmaclk(txpmaclk),
        .rcvdclkch0(rcvdclkch0),
        .rcvdclkch1(rcvdclkch1),
        .hardrst(hardrst),
        .txpcsrst(txpcsrst),
        .rxpcsrst(rxpcsrst),
        .dprioagg(dprioagg),
        .rcvdclkout(rcvdclkout),
        .rcvdclkouttop(rcvdclkouttop),
        .rcvdclkoutbot(rcvdclkoutbot),
        .rdenablesynctopch1(rdenablesynctopch1),
        .txdatatctopch1(txdatatctopch1),
        .txctltctopch1(txctltctopch1),
        .syncstatustopch1(syncstatustopch1),
        .rdaligntopch1(rdaligntopch1),
        .aligndetsynctopch1(aligndetsynctopch1),
        .fifordintopch1(fifordintopch1),
        .alignstatussynctopch1(alignstatussynctopch1),
        .cgcomprddintopch1(cgcomprddintopch1),
        .cgcompwrintopch1(cgcompwrintopch1),
        .delcondmetintopch1(delcondmetintopch1),
        .fifoovrintopch1(fifoovrintopch1),
        .latencycompintopch1(latencycompintopch1),
        .insertincompleteintopch1(insertincompleteintopch1),
        .decdatatopch1(decdatatopch1),
        .decctltopch1(decctltopch1),
        .decdatavalidtopch1(decdatavalidtopch1),
        .runningdisptopch1(runningdisptopch1),
        .txdatatstopch1(txdatatstopch1),
        .txctltstopch1(txctltstopch1),
        .fiforstrdqdtopch1(fiforstrdqdtopch1),
        .endskwqdtopch1(endskwqdtopch1),
        .endskwrdptrstopch1(endskwrdptrstopch1),
        .alignstatustopch1(alignstatustopch1),
        .alignstatussync0topch1(alignstatussync0topch1),
        .fifordoutcomp0topch1(fifordoutcomp0topch1),
        .cgcomprddalltopch1(cgcomprddalltopch1),
        .cgcompwralltopch1(cgcompwralltopch1),
        .delcondmet0topch1(delcondmet0topch1),
        .insertincomplete0topch1(insertincomplete0topch1),
        .fifoovr0topch1(fifoovr0topch1),
        .latencycomp0topch1(latencycomp0topch1),
        .rxdatarstopch1(rxdatarstopch1),
        .rxctlrstopch1(rxctlrstopch1),
        .rdenablesynctopch0(rdenablesynctopch0),
        .txdatatctopch0(txdatatctopch0),
        .txctltctopch0(txctltctopch0),
        .syncstatustopch0(syncstatustopch0),
        .rdaligntopch0(rdaligntopch0),
        .aligndetsynctopch0(aligndetsynctopch0),
        .fifordintopch0(fifordintopch0),
        .alignstatussynctopch0(alignstatussynctopch0),
        .cgcomprddintopch0(cgcomprddintopch0),
        .cgcompwrintopch0(cgcompwrintopch0),
        .delcondmetintopch0(delcondmetintopch0),
        .fifoovrintopch0(fifoovrintopch0),
        .latencycompintopch0(latencycompintopch0),
        .insertincompleteintopch0(insertincompleteintopch0),
        .decdatatopch0(decdatatopch0),
        .decctltopch0(decctltopch0),
        .decdatavalidtopch0(decdatavalidtopch0),
        .runningdisptopch0(runningdisptopch0),
        .txdatatstopch0(txdatatstopch0),
        .txctltstopch0(txctltstopch0),
        .fiforstrdqdtopch0(fiforstrdqdtopch0),
        .endskwqdtopch0(endskwqdtopch0),
        .endskwrdptrstopch0(endskwrdptrstopch0),
        .alignstatustopch0(alignstatustopch0),
        .alignstatussync0topch0(alignstatussync0topch0),
        .fifordoutcomp0topch0(fifordoutcomp0topch0),
        .cgcomprddalltopch0(cgcomprddalltopch0),
        .cgcompwralltopch0(cgcompwralltopch0),
        .delcondmet0topch0(delcondmet0topch0),
        .insertincomplete0topch0(insertincomplete0topch0),
        .fifoovr0topch0(fifoovr0topch0),
        .latencycomp0topch0(latencycomp0topch0),
        .rxdatarstopch0(rxdatarstopch0),
        .rxctlrstopch0(rxctlrstopch0),
        .rdenablesyncch2(rdenablesyncch2),
        .txdatatcch2(txdatatcch2),
        .txctltcch2(txctltcch2),
        .syncstatusch2(syncstatusch2),
        .rdalignch2(rdalignch2),
        .aligndetsyncch2(aligndetsyncch2),
        .fifordinch2(fifordinch2),
        .alignstatussyncch2(alignstatussyncch2),
        .cgcomprddinch2(cgcomprddinch2),
        .cgcompwrinch2(cgcompwrinch2),
        .delcondmetinch2(delcondmetinch2),
        .fifoovrinch2(fifoovrinch2),
        .latencycompinch2(latencycompinch2),
        .insertincompleteinch2(insertincompleteinch2),
        .decdatach2(decdatach2),
        .decctlch2(decctlch2),
        .decdatavalidch2(decdatavalidch2),
        .runningdispch2(runningdispch2),
        .txdatatsch2(txdatatsch2),
        .txctltsch2(txctltsch2),
        .fiforstrdqdch2(fiforstrdqdch2),
        .endskwqdch2(endskwqdch2),
        .endskwrdptrsch2(endskwrdptrsch2),
        .alignstatusch2(alignstatusch2),
        .alignstatussync0ch2(alignstatussync0ch2),
        .fifordoutcomp0ch2(fifordoutcomp0ch2),
        .cgcomprddallch2(cgcomprddallch2),
        .cgcompwrallch2(cgcompwrallch2),
        .delcondmet0ch2(delcondmet0ch2),
        .insertincomplete0ch2(insertincomplete0ch2),
        .fifoovr0ch2(fifoovr0ch2),
        .latencycomp0ch2(latencycomp0ch2),
        .rxdatarsch2(rxdatarsch2),
        .rxctlrsch2(rxctlrsch2),
        .rdenablesyncch1(rdenablesyncch1),
        .txdatatcch1(txdatatcch1),
        .txctltcch1(txctltcch1),
        .syncstatusch1(syncstatusch1),
        .rdalignch1(rdalignch1),
        .aligndetsyncch1(aligndetsyncch1),
        .fifordinch1(fifordinch1),
        .alignstatussyncch1(alignstatussyncch1),
        .cgcomprddinch1(cgcomprddinch1),
        .cgcompwrinch1(cgcompwrinch1),
        .delcondmetinch1(delcondmetinch1),
        .fifoovrinch1(fifoovrinch1),
        .latencycompinch1(latencycompinch1),
        .insertincompleteinch1(insertincompleteinch1),
        .decdatach1(decdatach1),
        .decctlch1(decctlch1),
        .decdatavalidch1(decdatavalidch1),
        .runningdispch1(runningdispch1),
        .txdatatsch1(txdatatsch1),
        .txctltsch1(txctltsch1),
        .fiforstrdqdch1(fiforstrdqdch1),
        .endskwqdch1(endskwqdch1),
        .endskwrdptrsch1(endskwrdptrsch1),
        .alignstatusch1(alignstatusch1),
        .alignstatussync0ch1(alignstatussync0ch1),
        .fifordoutcomp0ch1(fifordoutcomp0ch1),
        .cgcomprddallch1(cgcomprddallch1),
        .cgcompwrallch1(cgcompwrallch1),
        .delcondmet0ch1(delcondmet0ch1),
        .insertincomplete0ch1(insertincomplete0ch1),
        .fifoovr0ch1(fifoovr0ch1),
        .latencycomp0ch1(latencycomp0ch1),
        .rxdatarsch1(rxdatarsch1),
        .rxctlrsch1(rxctlrsch1),
        .rdenablesyncch0(rdenablesyncch0),
        .txdatatcch0(txdatatcch0),
        .txctltcch0(txctltcch0),
        .syncstatusch0(syncstatusch0),
        .rdalignch0(rdalignch0),
        .aligndetsyncch0(aligndetsyncch0),
        .fifordinch0(fifordinch0),
        .alignstatussyncch0(alignstatussyncch0),
        .cgcomprddinch0(cgcomprddinch0),
        .cgcompwrinch0(cgcompwrinch0),
        .delcondmetinch0(delcondmetinch0),
        .fifoovrinch0(fifoovrinch0),
        .latencycompinch0(latencycompinch0),
        .insertincompleteinch0(insertincompleteinch0),
        .decdatach0(decdatach0),
        .decctlch0(decctlch0),
        .decdatavalidch0(decdatavalidch0),
        .runningdispch0(runningdispch0),
        .txdatatsch0(txdatatsch0),
        .txctltsch0(txctltsch0),
        .fiforstrdqdch0(fiforstrdqdch0),
        .endskwqdch0(endskwqdch0),
        .endskwrdptrsch0(endskwrdptrsch0),
        .alignstatusch0(alignstatusch0),
        .alignstatussync0ch0(alignstatussync0ch0),
        .fifordoutcomp0ch0(fifordoutcomp0ch0),
        .cgcomprddallch0(cgcomprddallch0),
        .cgcompwrallch0(cgcompwrallch0),
        .delcondmet0ch0(delcondmet0ch0),
        .insertincomplete0ch0(insertincomplete0ch0),
        .fifoovr0ch0(fifoovr0ch0),
        .latencycomp0ch0(latencycomp0ch0),
        .rxdatarsch0(rxdatarsch0),
        .rxctlrsch0(rxctlrsch0),
        .rdenablesyncbotch2(rdenablesyncbotch2),
        .txdatatcbotch2(txdatatcbotch2),
        .txctltcbotch2(txctltcbotch2),
        .syncstatusbotch2(syncstatusbotch2),
        .rdalignbotch2(rdalignbotch2),
        .aligndetsyncbotch2(aligndetsyncbotch2),
        .fifordinbotch2(fifordinbotch2),
        .alignstatussyncbotch2(alignstatussyncbotch2),
        .cgcomprddinbotch2(cgcomprddinbotch2),
        .cgcompwrinbotch2(cgcompwrinbotch2),
        .delcondmetinbotch2(delcondmetinbotch2),
        .fifoovrinbotch2(fifoovrinbotch2),
        .latencycompinbotch2(latencycompinbotch2),
        .insertincompleteinbotch2(insertincompleteinbotch2),
        .decdatabotch2(decdatabotch2),
        .decctlbotch2(decctlbotch2),
        .decdatavalidbotch2(decdatavalidbotch2),
        .runningdispbotch2(runningdispbotch2),
        .txdatatsbotch2(txdatatsbotch2),
        .txctltsbotch2(txctltsbotch2),
        .fiforstrdqdbotch2(fiforstrdqdbotch2),
        .endskwqdbotch2(endskwqdbotch2),
        .endskwrdptrsbotch2(endskwrdptrsbotch2),
        .alignstatusbotch2(alignstatusbotch2),
        .alignstatussync0botch2(alignstatussync0botch2),
        .fifordoutcomp0botch2(fifordoutcomp0botch2),
        .cgcomprddallbotch2(cgcomprddallbotch2),
        .cgcompwrallbotch2(cgcompwrallbotch2),
        .delcondmet0botch2(delcondmet0botch2),
        .insertincomplete0botch2(insertincomplete0botch2),
        .fifoovr0botch2(fifoovr0botch2),
        .latencycomp0botch2(latencycomp0botch2),
        .rxdatarsbotch2(rxdatarsbotch2),
        .rxctlrsbotch2(rxctlrsbotch2) );
    defparam inst.xaui_sm_operation = xaui_sm_operation;
    defparam inst.dskw_sm_operation = dskw_sm_operation;
    defparam inst.data_agg_bonding = data_agg_bonding;
    defparam inst.prot_mode_tx = prot_mode_tx;
    defparam inst.pcs_dw_datapath = pcs_dw_datapath;
    defparam inst.dskw_control = dskw_control;
    defparam inst.refclkdig_sel = refclkdig_sel;

endmodule //stratixv_hssi_8g_pcs_aggregate

`timescale 1 ps/1 ps

module stratixv_hssi_8g_rx_pcs
(
	hrdrst,
	rxpcsrst,
	rmfifouserrst,
	phfifouserrst,
	scanmode,
	enablecommadetect,
	a1a2size,
	bitslip,
	rmfiforeadenable,
	rmfifowriteenable,
	pldrxclk,
	softresetrclk1,
	polinvrx,
	bitreversalenable,
	bytereversalenable,
	rcvdclkpma,
	datain,
	sigdetfrompma,
	fiforstrdqd,
	endskwqd,
	endskwrdptrs,
	alignstatus,
	fiforstrdqdtoporbot,
	endskwqdtoporbot,
	endskwrdptrstoporbot,
	alignstatustoporbot,
	datafrinaggblock,
	ctrlfromaggblock,
	rxdatarstoporbot,
	rxcontrolrstoporbot,
	rcvdclk0pma,
	parallelloopback,
	txpmaclk,
	byteorder,
	pxfifowrdisable,
	pcfifordenable,
	pmatestbus,
	encodertestbus,
	txctrltestbus,
	phystatusinternal,
	rxvalidinternal,
	rxstatusinternal,
	phystatuspcsgen3,
	rxvalidpcsgen3,
	rxstatuspcsgen3,
	rxdatavalidpcsgen3,
	rxblkstartpcsgen3,
	rxsynchdrpcsgen3,
	rxdatapcsgen3,
	pipepowerdown,
	rateswitchcontrol,
	gen2ngen1,
	gen2ngen1bundle,
	eidleinfersel,
	pipeloopbk,
	pldltr,
	prbscidenable,
	txdiv2syncoutpipeup,
	fifoselectoutpipeup,
	txwrenableoutpipeup,
	txrdenableoutpipeup,
	txdiv2syncoutpipedown,
	fifoselectoutpipedown,
	txwrenableoutpipedown,
	txrdenableoutpipedown,
	alignstatussync0,
	rmfifordincomp0,
	cgcomprddall,
	cgcompwrall,
	delcondmet0,
	fifoovr0,
	latencycomp0,
	insertincomplete0,
	alignstatussync0toporbot,
	fifordincomp0toporbot,
	cgcomprddalltoporbot,
	cgcompwralltoporbot,
	delcondmet0toporbot,
	fifoovr0toporbot,
	latencycomp0toporbot,
	insertincomplete0toporbot,
	alignstatussync,
	fifordoutcomp,
	cgcomprddout,
	cgcompwrout,
	delcondmetout,
	fifoovrout,
	latencycompout,
	insertincompleteout,
	dataout,
	parallelrevloopback,
	clocktopld,
	bisterr,
	clk2b,
	rcvdclkpmab,
	syncstatus,
	decoderdatavalid,
	decoderdata,
	decoderctrl,
	runningdisparity,
	selftestdone,
	selftesterr,
	errdata,
	errctrl,
	prbsdone,
	prbserrlt,
	signaldetectout,
	aligndetsync,
	rdalign,
	bistdone,
	runlengthviolation,
	rlvlt,
	rmfifopartialfull,
	rmfifofull,
	rmfifopartialempty,
	rmfifoempty,
	pcfifofull,
	pcfifoempty,
	a1a2k1k2flag,
	byteordflag,
	rxpipeclk,
	channeltestbusout,
	rxpipesoftreset,
	phystatus,
	rxvalid,
	rxstatus,
	pipedata,
	rxdatavalid,
	rxblkstart,
	rxsynchdr,
	speedchange,
	eidledetected,
	wordalignboundary,
	rxclkslip,
	eidleexit,
	earlyeios,
	ltr,
	pcswrapbackin,
	rxdivsyncinchnlup,
	rxdivsyncinchnldown,
	wrenableinchnlup,
	wrenableinchnldown,
	rdenableinchnlup,
	rdenableinchnldown,
	rxweinchnlup,
	rxweinchnldown,
	resetpcptrsinchnlup,
	resetpcptrsinchnldown,
	configselinchnlup,
	configselinchnldown,
	speedchangeinchnlup,
	speedchangeinchnldown,
	pcieswitch,
	rxdivsyncoutchnlup,
	rxweoutchnlup,
	wrenableoutchnlup,
	rdenableoutchnlup,
	resetpcptrsoutchnlup,
	speedchangeoutchnlup,
	configseloutchnlup,
	rxdivsyncoutchnldown,
	rxweoutchnldown,
	wrenableoutchnldown,
	rdenableoutchnldown,
	resetpcptrsoutchnldown,
	speedchangeoutchnldown,
	configseloutchnldown,
	resetpcptrsinchnluppipe,
	resetpcptrsinchnldownpipe,
	speedchangeinchnluppipe,
	speedchangeinchnldownpipe,
	disablepcfifobyteserdes,
	resetpcptrs,
	rcvdclkagg,
	rcvdclkaggtoporbot,
	dispcbytegen3,
	refclkdig,
	txfifordclkraw,
	resetpcptrsgen3,
	syncdatain,
	observablebyteserdesclock
); 

parameter channel_number = 0;
// parameter declaration and default value assignemnt
parameter prot_mode = "gige";	//Valid values: PIPE_G1|PIPE_G2|PIPE_G3|CPRI|CPRI_RX_TX|GIGE|XAUI|SRIO_2P1|TEST|BASIC|DISABLED_PROT_MODE
parameter tx_rx_parallel_loopback = "dis_plpbk";	//Valid values: DIS_PLPBK|EN_PLPBK
parameter pma_dw = "eight_bit";	//Valid values: EIGHT_BIT|TEN_BIT|SIXTEEN_BIT|TWENTY_BIT
parameter pcs_bypass = "dis_pcs_bypass";	//Valid values: DIS_PCS_BYPASS|EN_PCS_BYPASS
parameter polarity_inversion = "dis_pol_inv";	//Valid values: DIS_POL_INV|EN_POL_INV
parameter wa_pd = "wa_pd_10";	//Valid values: DONT_CARE_WA_PD_0|DONT_CARE_WA_PD_1|WA_PD_7|WA_PD_10|WA_PD_20|WA_PD_40|WA_PD_8_SW|WA_PD_8_DW|WA_PD_16_SW|WA_PD_16_DW|WA_PD_32|WA_PD_FIXED_7_K28P5|WA_PD_FIXED_10_K28P5|WA_PD_FIXED_16_A1A2_SW|WA_PD_FIXED_16_A1A2_DW|WA_PD_FIXED_32_A1A1A2A2
parameter wa_pd_data = 40'b0;	//Valid values: 40
parameter wa_boundary_lock_ctrl = "bit_slip";	//Valid values: BIT_SLIP|SYNC_SM|DETERMINISTIC_LATENCY|AUTO_ALIGN_PLD_CTRL
parameter wa_pld_controlled = "dis_pld_ctrl";	//Valid values: DIS_PLD_CTRL|PLD_CTRL_SW|RISING_EDGE_SENSITIVE_DW|LEVEL_SENSITIVE_DW
parameter wa_sync_sm_ctrl = "gige_sync_sm";	//Valid values: GIGE_SYNC_SM|PIPE_SYNC_SM|XAUI_SYNC_SM|SRIO1P3_SYNC_SM|SRIO2P1_SYNC_SM|SW_BASIC_SYNC_SM|DW_BASIC_SYNC_SM|FIBRE_CHANNEL_SYNC_SM
parameter wa_rknumber_data = 8'b0;	//Valid values: 8
parameter wa_renumber_data = 6'b0;	//Valid values: 6
parameter wa_rgnumber_data = 8'b0;	//Valid values: 8
parameter wa_rosnumber_data = 2'b0;	//Valid values: 2
parameter wa_kchar = "dis_kchar";	//Valid values: DIS_KCHAR|EN_KCHAR
parameter wa_det_latency_sync_status_beh = "assert_sync_status_non_imm";	//Valid values: ASSERT_SYNC_STATUS_IMM|ASSERT_SYNC_STATUS_NON_IMM|DONT_CARE_ASSERT_SYNC
parameter wa_clk_slip_spacing = "min_clk_slip_spacing";	//Valid values: MIN_CLK_SLIP_SPACING|USER_PROGRAMMABLE_CLK_SLIP_SPACING
parameter wa_clk_slip_spacing_data = 10'b10000;	//Valid values: 10
parameter bit_reversal = "dis_bit_reversal";	//Valid values: DIS_BIT_REVERSAL|EN_BIT_REVERSAL
parameter symbol_swap = "dis_symbol_swap";	//Valid values: DIS_SYMBOL_SWAP|EN_SYMBOL_SWAP
parameter deskew_pattern = 10'b1101101000;	//Valid values: 10
parameter deskew_prog_pattern_only = "en_deskew_prog_pat_only";	//Valid values: DIS_DESKEW_PROG_PAT_ONLY|EN_DESKEW_PROG_PAT_ONLY
parameter rate_match = "dis_rm";	//Valid values: DIS_RM|XAUI_RM|GIGE_RM|PIPE_RM|PIPE_RM_0PPM|SW_BASIC_RM|SRIO_V2P1_RM|SRIO_V2P1_RM_0PPM|DW_BASIC_RM
parameter eightb_tenb_decoder = "dis_8b10b";	//Valid values: DIS_8B10B|EN_8B10B_IBM|EN_8B10B_SGX
parameter err_flags_sel = "err_flags_wa";	//Valid values: ERR_FLAGS_WA|ERR_FLAGS_8B10B
parameter polinv_8b10b_dec = "dis_polinv_8b10b_dec";	//Valid values: DIS_POLINV_8B10B_DEC|EN_POLINV_8B10B_DEC
parameter eightbtenb_decoder_output_sel = "data_8b10b_decoder";	//Valid values: DATA_8B10B_DECODER|DATA_XAUI_SM
parameter invalid_code_flag_only = "dis_invalid_code_only";	//Valid values: DIS_INVALID_CODE_ONLY|EN_INVALID_CODE_ONLY
parameter auto_error_replacement = "dis_err_replace";	//Valid values: DIS_ERR_REPLACE|EN_ERR_REPLACE
parameter pad_or_edb_error_replace = "replace_edb";	//Valid values: REPLACE_EDB|REPLACE_PAD
parameter byte_deserializer = "dis_bds";	//Valid values: DIS_BDS|EN_BDS_BY_2|EN_BDS_BY_4|EN_BDS_BY_2_DET
parameter byte_order = "dis_bo";	//Valid values: DIS_BO|EN_PCS_CTRL_EIGHT_BIT_BO|EN_PCS_CTRL_NINE_BIT_BO|EN_PCS_CTRL_TEN_BIT_BO|EN_PLD_CTRL_EIGHT_BIT_BO|EN_PLD_CTRL_NINE_BIT_BO|EN_PLD_CTRL_TEN_BIT_BO
parameter re_bo_on_wa = "dis_re_bo_on_wa";	//Valid values: DIS_RE_BO_ON_WA|EN_RE_BO_ON_WA
parameter bo_pattern = 20'b0;	//Valid values: 20
parameter bo_pad = 10'b0;	//Valid values: 10
parameter phase_compensation_fifo = "low_latency";	//Valid values: LOW_LATENCY|NORMAL_LATENCY|REGISTER_FIFO|PLD_CTRL_LOW_LATENCY|PLD_CTRL_NORMAL_LATENCY
parameter prbs_ver = "dis_prbs";	//Valid values: DIS_PRBS|PRBS_7_SW|PRBS_7_DW|PRBS_8|PRBS_10|PRBS_23_SW|PRBS_23_DW|PRBS_15|PRBS_31|PRBS_HF_SW|PRBS_HF_DW|PRBS_LF_SW|PRBS_LF_DW|PRBS_MF_SW|PRBS_MF_DW
parameter cid_pattern = "cid_pattern_0";	//Valid values: CID_PATTERN_0|CID_PATTERN_1
parameter cid_pattern_len = 8'b0;	//Valid values: 8
parameter bist_ver = "dis_bist";	//Valid values: DIS_BIST|INCREMENTAL|CJPAT|CRPAT
parameter cdr_ctrl = "dis_cdr_ctrl";	//Valid values: DIS_CDR_CTRL|EN_CDR_CTRL|EN_CDR_CTRL_W_CID
parameter cdr_ctrl_rxvalid_mask = "dis_rxvalid_mask";	//Valid values: DIS_RXVALID_MASK|EN_RXVALID_MASK
parameter wait_cnt = 8'b0;	//Valid values: 8
parameter mask_cnt = 10'h3ff;	//Valid values: 10
parameter auto_deassert_pc_rst_cnt_data = 5'b0;	//Valid values: 5
parameter auto_pc_en_cnt_data = 7'b0;	//Valid values: 7
parameter eidle_entry_sd = "dis_eidle_sd";	//Valid values: DIS_EIDLE_SD|EN_EIDLE_SD
parameter eidle_entry_eios = "dis_eidle_eios";	//Valid values: DIS_EIDLE_EIOS|EN_EIDLE_EIOS
parameter eidle_entry_iei = "dis_eidle_iei";	//Valid values: DIS_EIDLE_IEI|EN_EIDLE_IEI
parameter rx_rcvd_clk = "rcvd_clk_rcvd_clk";	//Valid values: RCVD_CLK_RCVD_CLK|REFLCLK_DIG|TX_PMA_CLOCK_RCVD_CLK
parameter rx_clk1 = "rcvd_clk_clk1";	//Valid values: RCVD_CLK_CLK1|RCVD_CLK_AGG_CLK1|RCVD_CLK_AGG_TOP_OR_BOTTOM_CLK1|TX_PMA_CLOCK_CLK1
parameter rx_clk2 = "rcvd_clk_clk2";	//Valid values: RCVD_CLK_CLK2|TX_PMA_CLOCK_CLK2
parameter rx_rd_clk = "pld_rx_clk";	//Valid values: PLD_RX_CLK|RX_CLK
parameter dw_one_or_two_symbol_bo = "donot_care_one_two_bo";	//Valid values: DONOT_CARE_ONE_TWO_BO|ONE_SYMBOL_BO|TWO_SYMBOL_BO_EIGHT_BIT|TWO_SYMBOL_BO_NINE_BIT|TWO_SYMBOL_BO_TEN_BIT
parameter comp_fifo_rst_pld_ctrl = "dis_comp_fifo_rst_pld_ctrl";	//Valid values: DIS_COMP_FIFO_RST_PLD_CTRL|EN_COMP_FIFO_RST_PLD_CTRL
parameter bypass_pipeline_reg = "dis_bypass_pipeline";	//Valid values: DIS_BYPASS_PIPELINE|EN_BYPASS_PIPELINE
parameter agg_block_sel = "same_smrt_pack";	//Valid values: SAME_SMRT_PACK|OTHER_SMRT_PACK
parameter test_bus_sel = "test_bus_sel";	//Valid values: TEST_BUS_SEL
parameter wa_rvnumber_data = 13'b0;	//Valid values: 13
parameter ctrl_plane_bonding_compensation = "dis_compensation";	//Valid values: DIS_COMPENSATION|EN_COMPENSATION
parameter clock_gate_rx = "dis_clk_gating";	//Valid values: DIS_CLK_GATING|EN_CLK_GATING
parameter prbs_ver_clr_flag = "dis_prbs_clr_flag";	//Valid values: DIS_PRBS_CLR_FLAG|EN_PRBS_CLR_FLAG
parameter hip_mode = "dis_hip";	//Valid values: DIS_HIP|EN_HIP
parameter ctrl_plane_bonding_distribution = "not_master_chnl_distr";	//Valid values: MASTER_CHNL_DISTR|NOT_MASTER_CHNL_DISTR
parameter ctrl_plane_bonding_consumption = "individual";	//Valid values: INDIVIDUAL|BUNDLED_MASTER|BUNDLED_SLAVE_BELOW|BUNDLED_SLAVE_ABOVE
parameter pma_done_count = 18'b0;	//Valid values: 18
parameter test_mode = "prbs";	//Valid values: DONT_CARE_TEST|PRBS|BIST
parameter bist_ver_clr_flag = "dis_bist_clr_flag";	//Valid values: DIS_BIST_CLR_FLAG|EN_BIST_CLR_FLAG
parameter wa_disp_err_flag = "dis_disp_err_flag";	//Valid values: DIS_DISP_ERR_FLAG|EN_DISP_ERR_FLAG
parameter wait_for_phfifo_cnt_data = 6'b0;	//Valid values: 6
parameter runlength_check = "en_runlength_sw";	//Valid values: DIS_RUNLENGTH|EN_RUNLENGTH_SW|EN_RUNLENGTH_DW
parameter test_bus_sel_val = 4'b0;	//Valid values: 4
parameter runlength_val = 6'b0;	//Valid values: 6
parameter force_signal_detect = "en_force_signal_detect";	//Valid values: EN_FORCE_SIGNAL_DETECT|DIS_FORCE_SIGNAL_DETECT
parameter deskew = "dis_deskew";	//Valid values: DIS_DESKEW|EN_SRIO_V2P1|EN_XAUI|BASIC_SW|BASIC_DW
parameter rx_wr_clk = "rx_clk2_div_1_2_4";	//Valid values: RX_CLK2_DIV_1_2_4|TXFIFO_RD_CLK
parameter rx_clk_free_running = "en_rx_clk_free_run";	//Valid values: DIS_RX_CLK_FREE_RUN|EN_RX_CLK_FREE_RUN
parameter rx_pcs_urst = "en_rx_pcs_urst";	//Valid values: DIS_RX_PCS_URST|EN_RX_PCS_URST
parameter self_switch_dw_scaling = "dis_self_switch_dw_scaling";	//Valid values: DIS_SELF_SWITCH_DW_SCALING|EN_SELF_SWITCH_DW_SCALING
parameter pipe_if_enable = "dis_pipe_rx";	//Valid values: DIS_PIPE_RX|EN_PIPE_RX|EN_PIPE3_RX
parameter pc_fifo_rst_pld_ctrl = "dis_pc_fifo_rst_pld_ctrl";	//Valid values: DIS_PC_FIFO_RST_PLD_CTRL|EN_PC_FIFO_RST_PLD_CTRL
parameter auto_speed_nego_gen2 = "dis_auto_speed_nego_g2";	//Valid values: DIS_AUTO_SPEED_NEGO_G2|EN_FREQ_SCALING_G2|EN_DATA_WIDTH_SCALING_G2
parameter auto_speed_nego_gen3 = "dis_auto_speed_nego_g3";	//Valid values: DIS_AUTO_SPEED_NEGO_G3|EN_AUTO_SPEED_NEGO_G3
parameter ibm_invalid_code = "dis_ibm_invalid_code";	//Valid values: DIS_IBM_INVALID_CODE|EN_IBM_INVALID_CODE
parameter rx_refclk = "dis_refclk_sel";	//Valid values: DIS_REFCLK_SEL|EN_REFCLK_SEL

//input and output port declaration
input [ 0:0 ] hrdrst;
input [ 0:0 ] rxpcsrst;
input [ 0:0 ] rmfifouserrst;
input [ 0:0 ] phfifouserrst;
input [ 0:0 ] scanmode;
input [ 0:0 ] enablecommadetect;
input [ 0:0 ] a1a2size;
input [ 0:0 ] bitslip;
input [ 0:0 ] rmfiforeadenable;
input [ 0:0 ] rmfifowriteenable;
input [ 0:0 ] pldrxclk;
output [ 0:0 ] softresetrclk1;
input [ 0:0 ] polinvrx;
input [ 0:0 ] bitreversalenable;
input [ 0:0 ] bytereversalenable;
input [ 0:0 ] rcvdclkpma;
input [ 19:0 ] datain;
input [ 0:0 ] sigdetfrompma;
input [ 0:0 ] fiforstrdqd;
input [ 0:0 ] endskwqd;
input [ 0:0 ] endskwrdptrs;
input [ 0:0 ] alignstatus;
input [ 0:0 ] fiforstrdqdtoporbot;
input [ 0:0 ] endskwqdtoporbot;
input [ 0:0 ] endskwrdptrstoporbot;
input [ 0:0 ] alignstatustoporbot;
input [ 7:0 ] datafrinaggblock;
input [ 0:0 ] ctrlfromaggblock;
input [ 7:0 ] rxdatarstoporbot;
input [ 0:0 ] rxcontrolrstoporbot;
input [ 0:0 ] rcvdclk0pma;
input [ 19:0 ] parallelloopback;
input [ 0:0 ] txpmaclk;
input [ 0:0 ] byteorder;
input [ 0:0 ] pxfifowrdisable;
input [ 0:0 ] pcfifordenable;
input [ 7:0 ] pmatestbus;
input [ 9:0 ] encodertestbus;
input [ 9:0 ] txctrltestbus;
input [ 0:0 ] phystatusinternal;
input [ 0:0 ] rxvalidinternal;
input [ 2:0 ] rxstatusinternal;
input [ 0:0 ] phystatuspcsgen3;
input [ 0:0 ] rxvalidpcsgen3;
input [ 2:0 ] rxstatuspcsgen3;
input [ 3:0 ] rxdatavalidpcsgen3;
input [ 3:0 ] rxblkstartpcsgen3;
input [ 1:0 ] rxsynchdrpcsgen3;
input [ 63:0 ] rxdatapcsgen3;
input [ 1:0 ] pipepowerdown;
input [ 0:0 ] rateswitchcontrol;
input [ 0:0 ] gen2ngen1;
input [ 0:0 ] gen2ngen1bundle;
input [ 2:0 ] eidleinfersel;
input [ 0:0 ] pipeloopbk;
input [ 0:0 ] pldltr;
input [ 0:0 ] prbscidenable;
input [ 0:0 ] txdiv2syncoutpipeup;
input [ 0:0 ] fifoselectoutpipeup;
input [ 0:0 ] txwrenableoutpipeup;
input [ 0:0 ] txrdenableoutpipeup;
input [ 0:0 ] txdiv2syncoutpipedown;
input [ 0:0 ] fifoselectoutpipedown;
input [ 0:0 ] txwrenableoutpipedown;
input [ 0:0 ] txrdenableoutpipedown;
input [ 0:0 ] alignstatussync0;
input [ 0:0 ] rmfifordincomp0;
input [ 0:0 ] cgcomprddall;
input [ 0:0 ] cgcompwrall;
input [ 0:0 ] delcondmet0;
input [ 0:0 ] fifoovr0;
input [ 0:0 ] latencycomp0;
input [ 0:0 ] insertincomplete0;
input [ 0:0 ] alignstatussync0toporbot;
input [ 0:0 ] fifordincomp0toporbot;
input [ 0:0 ] cgcomprddalltoporbot;
input [ 0:0 ] cgcompwralltoporbot;
input [ 0:0 ] delcondmet0toporbot;
input [ 0:0 ] fifoovr0toporbot;
input [ 0:0 ] latencycomp0toporbot;
input [ 0:0 ] insertincomplete0toporbot;
output [ 0:0 ] alignstatussync;
output [ 0:0 ] fifordoutcomp;
output [ 1:0 ] cgcomprddout;
output [ 1:0 ] cgcompwrout;
output [ 0:0 ] delcondmetout;
output [ 0:0 ] fifoovrout;
output [ 0:0 ] latencycompout;
output [ 0:0 ] insertincompleteout;
output [ 63:0 ] dataout;
output [ 19:0 ] parallelrevloopback;
output [ 0:0 ] clocktopld;
output [ 0:0 ] bisterr;
output [ 0:0 ] clk2b;
output [ 0:0 ] rcvdclkpmab;
output [ 0:0 ] syncstatus;
output [ 0:0 ] decoderdatavalid;
output [ 7:0 ] decoderdata;
output [ 0:0 ] decoderctrl;
output [ 1:0 ] runningdisparity;
output [ 0:0 ] selftestdone;
output [ 0:0 ] selftesterr;
output [ 15:0 ] errdata;
output [ 1:0 ] errctrl;
output [ 0:0 ] prbsdone;
output [ 0:0 ] prbserrlt;
output [ 0:0 ] signaldetectout;
output [ 1:0 ] aligndetsync;
output [ 1:0 ] rdalign;
output [ 0:0 ] bistdone;
output [ 0:0 ] runlengthviolation;
output [ 0:0 ] rlvlt;
output [ 0:0 ] rmfifopartialfull;
output [ 0:0 ] rmfifofull;
output [ 0:0 ] rmfifopartialempty;
output [ 0:0 ] rmfifoempty;
output [ 0:0 ] pcfifofull;
output [ 0:0 ] pcfifoempty;
output [ 3:0 ] a1a2k1k2flag;
output [ 0:0 ] byteordflag;
output [ 0:0 ] rxpipeclk;
output [ 9:0 ] channeltestbusout;
output [ 0:0 ] rxpipesoftreset;
output [ 0:0 ] phystatus;
output [ 0:0 ] rxvalid;
output [ 2:0 ] rxstatus;
output [ 63:0 ] pipedata;
output [ 3:0 ] rxdatavalid;
output [ 3:0 ] rxblkstart;
output [ 1:0 ] rxsynchdr;
output [ 0:0 ] speedchange;
output [ 0:0 ] eidledetected;
output [ 4:0 ] wordalignboundary;
output [ 0:0 ] rxclkslip;
output [ 0:0 ] eidleexit;
output [ 0:0 ] earlyeios;
output [ 0:0 ] ltr;
input [ 69:0 ] pcswrapbackin;
input [ 1:0 ] rxdivsyncinchnlup;
input [ 1:0 ] rxdivsyncinchnldown;
input [ 0:0 ] wrenableinchnlup;
input [ 0:0 ] wrenableinchnldown;
input [ 0:0 ] rdenableinchnlup;
input [ 0:0 ] rdenableinchnldown;
input [ 1:0 ] rxweinchnlup;
input [ 1:0 ] rxweinchnldown;
input [ 0:0 ] resetpcptrsinchnlup;
input [ 0:0 ] resetpcptrsinchnldown;
input [ 0:0 ] configselinchnlup;
input [ 0:0 ] configselinchnldown;
input [ 0:0 ] speedchangeinchnlup;
input [ 0:0 ] speedchangeinchnldown;
output [ 0:0 ] pcieswitch;
output [ 1:0 ] rxdivsyncoutchnlup;
output [ 1:0 ] rxweoutchnlup;
output [ 0:0 ] wrenableoutchnlup;
output [ 0:0 ] rdenableoutchnlup;
output [ 0:0 ] resetpcptrsoutchnlup;
output [ 0:0 ] speedchangeoutchnlup;
output [ 0:0 ] configseloutchnlup;
output [ 1:0 ] rxdivsyncoutchnldown;
output [ 1:0 ] rxweoutchnldown;
output [ 0:0 ] wrenableoutchnldown;
output [ 0:0 ] rdenableoutchnldown;
output [ 0:0 ] resetpcptrsoutchnldown;
output [ 0:0 ] speedchangeoutchnldown;
output [ 0:0 ] configseloutchnldown;
output [ 0:0 ] resetpcptrsinchnluppipe;
output [ 0:0 ] resetpcptrsinchnldownpipe;
output [ 0:0 ] speedchangeinchnluppipe;
output [ 0:0 ] speedchangeinchnldownpipe;
output [ 0:0 ] disablepcfifobyteserdes;
output [ 0:0 ] resetpcptrs;
input [ 0:0 ] rcvdclkagg;
input [ 0:0 ] rcvdclkaggtoporbot;
input [ 0:0 ] dispcbytegen3;
input [ 0:0 ] refclkdig;
input [ 0:0 ] txfifordclkraw;
input [ 0:0 ] resetpcptrsgen3;
output [ 0:0 ] syncdatain;
output [ 0:0 ] observablebyteserdesclock;

stratixv_hssi_8g_rx_pcs_encrypted inst (
.hrdrst(hrdrst),
.rxpcsrst(rxpcsrst),
.rmfifouserrst(rmfifouserrst),
.phfifouserrst(phfifouserrst),
.scanmode(scanmode),
.enablecommadetect(enablecommadetect),
.a1a2size(a1a2size),
.bitslip(bitslip),
.rmfiforeadenable(rmfiforeadenable),
.rmfifowriteenable(rmfifowriteenable),
.pldrxclk(pldrxclk),
.softresetrclk1(softresetrclk1),
.polinvrx(polinvrx),
.bitreversalenable(bitreversalenable),
.bytereversalenable(bytereversalenable),
.rcvdclkpma(rcvdclkpma),
.datain(datain),
.sigdetfrompma(sigdetfrompma),
.fiforstrdqd(fiforstrdqd),
.endskwqd(endskwqd),
.endskwrdptrs(endskwrdptrs),
.alignstatus(alignstatus),
.fiforstrdqdtoporbot(fiforstrdqdtoporbot),
.endskwqdtoporbot(endskwqdtoporbot),
.endskwrdptrstoporbot(endskwrdptrstoporbot),
.alignstatustoporbot(alignstatustoporbot),
.datafrinaggblock(datafrinaggblock),
.ctrlfromaggblock(ctrlfromaggblock),
.rxdatarstoporbot(rxdatarstoporbot),
.rxcontrolrstoporbot(rxcontrolrstoporbot),
.rcvdclk0pma(rcvdclk0pma),
.parallelloopback(parallelloopback),
.txpmaclk(txpmaclk),
.byteorder(byteorder),
.pxfifowrdisable(pxfifowrdisable),
.pcfifordenable(pcfifordenable),
.pmatestbus(pmatestbus),
.encodertestbus(encodertestbus),
.txctrltestbus(txctrltestbus),
.phystatusinternal(phystatusinternal),
.rxvalidinternal(rxvalidinternal),
.rxstatusinternal(rxstatusinternal),
.phystatuspcsgen3(phystatuspcsgen3),
.rxvalidpcsgen3(rxvalidpcsgen3),
.rxstatuspcsgen3(rxstatuspcsgen3),
.rxdatavalidpcsgen3(rxdatavalidpcsgen3),
.rxblkstartpcsgen3(rxblkstartpcsgen3),
.rxsynchdrpcsgen3(rxsynchdrpcsgen3),
.rxdatapcsgen3(rxdatapcsgen3),
.pipepowerdown(pipepowerdown),
.rateswitchcontrol(rateswitchcontrol),
.gen2ngen1(gen2ngen1),
.gen2ngen1bundle(gen2ngen1bundle),
.eidleinfersel(eidleinfersel),
.pipeloopbk(pipeloopbk),
.pldltr(pldltr),
.prbscidenable(prbscidenable),
.txdiv2syncoutpipeup(txdiv2syncoutpipeup),
.fifoselectoutpipeup(fifoselectoutpipeup),
.txwrenableoutpipeup(txwrenableoutpipeup),
.txrdenableoutpipeup(txrdenableoutpipeup),
.txdiv2syncoutpipedown(txdiv2syncoutpipedown),
.fifoselectoutpipedown(fifoselectoutpipedown),
.txwrenableoutpipedown(txwrenableoutpipedown),
.txrdenableoutpipedown(txrdenableoutpipedown),
.alignstatussync0(alignstatussync0),
.rmfifordincomp0(rmfifordincomp0),
.cgcomprddall(cgcomprddall),
.cgcompwrall(cgcompwrall),
.delcondmet0(delcondmet0),
.fifoovr0(fifoovr0),
.latencycomp0(latencycomp0),
.insertincomplete0(insertincomplete0),
.alignstatussync0toporbot(alignstatussync0toporbot),
.fifordincomp0toporbot(fifordincomp0toporbot),
.cgcomprddalltoporbot(cgcomprddalltoporbot),
.cgcompwralltoporbot(cgcompwralltoporbot),
.delcondmet0toporbot(delcondmet0toporbot),
.fifoovr0toporbot(fifoovr0toporbot),
.latencycomp0toporbot(latencycomp0toporbot),
.insertincomplete0toporbot(insertincomplete0toporbot),
.alignstatussync(alignstatussync),
.fifordoutcomp(fifordoutcomp),
.cgcomprddout(cgcomprddout),
.cgcompwrout(cgcompwrout),
.delcondmetout(delcondmetout),
.fifoovrout(fifoovrout),
.latencycompout(latencycompout),
.insertincompleteout(insertincompleteout),
.dataout(dataout),
.parallelrevloopback(parallelrevloopback),
.clocktopld(clocktopld),
.bisterr(bisterr),
.clk2b(clk2b),
.rcvdclkpmab(rcvdclkpmab),
.syncstatus(syncstatus),
.decoderdatavalid(decoderdatavalid),
.decoderdata(decoderdata),
.decoderctrl(decoderctrl),
.runningdisparity(runningdisparity),
.selftestdone(selftestdone),
.selftesterr(selftesterr),
.errdata(errdata),
.errctrl(errctrl),
.prbsdone(prbsdone),
.prbserrlt(prbserrlt),
.signaldetectout(signaldetectout),
.aligndetsync(aligndetsync),
.rdalign(rdalign),
.bistdone(bistdone),
.runlengthviolation(runlengthviolation),
.rlvlt(rlvlt),
.rmfifopartialfull(rmfifopartialfull),
.rmfifofull(rmfifofull),
.rmfifopartialempty(rmfifopartialempty),
.rmfifoempty(rmfifoempty),
.pcfifofull(pcfifofull),
.pcfifoempty(pcfifoempty),
.a1a2k1k2flag(a1a2k1k2flag),
.byteordflag(byteordflag),
.rxpipeclk(rxpipeclk),
.channeltestbusout(channeltestbusout),
.rxpipesoftreset(rxpipesoftreset),
.phystatus(phystatus),
.rxvalid(rxvalid),
.rxstatus(rxstatus),
.pipedata(pipedata),
.rxdatavalid(rxdatavalid),
.rxblkstart(rxblkstart),
.rxsynchdr(rxsynchdr),
.speedchange(speedchange),
.eidledetected(eidledetected),
.wordalignboundary(wordalignboundary),
.rxclkslip(rxclkslip),
.eidleexit(eidleexit),
.earlyeios(earlyeios),
.ltr(ltr),
.pcswrapbackin(pcswrapbackin),
.rxdivsyncinchnlup(rxdivsyncinchnlup),
.rxdivsyncinchnldown(rxdivsyncinchnldown),
.wrenableinchnlup(wrenableinchnlup),
.wrenableinchnldown(wrenableinchnldown),
.rdenableinchnlup(rdenableinchnlup),
.rdenableinchnldown(rdenableinchnldown),
.rxweinchnlup(rxweinchnlup),
.rxweinchnldown(rxweinchnldown),
.resetpcptrsinchnlup(resetpcptrsinchnlup),
.resetpcptrsinchnldown(resetpcptrsinchnldown),
.configselinchnlup(configselinchnlup),
.configselinchnldown(configselinchnldown),
.speedchangeinchnlup(speedchangeinchnlup),
.speedchangeinchnldown(speedchangeinchnldown),
.pcieswitch(pcieswitch),
.rxdivsyncoutchnlup(rxdivsyncoutchnlup),
.rxweoutchnlup(rxweoutchnlup),
.wrenableoutchnlup(wrenableoutchnlup),
.rdenableoutchnlup(rdenableoutchnlup),
.resetpcptrsoutchnlup(resetpcptrsoutchnlup),
.speedchangeoutchnlup(speedchangeoutchnlup),
.configseloutchnlup(configseloutchnlup),
.rxdivsyncoutchnldown(rxdivsyncoutchnldown),
.rxweoutchnldown(rxweoutchnldown),
.wrenableoutchnldown(wrenableoutchnldown),
.rdenableoutchnldown(rdenableoutchnldown),
.resetpcptrsoutchnldown(resetpcptrsoutchnldown),
.speedchangeoutchnldown(speedchangeoutchnldown),
.configseloutchnldown(configseloutchnldown),
.resetpcptrsinchnluppipe(resetpcptrsinchnluppipe),
.resetpcptrsinchnldownpipe(resetpcptrsinchnldownpipe),
.speedchangeinchnluppipe(speedchangeinchnluppipe),
.speedchangeinchnldownpipe(speedchangeinchnldownpipe),
.disablepcfifobyteserdes(disablepcfifobyteserdes),
.resetpcptrs(resetpcptrs),
.rcvdclkagg(rcvdclkagg),
.rcvdclkaggtoporbot(rcvdclkaggtoporbot),
.dispcbytegen3(dispcbytegen3),
.refclkdig(refclkdig),
.txfifordclkraw(txfifordclkraw),
.resetpcptrsgen3(resetpcptrsgen3),
.syncdatain(syncdatain),
.observablebyteserdesclock(observablebyteserdesclock)
);


defparam inst.prot_mode = prot_mode;
defparam inst.tx_rx_parallel_loopback = tx_rx_parallel_loopback;
defparam inst.pma_dw = pma_dw;
defparam inst.pcs_bypass = pcs_bypass;
defparam inst.polarity_inversion = polarity_inversion;
defparam inst.wa_pd = wa_pd;
defparam inst.wa_pd_data = wa_pd_data;
defparam inst.wa_boundary_lock_ctrl = wa_boundary_lock_ctrl;
defparam inst.wa_pld_controlled = wa_pld_controlled;
defparam inst.wa_sync_sm_ctrl = wa_sync_sm_ctrl;
defparam inst.wa_rknumber_data = wa_rknumber_data;
defparam inst.wa_renumber_data = wa_renumber_data;
defparam inst.wa_rgnumber_data = wa_rgnumber_data;
defparam inst.wa_rosnumber_data = wa_rosnumber_data;
defparam inst.wa_kchar = wa_kchar;
defparam inst.wa_det_latency_sync_status_beh = wa_det_latency_sync_status_beh;
defparam inst.wa_clk_slip_spacing = wa_clk_slip_spacing;
defparam inst.wa_clk_slip_spacing_data = wa_clk_slip_spacing_data;
defparam inst.bit_reversal = bit_reversal;
defparam inst.symbol_swap = symbol_swap;
defparam inst.deskew_pattern = deskew_pattern;
defparam inst.deskew_prog_pattern_only = deskew_prog_pattern_only;
defparam inst.rate_match = rate_match;
defparam inst.eightb_tenb_decoder = eightb_tenb_decoder;
defparam inst.err_flags_sel = err_flags_sel;
defparam inst.polinv_8b10b_dec = polinv_8b10b_dec;
defparam inst.eightbtenb_decoder_output_sel = eightbtenb_decoder_output_sel;
defparam inst.invalid_code_flag_only = invalid_code_flag_only;
defparam inst.auto_error_replacement = auto_error_replacement;
defparam inst.pad_or_edb_error_replace = pad_or_edb_error_replace;
defparam inst.byte_deserializer = byte_deserializer;
defparam inst.byte_order = byte_order;
defparam inst.re_bo_on_wa = re_bo_on_wa;
defparam inst.bo_pattern = bo_pattern;
defparam inst.bo_pad = bo_pad;
defparam inst.phase_compensation_fifo = phase_compensation_fifo;
defparam inst.prbs_ver = prbs_ver;
defparam inst.cid_pattern = cid_pattern;
defparam inst.cid_pattern_len = cid_pattern_len;
defparam inst.bist_ver = bist_ver;
defparam inst.cdr_ctrl = cdr_ctrl;
defparam inst.cdr_ctrl_rxvalid_mask = cdr_ctrl_rxvalid_mask;
defparam inst.wait_cnt = wait_cnt;
defparam inst.mask_cnt = mask_cnt;
defparam inst.auto_deassert_pc_rst_cnt_data = auto_deassert_pc_rst_cnt_data;
defparam inst.auto_pc_en_cnt_data = auto_pc_en_cnt_data;
defparam inst.eidle_entry_sd = eidle_entry_sd;
defparam inst.eidle_entry_eios = eidle_entry_eios;
defparam inst.eidle_entry_iei = eidle_entry_iei;
defparam inst.rx_rcvd_clk = rx_rcvd_clk;
defparam inst.rx_clk1 = rx_clk1;
defparam inst.rx_clk2 = rx_clk2;
defparam inst.rx_rd_clk = rx_rd_clk;
defparam inst.dw_one_or_two_symbol_bo = dw_one_or_two_symbol_bo;
defparam inst.comp_fifo_rst_pld_ctrl = comp_fifo_rst_pld_ctrl;
defparam inst.bypass_pipeline_reg = bypass_pipeline_reg;
defparam inst.agg_block_sel = agg_block_sel;
defparam inst.test_bus_sel = test_bus_sel;
defparam inst.wa_rvnumber_data = wa_rvnumber_data;
defparam inst.ctrl_plane_bonding_compensation = ctrl_plane_bonding_compensation;
defparam inst.clock_gate_rx = clock_gate_rx;
defparam inst.prbs_ver_clr_flag = prbs_ver_clr_flag;
defparam inst.hip_mode = hip_mode;
defparam inst.ctrl_plane_bonding_distribution = ctrl_plane_bonding_distribution;
defparam inst.ctrl_plane_bonding_consumption = ctrl_plane_bonding_consumption;
defparam inst.pma_done_count = pma_done_count;
defparam inst.test_mode = test_mode;
defparam inst.bist_ver_clr_flag = bist_ver_clr_flag;
defparam inst.wa_disp_err_flag = wa_disp_err_flag;
defparam inst.wait_for_phfifo_cnt_data = wait_for_phfifo_cnt_data;
defparam inst.runlength_check = runlength_check;
defparam inst.test_bus_sel_val = test_bus_sel_val;
defparam inst.runlength_val = runlength_val;
defparam inst.force_signal_detect = force_signal_detect;
defparam inst.deskew = deskew;
defparam inst.rx_wr_clk = rx_wr_clk;
defparam inst.rx_clk_free_running = rx_clk_free_running;
defparam inst.rx_pcs_urst = rx_pcs_urst;
defparam inst.self_switch_dw_scaling = self_switch_dw_scaling;
defparam inst.pipe_if_enable = pipe_if_enable;
defparam inst.pc_fifo_rst_pld_ctrl = pc_fifo_rst_pld_ctrl;
defparam inst.auto_speed_nego_gen2 = auto_speed_nego_gen2;
defparam inst.auto_speed_nego_gen3 = auto_speed_nego_gen3;
defparam inst.ibm_invalid_code = ibm_invalid_code;
defparam inst.channel_number = channel_number;
defparam inst.rx_refclk = rx_refclk;

endmodule	//stratixv_hssi_8g_rx_pcs


`timescale 1 ps/1 ps

module stratixv_hssi_8g_tx_pcs
(
	txpcsreset,
	refclkdig,
	scanmode,
	datain,
	coreclk,
	invpol,
	xgmdatain,
	xgmctrl,
	xgmdataintoporbottom,
	xgmctrltoporbottom,
	txpmalocalclk,
	enrevparallellpbk,
	revparallellpbkdata,
	phfifowrenable,
	phfiforddisable,
	phfiforeset,
	detectrxloopin,
	powerdn,
	pipeenrevparallellpbkin,
	pipetxswing,
	pipetxdeemph,
	pipetxmargin,
	rxpolarityin,
	polinvrxin,
	elecidleinfersel,
	rateswitch,
	rateswitchbundle,
	prbscidenable,
	bitslipboundaryselect,
	phfifooverflow,
	phfifounderflow,
	clkout,
	clkoutgen3,
	xgmdataout,
	xgmctrlenable,
	dataout,
	rdenablesync,
	refclkb,
	parallelfdbkout,
	txpipeclk,
	encodertestbus,
	txctrltestbus,
	txpipesoftreset,
	txpipeelectidle,
	detectrxloopout,
	pipepowerdownout,
	pipeenrevparallellpbkout,
	phfifotxswing,
	phfifotxdeemph,
	phfifotxmargin,
	txdataouttogen3,
	txdatakouttogen3,
	txdatavalidouttogen3,
	txblkstartout,
	txsynchdrout,
	txcomplianceout,
	txelecidleout,
	rxpolarityout,
	polinvrxout,
	grayelecidleinferselout,
	txdivsyncinchnlup,
	txdivsyncinchnldown,
	wrenableinchnlup,
	wrenableinchnldown,
	rdenableinchnlup,
	rdenableinchnldown,
	fifoselectinchnlup,
	fifoselectinchnldown,
	resetpcptrs,
	resetpcptrsinchnlup,
	resetpcptrsinchnldown,
	dispcbyte,
	txdivsyncoutchnlup,
	txdivsyncoutchnldown,
	rdenableoutchnlup,
	rdenableoutchnldown,
	wrenableoutchnlup,
	wrenableoutchnldown,
	fifoselectoutchnlup,
	fifoselectoutchnldown,
	txfifordclkraw,
	syncdatain,
	observablebyteserdesclock
); 

// parameter declaration and default value assignemnt
parameter prot_mode = "basic";	//Valid values: PIPE_G1|PIPE_G2|PIPE_G3|CPRI|CPRI_RX_TX|GIGE|XAUI|SRIO_2P1|TEST|BASIC|DISABLED_PROT_MODE
parameter hip_mode = "dis_hip";	//Valid values: DIS_HIP|EN_HIP
parameter pma_dw = "eight_bit";	//Valid values: EIGHT_BIT|TEN_BIT|SIXTEEN_BIT|TWENTY_BIT
parameter pcs_bypass = "dis_pcs_bypass";	//Valid values: DIS_PCS_BYPASS|EN_PCS_BYPASS
parameter phase_compensation_fifo = "low_latency";	//Valid values: LOW_LATENCY|NORMAL_LATENCY|REGISTER_FIFO|PLD_CTRL_LOW_LATENCY|PLD_CTRL_NORMAL_LATENCY
parameter tx_compliance_controlled_disparity = "dis_txcompliance";	//Valid values: DIS_TXCOMPLIANCE|EN_TXCOMPLIANCE_PIPE2P0|EN_TXCOMPLIANCE_PIPE3P0
parameter force_kchar = "dis_force_kchar";	//Valid values: DIS_FORCE_KCHAR|EN_FORCE_KCHAR
parameter force_echar = "dis_force_echar";	//Valid values: DIS_FORCE_ECHAR|EN_FORCE_ECHAR
parameter byte_serializer = "dis_bs";	//Valid values: DIS_BS|EN_BS_BY_2|EN_BS_BY_4
parameter data_selection_8b10b_encoder_input = "normal_data_path";	//Valid values: NORMAL_DATA_PATH|XAUI_SM|GIGE_IDLE_CONVERSION
parameter eightb_tenb_disp_ctrl = "dis_disp_ctrl";	//Valid values: DIS_DISP_CTRL|EN_DISP_CTRL|EN_IB_DISP_CTRL
parameter eightb_tenb_encoder = "dis_8b10b";	//Valid values: DIS_8B10B|EN_8B10B_IBM|EN_8B10B_SGX
parameter prbs_gen = "dis_prbs";	//Valid values: DIS_PRBS|PRBS_7_SW|PRBS_7_DW|PRBS_8|PRBS_10|PRBS_23_SW|PRBS_23_DW|PRBS_15|PRBS_31|PRBS_HF_SW|PRBS_HF_DW|PRBS_LF_SW|PRBS_LF_DW|PRBS_MF_SW|PRBS_MF_DW
parameter cid_pattern = "cid_pattern_0";	//Valid values: CID_PATTERN_0|CID_PATTERN_1
parameter cid_pattern_len = 8'b0;	//Valid values: 8
parameter bist_gen = "dis_bist";	//Valid values: DIS_BIST|INCREMENTAL|CJPAT|CRPAT
parameter bit_reversal = "dis_bit_reversal";	//Valid values: DIS_BIT_REVERSAL|EN_BIT_REVERSAL
parameter symbol_swap = "dis_symbol_swap";	//Valid values: DIS_SYMBOL_SWAP|EN_SYMBOL_SWAP
parameter polarity_inversion = "dis_polinv";	//Valid values: DIS_POLINV|ENABLE_POLINV
parameter tx_bitslip = "dis_tx_bitslip";	//Valid values: DIS_TX_BITSLIP|EN_TX_BITSLIP
parameter agg_block_sel = "same_smrt_pack";	//Valid values: SAME_SMRT_PACK|OTHER_SMRT_PACK
parameter revloop_back_rm = "dis_rev_loopback_rx_rm";	//Valid values: DIS_REV_LOOPBACK_RX_RM|EN_REV_LOOPBACK_RX_RM
parameter phfifo_write_clk_sel = "pld_tx_clk";	//Valid values: PLD_TX_CLK|TX_CLK
parameter ctrl_plane_bonding_consumption = "individual";	//Valid values: INDIVIDUAL|BUNDLED_MASTER|BUNDLED_SLAVE_BELOW|BUNDLED_SLAVE_ABOVE
parameter bypass_pipeline_reg = "dis_bypass_pipeline";	//Valid values: DIS_BYPASS_PIPELINE|EN_BYPASS_PIPELINE
parameter ctrl_plane_bonding_distribution = "not_master_chnl_distr";	//Valid values: MASTER_CHNL_DISTR|NOT_MASTER_CHNL_DISTR
parameter test_mode = "prbs";	//Valid values: DONT_CARE_TEST|PRBS|BIST
parameter clock_gate_tx = "dis_clk_gating";	//Valid values: DIS_CLK_GATING|EN_CLK_GATING
parameter self_switch_dw_scaling = "dis_self_switch_dw_scaling";	//Valid values: DIS_SELF_SWITCH_DW_SCALING|EN_SELF_SWITCH_DW_SCALING
parameter ctrl_plane_bonding_compensation = "dis_compensation";	//Valid values: DIS_COMPENSATION|EN_COMPENSATION
parameter refclk_b_clk_sel = "tx_pma_clock";	//Valid values: TX_PMA_CLOCK|REFCLK_DIG
parameter auto_speed_nego_gen2 = "dis_auto_speed_nego_g2";	//Valid values: DIS_AUTO_SPEED_NEGO_G2|EN_FREQ_SCALING_G2|EN_DATA_WIDTH_SCALING_G2
parameter auto_speed_nego_gen3 = "dis_auto_speed_nego_g3";	//Valid values: DIS_AUTO_SPEED_NEGO_G3|EN_AUTO_SPEED_NEGO_G3
parameter channel_number = 0;	//Valid values: 0

//input and output port declaration
input [ 0:0 ] txpcsreset;
input [ 0:0 ] refclkdig;
input [ 0:0 ] scanmode;
input [ 43:0 ] datain;
input [ 0:0 ] coreclk;
input [ 0:0 ] invpol;
input [ 7:0 ] xgmdatain;
input [ 0:0 ] xgmctrl;
input [ 7:0 ] xgmdataintoporbottom;
input [ 0:0 ] xgmctrltoporbottom;
input [ 0:0 ] txpmalocalclk;
input [ 0:0 ] enrevparallellpbk;
input [ 19:0 ] revparallellpbkdata;
input [ 0:0 ] phfifowrenable;
input [ 0:0 ] phfiforddisable;
input [ 0:0 ] phfiforeset;
input [ 0:0 ] detectrxloopin;
input [ 1:0 ] powerdn;
input [ 0:0 ] pipeenrevparallellpbkin;
input [ 0:0 ] pipetxswing;
input [ 0:0 ] pipetxdeemph;
input [ 2:0 ] pipetxmargin;
input [ 0:0 ] rxpolarityin;
input [ 0:0 ] polinvrxin;
input [ 2:0 ] elecidleinfersel;
input [ 0:0 ] rateswitch;
input [ 0:0 ] rateswitchbundle;
input [ 0:0 ] prbscidenable;
input [ 4:0 ] bitslipboundaryselect;
output [ 0:0 ] phfifooverflow;
output [ 0:0 ] phfifounderflow;
output [ 0:0 ] clkout;
output [ 0:0 ] clkoutgen3;
output [ 7:0 ] xgmdataout;
output [ 0:0 ] xgmctrlenable;
output [ 19:0 ] dataout;
output [ 0:0 ] rdenablesync;
output [ 0:0 ] refclkb;
output [ 19:0 ] parallelfdbkout;
output [ 0:0 ] txpipeclk;
output [ 9:0 ] encodertestbus;
output [ 9:0 ] txctrltestbus;
output [ 0:0 ] txpipesoftreset;
output [ 0:0 ] txpipeelectidle;
output [ 0:0 ] detectrxloopout;
output [ 1:0 ] pipepowerdownout;
output [ 0:0 ] pipeenrevparallellpbkout;
output [ 0:0 ] phfifotxswing;
output [ 0:0 ] phfifotxdeemph;
output [ 2:0 ] phfifotxmargin;
output [ 31:0 ] txdataouttogen3;
output [ 3:0 ] txdatakouttogen3;
output [ 3:0 ] txdatavalidouttogen3;
output [ 3:0 ] txblkstartout;
output [ 1:0 ] txsynchdrout;
output [ 0:0 ] txcomplianceout;
output [ 0:0 ] txelecidleout;
output [ 0:0 ] rxpolarityout;
output [ 0:0 ] polinvrxout;
output [ 2:0 ] grayelecidleinferselout;
input [ 1:0 ] txdivsyncinchnlup;
input [ 1:0 ] txdivsyncinchnldown;
input [ 0:0 ] wrenableinchnlup;
input [ 0:0 ] wrenableinchnldown;
input [ 0:0 ] rdenableinchnlup;
input [ 0:0 ] rdenableinchnldown;
input [ 1:0 ] fifoselectinchnlup;
input [ 1:0 ] fifoselectinchnldown;
input [ 0:0 ] resetpcptrs;
input [ 0:0 ] resetpcptrsinchnlup;
input [ 0:0 ] resetpcptrsinchnldown;
input [ 0:0 ] dispcbyte;
output [ 1:0 ] txdivsyncoutchnlup;
output [ 1:0 ] txdivsyncoutchnldown;
output [ 0:0 ] rdenableoutchnlup;
output [ 0:0 ] rdenableoutchnldown;
output [ 0:0 ] wrenableoutchnlup;
output [ 0:0 ] wrenableoutchnldown;
output [ 1:0 ] fifoselectoutchnlup;
output [ 1:0 ] fifoselectoutchnldown;
output [ 0:0 ] txfifordclkraw;
output [ 0:0 ] syncdatain;
output [ 0:0 ] observablebyteserdesclock;

stratixv_hssi_8g_tx_pcs_encrypted inst (
.txpcsreset(txpcsreset),
.refclkdig(refclkdig),
.scanmode(scanmode),
.datain(datain),
.coreclk(coreclk),
.invpol(invpol),
.xgmdatain(xgmdatain),
.xgmctrl(xgmctrl),
.xgmdataintoporbottom(xgmdataintoporbottom),
.xgmctrltoporbottom(xgmctrltoporbottom),
.txpmalocalclk(txpmalocalclk),
.enrevparallellpbk(enrevparallellpbk),
.revparallellpbkdata(revparallellpbkdata),
.phfifowrenable(phfifowrenable),
.phfiforddisable(phfiforddisable),
.phfiforeset(phfiforeset),
.detectrxloopin(detectrxloopin),
.powerdn(powerdn),
.pipeenrevparallellpbkin(pipeenrevparallellpbkin),
.pipetxswing(pipetxswing),
.pipetxdeemph(pipetxdeemph),
.pipetxmargin(pipetxmargin),
.rxpolarityin(rxpolarityin),
.polinvrxin(polinvrxin),
.elecidleinfersel(elecidleinfersel),
.rateswitch(rateswitch),
.rateswitchbundle(rateswitchbundle),
.prbscidenable(prbscidenable),
.bitslipboundaryselect(bitslipboundaryselect),
.phfifooverflow(phfifooverflow),
.phfifounderflow(phfifounderflow),
.clkout(clkout),
.clkoutgen3(clkoutgen3),
.xgmdataout(xgmdataout),
.xgmctrlenable(xgmctrlenable),
.dataout(dataout),
.rdenablesync(rdenablesync),
.refclkb(refclkb),
.parallelfdbkout(parallelfdbkout),
.txpipeclk(txpipeclk),
.encodertestbus(encodertestbus),
.txctrltestbus(txctrltestbus),
.txpipesoftreset(txpipesoftreset),
.txpipeelectidle(txpipeelectidle),
.detectrxloopout(detectrxloopout),
.pipepowerdownout(pipepowerdownout),
.pipeenrevparallellpbkout(pipeenrevparallellpbkout),
.phfifotxswing(phfifotxswing),
.phfifotxdeemph(phfifotxdeemph),
.phfifotxmargin(phfifotxmargin),
.txdataouttogen3(txdataouttogen3),
.txdatakouttogen3(txdatakouttogen3),
.txdatavalidouttogen3(txdatavalidouttogen3),
.txblkstartout(txblkstartout),
.txsynchdrout(txsynchdrout),
.txcomplianceout(txcomplianceout),
.txelecidleout(txelecidleout),
.rxpolarityout(rxpolarityout),
.polinvrxout(polinvrxout),
.grayelecidleinferselout(grayelecidleinferselout),
.txdivsyncinchnlup(txdivsyncinchnlup),
.txdivsyncinchnldown(txdivsyncinchnldown),
.wrenableinchnlup(wrenableinchnlup),
.wrenableinchnldown(wrenableinchnldown),
.rdenableinchnlup(rdenableinchnlup),
.rdenableinchnldown(rdenableinchnldown),
.fifoselectinchnlup(fifoselectinchnlup),
.fifoselectinchnldown(fifoselectinchnldown),
.resetpcptrs(resetpcptrs),
.resetpcptrsinchnlup(resetpcptrsinchnlup),
.resetpcptrsinchnldown(resetpcptrsinchnldown),
.dispcbyte(dispcbyte),
.txdivsyncoutchnlup(txdivsyncoutchnlup),
.txdivsyncoutchnldown(txdivsyncoutchnldown),
.rdenableoutchnlup(rdenableoutchnlup),
.rdenableoutchnldown(rdenableoutchnldown),
.wrenableoutchnlup(wrenableoutchnlup),
.wrenableoutchnldown(wrenableoutchnldown),
.fifoselectoutchnlup(fifoselectoutchnlup),
.fifoselectoutchnldown(fifoselectoutchnldown),
.txfifordclkraw(txfifordclkraw),
.syncdatain(syncdatain),
.observablebyteserdesclock(observablebyteserdesclock)
);

defparam inst.prot_mode = prot_mode;
defparam inst.hip_mode = hip_mode;
defparam inst.pma_dw = pma_dw;
defparam inst.pcs_bypass = pcs_bypass;
defparam inst.phase_compensation_fifo = phase_compensation_fifo;
defparam inst.tx_compliance_controlled_disparity = tx_compliance_controlled_disparity;
defparam inst.force_kchar = force_kchar;
defparam inst.force_echar = force_echar;
defparam inst.byte_serializer = byte_serializer;
defparam inst.data_selection_8b10b_encoder_input = data_selection_8b10b_encoder_input;
defparam inst.eightb_tenb_disp_ctrl = eightb_tenb_disp_ctrl;
defparam inst.eightb_tenb_encoder = eightb_tenb_encoder;
defparam inst.prbs_gen = prbs_gen;
defparam inst.cid_pattern = cid_pattern;
defparam inst.cid_pattern_len = cid_pattern_len;
defparam inst.bist_gen = bist_gen;
defparam inst.bit_reversal = bit_reversal;
defparam inst.symbol_swap = symbol_swap;
defparam inst.polarity_inversion = polarity_inversion;
defparam inst.tx_bitslip = tx_bitslip;
defparam inst.agg_block_sel = agg_block_sel;
defparam inst.revloop_back_rm = revloop_back_rm;
defparam inst.phfifo_write_clk_sel = phfifo_write_clk_sel;
defparam inst.ctrl_plane_bonding_consumption = ctrl_plane_bonding_consumption;
defparam inst.bypass_pipeline_reg = bypass_pipeline_reg;
defparam inst.ctrl_plane_bonding_distribution = ctrl_plane_bonding_distribution;
defparam inst.test_mode = test_mode;
defparam inst.clock_gate_tx = clock_gate_tx;
defparam inst.self_switch_dw_scaling = self_switch_dw_scaling;
defparam inst.ctrl_plane_bonding_compensation = ctrl_plane_bonding_compensation;
defparam inst.refclk_b_clk_sel = refclk_b_clk_sel;
defparam inst.auto_speed_nego_gen2 = auto_speed_nego_gen2;
defparam inst.auto_speed_nego_gen3 = auto_speed_nego_gen3;
defparam inst.channel_number = channel_number;

endmodule	//stratixv_hssi_8g_tx_pcs

`timescale 1 ps/1 ps

module    stratixv_hssi_pipe_gen1_2    (
    pipetxclk,
    piperxclk,
    refclkb,
    txpipereset,
    rxpipereset,
    refclkbreset,
    rrdwidthrx,
    txdetectrxloopback,
    txelecidlein,
    powerdown,
    txdeemph,
    txmargin,
    txswingport,
    txdch,
    rxpolarity,
    sigdetni,
    rxvalid,
    rxelecidle,
    rxstatus,
    rxdch,
    phystatus,
    revloopback,
    polinvrx,
    txd,
    revloopbk,
    revloopbkpcsgen3,
    rxelectricalidlepcsgen3,
    txelecidlecomp,
    rindvrx,
    rmasterrx,
    speedchange,
    speedchangechnlup,
    speedchangechnldown,
    rxd,
    txelecidleout,
    txdetectrx,
    powerstate,
    rxfound,
    rxdetectvalid,
    rxelectricalidle,
    powerstatetransitiondone,
    powerstatetransitiondoneena,
    txdeemphint,
    txmarginint,
    txswingint,
    rxelectricalidleout,
    rxpolaritypcsgen3,
    polinvrxint,
    speedchangeout);

    parameter    channel_number = 0;
    parameter    prot_mode    =    "pipe_g1";
    parameter    hip_mode    =    "dis_hip";
    parameter    tx_pipe_enable    =    "dis_pipe_tx";
    parameter    rx_pipe_enable    =    "dis_pipe_rx";
    parameter    pipe_byte_de_serializer_en    =    "dont_care_bds";
    parameter    txswing    =    "dis_txswing";
    parameter    rxdetect_bypass    =    "dis_rxdetect_bypass";
    parameter    error_replace_pad    =    "replace_edb";
    parameter    ind_error_reporting    =    "dis_ind_error_reporting";
    parameter    phystatus_rst_toggle    =    "dis_phystatus_rst_toggle";
    parameter    elecidle_delay    =    "elec_idle_delay";
    parameter    elec_idle_delay_val    =    3'b0;
    parameter    phy_status_delay    =    "phystatus_delay";
    parameter    phystatus_delay_val    =    3'b0;
    parameter    ctrl_plane_bonding_consumption    =    "individual";
    parameter    byte_deserializer    =    "dis_bds";


    input    [0:0]    pipetxclk;
    input    [0:0]    piperxclk;
    input    [0:0]    refclkb;
    input    [0:0]    txpipereset;
    input    [0:0]    rxpipereset;
    input    [0:0]    refclkbreset;
    input    [0:0]    rrdwidthrx;
    input    [0:0]    txdetectrxloopback;
    input    [0:0]    txelecidlein;
    input    [1:0]    powerdown;
    input    [0:0]    txdeemph;
    input    [2:0]    txmargin;
    input    [0:0]    txswingport;
    input    [43:0]    txdch;
    input    [0:0]    rxpolarity;
    input    [0:0]    sigdetni;
    output    [0:0]    rxvalid;
    output    [0:0]    rxelecidle;
    output    [2:0]    rxstatus;
    output    [63:0]    rxdch;
    output    [0:0]    phystatus;
    input    [0:0]    revloopback;
    input    [0:0]    polinvrx;
    output    [43:0]    txd;
    output    [0:0]    revloopbk;
    input    [0:0]    revloopbkpcsgen3;
    input    [0:0]    rxelectricalidlepcsgen3;
    input    [0:0]    txelecidlecomp;
    input    [0:0]    rindvrx;
    input    [1:0]    rmasterrx;
    input    [0:0]    speedchange;
    input    [0:0]    speedchangechnlup;
    input    [0:0]    speedchangechnldown;
    input    [63:0]    rxd;
    output    [0:0]    txelecidleout;
    output    [0:0]    txdetectrx;
    output    [3:0]    powerstate;
    input    [0:0]    rxfound;
    input    [0:0]    rxdetectvalid;
    input    [0:0]    rxelectricalidle;
    input    [0:0]    powerstatetransitiondone;
    input    [0:0]    powerstatetransitiondoneena;
    output    [0:0]    txdeemphint;
    output    [2:0]    txmarginint;
    output    [0:0]    txswingint;
    output    [0:0]    rxelectricalidleout;
    input    [0:0]    rxpolaritypcsgen3;
    output    [0:0]    polinvrxint;
    output    [0:0]    speedchangeout;

    stratixv_hssi_pipe_gen1_2_encrypted inst (
        .pipetxclk(pipetxclk),
        .piperxclk(piperxclk),
        .refclkb(refclkb),
        .txpipereset(txpipereset),
        .rxpipereset(rxpipereset),
        .refclkbreset(refclkbreset),
        .rrdwidthrx(rrdwidthrx),
        .txdetectrxloopback(txdetectrxloopback),
        .txelecidlein(txelecidlein),
        .powerdown(powerdown),
        .txdeemph(txdeemph),
        .txmargin(txmargin),
        .txswingport(txswingport),
        .txdch(txdch),
        .rxpolarity(rxpolarity),
        .sigdetni(sigdetni),
        .rxvalid(rxvalid),
        .rxelecidle(rxelecidle),
        .rxstatus(rxstatus),
        .rxdch(rxdch),
        .phystatus(phystatus),
        .revloopback(revloopback),
        .polinvrx(polinvrx),
        .txd(txd),
        .revloopbk(revloopbk),
        .revloopbkpcsgen3(revloopbkpcsgen3),
        .rxelectricalidlepcsgen3(rxelectricalidlepcsgen3),
        .txelecidlecomp(txelecidlecomp),
        .rindvrx(rindvrx),
        .rmasterrx(rmasterrx),
        .speedchange(speedchange),
        .speedchangechnlup(speedchangechnlup),
        .speedchangechnldown(speedchangechnldown),
        .rxd(rxd),
        .txelecidleout(txelecidleout),
        .txdetectrx(txdetectrx),
        .powerstate(powerstate),
        .rxfound(rxfound),
        .rxdetectvalid(rxdetectvalid),
        .rxelectricalidle(rxelectricalidle),
        .powerstatetransitiondone(powerstatetransitiondone),
        .powerstatetransitiondoneena(powerstatetransitiondoneena),
        .txdeemphint(txdeemphint),
        .txmarginint(txmarginint),
        .txswingint(txswingint),
        .rxelectricalidleout(rxelectricalidleout),
        .rxpolaritypcsgen3(rxpolaritypcsgen3),
        .polinvrxint(polinvrxint),
        .speedchangeout(speedchangeout) );
    defparam inst.prot_mode = prot_mode;
    defparam inst.hip_mode = hip_mode;
    defparam inst.tx_pipe_enable = tx_pipe_enable;
    defparam inst.rx_pipe_enable = rx_pipe_enable;
    defparam inst.pipe_byte_de_serializer_en = pipe_byte_de_serializer_en;
    defparam inst.txswing = txswing;
    defparam inst.rxdetect_bypass = rxdetect_bypass;
    defparam inst.error_replace_pad = error_replace_pad;
    defparam inst.ind_error_reporting = ind_error_reporting;
    defparam inst.phystatus_rst_toggle = phystatus_rst_toggle;
    defparam inst.elecidle_delay = elecidle_delay;
    defparam inst.elec_idle_delay_val = elec_idle_delay_val;
    defparam inst.phy_status_delay = phy_status_delay;
    defparam inst.phystatus_delay_val = phystatus_delay_val;
    defparam inst.ctrl_plane_bonding_consumption = ctrl_plane_bonding_consumption;
    defparam inst.byte_deserializer = byte_deserializer;
	defparam inst.channel_number = channel_number;

endmodule //stratixv_hssi_pipe_gen1_2

`timescale 1 ps/1 ps

module    stratixv_hssi_pipe_gen3    (
    rcvdclk,
    txpmaclk,
    pcsdigclk,
    pllfixedclk,
    rtxgen3capen,
    rrxgen3capen,
    rtxdigclksel,
    rrxdigclksel,
    rxrstn,
    txrstn,
    scanmoden,
    pldasyncstatus,
    testout,
    gen3datasel,
    gen3clksel,
    pcsrst,
    dispcbyte,
    resetpcprts,
    shutdownclk,
    txdata,
    txdatak,
    txdataskip,
    txsynchdr,
    txblkstart,
    txelecidle,
    txdetectrxloopback,
    txcompliance,
    rxpolarity,
    powerdown,
    rate,
    txmargin,
    txdeemph,
    txswing,
    eidleinfersel,
    currentcoeff,
    currentrxpreset,
    rxupdatefc,
    rxdataskip,
    rxsynchdr,
    rxblkstart,
    rxvalid,
    phystatus,
    rxelecidle,
    rxstatus,
    rxdataint,
    rxdatakint,
    rxdataskipint,
    rxsynchdrint,
    rxblkstartint,
    txdataint,
    txdatakint,
    txdataskipint,
    txsynchdrint,
    txblkstartint,
    testinfei,
    eidetint,
    eipartialdetint,
    idetint,
    blkalgndint,
    clkcompinsertint,
    clkcompdeleteint,
    clkcompoverflint,
    clkcompundflint,
    errdecodeint,
    rcvlfsrchkint,
    errencodeint,
    rxpolarityint,
    revlpbkint,
    inferredrxvalidint,
    rxd8gpcsin,
    rxelecidle8gpcsin,
    pldltr,
    rxd8gpcsout,
    revlpbk8gpcsout,
    pmarxdetectvalid,
    pmarxfound,
    pmasignaldet,
    pmapcieswdone,
    pmapcieswitch,
    pmatxmargin,
    pmatxdeemph,
    pmatxswing,
    pmacurrentcoeff,
    pmacurrentrxpreset,
    pmatxelecidle,
    pmatxdetectrx,
    ppmeidleexit,
    pmaltr,
    pmaearlyeios,
    pmarxdetpd,
    bundlingindown,
    bundlingoutdown,
    rxpolarity8gpcsout,
    speedchangeg2,
    bundlingoutup,
    bundlinginup,
    masktxpll);

    parameter    channel_number = 0;
    parameter    mode    =    "pipe_g1";
    parameter    ctrl_plane_bonding    =    "individual";
    parameter    pipe_clk_sel    =    "func_clk";
    parameter    rate_match_pad_insertion    =    "dis_rm_fifo_pad_ins";
    parameter    ind_error_reporting    =    "dis_ind_error_reporting";
    parameter    phystatus_rst_toggle_g3    =    "dis_phystatus_rst_toggle_g3";
    parameter    phystatus_rst_toggle_g12    =    "dis_phystatus_rst_toggle";
    parameter    cdr_control    =    "en_cdr_ctrl";
    parameter    cid_enable    =    "en_cid_mode";
    parameter    parity_chk_ts1    =    "en_ts1_parity_chk";
    parameter    rxvalid_mask    =    "rxvalid_mask_en";
    parameter    ph_fifo_reg_mode    =    "phfifo_reg_mode_dis";
    parameter    test_mode_timers    =    "dis_test_mode_timers";
    parameter    inf_ei_enable    =    "dis_inf_ei";
    parameter    spd_chnge_g2_sel    =    "false";
    parameter    cp_up_mstr    =    "false";
    parameter    cp_dwn_mstr    =    "false";
    parameter    cp_cons_sel    =    "cp_cons_default";
    parameter    elecidle_delay_g12_data    =    3'b0;
    parameter    elecidle_delay_g12    =    "elecidle_delay_g12";
    parameter    elecidle_delay_g3_data    =    3'b0;
    parameter    elecidle_delay_g3    =    "elecidle_delay_g3";
    parameter    phy_status_delay_g12_data    =    3'b0;
    parameter    phy_status_delay_g12    =    "phy_status_delay_g12";
    parameter    phy_status_delay_g3_data    =    3'b0;
    parameter    phy_status_delay_g3    =    "phy_status_delay_g3";
    parameter    sigdet_wait_counter_data    =    8'b0;
    parameter    sigdet_wait_counter    =    "sigdet_wait_counter";
    parameter    data_mask_count_val    =    10'b0;
    parameter    data_mask_count    =    "data_mask_count";
    parameter    pma_done_counter_data    =    18'b0;
    parameter    pma_done_counter    =    "pma_done_count";
    parameter    pc_en_counter_data    =    5'b0;
    parameter    pc_en_counter    =    "pc_en_count";
    parameter    pc_rst_counter_data    =    4'b0;
    parameter    pc_rst_counter    =    "pc_rst_count";
    parameter    phfifo_flush_wait_data    =    6'b0;
    parameter    phfifo_flush_wait    =    "phfifo_flush_wait";
    parameter    asn_clk_enable    =    "false";
    parameter    free_run_clk_enable    =    "true";
    parameter    asn_enable    =    "dis_asn";


    input    [0:0]    rcvdclk;
    input    [0:0]    txpmaclk;
    input    [0:0]    pcsdigclk;
    input    [0:0]    pllfixedclk;
    input    [0:0]    rtxgen3capen;
    input    [0:0]    rrxgen3capen;
    input    [0:0]    rtxdigclksel;
    input    [0:0]    rrxdigclksel;
    input    [0:0]    rxrstn;
    input    [0:0]    txrstn;
    input    [0:0]    scanmoden;
    output    [5:0]    pldasyncstatus;
    output    [19:0]    testout;
    output    [0:0]    gen3datasel;
    output    [0:0]    gen3clksel;
    output    [0:0]    pcsrst;
    output    [0:0]    dispcbyte;
    output    [0:0]    resetpcprts;
    output    [0:0]    shutdownclk;
    input    [31:0]    txdata;
    input    [3:0]    txdatak;
    input    [0:0]    txdataskip;
    input    [1:0]    txsynchdr;
    input    [0:0]    txblkstart;
    input    [0:0]    txelecidle;
    input    [0:0]    txdetectrxloopback;
    input    [0:0]    txcompliance;
    input    [0:0]    rxpolarity;
    input    [1:0]    powerdown;
    input    [1:0]    rate;
    input    [2:0]    txmargin;
    input    [0:0]    txdeemph;
    input    [0:0]    txswing;
    input    [2:0]    eidleinfersel;
    input    [17:0]    currentcoeff;
    input    [2:0]    currentrxpreset;
    input    [0:0]    rxupdatefc;
    output    [3:0]    rxdataskip;
    output    [1:0]    rxsynchdr;
    output    [3:0]    rxblkstart;
    output    [0:0]    rxvalid;
    output    [0:0]    phystatus;
    output    [0:0]    rxelecidle;
    output    [2:0]    rxstatus;
    input    [31:0]    rxdataint;
    input    [3:0]    rxdatakint;
    input    [0:0]    rxdataskipint;
    input    [1:0]    rxsynchdrint;
    input    [0:0]    rxblkstartint;
    output    [31:0]    txdataint;
    output    [3:0]    txdatakint;
    output    [0:0]    txdataskipint;
    output    [1:0]    txsynchdrint;
    output    [0:0]    txblkstartint;
    output    [18:0]    testinfei;
    input    [0:0]    eidetint;
    input    [0:0]    eipartialdetint;
    input    [0:0]    idetint;
    input    [0:0]    blkalgndint;
    input    [0:0]    clkcompinsertint;
    input    [0:0]    clkcompdeleteint;
    input    [0:0]    clkcompoverflint;
    input    [0:0]    clkcompundflint;
    input    [0:0]    errdecodeint;
    input    [0:0]    rcvlfsrchkint;
    input    [0:0]    errencodeint;
    output    [0:0]    rxpolarityint;
    output    [0:0]    revlpbkint;
    output    [0:0]    inferredrxvalidint;
    input    [63:0]    rxd8gpcsin;
    input    [0:0]    rxelecidle8gpcsin;
    input    [0:0]    pldltr;
    output    [63:0]    rxd8gpcsout;
    output    [0:0]    revlpbk8gpcsout;
    input    [0:0]    pmarxdetectvalid;
    input    [0:0]    pmarxfound;
    input    [0:0]    pmasignaldet;
    input    [1:0]    pmapcieswdone;
    output    [1:0]    pmapcieswitch;
    output    [2:0]    pmatxmargin;
    output    [0:0]    pmatxdeemph;
    output    [0:0]    pmatxswing;
    output    [17:0]    pmacurrentcoeff;
    output    [2:0]    pmacurrentrxpreset;
    output    [0:0]    pmatxelecidle;
    output    [0:0]    pmatxdetectrx;
    output    [0:0]    ppmeidleexit;
    output    [0:0]    pmaltr;
    output    [0:0]    pmaearlyeios;
    output    [0:0]    pmarxdetpd;
    input    [9:0]    bundlingindown;
    output    [9:0]    bundlingoutdown;
    output    [0:0]    rxpolarity8gpcsout;
    input    [0:0]    speedchangeg2;
    output    [9:0]    bundlingoutup;
    input    [9:0]    bundlinginup;
    output    [0:0]    masktxpll;

    stratixv_hssi_pipe_gen3_encrypted inst (
        .rcvdclk(rcvdclk),
        .txpmaclk(txpmaclk),
        .pcsdigclk(pcsdigclk),
        .pllfixedclk(pllfixedclk),
        .rtxgen3capen(rtxgen3capen),
        .rrxgen3capen(rrxgen3capen),
        .rtxdigclksel(rtxdigclksel),
        .rrxdigclksel(rrxdigclksel),
        .rxrstn(rxrstn),
        .txrstn(txrstn),
        .scanmoden(scanmoden),
        .pldasyncstatus(pldasyncstatus),
        .testout(testout),
        .gen3datasel(gen3datasel),
        .gen3clksel(gen3clksel),
        .pcsrst(pcsrst),
        .dispcbyte(dispcbyte),
        .resetpcprts(resetpcprts),
        .shutdownclk(shutdownclk),
        .txdata(txdata),
        .txdatak(txdatak),
        .txdataskip(txdataskip),
        .txsynchdr(txsynchdr),
        .txblkstart(txblkstart),
        .txelecidle(txelecidle),
        .txdetectrxloopback(txdetectrxloopback),
        .txcompliance(txcompliance),
        .rxpolarity(rxpolarity),
        .powerdown(powerdown),
        .rate(rate),
        .txmargin(txmargin),
        .txdeemph(txdeemph),
        .txswing(txswing),
        .eidleinfersel(eidleinfersel),
        .currentcoeff(currentcoeff),
        .currentrxpreset(currentrxpreset),
        .rxupdatefc(rxupdatefc),
        .rxdataskip(rxdataskip),
        .rxsynchdr(rxsynchdr),
        .rxblkstart(rxblkstart),
        .rxvalid(rxvalid),
        .phystatus(phystatus),
        .rxelecidle(rxelecidle),
        .rxstatus(rxstatus),
        .rxdataint(rxdataint),
        .rxdatakint(rxdatakint),
        .rxdataskipint(rxdataskipint),
        .rxsynchdrint(rxsynchdrint),
        .rxblkstartint(rxblkstartint),
        .txdataint(txdataint),
        .txdatakint(txdatakint),
        .txdataskipint(txdataskipint),
        .txsynchdrint(txsynchdrint),
        .txblkstartint(txblkstartint),
        .testinfei(testinfei),
        .eidetint(eidetint),
        .eipartialdetint(eipartialdetint),
        .idetint(idetint),
        .blkalgndint(blkalgndint),
        .clkcompinsertint(clkcompinsertint),
        .clkcompdeleteint(clkcompdeleteint),
        .clkcompoverflint(clkcompoverflint),
        .clkcompundflint(clkcompundflint),
        .errdecodeint(errdecodeint),
        .rcvlfsrchkint(rcvlfsrchkint),
        .errencodeint(errencodeint),
        .rxpolarityint(rxpolarityint),
        .revlpbkint(revlpbkint),
        .inferredrxvalidint(inferredrxvalidint),
        .rxd8gpcsin(rxd8gpcsin),
        .rxelecidle8gpcsin(rxelecidle8gpcsin),
        .pldltr(pldltr),
        .rxd8gpcsout(rxd8gpcsout),
        .revlpbk8gpcsout(revlpbk8gpcsout),
        .pmarxdetectvalid(pmarxdetectvalid),
        .pmarxfound(pmarxfound),
        .pmasignaldet(pmasignaldet),
        .pmapcieswdone(pmapcieswdone),
        .pmapcieswitch(pmapcieswitch),
        .pmatxmargin(pmatxmargin),
        .pmatxdeemph(pmatxdeemph),
        .pmatxswing(pmatxswing),
        .pmacurrentcoeff(pmacurrentcoeff),
        .pmacurrentrxpreset(pmacurrentrxpreset),
        .pmatxelecidle(pmatxelecidle),
        .pmatxdetectrx(pmatxdetectrx),
        .ppmeidleexit(ppmeidleexit),
        .pmaltr(pmaltr),
        .pmaearlyeios(pmaearlyeios),
        .pmarxdetpd(pmarxdetpd),
        .bundlingindown(bundlingindown),
        .bundlingoutdown(bundlingoutdown),
        .rxpolarity8gpcsout(rxpolarity8gpcsout),
        .speedchangeg2(speedchangeg2),
        .bundlingoutup(bundlingoutup),
        .bundlinginup(bundlinginup),
        .masktxpll(masktxpll) );
    defparam inst.mode = mode;
    defparam inst.ctrl_plane_bonding = ctrl_plane_bonding;
    defparam inst.pipe_clk_sel = pipe_clk_sel;
    defparam inst.rate_match_pad_insertion = rate_match_pad_insertion;
    defparam inst.ind_error_reporting = ind_error_reporting;
    defparam inst.phystatus_rst_toggle_g3 = phystatus_rst_toggle_g3;
    defparam inst.phystatus_rst_toggle_g12 = phystatus_rst_toggle_g12;
    defparam inst.cdr_control = cdr_control;
    defparam inst.cid_enable = cid_enable;
    defparam inst.parity_chk_ts1 = parity_chk_ts1;
    defparam inst.rxvalid_mask = rxvalid_mask;
    defparam inst.ph_fifo_reg_mode = ph_fifo_reg_mode;
    defparam inst.test_mode_timers = test_mode_timers;
    defparam inst.inf_ei_enable = inf_ei_enable;
    defparam inst.spd_chnge_g2_sel = spd_chnge_g2_sel;
    defparam inst.cp_up_mstr = cp_up_mstr;
    defparam inst.cp_dwn_mstr = cp_dwn_mstr;
    defparam inst.cp_cons_sel = cp_cons_sel;
    defparam inst.elecidle_delay_g12_data = elecidle_delay_g12_data;
    defparam inst.elecidle_delay_g12 = elecidle_delay_g12;
    defparam inst.elecidle_delay_g3_data = elecidle_delay_g3_data;
    defparam inst.elecidle_delay_g3 = elecidle_delay_g3;
    defparam inst.phy_status_delay_g12_data = phy_status_delay_g12_data;
    defparam inst.phy_status_delay_g12 = phy_status_delay_g12;
    defparam inst.phy_status_delay_g3_data = phy_status_delay_g3_data;
    defparam inst.phy_status_delay_g3 = phy_status_delay_g3;
    defparam inst.sigdet_wait_counter_data = sigdet_wait_counter_data;
    defparam inst.sigdet_wait_counter = sigdet_wait_counter;
    defparam inst.data_mask_count_val = data_mask_count_val;
    defparam inst.data_mask_count = data_mask_count;
    defparam inst.pma_done_counter_data = pma_done_counter_data;
    defparam inst.pma_done_counter = pma_done_counter;
    defparam inst.pc_en_counter_data = pc_en_counter_data;
    defparam inst.pc_en_counter = pc_en_counter;
    defparam inst.pc_rst_counter_data = pc_rst_counter_data;
    defparam inst.pc_rst_counter = pc_rst_counter;
    defparam inst.phfifo_flush_wait_data = phfifo_flush_wait_data;
    defparam inst.phfifo_flush_wait = phfifo_flush_wait;
    defparam inst.asn_clk_enable = asn_clk_enable;
    defparam inst.free_run_clk_enable = free_run_clk_enable;
    defparam inst.asn_enable = asn_enable;
	defparam inst.channel_number = channel_number;

endmodule //stratixv_hssi_pipe_gen3

`timescale 1 ps/1 ps

module    stratixv_hssi_pma_cdr_refclk_select_mux    (
    calclk,
    ffplloutbot,
    ffpllouttop,
    pldclk,
    refiqclk0,
    refiqclk1,
    refiqclk10,
    refiqclk2,
    refiqclk3,
    refiqclk4,
    refiqclk5,
    refiqclk6,
    refiqclk7,
    refiqclk8,
    refiqclk9,
    rxiqclk0,
    rxiqclk1,
    rxiqclk10,
    rxiqclk2,
    rxiqclk3,
    rxiqclk4,
    rxiqclk5,
    rxiqclk6,
    rxiqclk7,
    rxiqclk8,
    rxiqclk9,
    clkout);

    parameter    lpm_type    =    "stratixv_hssi_pma_cdr_refclk_select_mux";
    parameter    channel_number    =    0;
    parameter    refclk_select    =    "ref_iqclk0";
    parameter    reference_clock_frequency    =    "0 ps";


    input    calclk;
    input    ffplloutbot;
    input    ffpllouttop;
    input    pldclk;
    input    refiqclk0;
    input    refiqclk1;
    input    refiqclk10;
    input    refiqclk2;
    input    refiqclk3;
    input    refiqclk4;
    input    refiqclk5;
    input    refiqclk6;
    input    refiqclk7;
    input    refiqclk8;
    input    refiqclk9;
    input    rxiqclk0;
    input    rxiqclk1;
    input    rxiqclk10;
    input    rxiqclk2;
    input    rxiqclk3;
    input    rxiqclk4;
    input    rxiqclk5;
    input    rxiqclk6;
    input    rxiqclk7;
    input    rxiqclk8;
    input    rxiqclk9;
    output    clkout;

    stratixv_hssi_pma_cdr_refclk_select_mux_encrypted inst (
        .calclk(calclk),
        .ffplloutbot(ffplloutbot),
        .ffpllouttop(ffpllouttop),
        .pldclk(pldclk),
        .refiqclk0(refiqclk0),
        .refiqclk1(refiqclk1),
        .refiqclk10(refiqclk10),
        .refiqclk2(refiqclk2),
        .refiqclk3(refiqclk3),
        .refiqclk4(refiqclk4),
        .refiqclk5(refiqclk5),
        .refiqclk6(refiqclk6),
        .refiqclk7(refiqclk7),
        .refiqclk8(refiqclk8),
        .refiqclk9(refiqclk9),
        .rxiqclk0(rxiqclk0),
        .rxiqclk1(rxiqclk1),
        .rxiqclk10(rxiqclk10),
        .rxiqclk2(rxiqclk2),
        .rxiqclk3(rxiqclk3),
        .rxiqclk4(rxiqclk4),
        .rxiqclk5(rxiqclk5),
        .rxiqclk6(rxiqclk6),
        .rxiqclk7(rxiqclk7),
        .rxiqclk8(rxiqclk8),
        .rxiqclk9(rxiqclk9),
        .clkout(clkout) );
    defparam inst.lpm_type = lpm_type;
    defparam inst.channel_number = channel_number;
    defparam inst.refclk_select = refclk_select;
    defparam inst.reference_clock_frequency = reference_clock_frequency;

endmodule //stratixv_hssi_pma_cdr_refclk_select_mux

`timescale 1 ps/1 ps

module stratixv_hssi_pma_rx_buf (
	ck0sigdet,
	datain,
	fined2aout,
	lpbkp,
	occalen,
	refclklpbk,
	rstn,
	rxqpipulldn,
	slpbk,
	dataout,
	nonuserfrompmaux,
	rdlpbkp,
	rxpadce,
	sd
);

	parameter lpm_type = "stxv_hssi_pma_rx_buf";
	parameter adce_pd = "false";
	parameter bypass_eqz_stages_123 = "all_stages_enabled";
	parameter eq_bw_sel = "bw_full_12p5";
	parameter input_vcm_sel = "high_vcm";
	parameter pdb_dfe = "false";
	parameter pdb_sd = "false";
	parameter qpi_enable = "false";
	parameter rx_dc_gain = "dc_gain_0db";
	parameter rx_sel_bias_source = "bias_vcmdrv";
	parameter sd_off = "clk_divrx_2";
	parameter sd_on = "data_pulse_6";
	parameter sd_threshold = "sdlv_30mv";
	parameter serial_loopback = "lpbkp_dis";
	parameter term_sel = "r_100ohm";
	parameter vccela_supply_voltage = "vccela_1p0v";
	parameter vcm_sel = "vtt_0p7v";
	parameter channel_number = 0;

	input			ck0sigdet;
	input			datain;
	input			fined2aout;
	input			lpbkp;
	input			occalen;
	input			refclklpbk;
	input			rstn;
	input			rxqpipulldn;
	input			slpbk;

	output			dataout;
	output			nonuserfrompmaux;
	output			rdlpbkp;
	output			rxpadce;
	output			sd;
	
stratixv_hssi_pma_rx_buf_encrypted inst (
.ck0sigdet(ck0sigdet),
.datain(datain),
.fined2aout(fined2aout),
.lpbkp(lpbkp),
.occalen(occalen),
.refclklpbk(refclklpbk),
.rstn(rstn),
.rxqpipulldn(rxqpipulldn),
.slpbk(slpbk),
.dataout(dataout),
.nonuserfrompmaux(nonuserfrompmaux),
.rdlpbkp(rdlpbkp),
.rxpadce(rxpadce),
.sd(sd)
);

defparam inst.lpm_type = lpm_type;
defparam inst.adce_pd = adce_pd;
defparam inst.bypass_eqz_stages_123 = bypass_eqz_stages_123;
defparam inst.eq_bw_sel = eq_bw_sel;
defparam inst.input_vcm_sel = input_vcm_sel;
defparam inst.pdb_dfe = pdb_dfe;
defparam inst.pdb_sd = pdb_sd;
defparam inst.qpi_enable = qpi_enable;
defparam inst.rx_dc_gain = rx_dc_gain;
defparam inst.rx_sel_bias_source = rx_sel_bias_source;
defparam inst.sd_off = sd_off;
defparam inst.sd_on = sd_on;
defparam inst.sd_threshold = sd_threshold;
defparam inst.serial_loopback = serial_loopback;
defparam inst.term_sel = term_sel;
defparam inst.vccela_supply_voltage = vccela_supply_voltage;
defparam inst.vcm_sel = vcm_sel;
defparam inst.channel_number = channel_number;

endmodule	//stratixv_hssi_pma_rx_buf

`timescale 1 ps/1 ps

module stratixv_hssi_pma_rx_deser (
	bslip,
	clk90b,
	clk270b,
	deven,
	dodd,
	pciesw,
	pfdmodelock,
	rstn,
	clk33pcs,
	clkdivrx,
	clkdivrxrx,
	dout,
	pciel,
	pciem
);

	parameter lpm_type = "stratixv_hssi_pma_rx_deser";
	parameter auto_negotiation = "false";
	parameter bit_slip_bypass = "false";
	parameter mode = 8;
	parameter sdclk_enable = "false";
	parameter vco_bypass = "vco_bypass_normal";
	parameter channel_number = 0;
	parameter clk_forward_only_mode = "false";

	input			bslip;
	input			clk90b;
	input			clk270b;
	input			deven;
	input			dodd;
	input	[1:0]	pciesw;
	input			pfdmodelock;
	input			rstn;

	output			clk33pcs;
	output			clkdivrx;
	output			clkdivrxrx;
	output	[39:0]	dout;
	output			pciel;
	output			pciem;

stratixv_hssi_pma_rx_deser_encrypted inst (
.bslip(bslip),
.clk90b(clk90b),
.clk270b(clk270b),
.deven(deven),
.dodd(dodd),
.pciesw(pciesw),
.pfdmodelock(pfdmodelock),
.rstn(rstn),
.clk33pcs(clk33pcs),
.clkdivrx(clkdivrx),
.clkdivrxrx(clkdivrxrx),
.dout(dout),
.pciel(pciel),
.pciem(pciem)
);

defparam inst.lpm_type = lpm_type;
defparam inst.auto_negotiation = auto_negotiation;
defparam inst.bit_slip_bypass = bit_slip_bypass;
defparam inst.mode = mode;
defparam inst.sdclk_enable = sdclk_enable;
defparam inst.vco_bypass = vco_bypass;
defparam inst.channel_number = channel_number;
defparam inst.clk_forward_only_mode = clk_forward_only_mode;

endmodule	//stratixv_hssi_pma_rx_deser

`timescale 1 ps/1 ps

module stratixv_hssi_pma_tx_buf (
	datain,
	rxdetclk,
	txdetrx,
	txelecidl,
	txqpipulldn,
	txqpipullup,
	compass,
	dataout,
	detecton,
	fixedclkout,
	nonuserfrompmaux,
	probepass,
	rxdetectvalid,
	rxfound
);

	parameter lpm_type = "stratixv_hssi_pma_tx_buf";
	parameter elec_idl_gate_ctrl = "true";
	parameter pre_emp_switching_ctrl_1st_post_tap = "fir_1pt_disabled";
	parameter pre_emp_switching_ctrl_2nd_post_tap = "fir_2pt_disabled";
	parameter pre_emp_switching_ctrl_pre_tap = "fir_pre_disabled";
	parameter qpi_en = "false";
	parameter rx_det = "mode_0";
	parameter rx_det_output_sel = "rx_det_pcie_out";
	parameter rx_det_pdb = "true";
	parameter sig_inv_2nd_tap = "false";
	parameter sig_inv_pre_tap = "false";
	parameter slew_rate_ctrl = "slew_30ps";
	parameter term_sel = "r_100ohm";
	parameter vod_switching_ctrl_main_tap = "fir_main_2p0ma";
	parameter channel_number = 0;

	input			datain;
	input			rxdetclk;
	input			txdetrx;
	input			txelecidl;
	input			txqpipulldn;
	input			txqpipullup;

	output			compass;
	output			dataout;
	output	[1:0]	detecton;
	output			fixedclkout;
	output			nonuserfrompmaux;
	output			probepass;
	output			rxdetectvalid;
	output			rxfound;
	
stratixv_hssi_pma_tx_buf_encrypted inst (
.datain(datain),
.rxdetclk(rxdetclk),
.txdetrx(txdetrx),
.txelecidl(txelecidl),
.txqpipulldn(txqpipulldn),
.txqpipullup(txqpipullup),
.compass(compass),
.dataout(dataout),
.detecton(detecton),
.fixedclkout(fixedclkout),
.nonuserfrompmaux(nonuserfrompmaux),
.probepass(probepass),
.rxdetectvalid(rxdetectvalid),
.rxfound(rxfound)
);

defparam inst.lpm_type = lpm_type;
defparam inst.elec_idl_gate_ctrl = elec_idl_gate_ctrl;
defparam inst.pre_emp_switching_ctrl_1st_post_tap = pre_emp_switching_ctrl_1st_post_tap;
defparam inst.pre_emp_switching_ctrl_2nd_post_tap = pre_emp_switching_ctrl_2nd_post_tap;
defparam inst.pre_emp_switching_ctrl_pre_tap = pre_emp_switching_ctrl_pre_tap;
defparam inst.qpi_en = qpi_en;
defparam inst.rx_det = rx_det;
defparam inst.rx_det_output_sel = rx_det_output_sel;
defparam inst.rx_det_pdb = rx_det_pdb;
defparam inst.sig_inv_2nd_tap = sig_inv_2nd_tap;
defparam inst.sig_inv_pre_tap = sig_inv_pre_tap;
defparam inst.slew_rate_ctrl = slew_rate_ctrl;
defparam inst.term_sel = term_sel;
defparam inst.vod_switching_ctrl_main_tap = vod_switching_ctrl_main_tap;
defparam inst.channel_number = channel_number;

endmodule	//stratixv_hssi_pma_tx_buf

`timescale 1 ps/1 ps

module stratixv_hssi_pma_tx_cgb (
	clkbcdr1adj,
	clkbcdr1loc,
	clkbcdrloc,
	clkbdnseg,
	clkbffpll,
	clkblcb,
	clkblct,
	clkbupseg,
	clkcdr1adj,
	clkcdr1loc,
	clkcdrloc,
	clkdnseg,
	clkffpll,
	clklcb,
	clklct,
	clkupseg,
	cpulsex6adj,
	cpulsex6loc,
	cpulsexndn,
	cpulsexnup,
	hfclknx6adj,
	hfclknx6loc,
	hfclknxndn,
	hfclknxnup,
	hfclkpx6adj,
	hfclkpx6loc,
	hfclkpxndn,
	hfclkpxnup,
	lfclknx6adj,
	lfclknx6loc,
	lfclknxndn,
	lfclknxnup,
	lfclkpx6adj,
	lfclkpx6loc,
	lfclkpxndn,
	lfclkpxnup,
	pciesw,
	pclk0x6adj,
	pclk0x6loc,
	pclk0xndn,
	pclk0xnup,
	pclk1x6adj,
	pclk1x6loc,
	pclk1xndn,
	pclk1xnup,
	pclkx6adj,
	pclkx6loc,
	pclkxndn,
	pclkxnup,
	rxclk,
	txpmarstb,
	txpmasyncp,
	xnresetin,
	cpulse,
	cpulseout,
	hfclkn,
	hfclknout,
	hfclkp,
	hfclkpout,
	lfclkn,
	lfclknout,
	lfclkp,
	lfclkpout,
	pcieswdone,
	pclk0,
	pclk0out,
	pclk1,
	pclk1out,
	pclk,
	pclkout,
	rxiqclk,
	xnresetout
);

	parameter lpm_type = "stratixv_hssi_pma_tx_cgb";
	parameter auto_negotiation = "false";
	parameter x1_div_m_sel = 1;
	parameter channel_number = 0;
	parameter data_rate = "";
	parameter mode = 8;
	parameter rx_iqclk_sel = "cgb_x1_n_div";
	parameter tx_mux_power_down = "normal";
	parameter x1_clock_source_sel = "x1_clk_unused";
	parameter xn_clock_source_sel = "cgb_xn_unused";
	parameter xn_network_driver = "enable_clock_entwork_driver";
	parameter cgb_iqclk_sel = "cgb_x1_n_div";
	parameter ht_delay_enable = "false";

	input			clkbcdr1adj;
	input			clkbcdr1loc;
	input			clkbcdrloc;
	input			clkbdnseg;
	input			clkbffpll;
	input			clkblcb;
	input			clkblct;
	input			clkbupseg;
	input			clkcdr1adj;
	input			clkcdr1loc;
	input			clkcdrloc;
	input			clkdnseg;
	input			clkffpll;
	input			clklcb;
	input			clklct;
	input			clkupseg;
	input			cpulsex6adj;
	input			cpulsex6loc;
	input			cpulsexndn;
	input			cpulsexnup;
	input			hfclknx6adj;
	input			hfclknx6loc;
	input			hfclknxndn;
	input			hfclknxnup;
	input			hfclkpx6adj;
	input			hfclkpx6loc;
	input			hfclkpxndn;
	input			hfclkpxnup;
	input			lfclknx6adj;
	input			lfclknx6loc;
	input			lfclknxndn;
	input			lfclknxnup;
	input			lfclkpx6adj;
	input			lfclkpx6loc;
	input			lfclkpxndn;
	input			lfclkpxnup;
	input	[1:0]	pciesw;
	input			pclk0x6adj;
	input			pclk0x6loc;
	input			pclk0xndn;
	input			pclk0xnup;
	input			pclk1x6adj;
	input			pclk1x6loc;
	input			pclk1xndn;
	input			pclk1xnup;
	input	[2:0]	pclkx6adj;
	input	[2:0]	pclkx6loc;
	input	[2:0]	pclkxndn;
	input	[2:0]	pclkxnup;
	input			rxclk;
	input			txpmarstb;
	input			txpmasyncp;
	input			xnresetin;

	output			cpulse;
	output			cpulseout;
	output			hfclkn;
	output			hfclknout;
	output			hfclkp;
	output			hfclkpout;
	output			lfclkn;
	output			lfclknout;
	output			lfclkp;
	output			lfclkpout;
	output	[1:0]	pcieswdone;
	output			pclk0;
	output			pclk0out;
	output			pclk1;
	output			pclk1out;
	output	[2:0]	pclk;
	output	[2:0]	pclkout;
	output			rxiqclk;
	output			xnresetout;
	
stratixv_hssi_pma_tx_cgb_encrypted inst (
.clkbcdr1adj(clkbcdr1adj),
.clkbcdr1loc(clkbcdr1loc),
.clkbcdrloc(clkbcdrloc),
.clkbdnseg(clkbdnseg),
.clkbffpll(clkbffpll),
.clkblcb(clkblcb),
.clkblct(clkblct),
.clkbupseg(clkbupseg),
.clkcdr1adj(clkcdr1adj),
.clkcdr1loc(clkcdr1loc),
.clkcdrloc(clkcdrloc),
.clkdnseg(clkdnseg),
.clkffpll(clkffpll),
.clklcb(clklcb),
.clklct(clklct),
.clkupseg(clkupseg),
.cpulsex6adj(cpulsex6adj),
.cpulsex6loc(cpulsex6loc),
.cpulsexndn(cpulsexndn),
.cpulsexnup(cpulsexnup),
.hfclknx6adj(hfclknx6adj),
.hfclknx6loc(hfclknx6loc),
.hfclknxndn(hfclknxndn),
.hfclknxnup(hfclknxnup),
.hfclkpx6adj(hfclkpx6adj),
.hfclkpx6loc(hfclkpx6loc),
.hfclkpxndn(hfclkpxndn),
.hfclkpxnup(hfclkpxnup),
.lfclknx6adj(lfclknx6adj),
.lfclknx6loc(lfclknx6loc),
.lfclknxndn(lfclknxndn),
.lfclknxnup(lfclknxnup),
.lfclkpx6adj(lfclkpx6adj),
.lfclkpx6loc(lfclkpx6loc),
.lfclkpxndn(lfclkpxndn),
.lfclkpxnup(lfclkpxnup),
.pciesw(pciesw),
.pclk0x6adj(pclk0x6adj),
.pclk0x6loc(pclk0x6loc),
.pclk0xndn(pclk0xndn),
.pclk0xnup(pclk0xnup),
.pclk1x6adj(pclk1x6adj),
.pclk1x6loc(pclk1x6loc),
.pclk1xndn(pclk1xndn),
.pclk1xnup(pclk1xnup),
.pclkx6adj(pclkx6adj),
.pclkx6loc(pclkx6loc),
.pclkxndn(pclkxndn),
.pclkxnup(pclkxnup),
.rxclk(rxclk),
.txpmarstb(txpmarstb),
.txpmasyncp(txpmasyncp),
.xnresetin(xnresetin),
.cpulse(cpulse),
.cpulseout(cpulseout),
.hfclkn(hfclkn),
.hfclknout(hfclknout),
.hfclkp(hfclkp),
.hfclkpout(hfclkpout),
.lfclkn(lfclkn),
.lfclknout(lfclknout),
.lfclkp(lfclkp),
.lfclkpout(lfclkpout),
.pcieswdone(pcieswdone),
.pclk0(pclk0),
.pclk0out(pclk0out),
.pclk1(pclk1),
.pclk1out(pclk1out),
.pclk(pclk),
.pclkout(pclkout),
.rxiqclk(rxiqclk),
.xnresetout(xnresetout)
);

defparam inst.lpm_type = lpm_type;
defparam inst.auto_negotiation = auto_negotiation;
defparam inst.x1_div_m_sel = x1_div_m_sel;
defparam inst.data_rate = data_rate;
defparam inst.mode = mode;
defparam inst.rx_iqclk_sel = rx_iqclk_sel;
defparam inst.tx_mux_power_down = tx_mux_power_down;
defparam inst.x1_clock_source_sel = x1_clock_source_sel;
defparam inst.xn_clock_source_sel = xn_clock_source_sel;
defparam inst.xn_network_driver = xn_network_driver;
defparam inst.cgb_iqclk_sel = cgb_iqclk_sel;
defparam inst.ht_delay_enable = ht_delay_enable;
defparam inst.channel_number = channel_number;

endmodule //stratixv_hssi_pma_tx_cgb

`timescale 1 ps/1 ps

module stratixv_hssi_pma_tx_ser (
	cpulse,
	datain,
	hfclk,
	hfclkn,
	lfclk,
	lfclkn,
	pciesw,
	pclk0,
	pclk1,
	pclk2,
	pclk,
	rstn,
	clkdivtx,
	dataout,
	div5,
	lbvop
);

	parameter lpm_type = "stratixv_hssi_pma_tx_ser";
	parameter auto_negotiation = "false";
	parameter clk_divtx_deskew = "deskew_delay1";
	parameter mode = 8;
	parameter post_tap_1_en = "false";
	parameter post_tap_2_en = "false";
	parameter pre_tap_en = "false";
	parameter ser_loopback = "false";
	parameter pclksel = "local_pclk";
	parameter channel_number = 0;
	parameter clk_forward_only_mode = "false";

	input			cpulse;
	input	[39:0]	datain;
	input			hfclk;
	input			hfclkn;
	input			lfclk;
	input			lfclkn;
	input	[1:0]	pciesw;
	input			pclk0;
	input			pclk1;
	input			pclk2;
	input	[2:0]	pclk;
	input			rstn;

	output			clkdivtx;
	output			dataout;
	output			div5;
	output			lbvop;
	
stratixv_hssi_pma_tx_ser_encrypted inst (
.cpulse(cpulse),
.datain(datain),
.hfclk(hfclk),
.hfclkn(hfclkn),
.lfclk(lfclk),
.lfclkn(lfclkn),
.pciesw(pciesw),
.pclk0(pclk0),
.pclk1(pclk1),
.pclk2(pclk2),
.pclk(pclk),
.rstn(rstn),
.clkdivtx(clkdivtx),
.dataout(dataout),
.div5(div5),
.lbvop(lbvop)
);

defparam inst.lpm_type = lpm_type;
defparam inst.auto_negotiation = auto_negotiation;
defparam inst.clk_divtx_deskew = clk_divtx_deskew;
defparam inst.mode = mode;
defparam inst.post_tap_1_en = post_tap_1_en;
defparam inst.post_tap_2_en = post_tap_2_en;
defparam inst.pre_tap_en = pre_tap_en;
defparam inst.ser_loopback = ser_loopback;
defparam inst.pclksel = pclksel;
defparam inst.clk_forward_only_mode = clk_forward_only_mode;
defparam inst.channel_number = channel_number;

endmodule //stratixv_hssi_pma_tx_ser

`timescale 1 ps/1 ps

module stratixv_hssi_common_pcs_pma_interface (
	fref,
	clklow,
	pmapcieswdone,
	pmarxfound,
	pmarxdetectvalid,
	pmahclk,
	pldoffcalen,
	aggrcvdclkagg,
	aggtxdatats,
	aggtxctlts,
	aggfiforstrdqd,
	aggendskwqd,
	aggendskwrdptrs,
	aggalignstatus,
	aggalignstatussync0,
	aggcgcomprddall,
	aggcgcompwrall,
	aggfifordincomp0,
	aggdelcondmet0,
	agginsertincomplete0,
	aggfifoovr0,
	agglatencycomp0,
	aggrxdatars,
	aggrxcontrolrs,
	aggrcvdclkaggtoporbot,
	aggtxdatatstoporbot,
	aggtxctltstoporbot,
	aggfiforstrdqdtoporbot,
	aggendskwqdtoporbot,
	aggendskwrdptrstoporbot,
	aggalignstatustoporbot,
	aggalignstatussync0toporbot,
	aggcgcomprddalltoporbot,
	aggcgcompwralltoporbot,
	aggfifordincomp0toporbot,
	aggdelcondmet0toporbot,
	agginsertincomplete0toporbot,
	aggfifoovr0toporbot,
	agglatencycomp0toporbot,
	aggrxdatarstoporbot,
	aggrxcontrolrstoporbot,
	pcsgen3pmapcieswitch,
	pcsgen3pmatxmargin,
	pcsgen3pmatxdeemph,
	pcsgen3pmatxswing,
	pcsgen3pmacurrentcoeff,
	pcsgen3pmacurrentrxpreset,
	pcsgen3pmatxelecidle,
	pcsgen3pmatxdetectrx,
	pcsgen3ppmeidleexit,
	pcsgen3pmaltr,
	pcsgen3pmaearlyeios,
	pcs8gpcieswitch,
	pcs8gtxelecidle,
	pcs8gtxdetectrx,
	pcs8gearlyeios,
	pcs8gtxdeemphpma,
	pcs8gtxmarginpma,
	pcs8gtxswingpma,
	pcs8gltrpma,
	pcs8geidleexit,
	pcsaggtxpcsrst,
	pcsaggrxpcsrst,
	pcsaggtxdatatc,
	pcsaggtxctltc,
	pcsaggrdenablesync,
	pcsaggsyncstatus,
	pcsaggaligndetsync,
	pcsaggrdalign,
	pcsaggalignstatussync,
	pcsaggfifordoutcomp,
	pcsaggcgcomprddout,
	pcsaggcgcompwrout,
	pcsaggdelcondmetout,
	pcsaggfifoovrout,
	pcsagglatencycompout,
	pcsagginsertincompleteout,
	pcsaggdecdatavalid,
	pcsaggdecdata,
	pcsaggdecctl,
	pcsaggrunningdisp,
	pldrxclkslip,
	pldhardreset,
	pcsscanmoden,
	pcsscanshiftn,
	pcsrefclkdig,
	pcsaggscanmoden,
	pcsaggscanshiftn,
	pcsaggrefclkdig,
	pcsgen3gen3datasel,
	pldlccmurstb,
	pmaoffcaldonein,
	pmarxpmarstb,
	pmahardreset,
	freqlock,
	pmapcieswitch,
	pmaearlyeios,
	pmatxdetectrx,
	pmatxelecidle,
	pmatxdeemph,
	pmatxswing,
	pmatxmargin,
	pmacurrentcoeff,
	pmacurrentrxpreset,
	pmaoffcaldoneout,
	pmalccmurstb,
	pmaltr,
	aggtxpcsrst,
	aggrxpcsrst,
	aggtxdatatc,
	aggtxctltc,
	aggrdenablesync,
	aggsyncstatus,
	aggaligndetsync,
	aggrdalign,
	aggalignstatussync,
	aggfifordoutcomp,
	aggcgcomprddout,
	aggcgcompwrout,
	aggdelcondmetout,
	aggfifoovrout,
	agglatencycompout,
	agginsertincompleteout,
	aggdecdatavalid,
	aggdecdata,
	aggdecctl,
	aggrunningdisp,
	pcsgen3pmarxdetectvalid,
	pcsgen3pmarxfound,
	pcsgen3pmapcieswdone,
	pcsgen3pllfixedclk,
	pcsaggrcvdclkagg,
	pcsaggtxdatats,
	pcsaggtxctlts,
	pcsaggfiforstrdqd,
	pcsaggendskwqd,
	pcsaggendskwrdptrs,
	pcsaggalignstatus,
	pcsaggalignstatussync0,
	pcsaggcgcomprddall,
	pcsaggcgcompwrall,
	pcsaggfifordincomp0,
	pcsaggdelcondmet0,
	pcsagginsertincomplete0,
	pcsaggfifoovr0,
	pcsagglatencycomp0,
	pcsaggrxdatars,
	pcsaggrxcontrolrs,
	pcsaggrcvdclkaggtoporbot,
	pcsaggtxdatatstoporbot,
	pcsaggtxctltstoporbot,
	pcsaggfiforstrdqdtoporbot,
	pcsaggendskwqdtoporbot,
	pcsaggendskwrdptrstoporbot,
	pcsaggalignstatustoporbot,
	pcsaggalignstatussync0toporbot,
	pcsaggcgcomprddalltoporbot,
	pcsaggcgcompwralltoporbot,
	pcsaggfifordincomp0toporbot,
	pcsaggdelcondmet0toporbot,
	pcsagginsertincomplete0toporbot,
	pcsaggfifoovr0toporbot,
	pcsagglatencycomp0toporbot,
	pcsaggrxdatarstoporbot,
	pcsaggrxcontrolrstoporbot,
	pcs8grxdetectvalid,
	pcs8gpmarxfound,
	pcs8ggen2ngen1,
	pcs8gpowerstatetransitiondone,
	ppmcntlatch,
	pldhclkout,
	aggscanmoden,
	aggscanshiftn,
	aggrefclkdig,
	pmaoffcalen,
	pmafrefout,
	pmaclklowout
);

parameter channel_number = 0;
parameter lpm_type = "stratixv_hssi_common_pcs_pma_interface";
parameter auto_speed_ena = "dis_auto_speed_ena";
parameter force_freqdet = "force_freqdet_dis";
parameter func_mode = "disable";
parameter pcie_gen3_cap = "non_pcie_gen3_cap";
parameter pipe_if_g3pcs = "pipe_if_8gpcs";
parameter pma_if_dft_en = "dft_dis";
parameter pma_if_dft_val = "dft_0";
parameter ppm_cnt_rst = "ppm_cnt_rst_dis";
parameter ppm_deassert_early = "deassert_early_dis";
parameter ppm_gen1_2_cnt = "cnt_32k";
parameter ppm_post_eidle_delay = "cnt_200_cycles";
parameter ppmsel = "ppmsel_default";
parameter prot_mode = "disabled_prot_mode";
parameter refclk_dig_sel = "refclk_dig_dis";
parameter selectpcs = "eight_g_pcs";
parameter sup_mode = "full_mode";

// I/O ports
input			fref;
input			clklow;
input [1:0]		pmapcieswdone;
input			pmarxfound;
input			pmarxdetectvalid;
input			pmahclk;
input			pldoffcalen;
input			aggrcvdclkagg;
input [7:0]		aggtxdatats;
input			aggtxctlts;
input			aggfiforstrdqd;
input			aggendskwqd;
input			aggendskwrdptrs;
input			aggalignstatus;
input			aggalignstatussync0;
input			aggcgcomprddall;
input			aggcgcompwrall;
input			aggfifordincomp0;
input			aggdelcondmet0;
input			agginsertincomplete0;
input			aggfifoovr0;
input			agglatencycomp0;
input [7:0]		aggrxdatars;
input			aggrxcontrolrs;
input			aggrcvdclkaggtoporbot;
input [7:0]		aggtxdatatstoporbot;
input			aggtxctltstoporbot;
input			aggfiforstrdqdtoporbot;
input			aggendskwqdtoporbot;
input			aggendskwrdptrstoporbot;
input			aggalignstatustoporbot;
input			aggalignstatussync0toporbot;
input			aggcgcomprddalltoporbot;
input			aggcgcompwralltoporbot;
input			aggfifordincomp0toporbot;
input			aggdelcondmet0toporbot;
input			agginsertincomplete0toporbot;
input			aggfifoovr0toporbot;
input			agglatencycomp0toporbot;
input [7:0]		aggrxdatarstoporbot;
input			aggrxcontrolrstoporbot;
input [1:0]		pcsgen3pmapcieswitch;
input [2:0]		pcsgen3pmatxmargin;
input			pcsgen3pmatxdeemph;
input			pcsgen3pmatxswing;
input [17:0]	pcsgen3pmacurrentcoeff;
input [2:0]		pcsgen3pmacurrentrxpreset;
input			pcsgen3pmatxelecidle;
input			pcsgen3pmatxdetectrx;
input			pcsgen3ppmeidleexit;
input			pcsgen3pmaltr;
input			pcsgen3pmaearlyeios;
input			pcs8gpcieswitch;
input			pcs8gtxelecidle;
input			pcs8gtxdetectrx;
input			pcs8gearlyeios;
input			pcs8gtxdeemphpma;
input [2:0]		pcs8gtxmarginpma;
input			pcs8gtxswingpma;
input			pcs8gltrpma;
input			pcs8geidleexit;
input			pcsaggtxpcsrst;
input			pcsaggrxpcsrst;
input [7:0]		pcsaggtxdatatc;
input			pcsaggtxctltc;
input			pcsaggrdenablesync;
input			pcsaggsyncstatus;
input [1:0]		pcsaggaligndetsync;
input [1:0]		pcsaggrdalign;
input			pcsaggalignstatussync;
input			pcsaggfifordoutcomp;
input [1:0]		pcsaggcgcomprddout;
input [1:0]		pcsaggcgcompwrout;
input			pcsaggdelcondmetout;
input			pcsaggfifoovrout;
input			pcsagglatencycompout;
input			pcsagginsertincompleteout;
input			pcsaggdecdatavalid;
input [7:0]		pcsaggdecdata;
input			pcsaggdecctl;
input [1:0]		pcsaggrunningdisp;
input			pldrxclkslip;
input			pldhardreset;
input			pcsscanmoden;
input			pcsscanshiftn;
input			pcsrefclkdig;
input			pcsaggscanmoden;
input			pcsaggscanshiftn;
input			pcsaggrefclkdig;
input			pcsgen3gen3datasel;
input			pldlccmurstb;
input			pmaoffcaldonein;
input			pmarxpmarstb;
//input [5:0]		rcomppmsel;
//input			rcomforcehigh;
//input			rcomforcelow;
//input			rcomppmcntreset;
//input			rcomautospeedena;
//input			rcomppmgen12xcnten;
//input			rcomppmposteidledel;
//input			rcomrefclkdigsel;
//input			rcompciegen3cap;
	
output			pmahardreset;
output			freqlock;
output [1:0]	pmapcieswitch;
output			pmaearlyeios;
output			pmatxdetectrx;
output			pmatxelecidle;
output			pmatxdeemph;
output			pmatxswing;
output [2:0]	pmatxmargin;
output [17:0]	pmacurrentcoeff;
output [2:0]	pmacurrentrxpreset;
output			pmaoffcaldoneout;
output			pmalccmurstb;
output			pmaltr;
output			aggtxpcsrst;
output			aggrxpcsrst;
output [7:0]	aggtxdatatc;
output			aggtxctltc;
output			aggrdenablesync;
output			aggsyncstatus;
output [1:0]	aggaligndetsync;
output [1:0]	aggrdalign;
output			aggalignstatussync;
output			aggfifordoutcomp;
output [1:0]	aggcgcomprddout;
output [1:0]	aggcgcompwrout;
output			aggdelcondmetout;
output			aggfifoovrout;
output			agglatencycompout;
output			agginsertincompleteout;
output			aggdecdatavalid;
output [7:0]	aggdecdata;
output			aggdecctl;
output [1:0]	aggrunningdisp;
output			pcsgen3pmarxdetectvalid;
output			pcsgen3pmarxfound;
output [1:0]	pcsgen3pmapcieswdone;
output			pcsgen3pllfixedclk;
output			pcsaggrcvdclkagg;
output [7:0]	pcsaggtxdatats;
output			pcsaggtxctlts;
output			pcsaggfiforstrdqd;
output			pcsaggendskwqd;
output			pcsaggendskwrdptrs;
output			pcsaggalignstatus;
output			pcsaggalignstatussync0;
output			pcsaggcgcomprddall;
output			pcsaggcgcompwrall;
output			pcsaggfifordincomp0;
output			pcsaggdelcondmet0;
output			pcsagginsertincomplete0;
output			pcsaggfifoovr0;
output			pcsagglatencycomp0;
output [7:0]	pcsaggrxdatars;
output			pcsaggrxcontrolrs;
output			pcsaggrcvdclkaggtoporbot;
output [7:0]	pcsaggtxdatatstoporbot;
output			pcsaggtxctltstoporbot;
output			pcsaggfiforstrdqdtoporbot;
output			pcsaggendskwqdtoporbot;
output			pcsaggendskwrdptrstoporbot;
output			pcsaggalignstatustoporbot;
output			pcsaggalignstatussync0toporbot;
output			pcsaggcgcomprddalltoporbot;
output			pcsaggcgcompwralltoporbot;
output			pcsaggfifordincomp0toporbot;
output			pcsaggdelcondmet0toporbot;
output			pcsagginsertincomplete0toporbot;
output			pcsaggfifoovr0toporbot;
output			pcsagglatencycomp0toporbot;
output [7:0]	pcsaggrxdatarstoporbot;
output			pcsaggrxcontrolrstoporbot;
output			pcs8grxdetectvalid;
output			pcs8gpmarxfound;
output			pcs8ggen2ngen1;
output			pcs8gpowerstatetransitiondone;
output [7:0]	ppmcntlatch;
output			pldhclkout;
output			aggscanmoden;
output			aggscanshiftn;
output			aggrefclkdig;
output			pmaoffcalen;
output			pmafrefout;
output			pmaclklowout;

stratixv_hssi_common_pcs_pma_interface_encrypted inst (
.fref(fref),
.clklow(clklow),
.pmapcieswdone(pmapcieswdone),
.pmarxfound(pmarxfound),
.pmarxdetectvalid(pmarxdetectvalid),
.pmahclk(pmahclk),
.pldoffcalen(pldoffcalen),
.aggrcvdclkagg(aggrcvdclkagg),
.aggtxdatats(aggtxdatats),
.aggtxctlts(aggtxctlts),
.aggfiforstrdqd(aggfiforstrdqd),
.aggendskwqd(aggendskwqd),
.aggendskwrdptrs(aggendskwrdptrs),
.aggalignstatus(aggalignstatus),
.aggalignstatussync0(aggalignstatussync0),
.aggcgcomprddall(aggcgcomprddall),
.aggcgcompwrall(aggcgcompwrall),
.aggfifordincomp0(aggfifordincomp0),
.aggdelcondmet0(aggdelcondmet0),
.agginsertincomplete0(agginsertincomplete0),
.aggfifoovr0(aggfifoovr0),
.agglatencycomp0(agglatencycomp0),
.aggrxdatars(aggrxdatars),
.aggrxcontrolrs(aggrxcontrolrs),
.aggrcvdclkaggtoporbot(aggrcvdclkaggtoporbot),
.aggtxdatatstoporbot(aggtxdatatstoporbot),
.aggtxctltstoporbot(aggtxctltstoporbot),
.aggfiforstrdqdtoporbot(aggfiforstrdqdtoporbot),
.aggendskwqdtoporbot(aggendskwqdtoporbot),
.aggendskwrdptrstoporbot(aggendskwrdptrstoporbot),
.aggalignstatustoporbot(aggalignstatustoporbot),
.aggalignstatussync0toporbot(aggalignstatussync0toporbot),
.aggcgcomprddalltoporbot(aggcgcomprddalltoporbot),
.aggcgcompwralltoporbot(aggcgcompwralltoporbot),
.aggfifordincomp0toporbot(aggfifordincomp0toporbot),
.aggdelcondmet0toporbot(aggdelcondmet0toporbot),
.agginsertincomplete0toporbot(agginsertincomplete0toporbot),
.aggfifoovr0toporbot(aggfifoovr0toporbot),
.agglatencycomp0toporbot(agglatencycomp0toporbot),
.aggrxdatarstoporbot(aggrxdatarstoporbot),
.aggrxcontrolrstoporbot(aggrxcontrolrstoporbot),
.pcsgen3pmapcieswitch(pcsgen3pmapcieswitch),
.pcsgen3pmatxmargin(pcsgen3pmatxmargin),
.pcsgen3pmatxdeemph(pcsgen3pmatxdeemph),
.pcsgen3pmatxswing(pcsgen3pmatxswing),
.pcsgen3pmacurrentcoeff(pcsgen3pmacurrentcoeff),
.pcsgen3pmacurrentrxpreset(pcsgen3pmacurrentrxpreset),
.pcsgen3pmatxelecidle(pcsgen3pmatxelecidle),
.pcsgen3pmatxdetectrx(pcsgen3pmatxdetectrx),
.pcsgen3ppmeidleexit(pcsgen3ppmeidleexit),
.pcsgen3pmaltr(pcsgen3pmaltr),
.pcsgen3pmaearlyeios(pcsgen3pmaearlyeios),
.pcs8gpcieswitch(pcs8gpcieswitch),
.pcs8gtxelecidle(pcs8gtxelecidle),
.pcs8gtxdetectrx(pcs8gtxdetectrx),
.pcs8gearlyeios(pcs8gearlyeios),
.pcs8gtxdeemphpma(pcs8gtxdeemphpma),
.pcs8gtxmarginpma(pcs8gtxmarginpma),
.pcs8gtxswingpma(pcs8gtxswingpma),
.pcs8gltrpma(pcs8gltrpma),
.pcs8geidleexit(pcs8geidleexit),
.pcsaggtxpcsrst(pcsaggtxpcsrst),
.pcsaggrxpcsrst(pcsaggrxpcsrst),
.pcsaggtxdatatc(pcsaggtxdatatc),
.pcsaggtxctltc(pcsaggtxctltc),
.pcsaggrdenablesync(pcsaggrdenablesync),
.pcsaggsyncstatus(pcsaggsyncstatus),
.pcsaggaligndetsync(pcsaggaligndetsync),
.pcsaggrdalign(pcsaggrdalign),
.pcsaggalignstatussync(pcsaggalignstatussync),
.pcsaggfifordoutcomp(pcsaggfifordoutcomp),
.pcsaggcgcomprddout(pcsaggcgcomprddout),
.pcsaggcgcompwrout(pcsaggcgcompwrout),
.pcsaggdelcondmetout(pcsaggdelcondmetout),
.pcsaggfifoovrout(pcsaggfifoovrout),
.pcsagglatencycompout(pcsagglatencycompout),
.pcsagginsertincompleteout(pcsagginsertincompleteout),
.pcsaggdecdatavalid(pcsaggdecdatavalid),
.pcsaggdecdata(pcsaggdecdata),
.pcsaggdecctl(pcsaggdecctl),
.pcsaggrunningdisp(pcsaggrunningdisp),
.pldrxclkslip(pldrxclkslip),
.pldhardreset(pldhardreset),
.pcsscanmoden(pcsscanmoden),
.pcsscanshiftn(pcsscanshiftn),
.pcsrefclkdig(pcsrefclkdig),
.pcsaggscanmoden(pcsaggscanmoden),
.pcsaggscanshiftn(pcsaggscanshiftn),
.pcsaggrefclkdig(pcsaggrefclkdig),
.pcsgen3gen3datasel(pcsgen3gen3datasel),
.pldlccmurstb(pldlccmurstb),
.pmaoffcaldonein(pmaoffcaldonein),
.pmarxpmarstb(pmarxpmarstb),
.pmahardreset(pmahardreset),
.freqlock(freqlock),
.pmapcieswitch(pmapcieswitch),
.pmaearlyeios(pmaearlyeios),
.pmatxdetectrx(pmatxdetectrx),
.pmatxelecidle(pmatxelecidle),
.pmatxdeemph(pmatxdeemph),
.pmatxswing(pmatxswing),
.pmatxmargin(pmatxmargin),
.pmacurrentcoeff(pmacurrentcoeff),
.pmacurrentrxpreset(pmacurrentrxpreset),
.pmaoffcaldoneout(pmaoffcaldoneout),
.pmalccmurstb(pmalccmurstb),
.pmaltr(pmaltr),
.aggtxpcsrst(aggtxpcsrst),
.aggrxpcsrst(aggrxpcsrst),
.aggtxdatatc(aggtxdatatc),
.aggtxctltc(aggtxctltc),
.aggrdenablesync(aggrdenablesync),
.aggsyncstatus(aggsyncstatus),
.aggaligndetsync(aggaligndetsync),
.aggrdalign(aggrdalign),
.aggalignstatussync(aggalignstatussync),
.aggfifordoutcomp(aggfifordoutcomp),
.aggcgcomprddout(aggcgcomprddout),
.aggcgcompwrout(aggcgcompwrout),
.aggdelcondmetout(aggdelcondmetout),
.aggfifoovrout(aggfifoovrout),
.agglatencycompout(agglatencycompout),
.agginsertincompleteout(agginsertincompleteout),
.aggdecdatavalid(aggdecdatavalid),
.aggdecdata(aggdecdata),
.aggdecctl(aggdecctl),
.aggrunningdisp(aggrunningdisp),
.pcsgen3pmarxdetectvalid(pcsgen3pmarxdetectvalid),
.pcsgen3pmarxfound(pcsgen3pmarxfound),
.pcsgen3pmapcieswdone(pcsgen3pmapcieswdone),
.pcsgen3pllfixedclk(pcsgen3pllfixedclk),
.pcsaggrcvdclkagg(pcsaggrcvdclkagg),
.pcsaggtxdatats(pcsaggtxdatats),
.pcsaggtxctlts(pcsaggtxctlts),
.pcsaggfiforstrdqd(pcsaggfiforstrdqd),
.pcsaggendskwqd(pcsaggendskwqd),
.pcsaggendskwrdptrs(pcsaggendskwrdptrs),
.pcsaggalignstatus(pcsaggalignstatus),
.pcsaggalignstatussync0(pcsaggalignstatussync0),
.pcsaggcgcomprddall(pcsaggcgcomprddall),
.pcsaggcgcompwrall(pcsaggcgcompwrall),
.pcsaggfifordincomp0(pcsaggfifordincomp0),
.pcsaggdelcondmet0(pcsaggdelcondmet0),
.pcsagginsertincomplete0(pcsagginsertincomplete0),
.pcsaggfifoovr0(pcsaggfifoovr0),
.pcsagglatencycomp0(pcsagglatencycomp0),
.pcsaggrxdatars(pcsaggrxdatars),
.pcsaggrxcontrolrs(pcsaggrxcontrolrs),
.pcsaggrcvdclkaggtoporbot(pcsaggrcvdclkaggtoporbot),
.pcsaggtxdatatstoporbot(pcsaggtxdatatstoporbot),
.pcsaggtxctltstoporbot(pcsaggtxctltstoporbot),
.pcsaggfiforstrdqdtoporbot(pcsaggfiforstrdqdtoporbot),
.pcsaggendskwqdtoporbot(pcsaggendskwqdtoporbot),
.pcsaggendskwrdptrstoporbot(pcsaggendskwrdptrstoporbot),
.pcsaggalignstatustoporbot(pcsaggalignstatustoporbot),
.pcsaggalignstatussync0toporbot(pcsaggalignstatussync0toporbot),
.pcsaggcgcomprddalltoporbot(pcsaggcgcomprddalltoporbot),
.pcsaggcgcompwralltoporbot(pcsaggcgcompwralltoporbot),
.pcsaggfifordincomp0toporbot(pcsaggfifordincomp0toporbot),
.pcsaggdelcondmet0toporbot(pcsaggdelcondmet0toporbot),
.pcsagginsertincomplete0toporbot(pcsagginsertincomplete0toporbot),
.pcsaggfifoovr0toporbot(pcsaggfifoovr0toporbot),
.pcsagglatencycomp0toporbot(pcsagglatencycomp0toporbot),
.pcsaggrxdatarstoporbot(pcsaggrxdatarstoporbot),
.pcsaggrxcontrolrstoporbot(pcsaggrxcontrolrstoporbot),
.pcs8grxdetectvalid(pcs8grxdetectvalid),
.pcs8gpmarxfound(pcs8gpmarxfound),
.pcs8ggen2ngen1(pcs8ggen2ngen1),
.pcs8gpowerstatetransitiondone(pcs8gpowerstatetransitiondone),
.ppmcntlatch(ppmcntlatch),
.pldhclkout(pldhclkout),
.aggscanmoden(aggscanmoden),
.aggscanshiftn(aggscanshiftn),
.aggrefclkdig(aggrefclkdig),
.pmaoffcalen(pmaoffcalen),
.pmafrefout(pmafrefout),
.pmaclklowout(pmaclklowout)
);

defparam inst.lpm_type = lpm_type;
defparam inst.auto_speed_ena = auto_speed_ena;
defparam inst.force_freqdet = force_freqdet;
defparam inst.func_mode = func_mode;
defparam inst.pcie_gen3_cap = pcie_gen3_cap;
defparam inst.pipe_if_g3pcs = pipe_if_g3pcs;
defparam inst.pma_if_dft_en = pma_if_dft_en;
defparam inst.pma_if_dft_val = pma_if_dft_val;
defparam inst.ppm_cnt_rst = ppm_cnt_rst;
defparam inst.ppm_deassert_early = ppm_deassert_early;
defparam inst.ppm_gen1_2_cnt = ppm_gen1_2_cnt;
defparam inst.ppm_post_eidle_delay = ppm_post_eidle_delay;
defparam inst.ppmsel = ppmsel;
defparam inst.prot_mode = prot_mode;
defparam inst.refclk_dig_sel = refclk_dig_sel;
defparam inst.selectpcs = selectpcs;
defparam inst.sup_mode = sup_mode;
defparam inst.channel_number = channel_number;

endmodule //stratixv_hssi_common_pcs_pma_interface

`timescale 1 ps/1 ps

module    stratixv_hssi_common_pld_pcs_interface    (
    pldhardresetin,
    pldscanmoden,
    pldscanshiftn,
    pldgen3refclkdig,
    pld10grefclkdig,
    pld8grefclkdig,
    pldaggrefclkdig,
    pldpcspmaifrefclkdig,
    pldrate,
    pldeidleinfersel,
    pld8gsoftresetallhssi,
    pld8gplniotri,
    pld8gprbsciden,
    pld8gltr,
    pld8gtxelecidle,
    pld8gtxdetectrxloopback,
    pld8gtxdeemph,
    pld8gtxmargin,
    pld8gtxswing,
    pld8grxpolarity,
    pld8gpowerdown,
    pldgen3currentcoeff,
    pldgen3currentrxpreset,
    pcs10gtestdata,
    pcs8gchnltestbusout,
    pcs8grxvalid,
    pcs8grxelecidle,
    pcs8grxstatus,
    pcs8gphystatus,
    pldhclkin,
    pcsgen3pldasyncstatus,
    pcsgen3testout,
    emsippcsreset,
    emsippcsctrl,
    pmafref,
    pmaclklow,
    pmaoffcaldone,
    pldoffcalenin,
    pcsgen3masktxpll,
    rcomemsip,
    rcomhipena,
    rcomblocksel,
    pldtestdata,
    pld8grxvalid,
    pld8grxelecidle,
    pld8grxstatus,
    pld8gphystatus,
    pldgen3pldasyncstatus,
    pcs10ghardresetn,
    pcs10gscanmoden,
    pcs10gscanshiftn,
    pcs10grefclkdig,
    pcs8ghardreset,
    pcs8gsoftresetallhssi,
    pcs8gplniotri,
    pcs8gscanmoden,
    pcs8gscanshiftn,
    pcs8grefclkdig,
    pcs8gprbsciden,
    pcs8gltr,
    pcs8gtxelecidle,
    pcs8gtxdetectrxloopback,
    pcs8gtxdeemph,
    pcs8gtxmargin,
    pcs8gtxswing,
    pcs8grxpolarity,
    pcs8grate,
    pcs8gpowerdown,
    pcs8geidleinfersel,
    pcsgen3pcsdigclk,
    pcsgen3rate,
    pcsgen3eidleinfersel,
    pcsgen3scanmoden,
    pcsgen3scanshiftn,
    pcsgen3pldltr,
    pldhardresetout,
    pcsgen3currentcoeff,
    pcsgen3currentrxpreset,
    pcsaggrefclkdig,
    pcspcspmaifrefclkdig,
    pcsaggscanmoden,
    pcsaggscanshiftn,
    pcspcspmaifscanmoden,
    pcspcspmaifscanshiftn,
    emsippcsclkout,
    emsippcsstatus,
    pldfref,
    pldclklow,
    emsipenabledusermode,
    pldoffcalenout,
    pldoffcaldone,
    pldgen3masktxpll);

    parameter    lpm_type    =    "stratixv_hssi_common_pld_pcs_interface";
    parameter    data_source    =    "pld";
    parameter    emsip_enable    =    "emsip_disable";
    parameter    selectpcs    =    "eight_g_pcs";
    parameter    channel_number = 0;


    input    pldhardresetin;
    input    pldscanmoden;
    input    pldscanshiftn;
    input    pldgen3refclkdig;
    input    pld10grefclkdig;
    input    pld8grefclkdig;
    input    pldaggrefclkdig;
    input    pldpcspmaifrefclkdig;
    input    [1:0]    pldrate;
    input    [2:0]    pldeidleinfersel;
    input    pld8gsoftresetallhssi;
    input    pld8gplniotri;
    input    pld8gprbsciden;
    input    pld8gltr;
    input    pld8gtxelecidle;
    input    pld8gtxdetectrxloopback;
    input    pld8gtxdeemph;
    input    [2:0]    pld8gtxmargin;
    input    pld8gtxswing;
    input    pld8grxpolarity;
    input    [1:0]    pld8gpowerdown;
    input    [17:0]    pldgen3currentcoeff;
    input    [2:0]    pldgen3currentrxpreset;
    input    [19:0]    pcs10gtestdata;
    input    [9:0]    pcs8gchnltestbusout;
    input    pcs8grxvalid;
    input    pcs8grxelecidle;
    input    [2:0]    pcs8grxstatus;
    input    pcs8gphystatus;
    input    pldhclkin;
    input    [5:0]    pcsgen3pldasyncstatus;
    input    [19:0]    pcsgen3testout;
    input    [2:0]    emsippcsreset;
    input    [38:0]    emsippcsctrl;
    input    pmafref;
    input    pmaclklow;
    input    pmaoffcaldone;
    input    pldoffcalenin;
    input    pcsgen3masktxpll;
    input    rcomemsip;
    input    rcomhipena;
    input    [1:0]    rcomblocksel;
    output    [19:0]    pldtestdata;
    output    pld8grxvalid;
    output    pld8grxelecidle;
    output    [2:0]    pld8grxstatus;
    output    pld8gphystatus;
    output    [5:0]    pldgen3pldasyncstatus;
    output    pcs10ghardresetn;
    output    pcs10gscanmoden;
    output    pcs10gscanshiftn;
    output    pcs10grefclkdig;
    output    pcs8ghardreset;
    output    pcs8gsoftresetallhssi;
    output    pcs8gplniotri;
    output    pcs8gscanmoden;
    output    pcs8gscanshiftn;
    output    pcs8grefclkdig;
    output    pcs8gprbsciden;
    output    pcs8gltr;
    output    pcs8gtxelecidle;
    output    pcs8gtxdetectrxloopback;
    output    pcs8gtxdeemph;
    output    [2:0]    pcs8gtxmargin;
    output    pcs8gtxswing;
    output    pcs8grxpolarity;
    output    pcs8grate;
    output    [1:0]    pcs8gpowerdown;
    output    [2:0]    pcs8geidleinfersel;
    output    pcsgen3pcsdigclk;
    output    [1:0]    pcsgen3rate;
    output    [2:0]    pcsgen3eidleinfersel;
    output    pcsgen3scanmoden;
    output    pcsgen3scanshiftn;
    output    pcsgen3pldltr;
    output    pldhardresetout;
    output    [17:0]    pcsgen3currentcoeff;
    output    [2:0]    pcsgen3currentrxpreset;
    output    pcsaggrefclkdig;
    output    pcspcspmaifrefclkdig;
    output    pcsaggscanmoden;
    output    pcsaggscanshiftn;
    output    pcspcspmaifscanmoden;
    output    pcspcspmaifscanshiftn;
    output    [2:0]    emsippcsclkout;
    output    [13:0]    emsippcsstatus;
    output    pldfref;
    output    pldclklow;
    output    emsipenabledusermode;
    output    pldoffcalenout;
    output    pldoffcaldone;
    output    pldgen3masktxpll;

    stratixv_hssi_common_pld_pcs_interface_encrypted inst (
        .pldhardresetin(pldhardresetin),
        .pldscanmoden(pldscanmoden),
        .pldscanshiftn(pldscanshiftn),
        .pldgen3refclkdig(pldgen3refclkdig),
        .pld10grefclkdig(pld10grefclkdig),
        .pld8grefclkdig(pld8grefclkdig),
        .pldaggrefclkdig(pldaggrefclkdig),
        .pldpcspmaifrefclkdig(pldpcspmaifrefclkdig),
        .pldrate(pldrate),
        .pldeidleinfersel(pldeidleinfersel),
        .pld8gsoftresetallhssi(pld8gsoftresetallhssi),
        .pld8gplniotri(pld8gplniotri),
        .pld8gprbsciden(pld8gprbsciden),
        .pld8gltr(pld8gltr),
        .pld8gtxelecidle(pld8gtxelecidle),
        .pld8gtxdetectrxloopback(pld8gtxdetectrxloopback),
        .pld8gtxdeemph(pld8gtxdeemph),
        .pld8gtxmargin(pld8gtxmargin),
        .pld8gtxswing(pld8gtxswing),
        .pld8grxpolarity(pld8grxpolarity),
        .pld8gpowerdown(pld8gpowerdown),
        .pldgen3currentcoeff(pldgen3currentcoeff),
        .pldgen3currentrxpreset(pldgen3currentrxpreset),
        .pcs10gtestdata(pcs10gtestdata),
        .pcs8gchnltestbusout(pcs8gchnltestbusout),
        .pcs8grxvalid(pcs8grxvalid),
        .pcs8grxelecidle(pcs8grxelecidle),
        .pcs8grxstatus(pcs8grxstatus),
        .pcs8gphystatus(pcs8gphystatus),
        .pldhclkin(pldhclkin),
        .pcsgen3pldasyncstatus(pcsgen3pldasyncstatus),
        .pcsgen3testout(pcsgen3testout),
        .emsippcsreset(emsippcsreset),
        .emsippcsctrl(emsippcsctrl),
        .pmafref(pmafref),
        .pmaclklow(pmaclklow),
        .pmaoffcaldone(pmaoffcaldone),
        .pldoffcalenin(pldoffcalenin),
        .pcsgen3masktxpll(pcsgen3masktxpll),
        .rcomemsip(rcomemsip),
        .rcomhipena(rcomhipena),
        .rcomblocksel(rcomblocksel),
        .pldtestdata(pldtestdata),
        .pld8grxvalid(pld8grxvalid),
        .pld8grxelecidle(pld8grxelecidle),
        .pld8grxstatus(pld8grxstatus),
        .pld8gphystatus(pld8gphystatus),
        .pldgen3pldasyncstatus(pldgen3pldasyncstatus),
        .pcs10ghardresetn(pcs10ghardresetn),
        .pcs10gscanmoden(pcs10gscanmoden),
        .pcs10gscanshiftn(pcs10gscanshiftn),
        .pcs10grefclkdig(pcs10grefclkdig),
        .pcs8ghardreset(pcs8ghardreset),
        .pcs8gsoftresetallhssi(pcs8gsoftresetallhssi),
        .pcs8gplniotri(pcs8gplniotri),
        .pcs8gscanmoden(pcs8gscanmoden),
        .pcs8gscanshiftn(pcs8gscanshiftn),
        .pcs8grefclkdig(pcs8grefclkdig),
        .pcs8gprbsciden(pcs8gprbsciden),
        .pcs8gltr(pcs8gltr),
        .pcs8gtxelecidle(pcs8gtxelecidle),
        .pcs8gtxdetectrxloopback(pcs8gtxdetectrxloopback),
        .pcs8gtxdeemph(pcs8gtxdeemph),
        .pcs8gtxmargin(pcs8gtxmargin),
        .pcs8gtxswing(pcs8gtxswing),
        .pcs8grxpolarity(pcs8grxpolarity),
        .pcs8grate(pcs8grate),
        .pcs8gpowerdown(pcs8gpowerdown),
        .pcs8geidleinfersel(pcs8geidleinfersel),
        .pcsgen3pcsdigclk(pcsgen3pcsdigclk),
        .pcsgen3rate(pcsgen3rate),
        .pcsgen3eidleinfersel(pcsgen3eidleinfersel),
        .pcsgen3scanmoden(pcsgen3scanmoden),
        .pcsgen3scanshiftn(pcsgen3scanshiftn),
        .pcsgen3pldltr(pcsgen3pldltr),
        .pldhardresetout(pldhardresetout),
        .pcsgen3currentcoeff(pcsgen3currentcoeff),
        .pcsgen3currentrxpreset(pcsgen3currentrxpreset),
        .pcsaggrefclkdig(pcsaggrefclkdig),
        .pcspcspmaifrefclkdig(pcspcspmaifrefclkdig),
        .pcsaggscanmoden(pcsaggscanmoden),
        .pcsaggscanshiftn(pcsaggscanshiftn),
        .pcspcspmaifscanmoden(pcspcspmaifscanmoden),
        .pcspcspmaifscanshiftn(pcspcspmaifscanshiftn),
        .emsippcsclkout(emsippcsclkout),
        .emsippcsstatus(emsippcsstatus),
        .pldfref(pldfref),
        .pldclklow(pldclklow),
        .emsipenabledusermode(emsipenabledusermode),
        .pldoffcalenout(pldoffcalenout),
        .pldoffcaldone(pldoffcaldone),
        .pldgen3masktxpll(pldgen3masktxpll) );
    defparam inst.lpm_type = lpm_type;
    defparam inst.data_source = data_source;
    defparam inst.emsip_enable = emsip_enable;
    defparam inst.selectpcs = selectpcs;
	defparam inst.channel_number = channel_number;

endmodule //stratixv_hssi_common_pld_pcs_interface

`timescale 1 ps/1 ps

module stratixv_hssi_rx_pcs_pma_interface (
	clockinfrompma,
	datainfrompma,
	pmasigdet,
	pmasignalok,
	pcs10grxclkiqout,
	pcsgen3rxclkiqout,
	pcs8grxclkiqout,
	pcs8grxclkslip,
	pmaclkdiv33txorrxin,
	pmarxplllockin,
	pldrxpmarstb,
	pldrxclkslip,
	rrxblocksel,
	rrxclkslipsel,
	
	pmarxclkslip,
	pmarxclkout,
	clkoutto10gpcs,
	dataoutto10gpcs,
	pcs10gsignalok,
	clockouttogen3pcs,
	dataouttogen3pcs,
	pcsgen3pmasignaldet,
	clockoutto8gpcs,
	dataoutto8gpcs,
	pcs8gsigdetni,
	pmaclkdiv33txorrxout,
	pcs10gclkdiv33txorrx,
	pmarxpmarstb,
	pmarxplllockout
);

//parameters
parameter lpm_type = "stratixv_hssi_rx_pcs_pma_interface";
parameter clkslip_sel = "pld";
parameter prot_mode = "other_protocols";
parameter selectpcs = "eight_g_pcs";
parameter channel_number = 0;

// I/O ports
input			clockinfrompma;
input [39:0]	datainfrompma;
input			pmasigdet;
input			pmasignalok;
input			pcs10grxclkiqout;
input			pcsgen3rxclkiqout;
input			pcs8grxclkiqout;
input			pcs8grxclkslip;
input			pmaclkdiv33txorrxin;
input			pmarxplllockin;
input			pldrxpmarstb;
input			pldrxclkslip;
input [1:0]		rrxblocksel;
input			rrxclkslipsel;

output			pmarxclkslip;
output			pmarxclkout;
output			clkoutto10gpcs;
output [39:0]	dataoutto10gpcs;
output			pcs10gsignalok;
output			clockouttogen3pcs;
output [31:0]	dataouttogen3pcs;
output			pcsgen3pmasignaldet;
output			clockoutto8gpcs;
output [19:0]	dataoutto8gpcs;
output			pcs8gsigdetni;
output			pmaclkdiv33txorrxout;
output			pcs10gclkdiv33txorrx;
output			pmarxpmarstb;
output			pmarxplllockout;

stratixv_hssi_rx_pcs_pma_interface_encrypted inst (
.clockinfrompma(clockinfrompma),
.datainfrompma(datainfrompma),
.pmasigdet(pmasigdet),
.pmasignalok(pmasignalok),
.pcs10grxclkiqout(pcs10grxclkiqout),
.pcsgen3rxclkiqout(pcsgen3rxclkiqout),
.pcs8grxclkiqout(pcs8grxclkiqout),
.pcs8grxclkslip(pcs8grxclkslip),
.pmaclkdiv33txorrxin(pmaclkdiv33txorrxin),
.pmarxplllockin(pmarxplllockin),
.pldrxpmarstb(pldrxpmarstb),
.pldrxclkslip(pldrxclkslip),
.rrxblocksel(rrxblocksel),
.rrxclkslipsel(rrxclkslipsel),
.pmarxclkslip(pmarxclkslip),
.pmarxclkout(pmarxclkout),
.clkoutto10gpcs(clkoutto10gpcs),
.dataoutto10gpcs(dataoutto10gpcs),
.pcs10gsignalok(pcs10gsignalok),
.clockouttogen3pcs(clockouttogen3pcs),
.dataouttogen3pcs(dataouttogen3pcs),
.pcsgen3pmasignaldet(pcsgen3pmasignaldet),
.clockoutto8gpcs(clockoutto8gpcs),
.dataoutto8gpcs(dataoutto8gpcs),
.pcs8gsigdetni(pcs8gsigdetni),
.pmaclkdiv33txorrxout(pmaclkdiv33txorrxout),
.pcs10gclkdiv33txorrx(pcs10gclkdiv33txorrx),
.pmarxpmarstb(pmarxpmarstb),
.pmarxplllockout(pmarxplllockout)
);

defparam inst.lpm_type = lpm_type;
defparam inst.clkslip_sel = clkslip_sel;
defparam inst.prot_mode = prot_mode;
defparam inst.selectpcs = selectpcs;
defparam inst.channel_number = channel_number;

endmodule //stratixv_hssi_rx_pcs_pma_interface

`timescale 1 ps/1 ps

module stratixv_hssi_rx_pld_pcs_interface (
	pld10grxpldclk,
	pld10grxpldrstn,
	pld10grxalignen,
	pld10grxalignclr,
	pld10grxrden,
	pld10grxdispclr,
	pld10grxclrerrblkcnt,
	pld10grxclrbercount,
	pld10grxprbserrclr,
	pld10grxbitslip,
	pld8grxurstpma,
	pld8grxurstpcs,
	pld8gcmpfifourst,
	pld8gphfifourstrx,
	pld8gencdt,
	pld8ga1a2size,
	pld8gbitslip,
	pld8grdenablermf,
	pld8gwrenablermf,
	pld8gpldrxclk,
	pld8gpolinvrx,
	pld8gbitlocreven,
	pld8gbytereven,
	pld8gbytordpld,
	pld8gwrdisablerx,
	pld8grdenablerx,
	pldgen3rxrstn,
	pldrxclkslipin,
	pld8gpldextrain,
	clockinfrom10gpcs,
	pcs10grxdatavalid,
	datainfrom10gpcs,
	pcs10grxcontrol,
	pcs10grxempty,
	pcs10grxpempty,
	pcs10grxpfull,
	pcs10grxoflwerr,
	pcs10grxalignval,
	pcs10grxblklock,
	pcs10grxhiber,
	pcs10grxframelock,
	pcs10grxrdpossts,
	pcs10grxrdnegsts,
	pcs10grxskipins,
	pcs10grxrxframe,
	pcs10grxpyldins,
	pcs10grxsyncerr,
	pcs10grxscrmerr,
	pcs10grxskiperr,
	pcs10grxdiagerr,
	pcs10grxsherr,
	pcs10grxmfrmerr,
	pcs10grxcrc32err,
	pcs10grxdiagstatus,
	datainfrom8gpcs,
	clockinfrom8gpcs,
	pcs8gbisterr,
	pcs8grcvdclkpmab,
	pcs8gsignaldetectout,
	pcs8gbistdone,
	pcs8grlvlt,
	pcs8gfullrmf,
	pcs8gemptyrmf,
	pcs8gfullrx,
	pcs8gemptyrx,
	pcs8ga1a2k1k2flag,
	pcs8gbyteordflag,
	pcs8gwaboundary,
	pcs8grxdatavalid,
	pcs8grxsynchdr,
	pcs8grxblkstart,
	pmaclkdiv33txorrx,
	emsippcsrxclkin,
	emsippcsrxreset,
	emsippcsrxctrl,
	pmarxplllock,
	pldrxpmarstbin,
	rrxblocksel,
	rrxemsip,
	emsipenabledusermode,
	pcs10grxfifoinsert,
	pld8gsyncsmeninput,
	pcs10grxfifodel,
	
	dataouttopld,
	pld10grxclkout,
	pld10grxdatavalid,
	pld10grxcontrol,
	pld10grxempty,
	pld10grxpempty,
	pld10grxpfull,
	pld10grxoflwerr,
	pld10grxalignval,
	pld10grxblklock,
	pld10grxhiber,
	pld10grxframelock,
	pld10grxrdpossts,
	pld10grxrdnegsts,
	pld10grxskipins,
	pld10grxrxframe,
	pld10grxpyldins,
	pld10grxsyncerr,
	pld10grxscrmerr,
	pld10grxskiperr,
	pld10grxdiagerr,
	pld10grxsherr,
	pld10grxmfrmerr,
	pld10grxcrc32err,
	pld10grxdiagstatus,
	pld8grxclkout,
	pld8gbisterr,
	pld8grcvdclkpmab,
	pld8gsignaldetectout,
	pld8gbistdone,
	pld8grlvlt,
	pld8gfullrmf,
	pld8gemptyrmf,
	pld8gfullrx,
	pld8gemptyrx,
	pld8ga1a2k1k2flag,
	pld8gbyteordflag,
	pld8gwaboundary,
	pld8grxdatavalid,
	pld8grxsynchdr,
	pld8grxblkstart,
	pcs10grxpldclk,
	pcs10grxpldrstn,
	pcs10grxalignen,
	pcs10grxalignclr,
	pcs10grxrden,
	pcs10grxdispclr,
	pcs10grxclrerrblkcnt,
	pcs10grxclrbercount,
	pcs10grxprbserrclr,
	pcs10grxbitslip,
	pcs8grxurstpma,
	pcs8grxurstpcs,
	pcs8gcmpfifourst,
	pcs8gphfifourstrx,
	pcs8gencdt,
	pcs8ga1a2size,
	pcs8gbitslip,
	pcs8grdenablermf,
	pcs8gwrenablermf,
	pcs8gpldrxclk,
	pcs8gpolinvrx,
	pcs8gbitlocreven,
	pcs8gbytereven,
	pcs8gbytordpld,
	pcs8gwrdisablerx,
	pcs8grdenablerx,
	pcs8gpldextrain,
	pcsgen3rxrstn,
	pldrxclkslipout,
	pldclkdiv33txorrx,
	emsiprxdata,
	emsippcsrxclkout,
	emsippcsrxstatus,
	pldrxpmarstbout,
	pldrxplllock,
	pld10grxfifodel,
	pldrxiqclkout,
	pld10grxfifoinsert,
	pcs8gsyncsmenoutput
);

//parameters
parameter lpm_type = "stratixv_hssi_rx_pld_pcs_interface";
parameter data_source = "pld";
parameter is_10g_0ppm = "false";
parameter is_8g_0ppm = "false";
parameter selectpcs = "eight_g_pcs";
parameter channel_number = 0;

// I/O ports
input			pld10grxpldclk;
input			pld10grxpldrstn;
input			pld10grxalignen;
input			pld10grxalignclr;
input			pld10grxrden;
input			pld10grxdispclr;
input			pld10grxclrerrblkcnt;
input			pld10grxclrbercount;
input			pld10grxprbserrclr;
input			pld10grxbitslip;
input			pld8grxurstpma;
input			pld8grxurstpcs;
input			pld8gcmpfifourst;
input			pld8gphfifourstrx;
input			pld8gencdt;
input			pld8ga1a2size;
input			pld8gbitslip;
input			pld8grdenablermf;
input			pld8gwrenablermf;
input			pld8gpldrxclk;
input			pld8gpolinvrx;
input			pld8gbitlocreven;
input			pld8gbytereven;
input			pld8gbytordpld;
input			pld8gwrdisablerx;
input			pld8grdenablerx;
input			pldgen3rxrstn;
input			pldrxclkslipin;
input [3:0]		pld8gpldextrain;
input			clockinfrom10gpcs;
input			pcs10grxdatavalid;
input [63:0]	datainfrom10gpcs;
input [9:0]		pcs10grxcontrol;
input			pcs10grxempty;
input			pcs10grxpempty;
input			pcs10grxpfull;
input			pcs10grxoflwerr;
input			pcs10grxalignval;
input			pcs10grxblklock;
input			pcs10grxhiber;
input			pcs10grxframelock;
input			pcs10grxrdpossts;
input			pcs10grxrdnegsts;
input			pcs10grxskipins;
input			pcs10grxrxframe;
input			pcs10grxpyldins;
input			pcs10grxsyncerr;
input			pcs10grxscrmerr;
input			pcs10grxskiperr;
input			pcs10grxdiagerr;
input			pcs10grxsherr;
input			pcs10grxmfrmerr;
input			pcs10grxcrc32err;
input [1:0]		pcs10grxdiagstatus;
input [63:0]	datainfrom8gpcs;
input			clockinfrom8gpcs;
input			pcs8gbisterr;
input			pcs8grcvdclkpmab;
input			pcs8gsignaldetectout;
input			pcs8gbistdone;
input			pcs8grlvlt;
input			pcs8gfullrmf;
input			pcs8gemptyrmf;
input			pcs8gfullrx;
input			pcs8gemptyrx;
input [3:0]		pcs8ga1a2k1k2flag;
input			pcs8gbyteordflag;
input [4:0]		pcs8gwaboundary;
input [3:0]		pcs8grxdatavalid;
input [1:0]		pcs8grxsynchdr;
input [3:0]		pcs8grxblkstart;
input			pmaclkdiv33txorrx;
input [2:0]		emsippcsrxclkin;
input [6:0]		emsippcsrxreset;
input [24:0]	emsippcsrxctrl;
input			pmarxplllock;
input			pldrxpmarstbin;
input [1:0]		rrxblocksel;
input			rrxemsip;
input			emsipenabledusermode;
input			pcs10grxfifoinsert;
input			pld8gsyncsmeninput;
input			pcs10grxfifodel;
	
output [63:0]	dataouttopld;
output			pld10grxclkout;
output			pld10grxdatavalid;
output [9:0]	pld10grxcontrol;
output			pld10grxempty;
output			pld10grxpempty;
output			pld10grxpfull;
output			pld10grxoflwerr;
output			pld10grxalignval;
output			pld10grxblklock;
output			pld10grxhiber;
output			pld10grxframelock;
output			pld10grxrdpossts;
output			pld10grxrdnegsts;
output			pld10grxskipins;
output			pld10grxrxframe;
output			pld10grxpyldins;
output			pld10grxsyncerr;
output			pld10grxscrmerr;
output			pld10grxskiperr;
output			pld10grxdiagerr;
output			pld10grxsherr;
output			pld10grxmfrmerr;
output			pld10grxcrc32err;
output [1:0]	pld10grxdiagstatus;
output			pld8grxclkout;
output			pld8gbisterr;
output			pld8grcvdclkpmab;
output			pld8gsignaldetectout;
output			pld8gbistdone;
output			pld8grlvlt;
output			pld8gfullrmf;
output			pld8gemptyrmf;
output			pld8gfullrx;
output			pld8gemptyrx;
output [3:0]	pld8ga1a2k1k2flag;
output			pld8gbyteordflag;
output [4:0]	pld8gwaboundary;
output [3:0]	pld8grxdatavalid;
output [1:0]	pld8grxsynchdr;
output [3:0]	pld8grxblkstart;
output			pcs10grxpldclk;
output			pcs10grxpldrstn;
output			pcs10grxalignen;
output			pcs10grxalignclr;
output			pcs10grxrden;
output			pcs10grxdispclr;
output			pcs10grxclrerrblkcnt;
output			pcs10grxclrbercount;
output			pcs10grxprbserrclr;
output			pcs10grxbitslip;
output			pcs8grxurstpma;
output			pcs8grxurstpcs;
output			pcs8gcmpfifourst;
output			pcs8gphfifourstrx;
output			pcs8gencdt;
output			pcs8ga1a2size;
output			pcs8gbitslip;
output			pcs8grdenablermf;
output			pcs8gwrenablermf;
output			pcs8gpldrxclk;
output			pcs8gpolinvrx;
output			pcs8gbitlocreven;
output			pcs8gbytereven;
output			pcs8gbytordpld;
output			pcs8gwrdisablerx;
output			pcs8grdenablerx;
output [3:0]	pcs8gpldextrain;
output			pcsgen3rxrstn;
output			pldrxclkslipout;
output			pldclkdiv33txorrx;
output [63:0]	emsiprxdata;
output [3:0]	emsippcsrxclkout;
output [63:0]	emsippcsrxstatus;
output			pldrxpmarstbout;
output			pldrxplllock;
output			pld10grxfifodel;
output			pldrxiqclkout;
output			pld10grxfifoinsert;
output			pcs8gsyncsmenoutput;

stratixv_hssi_rx_pld_pcs_interface_encrypted inst (
.pld10grxpldclk(pld10grxpldclk),
.pld10grxpldrstn(pld10grxpldrstn),
.pld10grxalignen(pld10grxalignen),
.pld10grxalignclr(pld10grxalignclr),
.pld10grxrden(pld10grxrden),
.pld10grxdispclr(pld10grxdispclr),
.pld10grxclrerrblkcnt(pld10grxclrerrblkcnt),
.pld10grxclrbercount(pld10grxclrbercount),
.pld10grxprbserrclr(pld10grxprbserrclr),
.pld10grxbitslip(pld10grxbitslip),
.pld8grxurstpma(pld8grxurstpma),
.pld8grxurstpcs(pld8grxurstpcs),
.pld8gcmpfifourst(pld8gcmpfifourst),
.pld8gphfifourstrx(pld8gphfifourstrx),
.pld8gencdt(pld8gencdt),
.pld8ga1a2size(pld8ga1a2size),
.pld8gbitslip(pld8gbitslip),
.pld8grdenablermf(pld8grdenablermf),
.pld8gwrenablermf(pld8gwrenablermf),
.pld8gpldrxclk(pld8gpldrxclk),
.pld8gpolinvrx(pld8gpolinvrx),
.pld8gbitlocreven(pld8gbitlocreven),
.pld8gbytereven(pld8gbytereven),
.pld8gbytordpld(pld8gbytordpld),
.pld8gwrdisablerx(pld8gwrdisablerx),
.pld8grdenablerx(pld8grdenablerx),
.pldgen3rxrstn(pldgen3rxrstn),
.pldrxclkslipin(pldrxclkslipin),
.pld8gpldextrain(pld8gpldextrain),
.clockinfrom10gpcs(clockinfrom10gpcs),
.pcs10grxdatavalid(pcs10grxdatavalid),
.datainfrom10gpcs(datainfrom10gpcs),
.pcs10grxcontrol(pcs10grxcontrol),
.pcs10grxempty(pcs10grxempty),
.pcs10grxpempty(pcs10grxpempty),
.pcs10grxpfull(pcs10grxpfull),
.pcs10grxoflwerr(pcs10grxoflwerr),
.pcs10grxalignval(pcs10grxalignval),
.pcs10grxblklock(pcs10grxblklock),
.pcs10grxhiber(pcs10grxhiber),
.pcs10grxframelock(pcs10grxframelock),
.pcs10grxrdpossts(pcs10grxrdpossts),
.pcs10grxrdnegsts(pcs10grxrdnegsts),
.pcs10grxskipins(pcs10grxskipins),
.pcs10grxrxframe(pcs10grxrxframe),
.pcs10grxpyldins(pcs10grxpyldins),
.pcs10grxsyncerr(pcs10grxsyncerr),
.pcs10grxscrmerr(pcs10grxscrmerr),
.pcs10grxskiperr(pcs10grxskiperr),
.pcs10grxdiagerr(pcs10grxdiagerr),
.pcs10grxsherr(pcs10grxsherr),
.pcs10grxmfrmerr(pcs10grxmfrmerr),
.pcs10grxcrc32err(pcs10grxcrc32err),
.pcs10grxdiagstatus(pcs10grxdiagstatus),
.datainfrom8gpcs(datainfrom8gpcs),
.clockinfrom8gpcs(clockinfrom8gpcs),
.pcs8gbisterr(pcs8gbisterr),
.pcs8grcvdclkpmab(pcs8grcvdclkpmab),
.pcs8gsignaldetectout(pcs8gsignaldetectout),
.pcs8gbistdone(pcs8gbistdone),
.pcs8grlvlt(pcs8grlvlt),
.pcs8gfullrmf(pcs8gfullrmf),
.pcs8gemptyrmf(pcs8gemptyrmf),
.pcs8gfullrx(pcs8gfullrx),
.pcs8gemptyrx(pcs8gemptyrx),
.pcs8ga1a2k1k2flag(pcs8ga1a2k1k2flag),
.pcs8gbyteordflag(pcs8gbyteordflag),
.pcs8gwaboundary(pcs8gwaboundary),
.pcs8grxdatavalid(pcs8grxdatavalid),
.pcs8grxsynchdr(pcs8grxsynchdr),
.pcs8grxblkstart(pcs8grxblkstart),
.pmaclkdiv33txorrx(pmaclkdiv33txorrx),
.emsippcsrxclkin(emsippcsrxclkin),
.emsippcsrxreset(emsippcsrxreset),
.emsippcsrxctrl(emsippcsrxctrl),
.pmarxplllock(pmarxplllock),
.pldrxpmarstbin(pldrxpmarstbin),
.rrxblocksel(rrxblocksel),
.rrxemsip(rrxemsip),
.emsipenabledusermode(emsipenabledusermode),
.pcs10grxfifoinsert(pcs10grxfifoinsert),
.pld8gsyncsmeninput(pld8gsyncsmeninput),
.pcs10grxfifodel(pcs10grxfifodel),
.dataouttopld(dataouttopld),
.pld10grxclkout(pld10grxclkout),
.pld10grxdatavalid(pld10grxdatavalid),
.pld10grxcontrol(pld10grxcontrol),
.pld10grxempty(pld10grxempty),
.pld10grxpempty(pld10grxpempty),
.pld10grxpfull(pld10grxpfull),
.pld10grxoflwerr(pld10grxoflwerr),
.pld10grxalignval(pld10grxalignval),
.pld10grxblklock(pld10grxblklock),
.pld10grxhiber(pld10grxhiber),
.pld10grxframelock(pld10grxframelock),
.pld10grxrdpossts(pld10grxrdpossts),
.pld10grxrdnegsts(pld10grxrdnegsts),
.pld10grxskipins(pld10grxskipins),
.pld10grxrxframe(pld10grxrxframe),
.pld10grxpyldins(pld10grxpyldins),
.pld10grxsyncerr(pld10grxsyncerr),
.pld10grxscrmerr(pld10grxscrmerr),
.pld10grxskiperr(pld10grxskiperr),
.pld10grxdiagerr(pld10grxdiagerr),
.pld10grxsherr(pld10grxsherr),
.pld10grxmfrmerr(pld10grxmfrmerr),
.pld10grxcrc32err(pld10grxcrc32err),
.pld10grxdiagstatus(pld10grxdiagstatus),
.pld8grxclkout(pld8grxclkout),
.pld8gbisterr(pld8gbisterr),
.pld8grcvdclkpmab(pld8grcvdclkpmab),
.pld8gsignaldetectout(pld8gsignaldetectout),
.pld8gbistdone(pld8gbistdone),
.pld8grlvlt(pld8grlvlt),
.pld8gfullrmf(pld8gfullrmf),
.pld8gemptyrmf(pld8gemptyrmf),
.pld8gfullrx(pld8gfullrx),
.pld8gemptyrx(pld8gemptyrx),
.pld8ga1a2k1k2flag(pld8ga1a2k1k2flag),
.pld8gbyteordflag(pld8gbyteordflag),
.pld8gwaboundary(pld8gwaboundary),
.pld8grxdatavalid(pld8grxdatavalid),
.pld8grxsynchdr(pld8grxsynchdr),
.pld8grxblkstart(pld8grxblkstart),
.pcs10grxpldclk(pcs10grxpldclk),
.pcs10grxpldrstn(pcs10grxpldrstn),
.pcs10grxalignen(pcs10grxalignen),
.pcs10grxalignclr(pcs10grxalignclr),
.pcs10grxrden(pcs10grxrden),
.pcs10grxdispclr(pcs10grxdispclr),
.pcs10grxclrerrblkcnt(pcs10grxclrerrblkcnt),
.pcs10grxclrbercount(pcs10grxclrbercount),
.pcs10grxprbserrclr(pcs10grxprbserrclr),
.pcs10grxbitslip(pcs10grxbitslip),
.pcs8grxurstpma(pcs8grxurstpma),
.pcs8grxurstpcs(pcs8grxurstpcs),
.pcs8gcmpfifourst(pcs8gcmpfifourst),
.pcs8gphfifourstrx(pcs8gphfifourstrx),
.pcs8gencdt(pcs8gencdt),
.pcs8ga1a2size(pcs8ga1a2size),
.pcs8gbitslip(pcs8gbitslip),
.pcs8grdenablermf(pcs8grdenablermf),
.pcs8gwrenablermf(pcs8gwrenablermf),
.pcs8gpldrxclk(pcs8gpldrxclk),
.pcs8gpolinvrx(pcs8gpolinvrx),
.pcs8gbitlocreven(pcs8gbitlocreven),
.pcs8gbytereven(pcs8gbytereven),
.pcs8gbytordpld(pcs8gbytordpld),
.pcs8gwrdisablerx(pcs8gwrdisablerx),
.pcs8grdenablerx(pcs8grdenablerx),
.pcs8gpldextrain(pcs8gpldextrain),
.pcsgen3rxrstn(pcsgen3rxrstn),
.pldrxclkslipout(pldrxclkslipout),
.pldclkdiv33txorrx(pldclkdiv33txorrx),
.emsiprxdata(emsiprxdata),
.emsippcsrxclkout(emsippcsrxclkout),
.emsippcsrxstatus(emsippcsrxstatus),
.pldrxpmarstbout(pldrxpmarstbout),
.pldrxplllock(pldrxplllock),
.pld10grxfifodel(pld10grxfifodel),
.pldrxiqclkout(pldrxiqclkout),
.pld10grxfifoinsert(pld10grxfifoinsert),
.pcs8gsyncsmenoutput(pcs8gsyncsmenoutput)
);

defparam inst.lpm_type = lpm_type;
defparam inst.data_source = data_source;
defparam inst.is_10g_0ppm = is_10g_0ppm;
defparam inst.is_8g_0ppm = is_8g_0ppm;
defparam inst.selectpcs = selectpcs;
defparam inst.channel_number = channel_number;

endmodule //stratixv_hssi_rx_pld_pcs_interface

`timescale 1 ps/1 ps

module stratixv_hssi_tx_pcs_pma_interface (
	clockinfrompma,
	datainfrom10gpcs,
	pcs10gtxclkiqout,
	pcsgen3txclkiqout,
	datainfromgen3pcs,
	pcs8gtxclkiqout,
	datainfrom8gpcs,
	pmaclkdiv33lcin,
	pmatxlcplllockin,
	pmatxcmuplllockin,
//	pldtxpmarstb,
	rtxblocksel,
	pcsgen3gen3datasel,
	pldtxpmasyncp,
	
	dataouttopma,
	pmatxclkout,
	clockoutto10gpcs,
	clockoutto8gpcs,
	pmaclkdiv33lcout,
	pcs10gclkdiv33lc,
//	pmatxpmarstb,
	pmatxlcplllockout,
	pmatxcmuplllockout,
	pmatxpmasyncp
);

//parameters
parameter lpm_type = "stratixv_hssi_tx_pcs_pma_interface";
parameter selectpcs = "eight_g_pcs";
parameter channel_number = 0;

// I/O ports
input			clockinfrompma;
input [39:0]	datainfrom10gpcs;
input			pcs10gtxclkiqout;
input			pcsgen3txclkiqout;
input [31:0]	datainfromgen3pcs;
input			pcs8gtxclkiqout;
input [19:0]	datainfrom8gpcs;
input			pmaclkdiv33lcin;
input			pmatxlcplllockin;
input			pmatxcmuplllockin;
//input			pldtxpmarstb;
input [1:0]		rtxblocksel;
input			pcsgen3gen3datasel;
input			pldtxpmasyncp;

output [39:0]	dataouttopma;
output			pmatxclkout;
output			clockoutto10gpcs;
output			clockoutto8gpcs;
output			pmaclkdiv33lcout;
output			pcs10gclkdiv33lc;
//output			pmatxpmarstb;
output			pmatxlcplllockout;
output			pmatxcmuplllockout;
output			pmatxpmasyncp;

stratixv_hssi_tx_pcs_pma_interface_encrypted inst (
.clockinfrompma(clockinfrompma),
.datainfrom10gpcs(datainfrom10gpcs),
.pcs10gtxclkiqout(pcs10gtxclkiqout),
.pcsgen3txclkiqout(pcsgen3txclkiqout),
.datainfromgen3pcs(datainfromgen3pcs),
.pcs8gtxclkiqout(pcs8gtxclkiqout),
.datainfrom8gpcs(datainfrom8gpcs),
.pmaclkdiv33lcin(pmaclkdiv33lcin),
.pmatxlcplllockin(pmatxlcplllockin),
.pmatxcmuplllockin(pmatxcmuplllockin),
.rtxblocksel(rtxblocksel),
.pcsgen3gen3datasel(pcsgen3gen3datasel),
.pldtxpmasyncp(pldtxpmasyncp),
.dataouttopma(dataouttopma),
.pmatxclkout(pmatxclkout),
.clockoutto10gpcs(clockoutto10gpcs),
.clockoutto8gpcs(clockoutto8gpcs),
.pmaclkdiv33lcout(pmaclkdiv33lcout),
.pcs10gclkdiv33lc(pcs10gclkdiv33lc),
.pmatxlcplllockout(pmatxlcplllockout),
.pmatxcmuplllockout(pmatxcmuplllockout),
.pmatxpmasyncp(pmatxpmasyncp)
);

defparam inst.lpm_type = lpm_type;
defparam inst.selectpcs = selectpcs;
defparam inst.channel_number = channel_number;

endmodule //stratixv_hssi_tx_pcs_pma_interface

`timescale 1 ps/1 ps

module stratixv_hssi_tx_pld_pcs_interface (
	datainfrompld,
	pld10gtxpldclk,
	pld10gtxpldrstn,
	pld10gtxdatavalid,
	pld10gtxcontrol,
	pld10gtxbitslip,
	pld10gtxdiagstatus,
	pld10gtxwordslip,
	pld10gtxbursten,
	pld8gpldtxclk,
	pld8gpolinvtx,
	pld8grevloopbk,
	pld8gwrenabletx,
	pld8grddisabletx,
	pld8gphfifoursttx,
	pld8gtxboundarysel,
	pld8gtxdatavalid,
	pld8gtxsynchdr,
	pld8gtxblkstart,
	pldgen3txrstn,
	pld8gtxurstpcs,
	clockinfrom10gpcs,
	pcs10gtxempty,
	pcs10gtxpempty,
	pcs10gtxpfull,
	pcs10gtxfull,
	pcs10gtxframe,
	pcs10gtxburstenexe,
	pcs10gtxwordslipexe,
	pcs8gfulltx,
	pcs8gemptytx,
	clockinfrom8gpcs,
	pmaclkdiv33lc,
	emsiptxdata,
	emsippcstxclkin,
	emsippcstxreset,
	emsippcstxctrl,
	pmatxlcplllock,
	pmatxcmuplllock,
	pldtxpmarstbin,
	pldlccmurstbin,
	rtxemsip,
	emsipenabledusermode,
	pcs10gextraout,
	pldtxpmasyncpin,
	pcs10gtxfifoinsert,
	pcs10gtxfifodel,
	pld10gextrain,
	pld10gtxclkout,
	pld10gtxempty,
	pld10gtxpempty,
	pld10gtxpfull,
	pld10gtxfull,
	pld10gtxframe,
	pld10gtxburstenexe,
	pld10gtxwordslipexe,
	pld8gfulltx,
	pld8gemptytx,
	pld8gtxclkout,
	pcs10gtxpldclk,
	pcs10gtxpldrstn,
	pcs10gtxdatavalid,
	dataoutto10gpcs,
	pcs10gtxcontrol,
	pcs10gtxbitslip,
	pcs10gtxdiagstatus,
	pcs10gtxwordslip,
	pcs10gtxbursten,
	pcs8gtxurstpcs,
	dataoutto8gpcs,
	pcs8gpldtxclk,
	pcs8gpolinvtx,
	pcs8grevloopbk,
	pcs8gwrenabletx,
	pcs8grddisabletx,
	pcs8gphfifoursttx,
	pcs8gtxboundarysel,
	pcs8gtxdatavalid,
	pcs8gtxsynchdr,
	pcs8gtxblkstart,
	pcsgen3txrstn,
	pldclkdiv33lc,
	emsippcstxclkout,
	emsippcstxstatus,
	pldtxpmarstbout,
	pldlccmurstbout,
	pldtxlcplllock,
	pldtxcmuplllock,
	pldtxiqclkout,
	pcs10gextrain,
	pld10gtxfifodel,
	pldtxpmasyncpout,
	pld10gtxfifoinsert,
	pld10gextraout
);

//parameter
parameter lpm_type = "stratixv_hssi_tx_pld_pcs_interface";
parameter data_source = "pld";
parameter is_10g_0ppm = "false";
parameter is_8g_0ppm = "false";
parameter channel_number = 0;

// I/O ports
input [63:0]	datainfrompld;
input			pld10gtxpldclk;
input			pld10gtxpldrstn;
input			pld10gtxdatavalid;
input [8:0]		pld10gtxcontrol;
input [6:0]		pld10gtxbitslip;
input [1:0]		pld10gtxdiagstatus;
input			pld10gtxwordslip;
input			pld10gtxbursten;
input			pld8gpldtxclk;
input			pld8gpolinvtx;
input			pld8grevloopbk;
input			pld8gwrenabletx;
input			pld8grddisabletx;
input			pld8gphfifoursttx;
input [4:0]		pld8gtxboundarysel;
input [3:0]		pld8gtxdatavalid;
input [1:0]		pld8gtxsynchdr;
input [3:0]		pld8gtxblkstart;
input			pldgen3txrstn;
input			pld8gtxurstpcs;
input			clockinfrom10gpcs;
input			pcs10gtxempty;
input			pcs10gtxpempty;
input			pcs10gtxpfull;
input			pcs10gtxfull;
input			pcs10gtxframe;
input			pcs10gtxburstenexe;
input			pcs10gtxwordslipexe;
input			pcs8gfulltx;
input			pcs8gemptytx;
input			clockinfrom8gpcs;
input			pmaclkdiv33lc;
input [63:0]	emsiptxdata;
input [2:0]		emsippcstxclkin;
input [5:0]		emsippcstxreset;
input [43:0]	emsippcstxctrl;
input			pmatxlcplllock;
input			pmatxcmuplllock;
input			pldtxpmarstbin;
input			pldlccmurstbin;
input			rtxemsip;
input			emsipenabledusermode;
input [3:0]		pcs10gextraout;
input			pldtxpmasyncpin;
input			pcs10gtxfifoinsert;
input			pcs10gtxfifodel;
input [3:0]		pld10gextrain;
	
output			pld10gtxclkout;
output			pld10gtxempty;
output			pld10gtxpempty;
output			pld10gtxpfull;
output			pld10gtxfull;
output			pld10gtxframe;
output			pld10gtxburstenexe;
output			pld10gtxwordslipexe;
output			pld8gfulltx;
output			pld8gemptytx;
output			pld8gtxclkout;
output			pcs10gtxpldclk;
output			pcs10gtxpldrstn;
output			pcs10gtxdatavalid;
output [63:0]	dataoutto10gpcs;
output [8:0]	pcs10gtxcontrol;
output [6:0]	pcs10gtxbitslip;
output [1:0]	pcs10gtxdiagstatus;
output			pcs10gtxwordslip;
output			pcs10gtxbursten;
output			pcs8gtxurstpcs;
output [43:0]	dataoutto8gpcs;
output			pcs8gpldtxclk;
output			pcs8gpolinvtx;
output			pcs8grevloopbk;
output			pcs8gwrenabletx;
output			pcs8grddisabletx;
output			pcs8gphfifoursttx;
output [4:0]	pcs8gtxboundarysel;
output [3:0]	pcs8gtxdatavalid;
output [1:0]	pcs8gtxsynchdr;
output [3:0]	pcs8gtxblkstart;
output			pcsgen3txrstn;
output			pldclkdiv33lc;
output [2:0]	emsippcstxclkout;
output [16:0]	emsippcstxstatus;
output			pldtxpmarstbout;
output			pldlccmurstbout;
output			pldtxlcplllock;
output			pldtxcmuplllock;
output			pldtxiqclkout;
output [3:0]	pcs10gextrain;
output			pld10gtxfifodel;
output			pldtxpmasyncpout;
output			pld10gtxfifoinsert;
output [3:0]	pld10gextraout;

stratixv_hssi_tx_pld_pcs_interface_encrypted inst (
.datainfrompld(datainfrompld),
.pld10gtxpldclk(pld10gtxpldclk),
.pld10gtxpldrstn(pld10gtxpldrstn),
.pld10gtxdatavalid(pld10gtxdatavalid),
.pld10gtxcontrol(pld10gtxcontrol),
.pld10gtxbitslip(pld10gtxbitslip),
.pld10gtxdiagstatus(pld10gtxdiagstatus),
.pld10gtxwordslip(pld10gtxwordslip),
.pld10gtxbursten(pld10gtxbursten),
.pld8gpldtxclk(pld8gpldtxclk),
.pld8gpolinvtx(pld8gpolinvtx),
.pld8grevloopbk(pld8grevloopbk),
.pld8gwrenabletx(pld8gwrenabletx),
.pld8grddisabletx(pld8grddisabletx),
.pld8gphfifoursttx(pld8gphfifoursttx),
.pld8gtxboundarysel(pld8gtxboundarysel),
.pld8gtxdatavalid(pld8gtxdatavalid),
.pld8gtxsynchdr(pld8gtxsynchdr),
.pld8gtxblkstart(pld8gtxblkstart),
.pldgen3txrstn(pldgen3txrstn),
.pld8gtxurstpcs(pld8gtxurstpcs),
.clockinfrom10gpcs(clockinfrom10gpcs),
.pcs10gtxempty(pcs10gtxempty),
.pcs10gtxpempty(pcs10gtxpempty),
.pcs10gtxpfull(pcs10gtxpfull),
.pcs10gtxfull(pcs10gtxfull),
.pcs10gtxframe(pcs10gtxframe),
.pcs10gtxburstenexe(pcs10gtxburstenexe),
.pcs10gtxwordslipexe(pcs10gtxwordslipexe),
.pcs8gfulltx(pcs8gfulltx),
.pcs8gemptytx(pcs8gemptytx),
.clockinfrom8gpcs(clockinfrom8gpcs),
.pmaclkdiv33lc(pmaclkdiv33lc),
.emsiptxdata(emsiptxdata),
.emsippcstxclkin(emsippcstxclkin),
.emsippcstxreset(emsippcstxreset),
.emsippcstxctrl(emsippcstxctrl),
.pmatxlcplllock(pmatxlcplllock),
.pmatxcmuplllock(pmatxcmuplllock),
.pldtxpmarstbin(pldtxpmarstbin),
.pldlccmurstbin(pldlccmurstbin),
.rtxemsip(rtxemsip),
.emsipenabledusermode(emsipenabledusermode),
.pcs10gextraout(pcs10gextraout),
.pldtxpmasyncpin(pldtxpmasyncpin),
.pcs10gtxfifoinsert(pcs10gtxfifoinsert),
.pcs10gtxfifodel(pcs10gtxfifodel),
.pld10gextrain(pld10gextrain),
.pld10gtxclkout(pld10gtxclkout),
.pld10gtxempty(pld10gtxempty),
.pld10gtxpempty(pld10gtxpempty),
.pld10gtxpfull(pld10gtxpfull),
.pld10gtxfull(pld10gtxfull),
.pld10gtxframe(pld10gtxframe),
.pld10gtxburstenexe(pld10gtxburstenexe),
.pld10gtxwordslipexe(pld10gtxwordslipexe),
.pld8gfulltx(pld8gfulltx),
.pld8gemptytx(pld8gemptytx),
.pld8gtxclkout(pld8gtxclkout),
.pcs10gtxpldclk(pcs10gtxpldclk),
.pcs10gtxpldrstn(pcs10gtxpldrstn),
.pcs10gtxdatavalid(pcs10gtxdatavalid),
.dataoutto10gpcs(dataoutto10gpcs),
.pcs10gtxcontrol(pcs10gtxcontrol),
.pcs10gtxbitslip(pcs10gtxbitslip),
.pcs10gtxdiagstatus(pcs10gtxdiagstatus),
.pcs10gtxwordslip(pcs10gtxwordslip),
.pcs10gtxbursten(pcs10gtxbursten),
.pcs8gtxurstpcs(pcs8gtxurstpcs),
.dataoutto8gpcs(dataoutto8gpcs),
.pcs8gpldtxclk(pcs8gpldtxclk),
.pcs8gpolinvtx(pcs8gpolinvtx),
.pcs8grevloopbk(pcs8grevloopbk),
.pcs8gwrenabletx(pcs8gwrenabletx),
.pcs8grddisabletx(pcs8grddisabletx),
.pcs8gphfifoursttx(pcs8gphfifoursttx),
.pcs8gtxboundarysel(pcs8gtxboundarysel),
.pcs8gtxdatavalid(pcs8gtxdatavalid),
.pcs8gtxsynchdr(pcs8gtxsynchdr),
.pcs8gtxblkstart(pcs8gtxblkstart),
.pcsgen3txrstn(pcsgen3txrstn),
.pldclkdiv33lc(pldclkdiv33lc),
.emsippcstxclkout(emsippcstxclkout),
.emsippcstxstatus(emsippcstxstatus),
.pldtxpmarstbout(pldtxpmarstbout),
.pldlccmurstbout(pldlccmurstbout),
.pldtxlcplllock(pldtxlcplllock),
.pldtxcmuplllock(pldtxcmuplllock),
.pldtxiqclkout(pldtxiqclkout),
.pcs10gextrain(pcs10gextrain),
.pld10gtxfifodel(pld10gtxfifodel),
.pldtxpmasyncpout(pldtxpmasyncpout),
.pld10gtxfifoinsert(pld10gtxfifoinsert),
.pld10gextraout(pld10gextraout)
);

defparam inst.lpm_type = lpm_type;
defparam inst.data_source = data_source;
defparam inst.is_10g_0ppm = is_10g_0ppm;
defparam inst.is_8g_0ppm = is_8g_0ppm;
defparam inst.channel_number = channel_number;

endmodule //stratixv_hssi_tx_pld_pcs_interface


// ***********************************************************
// This WYSIWYG atom header was automatically generated by the
// Atmgen build tool. To change it, alter data stored in the 
// corresponding WYS file(s) in the tools/atmgen subdirectory.
// ***********************************************************

// *** Section 1 -- Header ***

// -----------------------------------------------------------
//
// Module Name : stratixv_hssi_refclk_divider
//
// Description : DEV_FAMILY_STRATIXV stratixv_hssi_refclk_divider Verilog simulation model
//
// -----------------------------------------------------------

`timescale 1 ps/1 ps

// *** End of Section 1 ***

// *** Section 3 -- Module declaration ***

module stratixv_hssi_refclk_divider (
			dpriodisable,
			dprioin,
			inclk,

			clkout,
			dprioout 
			);

// *** End of Section 3 ***

// *** Section 4 -- Port size declarations ***

// Note: Variable port sizes dictated by parameters are not currently defined in
// the WYS file data. Busses are marked with the VARIABLE notation as a reminder.

`define  CLKOUT_PORTSIZE_CONST_stratixv_hssi_refclk_divider  1		
`define  DPRIODISABLE_PORTSIZE_CONST_stratixv_hssi_refclk_divider  1		
`define  DPRIOIN_PORTSIZE_CONST_stratixv_hssi_refclk_divider  1		
`define  DPRIOOUT_PORTSIZE_CONST_stratixv_hssi_refclk_divider  1		
`define  INCLK_PORTSIZE_CONST_stratixv_hssi_refclk_divider  1		

// *** End of Section 4 ***

// *** Section 5 -- Port declarations ***

input  dpriodisable;
input  dprioin;
input  inclk;
output clkout;
output dprioout;

// *** End of Section 5 ***

// *** Section 6 -- Parameter declarations and default values ***

parameter lpm_type = "stratixv_hssi_refclk_divider";
parameter divide_by =  1 ;
parameter enabled = "false";
parameter refclk_coupling_termination = "normal_100_ohm_termination";

// SIMULATION_ONLY_PARAMETERS_BEGIN

parameter protocol_hint = "basic";

// SIMULATION_ONLY_PARAMETERS_END

// *** End of Section 6 ***

// *** Section 7 -- Port declarations with defaults, if any ***

// This section will always be empty for WYSIWYG atoms
// tri1 devclrn;		//sample

// *** End of Section 7 ***

wire inclk_ipd;
wire dprioin_ipd;
wire dpriodisable_ipd;

assign inclk_ipd = (inclk === 1'b1) ? 1'b1 : 1'b0;
assign dprioin_ipd = (dprioin === 1'b1) ? 1'b1 : 1'b0;
assign dpriodisable_ipd = (dpriodisable === 1'b1) ? 1'b1 : 1'b0;

specify
    (inclk => clkout) = (0,0);
endspecify

wire divide_by_2_clk;
wire divide_by_2_out;

assign divide_by_2_clk = (enabled == "false") ? 1'b0 : inclk_ipd;
stratixv_hssi_aux_clock_div divide_by_2 (
    .clk(divide_by_2_clk),
    .reset(1'b0),
    .enable_d (1'b0),     // enable dprio
    .d        (8'h01),    // dprio
    .clkout(divide_by_2_out)
);
defparam divide_by_2.clk_divide_by = 2;
defparam divide_by_2.extra_latency = 0;

assign clkout = (enabled == "false") ? inclk_ipd : divide_by_2_out;
assign dprioout = (dpriodisable_ipd == 1'b1) ? 1'b0 : dprioin_ipd;

endmodule

`timescale 1 ps / 1 ps
module stratixv_hssi_aux_clock_div (
    clk,     // input clock
    reset,   // reset
    enable_d, // enable DPRIO
    d,        // division factor for DPRIO support
    clkout   // divided clock
);
input clk,reset;
input enable_d;
input [7:0] d;
output clkout;


parameter clk_divide_by  = 1;
parameter extra_latency  = 0;

integer clk_edges,m;
reg [2*extra_latency:0] div_n_register;
reg [7:0] d_factor_dly;
reg [31:0] clk_divide_value;

wire [7:0] d_factor;
wire int_reset;

initial
begin
    div_n_register = 'b0;
    clk_edges = -1;
    m = 0;
    d_factor_dly =  'b0;
    clk_divide_value = clk_divide_by;
end

assign d_factor = (enable_d === 1'b1) ? d : clk_divide_value[7:0];

always @(d_factor)
begin
    d_factor_dly <= d_factor;
end


// create a reset pulse when there is a change in the d_factor value
assign int_reset = (d_factor !== d_factor_dly) ? 1'b1 : 1'b0;

always @(posedge clk or negedge clk or posedge reset or posedge int_reset)
begin
    div_n_register <= {div_n_register, div_n_register[0]};

    if ((reset === 1'b1) || (int_reset === 1'b1)) 
    begin
        clk_edges = -1;
        div_n_register <= 'b0;
    end
    else
    begin
        if (clk_edges == -1) 
        begin
            div_n_register[0] <= clk;
            if (clk == 1'b1) clk_edges = 0;
        end
        else if (clk_edges % d_factor == 0) 
                div_n_register[0] <= ~div_n_register[0];
        if (clk_edges >= 0 || clk == 1'b1)
            clk_edges = (clk_edges + 1) % (2*d_factor) ;
    end
end

assign clkout = div_n_register[2*extra_latency];

endmodule


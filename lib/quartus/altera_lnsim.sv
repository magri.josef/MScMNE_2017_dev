// (C) 2001-2010 Altera Corporation. All rights reserved.
// Your use of Altera Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License Subscription 
// Agreement, Altera MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the applicable 
// agreement for further details.

`timescale 1ps/1ps
module altera_pll
#(
	//parameter
	parameter reference_clock_frequency       = "0 ps",
	parameter pll_type                        = "General",
	parameter number_of_clocks 				  = 1,
	parameter operation_mode				  = "internal feedback",
	parameter deserialization_factor 		  = 4,
	parameter data_rate                 	  = 0,
	parameter clock_switchover_mode 		  = "Auto",
	parameter clock_switchover_delay 		  = 3,
	
	parameter output_clock_frequency0 		  = "0 ps",
	parameter phase_shift0					  = "0 ps",
	parameter duty_cycle0					  = 50,
	
	parameter output_clock_frequency1 		  = "0 ps",
	parameter phase_shift1					  = "0 ps",
	parameter duty_cycle1					  = 50,
	
	parameter output_clock_frequency2 		  = "0 ps",
	parameter phase_shift2					  = "0 ps",
	parameter duty_cycle2					  = 50,
	
	parameter output_clock_frequency3 		  = "0 ps",
	parameter phase_shift3					  = "0 ps",
	parameter duty_cycle3					  = 50,
	
	parameter output_clock_frequency4 		  = "0 ps",
	parameter phase_shift4					  = "0 ps",
	parameter duty_cycle4					  = 50,
	
	parameter output_clock_frequency5 		  = "0 ps",
	parameter phase_shift5					  = "0 ps",
	parameter duty_cycle5					  = 50,
	
	parameter output_clock_frequency6 		  = "0 ps",
	parameter phase_shift6					  = "0 ps",
	parameter duty_cycle6					  = 50,
	
	parameter output_clock_frequency7 		  = "0 ps",
	parameter phase_shift7					  = "0 ps",
	parameter duty_cycle7					  = 50,
	
	parameter output_clock_frequency8 		  = "0 ps",
	parameter phase_shift8					  = "0 ps",
	parameter duty_cycle8					  = 50,
	
	parameter output_clock_frequency9 		  = "0 ps",
	parameter phase_shift9					  = "0 ps",
	parameter duty_cycle9					  = 50,	
	
	parameter output_clock_frequency10 		  = "0 ps",
	parameter phase_shift10					  = "0 ps",
	parameter duty_cycle10					  = 50,
	
	parameter output_clock_frequency11 		  = "0 ps",
	parameter phase_shift11					  = "0 ps",
	parameter duty_cycle11					  = 50,
	
	parameter output_clock_frequency12 		  = "0 ps",
	parameter phase_shift12					  = "0 ps",
	parameter duty_cycle12					  = 50,
	
	parameter output_clock_frequency13 		  = "0 ps",
	parameter phase_shift13					  = "0 ps",
	parameter duty_cycle13					  = 50,
	
	parameter output_clock_frequency14 		  = "0 ps",
	parameter phase_shift14					  = "0 ps",
	parameter duty_cycle14					  = 50,
	
	parameter output_clock_frequency15 		  = "0 ps",
	parameter phase_shift15					  = "0 ps",
	parameter duty_cycle15					  = 50,
	
	parameter output_clock_frequency16 		  = "0 ps",
	parameter phase_shift16					  = "0 ps",
	parameter duty_cycle16					  = 50,
	
	parameter output_clock_frequency17 		  = "0 ps",
	parameter phase_shift17					  = "0 ps",
	parameter duty_cycle17					  = 50
) ( 

	//input
	input	refclk,
	input	fbclk,
	input	rst,
	/*input 	extswitch,
	input 	pllen,
	input   refclk1,
	input   coreclk,
	input   zdb_in,
	input	phase_en,
	input 	up_dn,
	input 	dprio_rst_n,
	input 	dprio_clk,
	input   dprio_write,
	input   dprio_read,
	input	dprio_ser_shift_load,
	input 	dprio_mdio_dis,
	input 	nreset,
	input   pfden,
	input 	[4:0]	cnt_sel,
	input 	[3:0]	clken,
	input 	[3:0]	clkin,
	input 	[1:0]	dprio_byte_en,
	input	[6:0]	dprio_addr,
	input 	[15:0]	dprio_writedata,*/
	
					
	//output
	output	[ number_of_clocks -1 : 0] outclk,
	output	fboutclk,
	output	locked,
	
	//inout
	inout zdbfbclk
	/*output  clk0bad,
	output 	clk1bad,
	output 	pllclksel, 
	output	vcoover,
	output	vcounder,
	output  loaden, 
	output  dprio_block_select,
	output	phase_done,
	output	[7:0]	dpaclk0,
	output	[7:0]	dpaclk1,
	output  [3:0]	extclk,
	output  [1:0]	pll_out,
	output  [1:0]	lvdsclk,
	output  [15:0]	dprio_readdata*/
	
);

//wire
wire [ number_of_clocks -1 :0] fboutclk_wire;
wire [ number_of_clocks -1 :0] locked_wire;
wire [ number_of_clocks -1 :0] outclk_wire;
wire [ 17 : 0] divclk_wire;
wire fb_clkin;
wire fb_bidir_clkin;
wire fb_obuf_clk;


generate
genvar i;  
    if (pll_type == "General")
    for (i=0; i<=number_of_clocks-1; i = i +1)
    begin : general
    generic_pll gpll (
                     .refclk(refclk),
                     .fbclk((operation_mode == "external feedback") ? fb_clkin : (operation_mode == "zero delay buffer") ? fb_bidir_clkin : fboutclk_wire[0]),
                     .rst(rst),
                     .fboutclk(fboutclk_wire[i]),
                     .outclk(outclk_wire[i]),
                     .locked(locked_wire[i])
                 );

    defparam 
        gpll.reference_clock_frequency = reference_clock_frequency,
        gpll.output_clock_frequency = (i == 0) ? output_clock_frequency0 : 
        							(i == 1) ? output_clock_frequency1 :
        							(i == 2) ? output_clock_frequency2 :
        							(i == 3) ? output_clock_frequency3 :
        							(i == 4) ? output_clock_frequency4 :
        							(i == 5) ? output_clock_frequency5 :
        							(i == 6) ? output_clock_frequency6 :
        							(i == 7) ? output_clock_frequency7 :
        							(i == 8) ? output_clock_frequency8 :
        							(i == 9) ? output_clock_frequency9 :
        							(i == 10) ? output_clock_frequency10 :
        							(i == 11) ? output_clock_frequency11 :
        							(i == 12) ? output_clock_frequency12 :
        							(i == 13) ? output_clock_frequency13 :
        							(i == 14) ? output_clock_frequency14 :
        							(i == 15) ? output_clock_frequency15 :
        							(i == 16) ? output_clock_frequency16 : output_clock_frequency17, 
        gpll.duty_cycle = (i == 0) ? duty_cycle0 : 
						(i == 1) ? duty_cycle1 :
        				(i == 2) ? duty_cycle2 :
        				(i == 3) ? duty_cycle3 :
        				(i == 4) ? duty_cycle4 :
        				(i == 5) ? duty_cycle5 :
        				(i == 6) ? duty_cycle6 :
        				(i == 7) ? duty_cycle7 :
        				(i == 8) ? duty_cycle8 :
        				(i == 9) ? duty_cycle9 :
        				(i == 10) ? duty_cycle10 :
        				(i == 11) ? duty_cycle11 :
        				(i == 12) ? duty_cycle12 :
        				(i == 13) ? duty_cycle13 :
        				(i == 14) ? duty_cycle14 :
        				(i == 15) ? duty_cycle15 :
        				(i == 16) ? duty_cycle16 : duty_cycle17, 
        gpll.phase_shift = (i == 0) ? phase_shift0 : 
							(i == 1) ? phase_shift1 :
        					(i == 2) ? phase_shift2 :
        					(i == 3) ? phase_shift3 :
        					(i == 4) ? phase_shift4 :
        					(i == 5) ? phase_shift5 :
        					(i == 6) ? phase_shift6 :
        					(i == 7) ? phase_shift7 :
        					(i == 8) ? phase_shift8 :
        					(i == 9) ? phase_shift9 :
        					(i == 10) ? phase_shift10 :
        					(i == 11) ? phase_shift11 :
        					(i == 12) ? phase_shift12 :
        					(i == 13) ? phase_shift13 :
        					(i == 14) ? phase_shift14 :
        					(i == 15) ? phase_shift15 :
        					(i == 16) ? phase_shift16 : phase_shift17;
    end   
endgenerate

assign fboutclk = (operation_mode == "external feedback") ? fb_obuf_clk : (operation_mode == "zero delay buffer") ? fb_bidir_clkin : fboutclk_wire[0]; 
assign locked = locked_wire[0];
//assign outclk = (pll_type == "Dynamic Phase Shift" || pll_type == "Clock Switch Over") ? divclk_wire[ number_of_clocks -1 :0] : outclk_wire;
assign outclk = outclk_wire;

/*generate 
	if (pll_type == "LVDS") 
	begin : lvds
	altera_pll_lvds lvds_pll (
						//input
						.refclk(refclk),
    					.rst(rst),
 			   			//output
    					.outclk0(outclk_wire[0]),
    					.outclk1(outclk_wire[1]),
    					.outclk2(outclk_wire[2]),
    					.locked(locked_wire[0])
    				);
    defparam
    	lvds_pll.reference_clock_frequency = reference_clock_frequency,
		lvds_pll.data_rate = data_rate,
        lvds_pll.deserialization_factor = deserialization_factor;
	end
endgenerate

generate
	if (pll_type == "Dynamic Phase Shift" || pll_type == "Clock Switch Over")
	begin : stratixv_pll
	altera_pll_stratixv_pll stratixvpll (
						//input
						.extswitch(extswitch),
						.pllen(pllen),
						.refclk0(refclk),
						.refclk1(refclk1),
						.cnt_sel(cnt_sel),
						.clken(clken),
						.clkin(clkin),
						.coreclk(coreclk),
						.zdb_in(zdb_in),
						.phase_en(phase_en),
						.up_dn(up_dn),
						.dprio_rst_n(dprio_rst_n),
						.dprio_clk(dprio_clk),
						.dprio_write(dprio_write),
						.dprio_read(dprio_read),
						.dprio_byte_en(dprio_byte_en),
						.dprio_reg_addr(dprio_addr),
						.dprio_writedata(dprio_writedata),
						.dprio_ser_shift_load(dprio_ser_shift_load),
						.dprio_mdio_dis(dprio_mdio_dis),
						.nreset(nreset),
						.pfden(pfden),

						//output
						.clk0bad(clk0bad),
						.clk1bad(clk1bad),
						.pllclksel(pllclksel),
						.lock(locked_wire[0]),
						.vcoover(vcoover),
						.vcounder(vcounder),
						.dpaclk0(dpaclk0),
						.dpaclk1(dpaclk1),
						.extclk(extclk),
						.pll_out(pll_out),
						.lvdsclk(lvdsclk),
						.loaden(loaden),
						.dprio_readdata(dprio_readdata),
						.dprio_block_select(dprio_block_select),
						.divclk(divclk_wire),
						.phase_done(phase_done)
					);
	defparam  
        stratixvpll.reference_clock_frequency = reference_clock_frequency,
        stratixvpll.clock_switchover_mode = clock_switchover_mode,
        stratixvpll.clock_switchover_delay = clock_switchover_delay,
        stratixvpll.output_clock_frequency0 = output_clock_frequency0, 
        stratixvpll.output_clock_frequency1 = output_clock_frequency1, 
        stratixvpll.output_clock_frequency2 = output_clock_frequency2, 
        stratixvpll.output_clock_frequency3 = output_clock_frequency3, 
        stratixvpll.output_clock_frequency4 = output_clock_frequency4, 
        stratixvpll.output_clock_frequency5 = output_clock_frequency5, 
        stratixvpll.output_clock_frequency6 = output_clock_frequency6, 
        stratixvpll.output_clock_frequency7 = output_clock_frequency7, 
        stratixvpll.output_clock_frequency8 = output_clock_frequency8, 
        stratixvpll.output_clock_frequency9 = output_clock_frequency9, 
        stratixvpll.output_clock_frequency10 = output_clock_frequency10, 
        stratixvpll.output_clock_frequency11 = output_clock_frequency11, 
        stratixvpll.output_clock_frequency12 = output_clock_frequency12, 
        stratixvpll.output_clock_frequency13 = output_clock_frequency13, 
        stratixvpll.output_clock_frequency14 = output_clock_frequency14, 
        stratixvpll.output_clock_frequency15 = output_clock_frequency15, 
        stratixvpll.output_clock_frequency16 = output_clock_frequency16, 
        stratixvpll.output_clock_frequency17 = output_clock_frequency17, 
        stratixvpll.duty_cycle0 = duty_cycle0, 
        stratixvpll.duty_cycle1 = duty_cycle1, 
        stratixvpll.duty_cycle2 = duty_cycle2, 
        stratixvpll.duty_cycle3 = duty_cycle3, 
        stratixvpll.duty_cycle4 = duty_cycle4, 
        stratixvpll.duty_cycle5 = duty_cycle5, 
        stratixvpll.duty_cycle6 = duty_cycle6, 
        stratixvpll.duty_cycle7 = duty_cycle7, 
        stratixvpll.duty_cycle8 = duty_cycle8, 
        stratixvpll.duty_cycle9 = duty_cycle9, 
        stratixvpll.duty_cycle10 = duty_cycle10, 
        stratixvpll.duty_cycle11 = duty_cycle11, 
        stratixvpll.duty_cycle12 = duty_cycle12, 
        stratixvpll.duty_cycle13 = duty_cycle13, 
        stratixvpll.duty_cycle14 = duty_cycle14, 
        stratixvpll.duty_cycle15 = duty_cycle15, 
        stratixvpll.duty_cycle16 = duty_cycle16, 
        stratixvpll.duty_cycle17 = duty_cycle17, 
        stratixvpll.phase_shift0 = phase_shift0, 
        stratixvpll.phase_shift1 = phase_shift1, 
        stratixvpll.phase_shift2 = phase_shift2, 
        stratixvpll.phase_shift3 = phase_shift3, 
        stratixvpll.phase_shift4 = phase_shift4, 
        stratixvpll.phase_shift5 = phase_shift5, 
        stratixvpll.phase_shift6 = phase_shift6, 
        stratixvpll.phase_shift7 = phase_shift7, 
        stratixvpll.phase_shift8 = phase_shift8, 
        stratixvpll.phase_shift9 = phase_shift9, 
        stratixvpll.phase_shift10 = phase_shift10, 
        stratixvpll.phase_shift11 = phase_shift11, 
        stratixvpll.phase_shift12 = phase_shift12, 
        stratixvpll.phase_shift13 = phase_shift13, 
        stratixvpll.phase_shift14 = phase_shift14, 
        stratixvpll.phase_shift15 = phase_shift15, 
        stratixvpll.phase_shift16 = phase_shift16, 
        stratixvpll.phase_shift17 = phase_shift17;
	end
	endgenerate*/
	
generate
    if (operation_mode == "external feedback")
    begin: fb_ibuf
    alt_inbuf fb_ibuf (
                          .i(fbclk),
                          .o(fb_clkin)
                      );
    defparam fb_ibuf.enable_bus_hold = "NONE";
    end
endgenerate

generate 
    if (operation_mode == "external feedback")
    begin: fb_obuf
    alt_outbuf fb_obuf (
                           .i(fboutclk_wire[0]),
                           .o(fb_obuf_clk)
                       );
    defparam fb_obuf.enable_bus_hold = "NONE";
    end
endgenerate

generate
    if (operation_mode == "zero delay buffer")
    begin: fb_iobuf
    alt_iobuf fb_iobuf (
                           .i(fboutclk_wire[0]),
                           .oe(1'b1),
                           .io(zdbfbclk),
                           .o(fb_bidir_clkin)
                       );
    defparam fb_iobuf.enable_bus_hold = "NONE";
    end
endgenerate

endmodule

`ifndef ALTERA_LNSIM_FUNCTIONS_DEFINED
`define ALTERA_LNSIM_FUNCTIONS_DEFINED
package altera_lnsim_functions;
	localparam MAX_STRING_LENGTH = 20;

	function real get_real_value;
		input [8*MAX_STRING_LENGTH:1] time_string;

		integer index;
		real result;
		real integer_value;
		real fractional_value;
		real fractional_precision;
		reg [8*MAX_STRING_LENGTH:1] temp_str;
		reg [8:1] temp_char;
		reg [8:1] temp_digit;
		reg [8*3:1] time_unit;

		begin
			result = 0.0;
			index = 0;
			fractional_precision = 0.0;
			integer_value = 0.0;
			fractional_value = 0.0;
			time_unit = "";

			temp_str = time_string;
	        
			// Get the integer value and fractional value by switching on the decimal point
			for (index = 1; index <= MAX_STRING_LENGTH; index = index + 1)
				begin
					// 1. Get the most signifcant character
					// 2. Convert the char to decimal
					// 3. Shift the temporary string to get the next character
					temp_char = temp_str[160:153];
					temp_digit = temp_char & 8'b00001111;
					temp_str = temp_str << 8;

					// Found the decimal point
					if (temp_char == ".")
						fractional_precision = 1;
					// Found a digit
					else if ((temp_char >= "0") && (temp_char <= "9"))
						if (fractional_precision > 0)
							begin
								fractional_precision = fractional_precision / 10;
								fractional_value = fractional_value + (fractional_precision * temp_digit);
							end
						else
							integer_value = (integer_value * 10) + temp_digit;
					else if ((temp_char >= "a" && temp_char <= "z") || (temp_char >= "A" && temp_char <= "Z"))
						begin
							// Convert to upper case
							if (temp_char >= "a" && temp_char <= "z")
								temp_char = (temp_char & 8'h5f);
							time_unit = (time_unit << 8) | temp_char;
						end
				end

			// The result is the integer and fractional values      
			result = integer_value + fractional_value;

			// Convert the frequency unit to the resolution of the timescale (currently ps)
			if (time_unit == "")
				result = result;
			else if (time_unit == "FS")
				result = result / 10**3;
			else if (time_unit == "PS")
				result = result;
			else if (time_unit == "NS")
				result = result * 10**3;
			else if (time_unit == "US")
				result = result * 10**6;
			else if (time_unit == "MS")
				result = result * 10**9;
			else if (time_unit == "KHZ")
				result = 10**9 / result;
			else if (time_unit == "MHZ")
				result = 10**6 / result;
			else if (time_unit == "GHZ")
				result = 10**3 / result;
			else
				$display("Error: Unit \"%s\" is not supported", time_unit);

			get_real_value = result;
		end
	endfunction

	function time get_time_value;
		input [8*MAX_STRING_LENGTH:1] time_string;

		integer index;
		time result;
		real integer_value;
		real fractional_value;
		real fractional_precision;
		reg [8*MAX_STRING_LENGTH:1] temp_str;
		reg [8:1] temp_char;
		reg [8:1] temp_digit;
		reg [8*3:1] time_unit;

		begin
			result = 0;
			index = 0;
			fractional_precision = 0.0;
			integer_value = 0.0;
			fractional_value = 0.0;
			time_unit = "";

			temp_str = time_string;
	        
			// Get the integer value and fractional value by switching on the decimal point
			for (index = 1; index <= MAX_STRING_LENGTH; index = index + 1)
				begin
					// 1. Get the most signifcant character
					// 2. Convert the char to decimal
					// 3. Shift the temporary string to get the next character
					temp_char = temp_str[160:153];
					temp_digit = temp_char & 8'b00001111;
					temp_str = temp_str << 8;

					// Found the decimal point
					if (temp_char == ".")
						fractional_precision = 1;
					// Found a digit
					else if ((temp_char >= "0") && (temp_char <= "9"))
						if (fractional_precision > 0)
							begin
								fractional_precision = fractional_precision / 10;
								fractional_value = fractional_value + (fractional_precision * temp_digit);
							end
						else
							integer_value = (integer_value * 10) + temp_digit;
					else if ((temp_char >= "a" && temp_char <= "z") || (temp_char >= "A" && temp_char <= "Z"))
						begin
							// Convert to upper case
							if (temp_char >= "a" && temp_char <= "z")
								temp_char = (temp_char & 8'h5f);
							time_unit = (time_unit << 8) | temp_char;
						end
				end

			// The result is the integer and fractional values      
			result = integer_value + fractional_value;

			// Convert the frequency unit to the resolution of the timescale (currently ps)
			if (time_unit == "")
				result = result;
			else if (time_unit == "FS")
				result = result / 10**3;
			else if (time_unit == "PS")
				result = result;
			else if (time_unit == "NS")
				result = result * 10**3;
			else if (time_unit == "US")
				result = result * 10**6;
			else if (time_unit == "MS")
				result = result * 10**9;
			else if (time_unit == "KHZ")
				result = 10**9 / result;
			else if (time_unit == "MHZ")
				result = 10**6 / result;
			else if (time_unit == "GHZ")
				result = 10**3 / result;
			else
				$display("Error: Unit \"%s\" is not supported", time_unit);

			get_time_value = result;
		end
	endfunction

	function [8*MAX_STRING_LENGTH-1:0] get_time_string;
		input time time_value;
		input [8*MAX_STRING_LENGTH:1] time_unit;

		integer pos;
		integer initial_pos;
		integer f_unit;	// 10**f_unit is offset from Hz for larger unit

		begin
			// Convert the frequency unit to the resolution of the timescale (currently ps)
			if (time_unit == "")
				begin
					get_time_string = "0 ps";
					initial_pos = 3;
					f_unit = -1;
				end
			else if (time_unit == "fs")
				begin
					get_time_string = "0.000000 fs";
					initial_pos = 3;
				end
			else if (time_unit == "ps")
				begin
					get_time_string = "0 ps";
					initial_pos = 3;
					f_unit = -1;
				end
			else if (time_unit == "ns")
				begin
					get_time_string = "0.000000 ns";
					initial_pos = 3;
				end
			else if (time_unit == "us")
				begin
					get_time_string = "0.000000 us";
					initial_pos = 3;
				end
			else if (time_unit == "ms")
				begin
					get_time_string = "0.000000 ms";
					initial_pos = 3;
				end
			else if (time_unit == "kHz")
				begin
					get_time_string = "0.000000 kHz";
					initial_pos = 4;
				end
			else if (time_unit == "MHz")
				begin
					get_time_string = "0.000000 MHz";
					initial_pos = 4;
				end
			else if (time_unit == "GHz")
				begin
					get_time_string = "0.000000 GHz";
					initial_pos = 4;
				end
			else
				$display("Error: Unit \"%s\" is not supported", time_unit);

			// Convert time back to string with frequency units
			// Since char inital positions are used by <time_unit>, start with digits at initial_pos
			for (pos = initial_pos; pos < MAX_STRING_LENGTH && time_value > 0; pos = pos + 1)
				begin
					if (f_unit == 0)
						begin
							get_time_string[pos*8 +: 8] = 8'h2e;
							pos = pos + 1;
						end
					f_unit = f_unit - 1;
					get_time_string[pos*8 +: 8] = (time_value % 10) | 8'h30;
					time_value = time_value / 10;
				end
		end

	endfunction

endpackage
`endif
`timescale 1 ps/1 ps
module generic_pll
#(
	parameter lpm_type = "generic_pll",
	parameter duty_cycle =  50,
	parameter output_clock_frequency = "0 ps",
	parameter phase_shift = "0 ps",
	parameter reference_clock_frequency = "0 ps",
	parameter sim_additional_refclk_cycles_to_lock = 0
) (
	input wire refclk,
	input wire rst,
	input wire fbclk,
	
	output wire outclk,
	output wire locked,
	output wire fboutclk
);

	import altera_lnsim_functions::*;

	real output_clock_high_period;
	real output_clock_low_period;
	real reference_clock_period_value;
	real output_clock_period_value;
	real phase_shift_value;
	real duty_cycle_value;
	real output_clock_period;
	reg rst_reg;
	reg outclk_reg;
	reg locked_reg;
	reg phase_shift_done_reg;
	integer refclk_cycles;

	initial
	begin
		reference_clock_period_value = get_real_value(reference_clock_frequency);
		output_clock_period_value = get_real_value(output_clock_frequency);
		phase_shift_value = get_real_value(phase_shift);
		duty_cycle_value = duty_cycle / 100.0;
		output_clock_high_period = output_clock_period_value * duty_cycle_value;
		output_clock_low_period = output_clock_period_value * (1.0 - duty_cycle_value);
		rst_reg = 0;
		outclk_reg = 0;
		locked_reg = 0;
		phase_shift_done_reg = 0;
		refclk_cycles = 0;

		$display("Info: =================================================");
		$display("Info:           Generic PLL Summary");
		$display("Info: =================================================");
		$printtimescale;
		$display("Info: hierarchical_name = %m");
		$display("Info: reference_clock_frequency = %0s", reference_clock_frequency);
		$display("Info: output_clock_frequency = %0s", output_clock_frequency);
		$display("Info: phase_shift = %0s", phase_shift);
		$display("Info: duty_cycle = %0d", duty_cycle);
		$display("Info: sim_additional_refclk_cycles_to_lock = %0d", sim_additional_refclk_cycles_to_lock);
		$display("Info: output_clock_high_period = %0f", output_clock_high_period);
		$display("Info: output_clock_low_period = %0f", output_clock_low_period);
	end

  always @(posedge rst or negedge rst)
    begin
      rst_reg = rst;
      if (rst)
        begin
          refclk_cycles = 0;
          phase_shift_done_reg = 0;
          locked_reg = 0;
        end
    end

  always @(posedge refclk && !rst_reg)
    if (refclk_cycles < sim_additional_refclk_cycles_to_lock)
      refclk_cycles = refclk_cycles + 1;
    else
      locked_reg = 1;

  always @(posedge locked_reg)
    begin
      outclk_reg = 0;
      #phase_shift_value outclk_reg = 1;
      phase_shift_done_reg = 1;
    end
    
  always
      wait (phase_shift_done_reg == 1)
        begin
          #output_clock_high_period outclk_reg = 0;
          #output_clock_low_period outclk_reg = 1;
        end

	assign outclk = outclk_reg && locked;
	assign locked = locked_reg;
	assign fboutclk = refclk && locked;

endmodule

`timescale 1 ps/1 ps
module generic_cdr
#(
	parameter reference_clock_frequency = "0 ps",
	parameter output_clock_frequency = "0 ps",
	parameter sim_debug_msg = "false"
) (
	input wire extclk,
	input wire ltd,
	input wire ltr,
	input wire pciel,
	input wire pciem,
	input wire ppmlock,
	input wire refclk,
	input wire rst,
	input wire sd,
	input wire rxp,

	output wire clk90bdes,
	output wire clk270bdes,
	output wire clklow,
	output reg deven,
	output reg dodd,
	output wire fref,
	output wire pfdmodelock,
	output wire rxplllock
);

	import altera_lnsim_functions::*;

	wire clk0_wire;
	wire clk0_pcie_00_wire;
	wire clk0_pcie_01_wire;
	wire clk90_wire;
	wire clk90_pcie_00_wire;
	wire clk90_pcie_01_wire;
	wire clk180_wire;
	wire clk270_wire;
	wire datain_ipd;
	wire datain_dly;
	reg deven_int1_reg;
	reg deven_int2_reg;
	reg dodd_int1_reg;
	reg dodd_int2_reg;
	wire fboutclk_wire;
	wire lck2ref_wire;
	wire locked_wire;

	initial
	begin
		deven <= 1'b1;
		dodd <= 1'b1;
		deven_int1_reg = 1'b0;
		dodd_int1_reg = 1'b0;
		deven_int2_reg = 1'b0;
		dodd_int2_reg = 1'b0;
		$display("Info: =================================================");
		$display("Info:           Generic CDR Summary");
		$display("Info: =================================================");
		$printtimescale;
		$display("Info: hierarchical_name = %m");
		$display("Info: reference_clock_frequency = %0s", reference_clock_frequency);
		$display("Info: output_clock_frequency = %0s", output_clock_frequency);
	end

	//////////////////////////////////////////////////
	// clk0
	//////////////////////////////////////////////////
	generic_pll
	#(
		.reference_clock_frequency(reference_clock_frequency),
		.output_clock_frequency(output_clock_frequency),
		.phase_shift("0 ps")
	) inst_cdr_clk0_pcie_00 (
		.refclk(refclk),
		.rst(rst),
		.fbclk(fboutclk_wire),
		
		.outclk(clk0_pcie_00_wire),
		.locked(locked_wire),
		.fboutclk(fboutclk_wire)
	);

	generic_pll
	#(
		.reference_clock_frequency(reference_clock_frequency),
		.output_clock_frequency(get_time_string(get_time_value(output_clock_frequency) * 2, "ps")),
		.phase_shift("0 ps")
	) inst_cdr_clk0_pcie_01 (
		.refclk(refclk),
		.rst(rst),
		.fbclk(fboutclk_wire),
		
		.outclk(clk0_pcie_01_wire),
		.locked(),
		.fboutclk()
	);
	
	assign clk0_wire = {pciem, pciel} == 2'b00 ? clk0_pcie_00_wire :
					   {pciem, pciel} == 2'b01 ? clk0_pcie_01_wire :
					   {pciem, pciel} == 2'b10 ? 1'bx :
					   1'bx;
	
	//////////////////////////////////////////////////
	// clk90
	//////////////////////////////////////////////////
	generic_pll
	#(
		.reference_clock_frequency(reference_clock_frequency),
		.output_clock_frequency(output_clock_frequency),
		.phase_shift(get_time_string(get_time_value(output_clock_frequency) / 4, "ps"))
	) inst_cdr_clk90_pcie_00 (
		.refclk(refclk),
		.rst(rst),
		.fbclk(fboutclk_wire),
		
		.outclk(clk90_pcie_00_wire),
		.locked(),
		.fboutclk()
	);

	generic_pll
	#(
		.reference_clock_frequency(reference_clock_frequency),
		.output_clock_frequency(get_time_string(get_time_value(output_clock_frequency) * 2, "ps")),
		.phase_shift(get_time_string(get_time_value(output_clock_frequency) * 2 / 4, "ps"))
	) inst_cdr_clk90_pcie_01 (
		.refclk(refclk),
		.rst(rst),
		.fbclk(fboutclk_wire),
		
		.outclk(clk90_pcie_01_wire),
		.locked(),
		.fboutclk()
	);

	assign clk90_wire = {pciem, pciel} == 2'b00 ? clk90_pcie_00_wire :
						{pciem, pciel} == 2'b01 ? clk90_pcie_01_wire :
						{pciem, pciel} == 2'b10 ? 1'bx :
						1'bx;

	//////////////////////////////////////////////////
	// clk180
	//////////////////////////////////////////////////
	assign clk180_wire = !clk0_wire;
	
	//////////////////////////////////////////////////
	// clk270
	//////////////////////////////////////////////////
	assign clk270_wire = !clk90_wire;

	//////////////////////////////////////////////////
	// clk90bdes
	//////////////////////////////////////////////////
	assign clk90bdes = clk90_wire;

	//////////////////////////////////////////////////
	// clk270bdes
	//////////////////////////////////////////////////
	assign clk270bdes = clk270_wire;
	
	//////////////////////////////////////////////////
	// clklow
	//////////////////////////////////////////////////
	assign clklow = fboutclk_wire && !rst;

	//////////////////////////////////////////////////
	// deven and dodd
	//////////////////////////////////////////////////
	assign datain_ipd = (rxp === 1'b1) ? 1'b1 : 1'b0;
	assign #5 datain_dly = datain_ipd;
	
	always @ (clk0_wire)
		if (clk0_wire == 1'b1)
			deven_int1_reg <= datain_dly;
		else if (clk0_wire == 1'b0)
			deven_int2_reg <= deven_int1_reg;

	always @ (clk180_wire)
		if (clk180_wire == 1'b1)
			dodd_int1_reg <= datain_dly;
		else if (clk180_wire == 1'b0)
			dodd_int2_reg <= dodd_int1_reg;

	always @ (posedge clk90_wire)
		{deven ,dodd} <= {deven_int2_reg, dodd_int2_reg};

	//////////////////////////////////////////////////
	// fref
	//////////////////////////////////////////////////
	assign fref = refclk && !rst;
	
	//////////////////////////////////////////////////
	// pciel, pciem, and rst
	//////////////////////////////////////////////////
	generate
		if (sim_debug_msg == "true")
			always @(ltd or ltr or pciel or pciem or ppmlock or rst or sd) 
				begin
					$display("Info: =================================================");
					$display("Info:           Generic CDR at time %0d", $time);
					$display("Info: =================================================");
					$display("Info: hierarchical_name = %m");
					$display("Info: ltd = %v ", ltd);
					$display("Info: ltr = %v ", ltr);
					$display("Info: pciel = %v", pciel);
					$display("Info: pciem = %v", pciem);
					$display("Info: ppmlock = %v", ppmlock);
					$display("Info: rst = %v ", rst);
					$display("Info: sd = %v", sd);
				end
	endgenerate

	//////////////////////////////////////////////////
	// pfdmodelock
	//////////////////////////////////////////////////
	assign pfdmodelock = locked_wire && ~rst;
	
	//////////////////////////////////////////////////
	// rxplllock
	//////////////////////////////////////////////////
	assign lck2ref_wire = ltd ? 1'b0 :
						 (~sd | ~ppmlock | ltr ) ? 1'b1 : 
						 (sd & ppmlock & ~ltr & ~pfdmodelock ) ? 1'b1 :
						 (sd & ppmlock & ~ltr &  pfdmodelock ) ? 1'b0 :
						 1'b0;
	assign rxplllock = !lck2ref_wire;

endmodule


`timescale 1 ps/1 ps

// Deactivate the LEDA rule that requires uppercase letters for all
// parameter names
// leda rule_G_521_3_B off

//--------------------------------------------------------------------------
// Module Name     : common_28nm_ram_pulse_generator
// Description     : Generate pulse to initiate memory read/write operations
//--------------------------------------------------------------------------

module common_28nm_ram_pulse_generator (
                                    clk,
                                    ena,
                                    pulse,
                                    cycle
                                   );
input  clk;   // clock
input  ena;   // pulse enable
output pulse; // pulse
output cycle; // delayed clock

parameter delay_pulse = 1'b0;
parameter start_delay = (delay_pulse == 1'b0) ? 1 : 2; // delay write
reg  state;
reg  clk_prev;
wire clk_ipd;

specify
    specparam t_decode = 0,t_access = 0;
    (posedge clk => (pulse +: state)) = (t_decode,t_access);
endspecify

buf #(start_delay) (clk_ipd,clk);
wire  pulse_opd;

buf buf_pulse  (pulse,pulse_opd);

initial clk_prev = 1'bx;

always @(clk_ipd or posedge pulse)
begin
    if      (pulse) state <= 1'b0;
    else if (ena && clk_ipd === 1'b1 && clk_prev === 1'b0)   state <= 1'b1;
  clk_prev = clk_ipd;
end

assign cycle = clk_ipd;
assign pulse_opd = state; 

endmodule

//--------------------------------------------------------------------------
// Module Name     : common_28nm_ram_register
// Description     : Register module for RAM inputs/outputs
//--------------------------------------------------------------------------

`timescale 1 ps/1 ps

module common_28nm_ram_register (
                             d,
                             clk,
                             aclr,
                             devclrn,
                             devpor,
                             stall,
                             ena,
                             q,
                             aclrout
                            );

parameter width = 1;      // data width
parameter preset = 1'b0;  // clear acts as preset

input [width - 1:0] d;    // data
input clk;                // clock
input aclr;               // asynch clear
input devclrn,devpor;     // device wide clear/reset
input stall; // address stall
input ena;                // clock enable
output [width - 1:0] q;   // register output
output aclrout;           // delayed asynch clear

wire ena_ipd;
wire clk_ipd;
wire aclr_ipd;
wire [width - 1:0] d_ipd;

buf buf_ena (ena_ipd,ena);
buf buf_clk (clk_ipd,clk);
buf buf_aclr (aclr_ipd,aclr);
buf buf_d [width - 1:0] (d_ipd,d);
wire stall_ipd;

buf buf_stall (stall_ipd,stall);
wire  [width - 1:0] q_opd;

buf buf_q  [width - 1:0] (q,q_opd);
reg   [width - 1:0] q_reg;

reg viol_notifier;
wire reset;

assign reset = devpor && devclrn && (!aclr_ipd) && (ena_ipd);
specify
      $setup  (d,    posedge clk &&& reset, 0, viol_notifier);
      $setup  (aclr, posedge clk, 0, viol_notifier);
      $setup  (ena,  posedge clk &&& reset, 0, viol_notifier );
      $setup  (stall, posedge clk &&& reset, 0, viol_notifier );
      $hold   (posedge clk &&& reset, d   , 0, viol_notifier);
      $hold   (posedge clk, aclr, 0, viol_notifier);
      $hold   (posedge clk &&& reset, ena , 0, viol_notifier );
      $hold   (posedge clk &&& reset, stall, 0, viol_notifier );
      (posedge clk =>  (q +: q_reg)) = (0,0);
      (posedge aclr => (q +: q_reg)) = (0,0);
endspecify

initial q_reg <= (preset) ? {width{1'b1}} : 'b0;

always @(posedge clk_ipd or posedge aclr_ipd or negedge devclrn or negedge devpor)
begin
    if (aclr_ipd || ~devclrn || ~devpor)
        q_reg <= (preset) ? {width{1'b1}} : 'b0;
        else if (ena_ipd & !stall_ipd)
        q_reg <= d_ipd;
end
assign aclrout = aclr_ipd;

assign q_opd = q_reg; 

endmodule

`timescale 1 ps/1 ps

`define PRIME 1
`define SEC   0

//--------------------------------------------------------------------------
// Module Name     : common_28nm_ram_block
// Description     : Main RAM module
//--------------------------------------------------------------------------

module common_28nm_ram_block
    (
     portadatain,
     portaaddr,
     portawe,
     portare,
     portbdatain,
     portbaddr,
     portbwe,
     portbre,
     clk0, clk1,
     ena0, ena1,
     ena2, ena3,
     clr0, clr1,
     nerror,
     portabyteenamasks,
     portbbyteenamasks,
     portaaddrstall,
     portbaddrstall,
     devclrn,
     devpor,
     eccstatus,
     portadataout,
     portbdataout
      ,dftout
     );
// -------- GLOBAL PARAMETERS ---------
parameter operation_mode = "single_port";
parameter mixed_port_feed_through_mode = "dont_care";
parameter ram_block_type = "auto";
parameter logical_ram_name = "ram_name";

parameter init_file = "init_file.hex";
parameter init_file_layout = "none";

parameter ecc_pipeline_stage_enabled = "false";
parameter enable_ecc = "false";
parameter width_eccstatus = 2;
parameter data_interleave_width_in_bits = 1;
parameter data_interleave_offset_in_bits = 1;
parameter port_a_logical_ram_depth = 0;
parameter port_a_logical_ram_width = 0;
parameter port_a_first_address = 0;
parameter port_a_last_address = 0;
parameter port_a_first_bit_number = 0;

parameter port_a_data_out_clear = "none";

parameter port_a_data_out_clock = "none";

parameter port_a_data_width = 1;
parameter port_a_address_width = 1;
parameter port_a_byte_enable_mask_width = 1;

parameter port_b_logical_ram_depth = 0;
parameter port_b_logical_ram_width = 0;
parameter port_b_first_address = 0;
parameter port_b_last_address = 0;
parameter port_b_first_bit_number = 0;

parameter port_b_address_clear = "none";
parameter port_b_data_out_clear = "none";

parameter port_b_data_in_clock = "clock1";
parameter port_b_address_clock = "clock1";
parameter port_b_write_enable_clock = "clock1";
parameter port_b_read_enable_clock  = "clock1";
parameter port_b_byte_enable_clock = "clock1";
parameter port_b_data_out_clock = "none";

parameter port_b_data_width = 1;
parameter port_b_address_width = 1;
parameter port_b_byte_enable_mask_width = 1;

parameter port_a_read_during_write_mode = "new_data_no_nbe_read";
parameter port_b_read_during_write_mode = "new_data_no_nbe_read";
parameter power_up_uninitialized = "false";
parameter lpm_type = "stratixv_ram_block";
parameter lpm_hint = "true";
parameter connectivity_checking = "off";

parameter mem_init0 = 2048'b0;
parameter mem_init1 = 2048'b0;
parameter mem_init2 = 2048'b0;
parameter mem_init3 = 2048'b0;
parameter mem_init4 = 2048'b0;
parameter mem_init5 = 2048'b0;
parameter mem_init6 = 2048'b0;
parameter mem_init7 = 2048'b0;
parameter mem_init8 = 2048'b0;
parameter mem_init9 = 2048'b0;

parameter port_a_byte_size = 0;
parameter port_b_byte_size = 0;

parameter clk0_input_clock_enable  = "none"; // ena0,ena2,none
parameter clk0_core_clock_enable   = "none"; // ena0,ena2,none
parameter clk0_output_clock_enable = "none"; // ena0,none
parameter clk1_input_clock_enable  = "none"; // ena1,ena3,none
parameter clk1_core_clock_enable   = "none"; // ena1,ena3,none
parameter clk1_output_clock_enable = "none"; // ena1,none

parameter bist_ena = "false"; //false, true 

// SIMULATION_ONLY_PARAMETERS_BEGIN

parameter port_a_address_clear = "none";

parameter port_a_data_in_clock = "clock0";
parameter port_a_address_clock = "clock0";
parameter port_a_write_enable_clock = "clock0";
parameter port_a_byte_enable_clock = "clock0";
parameter port_a_read_enable_clock = "clock0";

// SIMULATION_ONLY_PARAMETERS_END

// LOCAL_PARAMETERS_BEGIN

parameter primary_port_is_a  = (port_b_data_width <= port_a_data_width) ? 1'b1 : 1'b0;
parameter primary_port_is_b  = ~primary_port_is_a;

parameter mode_is_rom_or_sp  = ((operation_mode == "rom") || (operation_mode == "single_port")) ? 1'b1 : 1'b0;
parameter data_width         = (primary_port_is_a) ? port_a_data_width : port_b_data_width;
parameter data_unit_width    = (mode_is_rom_or_sp | primary_port_is_b) ? port_a_data_width : port_b_data_width;
parameter address_width      = (mode_is_rom_or_sp | primary_port_is_b) ? port_a_address_width : port_b_address_width;
parameter address_unit_width = (mode_is_rom_or_sp | primary_port_is_a) ? port_a_address_width : port_b_address_width;
parameter wired_mode         = ((port_a_address_width == 1) && (port_a_address_width == port_b_address_width)
                                                            && (port_a_data_width != port_b_data_width));

parameter num_rows = 1 << address_unit_width;
parameter num_cols = (mode_is_rom_or_sp) ? 1 : ( wired_mode ? 2 :
                      ( (primary_port_is_a) ?
                      1 << (port_b_address_width - port_a_address_width) :
                      1 << (port_a_address_width - port_b_address_width) ) ) ;

parameter mask_width_prime = (primary_port_is_a) ?
                              port_a_byte_enable_mask_width : port_b_byte_enable_mask_width;
parameter mask_width_sec   = (primary_port_is_a) ?
                              port_b_byte_enable_mask_width : port_a_byte_enable_mask_width;

parameter byte_size_a = port_a_data_width/port_a_byte_enable_mask_width;
parameter byte_size_b = port_b_data_width/port_b_byte_enable_mask_width;

parameter mode_is_dp  = (operation_mode == "dual_port") ? 1'b1 : 1'b0;

// Hardware write modes
parameter dual_clock = ((operation_mode == "dual_port")  ||
                        (operation_mode == "bidir_dual_port")) &&
                        (port_b_address_clock == "clock1");
parameter both_new_data_same_port = (
                                      ((port_a_read_during_write_mode == "new_data_no_nbe_read") ||
                                       (port_a_read_during_write_mode == "dont_care")) &&
                                      ((port_b_read_during_write_mode == "new_data_no_nbe_read") ||
                                       (port_b_read_during_write_mode == "dont_care"))
                                    ) ? 1'b1 : 1'b0;
parameter hw_write_mode_a = (
                                ((port_a_read_during_write_mode == "old_data") ||
                                 (port_a_read_during_write_mode == "new_data_with_nbe_read"))
                            ) ? "R+W" : (
                                            dual_clock || (
                                               mixed_port_feed_through_mode == "dont_care" &&
                                               both_new_data_same_port
                                            ) ? "FW" : "DW"
                                        );
parameter hw_write_mode_b = (
                                ((port_b_read_during_write_mode == "old_data") ||
                                 (port_b_read_during_write_mode == "new_data_with_nbe_read"))
                            ) ? "R+W" : (
                                            dual_clock || (
                                               mixed_port_feed_through_mode == "dont_care" &&
                                               both_new_data_same_port
                                            ) ? "FW" : "DW"
                                        );
 parameter delay_write_pulse_a = (mode_is_dp && mixed_port_feed_through_mode == "dont_care") ? 1'b0 : ((hw_write_mode_a != "FW") ? 1'b1 : 1'b0);
parameter delay_write_pulse_b = (hw_write_mode_b != "FW") ? 1'b1 : 1'b0;
parameter be_mask_write_a     = (port_a_read_during_write_mode == "new_data_with_nbe_read") ? 1'b1 : 1'b0;
parameter be_mask_write_b     = (port_b_read_during_write_mode == "new_data_with_nbe_read") ? 1'b1 : 1'b0;
parameter old_data_write_a     = (port_a_read_during_write_mode == "old_data") ? 1'b1 : 1'b0;
parameter old_data_write_b     = (port_b_read_during_write_mode == "old_data") ? 1'b1 : 1'b0;
parameter read_before_write_a = (hw_write_mode_a == "R+W") ? 1'b1 : 1'b0;
parameter read_before_write_b = (hw_write_mode_b == "R+W") ? 1'b1 : 1'b0;

// LOCAL_PARAMETERS_END

// -------- PORT DECLARATIONS ---------
input portawe;
input portare;
input [port_a_data_width - 1:0] portadatain;
input [port_a_address_width - 1:0] portaaddr;
input [port_a_byte_enable_mask_width - 1:0] portabyteenamasks;

input portbwe, portbre;
input [port_b_data_width - 1:0] portbdatain;
input [port_b_address_width - 1:0] portbaddr;
input [port_b_byte_enable_mask_width - 1:0] portbbyteenamasks;

input clr0,clr1;
input clk0,clk1;
input ena0,ena1;
input ena2,ena3;
input nerror;

input devclrn,devpor;
input portaaddrstall;
input portbaddrstall;
output [port_a_data_width - 1:0] portadataout;
output [port_b_data_width - 1:0] portbdataout;
output [width_eccstatus - 1:0] eccstatus;
output [8:0] dftout;


tri0 portawe_int;
assign portawe_int = portawe;
tri1 portare_int;
assign portare_int = portare;
tri0 [port_a_data_width - 1:0] portadatain_int;
assign portadatain_int = portadatain;
tri0 [port_a_address_width - 1:0] portaaddr_int;
assign portaaddr_int = portaaddr;
tri1 [port_a_byte_enable_mask_width - 1:0] portabyteenamasks_int;
assign portabyteenamasks_int = portabyteenamasks;

tri0 portbwe_int;
assign portbwe_int = portbwe;
tri1 portbre_int;
assign portbre_int = portbre;
tri0 [port_b_data_width - 1:0] portbdatain_int;
assign portbdatain_int = portbdatain;
tri0 [port_b_address_width - 1:0] portbaddr_int;
assign portbaddr_int = portbaddr;
tri1 [port_b_byte_enable_mask_width - 1:0] portbbyteenamasks_int;
assign portbbyteenamasks_int = portbbyteenamasks;

tri0 clr0_int,clr1_int;
assign clr0_int = clr0;
assign clr1_int = clr1;
tri0 clk0_int,clk1_int;
assign clk0_int = clk0;
assign clk1_int = clk1;
tri1 ena0_int,ena1_int;
assign ena0_int = ena0;
assign ena1_int = ena1;
tri1 ena2_int,ena3_int;
assign ena2_int = ena2;
assign ena3_int = ena3;
tri1 nerror_int;
assign nerror_int = nerror;

tri0 portaaddrstall_int;
assign portaaddrstall_int = portaaddrstall;
tri0 portbaddrstall_int;
assign portbaddrstall_int = portbaddrstall;
tri1 devclrn;
tri1 devpor;


// -------- INTERNAL signals ---------
// clock / clock enable
wire clk_a_in,clk_a_byteena,clk_a_out,clk_a_out_secmux,clkena_a_out;
wire clk_a_rena, clk_a_wena;
wire clk_a_core;
wire clk_b_in,clk_b_byteena,clk_b_out,clk_b_out_secmux,clkena_b_out;
wire clk_b_rena, clk_b_wena;
wire clk_b_core;

wire write_cycle_a,write_cycle_b;

// asynch clear
wire datain_a_clr,dataout_a_clr,datain_b_clr,dataout_b_clr;
wire dataout_a_clr_reg, dataout_b_clr_reg;

wire addr_a_clr,addr_b_clr;
wire byteena_a_clr,byteena_b_clr;
wire we_a_clr, re_a_clr, we_b_clr, re_b_clr;

wire datain_a_clr_in,datain_b_clr_in;
wire addr_a_clr_in,addr_b_clr_in;
wire byteena_a_clr_in,byteena_b_clr_in;
wire we_a_clr_in, re_a_clr_in, we_b_clr_in, re_b_clr_in;

reg  mem_invalidate;
wire [`PRIME:`SEC] clear_asserted_during_write;
reg  clear_asserted_during_write_a,clear_asserted_during_write_b;

// port A registers
wire we_a_reg;
wire re_a_reg;
wire [port_a_address_width - 1:0] addr_a_reg;
wire [port_a_data_width - 1:0] datain_a_reg, dataout_a_reg;
reg  [port_a_data_width - 1:0] dataout_a;
wire [port_a_byte_enable_mask_width - 1:0] byteena_a_reg;
reg  out_a_is_reg;

// port B registers
wire we_b_reg, re_b_reg;
wire [port_b_address_width - 1:0] addr_b_reg;
wire [port_b_data_width - 1:0] datain_b_reg, dataout_b_reg, dataout_b_signal, ecc_pipeline_b_reg;
reg  [port_b_data_width - 1:0] dataout_b;
wire [port_b_byte_enable_mask_width - 1:0] byteena_b_reg;
reg  out_b_is_reg;

// placeholders for read/written data
reg  [data_width - 1:0] read_data_latch;
reg  [data_width - 1:0] mem_data;
reg  [data_width - 1:0] old_mem_data;

reg  [data_unit_width - 1:0] read_unit_data_latch;
reg  [data_width - 1:0]      mem_unit_data;

// pulses for A/B ports
wire write_pulse_a,write_pulse_b;
wire read_pulse_a,read_pulse_b;
wire read_pulse_a_feedthru,read_pulse_b_feedthru;
wire rw_pulse_a, rw_pulse_b;


wire [address_unit_width - 1:0] addr_prime_reg; // registered address
wire [address_width - 1:0]      addr_sec_reg;

wire [data_width - 1:0]       datain_prime_reg; // registered data
wire [data_unit_width - 1:0]  datain_sec_reg;


// pulses for primary/secondary ports
wire write_pulse_prime,write_pulse_sec;
wire read_pulse_prime,read_pulse_sec;
wire read_pulse_prime_feedthru,read_pulse_sec_feedthru;
wire rw_pulse_prime, rw_pulse_sec;

reg read_pulse_prime_last_value, read_pulse_sec_last_value;
reg rw_pulse_prime_last_value, rw_pulse_sec_last_value;

reg  [`PRIME:`SEC] dual_write;  // simultaneous write to same location

// (row,column) coordinates
reg  [address_unit_width - 1:0] row_sec;
reg  [address_width + data_unit_width - address_unit_width - 1:0] col_sec;

// memory core
reg  [data_width - 1:0] mem [num_rows - 1:0];

// byte enable
wire [data_width - 1:0]      mask_vector_prime, mask_vector_prime_int;
wire [data_unit_width - 1:0] mask_vector_sec,   mask_vector_sec_int;

reg  [data_unit_width - 1:0] mask_vector_common_int;

reg  [port_a_data_width - 1:0] mask_vector_a, mask_vector_a_int;
reg  [port_b_data_width - 1:0] mask_vector_b, mask_vector_b_int;

// memory initialization
integer i,j,k,l;
integer addr_range_init;
reg [data_width - 1:0] init_mem_word;
reg [(port_a_last_address - port_a_first_address + 1)*port_a_data_width - 1:0] mem_init;

// port active for read/write
wire  active_a_in, active_b_in;
wire active_a_core,active_a_core_in,active_b_core,active_b_core_in;
wire  active_write_a,active_write_b,active_write_clear_a,active_write_clear_b;

reg  mode_is_rom,mode_is_sp,mode_is_bdp; // ram mode
reg  ram_type;                               // ram type eg. MRAM


initial
begin
`ifdef QUARTUS_MEMORY_PLI
     $memory_connect(mem);
`endif
   ram_type = 0;



    mode_is_rom = (operation_mode == "rom");
    mode_is_sp  = (operation_mode == "single_port");
    mode_is_bdp = (operation_mode == "bidir_dual_port");

    out_a_is_reg = (port_a_data_out_clock == "none") ? 1'b0 : 1'b1;
    out_b_is_reg = (port_b_data_out_clock == "none") ? 1'b0 : 1'b1;

    // powerup output latches to 0
        dataout_a = 'b0;
        if (mode_is_dp || mode_is_bdp) dataout_b = 'b0;
     if ((power_up_uninitialized == "false") && ~ram_type)
         for (i = 0; i < num_rows; i = i + 1) mem[i] = 'b0;
    if ((init_file_layout == "port_a") || (init_file_layout == "port_b"))
    begin
        mem_init = { mem_init9, mem_init8, mem_init7, mem_init6, mem_init5,
            mem_init4, mem_init3, mem_init2, mem_init1, mem_init0
        };
        addr_range_init  = (primary_port_is_a) ?
                        port_a_last_address - port_a_first_address + 1 :
                        port_b_last_address - port_b_first_address + 1 ;
        for (j = 0; j < addr_range_init; j = j + 1)
        begin
            for (k = 0; k < data_width; k = k + 1)
                init_mem_word[k] = mem_init[j*data_width + k];
            mem[j] = init_mem_word;
        end
    end
    dual_write = 'b0;
end

assign clk_a_in      = clk0_int;
assign clk_a_wena = (port_a_write_enable_clock == "none") ? 1'b0 : clk_a_in;
assign clk_a_rena = (port_a_read_enable_clock  == "none") ? 1'b0 : clk_a_in;
// The byte enable registers are not preset to '1'.
assign clk_a_byteena = clk_a_in;
assign clk_a_out_secmux = (port_a_data_out_clock == "none")    ? 1'b0 : (
                       (port_a_data_out_clock == "clock0")  ? clk0_int : clk1_int);
assign clk_a_out = clk_a_out_secmux & nerror_int;

assign clk_b_in      = (port_b_address_clock == "clock0") ? clk0_int : clk1_int;
// The byte enable registers are not preset to '1'.
assign clk_b_byteena = (port_b_byte_enable_clock == "clock0") ? clk0_int : clk1_int;
assign clk_b_wena = (port_b_write_enable_clock == "none")   ? 1'b0 : (
                    (port_b_write_enable_clock == "clock0") ? clk0_int : clk1_int);
assign clk_b_rena = (port_b_read_enable_clock  == "none")   ? 1'b0 : (
                    (port_b_read_enable_clock  == "clock0") ? clk0_int : clk1_int);

assign clk_b_out_secmux = (port_b_data_out_clock == "none")      ? 1'b0 : (
                       (port_b_data_out_clock == "clock0")    ? clk0_int : clk1_int);
assign clk_b_out = clk_b_out_secmux & nerror_int;

assign addr_a_clr_in = (port_a_address_clear == "none")   ? 1'b0 : clr0_int;
assign addr_b_clr_in = (port_b_address_clear == "none")   ? 1'b0 : (
                       (port_b_address_clear == "clear0") ? clr0_int : clr1_int);

assign datain_a_clr_in = 1'b0;
assign dataout_a_clr    = (port_a_data_out_clear == "none")   ? 1'b0 : (
                           (port_a_data_out_clear == "clear0") ? clr0_int : clr1_int);

assign datain_b_clr_in = 1'b0;
assign dataout_b_clr    = (port_b_data_out_clear == "none")   ? 1'b0 : (
                           (port_b_data_out_clear == "clear0") ? clr0_int : clr1_int);

assign byteena_a_clr_in = 1'b0;
assign byteena_b_clr_in = 1'b0;

assign we_a_clr_in = 1'b0;
assign re_a_clr_in = 1'b0;

assign we_b_clr_in = 1'b0;
assign re_b_clr_in = 1'b0;

assign active_a_in = (clk0_input_clock_enable == "none") ? 1'b1 : (
                     (clk0_input_clock_enable == "ena0") ? ena0_int : ena2_int
                     );

assign active_a_core_in = (clk0_core_clock_enable == "none") ? 1'b1 : (
                          (clk0_core_clock_enable == "ena0") ? ena0_int : ena2_int
                          );

assign active_b_in = (port_b_address_clock == "clock0")  ? (
                           (clk0_input_clock_enable == "none") ? 1'b1 : ((clk0_input_clock_enable == "ena0") ? ena0_int : ena2_int)
                     ) : (
                           (clk1_input_clock_enable == "none") ? 1'b1 : ((clk1_input_clock_enable == "ena1") ? ena1_int : ena3_int)
                     );

assign active_b_core_in = (port_b_address_clock == "clock0")  ?  (
                              (clk0_core_clock_enable == "none") ? 1'b1 : ((clk0_core_clock_enable == "ena0") ? ena0_int : ena2_int)
                              ) : (
                                  (clk1_core_clock_enable == "none") ? 1'b1 : ((clk1_core_clock_enable == "ena1") ? ena1_int : ena3_int)
                              );


assign active_write_a = (byteena_a_reg !== 'b0);


assign active_write_b = (byteena_b_reg !== 'b0);

// Store core clock enable value for delayed write
// port A core active
common_28nm_ram_register active_core_port_a (
       .d(active_a_core_in),
       .clk(clk_a_in),
       .aclr(1'b0),
       .devclrn(1'b1),
       .devpor(1'b1),
       .stall(1'b0),
       .ena(1'b1),
       .q(active_a_core),.aclrout()
);
defparam active_core_port_a.width = 1;

// port B core active
common_28nm_ram_register active_core_port_b (
       .d(active_b_core_in),
       .clk(clk_b_in),
       .aclr(1'b0),
       .devclrn(1'b1),
       .devpor(1'b1),
       .stall(1'b0),
       .ena(1'b1),
       .q(active_b_core),.aclrout()
);
defparam active_core_port_b.width = 1;


// ------- A input registers -------
// write enable
common_28nm_ram_register we_a_register (
        .d(mode_is_rom ? 1'b0 : portawe_int),
        .clk(clk_a_wena),
        .aclr(we_a_clr_in),
        .devclrn(devclrn),
        .devpor(devpor),
        .stall(1'b0),
 .ena(active_a_core_in),
        .q(we_a_reg),
        .aclrout(we_a_clr)
        );
defparam we_a_register.width = 1;

// read enable
common_28nm_ram_register re_a_register (
        .d(portare_int),
        .clk(clk_a_rena),
        .aclr(re_a_clr_in),
        .devclrn(devclrn),
        .devpor(devpor),
        .stall(1'b0),
         .ena(active_a_core_in),
        .q(re_a_reg),
        .aclrout(re_a_clr)
        );

// address
common_28nm_ram_register addr_a_register (
        .d(portaaddr_int),
        .clk(clk_a_in),
        .aclr(addr_a_clr_in),
        .devclrn(devclrn),.devpor(devpor),
        .stall(portaaddrstall_int),
        .ena(active_a_in),
        .q(addr_a_reg),
        .aclrout(addr_a_clr)
        );
defparam addr_a_register.width = port_a_address_width;

// data
common_28nm_ram_register datain_a_register (
        .d(portadatain_int),
        .clk(clk_a_in),
        .aclr(datain_a_clr_in),
        .devclrn(devclrn),
        .devpor(devpor),
        .stall(1'b0),
        .ena(active_a_in),
        .q(datain_a_reg),
        .aclrout(datain_a_clr)
        );
defparam datain_a_register.width = port_a_data_width;

// byte enable
common_28nm_ram_register byteena_a_register (
        .d(portabyteenamasks_int),
        .clk(clk_a_byteena),
        .aclr(byteena_a_clr_in),
        .stall(1'b0),
        .devclrn(devclrn),
        .devpor(devpor),
        .ena(active_a_in),
        .q(byteena_a_reg),
        .aclrout(byteena_a_clr)
        );
defparam byteena_a_register.width = port_a_byte_enable_mask_width;

// ------- B input registers -------

// write enable

common_28nm_ram_register we_b_register (
        .d(portbwe_int),
        .clk(clk_b_wena),
        .aclr(we_b_clr_in),
        .stall(1'b0),
        .devclrn(devclrn),
        .devpor(devpor),
         .ena(active_b_core_in),
        .q(we_b_reg),
        .aclrout(we_b_clr)
        );
defparam we_b_register.width = 1;
defparam we_b_register.preset = 1'b0;

// read enable

common_28nm_ram_register re_b_register (
        .d(portbre_int),
        .clk(clk_b_rena),
        .aclr(re_b_clr_in),
        .stall(1'b0),
        .devclrn(devclrn),
        .devpor(devpor),
         .ena(active_b_core_in),
        .q(re_b_reg),
        .aclrout(re_b_clr)
        );
defparam re_b_register.width = 1;
defparam re_b_register.preset = 1'b0;



// address
common_28nm_ram_register addr_b_register (
        .d(portbaddr_int),
        .clk(clk_b_in),
        .aclr(addr_b_clr_in),
        .devclrn(devclrn),
        .devpor(devpor),
        .stall(portbaddrstall_int),
        .ena(active_b_in),
        .q(addr_b_reg),
        .aclrout(addr_b_clr)
        );
defparam addr_b_register.width = port_b_address_width;

// data
common_28nm_ram_register datain_b_register (
        .d(portbdatain_int),
        .clk(clk_b_in),
        .aclr(datain_b_clr_in),
        .devclrn(devclrn),
        .devpor(devpor),
        .stall(1'b0),
        .ena(active_b_in),
        .q(datain_b_reg),
        .aclrout(datain_b_clr)
        );
defparam datain_b_register.width = port_b_data_width;

// byte enable
common_28nm_ram_register byteena_b_register (
        .d(portbbyteenamasks_int),
        .clk(clk_b_byteena),
        .aclr(byteena_b_clr_in),
        .stall(1'b0),
        .devclrn(devclrn),
        .devpor(devpor),
        .ena(active_b_in),
        .q(byteena_b_reg),
        .aclrout(byteena_b_clr)
        );
defparam byteena_b_register.width  = port_b_byte_enable_mask_width;

assign datain_prime_reg = (primary_port_is_a) ? datain_a_reg : datain_b_reg;
assign addr_prime_reg   = (primary_port_is_a) ? addr_a_reg   : addr_b_reg;

assign datain_sec_reg   = (primary_port_is_a) ? datain_b_reg : datain_a_reg;
assign addr_sec_reg     = (primary_port_is_a) ? addr_b_reg   : addr_a_reg;

assign mask_vector_prime     = (primary_port_is_a) ? mask_vector_a     : mask_vector_b;
assign mask_vector_prime_int = (primary_port_is_a) ? mask_vector_a_int :  mask_vector_b_int;

assign mask_vector_sec       = (primary_port_is_a) ? mask_vector_b     : mask_vector_a;
assign mask_vector_sec_int   = (primary_port_is_a) ? mask_vector_b_int : mask_vector_a_int;

// Hardware Write Modes
// Write pulse generation
common_28nm_ram_pulse_generator wpgen_a (
       .clk(clk_a_in),
       .ena(active_a_core & active_write_a & we_a_reg),
        .pulse(write_pulse_a),
        .cycle(write_cycle_a)
        );
defparam wpgen_a.delay_pulse = delay_write_pulse_a;

common_28nm_ram_pulse_generator wpgen_b (
       .clk(clk_b_in),
       .ena(active_b_core & active_write_b & mode_is_bdp & we_b_reg),
        .pulse(write_pulse_b),
        .cycle(write_cycle_b)
        );
defparam wpgen_b.delay_pulse = delay_write_pulse_b;

// Read pulse generation
common_28nm_ram_pulse_generator rpgen_a (
        .clk(clk_a_in),
        .ena(active_a_core & re_a_reg & ~we_a_reg & (~dataout_a_clr || out_a_is_reg)),
        .pulse(read_pulse_a),
       .cycle(clk_a_core)
        );

common_28nm_ram_pulse_generator rpgen_b (
        .clk(clk_b_in),
        .ena((mode_is_dp | mode_is_bdp) & active_b_core & re_b_reg & ~we_b_reg & (~dataout_b_clr || out_b_is_reg)),
        .pulse(read_pulse_b),
       .cycle(clk_b_core)
        );

// Read during write pulse generation
common_28nm_ram_pulse_generator rwpgen_a (
    .clk(clk_a_in),
     .ena(active_a_core & re_a_reg & we_a_reg & read_before_write_a & (~dataout_a_clr || out_a_is_reg)),
    .pulse(rw_pulse_a),.cycle()
);

common_28nm_ram_pulse_generator rwpgen_b (
    .clk(clk_b_in),
     .ena(active_b_core & mode_is_bdp & re_b_reg & we_b_reg & read_before_write_b & (~dataout_b_clr || out_b_is_reg)),
    .pulse(rw_pulse_b),.cycle()
);

assign write_pulse_prime = (primary_port_is_a) ? write_pulse_a : write_pulse_b;
assign read_pulse_prime  = (primary_port_is_a) ? read_pulse_a : read_pulse_b;
assign read_pulse_prime_feedthru = (primary_port_is_a) ? read_pulse_a_feedthru : read_pulse_b_feedthru;
assign rw_pulse_prime = (primary_port_is_a) ? rw_pulse_a : rw_pulse_b;

assign write_pulse_sec = (primary_port_is_a) ? write_pulse_b : write_pulse_a;
assign read_pulse_sec  = (primary_port_is_a) ? read_pulse_b : read_pulse_a;
assign read_pulse_sec_feedthru = (primary_port_is_a) ? read_pulse_b_feedthru : read_pulse_a_feedthru;
assign rw_pulse_sec   = (primary_port_is_a) ? rw_pulse_b : rw_pulse_a;

// Create internal masks for byte enable processing
always @(byteena_a_reg)
begin
    for (i = 0; i < port_a_data_width; i = i + 1)
    begin
        mask_vector_a[i]     = (byteena_a_reg[i/byte_size_a] === 1'b1) ? 1'b0 : 1'bx;
        mask_vector_a_int[i] = (byteena_a_reg[i/byte_size_a] === 1'b0) ? 1'b0 : 1'bx;
    end
end

always @(byteena_b_reg)
begin
    for (l = 0; l < port_b_data_width; l = l + 1)
    begin
        mask_vector_b[l]     = (byteena_b_reg[l/byte_size_b] === 1'b1) ? 1'b0 : 1'bx;
        mask_vector_b_int[l] = (byteena_b_reg[l/byte_size_b] === 1'b0) ? 1'b0 : 1'bx;
    end
end

// Latch Clear port A - only if the output is unregistered
always @(posedge dataout_a_clr)
begin
    if (primary_port_is_a) 
    begin 
    	if (port_a_data_out_clock == "none") 
	begin 
		read_data_latch <= 'b0;
		dataout_a <= 'b0; 
	end
    end
    else                   
    begin 
    	if (port_a_data_out_clock == "none") 
	begin 
		read_unit_data_latch <= 'b0;
		dataout_a <= 'b0; 
	end
    end
end


// Latch Clear port B - only if the output is unregistered
always @(posedge dataout_b_clr)
begin
    if (primary_port_is_b) 
    begin 
    	if (port_b_data_out_clock == "none") 
	begin 
		read_data_latch <= 'b0; 
		dataout_b <= 'b0; 
	end
    end
    else                   
    begin 
    	if (port_b_data_out_clock == "none") 
	begin 
		read_unit_data_latch <= 'b0;
		dataout_b <= 'b0; 
	end
    end
end

always @(posedge write_pulse_prime or posedge write_pulse_sec or
         posedge read_pulse_prime or posedge read_pulse_sec
         or posedge rw_pulse_prime or posedge rw_pulse_sec
        )
begin

    // Read before Write stage 1 : read data from memory
    if (rw_pulse_prime && (rw_pulse_prime !== rw_pulse_prime_last_value))
    begin
       read_data_latch = mem[addr_prime_reg];
       rw_pulse_prime_last_value = rw_pulse_prime;
    end
    if (rw_pulse_sec && (rw_pulse_sec !== rw_pulse_sec_last_value))
    begin
       row_sec = addr_sec_reg / num_cols; col_sec = (addr_sec_reg % num_cols) * data_unit_width;
       mem_unit_data = mem[row_sec];
       for (j = col_sec; j <= col_sec + data_unit_width - 1; j = j + 1)
           read_unit_data_latch[j - col_sec] = mem_unit_data[j];
       rw_pulse_sec_last_value = rw_pulse_sec;
    end

    // Write stage 1 : write X to memory
    if (write_pulse_prime)
    begin
        old_mem_data = mem[addr_prime_reg];
        mem_data = mem[addr_prime_reg] ^ mask_vector_prime_int;
        mem[addr_prime_reg] = mem_data;
	if ((row_sec == addr_prime_reg) && (read_pulse_sec))
	begin
	    mem_unit_data = (mixed_port_feed_through_mode == "dont_care") ? {data_width{1'bx}} : old_mem_data;
	    for (j = col_sec; j <= col_sec + data_unit_width - 1; j = j + 1)
                read_unit_data_latch[j - col_sec] = mem_unit_data[j];
	end
    end
    if (write_pulse_sec)
    begin
        row_sec = addr_sec_reg / num_cols; col_sec = (addr_sec_reg % num_cols) * data_unit_width;
        mem_unit_data = mem[row_sec];
        for (j = col_sec; j <= col_sec + data_unit_width - 1; j = j + 1)
            mem_unit_data[j] = mem_unit_data[j] ^ mask_vector_sec_int[j - col_sec];
        mem[row_sec] = mem_unit_data;
    end

    if ((addr_prime_reg == row_sec) && write_pulse_prime && write_pulse_sec) dual_write = 2'b11;

    // Read stage 1 : read data from memory

    if (read_pulse_prime && read_pulse_prime !== read_pulse_prime_last_value)
    begin
       read_data_latch = mem[addr_prime_reg];
       read_pulse_prime_last_value = read_pulse_prime;
    end

    if (read_pulse_sec && read_pulse_sec !== read_pulse_sec_last_value)
    begin
        row_sec = addr_sec_reg / num_cols; col_sec = (addr_sec_reg % num_cols) * data_unit_width;
        if ((row_sec == addr_prime_reg) && (write_pulse_prime))
	    mem_unit_data = (mixed_port_feed_through_mode == "dont_care") ? {data_width{1'bx}} : old_mem_data;
        else
            mem_unit_data = mem[row_sec];
        for (j = col_sec; j <= col_sec + data_unit_width - 1; j = j + 1)
            read_unit_data_latch[j - col_sec] = mem_unit_data[j];
        read_pulse_sec_last_value = read_pulse_sec;
    end
end

// Simultaneous write to same/overlapping location by both ports
always @(dual_write)
begin
    if (dual_write == 2'b11)
    begin
           for (i = 0; i < data_unit_width; i = i + 1)
               mask_vector_common_int[i] = mask_vector_prime_int[col_sec + i] &
                                           mask_vector_sec_int[i];
    end
    else if (dual_write == 2'b01) mem_unit_data = mem[row_sec];
    else if (dual_write == 'b0)
    begin
       mem_data = mem[addr_prime_reg];
       for (i = 0; i < data_unit_width; i = i + 1)
               mem_data[col_sec + i] = mem_data[col_sec + i] ^ mask_vector_common_int[i];
       mem[addr_prime_reg] = mem_data;
    end
end

// Write stage 2 : Write actual data to memory
always @(negedge write_pulse_prime)
begin
    if (clear_asserted_during_write[`PRIME] !== 1'b1)
    begin
        for (i = 0; i < data_width; i = i + 1)
            if (mask_vector_prime[i] == 1'b0)
                mem_data[i] = datain_prime_reg[i];
        mem[addr_prime_reg] = mem_data;
    end
    dual_write[`PRIME] = 1'b0;
end

always @(negedge write_pulse_sec)
begin
    if (clear_asserted_during_write[`SEC] !== 1'b1)
    begin
        for (i = 0; i < data_unit_width; i = i + 1)
            if (mask_vector_sec[i] == 1'b0)
                mem_unit_data[col_sec + i] = datain_sec_reg[i];
        mem[row_sec] = mem_unit_data;
    end
    dual_write[`SEC] = 1'b0;
end

always @(negedge read_pulse_prime) read_pulse_prime_last_value = 1'b0;
always @(negedge read_pulse_sec)   read_pulse_sec_last_value = 1'b0;
always @(negedge rw_pulse_prime)   rw_pulse_prime_last_value = 1'b0;
always @(negedge rw_pulse_sec)     rw_pulse_sec_last_value = 1'b0;


// Read stage 2 : Send data to output
always @(negedge read_pulse_prime)
begin
    if (primary_port_is_a)
        dataout_a = read_data_latch;
    else
        dataout_b = read_data_latch;
end

always @(negedge read_pulse_sec)
begin
    if (primary_port_is_b)
        dataout_a = read_unit_data_latch;
    else
        dataout_b = read_unit_data_latch;
end

// Read during Write stage 2 : Send data to output

always @(negedge rw_pulse_prime)
begin
   if (primary_port_is_a)
   begin
       // BE mask write
       if (be_mask_write_a)
       begin
           for (i = 0; i < data_width; i = i + 1)
               if (mask_vector_prime[i] === 1'bx) // disabled byte
                   dataout_a[i] = read_data_latch[i];
       end
       else
           dataout_a = read_data_latch;
   end
   else
   begin
       // BE mask write
       if (be_mask_write_b)
       begin
           for (i = 0; i < data_width; i = i + 1)
               if (mask_vector_prime[i] === 1'bx) // disabled byte
                   dataout_b[i] = read_data_latch[i];
       end
       else
           dataout_b = read_data_latch;
   end
end

always @(negedge rw_pulse_sec)
begin
    if (primary_port_is_b)
    begin
        // BE mask write
        if (be_mask_write_a)
        begin
            for (i = 0; i < data_unit_width; i = i + 1)
                if (mask_vector_sec[i] === 1'bx) // disabled byte
                    dataout_a[i] = read_unit_data_latch[i];
        end
        else
            dataout_a = read_unit_data_latch;
    end
    else
    begin
        // BE mask write
        if (be_mask_write_b)
        begin
            for (i = 0; i < data_unit_width; i = i + 1)
                if (mask_vector_sec[i] === 1'bx) // disabled byte
                    dataout_b[i] = read_unit_data_latch[i];
        end
        else
            dataout_b = read_unit_data_latch;
    end
end

// Same port feed through
common_28nm_ram_pulse_generator ftpgen_a (
        .clk(clk_a_in),
           .ena(active_a_core & ~mode_is_dp & ~old_data_write_a & we_a_reg & re_a_reg & (~dataout_a_clr || out_a_is_reg)),
        .pulse(read_pulse_a_feedthru),.cycle()
        );

common_28nm_ram_pulse_generator ftpgen_b (
        .clk(clk_b_in),
           .ena(active_b_core & mode_is_bdp & ~old_data_write_b & we_b_reg & re_b_reg & (~dataout_b_clr || out_b_is_reg)),
        .pulse(read_pulse_b_feedthru),.cycle()
        );

always @(negedge read_pulse_prime_feedthru)
begin
    if (primary_port_is_a)
    begin
       if (be_mask_write_a)
       begin
          for (i = 0; i < data_width; i = i + 1)
              if (mask_vector_prime[i] == 1'b0) // enabled byte
                  dataout_a[i] = datain_prime_reg[i];
       end
       else
          dataout_a = datain_prime_reg ^ mask_vector_prime;
    end
    else
    begin
       if (be_mask_write_b)
       begin
          for (i = 0; i < data_width; i = i + 1)
              if (mask_vector_prime[i] == 1'b0) // enabled byte
                  dataout_b[i] = datain_prime_reg[i];
       end
       else
          dataout_b = datain_prime_reg ^ mask_vector_prime;
    end
end

always @(negedge read_pulse_sec_feedthru)
begin
    if (primary_port_is_b)
    begin
       if (be_mask_write_a)
       begin
          for (i = 0; i < data_unit_width; i = i + 1)
              if (mask_vector_sec[i] == 1'b0) // enabled byte
                  dataout_a[i] = datain_sec_reg[i];
       end
       else
          dataout_a = datain_sec_reg ^ mask_vector_sec;
    end
    else
    begin
       if (be_mask_write_b)
       begin
          for (i = 0; i < data_unit_width; i = i + 1)
              if (mask_vector_sec[i] == 1'b0) // enabled byte
                  dataout_b[i] = datain_sec_reg[i];
       end
       else
          dataout_b = datain_sec_reg ^ mask_vector_sec;
    end
end

// Input register clears

always @(posedge addr_a_clr or posedge datain_a_clr or posedge we_a_clr)
    clear_asserted_during_write_a = write_pulse_a;

assign active_write_clear_a = active_write_a & write_cycle_a;

always @(posedge addr_a_clr)
begin
    if (active_write_clear_a & we_a_reg)
        mem_invalidate = 1'b1;
 else if (active_a_core & re_a_reg)
    begin
        if (primary_port_is_a)
        begin
            read_data_latch = 'bx;
        end
        else
        begin
            read_unit_data_latch = 'bx;
        end
        dataout_a = 'bx;
    end
end

always @(posedge datain_a_clr or posedge we_a_clr)
begin
    if (active_write_clear_a & we_a_reg)
    begin
        if (primary_port_is_a)
            mem[addr_prime_reg] = 'bx;
        else
        begin
            mem_unit_data = mem[row_sec];
            for (j = col_sec; j <= col_sec + data_unit_width - 1; j = j + 1)
                mem_unit_data[j] = 1'bx;
            mem[row_sec] = mem_unit_data;
        end
        if (primary_port_is_a)
        begin
            read_data_latch = 'bx;
        end
        else
        begin
            read_unit_data_latch = 'bx;
        end
    end
end

assign active_write_clear_b = active_write_b & write_cycle_b;

always @(posedge addr_b_clr or posedge datain_b_clr or
        posedge we_b_clr)
    clear_asserted_during_write_b = write_pulse_b;

always @(posedge addr_b_clr)
begin
   if (mode_is_bdp & active_write_clear_b & we_b_reg)
        mem_invalidate = 1'b1;
  else if ((mode_is_dp | mode_is_bdp) & active_b_core & re_b_reg)
    begin
        if (primary_port_is_b)
        begin
            read_data_latch = 'bx;
        end
        else
        begin
            read_unit_data_latch = 'bx;
        end
        dataout_b = 'bx;
    end
end

always @(posedge datain_b_clr or posedge we_b_clr)
begin
   if (mode_is_bdp & active_write_clear_b & we_b_reg)

    begin
        if (primary_port_is_b)
            mem[addr_prime_reg] = 'bx;
        else
        begin
            mem_unit_data = mem[row_sec];
            for (j = col_sec; j <= col_sec + data_unit_width - 1; j = j + 1)
                 mem_unit_data[j] = 'bx;
            mem[row_sec] = mem_unit_data;
        end
        if (primary_port_is_b)
        begin
            read_data_latch = 'bx;
        end
        else
        begin
            read_unit_data_latch = 'bx;
        end
    end
end

assign clear_asserted_during_write[primary_port_is_a] = clear_asserted_during_write_a;
assign clear_asserted_during_write[primary_port_is_b] = clear_asserted_during_write_b;

always @(posedge mem_invalidate)
begin
    for (i = 0; i < num_rows; i = i + 1) mem[i] = 'bx;
    mem_invalidate = 1'b0;
end

 // ------- Aclr mux registers (Latch Clear) --------
 // port A
 common_28nm_ram_register aclr__a__mux_register (
        .d(dataout_a_clr),
        .clk(clk_a_core),
        .aclr(1'b0),
        .devclrn(devclrn),
        .devpor(devpor),
        .stall(1'b0),
        .ena(1'b1),
        .q(dataout_a_clr_reg),.aclrout()
        );
 // port B
 common_28nm_ram_register aclr__b__mux_register (
        .d(dataout_b_clr),
        .clk(clk_b_core),
        .aclr(1'b0),
        .devclrn(devclrn),
        .devpor(devpor),
        .stall(1'b0),
        .ena(1'b1),
        .q(dataout_b_clr_reg),.aclrout()
        );


// ------- ECC Pipeline registers --------

common_28nm_ram_register ecc_pipeline_b_register (
        .d( dataout_b ),
        .clk(clk_b_out),
 	.aclr(dataout_b_clr_reg),
        .devclrn(devclrn),.devpor(devpor),
        .stall(1'b0),
        .ena(clkena_b_out),
        .q(ecc_pipeline_b_reg),.aclrout()
        );
defparam ecc_pipeline_b_register.width = port_b_data_width;

assign dataout_b_signal = (enable_ecc == "true" && ecc_pipeline_stage_enabled == "true") ? ecc_pipeline_b_reg : dataout_b ;

// ------- Output registers --------

assign clkena_a_out = (port_a_data_out_clock == "clock0") ?
                       ((clk0_output_clock_enable == "none") ? 1'b1 : ena0_int) :
                       ((clk1_output_clock_enable == "none") ? 1'b1 : ena1_int) ;

common_28nm_ram_register dataout_a_register (
        .d(dataout_a),
        .clk(clk_a_out),
 	.aclr(dataout_a_clr),
        .devclrn(devclrn),
        .devpor(devpor),
        .stall(1'b0),
        .ena(clkena_a_out),
        .q(dataout_a_reg),.aclrout()
        );
defparam dataout_a_register.width = port_a_data_width;

 reg [port_a_data_width - 1:0] portadataout_clr;
 reg [port_b_data_width - 1:0] portbdataout_clr;
 initial
 begin
     portadataout_clr = 'b0;
     portbdataout_clr = 'b0;
 end

 assign portadataout =  (out_a_is_reg) ? dataout_a_reg : (
                            (dataout_a_clr || dataout_a_clr_reg) ? portadataout_clr : dataout_a
                       );

assign clkena_b_out = (port_b_data_out_clock == "clock0") ?
                       ((clk0_output_clock_enable == "none") ? 1'b1 : ena0_int) :
                       ((clk1_output_clock_enable == "none") ? 1'b1 : ena1_int) ;

common_28nm_ram_register dataout_b_register (
        .d( dataout_b_signal ),
        .clk(clk_b_out),
 	.aclr(dataout_b_clr),
        .devclrn(devclrn),.devpor(devpor),
        .stall(1'b0),
        .ena(clkena_b_out),
        .q(dataout_b_reg),.aclrout()
        );
defparam dataout_b_register.width = port_b_data_width;

 assign portbdataout = (out_b_is_reg) ? dataout_b_reg : (
                          (dataout_b_clr || dataout_b_clr_reg) ? portbdataout_clr : dataout_b_signal
                      );

assign eccstatus = {width_eccstatus{1'b0}};

endmodule // common_28nm_ram_block

//--------------------------------------------------------------------------
// Module Name     : generic_m20k
// Description     : Wrapper around the common 28nm RAM block
//--------------------------------------------------------------------------

`timescale 1 ps/1 ps

module generic_m20k
    (
     portadatain,
     portaaddr,
     portawe,
     portare,
     portbdatain,
     portbaddr,
     portbwe,
     portbre,
     clk0, clk1,
     ena0, ena1,
     ena2, ena3,
     clr0, clr1,
     nerror,
     portabyteenamasks,
     portbbyteenamasks,
     portaaddrstall,
     portbaddrstall,
     devclrn,
     devpor,
     eccstatus,
     portadataout,
     portbdataout
      ,dftout
     );
// -------- GLOBAL PARAMETERS ---------
parameter operation_mode = "single_port";
parameter mixed_port_feed_through_mode = "dont_care";
parameter ram_block_type = "auto";
parameter logical_ram_name = "ram_name";

parameter init_file = "init_file.hex";
parameter init_file_layout = "none";

parameter ecc_pipeline_stage_enabled = "false";
parameter enable_ecc = "false";
parameter width_eccstatus = 2;
parameter data_interleave_width_in_bits = 1;
parameter data_interleave_offset_in_bits = 1;
parameter port_a_logical_ram_depth = 0;
parameter port_a_logical_ram_width = 0;
parameter port_a_first_address = 0;
parameter port_a_last_address = 0;
parameter port_a_first_bit_number = 0;

parameter port_a_data_out_clear = "none";

parameter port_a_data_out_clock = "none";

parameter port_a_data_width = 1;
parameter port_a_address_width = 1;
parameter port_a_byte_enable_mask_width = 1;

parameter port_b_logical_ram_depth = 0;
parameter port_b_logical_ram_width = 0;
parameter port_b_first_address = 0;
parameter port_b_last_address = 0;
parameter port_b_first_bit_number = 0;

parameter port_b_address_clear = "none";
parameter port_b_data_out_clear = "none";

parameter port_b_data_in_clock = "clock1";
parameter port_b_address_clock = "clock1";
parameter port_b_write_enable_clock = "clock1";
parameter port_b_read_enable_clock  = "clock1";
parameter port_b_byte_enable_clock = "clock1";
parameter port_b_data_out_clock = "none";

parameter port_b_data_width = 1;
parameter port_b_address_width = 1;
parameter port_b_byte_enable_mask_width = 1;

parameter port_a_read_during_write_mode = "new_data_no_nbe_read";
parameter port_b_read_during_write_mode = "new_data_no_nbe_read";
parameter power_up_uninitialized = "false";
parameter lpm_type = "stratixv_ram_block";
parameter lpm_hint = "true";
parameter connectivity_checking = "off";

parameter mem_init0 = 2048'b0;
parameter mem_init1 = 2048'b0;
parameter mem_init2 = 2048'b0;
parameter mem_init3 = 2048'b0;
parameter mem_init4 = 2048'b0;
parameter mem_init5 = 2048'b0;
parameter mem_init6 = 2048'b0;
parameter mem_init7 = 2048'b0;
parameter mem_init8 = 2048'b0;
parameter mem_init9 = 2048'b0;

parameter port_a_byte_size = 0;
parameter port_b_byte_size = 0;

parameter clk0_input_clock_enable  = "none"; // ena0,ena2,none
parameter clk0_core_clock_enable   = "none"; // ena0,ena2,none
parameter clk0_output_clock_enable = "none"; // ena0,none
parameter clk1_input_clock_enable  = "none"; // ena1,ena3,none
parameter clk1_core_clock_enable   = "none"; // ena1,ena3,none
parameter clk1_output_clock_enable = "none"; // ena1,none

parameter bist_ena = "false"; //false, true 

// SIMULATION_ONLY_PARAMETERS_BEGIN

parameter port_a_address_clear = "none";

parameter port_a_data_in_clock = "clock0";
parameter port_a_address_clock = "clock0";
parameter port_a_write_enable_clock = "clock0";
parameter port_a_byte_enable_clock = "clock0";
parameter port_a_read_enable_clock = "clock0";

// SIMULATION_ONLY_PARAMETERS_END

// -------- PORT DECLARATIONS ---------
input portawe;
input portare;
input [port_a_data_width - 1:0] portadatain;
input [port_a_address_width - 1:0] portaaddr;
input [port_a_byte_enable_mask_width - 1:0] portabyteenamasks;

input portbwe, portbre;
input [port_b_data_width - 1:0] portbdatain;
input [port_b_address_width - 1:0] portbaddr;
input [port_b_byte_enable_mask_width - 1:0] portbbyteenamasks;

input clr0,clr1;
input clk0,clk1;
input ena0,ena1;
input ena2,ena3;
input nerror;

input devclrn,devpor;
input portaaddrstall;
input portbaddrstall;
output [port_a_data_width - 1:0] portadataout;
output [port_b_data_width - 1:0] portbdataout;
output [width_eccstatus - 1:0] eccstatus;
output [8:0] dftout;


// -------- RAM BLOCK INSTANTIATION ---
common_28nm_ram_block ram_core0
(
	.portawe(portawe),
	.portare(portare),
	.portadatain(portadatain),
	.portaaddr(portaaddr),
	.portabyteenamasks(portabyteenamasks),
	.portbwe(portbwe),
	.portbre(portbre),
	.portbdatain(portbdatain),
	.portbaddr(portbaddr),
	.portbbyteenamasks(portbbyteenamasks),
	.clr0(clr0),
	.clr1(clr1),
	.clk0(clk0),
	.clk1(clk1),
	.ena0(ena0),
	.ena1(ena1),
	.ena2(ena2),
	.ena3(ena3),
	.nerror(nerror),
	.devclrn(devclrn),
	.devpor(devpor),
	.portaaddrstall(portaaddrstall),
	.portbaddrstall(portbaddrstall),
	.portadataout(portadataout),
	.portbdataout(portbdataout),
	.eccstatus(eccstatus),
	.dftout(dftout)
);
defparam ram_core0.operation_mode = operation_mode;
defparam ram_core0.mixed_port_feed_through_mode = mixed_port_feed_through_mode;
defparam ram_core0.ram_block_type = ram_block_type;
defparam ram_core0.logical_ram_name = logical_ram_name;
defparam ram_core0.init_file = init_file;
defparam ram_core0.init_file_layout = init_file_layout;
defparam ram_core0.ecc_pipeline_stage_enabled = ecc_pipeline_stage_enabled;
defparam ram_core0.enable_ecc = enable_ecc;
defparam ram_core0.width_eccstatus = width_eccstatus;
defparam ram_core0.data_interleave_width_in_bits = data_interleave_width_in_bits;
defparam ram_core0.data_interleave_offset_in_bits = data_interleave_offset_in_bits;
defparam ram_core0.port_a_logical_ram_depth = port_a_logical_ram_depth;
defparam ram_core0.port_a_logical_ram_width = port_a_logical_ram_width;
defparam ram_core0.port_a_first_address = port_a_first_address;
defparam ram_core0.port_a_last_address = port_a_last_address;
defparam ram_core0.port_a_first_bit_number = port_a_first_bit_number;
defparam ram_core0.port_a_data_out_clear = port_a_data_out_clear;
defparam ram_core0.port_a_data_out_clock = port_a_data_out_clock;
defparam ram_core0.port_a_data_width = port_a_data_width;
defparam ram_core0.port_a_address_width = port_a_address_width;
defparam ram_core0.port_a_byte_enable_mask_width = port_a_byte_enable_mask_width;
defparam ram_core0.port_b_logical_ram_depth = port_b_logical_ram_depth;
defparam ram_core0.port_b_logical_ram_width = port_b_logical_ram_width;
defparam ram_core0.port_b_first_address = port_b_first_address;
defparam ram_core0.port_b_last_address = port_b_last_address;
defparam ram_core0.port_b_first_bit_number = port_b_first_bit_number;
defparam ram_core0.port_b_address_clear = port_b_address_clear;
defparam ram_core0.port_b_data_out_clear = port_b_data_out_clear;
defparam ram_core0.port_b_data_in_clock = port_b_data_in_clock;
defparam ram_core0.port_b_address_clock = port_b_address_clock;
defparam ram_core0.port_b_write_enable_clock = port_b_write_enable_clock;
defparam ram_core0.port_b_read_enable_clock = port_b_read_enable_clock;
defparam ram_core0.port_b_byte_enable_clock = port_b_byte_enable_clock;
defparam ram_core0.port_b_data_out_clock = port_b_data_out_clock;
defparam ram_core0.port_b_data_width = port_b_data_width;
defparam ram_core0.port_b_address_width = port_b_address_width;
defparam ram_core0.port_b_byte_enable_mask_width = port_b_byte_enable_mask_width;
defparam ram_core0.port_a_read_during_write_mode = port_a_read_during_write_mode;
defparam ram_core0.port_b_read_during_write_mode = port_b_read_during_write_mode;
defparam ram_core0.power_up_uninitialized = power_up_uninitialized;
defparam ram_core0.lpm_type = lpm_type;
defparam ram_core0.lpm_hint = lpm_hint;
defparam ram_core0.connectivity_checking = connectivity_checking;
defparam ram_core0.mem_init0 = mem_init0;
defparam ram_core0.mem_init1 = mem_init1;
defparam ram_core0.mem_init2 = mem_init2;
defparam ram_core0.mem_init3 = mem_init3;
defparam ram_core0.mem_init4 = mem_init4;
defparam ram_core0.mem_init5 = mem_init5;
defparam ram_core0.mem_init6 = mem_init6;
defparam ram_core0.mem_init7 = mem_init7;
defparam ram_core0.mem_init8 = mem_init8;
defparam ram_core0.mem_init9 = mem_init9;
defparam ram_core0.port_a_byte_size = port_a_byte_size;
defparam ram_core0.port_b_byte_size = port_b_byte_size;
defparam ram_core0.clk0_input_clock_enable = clk0_input_clock_enable;
defparam ram_core0.clk0_core_clock_enable = clk0_core_clock_enable ;
defparam ram_core0.clk0_output_clock_enable = clk0_output_clock_enable;
defparam ram_core0.clk1_input_clock_enable = clk1_input_clock_enable;
defparam ram_core0.clk1_core_clock_enable = clk1_core_clock_enable;
defparam ram_core0.clk1_output_clock_enable = clk1_output_clock_enable;
defparam ram_core0.bist_ena = bist_ena;
defparam ram_core0.port_a_address_clear = port_a_address_clear;
defparam ram_core0.port_a_data_in_clock = port_a_data_in_clock;
defparam ram_core0.port_a_address_clock = port_a_address_clock;
defparam ram_core0.port_a_write_enable_clock = port_a_write_enable_clock;
defparam ram_core0.port_a_byte_enable_clock = port_a_byte_enable_clock;
defparam ram_core0.port_a_read_enable_clock = port_a_read_enable_clock;

endmodule // m20k_ram_block

// Activate again the LEDA rule that requires uppercase letters for all
// parameter names
// leda rule_G_521_3_B on




//--------------------------------------------------------------------------
// Module Name     : common_28nm_mlab_cell_pulse_generator
// Description     : Generate pulse to initiate memory read/write operations
//--------------------------------------------------------------------------
`timescale 1 ps/1 ps

module common_28nm_mlab_cell_pulse_generator (
                                    clk,
                                    ena,     
                                    pulse,
                                    cycle    
                                   );
input  clk;   // clock
input  ena;   // pulse enable
output pulse; // pulse
output cycle; // delayed clock

reg  state;
reg  clk_prev;
wire clk_ipd;

specify
    specparam t_decode = 0,t_access = 0;
    (posedge clk => (pulse +: state)) = (t_decode,t_access);
endspecify

buf #(1) (clk_ipd,clk);
wire  pulse_opd;

buf buf_pulse  (pulse,pulse_opd);

initial clk_prev = 1'bx;

always @(clk_ipd or posedge pulse) 
begin
    if      (pulse) state <= 1'b0;
    else if (ena && clk_ipd === 1'b1 && clk_prev === 1'b0)   state <= 1'b1;
    clk_prev = clk_ipd;
end

assign cycle = clk_ipd;
assign pulse_opd = state; 

endmodule


//--------------------------------------------------------------------------
// Module Name     : common_28nm_mlab_cell
// Description     : Main RAM module
//--------------------------------------------------------------------------

`timescale 1 ps/1 ps

module common_28nm_mlab_cell
    (
     portadatain,
     portaaddr, 
     portabyteenamasks, 
     portbaddr,
     clk0, clk1,
     ena0, ena1,
     ena2,
     clr,
     devclrn,
     devpor,
     portbdataout
     );
// -------- GLOBAL PARAMETERS ---------

parameter logical_ram_name = "lutram";

parameter logical_ram_depth = 0;
parameter logical_ram_width = 0;
parameter first_address = 0;
parameter last_address = 0;
parameter first_bit_number = 0;

parameter mixed_port_feed_through_mode = "new";
parameter init_file = "NONE";

parameter data_width = 20;
parameter address_width = 6;
parameter byte_enable_mask_width = 1;
parameter byte_size = 1;
parameter port_b_data_out_clock = "none";
parameter port_b_data_out_clear = "none";

parameter lpm_type = "common_28nm_mlab_cell";
parameter lpm_hint = "true";

parameter mem_init0 = 640'b0; // 64x10 OR 32x20

// -------- PORT DECLARATIONS ---------
input [data_width - 1:0] portadatain;
input [address_width - 1:0] portaaddr;
input [byte_enable_mask_width - 1:0] portabyteenamasks;
input [address_width - 1:0] portbaddr;

input clk0;
input clk1;

input ena0;
input ena1;
input ena2;

input clr;

input devclrn;
input devpor;

output [data_width - 1:0] portbdataout;

tri1 devclrn;
tri1 devpor;

tri0 [data_width - 1:0] portadatain;
tri0 [address_width - 1:0] portaaddr;
tri1 [byte_enable_mask_width - 1:0] portabyteenamasks;
tri0 clr;
tri0 clk0,clk1;
tri1 ena0,ena1;
tri1 ena2;

// LOCAL_PARAMETERS_BEGIN

parameter port_byte_size = data_width/byte_enable_mask_width;
parameter num_rows = 1 << address_width;
parameter num_cols = 1;

// LOCAL_PARAMETERS_END

reg ena0_reg;

specify
      (portbaddr *> portbdataout) = (0,0);
endspecify

// -------- INTERNAL signals ---------
// clock / clock enable
wire clk_a_in;
wire clk_b_out;

// asynch clear
wire dataout_b_clr_in;

wire dataout_b_clr;
wire addr_a_clr;
wire datain_a_clr;
wire byteena_a_clr;


// port A registers
wire [address_width - 1:0] addr_a_reg;
wire [data_width - 1:0] datain_a_reg;
wire [byte_enable_mask_width - 1:0] byteena_a_reg;

// port B registers
wire [data_width - 1:0] dataout_b;
wire [data_width - 1:0] dataout_b_reg;
wire [data_width - 1:0] portbdataout_tmp;

// placeholders for read/written data
reg  [data_width - 1:0] mem_data;

// pulses for A/B ports (no read pulse)
wire write_pulse;
wire write_cycle;

// memory core
reg  [data_width - 1:0] mem [num_rows - 1:0];

// byte enable
reg  [data_width - 1:0] mask_vector, mask_vector_int;

// memory initialization
integer i,j,k;
integer addr_range_init;
reg [data_width - 1:0] init_mem_word;
reg [(last_address - first_address + 1)*data_width - 1:0] mem_init;

// port active for read/write
wire  active_a,active_a_in;
wire  active_write_a;

initial
begin
    ena0_reg = 1'b0;

    // powerup output to 0
    for (i = 0; i < num_rows; i = i + 1) 
		mem[i] = {data_width{1'b0}};
    mem_init = mem_init0;
    addr_range_init  = last_address - first_address + 1;
    for (j = 0; j < addr_range_init; j = j + 1)
    begin
        for (k = 0; k < data_width; k = k + 1)
            init_mem_word[k] = mem_init[j*data_width + k];
        mem[j] = init_mem_word;
    end
end

assign clk_a_in = clk0;
assign clk_b_out = (port_b_data_out_clock == "clock1") ? clk1 : 1'b0;

always @(posedge clk_a_in) ena0_reg <= ena0;

assign dataout_b_clr_in = (port_b_data_out_clear == "clear") ? clr : 1'b0;

// Port A registers
// address register
common_28nm_ram_register addr_a_register(
        .d(portaaddr),
        .clk(clk_a_in),
		.aclr(1'b0),
        .devclrn(devclrn),
        .devpor(devpor),
        .ena(ena2),
		.stall(1'b0),
        .q(addr_a_reg),
		.aclrout(addr_a_clr)
        );
defparam addr_a_register.width = address_width;

// data register
common_28nm_ram_register datain_a_register(
        .d(portadatain),
        .clk(clk_a_in),
		.aclr(1'b0),
        .devclrn(devclrn),
        .devpor(devpor),
        .ena(ena2),
		.stall(1'b0),
        .q(datain_a_reg),
		.aclrout(datain_a_clr)
        );
defparam datain_a_register.width = data_width;

// byte enable register
common_28nm_ram_register byteena_a_register(
        .d(portabyteenamasks),
        .clk(clk_a_in),
		.aclr(1'b0),
        .devclrn(devclrn),
        .devpor(devpor),
        .ena(ena2),
		.stall(1'b0),
        .q(byteena_a_reg),
		.aclrout(byteena_a_clr)
        );
defparam byteena_a_register.width = byte_enable_mask_width;

// data register
common_28nm_ram_register data_b_register(
        .d(dataout_b),
        .clk(clk_b_out),
		.aclr(dataout_b_clr_in),
        .devclrn(devclrn),
        .devpor(devpor),
        .ena(ena1),
		.stall(1'b0),
        .q(dataout_b_reg),
		.aclrout(dataout_b_clr)
        );
defparam data_b_register.width = data_width;

// Write pulse generation
common_28nm_mlab_cell_pulse_generator wpgen_a (
        .clk(clk_a_in),
        .ena(ena0_reg),
        .pulse(write_pulse),
	    .cycle(write_cycle)
        );

// Read pulse generation
// -- none --

// Create internal masks for byte enable processing
always @(byteena_a_reg)
begin
	for (i = 0; i < data_width; i = i + 1)
	begin
		if (port_byte_size == 0)
		begin
			$display("Error: Parameter data_width is smaller than parameter byte_enable_mask_width.");
			$display("Time: %0t  Instance: %m", $time);
			$finish;
		end
		if (byteena_a_reg[i/port_byte_size] === 1'b1)
			mask_vector[i] = 1'b0;
		else
			mask_vector[i] = 1'bx;

		if (byteena_a_reg[i/port_byte_size] === 1'b0)
			mask_vector_int[i] = 1'b0;
		else
			mask_vector_int[i] = 1'bx;
	end
end
                        
always @(posedge write_pulse) 
begin
    // Write stage 1 : write X to memory
    if (write_pulse)
    begin
        mem_data = mem[addr_a_reg] ^ mask_vector_int;
        mem[addr_a_reg] = mem_data;
    end
end

// Write stage 2 : Write actual data to memory
always @(negedge write_pulse)
begin
    for (i = 0; i < data_width; i = i + 1)
        if (mask_vector[i] == 1'b0)
            mem_data[i] = datain_a_reg[i];
    mem[addr_a_reg] = mem_data;
end

// Read stage : asynchronous continuous read

assign dataout_b = mem[portbaddr];
assign portbdataout_tmp = (port_b_data_out_clock == "clock1") ? dataout_b_reg : dataout_b;
assign portbdataout = portbdataout_tmp;

endmodule // common_28nm_mlab_cell

//--------------------------------------------------------------------------
// Module Name     : generic_mlab_cell
// Description     : Main RAM module
//--------------------------------------------------------------------------

`timescale 1 ps/1 ps

module generic_mlab_cell
    (
     portadatain,
     portaaddr, 
     portabyteenamasks, 
     portbaddr,
     clk0, clk1,
     ena0, ena1,
     ena2,
     clr,
     devclrn,
     devpor,
     portbdataout
     );
// -------- GLOBAL PARAMETERS ---------

parameter logical_ram_name = "lutram";

parameter logical_ram_depth = 0;
parameter logical_ram_width = 0;
parameter first_address = 0;
parameter last_address = 0;
parameter first_bit_number = 0;

parameter mixed_port_feed_through_mode = "new";
parameter init_file = "NONE";

parameter data_width = 20;
parameter address_width = 6;
parameter byte_enable_mask_width = 1;
parameter byte_size = 1;
parameter port_b_data_out_clock = "none";
parameter port_b_data_out_clear = "none";

parameter lpm_type = "common_28nm_mlab_cell";
parameter lpm_hint = "true";

parameter mem_init0 = 640'b0; // 64x10 OR 32x20

// -------- PORT DECLARATIONS ---------
input [data_width - 1:0] portadatain;
input [address_width - 1:0] portaaddr;
input [byte_enable_mask_width - 1:0] portabyteenamasks;
input [address_width - 1:0] portbaddr;

input clk0;
input clk1;

input ena0;
input ena1;
input ena2;

input clr;

input devclrn;
input devpor;

output [data_width - 1:0] portbdataout;

common_28nm_mlab_cell my_lutram0
(
	.portadatain(portadatain),
	.portaaddr(portaaddr),
	.portabyteenamasks(portabyteenamasks),
	.portbaddr(portbaddr),
	.clk0(clk0),
	.clk1(clk1),
	.ena0(ena0),
	.ena1(ena1),
	.ena2(ena2),
	.clr(clr),
	.devclrn(devclrn),
	.devpor(devpor),
	.portbdataout(portbdataout)
);
defparam my_lutram0.logical_ram_name = logical_ram_name;
defparam my_lutram0.logical_ram_depth = logical_ram_depth;
defparam my_lutram0.logical_ram_width = logical_ram_width;
defparam my_lutram0.first_address = first_address;
defparam my_lutram0.last_address = last_address;
defparam my_lutram0.first_bit_number = first_bit_number;
defparam my_lutram0.mixed_port_feed_through_mode = mixed_port_feed_through_mode;
defparam my_lutram0.init_file = init_file;
defparam my_lutram0.data_width = data_width;
defparam my_lutram0.address_width = address_width;
defparam my_lutram0.byte_enable_mask_width = byte_enable_mask_width;
defparam my_lutram0.byte_size = byte_size;
defparam my_lutram0.port_b_data_out_clock = port_b_data_out_clock;
defparam my_lutram0.port_b_data_out_clear = port_b_data_out_clear;
defparam my_lutram0.lpm_type = lpm_type;
defparam my_lutram0.lpm_hint = lpm_hint;
defparam my_lutram0.mem_init0 = mem_init0;

endmodule // generic_mlab_cell

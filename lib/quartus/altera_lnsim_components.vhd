-- Copyright (C) 1991-2010 Altera Corporation
-- This simulation model contains highly confidential and
-- proprietary information of Altera and is being provided
-- in accordance with and subject to the protections of the
-- applicable Altera Program License Subscription Agreement
-- which governs its use and disclosure. Your use of Altera
-- Corporation's design tools, logic functions and other
-- software and tools, and its AMPP partner logic functions,
-- and any output files any of the foregoing (including device
-- programming or simulation files), and any associated
-- documentation or information are expressly subject to the
-- terms and conditions of the Altera Program License Subscription
-- Agreement, Altera MegaCore Function License Agreement, or other
-- applicable license agreement, including, without limitation,
-- that your use is for the sole purpose of simulating designs for
-- use exclusively in logic devices manufactured by Altera and sold
-- by Altera or its authorized distributors. Please refer to the
-- applicable agreement for further details. Altera products and
-- services are protected under numerous U.S. and foreign patents,
-- maskwork rights, copyrights and other intellectual property laws.
-- Altera assumes no responsibility or liability arising out of the
-- application or use of this simulation model.

library ieee;
use ieee.std_logic_1164.all;

package altera_lnsim_components is

component altera_pll 
    generic(
        reference_clock_frequency: string  := "0 ps";
        pll_type        : string  := "General";
        number_of_clocks: integer := 1;
        operation_mode  : string  := "internal feedback";
        deserialization_factor: integer := 4;
        data_rate       : integer := 0;
        clock_switchover_mode: string  := "Auto";
        clock_switchover_delay: integer := 3;
        output_clock_frequency0: string  := "0 ps";
        phase_shift0    : string  := "0 ps";
        duty_cycle0     : integer := 50;
        output_clock_frequency1: string  := "0 ps";
        phase_shift1    : string  := "0 ps";
        duty_cycle1     : integer := 50;
        output_clock_frequency2: string  := "0 ps";
        phase_shift2    : string  := "0 ps";
        duty_cycle2     : integer := 50;
        output_clock_frequency3: string  := "0 ps";
        phase_shift3    : string  := "0 ps";
        duty_cycle3     : integer := 50;
        output_clock_frequency4: string  := "0 ps";
        phase_shift4    : string  := "0 ps";
        duty_cycle4     : integer := 50;
        output_clock_frequency5: string  := "0 ps";
        phase_shift5    : string  := "0 ps";
        duty_cycle5     : integer := 50;
        output_clock_frequency6: string  := "0 ps";
        phase_shift6    : string  := "0 ps";
        duty_cycle6     : integer := 50;
        output_clock_frequency7: string  := "0 ps";
        phase_shift7    : string  := "0 ps";
        duty_cycle7     : integer := 50;
        output_clock_frequency8: string  := "0 ps";
        phase_shift8    : string  := "0 ps";
        duty_cycle8     : integer := 50;
        output_clock_frequency9: string  := "0 ps";
        phase_shift9    : string  := "0 ps";
        duty_cycle9     : integer := 50;
        output_clock_frequency10: string  := "0 ps";
        phase_shift10   : string  := "0 ps";
        duty_cycle10    : integer := 50;
        output_clock_frequency11: string  := "0 ps";
        phase_shift11   : string  := "0 ps";
        duty_cycle11    : integer := 50;
        output_clock_frequency12: string  := "0 ps";
        phase_shift12   : string  := "0 ps";
        duty_cycle12    : integer := 50;
        output_clock_frequency13: string  := "0 ps";
        phase_shift13   : string  := "0 ps";
        duty_cycle13    : integer := 50;
        output_clock_frequency14: string  := "0 ps";
        phase_shift14   : string  := "0 ps";
        duty_cycle14    : integer := 50;
        output_clock_frequency15: string  := "0 ps";
        phase_shift15   : string  := "0 ps";
        duty_cycle15    : integer := 50;
        output_clock_frequency16: string  := "0 ps";
        phase_shift16   : string  := "0 ps";
        duty_cycle16    : integer := 50;
        output_clock_frequency17: string  := "0 ps";
        phase_shift17   : string  := "0 ps";
        duty_cycle17    : integer := 50
    );
    port(
        refclk          : in    std_logic;
        fbclk           : in    std_logic;
        rst             : in    std_logic;
        outclk          : out   std_logic_vector(number_of_clocks - 1 downto 0);
        fboutclk        : out   std_logic;
        locked          : out   std_logic;
        zdbfbclk        : inout std_logic
    );
end component;

component generic_pll 
    generic(
        lpm_type        : string  := "generic_pll";
        duty_cycle      : integer := 50;
        output_clock_frequency: string  := "0 ps";
        phase_shift     : string  := "0 ps";
        reference_clock_frequency: string  := "0 ps";
        sim_additional_refclk_cycles_to_lock: integer := 0
    );
    port(
        refclk          : in    std_logic;
        rst             : in    std_logic;
        fbclk           : in    std_logic;
        outclk          : out   std_logic;
        locked          : out   std_logic;
        fboutclk        : out   std_logic
    );
end component;

component generic_cdr 
    generic(
        reference_clock_frequency: string  := "0 ps";
        output_clock_frequency: string  := "0 ps"
    );
    port(
        extclk          : in    std_logic;
        ltd             : in    std_logic;
        ltr             : in    std_logic;
        ppmlock         : in    std_logic;
        refclk          : in    std_logic;
        rst             : in    std_logic;
        sd              : in    std_logic;
        rxp             : in    std_logic;
        clk90bdes       : out   std_logic;
        clk270bdes      : out   std_logic;
        clklow          : out   std_logic;
        deven           : out   std_logic;
        dodd            : out   std_logic;
        fref            : out   std_logic;
        pfdmodelock     : out   std_logic;
        rxplllock       : out   std_logic
    );
end component;

COMPONENT generic_m20k
    GENERIC (
        operation_mode                 :  STRING := "single_port";    
        mixed_port_feed_through_mode   :  STRING := "dont_care";    
        ram_block_type                 :  STRING := "auto";    
        logical_ram_name               :  STRING := "ram_name";    
        init_file                      :  STRING := "init_file.hex";    
        init_file_layout               :  STRING := "none";    
        ecc_pipeline_stage_enabled     :  STRING := "false";
        enable_ecc                     :  STRING := "false";
	width_eccstatus		       :  INTEGER := 2;   
        data_interleave_width_in_bits  :  INTEGER := 1;    
        data_interleave_offset_in_bits :  INTEGER := 1;    
        port_a_logical_ram_depth       :  INTEGER := 0;    
        port_a_logical_ram_width       :  INTEGER := 0;    
        port_a_first_address           :  INTEGER := 0;    
        port_a_last_address            :  INTEGER := 0;    
        port_a_first_bit_number        :  INTEGER := 0;    
        port_a_address_clear           :  STRING := "none";    
        port_a_data_out_clear          :  STRING := "none";    
        port_a_data_in_clock           :  STRING := "clock0";    
        port_a_address_clock           :  STRING := "clock0";    
        port_a_write_enable_clock      :  STRING := "clock0";    
        port_a_read_enable_clock     :  STRING := "clock0";           
        port_a_byte_enable_clock       :  STRING := "clock0";    
        port_a_data_out_clock          :  STRING := "none";    
        port_a_data_width              :  INTEGER := 1;    
        port_a_address_width           :  INTEGER := 1;    
        port_a_byte_enable_mask_width  :  INTEGER := 1;    
        port_b_logical_ram_depth       :  INTEGER := 0;    
        port_b_logical_ram_width       :  INTEGER := 0;    
        port_b_first_address           :  INTEGER := 0;    
        port_b_last_address            :  INTEGER := 0;    
        port_b_first_bit_number        :  INTEGER := 0;    
        port_b_address_clear           :  STRING := "none";    
        port_b_data_out_clear          :  STRING := "none";    
        port_b_data_in_clock           :  STRING := "clock1";    
        port_b_address_clock           :  STRING := "clock1";    
        port_b_write_enable_clock: STRING := "clock1";    
        port_b_read_enable_clock: STRING := "clock1";    
        port_b_byte_enable_clock       :  STRING := "clock1";    
        port_b_data_out_clock          :  STRING := "none";    
        port_b_data_width              :  INTEGER := 1;    
        port_b_address_width           :  INTEGER := 1;    
        port_b_byte_enable_mask_width  :  INTEGER := 1;    
        port_a_read_during_write_mode  :  STRING  := "new_data_no_nbe_read";
        port_b_read_during_write_mode  :  STRING  := "new_data_no_nbe_read";    
        power_up_uninitialized         :  STRING := "false";  
        port_b_byte_size : INTEGER := 0;
        port_a_byte_size : INTEGER := 0;  
        lpm_type                  : string := "stratixv_ram_block";
        lpm_hint                  : string := "true";
        clk0_input_clock_enable  : STRING := "none"; -- ena0,ena2,none
        clk0_core_clock_enable   : STRING := "none"; -- ena0,ena2,none
        clk0_output_clock_enable : STRING := "none"; -- ena0,none
        clk1_input_clock_enable  : STRING := "none"; -- ena1,ena3,none
        clk1_core_clock_enable   : STRING := "none"; -- ena1,ena3,none
        clk1_output_clock_enable : STRING := "none"; -- ena1,none
        mem_init0 : BIT_VECTOR := X"0";
        mem_init1 : BIT_VECTOR := X"0";
        mem_init2 : BIT_VECTOR := X"0";
        mem_init3 : BIT_VECTOR := X"0";
        mem_init4 : BIT_VECTOR := X"0";
        mem_init5 : BIT_VECTOR := X"0";
        mem_init6 : BIT_VECTOR := X"0";
        mem_init7 : BIT_VECTOR := X"0";
        mem_init8 : BIT_VECTOR := X"0";
        mem_init9 : BIT_VECTOR := X"0";
        connectivity_checking     : string := "off"
        );    
    PORT (
        portadatain             : IN STD_LOGIC_VECTOR(port_a_data_width - 1 DOWNTO 0)    := (OTHERS => '0');   
        portaaddr               : IN STD_LOGIC_VECTOR(port_a_address_width - 1 DOWNTO 0) := (OTHERS => '0');   
        portawe                 : IN STD_LOGIC := '0';   
        portare                 : IN STD_LOGIC := '1';   
        portbdatain             : IN STD_LOGIC_VECTOR(port_b_data_width - 1 DOWNTO 0)    := (OTHERS => '0');   
        portbaddr               : IN STD_LOGIC_VECTOR(port_b_address_width - 1 DOWNTO 0) := (OTHERS => '0');   
        portbwe                 : IN STD_LOGIC := '0';   
        portbre                 : IN STD_LOGIC := '1';   
        clk0                    : IN STD_LOGIC := '0';   
        clk1                    : IN STD_LOGIC := '0';   
        ena0                    : IN STD_LOGIC := '1';   
        ena1                    : IN STD_LOGIC := '1';   
        ena2                    : IN STD_LOGIC := '1';   
        ena3                    : IN STD_LOGIC := '1';   
        clr0                    : IN STD_LOGIC := '0';   
        clr1                    : IN STD_LOGIC := '0';   
        nerror                  : IN STD_LOGIC := '1';   
        portabyteenamasks       : IN STD_LOGIC_VECTOR(port_a_byte_enable_mask_width - 1 DOWNTO 0) := (OTHERS => '1');   
        portbbyteenamasks       : IN STD_LOGIC_VECTOR(port_b_byte_enable_mask_width - 1 DOWNTO 0) := (OTHERS => '1');   
        devclrn                 : IN STD_LOGIC := '1';   
        devpor                  : IN STD_LOGIC := '1';   
        portaaddrstall : IN STD_LOGIC := '0';
        portbaddrstall : IN STD_LOGIC := '0';
        eccstatus : OUT STD_LOGIC_VECTOR(width_eccstatus - 1 DOWNTO 0) := (OTHERS => '0');
        dftout : OUT STD_LOGIC_VECTOR(8 DOWNTO 0) := "000000000";
        portadataout            : OUT STD_LOGIC_VECTOR(port_a_data_width - 1 DOWNTO 0);   
        portbdataout            : OUT STD_LOGIC_VECTOR(port_b_data_width - 1 DOWNTO 0)
        );
END COMPONENT;

COMPONENT generic_mlab_cell
   GENERIC (
      logical_ram_name              : STRING := "lutram";
      logical_ram_depth             : INTEGER := 0;
      logical_ram_width             : INTEGER := 0;
      first_address                 : INTEGER := 0;
      last_address                  : INTEGER := 0;
      first_bit_number              : INTEGER := 0;
      init_file                     : STRING := "NONE";
      data_width                    : INTEGER := 20;
      address_width                 : INTEGER := 6;
      byte_enable_mask_width        : INTEGER := 1;
      byte_size                     : INTEGER := 1;
      port_b_data_out_clock         : STRING := "none";
      port_b_data_out_clear         : STRING := "none";
      lpm_type                      : STRING := "stratixv_lutram";
      lpm_hint                      : STRING := "true";
      mem_init0                     : BIT_VECTOR := X"0";
      mixed_port_feed_through_mode  : STRING := "new"
   );
   PORT (
      portadatain                   : IN STD_LOGIC_VECTOR(data_width - 1 DOWNTO 0) := (others => '0');
      portaaddr                     : IN STD_LOGIC_VECTOR(address_width - 1 DOWNTO 0) := (others => '0');
      portabyteenamasks             : IN STD_LOGIC_VECTOR(byte_enable_mask_width - 1 DOWNTO 0) := (others => '1');
      portbaddr                     : IN STD_LOGIC_VECTOR(address_width - 1 DOWNTO 0) := (others => '0');
      clk0                          : IN STD_LOGIC := '0';
      clk1                          : IN STD_LOGIC := '0';
      ena0                          : IN STD_LOGIC := '1';
      ena1                          : IN STD_LOGIC := '1';
      ena2                          : IN STD_LOGIC := '1';
      clr                           : IN STD_LOGIC := '0';
      devclrn                       : IN STD_LOGIC := '1';
      devpor                        : IN STD_LOGIC := '1';
      portbdataout                  : OUT STD_LOGIC_VECTOR(data_width - 1 DOWNTO 0)
   );
END COMPONENT;

component altera_functions 
end component;

end altera_lnsim_components;

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include <random>
#include <string.h>

enum System {
	ColumnCount = 8,
	SegmentCount = 3
};

class IGenerator;
class IOutputDevice;

class Channel {
	static const int Size = 3;

	unsigned char data[Size];

public:
	void Set(unsigned int value) {
		for (int index = 0; index < Size; ++index) {
			data[index] = (value >> (index << 3)) & 0xF;
		}
	}

	unsigned int Get(void) {
		unsigned int result = 0;

		for (int index = 0; index < Size; ++index) {
			result |= data[index] << (index << 3);
		}

		return result;
	}
};

struct Gassiplex {
	static const int Count = 16;

	std::vector<Channel> channelList;

	Gassiplex(void) { channelList.resize(Count); }
};

struct Dilogic {
	static const int Count = 3;

	std::vector<Gassiplex> gassiplexList;

	Dilogic(void) { gassiplexList.resize(Count); }
};



struct Column {
	static const int Count = 10;

	std::vector<Dilogic> dilogicList;

	Column(void) { dilogicList.resize(Count); }

	bool Populate(IGenerator *generator); 
	bool Persist(IOutputDevice *device, int segmentId, int columnId);
};

class IGenerator {
	unsigned int m_sequenceId;

public:
	virtual ~IGenerator() { }
	IGenerator(void) : m_sequenceId(0) { }

	virtual unsigned int Generate(unsigned int sequenceId) = 0;
	
	bool Populate(Column &column) { //8
		for (auto & dilogic : column.dilogicList) { //10
			for(auto & gassiplex : dilogic.gassiplexList) { //3
				for (auto & channel : gassiplex.channelList) { //16
					channel.Set(Generate(m_sequenceId++));
				}
			}
		}
		return true;
	}
};

class IOutputDevice {
public:
	virtual ~IOutputDevice() { }	
	virtual bool Write(Column &column, int segmentIndex, int columnIndex) = 0;
};

bool Column::Populate(IGenerator *generator) {
	return generator->Populate(*this);
}

bool Column::Persist(IOutputDevice *device, int segmentId, int columnId) {
	return device->Write(*this, segmentId, columnId);
}

class MersenneGenerator : 
	public IGenerator 
{
	std::mt19937 m_generator;

public:
	MersenneGenerator(int seed) 
		: IGenerator()
		, m_generator(seed)
	{ }

	unsigned int Generate(unsigned int sequenceId) override {
		return (unsigned int) m_generator();
	}
};

class ConstGenerator : 
	public IGenerator
{
	unsigned int m_value;

public:
	ConstGenerator(int value)
		: m_value(value) 
	{ }

	unsigned int Generate(unsigned int sequenceId) override {
		return m_value;
	}
};


class RampGenerator :
	public IGenerator 
{
public:
	RampGenerator(void) { }

	unsigned int Generate(unsigned int sequenceId) override {
		return sequenceId;
	}
};


class ConsoleDevice 
	: public IOutputDevice 
{
public:
	bool Write(Column &column, int segmentIndex, int columnIndex) {
		for (auto & dilogic : column.dilogicList) {
			for(auto & gassiplex : dilogic.gassiplexList) {
				for (auto & channel : gassiplex.channelList) {
					std::cout << std::setfill('0') << std::setw(3) << std::hex << ((unsigned int) channel.Get()) << ' ';
				}

				std::cout << '\t';
			}

			std::cout << '\n';
		}

		return true;
	}
};

class FileDevice 
	: public IOutputDevice 
{
	std::string m_filenamePrefix;
	std::string m_dirName;

	std::string MakeFilename(int segmentIndex, int columnIndex) {
		std::stringstream result;
		result << m_dirName << "/" << m_filenamePrefix << segmentIndex << columnIndex << ".txt";
		return result.str();
	}

public:
	FileDevice(std::string filenamePrefix,std::string dirName = "./")
		: m_filenamePrefix(filenamePrefix),
		  m_dirName(dirName)
	{ }

	bool Write(Column &column, int segmentIndex, int columnIndex) 
	{
		std::string filename = MakeFilename(segmentIndex, columnIndex);

		std::ofstream outputFile;
		outputFile.open(filename);

		if (outputFile.is_open()) {
			for (auto & dilogic : column.dilogicList) {
				for(auto & gassiplex : dilogic.gassiplexList) {
					for (auto & channel : gassiplex.channelList) {
						outputFile << std::setfill('0') << std::setw(3) << std::hex << ((unsigned int) channel.Get()) << ' ';
					}

					outputFile << '\t';
				}

				outputFile << '\n';
			}
		} else {
			std::cerr << "Unable to open [" << filename << "] for writing!" << std::endl;
			return false;
		}

		outputFile.close();

		return true;
	}
};


int main(int argc, char **argv) 
{
	IOutputDevice *outputDevice = nullptr;
	IGenerator *generator = nullptr;
	
	std::string newPath = "";
	std::string customFileName = "";

	std::cout << "Options : ";

	for (int argIndex = 1; argIndex < argc; ++argIndex) {
		if (!strcmp("-path", argv[argIndex]) && !outputDevice) {
			std::cout << "[PATH]";
			
			if (argIndex + 1 < argc) {
				++argIndex;
				newPath = argv[argIndex];
			}
		}
		else if (!strcmp("-debug", argv[argIndex]) && !outputDevice) {
			std::cout << "[DEBUG]";
			outputDevice = new ConsoleDevice();
		} 
		else if (!strcmp("-file", argv[argIndex]) && !outputDevice) {
			std::cout << "[FILE]";
			if (argIndex + 1 < argc) {
				++argIndex;
				customFileName = argv[argIndex];
			}
		} 
		else if (!strcmp("-ramp", argv[argIndex]) && !generator) {
			std::cout << "[RAMP]";
			generator = new RampGenerator();
			break;
		} 
		else if (!strcmp("-const", argv[argIndex]) && !generator) {
			std::cout << "[CONST]";
			if (argIndex + 1 < argc) {
				++argIndex;
				int value = atoi(argv[argIndex]);
				generator = new ConstGenerator(value);
			}
		} 
		else if (!strcmp("-random", argv[argIndex]) && !generator) {
			std::cout << "[RANDOM]";
			generator = new MersenneGenerator(time(0));
			break;
		} 
		else {
			std::cerr << "Unrecongnised command: " << argv[argIndex] << std::endl;
			std::cerr << "-- Available options: -debug <-ramp|-const value|-random>" << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	std::cout << std::endl;

	// If not using debug device, then create a file device
	if (outputDevice == nullptr && customFileName == "" && newPath == "")
		outputDevice = new FileDevice("ADC_");
	else 
		outputDevice = new FileDevice(customFileName != "" ? customFileName: "ADC_", newPath != ""? newPath : "./");



	if (generator == nullptr)
		generator = new MersenneGenerator(time(0));

	Column column;

	for (int segmentIndex = 0; segmentIndex < System::SegmentCount; ++segmentIndex) 
	{
		for (int columnIndex = 0; columnIndex < System::ColumnCount; ++columnIndex) 
		{
			column.Populate(generator);
			column.Persist(outputDevice, segmentIndex, columnIndex);
		}
	}

	delete outputDevice;
	delete generator;

	exit(EXIT_SUCCESS);
}

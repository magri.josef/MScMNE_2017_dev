#!/usr/bin/env python


import pygtk
pygtk.require('2.0')

import os
import gobject
import pango
import gtk
import math
import time
import select
import sys
import struct


from gtk import gdk
try:
	import cairo
except ImportError:
	pass

if gtk.pygtk_version < (2,3,93):
	print "PyGtk 2.3.93 or later required"
	raise SystemExit

TEXT = 'cairo'
BORDER_WIDTH = 5

# Define the Map Class
class Map:

	# const
	SEGN = 3
	COLN = 8
	DILN = 10
	CHAN = 48
	MAXADCVAL = 4096.0
	PW = 5
	PH = 5
	DPW = 7
	DPH = 7
	BASEX = 5
	BASEY = 5
	
	# data structures
	map = []
	entry = []
	dimension = 0
	updateMode = 0
	
	def __init__(self):
		# const
		self.SEGN = 3
		self.COLN = 8
		self.DILN = 10
		self.CHAN = 48
		self.dimension = (self.SEGN * self.COLN * self.DILN * self.CHAN )
		for index in range(0, self.dimension ):
			self.map.append(0.0)
			self.entry.append(0)

	def clear(self):
		for index in range(0, self.dimension):
			self.map[index] = 0.0
			self.entry[index] = 0

	def getDim(self):
		return self.dimension
	def getGeometry(self):
		return (SEGN, COLN, DILN, CHAN)
		
	def getMapDimPixel(self):
		return( self.SEGN * (self.COLN * (8 * self.DPW + 1) + 3), self.DILN * (5 * self.DPH + 1) )
	def setMapBasePoint(self, baseX, baseY ):
		self.BASEX = baseX
		self.BASEY = baseY
		return
	def getMapBasePoint(self):
		return(self.BASEX, self.BASEY)
		
	def setUpMode( self, mode ):
		if mode >= 0 and mode <=3:
			self.updateMode = mode
		return
	def getUpMode( self ):
		return self.updateMode
		
	def update(self, val, index):
		if val >= self.MAXADCVAL or index <0 or index>=self.dimension: return
		vp = val / self.MAXADCVAL
		if self.updateMode == 0:
			self.map[index] = vp
			self.entry[index] = 1
		elif self.updateMode == 1:
			self.map[index] = self.map[index] + vp
			self.entry[index] = self.entry[index] + 1
		elif self.updateMode == 2:
			self.map[index] = ( self.map[index] * self.entry[index] + vp ) / (self.entry[index]+1)
			self.entry[index] = self.entry[index] + 1
		elif self.updateMode == 3:
			self.map[index] = self.map[index] - vp
			self.entry[index] = 1
		return

	def draw(self, cr):
		index = 0
		for seg in range(0, self.SEGN):
			for col in range(0, self.COLN):
				for dil in range(0, self.DILN):
					for cha in range(0, self.CHAN):
						self.drawPad(cr,seg,col,dil,cha, index)
						index += 1
		cr.stroke()
		return

	def drawPad(self,cr,seg,col,dil,cha, index):
		if self.entry[index] == 0:
			r = g = b = 0.5
		else:
			(r,g,b) = self.val2col(self.map[index]) #here calculate the color by val
		cr.set_source_rgba(r,g,b,1.0)

		# calculate the x,y position
		x = (seg * (self.COLN * (8 * self.DPW + 1) + 5)) + (col * ( 8 * self.DPW + 1) + 1) + (cha % 8 * self.DPW)
		y = (dil * (6 * self.DPH + 1) + 1) + ( cha // 8 ) * self.DPH
		# draw the pad
		cr.rectangle(self.BASEX + x, self.BASEY + y, self.PW, self.PH)
		cr.fill()
		cr.stroke()
		return

	def drawScale(self,cr,posx,posy,width,height):
		cr.set_source_rgba(0.4,0.4,0.4,1.0)
		cr.rectangle(posx,posy,width,height)
		cr.fill()
		cr.stroke()
		x = posx+1
		dy = (height-2.0) / 101.0
		dx = width - 2
		y = posy+height-1
		for i in range(0,101):
			(r,g,b) = self.val2col(float(i)/100.0)
			cr.set_source_rgba(r,g,b,1.0)
			cr.rectangle(x, y - dy*(i+1), dx, dy+0.49)
			cr.fill()
			cr.stroke()
		return

	def val2col(self, val):
		slo = 4
		if val < 0.125:
			return( 0, 0, (val + 0.125) * slo )
		if val < 0.375:
			return( 0, (val - 0.125) * slo, 1.0 )
		if val < 0.625:
			return( (val - 0.375) * slo, 1.0, 1.0 - (val - 0.375) * slo)
		if val < 0.875:
			return( 1, 1.0 - (val - 0.625) * slo, 0)
		else:
			return( 1.0 - (val - 0.875) * slo, 0, 0)
		
	# # --- Read the DA Produced file one fo segment 
	# def readPedestalSegmentDAFile(self, fileName, segMent, type):
	# 	self.readPedThrSegmFile(fileName, segMent, type, 1)
	# 	return

	# # --- Read the ReadBack Produced file one fo segment 
	# def readPedestalSegmentFile(self, fileName, segMent, type):
	# 	self.readPedThrSegmFile(fileName, segMent, type, 0)
	# 	return	
		
	# # --- Read the for segment segment 
	# def readPedThrSegmFile(self, fileName, seg, type, isDA):
	# 	if not os.path.isfile(fileName):
	# 		print "The file %s does not exist at this time..." % fileName
	# 		return
	# 	baseIndex = self.calculateBaseIndex( segment = seg )
	# 	# open the file
	# 	fh = open( fileName, "r" )
	# 	# Read and ignore header line
	# 	if not isDA:
	# 		header = fh.readline()
	# 	column = 1
	# 	dilogic = 1
	# 	worldcount = 0
	# 	valInt = 0
	# 	willBeHeader = 1
	# 	index = baseIndex
	# 	for line in fh:
	# 		line = line.strip()
	# 		valu = line.split()
	# 		for valStr in valu:
	# 			valInt = self.extractValFromHex( valStr , type)
	# 			if worldcount == 64:
	# 				dilogic += 1
	# 				worldcount = 0
	# 			if dilogic == 11:
	# 				willBeHeader = 1
	# 			if willBeHeader == 1:
	# 				dilogic = 1
	# 				worldcount = 0
	# 				column += 1
	# 				willBeHeader = 0
	# 			else:
	# 				self.update( valInt , index )
	# 				index += 1
	# 				worldcount += 1
	# 	# close & exit
	# 	fh.close()
	# 	return
	
	# # --- read the pedestall/thr column files
	# def readPedestalColumnFile(self, fileName, seg, col, type):
	# 	if not os.path.isfile(fileName):
	# 		print "The file %s does not exist at this time..." % fileName
	# 		return	
	# 	baseIndex = self.calculateBaseIndex( segment = seg, column = col )
	# 	# open the file
	# 	fh = open( fileName, "r" )
	# 	# loop
	# 	index = baseIndex
	# 	worldcount = 0
	# 	for line in fh:
	# 		line = line.strip()
	# 		valInt = self.extractValFromHex( line , type)
	# 		self.update( valInt , index )
	# 		index += 1
	# 		worldcount += 1
	# 		if worldcount == (self.DILN * self.CHAN):
	# 			break
	# 	# close & exit
	# 	fh.close()
	# 	return

	# --- read Raw data files ...
	def readRawDataFile(self):
		ldcId = 0
		subEvent = 0
		#  Definition of header structures
		#   Base LDC Header
		str_baseEvH = '=iiiiiiiiiiiiiiiiii'
		str_baseEvHL = struct.calcsize(str_baseEvH)
		str_baseEvHU = struct.Struct(str_baseEvH).unpack_from
		#   Equipment Header
		str_equiH = '7i'
		str_equiHL = struct.calcsize(str_equiH)
		str_equiHU = struct.Struct(str_equiH).unpack_from
		#   CDH Header
		str_cdhH = '8i'
		str_cdhHL = struct.calcsize(str_cdhH)
		str_cdhHU = struct.Struct(str_cdhH).unpack_from
		#   HMPID header
		str_hmpidH = '5i'
		str_hmpidHL = struct.calcsize(str_hmpidH)
		str_hmpidHU = struct.Struct(str_hmpidH).unpack_from
		#   Binary word
		str_payW = 'i'
		str_payWL = struct.calcsize(str_payW)
		str_payWU = struct.Struct(str_payW).unpack_from
		# ----------------
		fh = open( "HMPIDout.bin", "r" )

		# if not os.path.isfile(fileName):
		# 	print "The file %s does not exist at this time..." % fileName
		# 	return
		# # open the file
		# fh = open( fileName, "r" )

		while True:
			# read the header
			baseHeader = fh.read(str_baseEvHL)
			if not baseHeader: break
			(EvS,Magic,EvHeS,EvV,EvT,EvRuNu,EvId,EvTrPat,evtp2,EvDePat,evdp2,EvTyAt1,evta2,evta3,EvLcdId,EvGdcId,EvTSsec,EvTSusec) = str_baseEvHU(baseHeader)
			
			# Verify the LDCId and the Event number
			if EvLcdId <> ldcId or EvId<>subEvent and subEvent<>-1:
				#print " Event not good, discharged !"
				nWordToRead = (EvS - str_baseEvHL) / 4
				#print " Words to read %d " % nWordToRead
				for i in range(0, nWordToRead):
					pw = fh.read(str_payWL)
				continue

#			print " EvS=%d Magic=%X EvHeS=%d EvV=%X EvT=%d EvRuNu=%d EvId=%X EvTrPat=%X.%X EvDePat=%X.%X EvTyAt=%X.%X.%X EvLcdId=%d EvGdcId=%d EvTSsec=%d EvTSusec=%d " % (EvS,Magic,EvHeS,EvV,EvT,EvRuNu,EvId,EvTrPat,evtp2,EvDePat,evdp2,EvTyAt1,evta2,evta3,EvLcdId,EvGdcId,EvTSsec,EvTSusec)

			if EvS != EvHeS:  # there is the payload
				# Read the Equipment Header
				equiH = fh.read(str_equiHL)
				if not equiH: break
				(EqSize,EqType,P2,Eq_TypeAtt1,Eq_TypeAtt2,Eq_TypeAtt3,Eq_BasicElSize) = str_equiHU(equiH)
				EqId_Det = (P2 & 0xF800000) >> 27
				EqId_DDL = (P2 & 0x07FFFFF)
#				print "Equipment --> Size=%d type=%d Det=%X DDL=%d Att1=%X Att2=%X Att3=%X Data Size=%d" % (EqSize,EqType,EqId_Det,EqId_DDL,Eq_TypeAtt1,Eq_TypeAtt2,Eq_TypeAtt3,Eq_BasicElSize)
					
				# Read the CDH
				cdhH = fh.read(str_cdhHL)
				if not cdhH: break
				(P0,P1,P2,P3,P4,P5,P6,P7) = str_cdhHU(cdhH)
				BlockLen = P0
				CDHVer = (P1 & 0xFF000000) >> 24
				L1TrigMess = (P1 & 0x003FC000) >> 14
				BunchCrossing = (P1 & 0x000003FF)
				OrbitNumber = (P2 & 0x00FFFFFF)
				BlockAttrib = (P3 & 0xFF000000) >> 24
				PartSubDect = (P3 & 0x00FFFFFF)
				StatusAndError = (P4 & 0x0FFFF000) >> 12
				MiniEventId = (P4 & 0x00000FFF)
				TriggerClassL = P5
				TriggerClassH = (P6 & 0x0003FFFF)
				RoiL = (P6 & 0xF0000000) >> 28
				RoiH =P7
#				print "CDH --> Len=%d ver=%X L1Mes=%X BunCros=%d Orbit=%d BlkAtt=%X SubDet=%d Stat=%X MiniEv=%d TrCl(L)=%X TrCl(H)=%X Roi(L)=%X Roi(H)=%X" % (BlockLen,CDHVer,L1TrigMess,BunchCrossing,OrbitNumber,BlockAttrib,PartSubDect,StatusAndError,MiniEventId,TriggerClassL,TriggerClassH,RoiL,RoiH)
		
				# Read the HMPID Header
				hmpidH = fh.read(str_hmpidHL)
				if not hmpidH: break
				(FirmwVer,StaAndErr,FEEReset,TTCReset,Dummy) = str_hmpidHU(hmpidH)
#				print "Hmpid H --> Firmware Ver=%d Status & Error =%X FEE Reset =%X TTC reset=%X" % (FirmwVer,StaAndErr,FEEReset,TTCReset)
		
				sampleNum = 0
				nWordToRead = (EvS - (EvHeS + str_equiHL + str_cdhHL + str_hmpidHL))
				#print " Words to read %d " % nWordToRead /4
		
				willBeRowMarker = 1
				willBeSegmentMarker = 0
				wordsPerRowCounter = 0
				columnCounter = 0
				index = -5
				# start the main loop
				for i in range(0, nWordToRead + 40):
					pw = fh.read(str_payWL)
					w1 = str_payWU(pw)
					wp = w1[0]  # from tuple to int
		
#					if willBeRowMarker:
#						rowSize = (wp & 0xffff0000) >> 16 # Number of words of row
#						marker = wp & 0x0000ffff
#						if( marker <> 0x36A8 and marker <> 0x32A8):
#							print ">>  Error in the Row Marker Word !!"
#						#print "Row Marker %X  row size = %d" % (marker, rowSize)
#						willBeRowMarker = 0
#						wordsPerRowCounter = 0
#						columnCounter += 1
#					if willBeSegmentMarker:
#						segSize = (wp & 0x000ffff0) >> 8 # Number of words of Segment
#						marker = (wp & 0xfff00000) >> 20
#						if( marker <> 0xAB0):
#							print ">>  Error in the Segment Marker Word !!"
#						Segment = wp & 0x0000000E
#						#print "Seg Marker %X Segment:%d size = %d" % (marker, Segment, segSize)
#						willBeRowMarker = 1
#						willBeSegmentMarker = 0
#					else:
					marker = wp & 0x0000ffff
					if ((marker == 0x36A8) or (marker == 0x32A8) ): continue
						#print "column found"
					elif ((wp & 0xf8000000) == 0):
							# PAD:0000.0ccc.ccdd.ddnn.nnnn.vvvv.vvvv.vvvv :: c=col,d=dilo,n=chan,v=value
							sampleNum += 1
							Col = (wp & 0x07c00000) >> 22
							Dilogic = (wp & 0x003C0000) >> 18
							Channel = (wp & 0x0003F000) >> 12
							Charge = (wp & 0x00000FFF)
							wordsPerRowCounter += 1
							
							# index = self.calculateIndex(column = Col-1, dilogic = Dilogic-1, channel = Channel )
							index += 1
							self.update( Charge , index )
							#print ">%d  %X" % (Charge, wp)
#						elif (wp & 0xf8000000) == 0x08000000:  # this is EoE marker
#							Col = (wp & 0x07c00000) >> 22
#							Dilogic = (wp & 0x003C0000) >> 18
#							Eoesize = (wp & 0x000000FF)
#							wordsPerRowCounter += 1
#							#print "EoE Marker %X Dilo:%d Row:%d size = %d" % (wp,dilogic, colonna, eoesize)
#					if wordsPerRowCounter > rowSize:
#						if columnCounter % 8 == 0:
#							willBeSegmentMarker = 1
#						else:
#							willBeRowMarker = 1

		print "Done !"	
		return

	# --- private methods ....
	def calculateBaseIndex( self, segment, column = 0):
		baseIndex = 0
		if segment > 0 and segment <= (self.SEGN):
			baseIndex = (segment-1) * (self.COLN * self.DILN * self.CHAN)
		if column > 0 and column <= (self.COLN):
			baseIndex = baseIndex + (column-1) * (self.DILN * self.CHAN)
		return( baseIndex ) 

	def calculateIndex( self, column, dilogic, channel):
		index = column * self.DILN * self.CHAN + dilogic * self.CHAN + channel
		return( index )

	def extractValFromHex(self, valStr , type):
		valInt = int(valStr,16)
		valInt = valInt & 0x3FFFF
		if type ==1:  # ped
			valInt = valInt & 0x000FF
		else:  # thr  
			valInt = valInt & 0x0FF00
			valInt = valInt >> 8
		valInt = valInt * 16
		return ( valInt )

		
# ----------------------------------------

# class Buffer:	
# 	buffer = []
# 	npar = 0

# 	def __init__(self):
# 		self.buffer = []
# 		self.npar = 0
		
# 	def clear( self ):
# 		self.buffer = []
# 		self.npar = 0

# 	def getCommand( self ):
# 		if self.npar < 1:
# 			return("")
# 		else:
# 			return( self.buffer[0] )
		
# 	def parse( self) :
# 		par1 = ""
# 		par2 = "0"
# 		par3 = "0"
# 		if len(self.buffer) < 2:
# 			pass
# 		else:
# 			par1 = self.buffer[1]
# 			if len(self.buffer) < 3:
# 				pass
# 			else:
# 				par2 = self.buffer[2]
# 				if len(self.buffer) < 4:
# 					pass
# 				else:  
# 					par3 = self.buffer[3]
# 		return( par1, par2, par3 )

# 	def build(self, line):
# 		line = line.strip('\n\r')
# 		self.buffer = line.split(" ")
# 		self.npar = len(self.buffer)


# #-----------------------------------------------

# # --- process the standard Input 
# def processInput( buffer, map ):

# 	read_list = [sys.stdin]
# 	timeout = 0.1
# 	ready = select.select(read_list, [], [], timeout)[0]
# 	if ready:
# 		for file in ready:
# 			line = file.readline()
# 			if not line: # EOF
# 				read_list.remove(file)
# 			else:
# 				buffer.build(line.rstrip())
# 				evaluateCommand( buffer, map )
# 	return

# # --- evaluate the buffer 
# def evaluateCommand( buffer, map ):

# 	com = buffer.getCommand()
# 	if com == "":
# 		buffer.clear()
# 		return
# 	i = 0
# 	if com.upper() == "Q":
# 		print "Bye bye !"
# 		sys.exit( )

# 	if com.upper() == "C":
# 		map.clear()
# 		buffer.clear()
# 		print "Clear the Map\n"

# 	elif com.upper() == "M":
# 		(param, fake1, fake2) = buffer.parse()
# 		if param.upper() == "O": # one shot
# 			map.setUpMode(0)
# 		elif param.upper() == "S": # Sum
# 			map.setUpMode(1)
# 		elif param.upper() == "A": # Average
# 			map.setUpMode(2)
# 		elif param.upper() == "D": # Difference
# 			map.setUpMode(3)
# 		print "Select mode %d\n" % map.getUpMode()

# 	elif com.upper() == "R": # read raw data file
# 		(fileName, ldcId, subEvent) = buffer.parse()
# 		map.readRawDataFile( fileName, int(ldcId), int(subEvent) )

# 	elif com.upper() == "D": # read pedestal segment file
# 		(fileName, segMent, fake1) = buffer.parse()
# 		map.readPedestalSegmentDAFile(fileName, int(segMent), 1)
# 		#map.draw()

# 	elif com.upper() == "E": # read threshold segment file
# 		(fileName, segMent, fake1) = buffer.parse()
# 		map.readPedestalSegmentDAFile(fileName, int(segMent), 2)
# 		#map.draw()
	
# 	elif com.upper() == "P": # read pedestal segment file
# 		(fileName, segMent, fake) = buffer.parse()
# 		map.readPedestalSegmentFile(fileName, int(segMent), 1)
# 		#map.draw()

# 	elif com.upper() == "T": # read threshold segment file
# 		(fileName, segMent, fake) = buffer.parse()
# 		map.readPedestalSegmentFile(fileName, int(segMent), 2)
# 		#map.draw()

# 	elif com.upper() == "U": # read pedestal column file
# 		(fileName, segMent, colUmn) = buffer.parse()
# 		map.readPedestalColumnFile(fileName, int(segMent), int(colUmn), 1)
# 		#map.draw()

# 	elif com.upper() == "V": # read thresholds  column file
# 		(fileName, segMent, colUmn) = buffer.parse()
# 		map.readPedestalColumnFile(fileName, int(segMent), int(colUmn), 2)
# 		#map.draw()

# 	buffer.clear()
# 	return

def progress_timeout(object):
	# global buffer
	global mappa
	x, y, w, h = object.allocation
	object.window.invalidate_rect((0,0,w,h),False)
	# processInput(buffer, mappa)
	mappa.readRawDataFile( )
	return

class PyGtkWidget(gtk.Widget):
	__gsignals__ = { 'realize': 'override',
					'expose-event' : 'override',
					'size-allocate': 'override',
					'size-request': 'override',}

	
	def __init__(self, mapPtr):
		gtk.Widget.__init__(self)
		self.mappa = mapPtr
		self.draw_gc = None
		self.layout = self.create_pango_layout(TEXT)
		self.layout.set_font_description(pango.FontDescription("sans serif 8"))
		self.timer = gobject.timeout_add (1000, progress_timeout, self)

	def do_realize(self):
		self.set_flags(self.flags() | gtk.REALIZED)
		self.window = gdk.Window(self.get_parent_window(),
								width=self.allocation.width,
								height=self.allocation.height,
								window_type=gdk.WINDOW_CHILD,
								wclass=gdk.INPUT_OUTPUT,
								event_mask=self.get_events() | gdk.EXPOSURE_MASK)
		if not hasattr(self.window, "cairo_create"):
			self.draw_gc = gdk.GC(self.window,
								line_width=1,
								line_style=gdk.SOLID,
								join_style=gdk.JOIN_ROUND)

		self.window.set_user_data(self)
		self.style.attach(self.window)
		self.style.set_background(self.window, gtk.STATE_NORMAL)
		self.window.move_resize(*self.allocation)

	def do_size_request(self, requisition):
		width, height = self.layout.get_size()
		requisition.width = (width // pango.SCALE + BORDER_WIDTH*4)* 1.45
		requisition.height = (3 * height // pango.SCALE + BORDER_WIDTH*4) * 1.2

	def do_size_allocate(self, allocation):
		self.allocation = allocation
		if self.flags() & gtk.REALIZED:
			self.window.move_resize(*allocation)

	def _expose_gdk(self, event):
		x, y, w, h = self.allocation
		self.layout = self.create_pango_layout('no cairo')
		fontw, fonth = self.layout.get_pixel_size()
		self.style.paint_layout(self.window, self.state, False,
							event.area, self, "label",
							(w - fontw) / 2, (h - fonth) / 2,
							self.layout)

	def _expose_cairo(self, event, cr):
		(w,h) = mappa.getMapDimPixel()
		(x,y) =	mappa.getMapBasePoint()
		cr.rectangle(x,y, w, h+70)
		cr.set_source_rgba(0.2,0.2,0.2,1)
		cr.fill()
		cr.stroke()
		mappa.draw(cr)
		mappa.drawScale(cr,x+w+5,y,20,h)
	
	def do_expose_event(self, event):
		self.chain(event)
		try:
			cr = self.window.cairo_create()
		except AttributeError:
			return self._expose_gdk(event)
		return self._expose_cairo(event, cr)

# ---------------------------------

mappa = Map()
mappa.setMapBasePoint(10,10)
(wid,hei) = mappa.getMapDimPixel()

# buffer = Buffer()


win = gtk.Window()
win.set_title('HMPID PADs Map')
win.resize(wid + 50, hei + 100)

win.connect('delete-event', gtk.main_quit)

event_box = gtk.EventBox()
event_box.connect("button_press_event", lambda w,e: win.set_decorated(not win.get_decorated()))
win.add(event_box)

w = PyGtkWidget(mappa)
event_box.add(w)

win.move(gtk.gdk.screen_width() - (wid+20), 40)
win.show_all()

gtk.main()

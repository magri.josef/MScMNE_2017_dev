#!/bin/bash  

rm -rf HMPIDout.bin
cat header.txt dcs_siu_data.txt >> temp_siu.txt
LINECT=0xe00000000; while read -r LINE; do (( LINECT+=1 )); done < temp_siu.txt;
length=$(printf %X $LINECT)
lengthn=${length:1:9}
sed -i "s/fileleng/$lengthn/g" temp_siu.txt
xxd -r -ps temp_siu.txt temp.bin
xxd -e temp.bin temp_1.bin
sed -i 's/^..........//' temp_1.bin
sed -i 's/.................$//' temp_1.bin
xxd -r -ps temp_1.bin HMPIDout.bin
rm -rf temp_siu.txt temp_1.bin temp.bin

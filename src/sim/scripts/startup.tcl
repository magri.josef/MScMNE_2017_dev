####################################################################
#### File : startup.tcl                                         ####
#### Description : Main simulation statup script                ####
#### Designer : Josef Magri (University of Malta CERN-EP/UAI)   ####
####################################################################

puts {
  ModelSimSE startup script version 1.0
  Copyright (c) Josef Magri, May 2017

##################################################
####             Welcome to the               ####
####    ALICE HMPID Simulation Environment    ####
##################################################
}

destroy  .buttons
toplevel .buttons
wm title .buttons "ALICE HMPID Simulation Environment"

# Close current wave windows
noview wave

# create dir for temporary generated files
file mkdir tmp_sim

# enable debug mode
set debug 0

set quartus_libDIR ../../../../lib/quartus/
set xilinx_libDIR ../../../../lib/xilinx/
set src_libDIR ../../../../src/
set sim_libDIR ../../../../src/sim/
set gassDIR ../../test_bench/file_IO/INPUT/GASSIPLEX
set ctpDIR ../../test_bench/file_IO/INPUT/CTP

set scriptDIR [pwd]
set comDIR ../test_bench/file_IO/COMMAND/
set comtype script
set simtype RCB_3_seg_8_col_DILO5_5_DIL_zero_ADC
set delay_val typ
set data_val off
set my_entry 0
set const_val 0
set count 0
set bool_val 0
set delay_typ 0
set CC 0
set NLM 0
set bsy_time 0
set seg 0
set col 0

set TTCrx_wave 0
set RCB_wave 0
set Seg_1_wave 0
set Seg_ALL_wave 0

set DILO_list [list]

proc add_frame title {
	global frame count
	set frame .buttons.frame$count
	
	frame $frame -border 2 -relief groove
	label $frame.label -text $title -foreground #0000FF
	pack  $frame       -side left -padx 2 -pady 2 -anchor n -fill both
	pack  $frame.label -side top  -padx 2 -pady 2
	incr count
}

proc add_label title {
	global frame count

	label $frame.$count -text $title -foreground #0000FF
	pack  $frame.$count -side top  -padx 2 -pady 2
	incr count
}

proc add_button {title command} {
	global frame count

	button $frame.$count -text $title -command $command
	pack   $frame.$count -side top -pady 1 -padx 1 -fill x
	incr count
}

proc add_checkbutton {title bool_val} { 
	global frame count

	checkbutton $frame.$count -text $title -variable $bool_val
	pack   $frame.$count -side top -pady 1 -padx 1 -anchor w
	incr count
}

proc add_radiobutton {title bool_val delay_typ state_typ} { 
	global frame count

	radiobutton $frame.$count -text $title -variable $bool_val -value $delay_typ -state $state_typ
	pack   $frame.$count -side top -pady 1 -padx 1 -anchor w
	incr count
}

proc add_message {title my_msg} {
	global frame count

	message $frame.$count -text $title -width 100 -textvariable $my_msg
	pack   $frame.$count -side top -pady 1 -padx 1  -anchor w
	incr count
}

proc add_entry {title} {
	global frame count
	global my_entry

	entry $frame.$count -text $title -textvariable my_entry -validate focusout -validatecommand {puts $my_entry; return $my_entry}
	pack   $frame.$count -side top -pady 2 -padx 2 -anchor w
	incr count
}

proc load_sim { } { 
	global delay_val
	global NLM
	global NLM_VAL
	global CC
	global CC_VAL
	global scriptDIR
	global simtype
	global seg
	global col

	set top_level {-wlfopt -t ps sim_top_modules.test_rcu}
	global sim_libDIR

	set tb_RCB /test_rcu/UUT_TOP_RCB

	set tb_ddl $tb_RCB/ddl_ctrlr_inst=${sim_libDIR}gen/RCB/ddl_ctrlr_vhd_fast.sdo
	set tb_ddl_fast $tb_RCB/ddl_ctrlr_inst=${sim_libDIR}gen/RCB/ddl_ctrlr_vhd_fast.sdo

	set tb_Seg_1_path $tb_RCB/TOP_segment_0_inst
	set tb_Seg_2_path $tb_RCB/TOP_segment_1_inst
	set tb_Seg_3_path $tb_RCB/TOP_segment_2_inst

	set tb_Seg_1 $tb_Seg_1_path/segment_ctrlr_inst=${sim_libDIR}gen/segment/segment_vhd_fast.sdo
	set tb_Seg_2 $tb_Seg_2_path/segment_ctrlr_inst=${sim_libDIR}gen/segment/segment_vhd_fast.sdo
	set tb_Seg_3 $tb_Seg_3_path/segment_ctrlr_inst=${sim_libDIR}gen/segment/segment_vhd_fast.sdo

	set tb_Seg_1_fast $tb_Seg_1_path/segment_ctrlr_inst=${sim_libDIR}gen/segment/segment_vhd_fast.sdo

	set tb_Col_1_path $tb_Seg_1_path/TOP_column_0_inst
	set tb_Col_2_path $tb_Seg_1_path/TOP_column_1_inst
	set tb_Col_3_path $tb_Seg_1_path/TOP_column_2_inst
	set tb_Col_4_path $tb_Seg_1_path/TOP_column_3_inst
	set tb_Col_5_path $tb_Seg_1_path/TOP_column_4_inst
	set tb_Col_6_path $tb_Seg_1_path/TOP_column_5_inst
	set tb_Col_7_path $tb_Seg_1_path/TOP_column_6_inst
	set tb_Col_8_path $tb_Seg_1_path/TOP_column_7_inst

	set tb_Seg_1_Col_1 $tb_Seg_1_path/TOP_column_0_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/new_typ.sdo
	set tb_Seg_1_Col_2 $tb_Seg_1_path/TOP_column_1_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/new_typ.sdo
	set tb_Seg_1_Col_3 $tb_Seg_1_path/TOP_column_2_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/new_typ.sdo
	set tb_Seg_1_Col_4 $tb_Seg_1_path/TOP_column_3_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_1_Col_5 $tb_Seg_1_path/TOP_column_4_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_1_Col_6 $tb_Seg_1_path/TOP_column_5_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_1_Col_7 $tb_Seg_1_path/TOP_column_6_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_1_Col_8 $tb_Seg_1_path/TOP_column_7_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo

	set tb_Seg_2_Col_1 $tb_Seg_2_path/TOP_column_0_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_2_Col_2 $tb_Seg_2_path/TOP_column_1_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_2_Col_3 $tb_Seg_2_path/TOP_column_2_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_2_Col_4 $tb_Seg_2_path/TOP_column_3_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_2_Col_5 $tb_Seg_2_path/TOP_column_4_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_2_Col_6 $tb_Seg_2_path/TOP_column_5_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_2_Col_7 $tb_Seg_2_path/TOP_column_6_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_2_Col_8 $tb_Seg_2_path/TOP_column_7_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo

	set tb_Seg_3_Col_1 $tb_Seg_3_path/TOP_column_0_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_3_Col_2 $tb_Seg_3_path/TOP_column_1_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_3_Col_3 $tb_Seg_3_path/TOP_column_2_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_3_Col_4 $tb_Seg_3_path/TOP_column_3_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_3_Col_5 $tb_Seg_3_path/TOP_column_4_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_3_Col_6 $tb_Seg_3_path/TOP_column_5_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_3_Col_7 $tb_Seg_3_path/TOP_column_6_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo
	set tb_Seg_3_Col_8 $tb_Seg_3_path/TOP_column_7_inst/column_fifo_inst=${sim_libDIR}gen/column_no_fifo/column_no_fifo_vhd_fast.sdo

		if { $NLM == 1 } {
			set	NLM_VAL " "
		} else {
			set	NLM_VAL "-quiet"
		} 

		if { $CC == 0 } {
			set	CC_VAL "-nocoverage"
		} else {
			set	CC_VAL "-coverage"
		} 

		switch -exact -- $simtype {

			RCB_only {
				eval vsim $NLM_VAL $CC_VAL $top_level -sdf$delay_val $tb_ddl
				eval wave_view
			}

			RCB_1_seg_1_col_no_DILO5 {
				eval vsim $NLM_VAL $CC_VAL $top_level -sdf$delay_val $tb_ddl -sdf$delay_val $tb_Seg_1 -sdf$delay_val $tb_Seg_1_Col_1
				eval wave_view
			}
			RCB_1_seg_1_col_DILO5_1_DIL {
				eval vsim $NLM_VAL $CC_VAL $top_level -sdf$delay_val $tb_ddl -sdf$delay_val $tb_Seg_1 -sdf$delay_val $tb_Seg_1_Col_1
				eval wave_view
			}  
			RCB_1_seg_8_col_no_DILO5 {
				eval vsim $NLM_VAL $CC_VAL $top_level -sdf$delay_val $tb_ddl -sdf$delay_val $tb_Seg_1 -sdf$delay_val $tb_Seg_1_Col_1 -sdf$delay_val $tb_Seg_1_Col_2 -sdf$delay_val $tb_Seg_1_Col_3 -sdf$delay_val $tb_Seg_1_Col_4 -sdf$delay_val $tb_Seg_1_Col_5 -sdf$delay_val $tb_Seg_1_Col_6 -sdf$delay_val $tb_Seg_1_Col_7 -sdf$delay_val $tb_Seg_1_Col_8
				eval wave_view
			} 
			RCB_1_seg_1_col_DILO5_5_DIL_zero_ADC {
				puts "Data Input generator Started"
				load_gen
				puts "Data Input generator DONE !"
				eval vsim $NLM_VAL $CC_VAL $top_level -sdf$delay_val $tb_ddl -sdf$delay_val $tb_Seg_1 -sdf$delay_val $tb_Seg_1_Col_1
				eval wave_view
				set seg 1
				set col 1
			} 
			RCB_1_seg_8_col_DILO5_5_DIL_zero_ADC {
				puts "Data Input generator Started"
				load_gen
				puts "Data Input generator DONE !"
				eval vsim $NLM_VAL $CC_VAL $top_level -sdf$delay_val $tb_ddl -sdf$delay_val $tb_Seg_1 -sdf$delay_val $tb_Seg_1_Col_1 -sdf$delay_val $tb_Seg_1_Col_2 -sdf$delay_val $tb_Seg_1_Col_3 -sdf$delay_val $tb_Seg_1_Col_4 -sdf$delay_val $tb_Seg_1_Col_5 -sdf$delay_val $tb_Seg_1_Col_6 -sdf$delay_val $tb_Seg_1_Col_7 -sdf$delay_val $tb_Seg_1_Col_8
				eval wave_view
				set seg 1
				set col 8
			}
			RCB_3_seg_8_col_DILO5_5_DIL_zero_ADC {
				puts "Data Input generator Started"
				load_gen
				puts "Data Input generator DONE !"
				eval vsim $NLM_VAL $CC_VAL $top_level -sdf$delay_val $tb_ddl -sdf$delay_val $tb_Seg_1 -sdf$delay_val $tb_Seg_1_Col_1 -sdf$delay_val $tb_Seg_1_Col_2 -sdf$delay_val $tb_Seg_1_Col_3 -sdf$delay_val $tb_Seg_1_Col_4 -sdf$delay_val $tb_Seg_1_Col_5 -sdf$delay_val $tb_Seg_1_Col_6 -sdf$delay_val $tb_Seg_1_Col_7 -sdf$delay_val $tb_Seg_1_Col_8 -sdf$delay_val $tb_Seg_2 -sdf$delay_val $tb_Seg_2_Col_1 -sdf$delay_val $tb_Seg_2_Col_2 -sdf$delay_val $tb_Seg_2_Col_3 -sdf$delay_val $tb_Seg_2_Col_4 -sdf$delay_val $tb_Seg_2_Col_5 -sdf$delay_val $tb_Seg_2_Col_6 -sdf$delay_val $tb_Seg_2_Col_7 -sdf$delay_val $tb_Seg_2_Col_8 -sdf$delay_val $tb_Seg_3 -sdf$delay_val $tb_Seg_3_Col_1 -sdf$delay_val $tb_Seg_3_Col_2 -sdf$delay_val $tb_Seg_3_Col_3 -sdf$delay_val $tb_Seg_3_Col_4 -sdf$delay_val $tb_Seg_3_Col_5 -sdf$delay_val $tb_Seg_3_Col_6 -sdf$delay_val $tb_Seg_3_Col_7 -sdf$delay_val $tb_Seg_3_Col_8
				eval wave_view
				set seg 3
				set col 8
			}
			default {}
		}
}

proc wave_view { } {
	global scriptDIR

	puts "Loading simulation waveforms..."
	set TIME_STAMP_FILE "time_stamp_pkg.vhd"
	set TIME_STAMP_TEMP_FILE "time_stamp_temp.vhd"
	do $scriptDIR/wave_view.tcl
}

proc load_ADC { } {
	global debug
	global scriptDIR
	global gassDIR
	global data
	global line
	global datum_index
	global DILO_list [list]
	global file_data
	global col_data
	global col_data_tmp
	global ch_no
	global seg
	global col
	global dilo
	set dilo 10

	puts "Loading ADC values"

	for {set segN 0} {$segN < $seg} {incr segN} {
		for {set colN 0} {$colN < $col} {incr colN} {
			if {$debug} { puts "Reading from : $gassDIR/ADC_$segN$colN.txt" }
			set fp [open "$gassDIR/ADC_$segN$colN.txt" r]
			set file_data [read $fp]
			set col_data [split $file_data "\n"]
			set col_data_tmp [lsearch -all -inline -not -exact $col_data {}]
				foreach line $col_data_tmp {
					set line_replaced [regsub -all "\t" $line ""]
					lappend DILO_list $line_replaced
				}
			close $fp
		}
		if {$debug} { puts "DILO_list $DILO_list" }
	}

	when -label trig_gass {sim:/test_rcu/UUT_TOP_RCB/TOP_segment_0_inst/TOP_column_0_inst/column_fifo_inst/TRIG'event and sim:/test_rcu/UUT_TOP_RCB/TOP_segment_0_inst/TOP_column_0_inst/column_fifo_inst/TRIG = '1'} {
		set datum_index 0
		puts "L0 Triger received"
		set dilo_noi [DILO_index $seg $col $dilo]
		DILO_data $DILO_list $datum_index $dilo_noi
		if {$debug} { puts "DILO_data $datum_index $dilo_noi" }
	}

	when -label clk_gass {sim:/test_rcu/UUT_TOP_RCB/TOP_segment_0_inst/TOP_column_0_inst/column_fifo_inst/CLKG'event and sim:/test_rcu/UUT_TOP_RCB/TOP_segment_0_inst/TOP_column_0_inst/column_fifo_inst/CLKG = '0'} {
		set datum_index [expr {$datum_index + 1}]
		if {$debug} { puts "Gassiplex CLK received" }
		DILO_data $DILO_list $datum_index $dilo_noi
		if {$debug} { puts "DILO_data $datum_index $dilo_noi" }
	}
}

proc DILO_index {segi coli diloi} {
	set index [expr {($segi - 1) * 80 + ($coli - 1) * 10 + $diloi}]
	return $index
}

proc DILO_revindex {index} {
	set segi [expr {$index / 80}]
	set coli [expr {($index - $segi*80) / 10}]
	set diloi [expr {$index % 10}]
	return [list $segi $coli $diloi]
}

proc DILO_data {dilo_list datum_index dilo_index} {
	global debug

	for {set dilo_no 0} {$dilo_no < $dilo_index} {incr dilo_no} {
		set data [lindex $dilo_list $dilo_no]
		set col_data [split $data " "]
		set ADC_data [lindex $col_data $datum_index]
		set revindex [DILO_revindex $dilo_no]
		set seg_i [lindex $revindex 0]
		set col_i [lindex $revindex 1]
		set dilo_i [expr {[lindex $revindex 2]}]
			if {$seg_i < 3 && $col_i < 8} {
				force -freeze sim:/test_rcu/UUT_TOP_RCB/TOP_segment_${seg_i}_inst/TOP_column_${col_i}_inst/TOP_AC_in_${dilo_i} 12'h$ADC_data 0
				if {$debug} { puts "Force is Segment: $seg_i Column: $col_i DILO: $dilo_i" }
			}
	}
}


proc busy_check { }	{
	global bsy_time
	global debug

	when -label bsy_start {sim:/test_rcu/UUT_TOP_RCB/ddl_ctrlr_inst/BUSY'event and sim:/test_rcu/UUT_TOP_RCB/ddl_ctrlr_inst/BUSY = '1'} {
		set before [RealToTime $now];
		if {$debug} { puts "BUSY SIGNAL START NOTED @ $before" }
	if {[when -label bsy_end] == ""} {
		when -label bsy_end {sim:/test_rcu/UUT_TOP_RCB/ddl_ctrlr_inst/BUSY'event and sim:/test_rcu/UUT_TOP_RCB/ddl_ctrlr_inst/BUSY = '0'} {
			set after [RealToTime $now];
			if {$debug} { puts "BUSY SIGNAL ENDED NOTED @ $after" }
			nowhen bsy_end;
			set bsy_time_tmp [subTime $after $before];
			if {$debug} { puts "total $bsy_time_tmp"}
			add_message "Busy time is" {bsy_time_tmp}
			}
		}
	}
}

proc load_gen { } {
	global debug
	global scriptDIR
	global data_val
	global gassDIR
	global my_entry
	puts "Generating ADC DATA..."
	if { $data_val != off } {
		set output [exec >&@stdout $scriptDIR/DataGen/ADCgen.out -path $gassDIR $data_val "$my_entry"]
		puts $output
	} else {
		puts "Using user defined data"
	} 
}

proc load_ctp { } {
	global scriptDIR
	global ctpDIR
	puts "CTP Emiulator loaded"
	cd $scriptDIR/CTPGen/
	exec >&@stdout $scriptDIR/CTPGen/TTC_gen.sh $ctpDIR
	cd $scriptDIR
}

proc load_display { } {
	global debug
	global scriptDIR
	global data_val
	global gassDIR
	global my_entry
	file copy -force ../test_bench/file_IO/OUTPUT/dcs_siu_data.txt $scriptDIR/DataDisplay/dcs_siu_data.txt
	if {$debug} { puts "Generating raw data file ..." }
	cd $scriptDIR/DataDisplay/
	set output [exec $scriptDIR/DataDisplay/conv.sh]
	if {$debug} { puts $output }
	if {$debug} { puts "Generating display  ..." }
	set python "python hmpDisplay.py" 
	set output [exec python $scriptDIR/DataDisplay/hmpDisplay.py]
	cd $scriptDIR
	if {$debug} { puts "Display Done" }
}

proc compile_sim {} {
	global scriptDIR
	uplevel #0 source $scriptDIR/time_sim_type.tcl
	uplevel #0 source $scriptDIR/compile.tcl
}

proc run_sim { } {
	global seg
	global col
	puts "Sim run started for :$seg seg $col col"
	set StdArithNoWarnings 1
	set NumericStdNoWarnings 1
	if { $seg == 3 && $col == 8 } {
		puts "Full run loaded: check busy enabled"
		busy_check
		load_ADC
		run -all
	} elseif { $seg > 0 && $col > 0 } {
		puts "Loading: ADC values"
		load_ADC
		run -all
	} else {
		puts "No ADC values needed"
		run -all
	}
}

proc quit_sim { } {
quit -sim
cd ../
}

add_frame  "Control"
add_button "Quit Simulation" {quit_sim}
add_button "Quit ModelSim"   {quit -force}
add_label ""
add_label "Simulate"
add_button "Run all"  {run_sim}
add_button "Restart"  {restart -force}
add_button "Continue"  {run -continue}
add_button "STOP"  {vsim_break}

add_frame "Type of Module"
add_button "Compile"      {compile_sim}
add_radiobutton "RCB_only"  {simtype} {RCB_only} {active}
add_radiobutton "RCB_1_seg_1_col_no_DILO5"        {simtype} {RCB_1_seg_1_col_no_DILO5} {active}
add_radiobutton "RCB_1_seg_1_col_DILO5_1_DIL" {simtype} {RCB_1_seg_1_col_DILO5_1_DIL} {active}
add_radiobutton "RCB_1_seg_8_col_no_DILO5" {simtype} {RCB_1_seg_8_col_no_DILO5} {active}  
add_radiobutton "RCB_1_seg_1_col_DILO5_5_DIL" {simtype} {RCB_1_seg_1_col_DILO5_5_DIL_zero_ADC} {active}
add_radiobutton "RCB_1_seg_8_col_DILO5_5_DIL" {simtype} {RCB_1_seg_8_col_DILO5_5_DIL_zero_ADC} {active}
add_radiobutton "RCB_3_seg_8_col_DILO5_5_DIL" {simtype} {RCB_3_seg_8_col_DILO5_5_DIL_zero_ADC} {active}

add_frame "Simulation Corner Model"
add_button "Load the Simulation"      {load_sim}
add_radiobutton "Minimum Delay" {delay_val} {min} {active}
add_radiobutton "Typical Delay" {delay_val} {typ} {active}
add_radiobutton "Maximum Delay" {delay_val} {max} {active}
add_label "Delay Factor"
add_label ""
add_label "     DATA Input Generator   "
add_radiobutton "OFF" {data_val} {off} {active}
add_radiobutton "Ramp" {data_val} {-ramp} {active}
add_radiobutton "Random" {data_val} {-random} {active}
add_radiobutton "Constant" {data_val} {-const} {active}
add_entry "Constant" 
add_label ""
add_button "CTP EMU"      {load_ctp}

add_frame  "Wave Window"
add_checkbutton "TTCrx" {TTCrx_wave}
add_checkbutton "RCB" {RCB_wave}
add_checkbutton "Segment 1" {Seg_1_wave}
add_checkbutton "Segment ALL" {Seg_1_wave}
add_checkbutton "Column 1" {Seg_1_wave}
add_checkbutton "DILO5 1" {Seg_1_wave}
add_checkbutton "DILO5 2" {Seg_1_wave}

# it is important that you do not include spaces in checkbutton {} as they are used as variable
# { PA } wrong format
# {PA} correct format
add_frame  "Simulation Monitors"
add_checkbutton "Performance Analyzer" {PA}
add_checkbutton "Code Coverage" {CC}
add_checkbutton "Enable Loading Message" {NLM}
add_label ""
add_button "Load HMPID Map Display"      {load_display}
add_label ""
add_label "        Results        "
add_message "Busy Value =>" {}

#####################################################################################
#### File        : wave_view.tcl                                       		       ####
#### Description : Sets the windows for wave patters according to user selection ####
#### Designer    : Josef Magri (University of Malta CERN-EP/UAI)   		           ####
#####################################################################################

puts {
#################################################
###        Setting Wave View Initialised     ####
#################################################
}

#set sim_libDIR ../../../../src/

####################################################################
####                  MAP of XILINX librarys               	    ####
####################################################################

set TTCrx_wave_patterns       	/test_rcu/UUT_TOP_RCB_1_seg/TTCrx_top_inst/*

set RCB_wave_patterns         	/test_rcu/UUT_TOP_RCB_1_seg/ddl_ctrlr_inst/*

set Segment_I_TOP_wave_patterns /test_rcu/UUT_TOP_RCB_1_seg/TOP_segment_ctrlr_I_inst/segment_ctrlr_inst/*

set Segment_I_wave_patterns   	/test_rcu/UUT_TOP_RCB_1_seg/TOP_segment_ctrlr_I_inst/segment_ctrlr_inst/*

set Segment_II_wave_patterns    /test_rcu/UUT_TOP_RCB_3_seg/TOP_segment_ctrlr_II_inst/segment_ctrlr_inst/*

set Segment_III_wave_patterns	/test_rcu/UUT_TOP_RCB_3_seg/TOP_segment_ctrlr_III_inst/segment_ctrlr_inst/*

set Column_I_wave_patterns {                 
                              /test_rcu/RCUTOP/segment_ctrlr_1/column_ctrlr_1/*
                           }

set Segment_All_wave_patterns {                 
                                /test_rcu/RCUTOP/segment_ctrlr_1/column_ctrlr_1/*
                              }

set Column_All_wave_patterns {                 
                                /test_rcu/RCUTOP/segment_ctrlr_1/column_ctrlr_1/*
                             }

set DILO5_col_1_wave_patterns {                 
                                /test_rcu/RCUTOP/segment_ctrlr_1/column_ctrlr_1/*
                              }

set DILOGIC_1_wave_patterns {                 
                                /test_rcu/RCUTOP/segment_ctrlr_1/column_ctrlr_1/*
                            }

set DILOGIC_DILO5_col_1_wave_patterns {                 
                                        /test_rcu/RCUTOP/segment_ctrlr_1/column_ctrlr_1/*
                                      }



# set wave_radices {
#                            hexadecimal {data q}
# } https://stackoverflow.com/questions/11804835/is-there-a-way-to-toggle-leaf-names-in-modelsim-through-the-tcl-api


# Prefer a fixed point font for the transcript
set PrefMain(font) {Courier 10 roman normal}

# Close current wave windows
noview wave

if { $TTCrx_wave == 1 } {
	
	view -new -title "TTCrx" wave
	  foreach pattern $TTCrx_wave_patterns {
	    add wave $pattern
	  }
	configure wave -signalnamewidth 1
}

if { $RCB_wave == 1 } {
	
	view -new -title "DDL Controller" wave
	  foreach pattern $RCB_wave_patterns {
	    add wave $pattern
	  }
	configure wave -signalnamewidth 1
}

if { $Seg_1_wave == 1} {

	view -new -title "Segment I" wave
	  foreach pattern $Segment_I_wave_patterns {
	    add wave $pattern
	  }
	configure wave -signalnamewidth 1
}

if { $Seg_ALL_wave == 1} {

	view -new -title "Segment I" wave
	  foreach pattern $Segment_I_wave_patterns {
	    add wave $pattern
	  }
	configure wave -signalnamewidth 1

	view -new -title "Segment II" wave
	  foreach pattern $Segment_II_wave_patterns {
	    add wave $pattern
	  }
	configure wave -signalnamewidth 1

	view -new -title "Segment III" wave
	  foreach pattern $Segment_II_wave_patterns {
	    add wave $pattern
	  }
	configure wave -signalnamewidth 1	
}

# if { $Col_1_wave == 1} {

# 	view -title "Column I" wave
# 	  foreach pattern $Column_I_wave_patterns {
# 	    add wave $pattern
# 	  }
# 	configure wave -signalnamewidth 1
# }



# if [llength $Segment_wave_patterns] {
#   view -new  wave
#   foreach pattern $Segment_wave_patterns {
#     add wave -divider Segment $pattern
#   }
#     configure wave -signalnamewidth 1
#   foreach {radix signals} $wave_radices {
#     foreach signal $signals {
#       catch {property wave -radix $radix $signal}
#     }
#   }
# }

puts {
#################################################
###        Setting Wave View DONE            ####
#################################################
}

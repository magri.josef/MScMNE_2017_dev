#####################################################################
#### File        : compile.tcl                                   ####
#### Description : Main simulation statup script                 ####
#### Designer    : Josef Magri (University of Malta CERN-EP/UAI) ####
#####################################################################

puts {
#################################################
###        Compiling Script Initialised      ####
#################################################
}

cd $scriptDIR/tmp_sim/

####################################################################
####                  MAP of XILINX librarys                    ####
####################################################################

set xilinx_library_file_list {
                               secureip
                               unisim
                               unimacro
                               simprim
                               xilinxcorelib
                             }

####################################################################
####                  MAP of Quartus librarys                   ####
####################################################################

set quartus_library_file_list {

                           altera             {
                                                altera_syn_attributes.vhd
                                                altera_standard_functions.vhd
                                                alt_dspbuilder_package.vhd
                                                altera_europa_support_lib.vhd
                                                altera_primitives_components.vhd
                                                altera_primitives.vhd
                                              }   

                           lpm                {
                                                220pack.vhd
                                                220model.vhd
                                              }

                           stratixii          {
                                                stratixii_atoms.vhd
                                                stratixii_components.vhd
                                              }

                           altera_mf          {
                                                altera_mf_components.vhd
                                                altera_mf.vhd
                                              }  

                           sgate              {
                                                sgate_pack.vhd
                                                sgate.vhd
                                              }  

                           cycloneiv_hssi     {
                                                cycloneiv_hssi_components.vhd
                                                cycloneiv_hssi_atoms.vhd
                                              }

                           cycloneiv          {
                                                cycloneiv_atoms.vhd
                                                cycloneiv_components.vhd
                                              }  

                           cycloneiv_pcie_hip {
                                                cycloneiv_pcie_hip_components.vhd
                                                cycloneiv_pcie_hip_atoms.vhd 
                                              }
                                }

####################################################################
####                  MAP of Simulation librarys                ####
####################################################################

set sim_library_file_list {

                            ttcrx_modules   {
                                              synth/TTCrx/reset_cordinator.vhd
                                              synth/TTCrx/registers.vhd
                                              synth/TTCrx/L1_output.vhd
                                              synth/TTCrx/indv_com_decoder.vhd
                                              synth/TTCrx/data_interface.vhd
                                              synth/TTCrx/counter_interface.vhd
                                              synth/TTCrx/counters.vhd
                                              synth/TTCrx/channelB_converter.vhd
                                              synth/TTCrx/TTCrx_TOP.vhf
                                            }

                            dilogic_modules {
                                              synth/DILOGIC-2/TOMem.vhd
                                              synth/DILOGIC-2/Sub.vhd
                                              synth/DILOGIC-2/IO_BUF_BUS.vhd
                                              synth/DILOGIC-2/InputRegister.vhd
                                              synth/DILOGIC-2/InputMUX.vhd
                                              synth/DILOGIC-2/EventCntr.vhd
                                              synth/DILOGIC-2/DFifoRam.vhd
                                              synth/DILOGIC-2/Controller.vhd
                                              synth/DILOGIC-2/Comp.vhd
                                              synth/DILOGIC-2/ClockCntr.vhd
                                              synth/DILOGIC-2/BmFifoRam.vhd
                                              synth/DILOGIC-2/AWordCntr.vhd
                                              synth/DILOGIC-2/TOP_DILO.vhf
                                            }         

                                    sim_gen {
                                              sim/gen/segment/segment.vho
                                              sim/gen/column_no_fifo/column_no_fifo.vho
                                              sim/gen/discrete_comp/column_fifo_only/fifo_only.vho
                                              sim/gen/discrete_comp/tri_bus.vhd
                                              sim/gen/discrete_comp/NOTGate.vhd
                                              sim/gen/discrete_comp/OR.vhd
                                              sim/gen/discrete_comp/70A-5250.vhd
                                              sim/gen/RCB/ddl_ctrlr.vho
                                            }

                                 tb_modules {
                                              sim/test_bench/tb_values.vhd
                                              sim/test_bench/tb_daq_pkg.vhd
                                              sim/test_bench/tb_ctp_pkg.vhd
                                            }
                          }

if { $simtype == "RCB_only"} {
  puts "Compiler loading RCB_only"
  set sim_top_type_file_list {
                            sim_top_modules {
                                              top_modules/TOP_RCB_only.vhd
                                              test_bench/tb_rcutop.vhd
                                            }
  }
} elseif { $simtype == "RCB_1_seg_1_col_no_DILO5"} {
  
  puts "Compiler loading RCB_1_seg_1_col_no_DILO5"
  set sim_top_type_file_list {
                            sim_top_modules {
                                              top_modules/TOP_col_no_DILO5.vhd
                                              top_modules/TOP_seg_1_col.vhd
                                              top_modules/TOP_RCB_1_seg.vhd
                                              test_bench/tb_rcutop.vhd
                                            }
  }
} elseif { $simtype == "RCB_1_seg_1_col_DILO5_1_DIL"} {
  
  puts "Compiler loading RCB_1_seg_1_col_DILO5_1_DIL"
  set sim_top_type_file_list {
                            sim_top_modules {
                                              top_modules/TOP_DILO5_1_DIL.vhd
                                              top_modules/TOP_col_zero_ADC.vhd
                                              top_modules/TOP_seg_1_col.vhd
                                              top_modules/TOP_RCB_1_seg.vhd
                                              test_bench/tb_rcutop.vhd
                                            }
  }
} elseif { $simtype == "RCB_1_seg_1_col_DILO5_5_DIL_zero_ADC"} {

  puts "Compiler loading RCB_1_seg_1_col_DILO5_5_DIL_zero_ADC"
  set sim_top_type_file_list {
                            sim_top_modules {
                                              top_modules/TOP_DILO5.vhd
                                              top_modules/TOP_col_zero_ADC.vhd
                                              top_modules/TOP_seg_1_col.vhd
                                              top_modules/TOP_RCB_1_seg.vhd
                                              test_bench/tb_rcutop.vhd
                                            }
  }
} elseif { $simtype == "RCB_1_seg_8_col_DILO5_5_DIL_zero_ADC"} {

  puts "Compiler loading RCB_1_seg_8_col_DILO5_5_DIL_zero_ADC"
  set sim_top_type_file_list {
                            sim_top_modules {
                                              top_modules/TOP_DILO5.vhd
                                              top_modules/TOP_col_zero_ADC.vhd
                                              top_modules/TOP_seg_8_col.vhd
                                              top_modules/TOP_RCB_1_seg.vhd
                                              test_bench/tb_rcutop.vhd
                                            }
  }
} elseif { $simtype == "RCB_3_seg_8_col_DILO5_5_DIL_zero_ADC"} {
  puts "Compiler loading RCB_3_seg_8_col_DILO5_5_DIL_zero_ADC"
  set sim_top_type_file_list {
                            sim_top_modules {
                                              top_modules/TOP_DILO5.vhd
                                              top_modules/TOP_col_zero_ADC.vhd
                                              top_modules/TOP_seg_8_col.vhd
                                              top_modules/TOP_RCB_3_seg.vhd
                                              test_bench/tb_rcutop.vhd
                                            }
  }
} elseif { $simtype == "RCB_1_seg_8_col_no_DILO5"} {
  puts "Compiler loading RCB_1_seg_8_col_no_DILO5"
  set sim_top_type_file_list {
                            sim_top_modules {
                                              top_modules/TOP_col_no_DILO5.vhd
                                              top_modules/TOP_seg_8_col.vhd
                                              top_modules/TOP_RCB_1_seg.vhd
                                              test_bench/tb_rcutop.vhd
                                            }
  }
} else {
    error "Something when wrong in compile.tcl script!"
}

foreach {library} $xilinx_library_file_list {
    vmap $library $xilinx_libDIR$library
}

foreach {library file_list} $quartus_library_file_list {
  vlib $library
  vmap work $library
    foreach file $file_list {
        vcom -93 $quartus_libDIR$file
  }
}



foreach {library file_list} $sim_library_file_list {
  vlib $library
  vmap work $library
    foreach file $file_list {
        if {[regexp {70A-5250.vhd?$} $file]} {
          vcom -2008 $src_libDIR$file
        } else {
          vcom -93 $src_libDIR$file
    }
  }
}

foreach {library file_list} $sim_top_type_file_list {
  vlib $library
  vmap work $library
    foreach file $file_list {
        vcom -2008 $sim_libDIR$file
        set last_compile_time 0
  }
}

puts {
#################################################
###        Compiling Script DONE             ####
#################################################
}

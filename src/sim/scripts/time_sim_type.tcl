#####################################################################
#### File        : time_sim_type.tcl                             ####
#### Description : Sets the simulation type and current time log ####
#### Designer    : Josef Magri (University of Malta CERN-EP/UAI) ####
#####################################################################

set SIM_VALUES_FILE ../test_bench/tb_values.vhd
set SIM_VALUES_TEMP_FILE ../test_bench/tb_values_temp.vhd


puts "\n################################################"
puts "####        Variable Setting Script         ####"
puts "####             Initialised...             ####"


# Check that the file to be modified is writable
if {[ file writable $SIM_VALUES_FILE ]} {

	# Get the system date and time.
	set DATE_TIME [ clock format [ clock seconds ] -format {%y_%m_%d_%H_%M_%S} ]

	# Open the file to be modified (FILE1) and the temporary file (FILE2).
	set FILE1 [ open $SIM_VALUES_FILE r]
	set FILE2 [ open $SIM_VALUES_TEMP_FILE w]

	# Initialize a variable to indicate that we have not made the change.
	set FOUND_time 0
	# Read each line of the file.
		while {[ gets $FILE1 LINE] >= 0} {
				# Check for the constant declaration.
				set INDEX [string first "constant SYNTHESIS_TIME_STAMP" $LINE ]
				# If it is not in this line...
			if { $INDEX == -1 } {
				# Write this line, unchanged, to the temporary file.
				puts $FILE2 $LINE
			} else {
				# If it is in this line, and we have not yet made a change, write the
				# current system date and time as a VHDL constant to the temporary file.
				if { $FOUND_time == 0 } {

					set DATE [ clock format [ clock seconds ] -format {%Y-%b-%d} ]
					set TIME [ clock format [ clock seconds ] -format {%H:%M:%S} ]
					puts $FILE2 "    constant SYNTHESIS_TIME_STAMP : std_logic_vector( 47 downto 0 ) := X\"$DATE_TIME\";"
					gets $FILE1 LINE
					puts $FILE2 "    constant path_com : string := path_in_com & \"$simtype.txt\";"
					gets $FILE1 LINE
					puts $FILE2 "    -- SYNTHESIS_TIME_STAMP was modified by time_sim_type.tcl on $DATE at $TIME."
					gets $FILE1 LINE
					puts $FILE2 "    -- The constant has the form YY_MM_DD_HH_mm_ss."

					# Note that we found the string and made the change
					set FOUND_time [expr {$FOUND_time + 1}]

				} else {
					# This is an error because it indicates two identical constant declarations
					# in the VHDL file.
					close $FILE1
					close $FILE2
					puts "---->           !!!! FAILED !!!!            <----\n"
					eval file delete $SIM_VALUES_TEMP_FILE
					error "Multiple \"constant SYNTHESIS_TIME_STAMP\" strings found in file $SIM_VALUES_FILE!!!"
				}
			}
		}

	# Close both files
	close $FILE1
	close $FILE2

	# If we never found the constant, throw an error.|| $FOUND_type == 0 
	if {$FOUND_time == 0 } {
		puts "---->           !!!! FAILED !!!!            <----\n"
		eval file delete $SIM_VALUES_TEMP_FILE
		error "\"constant SYNTHESIS_TIME_STAMP OR constant path_com\" not found in file $SIM_VALUES_FILE!!!"
	} else {
		set FOUND_time 0
		# Delete the original, to be modified file...
		eval file delete $SIM_VALUES_FILE
		# and rename the temporary file to the name of the original file.
		file rename -force $SIM_VALUES_TEMP_FILE $SIM_VALUES_FILE
		puts "####       -------------------------        ####"
		puts "####        Completed Successfully!         ####"
		puts "################################################\n"
	}

} else {
	puts "\n---->        Variable Setting Script        <----"
	puts "---->           !!!! FAILED !!!!            <----\n"
	error "File: $SIM_VALUES_FILE not writable!!!"
}

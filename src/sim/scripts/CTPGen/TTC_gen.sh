#!/bin/bash  

dirpath="${1:-$pwd}"

echo Welcome to Trigger generator
echo Please enter the following in binary

L1_TRIG_MESSAGE="00000000"
EVENT_ID_1="000000000000"
EVENT_ID_2_HIGH="000000000000"
EVENT_ID_2_LOW="000000000000"
TRIGGER_CLASSES_A="000000000000"
TRIGGER_CLASSES_B="000000000000"
TRIGGER_CLASSES_C="000000000000"
TRIGGER_CLASSES_D="000000000000"
TRIGGER_CLASSES_E="000000000000"
TRIGGER_CLASSES_F="000000000000"
TRIGGER_CLASSES_G="000000000000"
TRIGGER_CLASSES_H="000000000000"
TRIGGER_CLASSES_I="0000"

IDLE="200" #time between messages

L1_address="0001"
L2a_address="0011"
L2_address="0100"
L2r_address="0101"

L2_type="a"
L0_L1="6"
L1_L2="80"


rm -rf temp_trig.txt
rm -rf L0L1L2.txt

cat L0.txt > temp_trig.txt

# at 40Mhz 1/40Mhz=25nS
# 1 line 25ns

read -e -i "$L0_L1" -p "L0 to L1 time in micro seconds : " input
L0_L1="${input:-$L0_L1}"
L0_L1="$(echo $(($L0_L1*1000)))"
L0_L1="$(echo $(($L0_L1/25)))"

echo $L0_L1

read -e -i "$L1_L2" -p "L1 to L2 time in micro seconds : " input
L1_L2="${input:-$L1_L2}"
L1_L2="$(echo $(($L1_L2*1000)))"
L1_L2="$(echo $(($L1_L2/25)))"

echo $L1_L2

read -e -i "$L2_type" -p "Generate L2 accept[a] or reject[r] ?: " input
L2_type="${input:-$L2_type}"

# enable External commands / Parallel outputs
cat extCom.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

echo "1  0  1 !!!!!!!! L0 !!!!!!!!!!!!!!" >> temp_trig.txt

for (( c=0; c<=$L0_L1; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done



# IN RCB
# SA[[7..4] ->  Address[3..0]
# SA[3..0]D[7..0] -> DATA_IN[11.0]

# IN TTC STREAM
# 1 - 'E' 0= internal address 1=external address
# 1 - Always 1 seperator
# 0 -7 8bit subaddr - 00010000 -- L1 Message 
# 0 -6
# 0 -5
# 1 -4
# 0 -3
# 0 -2
# 0 -1
# 1 -0
# 1 -7 8bit data - 00101010 - edited to 10101010 for hmpid
# 0 -6
# 1 -5
# 0 -4
# 1 -3
# 0 -2
# 1 -1
# 0 -0
# 0 -0 7bit Checksum
# 1 -1
# 1 -2
# 1 -3
# 1 -4
# 1 -5
# 1 -6
# 1 - continue in idle state

# 43211000 -> 1234 0001
# 00008765 -> 5678 0000

# 43210010 -> 1234 0100
# 32198765 -> 56789123

# read -e -i "$L1_TRIG_MESSAGE" -p "L1_TRIG_MESSAGE [8 bit]: " input
# L1_TRIG_MESSAGE="${input:-$L1_TRIG_MESSAGE}"
# var1=$(echo $L1_TRIG_MESSAGE | cut -c1-4 |rev)
# var2=$(echo $L1_TRIG_MESSAGE | cut -c5-8 |rev)
# echo $var1
# echo $var2

# adr="1000"
# dat="0000"

# L1_TRIG_MESSAGE=$var1$adr$dat$var2
# echo "$L1_TRIG_MESSAGE"
# L1_TRIG_MESSAGE="$(echo $L1_TRIG_MESSAGE | sed 's:\(.\):\1  0  0,:g')"
# echo "$L1_TRIG_MESSAGE"


#Generate L1 trigger
echo "1  1  0 !!!!!!!! L1 !!!!!!!!!!!!!!" >> temp_trig.txt
echo "1  1  0 !!!!!!!! L1 !!!!!!!!!!!!!!" >> temp_trig.txt
# SA[3..0]D[7..4] -> DATA_IN[11..4]
cat data.txt >> temp_trig.txt
read -e -i "$L1_TRIG_MESSAGE" -p "L1_TRIG_MESSAGE [8 bit]: " input
L1_TRIG_MESSAGE="${input:-$L1_TRIG_MESSAGE}"
dat="1010"
L1_TRIG_MESSAGE=$L1_address$L1_TRIG_MESSAGE$dat
L1_TRIG_MESSAGE="$(echo $L1_TRIG_MESSAGE | sed 's:\(.\):\1  0  0,:g')"
echo "$L1_TRIG_MESSAGE" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$L1_L2; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

if [ "$L2_type" == "a" ]; then

cat data.txt >> temp_trig.txt
read -e -i "$EVENT_ID_1" -p "EVENT_ID_1 [12 bit]: " input
EVENT_ID_1="${input:-$EVENT_ID_1}"
EVENT_ID_1=$L2a_address$EVENT_ID_1
EVENT_ID_1="$(echo $EVENT_ID_1 | sed 's:\(.\):\1  0  0,:g')"
echo "$EVENT_ID_1" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

cat data.txt >> temp_trig.txt
read -e -i "$EVENT_ID_2_HIGH" -p "EVENT_ID_2_HIGH [12 bit]: " input
EVENT_ID_2_HIGH="${input:-$EVENT_ID_2_HIGH}"
EVENT_ID_2_HIGH=$L2_address$EVENT_ID_2_HIGH
EVENT_ID_2_HIGH="$(echo $EVENT_ID_2_HIGH | sed 's:\(.\):\1  0  0,:g')"
echo "$EVENT_ID_2_HIGH" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

cat data.txt >> temp_trig.txt
read -e -i "$EVENT_ID_2_LOW" -p "EVENT_ID_2_LOW [12 bit]: " input
EVENT_ID_2_LOW="${input:-$EVENT_ID_2_LOW}"
EVENT_ID_2_LOW=$L2_address$EVENT_ID_2_LOW
EVENT_ID_2_LOW="$(echo $EVENT_ID_2_LOW | sed 's:\(.\):\1  0  0,:g')"
echo "$EVENT_ID_2_LOW" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

cat data.txt >> temp_trig.txt
read -e -i "$TRIGGER_CLASSES_A" -p "TRIGGER_CLASSES_A [12 bit]: " input
TRIGGER_CLASSES_A="${input:-$TRIGGER_CLASSES_A}"
TRIGGER_CLASSES_A=$L2_address$TRIGGER_CLASSES_A
TRIGGER_CLASSES_A="$(echo $TRIGGER_CLASSES_A | sed 's:\(.\):\1  0  0,:g')"
echo "$TRIGGER_CLASSES_A" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

cat data.txt >> temp_trig.txt
read -e -i "$TRIGGER_CLASSES_B" -p "TRIGGER_CLASSES_B [12 bit]: " input
TRIGGER_CLASSES_B="${input:-$TRIGGER_CLASSES_B}"
TRIGGER_CLASSES_B=$L2_address$TRIGGER_CLASSES_B
TRIGGER_CLASSES_B="$(echo $TRIGGER_CLASSES_B | sed 's:\(.\):\1  0  0,:g')"
echo "$TRIGGER_CLASSES_B" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

cat data.txt >> temp_trig.txt
read -e -i "$TRIGGER_CLASSES_C" -p "TRIGGER_CLASSES_C [12 bit]: " input
TRIGGER_CLASSES_C="${input:-$TRIGGER_CLASSES_C}"
TRIGGER_CLASSES_C=$L2_address$TRIGGER_CLASSES_C
TRIGGER_CLASSES_C="$(echo $TRIGGER_CLASSES_C | sed 's:\(.\):\1  0  0,:g')"
echo "$TRIGGER_CLASSES_C" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

cat data.txt >> temp_trig.txt
read -e -i "$TRIGGER_CLASSES_D" -p "TRIGGER_CLASSES_D [12 bit]: " input
TRIGGER_CLASSES_D="${input:-$TRIGGER_CLASSES_D}"
TRIGGER_CLASSES_D=$L2_address$TRIGGER_CLASSES_D
TRIGGER_CLASSES_D="$(echo $TRIGGER_CLASSES_D | sed 's:\(.\):\1  0  0,:g')"
echo "$TRIGGER_CLASSES_D" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

cat data.txt >> temp_trig.txt
read -e -i "$TRIGGER_CLASSES_E" -p "TRIGGER_CLASSES_E [12 bit]: " input
TRIGGER_CLASSES_E="${input:-$TRIGGER_CLASSES_E}"
TRIGGER_CLASSES_E=$L2_address$TRIGGER_CLASSES_E
TRIGGER_CLASSES_E="$(echo $TRIGGER_CLASSES_E | sed 's:\(.\):\1  0  0,:g')"
echo "$TRIGGER_CLASSES_E" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

cat data.txt >> temp_trig.txt
read -e -i "$TRIGGER_CLASSES_F" -p "TRIGGER_CLASSES_F [12 bit]: " input
TRIGGER_CLASSES_F="${input:-$TRIGGER_CLASSES_F}"
TRIGGER_CLASSES_F=$L2_address$TRIGGER_CLASSES_F
TRIGGER_CLASSES_F="$(echo $TRIGGER_CLASSES_F | sed 's:\(.\):\1  0  0,:g')"
echo "$TRIGGER_CLASSES_F" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

cat data.txt >> temp_trig.txt
read -e -i "$TRIGGER_CLASSES_G" -p "TRIGGER_CLASSES_G [12 bit]: " input
TRIGGER_CLASSES_G="${input:-$TRIGGER_CLASSES_G}"
TRIGGER_CLASSES_G=$L2_address$TRIGGER_CLASSES_G
TRIGGER_CLASSES_G="$(echo $TRIGGER_CLASSES_G | sed 's:\(.\):\1  0  0,:g')"
echo "$TRIGGER_CLASSES_G" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

cat data.txt >> temp_trig.txt
read -e -i "$TRIGGER_CLASSES_H" -p "TRIGGER_CLASSES_H [12 bit]: " input
TRIGGER_CLASSES_H="${input:-$TRIGGER_CLASSES_H}"
TRIGGER_CLASSES_H=$L2_address$TRIGGER_CLASSES_H
TRIGGER_CLASSES_H="$(echo $TRIGGER_CLASSES_H | sed 's:\(.\):\1  0  0,:g')"
echo "$TRIGGER_CLASSES_H" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

#43210010 -> 01000 4321
#SA[3..0] ->DATA_IN[11..8]
cat data.txt >> temp_trig.txt
read -e -i "$TRIGGER_CLASSES_I" -p "TRIGGER_CLASSES_I [4 bit]: " input
TRIGGER_CLASSES_I="${input:-$TRIGGER_CLASSES_I}"
dat="00000000"
TRIGGER_CLASSES_I=$L2_address$TRIGGER_CLASSES_I$dat
TRIGGER_CLASSES_I="$(echo $TRIGGER_CLASSES_I | sed 's:\(.\):\1  0  0,:g')"
echo "$TRIGGER_CLASSES_I" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

tr -s ',' '\n' < temp_trig.txt > $dirpath/L0L1L2a.txt

elif [ "$L2_type" == "r" ]; then

cat data.txt >> temp_trig.txt
read -e -i "$EVENT_ID_1" -p "EVENT_ID_1 [12 bit]: " input
EVENT_ID_1="${input:-$EVENT_ID_1}"
EVENT_ID_1=$L2r_address$EVENT_ID_1
echo "$EVENT_ID_1" >> temp_trig.txt
cat chksum.txt >> temp_trig.txt

for (( c=0; c<=$IDLE; c++ ))
do  
   echo "1  0  0" >> temp_trig.txt
done

tr -s ',' '\n' < temp_trig.txt > $dirpath/L0L1L2r.txt

else
    echo "undefined option"
fi

rm -rf temp_trig.txt

echo "DONE"
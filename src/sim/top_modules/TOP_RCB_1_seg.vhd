LIBRARY IEEE;
LIBRARY STRATIXII;
LIBRARY sim_gen;
LIBRARY sim_top_modules;
LIBRARY ttcrx_modules;
USE IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
USE STRATIXII.STRATIXII_COMPONENTS.ALL;

ENTITY TOP_RCB IS
  PORT (
        TOP_fbD                    : INOUT std_logic_vector(31 DOWNTO 0);
    --from SIU 
        TOP_fiDIR                  : IN    std_logic;
        TOP_fiBENn                 : IN    std_logic;
        TOP_fiLFn                  : IN    std_logic;
    --to SIU
        TOP_foCLK                  : OUT   std_logic;
        TOP_foBSYn                 : OUT   std_logic;
        TOP_fbCTRLn                : INOUT std_logic;
        TOP_fbTENn                 : INOUT std_logic;
    --L0 to Column
        TOP_SEG_TRIG               : OUT   std_logic;
    --to LTU
        TOP_BUSY                   : OUT   std_logic;
    --from LTU
        TOP_L0_EXT_LVDS            : IN    std_logic;
    --to LEDs   
        TOP_TRIG_LED               : OUT   std_logic;
        TOP_ACCESS_LED             : OUT   std_logic;
    --from TOP to TTCRX
        TOP_clk                    : IN    std_logic; -- 40 Mhz included for simulation (in HW this is recovered from Optic Channels)
        TOP_Channel_A              : IN    std_logic;
        TOP_Channel_B              : IN    std_logic;
        TOP_RCB_clk                : IN    std_logic;
        TOP_rst                    : IN    std_logic
    );
  end TOP_RCB;

architecture BEHAVIORIAL of TOP_RCB is 

Signal    DoutStrb_i          :  std_logic;
Signal    SubAddr_i           :  std_logic_vector(7 DOWNTO 0);
Signal    DQ_i                :  std_logic_vector(3 DOWNTO 0);
Signal    BCntStrb_i          :  std_logic; -- := '0';
Signal    L1Accept_i          :  std_logic; 
Signal    BCnt_i              :  std_logic_vector(11 DOWNTO 0) := (others=>'0');
Signal    Dout_i              :  std_logic_vector(7 DOWNTO 0);
Signal    TTCready_i          :  std_logic;
Signal    reset_b_i           :  std_logic;
SIGNAL    Clock40_i           :  std_logic;
Signal    SEG_CLOCK_i         :  std_logic;
Signal    RESETn_to_SEGMENT_i :  std_logic;
Signal    LOC_RnW_i           :  std_logic;
Signal    DATABUS_ADD_i       :  std_logic_vector(12 DOWNTO 0);
Signal    LOC_CSn_i           :  std_logic;
Signal    LOCADD_i            :  std_logic_vector(3 DOWNTO 0);
Signal    BUSY_i              :  std_logic_vector(7 DOWNTO 0);
Signal    SEG_TRIG_i          :  std_logic;
Signal    RESETn_i            :  std_logic;
Signal    SPARE1_i            :  std_logic; 

Signal    LOCBUS_i            :  std_logic_vector(31 DOWNTO 0);
Signal    BUS_DIR_0_i         :  std_logic;
Signal    BUS_DIR_1_i         :  std_logic;
Signal    BUS_DIR_2_i         :  std_logic;
Signal    BUS_OE_0_i          :  std_logic;
Signal    BUS_OE_1_i          :  std_logic;
Signal    BUS_OE_2_i          :  std_logic;

-- TTCrx chip TOP
COMPONENT TTCrx_TOP is
   port ( Channel_A     : in    std_logic; 
          Channel_B     : in    std_logic; 
          clk           : in    std_logic; 
          reset_b       : in    std_logic; 
          BCnt          : out   std_logic_vector (11 downto 0); 
          BCntStrb      : out   std_logic; 
          Dout          : out   std_logic_vector (7 downto 0); 
          DoutStrb      : out   std_logic; 
          DQ            : out   std_logic_vector (3 downto 0); 
          L1Accept      : out   std_logic; 
          SubAddr       : out   std_logic_vector (7 downto 0);
          TTCReady      : out   std_logic
        );
END COMPONENT;

-- RCB top
 COMPONENT ddl_ctrlr IS
    PORT (
      SEG_CLOCK          : OUT   std_logic;
      CLK40DES1          : IN    std_logic;
      D_Str              : IN    std_logic;
      SA                 : IN    std_logic_vector(7 DOWNTO 0);
      DQ                 : IN    std_logic_vector(3 DOWNTO 0);
      BCNTSTR            : IN    std_logic;
      L1A                : IN    std_logic;
      L0_EXT_LVDS        : IN    std_logic;
      BCNT               : IN    std_logic_vector(11 DOWNTO 0);
      D                  : IN    std_logic_vector(7 DOWNTO 0);
      fiDIR              : IN    std_logic;
      fiBENn             : IN    std_logic;
      fiLFn              : IN    std_logic;
      fbCTRLn            : INOUT std_logic;
      fbTENn             : INOUT std_logic;
      fbD                : INOUT std_logic_vector(31 DOWNTO 0);
      TTC_READY          : IN    std_logic;
      foCLK              : OUT   std_logic;
      LOC_CSn            : OUT   std_logic;
      SEG_TRIG           : OUT   std_logic;
      BUSY               : OUT   std_logic;
      TRIG_LED           : OUT   std_logic;
      ACCESS_LED         : OUT   std_logic;
      foBSYn             : OUT   std_logic;
      Aout               : OUT   std_logic;
      L0_EXT_NIM         : IN    std_logic;
      Bout               : OUT   std_logic;
      Cout               : OUT   std_logic;
      L2_EXT             : IN    std_logic;
      Dout               : OUT   std_logic;
      EXT_RESET          : IN    std_logic;
      RESETn_to_SEGMENT  : OUT   std_logic;
      RESETn_to_TTCRX    : OUT   std_logic;
      LOC_RnW            : OUT   std_logic;
      DATABUS_ADD        : OUT   std_logic_vector(12 DOWNTO 0) );
  END COMPONENT;

--Tri State Buffer
 COMPONENT tri_bus is
   port ( DIR : in    std_logic; 
          OEn : in    std_logic; 
          A   : INOUT std_logic_vector(31 DOWNTO 0);
          B   : INOUT std_logic_vector(31 DOWNTO 0)
          );
END COMPONENT;

COMPONENT TOP_segment_ctrlr IS
  PORT (
        -- from DDL_CNTR
        TOP_fbD_seg      : INOUT std_logic_vector(27 DOWNTO 0);
        TOP_G_CLOCK      : IN std_logic;
        TOP_G_RESETn     : IN std_logic;
        TOP_LOC_CSn      : IN std_logic;
        TOP_LOC_RWn      : IN std_logic;
        TOP_DATABUS_ADD  : IN std_logic_vector(7 DOWNTO 0);
        --from to Buffer chip
        TOP_BUS_OE       : OUT std_logic;
        TOP_BUS_DIR      : OUT std_logic;
        -- PYSICAL DIP SWITCH
        TOP_SEG_NBR      : IN std_logic_vector(1 DOWNTO 0);
        TOP_Trig_DDL     : IN std_logic
      );
 END COMPONENT;

begin

  TTCrx_top_inst : TTCrx_top
    PORT MAP(

      --from TOP to TTCRX
          clk           => TOP_clk, 
          Channel_A     => TOP_Channel_A, 
          Channel_B     => TOP_Channel_B, 

      --from TTCRX to DDL
          DoutStrb      => DoutStrb_i, 
          SubAddr       => SubAddr_i, 
          DQ            => DQ_i, 
          BCntStrb      => BCntStrb_i, 
          L1Accept      => L1Accept_i, 
          BCnt          => BCnt_i,
          Dout          => Dout_i, 
          TTCReady      => TTCReady_i,

      --from DLL to TTCRX
          reset_b       => reset_b_i);

ddl_ctrlr_inst : ddl_ctrlr
  PORT MAP(
            fbD               => TOP_fbD, -- GLOBAL BUS
         --from SIU 
            fiDIR             => TOP_fiDIR,
            fiBENn            => TOP_fiBENn,
            fiLFn             => TOP_fiLFn,
         --from TTCRX
            CLK40DES1         => TOP_RCB_clk, 
            D_Str             => DoutStrb_i, 
            SA                => SubAddr_i, 
            DQ                => DQ_i,
            BCNTSTR           => BCntStrb_i, 
            L1A               => L1Accept_i, 
            BCNT              => BCnt_i,
            D                 => Dout_i, 
            TTC_READY         => TTCReady_i,

            L0_EXT_LVDS       => TOP_L0_EXT_LVDS, -- L0 from LTU    

       --from DDL_CNTR to TOP     
         --to segments
            RESETn_to_SEGMENT => RESETn_to_SEGMENT_i,
            SEG_TRIG          => TOP_SEG_TRIG,
            LOC_CSn           => LOC_CSn_i,
            LOC_RnW           => LOC_RnW_i,
            SEG_CLOCK         => SEG_CLOCK_i,
            DATABUS_ADD       => DATABUS_ADD_i, -- only [7:0]
         --to SIU
            foBSYn            => TOP_foBSYn,
            fbCTRLn           => TOP_fbCTRLn,
            fbTENn            => TOP_fbTENn,
            foCLK             => TOP_foCLK,
         --to LEDs
            TRIG_LED          => TOP_TRIG_LED,
            ACCESS_LED        => TOP_ACCESS_LED, 
         --to TTCRX
            RESETn_to_TTCRX   => open, --reset_b_i,
            
            BUSY              => TOP_BUSY, -- Busy To LTU

        -- Not Used Outputs
            Aout              => open,
            Bout              => open,
            Cout              => open,
            Dout              => open,

        -- Not Used Inputs
            EXT_RESET         => '0',
            L0_EXT_NIM        => '0',
            L2_EXT            => '0'
          );

------------------------------------------------------------------------
-- Segment 1
------------------------------------------------------------------------
tri_bus_0_inst :  tri_bus
    PORT MAP(
              DIR => BUS_DIR_0_i,
              OEn => BUS_OE_0_i, 
              A   => TOP_fbD,
              B   => LOCBUS_i -- to segment & coloumn
            );

TOP_segment_0_inst : TOP_segment_ctrlr
  PORT MAP (
            TOP_fbD_seg      => LOCBUS_i(27 DOWNTO 0),
            TOP_G_CLOCK      => SEG_CLOCK_i,
            TOP_G_RESETn     => RESETn_to_SEGMENT_i,
            TOP_LOC_CSn      => LOC_CSn_i,
            TOP_LOC_RWn      => LOC_RnW_i,
            TOP_DATABUS_ADD  => DATABUS_ADD_i(7 downto 0),
            --from to Buffer chip
            TOP_BUS_OE       => BUS_OE_0_i,
            TOP_BUS_DIR      => BUS_DIR_0_i,
            -- PYSICAL DIP SWITCH
            TOP_SEG_NBR      => "01",
            TOP_Trig_DDL     => TOP_SEG_TRIG
          );
  end BEHAVIORIAL;
-------------------------------------------------------------------------------
-- File : TOP_DILO5.vhd
-- Description : HMPID DILO5 card contains five DILOGIC chips
-- Designer : Josef Magri (University of Malta CERN-EP/UAI)
-------------------------------------------------------------------------------

LIBRARY IEEE;
LIBRARY STRATIXII;
LIBRARY sim_gen;
LIBRARY sim_top_modules;
LIBRARY dilogic_modules;
USE IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
USE STRATIXII.STRATIXII_COMPONENTS.ALL;
--USE work.my_values.ALL;

ENTITY TOP_DILO5 IS
  PORT (
--    TOP_fbD : INOUT std_logic_vector(31 DOWNTO 0);
    --from Column 
    TOP_CLK     : IN std_logic;
    TOP_Trg     : IN std_logic;
    TOP_Clr_N   : IN std_logic;
    TOP_StrIn_N : IN std_logic;
    TOP_Fcode   : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
    TOP_NrofGx  : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
    TOP_Rst_N   : IN STD_LOGIC;
    TOP_Sub_Cmp : IN STD_LOGIC;
    TOP_ADDRGX  : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
    
    TOP_DILOBUS     : INOUT std_logic_vector(17 DOWNTO 0);
    --to column 1 each DILO
    TOP_EN_IN1      : IN STD_LOGIC;
    TOP_EN_OUT1_IN2 : INOUT std_logic;
    TOP_EN_OUT2_IN3 : INOUT std_logic;
    TOP_EN_OUT3_IN4 : INOUT std_logic;
    TOP_EN_OUT4_IN5 : INOUT std_logic;
    TOP_EN_OUT5     : OUT std_logic;

    -- 1 each DIL-5
    TOP_DFifoEmpty_N : OUT std_logic; -- s_empty
    TOP_NoAData_N    : OUT STD_LOGIC; -- s_noAdata
    TOP_MAck_N       : OUT STD_LOGIC; -- MACK1_5

    -- from ADC 1 each DILO
    TOP_AC_in_1 : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
    TOP_AC_in_2 : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
    TOP_AC_in_3 : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
    TOP_AC_in_4 : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
    TOP_AC_in_5 : IN STD_LOGIC_VECTOR (11 DOWNTO 0)
  );
END TOP_DILO5;

ARCHITECTURE BEHAVIORIAL OF TOP_DILO5 IS

  SIGNAL TOP_StrIn_N_i      : std_logic;
  SIGNAL TOP_Clr_N_i        : std_logic;
  SIGNAL TOP_Rst_N_i        : std_logic;
  SIGNAL TOP_DFifoEmpty_N_i : std_logic;
  SIGNAL TOP_NoAData_N_i    : std_logic;
  SIGNAL TOP_EN_IN1_i       : std_logic;
  SIGNAL TOP_EN_OUT1_IN2_i  : std_logic;
  SIGNAL TOP_MAck_N_i       : std_logic := 'H'; -- Weak High, implemented through resistor R68 102 Ohm 
  SIGNAL TOP_MAck_0_i       : std_logic := 'H'; -- Weak High, implemented through resistor R68 102 Ohm 
  SIGNAL TOP_MAck_1_i       : std_logic := 'H'; -- Weak High, implemented through resistor R68 102 Ohm 
  SIGNAL TOP_MAck_2_i       : std_logic := 'H'; -- Weak High, implemented through resistor R68 102 Ohm 
  SIGNAL TOP_MAck_3_i       : std_logic := 'H'; -- Weak High, implemented through resistor R68 102 Ohm 
  SIGNAL TOP_MAck_4_i       : std_logic := 'H'; -- Weak High, implemented through resistor R68 102 Ohm 
  SIGNAL TOP_CLK_i       : std_logic;
  SIGNAL TOP_Trg_i       : std_logic;
  SIGNAL TOP_ADDRGX_i    :  STD_LOGIC_VECTOR (5 DOWNTO 0);

  COMPONENT TOP_DILO IS
    PORT (
          StrIn_N      : IN STD_LOGIC;
          CLK          : IN STD_LOGIC;
          Clr_N        : IN STD_LOGIC;
          EnaIn_N      : IN STD_LOGIC;
          Fcode        : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
          NrofGx       : IN STD_LOGIC_VECTOR (1 DOWNTO 0);
          Rst_N        : IN STD_LOGIC;
          DFifoEmpty_N : OUT STD_LOGIC;
          NoAData_N    : OUT STD_LOGIC;
          Trg          : IN STD_LOGIC;
          Sub_Cmp      : IN STD_LOGIC;
          ChAddr       : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
          AC_in        : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
          MAck_N       : OUT STD_LOGIC;
          EnaOut_N     : OUT STD_LOGIC;
          D            : INOUT STD_LOGIC_VECTOR (17 DOWNTO 0)
    );
  END COMPONENT;

  COMPONENT notGate IS
    PORT (
          in_0  : IN std_logic;
          in_1  : IN std_logic;
          in_2  : IN std_logic;
          in_3  : IN std_logic;
          in_4  : IN std_logic;
          in_5  : IN std_logic;
          in_6  : IN std_logic;
          in_7  : IN std_logic;
          out_0 : OUT std_logic;
          out_1 : OUT std_logic;
          out_2 : OUT std_logic;
          out_3 : OUT std_logic;
          out_4 : OUT std_logic;
          out_5 : OUT std_logic;
          out_6 : OUT std_logic;
          out_7 : OUT std_logic
    );
  END COMPONENT;

COMPONENT  ORgate is
   port ( IN_0 : in    std_logic; 
          IN_1 : in    std_logic; 
          IN_2: in    std_logic; 
          IN_3: in    std_logic; 
          IN_4: in    std_logic;
          OUT_0 : out    std_logic
          );
  END COMPONENT;

BEGIN

notGate_inst : notGate
  PORT MAP(
            in_0  => TOP_StrIn_N, 
            in_1  => TOP_Clr_N, 
            in_2  => TOP_Rst_N, 
            in_3  => TOP_CLK, 
            in_4  => TOP_Trg, -- not used
            in_5  => TOP_NoAData_N_i, 
            in_6  => TOP_DFifoEmpty_N_i, 
            in_7  => TOP_MAck_N_i, 
            out_0 => TOP_StrIn_N_i, 
            out_1 => TOP_Clr_N_i, 
            out_2 => TOP_Rst_N_i, 
            out_3 => TOP_CLK_i, 
            out_4 => TOP_Trg_i, -- not used
            out_5 => TOP_NoAData_N, 
            out_6 => TOP_DFifoEmpty_N, 
            out_7 => TOP_MAck_N
  );

notGate_ch_inst : notGate
  PORT MAP(
            in_0  => TOP_ADDRGX(0), 
            in_1  => TOP_ADDRGX(1), 
            in_2  => TOP_ADDRGX(2), 
            in_3  => TOP_ADDRGX(3), 
            in_4  => TOP_ADDRGX(4),
            in_5  => TOP_ADDRGX(5), 
            in_6  => '0', 
            in_7  => '0', 
            out_0 => TOP_ADDRGX_i(0),
            out_1 => TOP_ADDRGX_i(1),
            out_2 => TOP_ADDRGX_i(2),
            out_3 => TOP_ADDRGX_i(3),
            out_4 => TOP_ADDRGX_i(4),
            out_5 => TOP_ADDRGX_i(5),
            out_6 => open, 
            out_7 => open
  );

  OR_inst : ORgate
  PORT MAP(
            in_0  => TOP_MAck_0_i, 
            in_1  => TOP_MAck_1_i, 
            in_2  => TOP_MAck_2_i, 
            in_3  => TOP_MAck_3_i, 
            in_4  => TOP_MAck_4_i,
            out_0 => TOP_MAck_N_i
  );


TOP_DILO_1_inst : TOP_DILO
  PORT MAP(
            StrIn_N      => TOP_StrIn_N_i, 
            CLK          => TOP_CLK_i, 
            Clr_N        => TOP_Clr_N_i, --
            Fcode        => TOP_Fcode, 
            NrofGx       => TOP_NrofGx, 
            Rst_N        => TOP_Rst_N_i, 
            DFifoEmpty_N => TOP_DFifoEmpty_N_i, -- to adder
            NoAData_N    => TOP_NoAData_N_i, -- to adder
            Trg          => TOP_Trg_i, 
            Sub_Cmp      => TOP_Sub_Cmp, 
            MAck_N       => TOP_MAck_0_i,
            ChAddr       => TOP_ADDRGX_i,

            D            => TOP_DILOBUS(17 DOWNTO 0),
            AC_in        => TOP_AC_in_1, 
            EnaIn_N      => TOP_EN_IN1, 
            EnaOut_N     => TOP_EN_OUT1_IN2
  );

TOP_DILO_2_inst : TOP_DILO
  PORT MAP(
            StrIn_N      => TOP_StrIn_N_i, 
            CLK          => TOP_CLK_i, 
            Clr_N        => TOP_Clr_N_i, --
            Fcode        => TOP_Fcode, 
            NrofGx       => TOP_NrofGx, 
            Rst_N        => TOP_Rst_N_i, 
            DFifoEmpty_N => TOP_DFifoEmpty_N_i, -- to adder
            NoAData_N    => TOP_NoAData_N_i, -- to adder
            Trg          => TOP_Trg_i, 
            Sub_Cmp      => TOP_Sub_Cmp, 
            MAck_N       => TOP_MAck_1_i,
            ChAddr       => TOP_ADDRGX_i,

            D            => TOP_DILOBUS(17 DOWNTO 0),
            AC_in        => TOP_AC_in_2, 
            EnaIn_N      => TOP_EN_OUT1_IN2, 
            EnaOut_N     => TOP_EN_OUT2_IN3
  );

TOP_DILO_3_inst : TOP_DILO
  PORT MAP(
            StrIn_N      => TOP_StrIn_N_i, 
            CLK          => TOP_CLK_i, 
            Clr_N        => TOP_Clr_N_i, --
            Fcode        => TOP_Fcode, 
            NrofGx       => TOP_NrofGx, 
            Rst_N        => TOP_Rst_N_i, 
            DFifoEmpty_N => TOP_DFifoEmpty_N_i, -- to adder
            NoAData_N    => TOP_NoAData_N_i, -- to adder
            Trg          => TOP_Trg_i, 
            Sub_Cmp      => TOP_Sub_Cmp, 
            MAck_N       => TOP_MAck_2_i,
            ChAddr       => TOP_ADDRGX_i,

            D            => TOP_DILOBUS(17 DOWNTO 0),
            AC_in        => TOP_AC_in_3, 
            EnaIn_N      => TOP_EN_OUT2_IN3, 
            EnaOut_N     => TOP_EN_OUT3_IN4
  );

TOP_DILO_4_inst : TOP_DILO
  PORT MAP(
            StrIn_N      => TOP_StrIn_N_i, 
            CLK          => TOP_CLK_i, 
            Clr_N        => TOP_Clr_N_i, --
            Fcode        => TOP_Fcode, 
            NrofGx       => TOP_NrofGx, 
            Rst_N        => TOP_Rst_N_i, 
            DFifoEmpty_N => TOP_DFifoEmpty_N_i, -- to adder
            NoAData_N    => TOP_NoAData_N_i, -- to adder
            Trg          => TOP_Trg_i, 
            Sub_Cmp      => TOP_Sub_Cmp, 
            MAck_N       => TOP_MAck_3_i,
            ChAddr       => TOP_ADDRGX_i,

            D            => TOP_DILOBUS(17 DOWNTO 0),
            AC_in        => TOP_AC_in_4, 
            EnaIn_N      => TOP_EN_OUT3_IN4, 
            EnaOut_N     => TOP_EN_OUT4_IN5
  );

TOP_DILO_5_inst : TOP_DILO
  PORT MAP(
            StrIn_N      => TOP_StrIn_N_i, 
            CLK          => TOP_CLK_i, 
            Clr_N        => TOP_Clr_N_i, --
            Fcode        => TOP_Fcode, 
            NrofGx       => TOP_NrofGx, 
            Rst_N        => TOP_Rst_N_i, 
            DFifoEmpty_N => TOP_DFifoEmpty_N_i, -- to adder
            NoAData_N    => TOP_NoAData_N_i, -- to adder
            Trg          => TOP_Trg_i, 
            Sub_Cmp      => TOP_Sub_Cmp, 
            MAck_N       => TOP_MAck_4_i,
            ChAddr       => TOP_ADDRGX_i,

            D            => TOP_DILOBUS(17 DOWNTO 0),
            AC_in        => TOP_AC_in_5, 
            EnaIn_N      => TOP_EN_OUT4_IN5, 
            EnaOut_N     => TOP_EN_OUT5
  );

END BEHAVIORIAL;
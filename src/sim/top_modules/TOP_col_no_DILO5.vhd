-------------------------------------------------------------------------------
-- File : TOP_col_zero_ADC.vhd
-- Description : HMPID Coloumn Controller Card
-- Designer : Josef Magri (University of Malta CERN-EP/UAI)
-------------------------------------------------------------------------------

LIBRARY IEEE;
LIBRARY STRATIXII;
--LIBRARY design_library;
LIBRARY sim_gen;
LIBRARY sim_top_modules;
USE IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
USE STRATIXII.STRATIXII_COMPONENTS.ALL;
--USE design_library.my_values.ALL;

ENTITY TOP_column_fifo IS
	PORT 
	(
		TOP_fbD_col   : INOUT std_logic_vector(27 DOWNTO 0);
		-- from DDL_CNTR
		TOP_LOC_RWn   : IN std_logic;
		TOP_TRIG      : IN std_logic;
		-- from segment
		TOP_clockext  : IN std_logic;
		TOP_LOC_CS    : IN std_logic;
		TOP_LOCADD    : IN std_logic_vector(3 DOWNTO 0);
		TOP_RESETn    : IN std_logic;
		TOP_BUSY      : OUT std_logic;
		-- PYSICAL DIP SWITCH
		TOP_CARD_NUMB : IN std_logic_vector(4 DOWNTO 0);
		TOP_AC_in_0   : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		TOP_AC_in_1   : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		TOP_AC_in_2   : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		TOP_AC_in_3   : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		TOP_AC_in_4   : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		TOP_AC_in_5   : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		TOP_AC_in_6   : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		TOP_AC_in_7   : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		TOP_AC_in_8   : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		TOP_AC_in_9  : IN STD_LOGIC_VECTOR (11 DOWNTO 0)
	);
END TOP_column_fifo;

ARCHITECTURE BEHAVIORIAL OF TOP_column_fifo IS

	SIGNAL ADDR_DILO_i    : std_logic_vector(3 DOWNTO 0);
	SIGNAL CLKD_i         : std_logic;
	SIGNAL TRIGN_i        : std_logic;
	SIGNAL CLRD_i         : std_logic;
	SIGNAL ADDRGX_i       : std_logic_vector(5 DOWNTO 0);
	SIGNAL SUB_COMP_i     : std_logic;
	SIGNAL NBR_GASS_i     : std_logic_vector(1 DOWNTO 0);
	SIGNAL FC_i           : std_logic_vector(3 DOWNTO 0);
	SIGNAL STRIN_i        : std_logic;
	SIGNAL T1_i           : std_logic;
	SIGNAL RSTn_i         : std_logic;
	SIGNAL DL_IN_i        : std_logic := '1';
	SIGNAL s_almfull1_i   : std_logic;
	SIGNAL s_noAdata1_i   : std_logic;
	SIGNAL s_empty1_i     : std_logic;
	SIGNAL MACK1_5_i      : std_logic;
	SIGNAL s_almfull2_i   : std_logic;
	SIGNAL s_noAdata2_i   : std_logic;
	SIGNAL s_empty2_i     : std_logic;
	SIGNAL MACK6_10_i     : std_logic;
	SIGNAL EN_IN1_i       : std_logic;
	SIGNAL EN_OUT1_IN2_i  : std_logic; -- EnaIn_N
	SIGNAL EN_OUT2_IN3_i  : std_logic; -- EnaOut_N
	SIGNAL EN_OUT3_IN4_i  : std_logic;
	SIGNAL EN_OUT4_IN5_i  : std_logic;
	SIGNAL EN_OUT5_IN6_i  : std_logic;
	SIGNAL EN_OUT6_IN7_i  : std_logic;
	SIGNAL EN_OUT7_IN8_i  : std_logic;
	SIGNAL EN_OUT8_IN9_i  : std_logic;
	SIGNAL EN_OUT9_IN10_i : std_logic;
	SIGNAL EN_OUT10_i     : std_logic;
	SIGNAL aclr_i         : STD_LOGIC := '0';
	SIGNAL data_i         : STD_LOGIC_VECTOR (22 DOWNTO 0) := (others => '0');
	SIGNAL rdclk_i        : STD_LOGIC;
	SIGNAL rdreq_i        : STD_LOGIC;
	SIGNAL wrclk_i        : STD_LOGIC;
	SIGNAL wrreq_i        : STD_LOGIC;
	SIGNAL q_i            : STD_LOGIC_VECTOR (22 DOWNTO 0);
	SIGNAL rdempty_i      : STD_LOGIC;
	SIGNAL wrusedw_i      : STD_LOGIC_VECTOR (9 DOWNTO 0);
	SIGNAL DILOBUS_i      : std_logic_vector (17 DOWNTO 0);

	--Coloumn Top
	COMPONENT column_fifo IS
		PORT 
		(
			READ_NEXT    : OUT std_logic;
			STRIN        : OUT std_logic;
			CLRADD       : OUT std_logic;
			CLOCK_IN     : IN std_logic;
			RESETn       : IN std_logic;
			TRIG         : IN std_logic;
			LOC_CS       : IN std_logic;
			LOC_RWn      : IN std_logic;
			clockext     : IN std_logic;
			LOCADD       : IN std_logic_vector(3 DOWNTO 0);
			LOCBUS       : INOUT std_logic_vector(27 DOWNTO 0);
			BUSY         : OUT std_logic;
			INHIBIT      : IN std_logic;
			EN_OUT10     : IN std_logic;
			EMPTY_FIFO   : IN std_logic;
			wr_wrd       : IN std_logic_vector(9 DOWNTO 0);
			CLRG         : OUT std_logic;
			RSTn         : OUT std_logic;
			CLRD         : OUT std_logic;
			SUB_COMP     : OUT std_logic;
			TRIGN        : OUT std_logic;
			EN_IN1       : OUT std_logic;
			VTEST        : OUT std_logic;
			CLKD         : OUT std_logic;
			TRIG_DISABLE : OUT std_logic;
			DL_IN        : OUT std_logic;
			TRACK_HOLD   : OUT std_logic;
			CLOCK        : OUT std_logic;
			CLKADC       : OUT std_logic;
			CLKG         : OUT std_logic;
			WR_FIFO      : OUT std_logic;
			WR_CLK       : OUT std_logic;
			RD_FIFO      : OUT std_logic;
			RD_CLK       : OUT std_logic;
			aCLR         : OUT std_logic;
			EN_OUT8_IN9  : INOUT std_logic;
			EN_OUT9_IN10 : INOUT std_logic;
			EN_OUT4_IN5  : INOUT std_logic;
			EN_OUT7_IN8  : INOUT std_logic;
			EN_OUT5_IN6  : INOUT std_logic;
			EN_OUT6_IN7  : INOUT std_logic;
			EN_OUT3_IN4  : INOUT std_logic;
			EN_OUT1_IN2  : INOUT std_logic;
			EN_OUT2_IN3  : INOUT std_logic;
			ADDR_DILO    : OUT std_logic_vector(3 DOWNTO 0);
			MACK6_10     : IN std_logic;
			MACK1_5      : IN std_logic;
			ADDRGX       : OUT std_logic_vector(5 DOWNTO 0);
			DILOBUS      : INOUT std_logic_vector(17 DOWNTO 0);
			ff           : IN std_logic_vector(22 DOWNTO 0);
			FC           : OUT std_logic_vector(3 DOWNTO 0);
			FFIN         : OUT std_logic_vector(22 DOWNTO 0);
			NBR_GASS     : OUT std_logic_vector(1 DOWNTO 0);
			CARD_NUMB    : IN std_logic_vector(4 DOWNTO 0);
			spare2       : IN std_logic;
			spare1       : IN std_logic;
			START_READ   : IN std_logic;
			s_noAdata2   : IN std_logic;
			s_noAdata1   : IN std_logic;
			s_empty1     : IN std_logic;
			s_almfull1   : IN std_logic;
			s_empty2     : IN std_logic;
			s_almfull2   : IN std_logic
		);
	END COMPONENT;

	COMPONENT clmn_fifo IS
		PORT 
		(
			aclr    : IN STD_LOGIC := '0';
			data    : IN STD_LOGIC_VECTOR (22 DOWNTO 0);
			rdclk   : IN STD_LOGIC;
			rdreq   : IN STD_LOGIC;
			wrclk   : IN STD_LOGIC;
			wrreq   : IN STD_LOGIC;
			q       : OUT STD_LOGIC_VECTOR (22 DOWNTO 0);
			rdempty : OUT STD_LOGIC;
			wrfull  : OUT STD_LOGIC;
			wrusedw : OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
		);
	END COMPONENT;

	COMPONENT delay_chip IS
		PORT(
			 CLK_IN : in  std_logic; 
			 T1     : out std_logic
			);
	END COMPONENT;

BEGIN

delay_chip_inst : delay_chip
	PORT MAP (
			  CLK_IN => DL_IN_i,
			  T1     => T1_i
		     );

column_fifo_inst : column_fifo
	PORT MAP
	(
		CLRADD       => OPEN, 
		CLOCK_IN     => T1_i, -- 40MHz into gassiplex block
		DL_IN        => DL_IN_i,
		clockext     => TOP_clockext, -- 10Mhz from segmen
		TRIG_DISABLE => OPEN, 
		CLOCK        => OPEN, 
		spare1       => '0', -- clock_ext
		spare2       => '0',
		START_READ   => '0', -- GND
		READ_NEXT    => OPEN, -- GND
		LOC_CS       => TOP_LOC_CS,
		-- from RCB
		LOCBUS       => TOP_fbD_col,
		TRIG         => TOP_TRIG, -- L0 trigger from RCB
		LOC_RWn      => TOP_LOC_RWn,
		-- from Segment
		LOCADD       => TOP_LOCADD, 
		RESETn       => TOP_RESETn, 
		INHIBIT      => '0', -- GND
		BUSY         => TOP_BUSY,
		-- from Column to Dilogic -- ref. Dilogic datasheet
		ADDR_DILO    => ADDR_DILO_i, -- not used
		CLKD         => CLKD_i, -- CLK
		TRIGN        => TRIGN_i, -- TRG
		CLRD         => CLRD_i, -- Clr_N
		-- Ampl <11:0> is the ADC data input
		ADDRGX       => ADDRGX_i, -- ChAddr <5:0>
		SUB_COMP     => SUB_COMP_i, -- Sub_Cmp
		NBR_GASS     => NBR_GASS_i, -- NrofGx <1:0>
		FC           => FC_i, -- Fcode <3:0>
		STRIN        => STRIN_i, 
		RSTn         => RSTn_i, -- Rst_N
		-- from Dilogic to Column
		-------- DILO card 1 --------
		s_almfull1   => '0',
		s_noAdata1   => '0',
		s_empty1     => '0',
		MACK1_5      => MACK1_5_i,
		-------- DILO card 2 --------
		s_almfull2   => '0',
		s_noAdata2   => '0',
		s_empty2     => '0',
		MACK6_10     => MACK6_10_i,
		-- from Column to/from Dilogic
		DILOBUS      => DILOBUS_i, -- Ampl <11:0> ChAddr <5:0>
		-- DinA <17:0>
		EN_IN1       => EN_IN1_i, 
		EN_OUT1_IN2  => EN_OUT1_IN2_i, -- EnaIn_N
		EN_OUT2_IN3  => EN_OUT2_IN3_i, -- EnaOut_N
		EN_OUT3_IN4  => EN_OUT3_IN4_i, 
		EN_OUT4_IN5  => EN_OUT4_IN5_i, 
		EN_OUT5_IN6  => EN_OUT5_IN6_i, 
		EN_OUT6_IN7  => EN_OUT6_IN7_i, 
		EN_OUT7_IN8  => EN_OUT7_IN8_i, 
		EN_OUT8_IN9  => EN_OUT8_IN9_i,
		EN_OUT9_IN10 => EN_OUT9_IN10_i,
		EN_OUT10     => EN_OUT10_i, --'1'
		VTEST        => OPEN,
		-- from Column to Gassiplex -from pci connector to Header 5x2 from segment PCB
		CLRG         => OPEN, 
		-- to/from FIFO
		aCLR         => aclr_i, 
		FFIN         => data_i,
		RD_CLK       => rdclk_i, 
		RD_FIFO      => rdreq_i,
		WR_CLK       => wrclk_i, 
		WR_FIFO      => wrreq_i,
		ff           => q_i, 
		EMPTY_FIFO   => rdempty_i,
		wr_wrd       => wrusedw_i,
		-- PYSICAL DIP SWITCH
		CARD_NUMB => TOP_CARD_NUMB
	);
------------------------------------------------------------------------
-- Coloumn FIFO Buffer
------------------------------------------------------------------------
	clmn_fifo_inst : clmn_fifo
	PORT MAP
			(
			 aclr    => aclr_i, 
			 data    => data_i, 
			 rdclk   => rdclk_i, 
			 rdreq   => rdreq_i, 
			 wrclk   => wrclk_i, 
			 wrreq   => wrreq_i, 
			 q       => q_i, 
			 rdempty => rdempty_i, 
			 wrfull  => OPEN, 
			 wrusedw => wrusedw_i
	);
END BEHAVIORIAL;
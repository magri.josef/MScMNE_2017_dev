-------------------------------------------------------------------------------
-- File        : TOP_seg_1_col.vhd
-- Description : HMPID Segment with 1 Coloumn Controller Card
-- Designer    : Josef Magri (University of Malta CERN-EP/UAI)
-------------------------------------------------------------------------------

LIBRARY IEEE;
LIBRARY STRATIXII;
LIBRARY sim_gen;
LIBRARY sim_top_modules;
USE IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
USE STRATIXII.STRATIXII_COMPONENTS.ALL;

ENTITY TOP_segment_ctrlr IS
  PORT (
        -- from DDL_CNTR
        TOP_fbD_seg      : INOUT std_logic_vector(27 DOWNTO 0);
        TOP_G_CLOCK      : IN std_logic;
        TOP_G_RESETn     : IN std_logic;
        TOP_LOC_CSn      : IN std_logic;
        TOP_LOC_RWn      : IN std_logic;
        TOP_DATABUS_ADD  : IN std_logic_vector(7 DOWNTO 0);
        --from to Buffer chip
        TOP_BUS_OE       : OUT std_logic;
        TOP_BUS_DIR      : OUT std_logic;
        -- PYSICAL DIP SWITCH
        TOP_SEG_NBR      : IN std_logic_vector(1 DOWNTO 0);
        TOP_Trig_DDL     : IN std_logic
       );
end TOP_segment_ctrlr;

architecture BEHAVIORIAL of TOP_segment_ctrlr is 
Signal    BUSY_i              :  std_logic_vector(7 DOWNTO 0);
Signal    RESETn_i            :  std_logic;
Signal    SPARE1_i            :  std_logic;
Signal    SEG_TRIG_i          :  std_logic; 
Signal    LOCADD_i            :  std_logic_vector(3 DOWNTO 0);

Signal    LOC_CS1_i           :  std_logic;
Signal    LOC_CS2_i           :  std_logic;
Signal    LOC_CS3_i           :  std_logic;
Signal    LOC_CS4_i           :  std_logic;
Signal    LOC_CS5_i           :  std_logic;
Signal    LOC_CS6_i           :  std_logic;
Signal    LOC_CS7_i           :  std_logic;
Signal    LOC_CS8_i           :  std_logic;

COMPONENT  segment_ctrlr IS
    PORT (
          LOC_CS8      : OUT std_logic;
          G_CLOCK      : IN std_logic;
          LOC_CSn      : IN std_logic;
          SPARE1       : OUT std_logic;
          SEG_SELECTED : OUT std_logic;
          RESETn       : OUT std_logic;
          G_RESETn     : IN std_logic;
          DATABUS_ADD  : IN std_logic_vector (7 DOWNTO 0);
          SEG_NBR      : IN std_logic_vector(1 DOWNTO 0);
          LOC_CS5      : OUT std_logic;
          LOC_CS4      : OUT std_logic;
          LOC_CS7      : OUT std_logic;
          LOC_CS6      : OUT std_logic;
          LOC_CS1      : OUT std_logic;
          LOC_CS3      : OUT std_logic;
          LOC_CS2      : OUT std_logic;
          BUS_OE       : OUT std_logic;
          BUS_DIR      : OUT std_logic;
          LOC_RWn      : IN std_logic;
          READ8        : OUT std_logic;
          READ1        : OUT std_logic;
          INHIBIT      : OUT std_logic;
          LOCADD       : OUT std_logic_vector(3 DOWNTO 0);
          READ_DONE    : IN std_logic;
          BUSY         : IN std_logic_vector(7 DOWNTO 0);
          LOCBUS       : IN std_logic_vector(31 DOWNTO 0)
         );
END COMPONENT;

---Coloumn Top

COMPONENT TOP_column_fifo IS
  PORT (
        TOP_fbD_col     : INOUT std_logic_vector(27 DOWNTO 0);
        -- from DDL_CNTR
        TOP_LOC_RWn     : IN std_logic;
        TOP_TRIG        : IN std_logic;
        -- from segment
        TOP_clockext    : IN std_logic;
        TOP_LOC_CS      : IN std_logic;
        TOP_LOCADD      : IN std_logic_vector(3 DOWNTO 0);
        TOP_RESETn      : IN std_logic;
        TOP_BUSY        : OUT std_logic;
        -- PYSICAL DIP SWITCH
        TOP_CARD_NUMB   : IN std_logic_vector(4 DOWNTO 0);
        TOP_AC_in_0     : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        TOP_AC_in_1     : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        TOP_AC_in_2     : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        TOP_AC_in_3     : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        TOP_AC_in_4     : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        TOP_AC_in_5     : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        TOP_AC_in_6     : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        TOP_AC_in_7     : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        TOP_AC_in_8     : IN STD_LOGIC_VECTOR (11 DOWNTO 0);
        TOP_AC_in_9    : IN STD_LOGIC_VECTOR (11 DOWNTO 0)
    );
  end COMPONENT;

begin

------------------------------------------------------------------------
-- Segment
------------------------------------------------------------------------

segment_ctrlr_inst : segment_ctrlr
    PORT MAP (
      --from DDL_CNTR  
              G_CLOCK      => TOP_G_CLOCK, -- SEG_CLOCK 
              G_RESETn     => TOP_G_RESETn, -- RESETn_to_SEGMENT
              LOC_CSn      => TOP_LOC_CSn, -- LOC_CSn
              LOC_RWn      => TOP_LOC_RWn , -- LOC_RnW
              DATABUS_ADD  => TOP_DATABUS_ADD, -- DATABUS_ADD
              BUS_OE       => TOP_BUS_OE, -- tristate buffer ic
              BUS_DIR      => TOP_BUS_DIR,
      --from Segment to Columns
              LOC_CS1      => LOC_CS1_i, -- To first column
              LOC_CS2      => LOC_CS2_i,
              LOC_CS3      => LOC_CS3_i,
              LOC_CS4      => LOC_CS4_i,
              LOC_CS5      => LOC_CS5_i,
              LOC_CS6      => LOC_CS6_i,
              LOC_CS7      => LOC_CS7_i,
              LOC_CS8      => LOC_CS8_i,
              LOCADD       => LOCADD_i,
              SPARE1       => SPARE1_i,
              RESETn       => RESETn_i,
      --from Columns to Segment
              BUSY         => BUSY_i(7 DOWNTO 0),
      -- PYSICAL DIP SWITCH
              SEG_NBR      => TOP_SEG_NBR, --"01" first segment
      -- Not Used Outputs
              SEG_SELECTED => open,
      -- Not Used Inputs
              READ_DONE    => '0',
              LOCBUS       => (others => '0'),
      -- Not Used Outputs -- internally grounded  
              READ8        => open,
              READ1        => open,
              INHIBIT      => open
             );

------------------------------------------------------------------------
-- Coloumn 1
------------------------------------------------------------------------
TOP_column_0_inst : TOP_column_fifo 
   PORT MAP(
            TOP_fbD_col      => TOP_fbD_seg,
            TOP_LOC_RWn      => TOP_LOC_RWn,
            TOP_clockext     => SPARE1_i,    -- 10Mhz from segment
            TOP_LOC_CS       => LOC_CS1_i,
           -- from segment to Column
            TOP_LOCADD       => LOCADD_i,
            TOP_RESETn       => RESETn_i,
            TOP_BUSY         => BUSY_i(0),
            TOP_TRIG         => TOP_Trig_DDL, -- not 100% if correct
            TOP_CARD_NUMB    => "00001",
            TOP_AC_in_0      => (others => '0'),
            TOP_AC_in_1      => (others => '0'),
            TOP_AC_in_2      => (others => '0'),
            TOP_AC_in_3      => (others => '0'),
            TOP_AC_in_4      => (others => '0'),
            TOP_AC_in_5      => (others => '0'),
            TOP_AC_in_6      => (others => '0'),
            TOP_AC_in_7      => (others => '0'),
            TOP_AC_in_8      => (others => '0'),
            TOP_AC_in_9      => (others => '0')
           );

end BEHAVIORIAL;
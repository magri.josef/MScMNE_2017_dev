transcript on
if ![file isdirectory vhdl_libs] {
	file mkdir vhdl_libs
}

vlib vhdl_libs/stratixii
vmap stratixii ./vhdl_libs/stratixii
vcom -93 -work stratixii {/opt/altera/10.1/quartus/eda/sim_lib/stratixii_atoms.vhd}
vcom -93 -work stratixii {/opt/altera/10.1/quartus/eda/sim_lib/stratixii_components.vhd}

if {[file exists gate_work]} {
	vdel -lib gate_work -all
}
vlib gate_work
vmap work gate_work

vcom -93 -work work {column_no_fifo.vho}


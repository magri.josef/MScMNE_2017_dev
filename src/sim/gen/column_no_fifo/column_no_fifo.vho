-- Copyright (C) 1991-2010 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 10.1 Build 153 11/29/2010 SJ Web Edition"

-- DATE "06/24/2017 11:23:28"

-- 
-- Device: Altera EP2S15F484C5 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY IEEE;
LIBRARY STRATIXII;
USE IEEE.STD_LOGIC_1164.ALL;
USE STRATIXII.STRATIXII_COMPONENTS.ALL;

ENTITY 	column_fifo IS
    PORT (
	READ_NEXT : OUT std_logic;
	STRIN : OUT std_logic;
	CLRADD : OUT std_logic;
	CLOCK_IN : IN std_logic;
	RESETn : IN std_logic;
	TRIG : IN std_logic;
	LOC_CS : IN std_logic;
	LOC_RWn : IN std_logic;
	clockext : IN std_logic;
	LOCADD : IN std_logic_vector(3 DOWNTO 0);
	LOCBUS : INOUT std_logic_vector(27 DOWNTO 0);
	BUSY : OUT std_logic;
	INHIBIT : IN std_logic;
	EN_OUT10 : IN std_logic;
	EMPTY_FIFO : IN std_logic;
	wr_wrd : IN std_logic_vector(9 DOWNTO 0);
	CLRG : OUT std_logic;
	RSTn : OUT std_logic;
	CLRD : OUT std_logic;
	SUB_COMP : OUT std_logic;
	TRIGN : OUT std_logic;
	EN_IN1 : OUT std_logic;
	VTEST : OUT std_logic;
	CLKD : OUT std_logic;
	TRIG_DISABLE : OUT std_logic;
	DL_IN : OUT std_logic;
	TRACK_HOLD : OUT std_logic;
	CLOCK : OUT std_logic;
	CLKADC : OUT std_logic;
	CLKG : OUT std_logic;
	WR_FIFO : OUT std_logic;
	WR_CLK : OUT std_logic;
	RD_FIFO : OUT std_logic;
	RD_CLK : OUT std_logic;
	aclr : OUT std_logic;
	EN_OUT8_IN9 : INOUT std_logic;
	EN_OUT9_IN10 : INOUT std_logic;
	EN_OUT4_IN5 : INOUT std_logic;
	EN_OUT7_IN8 : INOUT std_logic;
	EN_OUT5_IN6 : INOUT std_logic;
	EN_OUT6_IN7 : INOUT std_logic;
	EN_OUT3_IN4 : INOUT std_logic;
	EN_OUT1_IN2 : INOUT std_logic;
	EN_OUT2_IN3 : INOUT std_logic;
	ADDR_DILO : OUT std_logic_vector(3 DOWNTO 0);
	MACK1_5 : IN std_logic;
	MACK6_10 : IN std_logic;
	ADDRGX : OUT std_logic_vector(5 DOWNTO 0);
	DILOBUS : INOUT std_logic_vector(17 DOWNTO 0);
	ff : IN std_logic_vector(22 DOWNTO 0);
	FC : OUT std_logic_vector(3 DOWNTO 0);
	FFIN : OUT std_logic_vector(22 DOWNTO 0);
	NBR_GASS : OUT std_logic_vector(1 DOWNTO 0);
	CARD_NUMB : IN std_logic_vector(4 DOWNTO 0);
	spare2 : IN std_logic;
	spare1 : IN std_logic;
	START_READ : IN std_logic;
	s_noAdata2 : IN std_logic;
	s_noAdata1 : IN std_logic;
	s_empty1 : IN std_logic;
	s_almfull1 : IN std_logic;
	s_empty2 : IN std_logic;
	s_almfull2 : IN std_logic
	);
END column_fifo;

-- Design Ports Information
-- LOCBUS[27]	=>  Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- LOCBUS[26]	=>  Location: PIN_L20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- LOCBUS[25]	=>  Location: PIN_AB8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[24]	=>  Location: PIN_K2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- LOCBUS[23]	=>  Location: PIN_A16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[22]	=>  Location: PIN_B12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[21]	=>  Location: PIN_Y17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[20]	=>  Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- LOCBUS[19]	=>  Location: PIN_H11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[18]	=>  Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- LOCBUS[17]	=>  Location: PIN_C14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[16]	=>  Location: PIN_B5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[15]	=>  Location: PIN_J7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- LOCBUS[14]	=>  Location: PIN_V11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[13]	=>  Location: PIN_G9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[12]	=>  Location: PIN_G15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[11]	=>  Location: PIN_C16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[10]	=>  Location: PIN_J18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- LOCBUS[9]	=>  Location: PIN_H12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[8]	=>  Location: PIN_AB10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[7]	=>  Location: PIN_B10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[6]	=>  Location: PIN_K4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- LOCBUS[5]	=>  Location: PIN_AB13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[4]	=>  Location: PIN_Y13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[3]	=>  Location: PIN_C12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LOCBUS[2]	=>  Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- LOCBUS[1]	=>  Location: PIN_P8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- LOCBUS[0]	=>  Location: PIN_A13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- EN_OUT8_IN9	=>  Location: PIN_K3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- EN_OUT9_IN10	=>  Location: PIN_W11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- EN_OUT4_IN5	=>  Location: PIN_J20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- EN_OUT7_IN8	=>  Location: PIN_N7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- EN_OUT5_IN6	=>  Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- EN_OUT6_IN7	=>  Location: PIN_A15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- EN_OUT3_IN4	=>  Location: PIN_P16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- EN_OUT1_IN2	=>  Location: PIN_B8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- EN_OUT2_IN3	=>  Location: PIN_AA11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[17]	=>  Location: PIN_Y5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[16]	=>  Location: PIN_C7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[15]	=>  Location: PIN_D8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[14]	=>  Location: PIN_C18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[13]	=>  Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- DILOBUS[12]	=>  Location: PIN_H22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- DILOBUS[11]	=>  Location: PIN_V14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[10]	=>  Location: PIN_H14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[9]	=>  Location: PIN_AB15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[8]	=>  Location: PIN_C17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[7]	=>  Location: PIN_W9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[6]	=>  Location: PIN_K17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- DILOBUS[5]	=>  Location: PIN_C19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[4]	=>  Location: PIN_F16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[3]	=>  Location: PIN_Y10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- DILOBUS[2]	=>  Location: PIN_K15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- DILOBUS[1]	=>  Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- DILOBUS[0]	=>  Location: PIN_AB7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- READ_NEXT	=>  Location: PIN_A19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- STRIN	=>  Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- CLRADD	=>  Location: PIN_C5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- BUSY	=>  Location: PIN_K16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- CLRG	=>  Location: PIN_K1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- RSTn	=>  Location: PIN_W10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- CLRD	=>  Location: PIN_P2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- SUB_COMP	=>  Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- TRIGN	=>  Location: PIN_J6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- EN_IN1	=>  Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- VTEST	=>  Location: PIN_V22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- CLKD	=>  Location: PIN_G22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- TRIG_DISABLE	=>  Location: PIN_L15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- DL_IN	=>  Location: PIN_J19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- TRACK_HOLD	=>  Location: PIN_T2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- CLOCK	=>  Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- CLKADC	=>  Location: PIN_C11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- CLKG	=>  Location: PIN_K7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- WR_FIFO	=>  Location: PIN_K20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- WR_CLK	=>  Location: PIN_P19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- RD_FIFO	=>  Location: PIN_N16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- RD_CLK	=>  Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- aclr	=>  Location: PIN_T10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- ADDR_DILO[3]	=>  Location: PIN_U2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- ADDR_DILO[2]	=>  Location: PIN_A8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- ADDR_DILO[1]	=>  Location: PIN_P17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- ADDR_DILO[0]	=>  Location: PIN_Y9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- ADDRGX[5]	=>  Location: PIN_N15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- ADDRGX[4]	=>  Location: PIN_U10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- ADDRGX[3]	=>  Location: PIN_C10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- ADDRGX[2]	=>  Location: PIN_K5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- ADDRGX[1]	=>  Location: PIN_T5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- ADDRGX[0]	=>  Location: PIN_L7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FC[3]	=>  Location: PIN_C9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FC[2]	=>  Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FC[1]	=>  Location: PIN_AA12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FC[0]	=>  Location: PIN_AB16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FFIN[22]	=>  Location: PIN_G19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[21]	=>  Location: PIN_E15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FFIN[20]	=>  Location: PIN_R3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[19]	=>  Location: PIN_AA10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FFIN[18]	=>  Location: PIN_H3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[17]	=>  Location: PIN_N8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[16]	=>  Location: PIN_K6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[15]	=>  Location: PIN_P3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[14]	=>  Location: PIN_P20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[13]	=>  Location: PIN_C6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FFIN[12]	=>  Location: PIN_B13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FFIN[11]	=>  Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[10]	=>  Location: PIN_L16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[9]	=>  Location: PIN_N21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[8]	=>  Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FFIN[7]	=>  Location: PIN_W2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[6]	=>  Location: PIN_N22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[5]	=>  Location: PIN_L3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[4]	=>  Location: PIN_Y11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FFIN[3]	=>  Location: PIN_T3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[2]	=>  Location: PIN_AA9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- FFIN[1]	=>  Location: PIN_K19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- FFIN[0]	=>  Location: PIN_N2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
-- NBR_GASS[1]	=>  Location: PIN_Y3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- NBR_GASS[0]	=>  Location: PIN_D13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- spare2	=>  Location: PIN_V16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- spare1	=>  Location: PIN_G7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- START_READ	=>  Location: PIN_A5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- s_noAdata2	=>  Location: PIN_AA5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- s_noAdata1	=>  Location: PIN_C4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- s_empty1	=>  Location: PIN_E19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- s_almfull1	=>  Location: PIN_Y15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- s_empty2	=>  Location: PIN_N4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- s_almfull2	=>  Location: PIN_V10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- INHIBIT	=>  Location: PIN_A10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- clockext	=>  Location: PIN_N20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- RESETn	=>  Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LOC_CS	=>  Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LOC_RWn	=>  Location: PIN_C8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- TRIG	=>  Location: PIN_G12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- EMPTY_FIFO	=>  Location: PIN_B18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- wr_wrd[4]	=>  Location: PIN_H21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- wr_wrd[5]	=>  Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- wr_wrd[6]	=>  Location: PIN_T8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- wr_wrd[7]	=>  Location: PIN_AA8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- wr_wrd[9]	=>  Location: PIN_N19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- wr_wrd[8]	=>  Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- wr_wrd[0]	=>  Location: PIN_E11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- wr_wrd[1]	=>  Location: PIN_H17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- wr_wrd[3]	=>  Location: PIN_N1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- wr_wrd[2]	=>  Location: PIN_J5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- EN_OUT10	=>  Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CLOCK_IN	=>  Location: PIN_M21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LOCADD[2]	=>  Location: PIN_A7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LOCADD[1]	=>  Location: PIN_P18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LOCADD[3]	=>  Location: PIN_K18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LOCADD[0]	=>  Location: PIN_P7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- MACK1_5	=>  Location: PIN_V9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- MACK6_10	=>  Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CARD_NUMB[0]	=>  Location: PIN_J3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[21]	=>  Location: PIN_M3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[20]	=>  Location: PIN_P6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[19]	=>  Location: PIN_K8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[18]	=>  Location: PIN_P21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[17]	=>  Location: PIN_Y12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[16]	=>  Location: PIN_C15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[15]	=>  Location: PIN_H7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[14]	=>  Location: PIN_D11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[13]	=>  Location: PIN_J8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[12]	=>  Location: PIN_K21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[11]	=>  Location: PIN_W15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[10]	=>  Location: PIN_B15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[9]	=>  Location: PIN_W13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[8]	=>  Location: PIN_B16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[7]	=>  Location: PIN_AB6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[6]	=>  Location: PIN_K22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[5]	=>  Location: PIN_F13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[4]	=>  Location: PIN_A18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[3]	=>  Location: PIN_V8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[2]	=>  Location: PIN_J16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[1]	=>  Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[0]	=>  Location: PIN_Y7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- ff[22]	=>  Location: PIN_N3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CARD_NUMB[4]	=>  Location: PIN_M20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CARD_NUMB[3]	=>  Location: PIN_D10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CARD_NUMB[2]	=>  Location: PIN_A6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- CARD_NUMB[1]	=>  Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF column_fifo IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_READ_NEXT : std_logic;
SIGNAL ww_STRIN : std_logic;
SIGNAL ww_CLRADD : std_logic;
SIGNAL ww_CLOCK_IN : std_logic;
SIGNAL ww_RESETn : std_logic;
SIGNAL ww_TRIG : std_logic;
SIGNAL ww_LOC_CS : std_logic;
SIGNAL ww_LOC_RWn : std_logic;
SIGNAL ww_clockext : std_logic;
SIGNAL ww_LOCADD : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_BUSY : std_logic;
SIGNAL ww_INHIBIT : std_logic;
SIGNAL ww_EN_OUT10 : std_logic;
SIGNAL ww_EMPTY_FIFO : std_logic;
SIGNAL ww_wr_wrd : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_CLRG : std_logic;
SIGNAL ww_RSTn : std_logic;
SIGNAL ww_CLRD : std_logic;
SIGNAL ww_SUB_COMP : std_logic;
SIGNAL ww_TRIGN : std_logic;
SIGNAL ww_EN_IN1 : std_logic;
SIGNAL ww_VTEST : std_logic;
SIGNAL ww_CLKD : std_logic;
SIGNAL ww_TRIG_DISABLE : std_logic;
SIGNAL ww_DL_IN : std_logic;
SIGNAL ww_TRACK_HOLD : std_logic;
SIGNAL ww_CLOCK : std_logic;
SIGNAL ww_CLKADC : std_logic;
SIGNAL ww_CLKG : std_logic;
SIGNAL ww_WR_FIFO : std_logic;
SIGNAL ww_WR_CLK : std_logic;
SIGNAL ww_RD_FIFO : std_logic;
SIGNAL ww_RD_CLK : std_logic;
SIGNAL ww_aclr : std_logic;
SIGNAL ww_ADDR_DILO : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_MACK1_5 : std_logic;
SIGNAL ww_MACK6_10 : std_logic;
SIGNAL ww_ADDRGX : std_logic_vector(5 DOWNTO 0);
SIGNAL ww_ff : std_logic_vector(22 DOWNTO 0);
SIGNAL ww_FC : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_FFIN : std_logic_vector(22 DOWNTO 0);
SIGNAL ww_NBR_GASS : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_CARD_NUMB : std_logic_vector(4 DOWNTO 0);
SIGNAL ww_spare2 : std_logic;
SIGNAL ww_spare1 : std_logic;
SIGNAL ww_START_READ : std_logic;
SIGNAL ww_s_noAdata2 : std_logic;
SIGNAL ww_s_noAdata1 : std_logic;
SIGNAL ww_s_empty1 : std_logic;
SIGNAL ww_s_almfull1 : std_logic;
SIGNAL ww_s_empty2 : std_logic;
SIGNAL ww_s_almfull2 : std_logic;
SIGNAL \clockext~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \1|73|CLKD_O~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \CLOCK_IN~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \1|2|20|44~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \350~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \1|2|12|lpm_mux_component|$00009|result_node~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \RESETn~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \1|73|op_2~22\ : std_logic;
SIGNAL \1|73|op_2~25_sumout\ : std_logic;
SIGNAL \1|2|20|21~regout\ : std_logic;
SIGNAL \1|2|20|42|DIVBY6~regout\ : std_logic;
SIGNAL \1|2|12|lpm_mux_component|$00009|result_node~0_combout\ : std_logic;
SIGNAL \1|2|20|22~regout\ : std_logic;
SIGNAL \1|2|20|24~regout\ : std_logic;
SIGNAL \1|2|12|lpm_mux_component|$00009|result_node~1_combout\ : std_logic;
SIGNAL \1|2|12|lpm_mux_component|$00009|result_node~combout\ : std_logic;
SIGNAL \1|73|CLKA_a~0_combout\ : std_logic;
SIGNAL \1|73|CLKA_a~combout\ : std_logic;
SIGNAL \1|73|CLKD_a~combout\ : std_logic;
SIGNAL \1|2|20|42|DIVBY6~0_combout\ : std_logic;
SIGNAL \1|2|20|22~0_combout\ : std_logic;
SIGNAL \1|2|20|23~regout\ : std_logic;
SIGNAL \1|2|20|24~0_combout\ : std_logic;
SIGNAL \1|73|CLKG_a~0_combout\ : std_logic;
SIGNAL \1|73|CLKG_a~combout\ : std_logic;
SIGNAL \1|2|63~combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[3]~0_combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|$00038|and_cascade[3]~0_combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|dffs[3]~0_combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[2]~1_combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|dffs[2]~1_combout\ : std_logic;
SIGNAL \1|2|20|42|DIVBY6~1_combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|dffs[1]~2_combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|dffs[0]~3_combout\ : std_logic;
SIGNAL \1|2|20|44~combout\ : std_logic;
SIGNAL \1|2|20|23~0_combout\ : std_logic;
SIGNAL \1|2|20|21~0_combout\ : std_logic;
SIGNAL \1|2|20|48~combout\ : std_logic;
SIGNAL \1|2|20|49~combout\ : std_logic;
SIGNAL \CLOCK_IN~combout\ : std_logic;
SIGNAL \CLOCK_IN~clkctrl_outclk\ : std_logic;
SIGNAL \1|2|20|44~clkctrl_outclk\ : std_logic;
SIGNAL \350~clkctrl_outclk\ : std_logic;
SIGNAL \1|2|12|lpm_mux_component|$00009|result_node~clkctrl_outclk\ : std_logic;
SIGNAL \LOCBUS~0\ : std_logic;
SIGNAL \LOCBUS~1\ : std_logic;
SIGNAL \LOCBUS~2\ : std_logic;
SIGNAL \LOCBUS~3\ : std_logic;
SIGNAL \LOCBUS~4\ : std_logic;
SIGNAL \LOCBUS~5\ : std_logic;
SIGNAL \LOCBUS~6\ : std_logic;
SIGNAL \LOCBUS~7\ : std_logic;
SIGNAL \LOCBUS~8\ : std_logic;
SIGNAL \LOCBUS~9\ : std_logic;
SIGNAL \LOCBUS~10\ : std_logic;
SIGNAL \LOCBUS~11\ : std_logic;
SIGNAL \LOCBUS~12\ : std_logic;
SIGNAL \LOCBUS~13\ : std_logic;
SIGNAL \LOCBUS~14\ : std_logic;
SIGNAL \LOCBUS~15\ : std_logic;
SIGNAL \LOCBUS~16\ : std_logic;
SIGNAL \LOCBUS~17\ : std_logic;
SIGNAL \LOCBUS~18\ : std_logic;
SIGNAL \LOCBUS~19\ : std_logic;
SIGNAL \LOCBUS~20\ : std_logic;
SIGNAL \LOCBUS~21\ : std_logic;
SIGNAL \LOCBUS~22\ : std_logic;
SIGNAL \EN_OUT8_IN9~0\ : std_logic;
SIGNAL \EN_OUT9_IN10~0\ : std_logic;
SIGNAL \EN_OUT4_IN5~0\ : std_logic;
SIGNAL \EN_OUT7_IN8~0\ : std_logic;
SIGNAL \EN_OUT5_IN6~0\ : std_logic;
SIGNAL \EN_OUT6_IN7~0\ : std_logic;
SIGNAL \EN_OUT3_IN4~0\ : std_logic;
SIGNAL \EN_OUT1_IN2~0\ : std_logic;
SIGNAL \EN_OUT2_IN3~0\ : std_logic;
SIGNAL \DILOBUS~0\ : std_logic;
SIGNAL \DILOBUS~1\ : std_logic;
SIGNAL \DILOBUS~2\ : std_logic;
SIGNAL \DILOBUS~3\ : std_logic;
SIGNAL \DILOBUS~4\ : std_logic;
SIGNAL \DILOBUS~5\ : std_logic;
SIGNAL \DILOBUS~6\ : std_logic;
SIGNAL \DILOBUS~7\ : std_logic;
SIGNAL \DILOBUS~8\ : std_logic;
SIGNAL \DILOBUS~9\ : std_logic;
SIGNAL \DILOBUS~10\ : std_logic;
SIGNAL \DILOBUS~11\ : std_logic;
SIGNAL \DILOBUS~12\ : std_logic;
SIGNAL \DILOBUS~13\ : std_logic;
SIGNAL \DILOBUS~14\ : std_logic;
SIGNAL \DILOBUS~15\ : std_logic;
SIGNAL \DILOBUS~16\ : std_logic;
SIGNAL \DILOBUS~17\ : std_logic;
SIGNAL \LOC_CS~combout\ : std_logic;
SIGNAL \LOC_RWn~combout\ : std_logic;
SIGNAL \412~0_combout\ : std_logic;
SIGNAL \386|FC[2]~feeder_combout\ : std_logic;
SIGNAL \RESETn~combout\ : std_logic;
SIGNAL \88|_~3_combout\ : std_logic;
SIGNAL \386|DATA_BUS~1_combout\ : std_logic;
SIGNAL \88|_~2_combout\ : std_logic;
SIGNAL \RESETn~clkctrl_outclk\ : std_logic;
SIGNAL \88|RESET_DASY2~regout\ : std_logic;
SIGNAL \88|STR_STRIN~feeder_combout\ : std_logic;
SIGNAL \EN_OUT10~combout\ : std_logic;
SIGNAL \379~regout\ : std_logic;
SIGNAL \88|STR_STRIN~regout\ : std_logic;
SIGNAL \88|IDLE~0_combout\ : std_logic;
SIGNAL \88|IDLE~regout\ : std_logic;
SIGNAL \386|FC[3]~0_combout\ : std_logic;
SIGNAL \88|_~4_combout\ : std_logic;
SIGNAL \88|DATA_WRITE_ON_FIFO~regout\ : std_logic;
SIGNAL \88|DATA_WRITE~0_combout\ : std_logic;
SIGNAL \88|DATA_WRITE~1_combout\ : std_logic;
SIGNAL \88|DATA_WRITE~regout\ : std_logic;
SIGNAL \88|READ_PED4~0_combout\ : std_logic;
SIGNAL \88|READ_PED4~regout\ : std_logic;
SIGNAL \88|FIFO_WR_SEL~3_combout\ : std_logic;
SIGNAL \88|READ_END~regout\ : std_logic;
SIGNAL \88|CLEAR_CODE~0_combout\ : std_logic;
SIGNAL \386|SUB_COMP~0_combout\ : std_logic;
SIGNAL \88|_~0_combout\ : std_logic;
SIGNAL \88|ENIN1~5_combout\ : std_logic;
SIGNAL \88|LOAD_PED~regout\ : std_logic;
SIGNAL \88|LOAD_PED1~regout\ : std_logic;
SIGNAL \88|_~5_combout\ : std_logic;
SIGNAL \88|LOAD_PULSE~regout\ : std_logic;
SIGNAL \88|DILO_SEL~0_combout\ : std_logic;
SIGNAL \88|LOAD_PED2~regout\ : std_logic;
SIGNAL \88|CLEAR_FIFO~4_combout\ : std_logic;
SIGNAL \88|LOAD_PED3~regout\ : std_logic;
SIGNAL \88|CLEAR_FIFO~3_combout\ : std_logic;
SIGNAL \88|PED_MEAS~regout\ : std_logic;
SIGNAL \88|PED_MEAS1~regout\ : std_logic;
SIGNAL \88|PED_MEAS2~regout\ : std_logic;
SIGNAL \88|PED_MEAS3~regout\ : std_logic;
SIGNAL \88|PED_MEAS4~0_combout\ : std_logic;
SIGNAL \88|PED_MEAS4~regout\ : std_logic;
SIGNAL \88|RESET_DASY~0_combout\ : std_logic;
SIGNAL \EMPTY_FIFO~combout\ : std_logic;
SIGNAL \88|DATA_READ_FIFO~0_combout\ : std_logic;
SIGNAL \88|DATA_READ_FIFO~regout\ : std_logic;
SIGNAL \88|_~1_combout\ : std_logic;
SIGNAL \88|RESET_DASY~1_combout\ : std_logic;
SIGNAL \88|RESET_DASY~regout\ : std_logic;
SIGNAL \421~combout\ : std_logic;
SIGNAL \88|DATA_READ~0_combout\ : std_logic;
SIGNAL \88|DATA_READ~regout\ : std_logic;
SIGNAL \286~0_combout\ : std_logic;
SIGNAL \45|52[11]~20_combout\ : std_logic;
SIGNAL \45|52[6]~13_combout\ : std_logic;
SIGNAL \45|52[10]~21_combout\ : std_logic;
SIGNAL \45|52[9]~22_combout\ : std_logic;
SIGNAL \45|52[8]~23_combout\ : std_logic;
SIGNAL \45|52[7]~24_combout\ : std_logic;
SIGNAL \45|52[6]~12_combout\ : std_logic;
SIGNAL \45|52[5]~14_combout\ : std_logic;
SIGNAL \45|52[4]~15_combout\ : std_logic;
SIGNAL \45|52[3]~16_combout\ : std_logic;
SIGNAL \45|52[2]~17_combout\ : std_logic;
SIGNAL \45|52[1]~18_combout\ : std_logic;
SIGNAL \45|52[0]~19_combout\ : std_logic;
SIGNAL \88|CHOOSER~0_combout\ : std_logic;
SIGNAL \88|CHOOSER~1_combout\ : std_logic;
SIGNAL \88|CHOOSER~regout\ : std_logic;
SIGNAL \88|ENIN1~2_combout\ : std_logic;
SIGNAL \45|50[3]~4_combout\ : std_logic;
SIGNAL \88|FIFO_WR_SEL~2_combout\ : std_logic;
SIGNAL \45|50[2]~5_combout\ : std_logic;
SIGNAL \88|FCO[1]~2_combout\ : std_logic;
SIGNAL \45|50[1]~6_combout\ : std_logic;
SIGNAL \88|FCO[0]~4_combout\ : std_logic;
SIGNAL \88|ENIN1~4_combout\ : std_logic;
SIGNAL \88|READ_PED~regout\ : std_logic;
SIGNAL \88|READ_PED1~regout\ : std_logic;
SIGNAL \88|READ_PED2~regout\ : std_logic;
SIGNAL \88|READ_PED3~regout\ : std_logic;
SIGNAL \88|FCO[0]~3_combout\ : std_logic;
SIGNAL \45|50[0]~7_combout\ : std_logic;
SIGNAL \45|43~1_combout\ : std_logic;
SIGNAL \386|SUB_COMP~regout\ : std_logic;
SIGNAL \45|48~1_combout\ : std_logic;
SIGNAL \271[9]~0_combout\ : std_logic;
SIGNAL \45|44[0]~2_combout\ : std_logic;
SIGNAL \45|45[3]~4_combout\ : std_logic;
SIGNAL \45|45[2]~5_combout\ : std_logic;
SIGNAL \45|45[1]~6_combout\ : std_logic;
SIGNAL \45|45[0]~7_combout\ : std_logic;
SIGNAL \45|47[1]~2_combout\ : std_logic;
SIGNAL \45|47[0]~3_combout\ : std_logic;
SIGNAL \45|46[1]~2_combout\ : std_logic;
SIGNAL \45|46[0]~3_combout\ : std_logic;
SIGNAL \clockext~combout\ : std_logic;
SIGNAL \clockext~clkctrl_outclk\ : std_logic;
SIGNAL \386|Bypass_dilo~0_combout\ : std_logic;
SIGNAL \46|52~regout\ : std_logic;
SIGNAL \46|51~regout\ : std_logic;
SIGNAL \46|56~regout\ : std_logic;
SIGNAL \46|53~regout\ : std_logic;
SIGNAL \46|55~regout\ : std_logic;
SIGNAL \46|54~regout\ : std_logic;
SIGNAL \46|57~regout\ : std_logic;
SIGNAL \46|48~regout\ : std_logic;
SIGNAL \46|49~regout\ : std_logic;
SIGNAL \88|DILO_SEL~combout\ : std_logic;
SIGNAL \88|STRIN~4_combout\ : std_logic;
SIGNAL \424~combout\ : std_logic;
SIGNAL \1|73|COUNT1[0]~0_combout\ : std_logic;
SIGNAL \1|27|5|count[0]~0_combout\ : std_logic;
SIGNAL \1|27|13~feeder_combout\ : std_logic;
SIGNAL \1|27|13~regout\ : std_logic;
SIGNAL \1|27|5|op_1~1_sumout\ : std_logic;
SIGNAL \1|27|5|op_1~2\ : std_logic;
SIGNAL \1|27|5|op_1~5_sumout\ : std_logic;
SIGNAL \1|27|5|BUSY_CLR~1_combout\ : std_logic;
SIGNAL \1|27|5|op_1~6\ : std_logic;
SIGNAL \1|27|5|op_1~9_sumout\ : std_logic;
SIGNAL \1|27|5|op_1~10\ : std_logic;
SIGNAL \1|27|5|op_1~13_sumout\ : std_logic;
SIGNAL \1|27|5|op_1~14\ : std_logic;
SIGNAL \1|27|5|op_1~17_sumout\ : std_logic;
SIGNAL \1|27|5|op_1~18\ : std_logic;
SIGNAL \1|27|5|op_1~21_sumout\ : std_logic;
SIGNAL \1|27|5|op_1~22\ : std_logic;
SIGNAL \1|27|5|op_1~25_sumout\ : std_logic;
SIGNAL \1|27|5|BUSY_CLR~0_combout\ : std_logic;
SIGNAL \1|27|5|BUSY_CLEAR~combout\ : std_logic;
SIGNAL \1|2|61~feeder_combout\ : std_logic;
SIGNAL \1|2|61~regout\ : std_logic;
SIGNAL \1|73|op_2~1_sumout\ : std_logic;
SIGNAL \1|73|op_2~2\ : std_logic;
SIGNAL \1|73|op_2~5_sumout\ : std_logic;
SIGNAL \1|73|op_2~6\ : std_logic;
SIGNAL \1|73|op_2~9_sumout\ : std_logic;
SIGNAL \1|73|op_2~10\ : std_logic;
SIGNAL \1|73|op_2~13_sumout\ : std_logic;
SIGNAL \1|6|refg[6]~0_combout\ : std_logic;
SIGNAL \1|73|op_2~14\ : std_logic;
SIGNAL \1|73|op_2~17_sumout\ : std_logic;
SIGNAL \1|73|op_2~18\ : std_logic;
SIGNAL \1|73|op_2~21_sumout\ : std_logic;
SIGNAL \1|6|refg[5]~1_combout\ : std_logic;
SIGNAL \1|6|_~0_combout\ : std_logic;
SIGNAL \1|73|CLRADD$wire~0_combout\ : std_logic;
SIGNAL \1|73|CLRADD$wire~1_combout\ : std_logic;
SIGNAL \1|73|CLRADD$wire~combout\ : std_logic;
SIGNAL \INHIBIT~combout\ : std_logic;
SIGNAL \1|27|7~feeder_combout\ : std_logic;
SIGNAL \1|27|7~regout\ : std_logic;
SIGNAL \1|27|9~combout\ : std_logic;
SIGNAL \1|73|CLR_G~0_combout\ : std_logic;
SIGNAL \1|2|9~0_combout\ : std_logic;
SIGNAL \1|2|9~regout\ : std_logic;
SIGNAL \1|74~combout\ : std_logic;
SIGNAL \1|73|CLKD_a~0_combout\ : std_logic;
SIGNAL \1|73|CLRG_O~combout\ : std_logic;
SIGNAL \359~regout\ : std_logic;
SIGNAL \357~regout\ : std_logic;
SIGNAL \397~regout\ : std_logic;
SIGNAL \398~regout\ : std_logic;
SIGNAL \399~regout\ : std_logic;
SIGNAL \400~combout\ : std_logic;
SIGNAL \352~regout\ : std_logic;
SIGNAL \378~regout\ : std_logic;
SIGNAL \TRIG~combout\ : std_logic;
SIGNAL \350~combout\ : std_logic;
SIGNAL \360~0_combout\ : std_logic;
SIGNAL \360~regout\ : std_logic;
SIGNAL \367~regout\ : std_logic;
SIGNAL \368~combout\ : std_logic;
SIGNAL \88|FCO[0]~0_combout\ : std_logic;
SIGNAL \88|ENIN1~3_combout\ : std_logic;
SIGNAL \88|ENIN1~combout\ : std_logic;
SIGNAL \1|2|89~0_combout\ : std_logic;
SIGNAL \1|2|89~regout\ : std_logic;
SIGNAL \1|73|CLKD~feeder_combout\ : std_logic;
SIGNAL \1|73|CLKD~regout\ : std_logic;
SIGNAL \1|73|CLKD_O~combout\ : std_logic;
SIGNAL \1|85~combout\ : std_logic;
SIGNAL \1|46~combout\ : std_logic;
SIGNAL \1|2|85~feeder_combout\ : std_logic;
SIGNAL \1|2|85~regout\ : std_logic;
SIGNAL \1|87~combout\ : std_logic;
SIGNAL \1|88~combout\ : std_logic;
SIGNAL \1|89~combout\ : std_logic;
SIGNAL \1|2|81~feeder_combout\ : std_logic;
SIGNAL \1|2|83~combout\ : std_logic;
SIGNAL \1|2|81~regout\ : std_logic;
SIGNAL \1|2|43~feeder_combout\ : std_logic;
SIGNAL \1|2|43~regout\ : std_logic;
SIGNAL \1|2|49~feeder_combout\ : std_logic;
SIGNAL \1|2|49~regout\ : std_logic;
SIGNAL \1|73|CLKA~feeder_combout\ : std_logic;
SIGNAL \1|73|CLR_A~0_combout\ : std_logic;
SIGNAL \1|73|CLKA~regout\ : std_logic;
SIGNAL \1|73|CLKA_O~combout\ : std_logic;
SIGNAL \1|73|CLKG~feeder_combout\ : std_logic;
SIGNAL \1|73|CLR_G~1_combout\ : std_logic;
SIGNAL \1|73|CLKG~regout\ : std_logic;
SIGNAL \1|73|CLKG_O~combout\ : std_logic;
SIGNAL \88|WR_FIFO~combout\ : std_logic;
SIGNAL \88|RD_FIFO~combout\ : std_logic;
SIGNAL \425~combout\ : std_logic;
SIGNAL \199|DILO10~0_combout\ : std_logic;
SIGNAL \199|READ_STATE~14_combout\ : std_logic;
SIGNAL \199|DILO10~regout\ : std_logic;
SIGNAL \417~feeder_combout\ : std_logic;
SIGNAL \417~regout\ : std_logic;
SIGNAL \199|IDLE~0_combout\ : std_logic;
SIGNAL \199|IDLE~regout\ : std_logic;
SIGNAL \199|WAITING_STATE~0_combout\ : std_logic;
SIGNAL \199|WAITING_STATE~regout\ : std_logic;
SIGNAL \199|DILO1~0_combout\ : std_logic;
SIGNAL \199|DILO1~regout\ : std_logic;
SIGNAL \199|DILO2~0_combout\ : std_logic;
SIGNAL \199|DILO2~regout\ : std_logic;
SIGNAL \199|DILO3~0_combout\ : std_logic;
SIGNAL \199|DILO3~regout\ : std_logic;
SIGNAL \199|DILO4~0_combout\ : std_logic;
SIGNAL \199|DILO4~regout\ : std_logic;
SIGNAL \199|DILO5~0_combout\ : std_logic;
SIGNAL \199|DILO5~regout\ : std_logic;
SIGNAL \199|DILO6~0_combout\ : std_logic;
SIGNAL \199|DILO6~regout\ : std_logic;
SIGNAL \199|DILO7~0_combout\ : std_logic;
SIGNAL \199|DILO7~regout\ : std_logic;
SIGNAL \199|DILO8~0_combout\ : std_logic;
SIGNAL \199|DILO8~regout\ : std_logic;
SIGNAL \199|DILO9~0_combout\ : std_logic;
SIGNAL \199|DILO9~regout\ : std_logic;
SIGNAL \1|73|CLKD_O~clkctrl_outclk\ : std_logic;
SIGNAL \1|73|ADDRESS[0]~1_combout\ : std_logic;
SIGNAL \1|73|ADDRESS[5]~0_combout\ : std_logic;
SIGNAL \1|73|op_1~1_sumout\ : std_logic;
SIGNAL \1|73|op_1~2\ : std_logic;
SIGNAL \1|73|op_1~5_sumout\ : std_logic;
SIGNAL \1|73|op_1~6\ : std_logic;
SIGNAL \1|73|op_1~9_sumout\ : std_logic;
SIGNAL \1|73|op_1~10\ : std_logic;
SIGNAL \1|73|op_1~13_sumout\ : std_logic;
SIGNAL \1|73|op_1~14\ : std_logic;
SIGNAL \1|73|op_1~17_sumout\ : std_logic;
SIGNAL \88|FCO[2]~1_combout\ : std_logic;
SIGNAL \MACK6_10~combout\ : std_logic;
SIGNAL \MACK1_5~combout\ : std_logic;
SIGNAL \197~combout\ : std_logic;
SIGNAL \340~regout\ : std_logic;
SIGNAL \371~regout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00053|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00051|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00049|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00047|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00045|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00043|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00041|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00039|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00037|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00035|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00033|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00031|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00029|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00027|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00025|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00023|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00021|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00019|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00017|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00015|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00013|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00011|result_node~0_combout\ : std_logic;
SIGNAL \299|lpm_mux_component|$00009|result_node~0_combout\ : std_logic;
SIGNAL \CARD_NUMB~combout\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \LOCADD~combout\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \ff~combout\ : std_logic_vector(22 DOWNTO 0);
SIGNAL \wr_wrd~combout\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \199|ADDRESS\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \386|CLK_SEL\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \386|DILOFC\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \386|FC\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \386|FREQ_SEL\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \1|6|refg\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \1|2|20|41|lpm_counter_component|dffs\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \1|73|ADDRESS\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \1|73|COUNT1\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \1|27|5|count\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \88|FCO\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \339\ : std_logic_vector(17 DOWNTO 0);
SIGNAL \401\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \414\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \1|27|ALT_INV_7~regout\ : std_logic;
SIGNAL \ALT_INV_359~regout\ : std_logic;
SIGNAL \ALT_INV_357~regout\ : std_logic;
SIGNAL \ALT_INV_397~regout\ : std_logic;
SIGNAL \ALT_INV_399~regout\ : std_logic;
SIGNAL \ALT_INV_398~regout\ : std_logic;
SIGNAL \386|ALT_INV_SUB_COMP~regout\ : std_logic;
SIGNAL \ALT_INV_367~regout\ : std_logic;
SIGNAL \ALT_INV_360~regout\ : std_logic;
SIGNAL \88|ALT_INV_CHOOSER~regout\ : std_logic;
SIGNAL \88|ALT_INV_DATA_READ~regout\ : std_logic;
SIGNAL \88|ALT_INV_DATA_READ_FIFO~regout\ : std_logic;
SIGNAL \88|ALT_INV_DATA_WRITE_ON_FIFO~regout\ : std_logic;
SIGNAL \88|ALT_INV_IDLE~regout\ : std_logic;
SIGNAL \88|ALT_INV_DATA_WRITE~regout\ : std_logic;
SIGNAL \88|ALT_INV_ENIN1~2_combout\ : std_logic;
SIGNAL \88|ALT_INV_RESET_DASY~regout\ : std_logic;
SIGNAL \88|ALT_INV_RESET_DASY2~regout\ : std_logic;
SIGNAL \88|ALT_INV_FCO[0]~0_combout\ : std_logic;
SIGNAL \88|ALT_INV_READ_END~regout\ : std_logic;
SIGNAL \88|ALT_INV_LOAD_PED3~regout\ : std_logic;
SIGNAL \88|ALT_INV_READ_PED~regout\ : std_logic;
SIGNAL \88|ALT_INV_PED_MEAS~regout\ : std_logic;
SIGNAL \88|ALT_INV_ENIN1~3_combout\ : std_logic;
SIGNAL \1|ALT_INV_46~combout\ : std_logic;
SIGNAL \1|2|ALT_INV_49~regout\ : std_logic;
SIGNAL \1|2|ALT_INV_9~regout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLKA~regout\ : std_logic;
SIGNAL \1|2|ALT_INV_89~regout\ : std_logic;
SIGNAL \88|ALT_INV_READ_PED4~regout\ : std_logic;
SIGNAL \88|ALT_INV_PED_MEAS4~regout\ : std_logic;
SIGNAL \88|ALT_INV_LOAD_PED1~regout\ : std_logic;
SIGNAL \88|ALT_INV_LOAD_PULSE~regout\ : std_logic;
SIGNAL \386|ALT_INV_DILOFC\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \88|ALT_INV_CLEAR_FIFO~3_combout\ : std_logic;
SIGNAL \386|ALT_INV_FC\ : std_logic_vector(2 DOWNTO 1);
SIGNAL \ALT_INV_379~regout\ : std_logic;
SIGNAL \88|ALT_INV_LOAD_PED2~regout\ : std_logic;
SIGNAL \88|ALT_INV_CLEAR_FIFO~4_combout\ : std_logic;
SIGNAL \199|ALT_INV_DILO10~regout\ : std_logic;
SIGNAL \199|ALT_INV_DILO8~regout\ : std_logic;
SIGNAL \199|ALT_INV_DILO9~regout\ : std_logic;
SIGNAL \199|ALT_INV_DILO7~regout\ : std_logic;
SIGNAL \199|ALT_INV_DILO6~regout\ : std_logic;
SIGNAL \199|ALT_INV_DILO4~regout\ : std_logic;
SIGNAL \199|ALT_INV_DILO5~regout\ : std_logic;
SIGNAL \199|ALT_INV_DILO3~regout\ : std_logic;
SIGNAL \199|ALT_INV_DILO2~regout\ : std_logic;
SIGNAL \199|ALT_INV_DILO1~regout\ : std_logic;
SIGNAL \1|73|ALT_INV_ADDRESS\ : std_logic_vector(5 DOWNTO 0);
SIGNAL \88|ALT_INV_PED_MEAS3~regout\ : std_logic;
SIGNAL \88|ALT_INV_PED_MEAS2~regout\ : std_logic;
SIGNAL \88|ALT_INV_PED_MEAS1~regout\ : std_logic;
SIGNAL \88|ALT_INV_FIFO_WR_SEL~2_combout\ : std_logic;
SIGNAL \88|ALT_INV_FCO[2]~1_combout\ : std_logic;
SIGNAL \88|ALT_INV_FCO[1]~2_combout\ : std_logic;
SIGNAL \88|ALT_INV_READ_PED3~regout\ : std_logic;
SIGNAL \88|ALT_INV_READ_PED2~regout\ : std_logic;
SIGNAL \88|ALT_INV_READ_PED1~regout\ : std_logic;
SIGNAL \88|ALT_INV_FCO[0]~3_combout\ : std_logic;
SIGNAL \88|ALT_INV_FCO[0]~4_combout\ : std_logic;
SIGNAL \ALT_INV_371~regout\ : std_logic;
SIGNAL ALT_INV_414 : std_logic_vector(3 DOWNTO 0);
SIGNAL ALT_INV_339 : std_logic_vector(17 DOWNTO 0);
SIGNAL \88|ALT_INV_STRIN~4_combout\ : std_logic;
SIGNAL \1|73|ALT_INV_COUNT1\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \1|6|ALT_INV_refg\ : std_logic_vector(7 DOWNTO 5);
SIGNAL \1|73|ALT_INV_CLRADD$wire~0_combout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLRADD$wire~1_combout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLRADD$wire~combout\ : std_logic;
SIGNAL \1|2|ALT_INV_61~regout\ : std_logic;
SIGNAL \1|27|5|ALT_INV_count\ : std_logic_vector(7 DOWNTO 0);
SIGNAL \1|27|5|ALT_INV_BUSY_CLR~0_combout\ : std_logic;
SIGNAL \1|27|5|ALT_INV_BUSY_CLR~1_combout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLKD_a~0_combout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLR_G~0_combout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLRG_O~combout\ : std_logic;
SIGNAL ALT_INV_401 : std_logic_vector(3 DOWNTO 0);
SIGNAL \ALT_INV_378~regout\ : std_logic;
SIGNAL \ALT_INV_350~combout\ : std_logic;
SIGNAL \88|ALT_INV_STR_STRIN~regout\ : std_logic;
SIGNAL \88|ALT_INV__~0_combout\ : std_logic;
SIGNAL \88|ALT_INV_CHOOSER~0_combout\ : std_logic;
SIGNAL \386|ALT_INV_DATA_BUS~1_combout\ : std_logic;
SIGNAL \88|ALT_INV__~1_combout\ : std_logic;
SIGNAL \88|ALT_INV__~2_combout\ : std_logic;
SIGNAL \88|ALT_INV__~3_combout\ : std_logic;
SIGNAL \88|ALT_INV_DATA_WRITE~0_combout\ : std_logic;
SIGNAL \88|ALT_INV_RESET_DASY~0_combout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLKD~regout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLKD_O~combout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLKG~regout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLKG_O~combout\ : std_logic;
SIGNAL \88|ALT_INV_LOAD_PED~regout\ : std_logic;
SIGNAL \88|ALT_INV_DILO_SEL~0_combout\ : std_logic;
SIGNAL \199|ALT_INV_WAITING_STATE~regout\ : std_logic;
SIGNAL \386|ALT_INV_CLK_SEL\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \1|2|ALT_INV_81~regout\ : std_logic;
SIGNAL \1|2|20|ALT_INV_21~regout\ : std_logic;
SIGNAL \1|2|20|42|ALT_INV_DIVBY6~regout\ : std_logic;
SIGNAL \386|ALT_INV_FREQ_SEL\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \1|2|12|lpm_mux_component|$00009|ALT_INV_result_node~0_combout\ : std_logic;
SIGNAL \1|2|20|ALT_INV_22~regout\ : std_logic;
SIGNAL \1|2|20|ALT_INV_24~regout\ : std_logic;
SIGNAL \1|2|12|lpm_mux_component|$00009|ALT_INV_result_node~1_combout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLKA_a~0_combout\ : std_logic;
SIGNAL \ALT_INV_417~regout\ : std_logic;
SIGNAL \199|ALT_INV_IDLE~regout\ : std_logic;
SIGNAL \ALT_INV_286~0_combout\ : std_logic;
SIGNAL \ALT_INV_412~0_combout\ : std_logic;
SIGNAL \1|2|ALT_INV_85~regout\ : std_logic;
SIGNAL \1|ALT_INV_87~combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|ALT_INV_dffs\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \1|2|20|ALT_INV_23~regout\ : std_logic;
SIGNAL \1|73|ALT_INV_CLKG_a~0_combout\ : std_logic;
SIGNAL \1|2|ALT_INV_63~combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|add_sub|adder[0]|ALT_INV_result_node[3]~0_combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|$00038|ALT_INV_and_cascade[3]~0_combout\ : std_logic;
SIGNAL \1|2|20|41|lpm_counter_component|add_sub|adder[0]|ALT_INV_result_node[2]~1_combout\ : std_logic;
SIGNAL \1|2|20|42|ALT_INV_DIVBY6~1_combout\ : std_logic;
SIGNAL \1|ALT_INV_85~combout\ : std_logic;
SIGNAL \1|ALT_INV_74~combout\ : std_logic;
SIGNAL \1|ALT_INV_88~combout\ : std_logic;
SIGNAL \1|2|20|ALT_INV_48~combout\ : std_logic;
SIGNAL \1|2|20|ALT_INV_49~combout\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~0\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~1\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~2\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~3\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~4\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~5\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~6\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~7\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~8\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~9\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~10\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~11\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~12\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~13\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~14\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~15\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~16\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~17\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~18\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~19\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~20\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~21\ : std_logic;
SIGNAL \ALT_INV_LOCBUS~22\ : std_logic;
SIGNAL \ALT_INV_EN_OUT8_IN9~0\ : std_logic;
SIGNAL \ALT_INV_EN_OUT9_IN10~0\ : std_logic;
SIGNAL \ALT_INV_EN_OUT4_IN5~0\ : std_logic;
SIGNAL \ALT_INV_EN_OUT7_IN8~0\ : std_logic;
SIGNAL \ALT_INV_EN_OUT5_IN6~0\ : std_logic;
SIGNAL \ALT_INV_EN_OUT6_IN7~0\ : std_logic;
SIGNAL \ALT_INV_EN_OUT3_IN4~0\ : std_logic;
SIGNAL \ALT_INV_EN_OUT1_IN2~0\ : std_logic;
SIGNAL \ALT_INV_EN_OUT2_IN3~0\ : std_logic;
SIGNAL \ALT_INV_INHIBIT~combout\ : std_logic;
SIGNAL \ALT_INV_clockext~combout\ : std_logic;
SIGNAL \ALT_INV_RESETn~combout\ : std_logic;
SIGNAL \ALT_INV_LOC_CS~combout\ : std_logic;
SIGNAL \ALT_INV_LOC_RWn~combout\ : std_logic;
SIGNAL \ALT_INV_TRIG~combout\ : std_logic;
SIGNAL \ALT_INV_EMPTY_FIFO~combout\ : std_logic;
SIGNAL \ALT_INV_wr_wrd~combout\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \ALT_INV_EN_OUT10~combout\ : std_logic;
SIGNAL \ALT_INV_CLOCK_IN~combout\ : std_logic;
SIGNAL \ALT_INV_MACK1_5~combout\ : std_logic;
SIGNAL \ALT_INV_MACK6_10~combout\ : std_logic;
SIGNAL \ALT_INV_CARD_NUMB~combout\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \ALT_INV_ff~combout\ : std_logic_vector(22 DOWNTO 0);
SIGNAL \ALT_INV_clockext~clkctrl_outclk\ : std_logic;
SIGNAL \1|2|12|lpm_mux_component|$00009|ALT_INV_result_node~clkctrl_outclk\ : std_logic;
SIGNAL \ALT_INV_RESETn~clkctrl_outclk\ : std_logic;

BEGIN

READ_NEXT <= ww_READ_NEXT;
STRIN <= ww_STRIN;
CLRADD <= ww_CLRADD;
ww_CLOCK_IN <= CLOCK_IN;
ww_RESETn <= RESETn;
ww_TRIG <= TRIG;
ww_LOC_CS <= LOC_CS;
ww_LOC_RWn <= LOC_RWn;
ww_clockext <= clockext;
ww_LOCADD <= LOCADD;
BUSY <= ww_BUSY;
ww_INHIBIT <= INHIBIT;
ww_EN_OUT10 <= EN_OUT10;
ww_EMPTY_FIFO <= EMPTY_FIFO;
ww_wr_wrd <= wr_wrd;
CLRG <= ww_CLRG;
RSTn <= ww_RSTn;
CLRD <= ww_CLRD;
SUB_COMP <= ww_SUB_COMP;
TRIGN <= ww_TRIGN;
EN_IN1 <= ww_EN_IN1;
VTEST <= ww_VTEST;
CLKD <= ww_CLKD;
TRIG_DISABLE <= ww_TRIG_DISABLE;
DL_IN <= ww_DL_IN;
TRACK_HOLD <= ww_TRACK_HOLD;
CLOCK <= ww_CLOCK;
CLKADC <= ww_CLKADC;
CLKG <= ww_CLKG;
WR_FIFO <= ww_WR_FIFO;
WR_CLK <= ww_WR_CLK;
RD_FIFO <= ww_RD_FIFO;
RD_CLK <= ww_RD_CLK;
aclr <= ww_aclr;
ADDR_DILO <= ww_ADDR_DILO;
ww_MACK1_5 <= MACK1_5;
ww_MACK6_10 <= MACK6_10;
ADDRGX <= ww_ADDRGX;
ww_ff <= ff;
FC <= ww_FC;
FFIN <= ww_FFIN;
NBR_GASS <= ww_NBR_GASS;
ww_CARD_NUMB <= CARD_NUMB;
ww_spare2 <= spare2;
ww_spare1 <= spare1;
ww_START_READ <= START_READ;
ww_s_noAdata2 <= s_noAdata2;
ww_s_noAdata1 <= s_noAdata1;
ww_s_empty1 <= s_empty1;
ww_s_almfull1 <= s_almfull1;
ww_s_empty2 <= s_empty2;
ww_s_almfull2 <= s_almfull2;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\clockext~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \clockext~combout\);

\1|73|CLKD_O~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \1|73|CLKD_O~combout\);

\CLOCK_IN~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \CLOCK_IN~combout\);

\1|2|20|44~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \1|2|20|44~combout\);

\350~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \350~combout\);

\1|2|12|lpm_mux_component|$00009|result_node~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \1|2|12|lpm_mux_component|$00009|result_node~combout\);

\RESETn~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \RESETn~combout\);
\1|27|ALT_INV_7~regout\ <= NOT \1|27|7~regout\;
\ALT_INV_359~regout\ <= NOT \359~regout\;
\ALT_INV_357~regout\ <= NOT \357~regout\;
\ALT_INV_397~regout\ <= NOT \397~regout\;
\ALT_INV_399~regout\ <= NOT \399~regout\;
\ALT_INV_398~regout\ <= NOT \398~regout\;
\386|ALT_INV_SUB_COMP~regout\ <= NOT \386|SUB_COMP~regout\;
\ALT_INV_367~regout\ <= NOT \367~regout\;
\ALT_INV_360~regout\ <= NOT \360~regout\;
\88|ALT_INV_CHOOSER~regout\ <= NOT \88|CHOOSER~regout\;
\88|ALT_INV_DATA_READ~regout\ <= NOT \88|DATA_READ~regout\;
\88|ALT_INV_DATA_READ_FIFO~regout\ <= NOT \88|DATA_READ_FIFO~regout\;
\88|ALT_INV_DATA_WRITE_ON_FIFO~regout\ <= NOT \88|DATA_WRITE_ON_FIFO~regout\;
\88|ALT_INV_IDLE~regout\ <= NOT \88|IDLE~regout\;
\88|ALT_INV_DATA_WRITE~regout\ <= NOT \88|DATA_WRITE~regout\;
\88|ALT_INV_ENIN1~2_combout\ <= NOT \88|ENIN1~2_combout\;
\88|ALT_INV_RESET_DASY~regout\ <= NOT \88|RESET_DASY~regout\;
\88|ALT_INV_RESET_DASY2~regout\ <= NOT \88|RESET_DASY2~regout\;
\88|ALT_INV_FCO[0]~0_combout\ <= NOT \88|FCO[0]~0_combout\;
\88|ALT_INV_READ_END~regout\ <= NOT \88|READ_END~regout\;
\88|ALT_INV_LOAD_PED3~regout\ <= NOT \88|LOAD_PED3~regout\;
\88|ALT_INV_READ_PED~regout\ <= NOT \88|READ_PED~regout\;
\88|ALT_INV_PED_MEAS~regout\ <= NOT \88|PED_MEAS~regout\;
\88|ALT_INV_ENIN1~3_combout\ <= NOT \88|ENIN1~3_combout\;
\1|ALT_INV_46~combout\ <= NOT \1|46~combout\;
\1|2|ALT_INV_49~regout\ <= NOT \1|2|49~regout\;
\1|2|ALT_INV_9~regout\ <= NOT \1|2|9~regout\;
\1|73|ALT_INV_CLKA~regout\ <= NOT \1|73|CLKA~regout\;
\1|2|ALT_INV_89~regout\ <= NOT \1|2|89~regout\;
\88|ALT_INV_READ_PED4~regout\ <= NOT \88|READ_PED4~regout\;
\88|ALT_INV_PED_MEAS4~regout\ <= NOT \88|PED_MEAS4~regout\;
\88|ALT_INV_LOAD_PED1~regout\ <= NOT \88|LOAD_PED1~regout\;
\88|ALT_INV_LOAD_PULSE~regout\ <= NOT \88|LOAD_PULSE~regout\;
\386|ALT_INV_DILOFC\(3) <= NOT \386|DILOFC\(3);
\386|ALT_INV_DILOFC\(1) <= NOT \386|DILOFC\(1);
\386|ALT_INV_DILOFC\(2) <= NOT \386|DILOFC\(2);
\386|ALT_INV_DILOFC\(0) <= NOT \386|DILOFC\(0);
\88|ALT_INV_CLEAR_FIFO~3_combout\ <= NOT \88|CLEAR_FIFO~3_combout\;
\386|ALT_INV_FC\(1) <= NOT \386|FC\(1);
\386|ALT_INV_FC\(2) <= NOT \386|FC\(2);
\ALT_INV_379~regout\ <= NOT \379~regout\;
\88|ALT_INV_LOAD_PED2~regout\ <= NOT \88|LOAD_PED2~regout\;
\88|ALT_INV_CLEAR_FIFO~4_combout\ <= NOT \88|CLEAR_FIFO~4_combout\;
\199|ALT_INV_DILO10~regout\ <= NOT \199|DILO10~regout\;
\199|ALT_INV_DILO8~regout\ <= NOT \199|DILO8~regout\;
\199|ALT_INV_DILO9~regout\ <= NOT \199|DILO9~regout\;
\199|ALT_INV_DILO7~regout\ <= NOT \199|DILO7~regout\;
\199|ALT_INV_DILO6~regout\ <= NOT \199|DILO6~regout\;
\199|ALT_INV_DILO4~regout\ <= NOT \199|DILO4~regout\;
\199|ALT_INV_DILO5~regout\ <= NOT \199|DILO5~regout\;
\199|ALT_INV_DILO3~regout\ <= NOT \199|DILO3~regout\;
\199|ALT_INV_DILO2~regout\ <= NOT \199|DILO2~regout\;
\199|ALT_INV_DILO1~regout\ <= NOT \199|DILO1~regout\;
\1|73|ALT_INV_ADDRESS\(5) <= NOT \1|73|ADDRESS\(5);
\1|73|ALT_INV_ADDRESS\(4) <= NOT \1|73|ADDRESS\(4);
\1|73|ALT_INV_ADDRESS\(3) <= NOT \1|73|ADDRESS\(3);
\1|73|ALT_INV_ADDRESS\(2) <= NOT \1|73|ADDRESS\(2);
\1|73|ALT_INV_ADDRESS\(1) <= NOT \1|73|ADDRESS\(1);
\1|73|ALT_INV_ADDRESS\(0) <= NOT \1|73|ADDRESS\(0);
\88|ALT_INV_PED_MEAS3~regout\ <= NOT \88|PED_MEAS3~regout\;
\88|ALT_INV_PED_MEAS2~regout\ <= NOT \88|PED_MEAS2~regout\;
\88|ALT_INV_PED_MEAS1~regout\ <= NOT \88|PED_MEAS1~regout\;
\88|ALT_INV_FIFO_WR_SEL~2_combout\ <= NOT \88|FIFO_WR_SEL~2_combout\;
\88|ALT_INV_FCO[2]~1_combout\ <= NOT \88|FCO[2]~1_combout\;
\88|ALT_INV_FCO[1]~2_combout\ <= NOT \88|FCO[1]~2_combout\;
\88|ALT_INV_READ_PED3~regout\ <= NOT \88|READ_PED3~regout\;
\88|ALT_INV_READ_PED2~regout\ <= NOT \88|READ_PED2~regout\;
\88|ALT_INV_READ_PED1~regout\ <= NOT \88|READ_PED1~regout\;
\88|ALT_INV_FCO[0]~3_combout\ <= NOT \88|FCO[0]~3_combout\;
\88|ALT_INV_FCO[0]~4_combout\ <= NOT \88|FCO[0]~4_combout\;
\ALT_INV_371~regout\ <= NOT \371~regout\;
ALT_INV_414(3) <= NOT \414\(3);
ALT_INV_414(2) <= NOT \414\(2);
ALT_INV_414(1) <= NOT \414\(1);
ALT_INV_414(0) <= NOT \414\(0);
ALT_INV_339(17) <= NOT \339\(17);
ALT_INV_339(16) <= NOT \339\(16);
ALT_INV_339(15) <= NOT \339\(15);
ALT_INV_339(14) <= NOT \339\(14);
ALT_INV_339(13) <= NOT \339\(13);
ALT_INV_339(12) <= NOT \339\(12);
ALT_INV_339(11) <= NOT \339\(11);
ALT_INV_339(10) <= NOT \339\(10);
ALT_INV_339(9) <= NOT \339\(9);
ALT_INV_339(8) <= NOT \339\(8);
ALT_INV_339(7) <= NOT \339\(7);
ALT_INV_339(6) <= NOT \339\(6);
ALT_INV_339(5) <= NOT \339\(5);
ALT_INV_339(4) <= NOT \339\(4);
ALT_INV_339(3) <= NOT \339\(3);
ALT_INV_339(2) <= NOT \339\(2);
ALT_INV_339(1) <= NOT \339\(1);
ALT_INV_339(0) <= NOT \339\(0);
\88|ALT_INV_STRIN~4_combout\ <= NOT \88|STRIN~4_combout\;
\1|73|ALT_INV_COUNT1\(5) <= NOT \1|73|COUNT1\(5);
\1|6|ALT_INV_refg\(5) <= NOT \1|6|refg\(5);
\1|73|ALT_INV_COUNT1\(7) <= NOT \1|73|COUNT1\(7);
\1|6|ALT_INV_refg\(7) <= NOT \1|6|refg\(7);
\1|73|ALT_INV_COUNT1\(6) <= NOT \1|73|COUNT1\(6);
\1|6|ALT_INV_refg\(6) <= NOT \1|6|refg\(6);
\1|73|ALT_INV_CLRADD$wire~0_combout\ <= NOT \1|73|CLRADD$wire~0_combout\;
\1|73|ALT_INV_COUNT1\(2) <= NOT \1|73|COUNT1\(2);
\1|73|ALT_INV_COUNT1\(0) <= NOT \1|73|COUNT1\(0);
\1|73|ALT_INV_CLRADD$wire~1_combout\ <= NOT \1|73|CLRADD$wire~1_combout\;
\1|73|ALT_INV_COUNT1\(1) <= NOT \1|73|COUNT1\(1);
\1|73|ALT_INV_COUNT1\(4) <= NOT \1|73|COUNT1\(4);
\1|73|ALT_INV_COUNT1\(3) <= NOT \1|73|COUNT1\(3);
\1|73|ALT_INV_CLRADD$wire~combout\ <= NOT \1|73|CLRADD$wire~combout\;
\1|2|ALT_INV_61~regout\ <= NOT \1|2|61~regout\;
\1|27|5|ALT_INV_count\(2) <= NOT \1|27|5|count\(2);
\1|27|5|ALT_INV_count\(3) <= NOT \1|27|5|count\(3);
\1|27|5|ALT_INV_count\(4) <= NOT \1|27|5|count\(4);
\1|27|5|ALT_INV_count\(5) <= NOT \1|27|5|count\(5);
\1|27|5|ALT_INV_count\(7) <= NOT \1|27|5|count\(7);
\1|27|5|ALT_INV_count\(6) <= NOT \1|27|5|count\(6);
\1|27|5|ALT_INV_BUSY_CLR~0_combout\ <= NOT \1|27|5|BUSY_CLR~0_combout\;
\1|27|5|ALT_INV_count\(1) <= NOT \1|27|5|count\(1);
\1|27|5|ALT_INV_count\(0) <= NOT \1|27|5|count\(0);
\1|27|5|ALT_INV_BUSY_CLR~1_combout\ <= NOT \1|27|5|BUSY_CLR~1_combout\;
\1|73|ALT_INV_CLKD_a~0_combout\ <= NOT \1|73|CLKD_a~0_combout\;
\1|73|ALT_INV_CLR_G~0_combout\ <= NOT \1|73|CLR_G~0_combout\;
\1|73|ALT_INV_CLRG_O~combout\ <= NOT \1|73|CLRG_O~combout\;
ALT_INV_401(2) <= NOT \401\(2);
ALT_INV_401(1) <= NOT \401\(1);
ALT_INV_401(3) <= NOT \401\(3);
ALT_INV_401(0) <= NOT \401\(0);
\ALT_INV_378~regout\ <= NOT \378~regout\;
\ALT_INV_350~combout\ <= NOT \350~combout\;
\88|ALT_INV_STR_STRIN~regout\ <= NOT \88|STR_STRIN~regout\;
\88|ALT_INV__~0_combout\ <= NOT \88|_~0_combout\;
\88|ALT_INV_CHOOSER~0_combout\ <= NOT \88|CHOOSER~0_combout\;
\386|ALT_INV_DATA_BUS~1_combout\ <= NOT \386|DATA_BUS~1_combout\;
\88|ALT_INV__~1_combout\ <= NOT \88|_~1_combout\;
\88|ALT_INV__~2_combout\ <= NOT \88|_~2_combout\;
\88|ALT_INV__~3_combout\ <= NOT \88|_~3_combout\;
\88|ALT_INV_DATA_WRITE~0_combout\ <= NOT \88|DATA_WRITE~0_combout\;
\88|ALT_INV_RESET_DASY~0_combout\ <= NOT \88|RESET_DASY~0_combout\;
\1|73|ALT_INV_CLKD~regout\ <= NOT \1|73|CLKD~regout\;
\1|73|ALT_INV_CLKD_O~combout\ <= NOT \1|73|CLKD_O~combout\;
\1|73|ALT_INV_CLKG~regout\ <= NOT \1|73|CLKG~regout\;
\1|73|ALT_INV_CLKG_O~combout\ <= NOT \1|73|CLKG_O~combout\;
\88|ALT_INV_LOAD_PED~regout\ <= NOT \88|LOAD_PED~regout\;
\88|ALT_INV_DILO_SEL~0_combout\ <= NOT \88|DILO_SEL~0_combout\;
\199|ALT_INV_WAITING_STATE~regout\ <= NOT \199|WAITING_STATE~regout\;
\386|ALT_INV_CLK_SEL\(0) <= NOT \386|CLK_SEL\(0);
\386|ALT_INV_CLK_SEL\(1) <= NOT \386|CLK_SEL\(1);
\1|2|ALT_INV_81~regout\ <= NOT \1|2|81~regout\;
\1|2|20|ALT_INV_21~regout\ <= NOT \1|2|20|21~regout\;
\1|2|20|42|ALT_INV_DIVBY6~regout\ <= NOT \1|2|20|42|DIVBY6~regout\;
\386|ALT_INV_FREQ_SEL\(0) <= NOT \386|FREQ_SEL\(0);
\1|2|12|lpm_mux_component|$00009|ALT_INV_result_node~0_combout\ <= NOT \1|2|12|lpm_mux_component|$00009|result_node~0_combout\;
\1|2|20|ALT_INV_22~regout\ <= NOT \1|2|20|22~regout\;
\1|2|20|ALT_INV_24~regout\ <= NOT \1|2|20|24~regout\;
\1|2|12|lpm_mux_component|$00009|ALT_INV_result_node~1_combout\ <= NOT \1|2|12|lpm_mux_component|$00009|result_node~1_combout\;
\386|ALT_INV_FREQ_SEL\(1) <= NOT \386|FREQ_SEL\(1);
\1|73|ALT_INV_CLKA_a~0_combout\ <= NOT \1|73|CLKA_a~0_combout\;
\ALT_INV_417~regout\ <= NOT \417~regout\;
\199|ALT_INV_IDLE~regout\ <= NOT \199|IDLE~regout\;
\ALT_INV_286~0_combout\ <= NOT \286~0_combout\;
\ALT_INV_412~0_combout\ <= NOT \412~0_combout\;
\1|2|ALT_INV_85~regout\ <= NOT \1|2|85~regout\;
\1|ALT_INV_87~combout\ <= NOT \1|87~combout\;
\1|2|20|41|lpm_counter_component|ALT_INV_dffs\(3) <= NOT \1|2|20|41|lpm_counter_component|dffs\(3);
\1|2|20|41|lpm_counter_component|ALT_INV_dffs\(2) <= NOT \1|2|20|41|lpm_counter_component|dffs\(2);
\1|2|20|41|lpm_counter_component|ALT_INV_dffs\(1) <= NOT \1|2|20|41|lpm_counter_component|dffs\(1);
\1|2|20|41|lpm_counter_component|ALT_INV_dffs\(0) <= NOT \1|2|20|41|lpm_counter_component|dffs\(0);
\1|2|20|ALT_INV_23~regout\ <= NOT \1|2|20|23~regout\;
\1|73|ALT_INV_CLKG_a~0_combout\ <= NOT \1|73|CLKG_a~0_combout\;
\1|2|ALT_INV_63~combout\ <= NOT \1|2|63~combout\;
\1|2|20|41|lpm_counter_component|add_sub|adder[0]|ALT_INV_result_node[3]~0_combout\ <= NOT \1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[3]~0_combout\;
\1|2|20|41|lpm_counter_component|$00038|ALT_INV_and_cascade[3]~0_combout\ <= NOT \1|2|20|41|lpm_counter_component|$00038|and_cascade[3]~0_combout\;
\1|2|20|41|lpm_counter_component|add_sub|adder[0]|ALT_INV_result_node[2]~1_combout\ <= NOT \1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[2]~1_combout\;
\1|2|20|42|ALT_INV_DIVBY6~1_combout\ <= NOT \1|2|20|42|DIVBY6~1_combout\;
\1|ALT_INV_85~combout\ <= NOT \1|85~combout\;
\1|ALT_INV_74~combout\ <= NOT \1|74~combout\;
\1|ALT_INV_88~combout\ <= NOT \1|88~combout\;
\1|2|20|ALT_INV_48~combout\ <= NOT \1|2|20|48~combout\;
\1|2|20|ALT_INV_49~combout\ <= NOT \1|2|20|49~combout\;
\ALT_INV_LOCBUS~0\ <= NOT \LOCBUS~0\;
\ALT_INV_LOCBUS~1\ <= NOT \LOCBUS~1\;
\ALT_INV_LOCBUS~2\ <= NOT \LOCBUS~2\;
\ALT_INV_LOCBUS~3\ <= NOT \LOCBUS~3\;
\ALT_INV_LOCBUS~4\ <= NOT \LOCBUS~4\;
\ALT_INV_LOCBUS~5\ <= NOT \LOCBUS~5\;
\ALT_INV_LOCBUS~6\ <= NOT \LOCBUS~6\;
\ALT_INV_LOCBUS~7\ <= NOT \LOCBUS~7\;
\ALT_INV_LOCBUS~8\ <= NOT \LOCBUS~8\;
\ALT_INV_LOCBUS~9\ <= NOT \LOCBUS~9\;
\ALT_INV_LOCBUS~10\ <= NOT \LOCBUS~10\;
\ALT_INV_LOCBUS~11\ <= NOT \LOCBUS~11\;
\ALT_INV_LOCBUS~12\ <= NOT \LOCBUS~12\;
\ALT_INV_LOCBUS~13\ <= NOT \LOCBUS~13\;
\ALT_INV_LOCBUS~14\ <= NOT \LOCBUS~14\;
\ALT_INV_LOCBUS~15\ <= NOT \LOCBUS~15\;
\ALT_INV_LOCBUS~16\ <= NOT \LOCBUS~16\;
\ALT_INV_LOCBUS~17\ <= NOT \LOCBUS~17\;
\ALT_INV_LOCBUS~18\ <= NOT \LOCBUS~18\;
\ALT_INV_LOCBUS~19\ <= NOT \LOCBUS~19\;
\ALT_INV_LOCBUS~20\ <= NOT \LOCBUS~20\;
\ALT_INV_LOCBUS~21\ <= NOT \LOCBUS~21\;
\ALT_INV_LOCBUS~22\ <= NOT \LOCBUS~22\;
\ALT_INV_EN_OUT8_IN9~0\ <= NOT \EN_OUT8_IN9~0\;
\ALT_INV_EN_OUT9_IN10~0\ <= NOT \EN_OUT9_IN10~0\;
\ALT_INV_EN_OUT4_IN5~0\ <= NOT \EN_OUT4_IN5~0\;
\ALT_INV_EN_OUT7_IN8~0\ <= NOT \EN_OUT7_IN8~0\;
\ALT_INV_EN_OUT5_IN6~0\ <= NOT \EN_OUT5_IN6~0\;
\ALT_INV_EN_OUT6_IN7~0\ <= NOT \EN_OUT6_IN7~0\;
\ALT_INV_EN_OUT3_IN4~0\ <= NOT \EN_OUT3_IN4~0\;
\ALT_INV_EN_OUT1_IN2~0\ <= NOT \EN_OUT1_IN2~0\;
\ALT_INV_EN_OUT2_IN3~0\ <= NOT \EN_OUT2_IN3~0\;
\ALT_INV_INHIBIT~combout\ <= NOT \INHIBIT~combout\;
\ALT_INV_clockext~combout\ <= NOT \clockext~combout\;
\ALT_INV_RESETn~combout\ <= NOT \RESETn~combout\;
\ALT_INV_LOC_CS~combout\ <= NOT \LOC_CS~combout\;
\ALT_INV_LOC_RWn~combout\ <= NOT \LOC_RWn~combout\;
\ALT_INV_TRIG~combout\ <= NOT \TRIG~combout\;
\ALT_INV_EMPTY_FIFO~combout\ <= NOT \EMPTY_FIFO~combout\;
\ALT_INV_wr_wrd~combout\(4) <= NOT \wr_wrd~combout\(4);
\ALT_INV_wr_wrd~combout\(5) <= NOT \wr_wrd~combout\(5);
\ALT_INV_wr_wrd~combout\(6) <= NOT \wr_wrd~combout\(6);
\ALT_INV_wr_wrd~combout\(7) <= NOT \wr_wrd~combout\(7);
\ALT_INV_wr_wrd~combout\(9) <= NOT \wr_wrd~combout\(9);
\ALT_INV_wr_wrd~combout\(8) <= NOT \wr_wrd~combout\(8);
\ALT_INV_wr_wrd~combout\(0) <= NOT \wr_wrd~combout\(0);
\ALT_INV_wr_wrd~combout\(1) <= NOT \wr_wrd~combout\(1);
\ALT_INV_wr_wrd~combout\(3) <= NOT \wr_wrd~combout\(3);
\ALT_INV_wr_wrd~combout\(2) <= NOT \wr_wrd~combout\(2);
\ALT_INV_EN_OUT10~combout\ <= NOT \EN_OUT10~combout\;
\ALT_INV_CLOCK_IN~combout\ <= NOT \CLOCK_IN~combout\;
\ALT_INV_MACK1_5~combout\ <= NOT \MACK1_5~combout\;
\ALT_INV_MACK6_10~combout\ <= NOT \MACK6_10~combout\;
\ALT_INV_CARD_NUMB~combout\(0) <= NOT \CARD_NUMB~combout\(0);
\ALT_INV_ff~combout\(21) <= NOT \ff~combout\(21);
\ALT_INV_ff~combout\(20) <= NOT \ff~combout\(20);
\ALT_INV_ff~combout\(19) <= NOT \ff~combout\(19);
\ALT_INV_ff~combout\(18) <= NOT \ff~combout\(18);
\ALT_INV_ff~combout\(17) <= NOT \ff~combout\(17);
\ALT_INV_ff~combout\(16) <= NOT \ff~combout\(16);
\ALT_INV_ff~combout\(15) <= NOT \ff~combout\(15);
\ALT_INV_ff~combout\(14) <= NOT \ff~combout\(14);
\ALT_INV_ff~combout\(13) <= NOT \ff~combout\(13);
\ALT_INV_ff~combout\(12) <= NOT \ff~combout\(12);
\ALT_INV_ff~combout\(11) <= NOT \ff~combout\(11);
\ALT_INV_ff~combout\(10) <= NOT \ff~combout\(10);
\ALT_INV_ff~combout\(9) <= NOT \ff~combout\(9);
\ALT_INV_ff~combout\(8) <= NOT \ff~combout\(8);
\ALT_INV_ff~combout\(7) <= NOT \ff~combout\(7);
\ALT_INV_ff~combout\(6) <= NOT \ff~combout\(6);
\ALT_INV_ff~combout\(5) <= NOT \ff~combout\(5);
\ALT_INV_ff~combout\(4) <= NOT \ff~combout\(4);
\ALT_INV_ff~combout\(3) <= NOT \ff~combout\(3);
\ALT_INV_ff~combout\(2) <= NOT \ff~combout\(2);
\ALT_INV_ff~combout\(1) <= NOT \ff~combout\(1);
\ALT_INV_ff~combout\(0) <= NOT \ff~combout\(0);
\ALT_INV_ff~combout\(22) <= NOT \ff~combout\(22);
\ALT_INV_CARD_NUMB~combout\(4) <= NOT \CARD_NUMB~combout\(4);
\ALT_INV_CARD_NUMB~combout\(3) <= NOT \CARD_NUMB~combout\(3);
\ALT_INV_CARD_NUMB~combout\(2) <= NOT \CARD_NUMB~combout\(2);
\ALT_INV_CARD_NUMB~combout\(1) <= NOT \CARD_NUMB~combout\(1);
\ALT_INV_clockext~clkctrl_outclk\ <= NOT \clockext~clkctrl_outclk\;
\1|2|12|lpm_mux_component|$00009|ALT_INV_result_node~clkctrl_outclk\ <= NOT \1|2|12|lpm_mux_component|$00009|result_node~clkctrl_outclk\;
\ALT_INV_RESETn~clkctrl_outclk\ <= NOT \RESETn~clkctrl_outclk\;

-- Location: LCCOMB_X25_Y17_N10
\1|73|op_2~21\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_2~21_sumout\ = SUM(( \1|73|COUNT1\(6) ) + ( GND ) + ( \1|73|op_2~18\ ))
-- \1|73|op_2~22\ = CARRY(( \1|73|COUNT1\(6) ) + ( GND ) + ( \1|73|op_2~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_COUNT1\(6),
	cin => \1|73|op_2~18\,
	sumout => \1|73|op_2~21_sumout\,
	cout => \1|73|op_2~22\);

-- Location: LCCOMB_X25_Y17_N12
\1|73|op_2~25\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_2~25_sumout\ = SUM(( \1|73|COUNT1\(7) ) + ( GND ) + ( \1|73|op_2~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_COUNT1\(7),
	cin => \1|73|op_2~22\,
	sumout => \1|73|op_2~25_sumout\);

-- Location: LCFF_X25_Y17_N13
\1|73|COUNT1[7]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|12|lpm_mux_component|$00009|result_node~combout\,
	datain => \1|73|op_2~25_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|2|61~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|COUNT1\(7));

-- Location: LCFF_X22_Y16_N9
\1|2|20|21\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|63~combout\,
	datain => \1|2|20|21~0_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|20|21~regout\);

-- Location: LCFF_X22_Y18_N9
\1|2|20|42|DIVBY6\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|20|44~combout\,
	datain => \1|2|20|42|DIVBY6~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|20|42|DIVBY6~regout\);

-- Location: LCCOMB_X21_Y16_N24
\1|2|12|lpm_mux_component|$00009|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|12|lpm_mux_component|$00009|result_node~0_combout\ = ( \1|2|20|42|DIVBY6~regout\ & ( (!\1|2|20|21~regout\ & !\386|FREQ_SEL\(0)) ) ) # ( !\1|2|20|42|DIVBY6~regout\ & ( (!\1|2|20|21~regout\) # (\386|FREQ_SEL\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111111001111110011111100111111000000110000001100000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \1|2|20|ALT_INV_21~regout\,
	datac => \386|ALT_INV_FREQ_SEL\(0),
	dataf => \1|2|20|42|ALT_INV_DIVBY6~regout\,
	combout => \1|2|12|lpm_mux_component|$00009|result_node~0_combout\);

-- Location: LCFF_X22_Y16_N11
\1|2|20|22\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|63~combout\,
	datain => \1|2|20|22~0_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|20|22~regout\);

-- Location: LCFF_X22_Y16_N29
\1|2|20|24\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|63~combout\,
	datain => \1|2|20|24~0_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|20|24~regout\);

-- Location: LCCOMB_X21_Y16_N26
\1|2|12|lpm_mux_component|$00009|result_node~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|12|lpm_mux_component|$00009|result_node~1_combout\ = ( \1|2|20|22~regout\ & ( (!\1|2|20|24~regout\ & \386|FREQ_SEL\(0)) ) ) # ( !\1|2|20|22~regout\ & ( (!\1|2|20|24~regout\) # (!\386|FREQ_SEL\(0)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111110000111111111111000000000000111100000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \1|2|20|ALT_INV_24~regout\,
	datad => \386|ALT_INV_FREQ_SEL\(0),
	dataf => \1|2|20|ALT_INV_22~regout\,
	combout => \1|2|12|lpm_mux_component|$00009|result_node~1_combout\);

-- Location: LCCOMB_X21_Y14_N22
\1|2|12|lpm_mux_component|$00009|result_node\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|12|lpm_mux_component|$00009|result_node~combout\ = LCELL(( \1|2|12|lpm_mux_component|$00009|result_node~0_combout\ & ( (!\1|2|12|lpm_mux_component|$00009|result_node~1_combout\ & \386|FREQ_SEL\(1)) ) ) # ( 
-- !\1|2|12|lpm_mux_component|$00009|result_node~0_combout\ & ( (!\1|2|12|lpm_mux_component|$00009|result_node~1_combout\) # (!\386|FREQ_SEL\(1)) ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111110000111111111111000000000000111100000000000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \1|2|12|lpm_mux_component|$00009|ALT_INV_result_node~1_combout\,
	datad => \386|ALT_INV_FREQ_SEL\(1),
	dataf => \1|2|12|lpm_mux_component|$00009|ALT_INV_result_node~0_combout\,
	combout => \1|2|12|lpm_mux_component|$00009|result_node~combout\);

-- Location: LCCOMB_X25_Y17_N24
\1|73|CLKA_a~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKA_a~0_combout\ = ( !\1|73|COUNT1\(6) & ( !\1|73|COUNT1\(5) & ( (!\1|73|COUNT1\(3) & (!\1|73|COUNT1\(0) & (!\1|73|COUNT1\(4) & !\1|73|COUNT1\(7)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|73|ALT_INV_COUNT1\(3),
	datab => \1|73|ALT_INV_COUNT1\(0),
	datac => \1|73|ALT_INV_COUNT1\(4),
	datad => \1|73|ALT_INV_COUNT1\(7),
	datae => \1|73|ALT_INV_COUNT1\(6),
	dataf => \1|73|ALT_INV_COUNT1\(5),
	combout => \1|73|CLKA_a~0_combout\);

-- Location: LCCOMB_X23_Y17_N20
\1|73|CLKA_a\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKA_a~combout\ = LCELL(( \1|73|COUNT1\(2) & ( (\1|73|CLKA_a~0_combout\ & !\1|73|COUNT1\(1)) ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000000000000111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \1|73|ALT_INV_CLKA_a~0_combout\,
	datad => \1|73|ALT_INV_COUNT1\(1),
	dataf => \1|73|ALT_INV_COUNT1\(2),
	combout => \1|73|CLKA_a~combout\);

-- Location: LCCOMB_X23_Y17_N12
\1|73|CLKD_a\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKD_a~combout\ = LCELL(( !\1|73|COUNT1\(5) & ( !\1|73|COUNT1\(6) & ( (!\1|73|COUNT1\(4) & (!\1|73|COUNT1\(1) & (!\1|73|COUNT1\(7) & \1|73|CLKD_a~0_combout\))) ) ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|73|ALT_INV_COUNT1\(4),
	datab => \1|73|ALT_INV_COUNT1\(1),
	datac => \1|73|ALT_INV_COUNT1\(7),
	datad => \1|73|ALT_INV_CLKD_a~0_combout\,
	datae => \1|73|ALT_INV_COUNT1\(5),
	dataf => \1|73|ALT_INV_COUNT1\(6),
	combout => \1|73|CLKD_a~combout\);

-- Location: LCFF_X22_Y18_N27
\1|2|20|41|lpm_counter_component|dffs[3]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|20|44~clkctrl_outclk\,
	datain => \1|2|20|41|lpm_counter_component|dffs[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|20|41|lpm_counter_component|dffs\(3));

-- Location: LCFF_X22_Y18_N25
\1|2|20|41|lpm_counter_component|dffs[2]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|20|44~clkctrl_outclk\,
	datain => \1|2|20|41|lpm_counter_component|dffs[2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|20|41|lpm_counter_component|dffs\(2));

-- Location: LCFF_X22_Y18_N21
\1|2|20|41|lpm_counter_component|dffs[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|20|44~clkctrl_outclk\,
	datain => \1|2|20|41|lpm_counter_component|dffs[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|20|41|lpm_counter_component|dffs\(1));

-- Location: LCFF_X22_Y18_N7
\1|2|20|41|lpm_counter_component|dffs[0]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|20|44~clkctrl_outclk\,
	datain => \1|2|20|41|lpm_counter_component|dffs[0]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|20|41|lpm_counter_component|dffs\(0));

-- Location: LCCOMB_X22_Y18_N8
\1|2|20|42|DIVBY6~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|42|DIVBY6~0_combout\ = ( \1|2|20|41|lpm_counter_component|dffs\(1) & ( (\1|2|20|41|lpm_counter_component|dffs\(2) & (!\1|2|20|41|lpm_counter_component|dffs\(3) & !\1|2|20|41|lpm_counter_component|dffs\(0))) ) ) # ( 
-- !\1|2|20|41|lpm_counter_component|dffs\(1) & ( (!\1|2|20|41|lpm_counter_component|dffs\(3) & ((!\1|2|20|41|lpm_counter_component|dffs\(2)) # (\1|2|20|41|lpm_counter_component|dffs\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011110000110000001111000000110000000000000011000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(2),
	datac => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(3),
	datad => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(0),
	dataf => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(1),
	combout => \1|2|20|42|DIVBY6~0_combout\);

-- Location: LCCOMB_X22_Y16_N10
\1|2|20|22~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|22~0_combout\ = ( \1|2|20|21~regout\ & ( !\1|2|20|22~regout\ ) ) # ( !\1|2|20|21~regout\ & ( \1|2|20|22~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|2|20|ALT_INV_22~regout\,
	dataf => \1|2|20|ALT_INV_21~regout\,
	combout => \1|2|20|22~0_combout\);

-- Location: LCFF_X22_Y16_N31
\1|2|20|23\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|63~combout\,
	datain => \1|2|20|23~0_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|20|23~regout\);

-- Location: LCCOMB_X22_Y16_N28
\1|2|20|24~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|24~0_combout\ = ( \1|2|20|23~regout\ & ( !\1|2|20|24~regout\ $ (((!\1|2|20|22~regout\) # (!\1|2|20|21~regout\))) ) ) # ( !\1|2|20|23~regout\ & ( \1|2|20|24~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000011111111000000001111111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \1|2|20|ALT_INV_22~regout\,
	datac => \1|2|20|ALT_INV_21~regout\,
	datad => \1|2|20|ALT_INV_24~regout\,
	dataf => \1|2|20|ALT_INV_23~regout\,
	combout => \1|2|20|24~0_combout\);

-- Location: LCCOMB_X23_Y17_N24
\1|73|CLKG_a~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKG_a~0_combout\ = (!\1|73|COUNT1\(2) & \1|73|COUNT1\(1))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000011000000110000001100000011000000110000001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \1|73|ALT_INV_COUNT1\(2),
	datac => \1|73|ALT_INV_COUNT1\(1),
	combout => \1|73|CLKG_a~0_combout\);

-- Location: LCCOMB_X23_Y17_N8
\1|73|CLKG_a\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKG_a~combout\ = LCELL(( \1|73|CLKG_a~0_combout\ & ( \1|73|CLRADD$wire~1_combout\ & ( (\1|73|CLKA_a~0_combout\ & (((!\1|73|CLRADD$wire~0_combout\) # (!\1|73|CLR_G~0_combout\)) # (\1|73|COUNT1\(3)))) ) ) ) # ( \1|73|CLKG_a~0_combout\ & ( 
-- !\1|73|CLRADD$wire~1_combout\ & ( \1|73|CLKA_a~0_combout\ ) ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000111100001101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|73|ALT_INV_COUNT1\(3),
	datab => \1|73|ALT_INV_CLRADD$wire~0_combout\,
	datac => \1|73|ALT_INV_CLKA_a~0_combout\,
	datad => \1|73|ALT_INV_CLR_G~0_combout\,
	datae => \1|73|ALT_INV_CLKG_a~0_combout\,
	dataf => \1|73|ALT_INV_CLRADD$wire~1_combout\,
	combout => \1|73|CLKG_a~combout\);

-- Location: LCCOMB_X21_Y17_N22
\1|2|63\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|63~combout\ = LCELL(( \1|2|61~regout\ & ( !\CLOCK_IN~combout\ ) ) # ( !\1|2|61~regout\ & ( (!\CLOCK_IN~combout\ & \1|2|49~regout\) ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_CLOCK_IN~combout\,
	datad => \1|2|ALT_INV_49~regout\,
	dataf => \1|2|ALT_INV_61~regout\,
	combout => \1|2|63~combout\);

-- Location: LCCOMB_X22_Y18_N4
\1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[3]~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[3]~0_combout\ = ( \1|2|20|41|lpm_counter_component|dffs\(1) & ( !\1|2|20|41|lpm_counter_component|dffs\(3) $ (((!\1|2|20|41|lpm_counter_component|dffs\(0)) # 
-- (!\1|2|20|41|lpm_counter_component|dffs\(2)))) ) ) # ( !\1|2|20|41|lpm_counter_component|dffs\(1) & ( \1|2|20|41|lpm_counter_component|dffs\(3) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000011111111000000001111111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(0),
	datac => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(2),
	datad => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(3),
	dataf => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(1),
	combout => \1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[3]~0_combout\);

-- Location: LCCOMB_X22_Y18_N10
\1|2|20|41|lpm_counter_component|$00038|and_cascade[3]~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|41|lpm_counter_component|$00038|and_cascade[3]~0_combout\ = ( !\1|2|20|41|lpm_counter_component|dffs\(1) & ( (!\1|2|20|41|lpm_counter_component|dffs\(2) & (\1|2|20|41|lpm_counter_component|dffs\(0) & \1|2|20|41|lpm_counter_component|dffs\(3))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001010000000000000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(2),
	datac => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(0),
	datad => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(3),
	dataf => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(1),
	combout => \1|2|20|41|lpm_counter_component|$00038|and_cascade[3]~0_combout\);

-- Location: LCCOMB_X22_Y18_N26
\1|2|20|41|lpm_counter_component|dffs[3]~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|41|lpm_counter_component|dffs[3]~0_combout\ = ( \1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[3]~0_combout\ & ( \RESETn~combout\ & ( (!\1|2|20|41|lpm_counter_component|$00038|and_cascade[3]~0_combout\ & 
-- ((!\1|27|5|BUSY_CLR~0_combout\) # ((!\1|27|5|BUSY_CLR~1_combout\) # (\1|27|5|count\(2))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111000010110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|27|5|ALT_INV_BUSY_CLR~0_combout\,
	datab => \1|27|5|ALT_INV_count\(2),
	datac => \1|2|20|41|lpm_counter_component|$00038|ALT_INV_and_cascade[3]~0_combout\,
	datad => \1|27|5|ALT_INV_BUSY_CLR~1_combout\,
	datae => \1|2|20|41|lpm_counter_component|add_sub|adder[0]|ALT_INV_result_node[3]~0_combout\,
	dataf => \ALT_INV_RESETn~combout\,
	combout => \1|2|20|41|lpm_counter_component|dffs[3]~0_combout\);

-- Location: LCCOMB_X22_Y18_N2
\1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[2]~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[2]~1_combout\ = ( \1|2|20|41|lpm_counter_component|dffs\(0) & ( !\1|2|20|41|lpm_counter_component|dffs\(2) $ (!\1|2|20|41|lpm_counter_component|dffs\(1)) ) ) # ( 
-- !\1|2|20|41|lpm_counter_component|dffs\(0) & ( \1|2|20|41|lpm_counter_component|dffs\(2) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101101010100101010110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(2),
	datad => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(1),
	dataf => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(0),
	combout => \1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[2]~1_combout\);

-- Location: LCCOMB_X22_Y18_N24
\1|2|20|41|lpm_counter_component|dffs[2]~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|41|lpm_counter_component|dffs[2]~1_combout\ = ( \1|2|20|41|lpm_counter_component|add_sub|adder[0]|result_node[2]~1_combout\ & ( \RESETn~combout\ & ( (!\1|2|20|41|lpm_counter_component|$00038|and_cascade[3]~0_combout\ & 
-- ((!\1|27|5|BUSY_CLR~0_combout\) # ((!\1|27|5|BUSY_CLR~1_combout\) # (\1|27|5|count\(2))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001111101100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|27|5|ALT_INV_BUSY_CLR~0_combout\,
	datab => \1|27|5|ALT_INV_count\(2),
	datac => \1|27|5|ALT_INV_BUSY_CLR~1_combout\,
	datad => \1|2|20|41|lpm_counter_component|$00038|ALT_INV_and_cascade[3]~0_combout\,
	datae => \1|2|20|41|lpm_counter_component|add_sub|adder[0]|ALT_INV_result_node[2]~1_combout\,
	dataf => \ALT_INV_RESETn~combout\,
	combout => \1|2|20|41|lpm_counter_component|dffs[2]~1_combout\);

-- Location: LCCOMB_X22_Y18_N0
\1|2|20|42|DIVBY6~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|42|DIVBY6~1_combout\ = ( \1|2|20|41|lpm_counter_component|dffs\(0) & ( !\1|2|20|41|lpm_counter_component|dffs\(1) ) ) # ( !\1|2|20|41|lpm_counter_component|dffs\(0) & ( \1|2|20|41|lpm_counter_component|dffs\(1) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(1),
	dataf => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(0),
	combout => \1|2|20|42|DIVBY6~1_combout\);

-- Location: LCCOMB_X22_Y18_N20
\1|2|20|41|lpm_counter_component|dffs[1]~2\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|41|lpm_counter_component|dffs[1]~2_combout\ = ( \1|2|20|42|DIVBY6~1_combout\ & ( !\1|2|20|41|lpm_counter_component|$00038|and_cascade[3]~0_combout\ & ( (\RESETn~combout\ & ((!\1|27|5|BUSY_CLR~1_combout\) # ((!\1|27|5|BUSY_CLR~0_combout\) # 
-- (\1|27|5|count\(2))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000001111101100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|27|5|ALT_INV_BUSY_CLR~1_combout\,
	datab => \1|27|5|ALT_INV_count\(2),
	datac => \1|27|5|ALT_INV_BUSY_CLR~0_combout\,
	datad => \ALT_INV_RESETn~combout\,
	datae => \1|2|20|42|ALT_INV_DIVBY6~1_combout\,
	dataf => \1|2|20|41|lpm_counter_component|$00038|ALT_INV_and_cascade[3]~0_combout\,
	combout => \1|2|20|41|lpm_counter_component|dffs[1]~2_combout\);

-- Location: LCCOMB_X22_Y18_N6
\1|2|20|41|lpm_counter_component|dffs[0]~3\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|41|lpm_counter_component|dffs[0]~3_combout\ = ( \RESETn~combout\ & ( (!\1|2|20|41|lpm_counter_component|dffs\(0) & ((!\1|27|5|BUSY_CLR~1_combout\) # ((!\1|27|5|BUSY_CLR~0_combout\) # (\1|27|5|count\(2))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011001000110011001100100011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|27|5|ALT_INV_BUSY_CLR~1_combout\,
	datab => \1|2|20|41|lpm_counter_component|ALT_INV_dffs\(0),
	datac => \1|27|5|ALT_INV_BUSY_CLR~0_combout\,
	datad => \1|27|5|ALT_INV_count\(2),
	dataf => \ALT_INV_RESETn~combout\,
	combout => \1|2|20|41|lpm_counter_component|dffs[0]~3_combout\);

-- Location: LCCOMB_X21_Y17_N10
\1|2|20|44\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|44~combout\ = LCELL(( \1|2|20|48~combout\ & ( !\1|2|63~combout\ ) ) # ( !\1|2|20|48~combout\ & ( \1|2|63~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|2|ALT_INV_63~combout\,
	dataf => \1|2|20|ALT_INV_48~combout\,
	combout => \1|2|20|44~combout\);

-- Location: LCCOMB_X22_Y16_N30
\1|2|20|23~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|23~0_combout\ = ( \1|2|20|21~regout\ & ( !\1|2|20|22~regout\ $ (!\1|2|20|23~regout\) ) ) # ( !\1|2|20|21~regout\ & ( \1|2|20|23~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100001111111100000000111111110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \1|2|20|ALT_INV_22~regout\,
	datad => \1|2|20|ALT_INV_23~regout\,
	dataf => \1|2|20|ALT_INV_21~regout\,
	combout => \1|2|20|23~0_combout\);

-- Location: LCCOMB_X22_Y16_N8
\1|2|20|21~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|21~0_combout\ = !\1|2|20|21~regout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|2|20|ALT_INV_21~regout\,
	combout => \1|2|20|21~0_combout\);

-- Location: LCCOMB_X21_Y17_N28
\1|2|20|48\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|48~combout\ = LCELL(( !\1|2|20|49~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \1|2|20|ALT_INV_49~combout\,
	combout => \1|2|20|48~combout\);

-- Location: LCCOMB_X21_Y17_N30
\1|2|20|49\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|20|49~combout\ = LCELL(( \1|2|63~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \1|2|ALT_INV_63~combout\,
	combout => \1|2|20|49~combout\);

-- Location: PIN_M21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLOCK_IN~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CLOCK_IN,
	combout => \CLOCK_IN~combout\);

-- Location: CLKCTRL_G1
\CLOCK_IN~clkctrl\ : stratixii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock")
-- pragma translate_on
PORT MAP (
	inclk => \CLOCK_IN~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \CLOCK_IN~clkctrl_outclk\);

-- Location: CLKCTRL_G15
\1|2|20|44~clkctrl\ : stratixii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock")
-- pragma translate_on
PORT MAP (
	inclk => \1|2|20|44~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \1|2|20|44~clkctrl_outclk\);

-- Location: CLKCTRL_G0
\350~clkctrl\ : stratixii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock")
-- pragma translate_on
PORT MAP (
	inclk => \350~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \350~clkctrl_outclk\);

-- Location: CLKCTRL_G10
\1|2|12|lpm_mux_component|$00009|result_node~clkctrl\ : stratixii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock")
-- pragma translate_on
PORT MAP (
	inclk => \1|2|12|lpm_mux_component|$00009|result_node~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \1|2|12|lpm_mux_component|$00009|result_node~clkctrl_outclk\);

-- Location: PIN_B12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[22]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[6]~12_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(22),
	combout => \LOCBUS~0\);

-- Location: PIN_Y17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[21]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[5]~14_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(21),
	combout => \LOCBUS~1\);

-- Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\LOCBUS[20]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[4]~15_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(20),
	combout => \LOCBUS~2\);

-- Location: PIN_H11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[19]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[3]~16_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(19),
	combout => \LOCBUS~3\);

-- Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\LOCBUS[18]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[2]~17_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(18),
	combout => \LOCBUS~4\);

-- Location: PIN_C14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[17]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[1]~18_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(17),
	combout => \LOCBUS~5\);

-- Location: PIN_B5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[16]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[0]~19_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(16),
	combout => \LOCBUS~6\);

-- Location: PIN_J7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\LOCBUS[15]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|50[3]~4_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(15),
	combout => \LOCBUS~7\);

-- Location: PIN_V11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[14]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|50[2]~5_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(14),
	combout => \LOCBUS~8\);

-- Location: PIN_G9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[13]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|50[1]~6_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(13),
	combout => \LOCBUS~9\);

-- Location: PIN_G15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[12]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|50[0]~7_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(12),
	combout => \LOCBUS~10\);

-- Location: PIN_C16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[11]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|43~1_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(11),
	combout => \LOCBUS~11\);

-- Location: PIN_J18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\LOCBUS[10]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|48~1_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(10),
	combout => \LOCBUS~12\);

-- Location: PIN_H12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[9]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \271[9]~0_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(9),
	combout => \LOCBUS~13\);

-- Location: PIN_AB10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[8]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|44[0]~2_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(8),
	combout => \LOCBUS~14\);

-- Location: PIN_B10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[7]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|45[3]~4_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(7),
	combout => \LOCBUS~15\);

-- Location: PIN_K4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\LOCBUS[6]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|45[2]~5_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(6),
	combout => \LOCBUS~16\);

-- Location: PIN_AB13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[5]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|45[1]~6_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(5),
	combout => \LOCBUS~17\);

-- Location: PIN_Y13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[4]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|45[0]~7_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(4),
	combout => \LOCBUS~18\);

-- Location: PIN_C12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[3]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|47[1]~2_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(3),
	combout => \LOCBUS~19\);

-- Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\LOCBUS[2]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|47[0]~3_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(2),
	combout => \LOCBUS~20\);

-- Location: PIN_P8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\LOCBUS[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|46[1]~2_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(1),
	combout => \LOCBUS~21\);

-- Location: PIN_A13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|46[0]~3_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(0),
	combout => \LOCBUS~22\);

-- Location: PIN_K3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\EN_OUT8_IN9~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => VCC,
	oe => \46|52~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => EN_OUT8_IN9,
	combout => \EN_OUT8_IN9~0\);

-- Location: PIN_W11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\EN_OUT9_IN10~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => VCC,
	oe => \46|51~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => EN_OUT9_IN10,
	combout => \EN_OUT9_IN10~0\);

-- Location: PIN_J20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\EN_OUT4_IN5~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => VCC,
	oe => \46|56~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => EN_OUT4_IN5,
	combout => \EN_OUT4_IN5~0\);

-- Location: PIN_N7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\EN_OUT7_IN8~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => VCC,
	oe => \46|53~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => EN_OUT7_IN8,
	combout => \EN_OUT7_IN8~0\);

-- Location: PIN_L21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\EN_OUT5_IN6~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => VCC,
	oe => \46|55~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => EN_OUT5_IN6,
	combout => \EN_OUT5_IN6~0\);

-- Location: PIN_A15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\EN_OUT6_IN7~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => VCC,
	oe => \46|54~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => EN_OUT6_IN7,
	combout => \EN_OUT6_IN7~0\);

-- Location: PIN_P16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\EN_OUT3_IN4~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => VCC,
	oe => \46|57~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => EN_OUT3_IN4,
	combout => \EN_OUT3_IN4~0\);

-- Location: PIN_B8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\EN_OUT1_IN2~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => VCC,
	oe => \46|48~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => EN_OUT1_IN2,
	combout => \EN_OUT1_IN2~0\);

-- Location: PIN_AA11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\EN_OUT2_IN3~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => VCC,
	oe => \46|49~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => EN_OUT2_IN3,
	combout => \EN_OUT2_IN3~0\);

-- Location: PIN_Y5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[17]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(17),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(17),
	combout => \DILOBUS~0\);

-- Location: PIN_C7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[16]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(16),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(16),
	combout => \DILOBUS~1\);

-- Location: PIN_D8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[15]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(15),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(15),
	combout => \DILOBUS~2\);

-- Location: PIN_C18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[14]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(14),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(14),
	combout => \DILOBUS~3\);

-- Location: PIN_H4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\DILOBUS[13]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(13),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(13),
	combout => \DILOBUS~4\);

-- Location: PIN_H22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\DILOBUS[12]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(12),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(12),
	combout => \DILOBUS~5\);

-- Location: PIN_V14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[11]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(11),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(11),
	combout => \DILOBUS~6\);

-- Location: PIN_H14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[10]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(10),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(10),
	combout => \DILOBUS~7\);

-- Location: PIN_AB15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[9]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(9),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(9),
	combout => \DILOBUS~8\);

-- Location: PIN_C17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[8]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(8),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(8),
	combout => \DILOBUS~9\);

-- Location: PIN_W9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[7]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(7),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(7),
	combout => \DILOBUS~10\);

-- Location: PIN_K17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\DILOBUS[6]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(6),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(6),
	combout => \DILOBUS~11\);

-- Location: PIN_C19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[5]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(5),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(5),
	combout => \DILOBUS~12\);

-- Location: PIN_F16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[4]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(4),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(4),
	combout => \DILOBUS~13\);

-- Location: PIN_Y10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[3]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(3),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(3),
	combout => \DILOBUS~14\);

-- Location: PIN_K15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\DILOBUS[2]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(2),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(2),
	combout => \DILOBUS~15\);

-- Location: PIN_G5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\DILOBUS[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(1),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(1),
	combout => \DILOBUS~16\);

-- Location: PIN_AB7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\DILOBUS[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ff~combout\(0),
	oe => \88|DILO_SEL~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => DILOBUS(0),
	combout => \DILOBUS~17\);

-- Location: PIN_H5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\LOC_CS~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_LOC_CS,
	combout => \LOC_CS~combout\);

-- Location: PIN_P18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\LOCADD[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_LOCADD(1),
	combout => \LOCADD~combout\(1));

-- Location: LCFF_X21_Y15_N15
\401[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \LOCADD~combout\(1),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \401\(1));

-- Location: PIN_C8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\LOC_RWn~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_LOC_RWn,
	combout => \LOC_RWn~combout\);

-- Location: PIN_A7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\LOCADD[2]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_LOCADD(2),
	combout => \LOCADD~combout\(2));

-- Location: LCFF_X21_Y15_N21
\401[2]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \LOCADD~combout\(2),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \401\(2));

-- Location: PIN_K18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\LOCADD[3]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_LOCADD(3),
	combout => \LOCADD~combout\(3));

-- Location: LCFF_X21_Y15_N27
\401[3]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \LOCADD~combout\(3),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \401\(3));

-- Location: LCCOMB_X21_Y15_N28
\412~0\ : stratixii_lcell_comb
-- Equation(s):
-- \412~0_combout\ = ( !\401\(2) & ( !\401\(3) & ( (\401\(0) & (\LOC_CS~combout\ & (!\401\(1) & !\LOC_RWn~combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_401(0),
	datab => \ALT_INV_LOC_CS~combout\,
	datac => ALT_INV_401(1),
	datad => \ALT_INV_LOC_RWn~combout\,
	datae => ALT_INV_401(2),
	dataf => ALT_INV_401(3),
	combout => \412~0_combout\);

-- Location: LCCOMB_X21_Y15_N0
\386|FC[2]~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \386|FC[2]~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \386|FC[2]~feeder_combout\);

-- Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\RESETn~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_RESETn,
	combout => \RESETn~combout\);

-- Location: PIN_H17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\wr_wrd[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr_wrd(1),
	combout => \wr_wrd~combout\(1));

-- Location: PIN_E11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\wr_wrd[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr_wrd(0),
	combout => \wr_wrd~combout\(0));

-- Location: PIN_J5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\wr_wrd[2]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr_wrd(2),
	combout => \wr_wrd~combout\(2));

-- Location: LCCOMB_X18_Y17_N24
\88|_~3\ : stratixii_lcell_comb
-- Equation(s):
-- \88|_~3_combout\ = ( !\wr_wrd~combout\(2) & ( (!\wr_wrd~combout\(3) & (!\wr_wrd~combout\(1) & !\wr_wrd~combout\(0))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000100000001000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_wr_wrd~combout\(3),
	datab => \ALT_INV_wr_wrd~combout\(1),
	datac => \ALT_INV_wr_wrd~combout\(0),
	dataf => \ALT_INV_wr_wrd~combout\(2),
	combout => \88|_~3_combout\);

-- Location: LCCOMB_X21_Y15_N20
\386|DATA_BUS~1\ : stratixii_lcell_comb
-- Equation(s):
-- \386|DATA_BUS~1_combout\ = ( \401\(1) & ( (!\401\(0) & (\LOC_CS~combout\ & (!\401\(3) & !\401\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000100000000000000010000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_401(0),
	datab => \ALT_INV_LOC_CS~combout\,
	datac => ALT_INV_401(3),
	datad => ALT_INV_401(2),
	dataf => ALT_INV_401(1),
	combout => \386|DATA_BUS~1_combout\);

-- Location: PIN_V12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\wr_wrd[5]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr_wrd(5),
	combout => \wr_wrd~combout\(5));

-- Location: PIN_N19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\wr_wrd[9]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr_wrd(9),
	combout => \wr_wrd~combout\(9));

-- Location: PIN_T8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\wr_wrd[6]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr_wrd(6),
	combout => \wr_wrd~combout\(6));

-- Location: PIN_AA8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\wr_wrd[7]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr_wrd(7),
	combout => \wr_wrd~combout\(7));

-- Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\wr_wrd[8]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr_wrd(8),
	combout => \wr_wrd~combout\(8));

-- Location: LCCOMB_X21_Y16_N12
\88|_~2\ : stratixii_lcell_comb
-- Equation(s):
-- \88|_~2_combout\ = ( \wr_wrd~combout\(7) & ( !\wr_wrd~combout\(8) & ( (!\wr_wrd~combout\(4) & (!\wr_wrd~combout\(5) & (\wr_wrd~combout\(9) & !\wr_wrd~combout\(6)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000010000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_wr_wrd~combout\(4),
	datab => \ALT_INV_wr_wrd~combout\(5),
	datac => \ALT_INV_wr_wrd~combout\(9),
	datad => \ALT_INV_wr_wrd~combout\(6),
	datae => \ALT_INV_wr_wrd~combout\(7),
	dataf => \ALT_INV_wr_wrd~combout\(8),
	combout => \88|_~2_combout\);

-- Location: CLKCTRL_G9
\RESETn~clkctrl\ : stratixii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock")
-- pragma translate_on
PORT MAP (
	inclk => \RESETn~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \RESETn~clkctrl_outclk\);

-- Location: LCFF_X23_Y16_N19
\88|RESET_DASY2\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \88|RESET_DASY~regout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|RESET_DASY2~regout\);

-- Location: LCCOMB_X19_Y18_N4
\88|STR_STRIN~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \88|STR_STRIN~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \88|STR_STRIN~feeder_combout\);

-- Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\EN_OUT10~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_EN_OUT10,
	combout => \EN_OUT10~combout\);

-- Location: LCFF_X19_Y18_N7
\379\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \EN_OUT10~combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \379~regout\);

-- Location: LCFF_X19_Y18_N5
\88|STR_STRIN\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLRADD$wire~combout\,
	datain => \88|STR_STRIN~feeder_combout\,
	aclr => \ALT_INV_379~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|STR_STRIN~regout\);

-- Location: LCCOMB_X18_Y18_N6
\88|IDLE~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|IDLE~0_combout\ = ( \88|STR_STRIN~regout\ & ( !\88|RESET_DASY2~regout\ ) ) # ( !\88|STR_STRIN~regout\ & ( (!\88|RESET_DASY2~regout\ & (((\88|IDLE~regout\) # (\386|FC\(2))) # (\88|_~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111000011110000011100001111000011110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV__~0_combout\,
	datab => \386|ALT_INV_FC\(2),
	datac => \88|ALT_INV_RESET_DASY2~regout\,
	datad => \88|ALT_INV_IDLE~regout\,
	dataf => \88|ALT_INV_STR_STRIN~regout\,
	combout => \88|IDLE~0_combout\);

-- Location: LCFF_X18_Y18_N7
\88|IDLE\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|IDLE~0_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|IDLE~regout\);

-- Location: PIN_P7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\LOCADD[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_LOCADD(0),
	combout => \LOCADD~combout\(0));

-- Location: LCFF_X21_Y15_N9
\401[0]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \LOCADD~combout\(0),
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \401\(0));

-- Location: LCCOMB_X21_Y15_N2
\386|FC[3]~0\ : stratixii_lcell_comb
-- Equation(s):
-- \386|FC[3]~0_combout\ = ( !\401\(3) & ( (\LOC_CS~combout\ & (\401\(2) & !\401\(0))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000000000000110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_LOC_CS~combout\,
	datac => ALT_INV_401(2),
	datad => ALT_INV_401(0),
	dataf => ALT_INV_401(3),
	combout => \386|FC[3]~0_combout\);

-- Location: LCFF_X21_Y15_N19
\386|FC[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \401\(1),
	aclr => \421~combout\,
	sload => VCC,
	ena => \386|FC[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|FC\(1));

-- Location: LCCOMB_X18_Y18_N16
\88|_~4\ : stratixii_lcell_comb
-- Equation(s):
-- \88|_~4_combout\ = ( \386|DATA_BUS~1_combout\ & ( (\88|DATA_WRITE~regout\ & ((!\88|_~3_combout\) # (!\88|_~2_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000010100000111100001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV__~3_combout\,
	datac => \88|ALT_INV_DATA_WRITE~regout\,
	datad => \88|ALT_INV__~2_combout\,
	dataf => \386|ALT_INV_DATA_BUS~1_combout\,
	combout => \88|_~4_combout\);

-- Location: LCFF_X18_Y18_N17
\88|DATA_WRITE_ON_FIFO\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|_~4_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|DATA_WRITE_ON_FIFO~regout\);

-- Location: LCCOMB_X21_Y15_N18
\88|DATA_WRITE~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|DATA_WRITE~0_combout\ = ( !\88|DATA_WRITE_ON_FIFO~regout\ & ( (!\386|FC\(2)) # ((!\386|FC\(1)) # (\88|IDLE~regout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111001111111111111100111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \386|ALT_INV_FC\(2),
	datac => \88|ALT_INV_IDLE~regout\,
	datad => \386|ALT_INV_FC\(1),
	dataf => \88|ALT_INV_DATA_WRITE_ON_FIFO~regout\,
	combout => \88|DATA_WRITE~0_combout\);

-- Location: LCCOMB_X18_Y17_N4
\88|DATA_WRITE~1\ : stratixii_lcell_comb
-- Equation(s):
-- \88|DATA_WRITE~1_combout\ = ( \88|DATA_WRITE~0_combout\ & ( (!\386|DATA_BUS~1_combout\ & (\88|DATA_WRITE~regout\ & ((!\88|_~3_combout\) # (!\88|_~2_combout\)))) ) ) # ( !\88|DATA_WRITE~0_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000110010000000000011001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV__~3_combout\,
	datab => \386|ALT_INV_DATA_BUS~1_combout\,
	datac => \88|ALT_INV__~2_combout\,
	datad => \88|ALT_INV_DATA_WRITE~regout\,
	dataf => \88|ALT_INV_DATA_WRITE~0_combout\,
	combout => \88|DATA_WRITE~1_combout\);

-- Location: LCFF_X18_Y17_N5
\88|DATA_WRITE\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|DATA_WRITE~1_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|DATA_WRITE~regout\);

-- Location: LCCOMB_X18_Y17_N10
\88|READ_PED4~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|READ_PED4~0_combout\ = ((\379~regout\ & \88|READ_PED4~regout\)) # (\88|READ_PED3~regout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101011111010101010101111101010101010111110101010101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_READ_PED3~regout\,
	datac => \ALT_INV_379~regout\,
	datad => \88|ALT_INV_READ_PED4~regout\,
	combout => \88|READ_PED4~0_combout\);

-- Location: LCFF_X18_Y17_N11
\88|READ_PED4\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|READ_PED4~0_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|READ_PED4~regout\);

-- Location: LCCOMB_X18_Y18_N30
\88|FIFO_WR_SEL~3\ : stratixii_lcell_comb
-- Equation(s):
-- \88|FIFO_WR_SEL~3_combout\ = ( !\379~regout\ & ( \88|READ_PED4~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \88|ALT_INV_READ_PED4~regout\,
	dataf => \ALT_INV_379~regout\,
	combout => \88|FIFO_WR_SEL~3_combout\);

-- Location: LCFF_X18_Y18_N31
\88|READ_END\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|FIFO_WR_SEL~3_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|READ_END~regout\);

-- Location: LCCOMB_X18_Y18_N14
\88|CLEAR_CODE~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|CLEAR_CODE~0_combout\ = ( \88|READ_END~regout\ ) # ( !\88|READ_END~regout\ & ( \88|LOAD_PED3~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \88|ALT_INV_LOAD_PED3~regout\,
	dataf => \88|ALT_INV_READ_END~regout\,
	combout => \88|CLEAR_CODE~0_combout\);

-- Location: LCCOMB_X21_Y15_N8
\386|SUB_COMP~0\ : stratixii_lcell_comb
-- Equation(s):
-- \386|SUB_COMP~0_combout\ = ( \401\(0) & ( !\401\(3) & ( (\LOC_RWn~combout\ & (\LOC_CS~combout\ & (!\401\(2) & \401\(1)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000001000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LOC_RWn~combout\,
	datab => \ALT_INV_LOC_CS~combout\,
	datac => ALT_INV_401(2),
	datad => ALT_INV_401(1),
	datae => ALT_INV_401(0),
	dataf => ALT_INV_401(3),
	combout => \386|SUB_COMP~0_combout\);

-- Location: LCFF_X18_Y18_N9
\386|DILOFC[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~combout\,
	adatasdata => \LOCBUS~17\,
	aclr => \88|CLEAR_CODE~0_combout\,
	sload => VCC,
	ena => \386|SUB_COMP~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|DILOFC\(1));

-- Location: LCFF_X18_Y18_N21
\386|DILOFC[3]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~combout\,
	adatasdata => \LOCBUS~15\,
	aclr => \88|CLEAR_CODE~0_combout\,
	sload => VCC,
	ena => \386|SUB_COMP~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|DILOFC\(3));

-- Location: LCFF_X18_Y18_N13
\386|DILOFC[2]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~combout\,
	adatasdata => \LOCBUS~16\,
	aclr => \88|CLEAR_CODE~0_combout\,
	sload => VCC,
	ena => \386|SUB_COMP~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|DILOFC\(2));

-- Location: LCCOMB_X18_Y18_N20
\88|_~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|_~0_combout\ = ( \386|DILOFC\(2) & ( (\386|DILOFC\(1) & \386|DILOFC\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \386|ALT_INV_DILOFC\(1),
	datad => \386|ALT_INV_DILOFC\(3),
	dataf => \386|ALT_INV_DILOFC\(2),
	combout => \88|_~0_combout\);

-- Location: LCFF_X18_Y18_N29
\386|DILOFC[0]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~combout\,
	adatasdata => \LOCBUS~18\,
	aclr => \88|CLEAR_CODE~0_combout\,
	sload => VCC,
	ena => \386|SUB_COMP~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|DILOFC\(0));

-- Location: LCCOMB_X18_Y18_N4
\88|ENIN1~5\ : stratixii_lcell_comb
-- Equation(s):
-- \88|ENIN1~5_combout\ = ( !\386|DILOFC\(0) & ( (!\386|FC\(2) & (\88|_~0_combout\ & !\88|IDLE~regout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000000000000011000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \386|ALT_INV_FC\(2),
	datac => \88|ALT_INV__~0_combout\,
	datad => \88|ALT_INV_IDLE~regout\,
	dataf => \386|ALT_INV_DILOFC\(0),
	combout => \88|ENIN1~5_combout\);

-- Location: LCFF_X18_Y18_N5
\88|LOAD_PED\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|ENIN1~5_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|LOAD_PED~regout\);

-- Location: LCFF_X18_Y17_N19
\88|LOAD_PED1\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \88|LOAD_PED~regout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|LOAD_PED1~regout\);

-- Location: LCCOMB_X18_Y17_N6
\88|_~5\ : stratixii_lcell_comb
-- Equation(s):
-- \88|_~5_combout\ = ( \88|LOAD_PED2~regout\ & ( \379~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_379~regout\,
	dataf => \88|ALT_INV_LOAD_PED2~regout\,
	combout => \88|_~5_combout\);

-- Location: LCFF_X18_Y17_N7
\88|LOAD_PULSE\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|_~5_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|LOAD_PULSE~regout\);

-- Location: LCCOMB_X18_Y17_N0
\88|DILO_SEL~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|DILO_SEL~0_combout\ = (\88|LOAD_PULSE~regout\) # (\88|LOAD_PED1~regout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100111111001111110011111100111111001111110011111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \88|ALT_INV_LOAD_PED1~regout\,
	datac => \88|ALT_INV_LOAD_PULSE~regout\,
	combout => \88|DILO_SEL~0_combout\);

-- Location: LCFF_X18_Y17_N1
\88|LOAD_PED2\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|DILO_SEL~0_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|LOAD_PED2~regout\);

-- Location: LCCOMB_X19_Y18_N8
\88|CLEAR_FIFO~4\ : stratixii_lcell_comb
-- Equation(s):
-- \88|CLEAR_FIFO~4_combout\ = ( !\379~regout\ & ( \88|LOAD_PED2~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \88|ALT_INV_LOAD_PED2~regout\,
	dataf => \ALT_INV_379~regout\,
	combout => \88|CLEAR_FIFO~4_combout\);

-- Location: LCFF_X18_Y18_N15
\88|LOAD_PED3\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \88|CLEAR_FIFO~4_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|LOAD_PED3~regout\);

-- Location: LCCOMB_X18_Y18_N26
\88|CLEAR_FIFO~3\ : stratixii_lcell_comb
-- Equation(s):
-- \88|CLEAR_FIFO~3_combout\ = ( !\386|DILOFC\(2) & ( (\88|CHOOSER~regout\ & (\386|DILOFC\(3) & (!\386|DILOFC\(0) & \386|DILOFC\(1)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010000000000000001000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_CHOOSER~regout\,
	datab => \386|ALT_INV_DILOFC\(3),
	datac => \386|ALT_INV_DILOFC\(0),
	datad => \386|ALT_INV_DILOFC\(1),
	dataf => \386|ALT_INV_DILOFC\(2),
	combout => \88|CLEAR_FIFO~3_combout\);

-- Location: LCFF_X18_Y18_N27
\88|PED_MEAS\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|CLEAR_FIFO~3_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|PED_MEAS~regout\);

-- Location: LCFF_X18_Y16_N19
\88|PED_MEAS1\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \88|PED_MEAS~regout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|PED_MEAS1~regout\);

-- Location: LCFF_X18_Y16_N17
\88|PED_MEAS2\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \88|PED_MEAS1~regout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|PED_MEAS2~regout\);

-- Location: LCFF_X18_Y17_N29
\88|PED_MEAS3\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \88|PED_MEAS2~regout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|PED_MEAS3~regout\);

-- Location: LCCOMB_X18_Y17_N30
\88|PED_MEAS4~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|PED_MEAS4~0_combout\ = ( \88|PED_MEAS4~regout\ & ( (\379~regout\) # (\88|PED_MEAS3~regout\) ) ) # ( !\88|PED_MEAS4~regout\ & ( \88|PED_MEAS3~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011111111111100001111000011110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \88|ALT_INV_PED_MEAS3~regout\,
	datad => \ALT_INV_379~regout\,
	datae => \88|ALT_INV_PED_MEAS4~regout\,
	combout => \88|PED_MEAS4~0_combout\);

-- Location: LCFF_X18_Y17_N31
\88|PED_MEAS4\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|PED_MEAS4~0_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|PED_MEAS4~regout\);

-- Location: LCCOMB_X19_Y18_N6
\88|RESET_DASY~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|RESET_DASY~0_combout\ = ( \88|PED_MEAS4~regout\ & ( (!\88|LOAD_PED3~regout\ & (!\88|READ_END~regout\ & \379~regout\)) ) ) # ( !\88|PED_MEAS4~regout\ & ( (!\88|LOAD_PED3~regout\ & !\88|READ_END~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000011000000110000001100000000000000110000000000000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \88|ALT_INV_LOAD_PED3~regout\,
	datac => \88|ALT_INV_READ_END~regout\,
	datad => \ALT_INV_379~regout\,
	dataf => \88|ALT_INV_PED_MEAS4~regout\,
	combout => \88|RESET_DASY~0_combout\);

-- Location: PIN_B18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\EMPTY_FIFO~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_EMPTY_FIFO,
	combout => \EMPTY_FIFO~combout\);

-- Location: LCCOMB_X18_Y18_N24
\88|DATA_READ_FIFO~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|DATA_READ_FIFO~0_combout\ = ( \88|_~1_combout\ & ( (!\EMPTY_FIFO~combout\) # (\88|DATA_READ~regout\) ) ) # ( !\88|_~1_combout\ & ( \88|DATA_READ~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111111111000011111111111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \88|ALT_INV_DATA_READ~regout\,
	datad => \ALT_INV_EMPTY_FIFO~combout\,
	dataf => \88|ALT_INV__~1_combout\,
	combout => \88|DATA_READ_FIFO~0_combout\);

-- Location: LCFF_X18_Y18_N25
\88|DATA_READ_FIFO\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|DATA_READ_FIFO~0_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|DATA_READ_FIFO~regout\);

-- Location: LCCOMB_X21_Y15_N14
\88|_~1\ : stratixii_lcell_comb
-- Equation(s):
-- \88|_~1_combout\ = ( \401\(1) & ( \88|DATA_READ_FIFO~regout\ & ( ((!\LOC_CS~combout\) # ((\401\(3)) # (\401\(2)))) # (\401\(0)) ) ) ) # ( !\401\(1) & ( \88|DATA_READ_FIFO~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111101111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => ALT_INV_401(0),
	datab => \ALT_INV_LOC_CS~combout\,
	datac => ALT_INV_401(2),
	datad => ALT_INV_401(3),
	datae => ALT_INV_401(1),
	dataf => \88|ALT_INV_DATA_READ_FIFO~regout\,
	combout => \88|_~1_combout\);

-- Location: LCCOMB_X22_Y15_N16
\88|RESET_DASY~1\ : stratixii_lcell_comb
-- Equation(s):
-- \88|RESET_DASY~1_combout\ = ( \88|RESET_DASY~0_combout\ & ( \88|_~1_combout\ & ( ((\88|_~3_combout\ & (\88|DATA_WRITE~regout\ & \88|_~2_combout\))) # (\EMPTY_FIFO~combout\) ) ) ) # ( !\88|RESET_DASY~0_combout\ & ( \88|_~1_combout\ ) ) # ( 
-- \88|RESET_DASY~0_combout\ & ( !\88|_~1_combout\ & ( (\88|_~3_combout\ & (\88|DATA_WRITE~regout\ & \88|_~2_combout\)) ) ) ) # ( !\88|RESET_DASY~0_combout\ & ( !\88|_~1_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000001111111111111111110101010101010111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_EMPTY_FIFO~combout\,
	datab => \88|ALT_INV__~3_combout\,
	datac => \88|ALT_INV_DATA_WRITE~regout\,
	datad => \88|ALT_INV__~2_combout\,
	datae => \88|ALT_INV_RESET_DASY~0_combout\,
	dataf => \88|ALT_INV__~1_combout\,
	combout => \88|RESET_DASY~1_combout\);

-- Location: LCFF_X22_Y15_N17
\88|RESET_DASY\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|RESET_DASY~1_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|RESET_DASY~regout\);

-- Location: LCCOMB_X21_Y15_N16
\421\ : stratixii_lcell_comb
-- Equation(s):
-- \421~combout\ = ( \88|RESET_DASY~regout\ ) # ( !\88|RESET_DASY~regout\ & ( !\RESETn~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_RESETn~combout\,
	dataf => \88|ALT_INV_RESET_DASY~regout\,
	combout => \421~combout\);

-- Location: LCFF_X21_Y15_N1
\386|FC[2]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \386|FC[2]~feeder_combout\,
	aclr => \421~combout\,
	ena => \386|FC[3]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|FC\(2));

-- Location: LCCOMB_X18_Y17_N16
\88|DATA_READ~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|DATA_READ~0_combout\ = ( \386|DATA_BUS~1_combout\ & ( ((!\88|IDLE~regout\ & (\386|FC\(2) & !\386|FC\(1)))) # (\88|DATA_READ_FIFO~regout\) ) ) # ( !\386|DATA_BUS~1_combout\ & ( (!\88|IDLE~regout\ & (\386|FC\(2) & !\386|FC\(1))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000100000001000000010000000100000111111110010000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_IDLE~regout\,
	datab => \386|ALT_INV_FC\(2),
	datac => \386|ALT_INV_FC\(1),
	datad => \88|ALT_INV_DATA_READ_FIFO~regout\,
	dataf => \386|ALT_INV_DATA_BUS~1_combout\,
	combout => \88|DATA_READ~0_combout\);

-- Location: LCFF_X18_Y17_N17
\88|DATA_READ\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|DATA_READ~0_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|DATA_READ~regout\);

-- Location: LCCOMB_X21_Y15_N22
\286~0\ : stratixii_lcell_comb
-- Equation(s):
-- \286~0_combout\ = (\LOC_CS~combout\ & ((\88|DATA_READ_FIFO~regout\) # (\88|DATA_READ~regout\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100110011000000110011001100000011001100110000001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_LOC_CS~combout\,
	datac => \88|ALT_INV_DATA_READ~regout\,
	datad => \88|ALT_INV_DATA_READ_FIFO~regout\,
	combout => \286~0_combout\);

-- Location: PIN_N3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[22]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(22),
	combout => \ff~combout\(22));

-- Location: LCCOMB_X23_Y16_N2
\45|52[11]~20\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[11]~20_combout\ = (!\412~0_combout\ & ((!\286~0_combout\) # ((\ff~combout\(22))))) # (\412~0_combout\ & (\EMPTY_FIFO~combout\ & ((!\286~0_combout\) # (\ff~combout\(22)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000110010101111100011001010111110001100101011111000110010101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datac => \ALT_INV_EMPTY_FIFO~combout\,
	datad => \ALT_INV_ff~combout\(22),
	combout => \45|52[11]~20_combout\);

-- Location: LCCOMB_X23_Y16_N22
\45|52[6]~13\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[6]~13_combout\ = (\286~0_combout\) # (\412~0_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111011101110111011101110111011101110111011101110111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	combout => \45|52[6]~13_combout\);

-- Location: PIN_M20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CARD_NUMB[4]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CARD_NUMB(4),
	combout => \CARD_NUMB~combout\(4));

-- Location: LCCOMB_X23_Y16_N6
\45|52[10]~21\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[10]~21_combout\ = ( !\412~0_combout\ & ( (!\286~0_combout\) # (\CARD_NUMB~combout\(4)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111001111110011000000000000000011110011111100110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_CARD_NUMB~combout\(4),
	datac => \ALT_INV_286~0_combout\,
	datae => \ALT_INV_412~0_combout\,
	combout => \45|52[10]~21_combout\);

-- Location: PIN_D10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CARD_NUMB[3]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CARD_NUMB(3),
	combout => \CARD_NUMB~combout\(3));

-- Location: LCCOMB_X21_Y16_N20
\45|52[9]~22\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[9]~22_combout\ = ( \CARD_NUMB~combout\(3) & ( (!\412~0_combout\) # (\wr_wrd~combout\(9)) ) ) # ( !\CARD_NUMB~combout\(3) & ( (!\286~0_combout\ & ((!\412~0_combout\) # (\wr_wrd~combout\(9)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000110010001100100011001000110010101111101011111010111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datac => \ALT_INV_wr_wrd~combout\(9),
	dataf => \ALT_INV_CARD_NUMB~combout\(3),
	combout => \45|52[9]~22_combout\);

-- Location: PIN_A6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CARD_NUMB[2]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CARD_NUMB(2),
	combout => \CARD_NUMB~combout\(2));

-- Location: LCCOMB_X21_Y16_N8
\45|52[8]~23\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[8]~23_combout\ = ( \wr_wrd~combout\(8) & ( (!\286~0_combout\) # (\CARD_NUMB~combout\(2)) ) ) # ( !\wr_wrd~combout\(8) & ( (!\412~0_combout\ & ((!\286~0_combout\) # (\CARD_NUMB~combout\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000101010001010100010101000101011001111110011111100111111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datac => \ALT_INV_CARD_NUMB~combout\(2),
	dataf => \ALT_INV_wr_wrd~combout\(8),
	combout => \45|52[8]~23_combout\);

-- Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CARD_NUMB[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CARD_NUMB(1),
	combout => \CARD_NUMB~combout\(1));

-- Location: LCCOMB_X21_Y16_N10
\45|52[7]~24\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[7]~24_combout\ = ( \CARD_NUMB~combout\(1) & ( (!\412~0_combout\) # (\wr_wrd~combout\(7)) ) ) # ( !\CARD_NUMB~combout\(1) & ( (!\286~0_combout\ & ((!\412~0_combout\) # (\wr_wrd~combout\(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100011001100100010001100110010101010111111111010101011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datad => \ALT_INV_wr_wrd~combout\(7),
	dataf => \ALT_INV_CARD_NUMB~combout\(1),
	combout => \45|52[7]~24_combout\);

-- Location: PIN_J3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CARD_NUMB[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_CARD_NUMB(0),
	combout => \CARD_NUMB~combout\(0));

-- Location: LCCOMB_X21_Y16_N18
\45|52[6]~12\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[6]~12_combout\ = ( \wr_wrd~combout\(6) & ( (!\286~0_combout\) # (\CARD_NUMB~combout\(0)) ) ) # ( !\wr_wrd~combout\(6) & ( (!\412~0_combout\ & ((!\286~0_combout\) # (\CARD_NUMB~combout\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010101010100010001010101011001100111111111100110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datad => \ALT_INV_CARD_NUMB~combout\(0),
	dataf => \ALT_INV_wr_wrd~combout\(6),
	combout => \45|52[6]~12_combout\);

-- Location: PIN_M3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[21]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(21),
	combout => \ff~combout\(21));

-- Location: LCCOMB_X21_Y16_N22
\45|52[5]~14\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[5]~14_combout\ = (!\412~0_combout\ & ((!\286~0_combout\) # ((\ff~combout\(21))))) # (\412~0_combout\ & (\wr_wrd~combout\(5) & ((!\286~0_combout\) # (\ff~combout\(21)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000110010101111100011001010111110001100101011111000110010101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datac => \ALT_INV_wr_wrd~combout\(5),
	datad => \ALT_INV_ff~combout\(21),
	combout => \45|52[5]~14_combout\);

-- Location: PIN_H21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\wr_wrd[4]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr_wrd(4),
	combout => \wr_wrd~combout\(4));

-- Location: PIN_P6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[20]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(20),
	combout => \ff~combout\(20));

-- Location: LCCOMB_X23_Y16_N26
\45|52[4]~15\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[4]~15_combout\ = (!\412~0_combout\ & ((!\286~0_combout\) # ((\ff~combout\(20))))) # (\412~0_combout\ & (\wr_wrd~combout\(4) & ((!\286~0_combout\) # (\ff~combout\(20)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000110010101111100011001010111110001100101011111000110010101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datac => \ALT_INV_wr_wrd~combout\(4),
	datad => \ALT_INV_ff~combout\(20),
	combout => \45|52[4]~15_combout\);

-- Location: PIN_N1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\wr_wrd[3]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_wr_wrd(3),
	combout => \wr_wrd~combout\(3));

-- Location: PIN_K8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[19]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(19),
	combout => \ff~combout\(19));

-- Location: LCCOMB_X18_Y17_N26
\45|52[3]~16\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[3]~16_combout\ = ( \286~0_combout\ & ( (\ff~combout\(19) & ((!\412~0_combout\) # (\wr_wrd~combout\(3)))) ) ) # ( !\286~0_combout\ & ( (!\412~0_combout\) # (\wr_wrd~combout\(3)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111110101111101011111010100000000111101010000000011110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_wr_wrd~combout\(3),
	datac => \ALT_INV_412~0_combout\,
	datad => \ALT_INV_ff~combout\(19),
	dataf => \ALT_INV_286~0_combout\,
	combout => \45|52[3]~16_combout\);

-- Location: PIN_P21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[18]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(18),
	combout => \ff~combout\(18));

-- Location: LCCOMB_X18_Y17_N14
\45|52[2]~17\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[2]~17_combout\ = ( \wr_wrd~combout\(2) & ( (!\286~0_combout\) # (\ff~combout\(18)) ) ) # ( !\wr_wrd~combout\(2) & ( (!\412~0_combout\ & ((!\286~0_combout\) # (\ff~combout\(18)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000110010001100100011001000110010101111101011111010111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_286~0_combout\,
	datab => \ALT_INV_412~0_combout\,
	datac => \ALT_INV_ff~combout\(18),
	dataf => \ALT_INV_wr_wrd~combout\(2),
	combout => \45|52[2]~17_combout\);

-- Location: PIN_Y12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[17]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(17),
	combout => \ff~combout\(17));

-- Location: LCCOMB_X18_Y17_N2
\45|52[1]~18\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[1]~18_combout\ = ( \412~0_combout\ & ( (\wr_wrd~combout\(1) & ((!\286~0_combout\) # (\ff~combout\(17)))) ) ) # ( !\412~0_combout\ & ( (!\286~0_combout\) # (\ff~combout\(17)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101011111111101010101111111100001010000011110000101000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_286~0_combout\,
	datac => \ALT_INV_wr_wrd~combout\(1),
	datad => \ALT_INV_ff~combout\(17),
	dataf => \ALT_INV_412~0_combout\,
	combout => \45|52[1]~18_combout\);

-- Location: PIN_C15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[16]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(16),
	combout => \ff~combout\(16));

-- Location: LCCOMB_X18_Y17_N12
\45|52[0]~19\ : stratixii_lcell_comb
-- Equation(s):
-- \45|52[0]~19_combout\ = ( \ff~combout\(16) & ( (!\412~0_combout\) # (\wr_wrd~combout\(0)) ) ) # ( !\ff~combout\(16) & ( (!\286~0_combout\ & ((!\412~0_combout\) # (\wr_wrd~combout\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000101010001010100010101000101011001111110011111100111111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_286~0_combout\,
	datab => \ALT_INV_412~0_combout\,
	datac => \ALT_INV_wr_wrd~combout\(0),
	dataf => \ALT_INV_ff~combout\(16),
	combout => \45|52[0]~19_combout\);

-- Location: LCCOMB_X18_Y18_N8
\88|CHOOSER~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|CHOOSER~0_combout\ = ( \386|DILOFC\(2) & ( \88|CHOOSER~regout\ ) ) # ( !\386|DILOFC\(2) & ( (\88|CHOOSER~regout\ & (((!\386|DILOFC\(3)) # (!\386|DILOFC\(1))) # (\386|DILOFC\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010001010101010101000101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_CHOOSER~regout\,
	datab => \386|ALT_INV_DILOFC\(0),
	datac => \386|ALT_INV_DILOFC\(3),
	datad => \386|ALT_INV_DILOFC\(1),
	dataf => \386|ALT_INV_DILOFC\(2),
	combout => \88|CHOOSER~0_combout\);

-- Location: LCCOMB_X18_Y18_N22
\88|CHOOSER~1\ : stratixii_lcell_comb
-- Equation(s):
-- \88|CHOOSER~1_combout\ = ( \88|STR_STRIN~regout\ & ( ((!\88|_~0_combout\ & (!\386|FC\(2) & !\88|IDLE~regout\))) # (\88|CHOOSER~0_combout\) ) ) # ( !\88|STR_STRIN~regout\ & ( \88|CHOOSER~0_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111110000000111111111000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV__~0_combout\,
	datab => \386|ALT_INV_FC\(2),
	datac => \88|ALT_INV_IDLE~regout\,
	datad => \88|ALT_INV_CHOOSER~0_combout\,
	dataf => \88|ALT_INV_STR_STRIN~regout\,
	combout => \88|CHOOSER~1_combout\);

-- Location: LCFF_X18_Y18_N23
\88|CHOOSER\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|CHOOSER~1_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|CHOOSER~regout\);

-- Location: LCCOMB_X18_Y18_N2
\88|ENIN1~2\ : stratixii_lcell_comb
-- Equation(s):
-- \88|ENIN1~2_combout\ = ( !\88|DATA_WRITE~regout\ & ( !\88|DATA_READ~regout\ & ( (\88|IDLE~regout\ & (!\88|DATA_WRITE_ON_FIFO~regout\ & (!\88|DATA_READ_FIFO~regout\ & !\88|CHOOSER~regout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_IDLE~regout\,
	datab => \88|ALT_INV_DATA_WRITE_ON_FIFO~regout\,
	datac => \88|ALT_INV_DATA_READ_FIFO~regout\,
	datad => \88|ALT_INV_CHOOSER~regout\,
	datae => \88|ALT_INV_DATA_WRITE~regout\,
	dataf => \88|ALT_INV_DATA_READ~regout\,
	combout => \88|ENIN1~2_combout\);

-- Location: PIN_H7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[15]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(15),
	combout => \ff~combout\(15));

-- Location: LCCOMB_X23_Y16_N20
\45|50[3]~4\ : stratixii_lcell_comb
-- Equation(s):
-- \45|50[3]~4_combout\ = (!\412~0_combout\ & ((!\286~0_combout\) # ((\ff~combout\(15))))) # (\412~0_combout\ & (\88|ENIN1~2_combout\ & ((!\286~0_combout\) # (\ff~combout\(15)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000110010101111100011001010111110001100101011111000110010101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datac => \88|ALT_INV_ENIN1~2_combout\,
	datad => \ALT_INV_ff~combout\(15),
	combout => \45|50[3]~4_combout\);

-- Location: LCCOMB_X18_Y16_N18
\88|FIFO_WR_SEL~2\ : stratixii_lcell_comb
-- Equation(s):
-- \88|FIFO_WR_SEL~2_combout\ = ( !\88|PED_MEAS1~regout\ & ( !\88|PED_MEAS2~regout\ & ( (!\88|PED_MEAS4~regout\ & (!\88|PED_MEAS3~regout\ & !\88|PED_MEAS~regout\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_PED_MEAS4~regout\,
	datab => \88|ALT_INV_PED_MEAS3~regout\,
	datac => \88|ALT_INV_PED_MEAS~regout\,
	datae => \88|ALT_INV_PED_MEAS1~regout\,
	dataf => \88|ALT_INV_PED_MEAS2~regout\,
	combout => \88|FIFO_WR_SEL~2_combout\);

-- Location: PIN_D11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[14]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(14),
	combout => \ff~combout\(14));

-- Location: LCCOMB_X17_Y18_N2
\45|50[2]~5\ : stratixii_lcell_comb
-- Equation(s):
-- \45|50[2]~5_combout\ = ( \ff~combout\(14) & ( \412~0_combout\ & ( (!\88|CHOOSER~regout\ & (\88|FIFO_WR_SEL~2_combout\ & \88|IDLE~regout\)) ) ) ) # ( !\ff~combout\(14) & ( \412~0_combout\ & ( (!\88|CHOOSER~regout\ & (\88|FIFO_WR_SEL~2_combout\ & 
-- (!\286~0_combout\ & \88|IDLE~regout\))) ) ) ) # ( \ff~combout\(14) & ( !\412~0_combout\ ) ) # ( !\ff~combout\(14) & ( !\412~0_combout\ & ( !\286~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111111111111111100000000001000000000000000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_CHOOSER~regout\,
	datab => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datac => \ALT_INV_286~0_combout\,
	datad => \88|ALT_INV_IDLE~regout\,
	datae => \ALT_INV_ff~combout\(14),
	dataf => \ALT_INV_412~0_combout\,
	combout => \45|50[2]~5_combout\);

-- Location: LCCOMB_X17_Y18_N22
\88|FCO[1]~2\ : stratixii_lcell_comb
-- Equation(s):
-- \88|FCO[1]~2_combout\ = ( \88|RESET_DASY~regout\ ) # ( !\88|RESET_DASY~regout\ & ( (((\88|CHOOSER~regout\) # (\88|DATA_READ_FIFO~regout\)) # (\88|DATA_READ~regout\)) # (\88|RESET_DASY2~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111111111111011111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_RESET_DASY2~regout\,
	datab => \88|ALT_INV_DATA_READ~regout\,
	datac => \88|ALT_INV_DATA_READ_FIFO~regout\,
	datad => \88|ALT_INV_CHOOSER~regout\,
	dataf => \88|ALT_INV_RESET_DASY~regout\,
	combout => \88|FCO[1]~2_combout\);

-- Location: PIN_J8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[13]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(13),
	combout => \ff~combout\(13));

-- Location: LCCOMB_X17_Y18_N6
\45|50[1]~6\ : stratixii_lcell_comb
-- Equation(s):
-- \45|50[1]~6_combout\ = ( \ff~combout\(13) & ( (!\88|FCO[1]~2_combout\) # (!\412~0_combout\) ) ) # ( !\ff~combout\(13) & ( (!\286~0_combout\ & ((!\88|FCO[1]~2_combout\) # (!\412~0_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111101000000000111110100000000011111010111110101111101011111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[1]~2_combout\,
	datac => \ALT_INV_412~0_combout\,
	datad => \ALT_INV_286~0_combout\,
	dataf => \ALT_INV_ff~combout\(13),
	combout => \45|50[1]~6_combout\);

-- Location: LCCOMB_X17_Y18_N20
\88|FCO[0]~4\ : stratixii_lcell_comb
-- Equation(s):
-- \88|FCO[0]~4_combout\ = ( !\88|RESET_DASY~regout\ & ( (!\88|RESET_DASY2~regout\ & \88|IDLE~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000010100000101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_RESET_DASY2~regout\,
	datac => \88|ALT_INV_IDLE~regout\,
	dataf => \88|ALT_INV_RESET_DASY~regout\,
	combout => \88|FCO[0]~4_combout\);

-- Location: LCCOMB_X18_Y18_N10
\88|ENIN1~4\ : stratixii_lcell_comb
-- Equation(s):
-- \88|ENIN1~4_combout\ = ( \88|_~0_combout\ & ( (\386|DILOFC\(0) & (!\386|FC\(2) & !\88|IDLE~regout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110000000000000011000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \386|ALT_INV_DILOFC\(0),
	datac => \386|ALT_INV_FC\(2),
	datad => \88|ALT_INV_IDLE~regout\,
	dataf => \88|ALT_INV__~0_combout\,
	combout => \88|ENIN1~4_combout\);

-- Location: LCFF_X18_Y18_N11
\88|READ_PED\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \88|ENIN1~4_combout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|READ_PED~regout\);

-- Location: LCFF_X18_Y17_N23
\88|READ_PED1\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \88|READ_PED~regout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|READ_PED1~regout\);

-- Location: LCFF_X18_Y17_N21
\88|READ_PED2\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \88|READ_PED1~regout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|READ_PED2~regout\);

-- Location: LCFF_X18_Y17_N9
\88|READ_PED3\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \88|READ_PED2~regout\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \88|READ_PED3~regout\);

-- Location: LCCOMB_X18_Y17_N22
\88|FCO[0]~3\ : stratixii_lcell_comb
-- Equation(s):
-- \88|FCO[0]~3_combout\ = ( !\88|READ_PED1~regout\ & ( !\88|READ_PED~regout\ & ( (!\88|READ_PED2~regout\ & (!\88|READ_END~regout\ & (!\88|READ_PED4~regout\ & !\88|READ_PED3~regout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_READ_PED2~regout\,
	datab => \88|ALT_INV_READ_END~regout\,
	datac => \88|ALT_INV_READ_PED4~regout\,
	datad => \88|ALT_INV_READ_PED3~regout\,
	datae => \88|ALT_INV_READ_PED1~regout\,
	dataf => \88|ALT_INV_READ_PED~regout\,
	combout => \88|FCO[0]~3_combout\);

-- Location: PIN_K21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[12]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(12),
	combout => \ff~combout\(12));

-- Location: LCCOMB_X17_Y18_N16
\45|50[0]~7\ : stratixii_lcell_comb
-- Equation(s):
-- \45|50[0]~7_combout\ = ( \412~0_combout\ & ( (!\88|FCO[0]~4_combout\ & (((!\286~0_combout\) # (\ff~combout\(12))))) # (\88|FCO[0]~4_combout\ & (!\88|FCO[0]~3_combout\ & ((!\286~0_combout\) # (\ff~combout\(12))))) ) ) # ( !\412~0_combout\ & ( 
-- (!\286~0_combout\) # (\ff~combout\(12)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100001111111111110000111111101110000011101110111000001110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~4_combout\,
	datab => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \ALT_INV_ff~combout\(12),
	datad => \ALT_INV_286~0_combout\,
	dataf => \ALT_INV_412~0_combout\,
	combout => \45|50[0]~7_combout\);

-- Location: PIN_W15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[11]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(11),
	combout => \ff~combout\(11));

-- Location: LCCOMB_X23_Y16_N24
\45|43~1\ : stratixii_lcell_comb
-- Equation(s):
-- \45|43~1_combout\ = (!\412~0_combout\ & ((!\286~0_combout\) # (\ff~combout\(11))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000101010001010100010101000101010001010100010101000101010001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datac => \ALT_INV_ff~combout\(11),
	combout => \45|43~1_combout\);

-- Location: LCFF_X21_Y16_N7
\386|SUB_COMP\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~12\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	ena => \386|SUB_COMP~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|SUB_COMP~regout\);

-- Location: PIN_B15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[10]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(10),
	combout => \ff~combout\(10));

-- Location: LCCOMB_X21_Y16_N6
\45|48~1\ : stratixii_lcell_comb
-- Equation(s):
-- \45|48~1_combout\ = ( \ff~combout\(10) & ( (!\412~0_combout\) # (\386|SUB_COMP~regout\) ) ) # ( !\ff~combout\(10) & ( (!\286~0_combout\ & ((!\412~0_combout\) # (\386|SUB_COMP~regout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100011001100100010001100110010101010111111111010101011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datad => \386|ALT_INV_SUB_COMP~regout\,
	dataf => \ALT_INV_ff~combout\(10),
	combout => \45|48~1_combout\);

-- Location: PIN_W13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[9]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(9),
	combout => \ff~combout\(9));

-- Location: LCCOMB_X17_Y18_N30
\271[9]~0\ : stratixii_lcell_comb
-- Equation(s):
-- \271[9]~0_combout\ = ( \LOC_CS~combout\ & ( ((!\88|DATA_READ~regout\ & !\88|DATA_READ_FIFO~regout\)) # (\ff~combout\(9)) ) ) # ( !\LOC_CS~combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111110101010101011111010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_ff~combout\(9),
	datac => \88|ALT_INV_DATA_READ~regout\,
	datad => \88|ALT_INV_DATA_READ_FIFO~regout\,
	dataf => \ALT_INV_LOC_CS~combout\,
	combout => \271[9]~0_combout\);

-- Location: PIN_B16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[8]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(8),
	combout => \ff~combout\(8));

-- Location: LCCOMB_X23_Y16_N12
\45|44[0]~2\ : stratixii_lcell_comb
-- Equation(s):
-- \45|44[0]~2_combout\ = (!\412~0_combout\ & ((!\286~0_combout\) # (\ff~combout\(8))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000101010001010100010101000101010001010100010101000101010001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datac => \ALT_INV_ff~combout\(8),
	combout => \45|44[0]~2_combout\);

-- Location: PIN_AB6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[7]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(7),
	combout => \ff~combout\(7));

-- Location: LCCOMB_X23_Y16_N14
\45|45[3]~4\ : stratixii_lcell_comb
-- Equation(s):
-- \45|45[3]~4_combout\ = ( \386|DILOFC\(3) & ( (!\286~0_combout\) # (\ff~combout\(7)) ) ) # ( !\386|DILOFC\(3) & ( (!\412~0_combout\ & ((!\286~0_combout\) # (\ff~combout\(7)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010101010100010001010101011001100111111111100110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datad => \ALT_INV_ff~combout\(7),
	dataf => \386|ALT_INV_DILOFC\(3),
	combout => \45|45[3]~4_combout\);

-- Location: PIN_K22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[6]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(6),
	combout => \ff~combout\(6));

-- Location: LCCOMB_X18_Y18_N12
\45|45[2]~5\ : stratixii_lcell_comb
-- Equation(s):
-- \45|45[2]~5_combout\ = ( \286~0_combout\ & ( (\ff~combout\(6) & ((!\412~0_combout\) # (\386|DILOFC\(2)))) ) ) # ( !\286~0_combout\ & ( (!\412~0_combout\) # (\386|DILOFC\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100110011111111110011001111111100001100000011110000110000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_412~0_combout\,
	datac => \ALT_INV_ff~combout\(6),
	datad => \386|ALT_INV_DILOFC\(2),
	dataf => \ALT_INV_286~0_combout\,
	combout => \45|45[2]~5_combout\);

-- Location: PIN_F13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[5]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(5),
	combout => \ff~combout\(5));

-- Location: LCCOMB_X23_Y16_N0
\45|45[1]~6\ : stratixii_lcell_comb
-- Equation(s):
-- \45|45[1]~6_combout\ = ( \386|DILOFC\(1) & ( (!\286~0_combout\) # (\ff~combout\(5)) ) ) # ( !\386|DILOFC\(1) & ( (!\412~0_combout\ & ((!\286~0_combout\) # (\ff~combout\(5)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100010101010100010001010101011001100111111111100110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datad => \ALT_INV_ff~combout\(5),
	dataf => \386|ALT_INV_DILOFC\(1),
	combout => \45|45[1]~6_combout\);

-- Location: PIN_A18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[4]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(4),
	combout => \ff~combout\(4));

-- Location: LCCOMB_X18_Y18_N28
\45|45[0]~7\ : stratixii_lcell_comb
-- Equation(s):
-- \45|45[0]~7_combout\ = ( \412~0_combout\ & ( (\386|DILOFC\(0) & ((!\286~0_combout\) # (\ff~combout\(4)))) ) ) # ( !\412~0_combout\ & ( (!\286~0_combout\) # (\ff~combout\(4)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100111111001111110011111100111100000000110011110000000011001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_286~0_combout\,
	datac => \ALT_INV_ff~combout\(4),
	datad => \386|ALT_INV_DILOFC\(0),
	dataf => \ALT_INV_412~0_combout\,
	combout => \45|45[0]~7_combout\);

-- Location: LCFF_X21_Y16_N5
\386|CLK_SEL[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~19\,
	sload => VCC,
	ena => \386|SUB_COMP~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|CLK_SEL\(1));

-- Location: PIN_V8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[3]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(3),
	combout => \ff~combout\(3));

-- Location: LCCOMB_X21_Y16_N4
\45|47[1]~2\ : stratixii_lcell_comb
-- Equation(s):
-- \45|47[1]~2_combout\ = ( \ff~combout\(3) & ( (!\412~0_combout\) # (\386|CLK_SEL\(1)) ) ) # ( !\ff~combout\(3) & ( (!\286~0_combout\ & ((!\412~0_combout\) # (\386|CLK_SEL\(1)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000100011001100100010001100110010101010111111111010101011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datad => \386|ALT_INV_CLK_SEL\(1),
	dataf => \ALT_INV_ff~combout\(3),
	combout => \45|47[1]~2_combout\);

-- Location: PIN_J16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[2]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(2),
	combout => \ff~combout\(2));

-- Location: LCFF_X21_Y16_N3
\386|CLK_SEL[0]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~20\,
	sload => VCC,
	ena => \386|SUB_COMP~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|CLK_SEL\(0));

-- Location: LCCOMB_X21_Y16_N2
\45|47[0]~3\ : stratixii_lcell_comb
-- Equation(s):
-- \45|47[0]~3_combout\ = ( \412~0_combout\ & ( (\386|CLK_SEL\(0) & ((!\286~0_combout\) # (\ff~combout\(2)))) ) ) # ( !\412~0_combout\ & ( (!\286~0_combout\) # (\ff~combout\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111110101111101011111010100000000111101010000000011110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_ff~combout\(2),
	datac => \ALT_INV_286~0_combout\,
	datad => \386|ALT_INV_CLK_SEL\(0),
	dataf => \ALT_INV_412~0_combout\,
	combout => \45|47[0]~3_combout\);

-- Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(1),
	combout => \ff~combout\(1));

-- Location: LCFF_X21_Y14_N23
\386|FREQ_SEL[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~combout\,
	adatasdata => \LOCBUS~21\,
	sload => VCC,
	ena => \386|SUB_COMP~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|FREQ_SEL\(1));

-- Location: LCCOMB_X21_Y14_N20
\45|46[1]~2\ : stratixii_lcell_comb
-- Equation(s):
-- \45|46[1]~2_combout\ = ( \412~0_combout\ & ( (\386|FREQ_SEL\(1) & ((!\286~0_combout\) # (\ff~combout\(1)))) ) ) # ( !\412~0_combout\ & ( (!\286~0_combout\) # (\ff~combout\(1)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111110101111101011111010100110001001100010011000100110001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_ff~combout\(1),
	datab => \386|ALT_INV_FREQ_SEL\(1),
	datac => \ALT_INV_286~0_combout\,
	dataf => \ALT_INV_412~0_combout\,
	combout => \45|46[1]~2_combout\);

-- Location: LCFF_X21_Y16_N27
\386|FREQ_SEL[0]\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~22\,
	sload => VCC,
	ena => \386|SUB_COMP~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \386|FREQ_SEL\(0));

-- Location: PIN_Y7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\ff[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_ff(0),
	combout => \ff~combout\(0));

-- Location: LCCOMB_X21_Y16_N16
\45|46[0]~3\ : stratixii_lcell_comb
-- Equation(s):
-- \45|46[0]~3_combout\ = ( \ff~combout\(0) & ( (!\412~0_combout\) # (\386|FREQ_SEL\(0)) ) ) # ( !\ff~combout\(0) & ( (!\286~0_combout\ & ((!\412~0_combout\) # (\386|FREQ_SEL\(0)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000110010001100100011001000110010101111101011111010111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_412~0_combout\,
	datab => \ALT_INV_286~0_combout\,
	datac => \386|ALT_INV_FREQ_SEL\(0),
	dataf => \ALT_INV_ff~combout\(0),
	combout => \45|46[0]~3_combout\);

-- Location: PIN_N20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\clockext~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_clockext,
	combout => \clockext~combout\);

-- Location: CLKCTRL_G3
\clockext~clkctrl\ : stratixii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock")
-- pragma translate_on
PORT MAP (
	inclk => \clockext~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clockext~clkctrl_outclk\);

-- Location: LCCOMB_X21_Y15_N26
\386|Bypass_dilo~0\ : stratixii_lcell_comb
-- Equation(s):
-- \386|Bypass_dilo~0_combout\ = ( !\401\(3) & ( \401\(0) & ( (\LOC_CS~combout\ & (\LOC_RWn~combout\ & (\401\(1) & \401\(2)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000010000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LOC_CS~combout\,
	datab => \ALT_INV_LOC_RWn~combout\,
	datac => ALT_INV_401(1),
	datad => ALT_INV_401(2),
	datae => ALT_INV_401(3),
	dataf => ALT_INV_401(0),
	combout => \386|Bypass_dilo~0_combout\);

-- Location: LCFF_X21_Y14_N1
\46|52\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~15\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	ena => \386|Bypass_dilo~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \46|52~regout\);

-- Location: LCFF_X21_Y14_N9
\46|51\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~14\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	ena => \386|Bypass_dilo~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \46|51~regout\);

-- Location: LCFF_X21_Y14_N7
\46|56\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~19\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	ena => \386|Bypass_dilo~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \46|56~regout\);

-- Location: LCFF_X21_Y14_N29
\46|53\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~16\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	ena => \386|Bypass_dilo~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \46|53~regout\);

-- Location: LCFF_X21_Y14_N17
\46|55\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~18\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	ena => \386|Bypass_dilo~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \46|55~regout\);

-- Location: LCFF_X18_Y18_N1
\46|54\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~17\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	ena => \386|Bypass_dilo~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \46|54~regout\);

-- Location: LCFF_X21_Y14_N19
\46|57\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~20\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	ena => \386|Bypass_dilo~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \46|57~regout\);

-- Location: LCFF_X21_Y14_N13
\46|48\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~22\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	ena => \386|Bypass_dilo~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \46|48~regout\);

-- Location: LCFF_X21_Y14_N27
\46|49\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \LOCBUS~21\,
	aclr => \ALT_INV_RESETn~clkctrl_outclk\,
	sload => VCC,
	ena => \386|Bypass_dilo~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \46|49~regout\);

-- Location: LCCOMB_X19_Y18_N10
\88|DILO_SEL\ : stratixii_lcell_comb
-- Equation(s):
-- \88|DILO_SEL~combout\ = ( \88|LOAD_PED~regout\ ) # ( !\88|LOAD_PED~regout\ & ( ((\88|LOAD_PED3~regout\) # (\88|LOAD_PED2~regout\)) # (\88|DILO_SEL~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111101111111011111110111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_DILO_SEL~0_combout\,
	datab => \88|ALT_INV_LOAD_PED2~regout\,
	datac => \88|ALT_INV_LOAD_PED3~regout\,
	dataf => \88|ALT_INV_LOAD_PED~regout\,
	combout => \88|DILO_SEL~combout\);

-- Location: LCCOMB_X18_Y17_N8
\88|STRIN~4\ : stratixii_lcell_comb
-- Equation(s):
-- \88|STRIN~4_combout\ = ( !\88|READ_PED4~regout\ & ( (!\88|PED_MEAS3~regout\ & (!\88|PED_MEAS4~regout\ & !\88|READ_PED3~regout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000110000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \88|ALT_INV_PED_MEAS3~regout\,
	datac => \88|ALT_INV_PED_MEAS4~regout\,
	datad => \88|ALT_INV_READ_PED3~regout\,
	dataf => \88|ALT_INV_READ_PED4~regout\,
	combout => \88|STRIN~4_combout\);

-- Location: LCCOMB_X17_Y18_N28
\424\ : stratixii_lcell_comb
-- Equation(s):
-- \424~combout\ = LCELL(( \clockext~combout\ & ( (!\88|STRIN~4_combout\) # ((\88|LOAD_PED2~regout\) # (\88|RESET_DASY2~regout\)) ) ) # ( !\clockext~combout\ & ( (\88|LOAD_PED2~regout\) # (\88|RESET_DASY2~regout\) ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111000011111111111111001111111111111100111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \88|ALT_INV_STRIN~4_combout\,
	datac => \88|ALT_INV_RESET_DASY2~regout\,
	datad => \88|ALT_INV_LOAD_PED2~regout\,
	dataf => \ALT_INV_clockext~combout\,
	combout => \424~combout\);

-- Location: LCCOMB_X25_Y18_N28
\1|73|COUNT1[0]~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|COUNT1[0]~0_combout\ = ( !\1|73|COUNT1\(0) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000011111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \1|73|ALT_INV_COUNT1\(0),
	combout => \1|73|COUNT1[0]~0_combout\);

-- Location: LCCOMB_X26_Y17_N0
\1|27|5|count[0]~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|count[0]~0_combout\ = ( !\1|27|5|count\(0) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000011111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \1|27|5|ALT_INV_count\(0),
	combout => \1|27|5|count[0]~0_combout\);

-- Location: LCCOMB_X26_Y17_N30
\1|27|13~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|13~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \1|27|13~feeder_combout\);

-- Location: LCFF_X26_Y17_N31
\1|27|13\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLRG_O~combout\,
	datain => \1|27|13~feeder_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|27|13~regout\);

-- Location: LCFF_X26_Y17_N1
\1|27|5|count[0]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~clkctrl_outclk\,
	datain => \1|27|5|count[0]~0_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|27|13~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|27|5|count\(0));

-- Location: LCCOMB_X26_Y17_N16
\1|27|5|op_1~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|op_1~1_sumout\ = SUM(( \1|27|5|count\(1) ) + ( \1|27|5|count\(0) ) + ( !VCC ))
-- \1|27|5|op_1~2\ = CARRY(( \1|27|5|count\(1) ) + ( \1|27|5|count\(0) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|27|5|ALT_INV_count\(1),
	dataf => \1|27|5|ALT_INV_count\(0),
	cin => GND,
	sumout => \1|27|5|op_1~1_sumout\,
	cout => \1|27|5|op_1~2\);

-- Location: LCFF_X26_Y17_N17
\1|27|5|count[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~clkctrl_outclk\,
	datain => \1|27|5|op_1~1_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|27|13~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|27|5|count\(1));

-- Location: LCCOMB_X26_Y17_N18
\1|27|5|op_1~5\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|op_1~5_sumout\ = SUM(( \1|27|5|count\(2) ) + ( GND ) + ( \1|27|5|op_1~2\ ))
-- \1|27|5|op_1~6\ = CARRY(( \1|27|5|count\(2) ) + ( GND ) + ( \1|27|5|op_1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|27|5|ALT_INV_count\(2),
	cin => \1|27|5|op_1~2\,
	sumout => \1|27|5|op_1~5_sumout\,
	cout => \1|27|5|op_1~6\);

-- Location: LCFF_X26_Y17_N19
\1|27|5|count[2]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~clkctrl_outclk\,
	datain => \1|27|5|op_1~5_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|27|13~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|27|5|count\(2));

-- Location: LCCOMB_X25_Y17_N28
\1|27|5|BUSY_CLR~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|BUSY_CLR~1_combout\ = ( !\1|27|5|count\(1) & ( !\1|27|5|count\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|27|5|ALT_INV_count\(0),
	dataf => \1|27|5|ALT_INV_count\(1),
	combout => \1|27|5|BUSY_CLR~1_combout\);

-- Location: LCCOMB_X26_Y17_N20
\1|27|5|op_1~9\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|op_1~9_sumout\ = SUM(( \1|27|5|count\(3) ) + ( GND ) + ( \1|27|5|op_1~6\ ))
-- \1|27|5|op_1~10\ = CARRY(( \1|27|5|count\(3) ) + ( GND ) + ( \1|27|5|op_1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|27|5|ALT_INV_count\(3),
	cin => \1|27|5|op_1~6\,
	sumout => \1|27|5|op_1~9_sumout\,
	cout => \1|27|5|op_1~10\);

-- Location: LCFF_X26_Y17_N21
\1|27|5|count[3]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~clkctrl_outclk\,
	datain => \1|27|5|op_1~9_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|27|13~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|27|5|count\(3));

-- Location: LCCOMB_X26_Y17_N22
\1|27|5|op_1~13\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|op_1~13_sumout\ = SUM(( \1|27|5|count\(4) ) + ( GND ) + ( \1|27|5|op_1~10\ ))
-- \1|27|5|op_1~14\ = CARRY(( \1|27|5|count\(4) ) + ( GND ) + ( \1|27|5|op_1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|27|5|ALT_INV_count\(4),
	cin => \1|27|5|op_1~10\,
	sumout => \1|27|5|op_1~13_sumout\,
	cout => \1|27|5|op_1~14\);

-- Location: LCFF_X26_Y17_N23
\1|27|5|count[4]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~clkctrl_outclk\,
	datain => \1|27|5|op_1~13_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|27|13~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|27|5|count\(4));

-- Location: LCCOMB_X26_Y17_N24
\1|27|5|op_1~17\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|op_1~17_sumout\ = SUM(( \1|27|5|count\(5) ) + ( GND ) + ( \1|27|5|op_1~14\ ))
-- \1|27|5|op_1~18\ = CARRY(( \1|27|5|count\(5) ) + ( GND ) + ( \1|27|5|op_1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|27|5|ALT_INV_count\(5),
	cin => \1|27|5|op_1~14\,
	sumout => \1|27|5|op_1~17_sumout\,
	cout => \1|27|5|op_1~18\);

-- Location: LCFF_X26_Y17_N25
\1|27|5|count[5]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~clkctrl_outclk\,
	datain => \1|27|5|op_1~17_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|27|13~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|27|5|count\(5));

-- Location: LCCOMB_X26_Y17_N26
\1|27|5|op_1~21\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|op_1~21_sumout\ = SUM(( \1|27|5|count\(6) ) + ( GND ) + ( \1|27|5|op_1~18\ ))
-- \1|27|5|op_1~22\ = CARRY(( \1|27|5|count\(6) ) + ( GND ) + ( \1|27|5|op_1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|27|5|ALT_INV_count\(6),
	cin => \1|27|5|op_1~18\,
	sumout => \1|27|5|op_1~21_sumout\,
	cout => \1|27|5|op_1~22\);

-- Location: LCFF_X26_Y17_N27
\1|27|5|count[6]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~clkctrl_outclk\,
	datain => \1|27|5|op_1~21_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|27|13~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|27|5|count\(6));

-- Location: LCCOMB_X26_Y17_N28
\1|27|5|op_1~25\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|op_1~25_sumout\ = SUM(( \1|27|5|count\(7) ) + ( GND ) + ( \1|27|5|op_1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|27|5|ALT_INV_count\(7),
	cin => \1|27|5|op_1~22\,
	sumout => \1|27|5|op_1~25_sumout\);

-- Location: LCFF_X26_Y17_N29
\1|27|5|count[7]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~clkctrl_outclk\,
	datain => \1|27|5|op_1~25_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|27|13~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|27|5|count\(7));

-- Location: LCCOMB_X25_Y17_N20
\1|27|5|BUSY_CLR~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|BUSY_CLR~0_combout\ = ( \1|27|5|count\(7) & ( (!\1|27|5|count\(6) & (!\1|27|5|count\(3) & (\1|27|5|count\(5) & !\1|27|5|count\(4)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001000000000000000100000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|27|5|ALT_INV_count\(6),
	datab => \1|27|5|ALT_INV_count\(3),
	datac => \1|27|5|ALT_INV_count\(5),
	datad => \1|27|5|ALT_INV_count\(4),
	dataf => \1|27|5|ALT_INV_count\(7),
	combout => \1|27|5|BUSY_CLR~0_combout\);

-- Location: LCCOMB_X25_Y17_N14
\1|27|5|BUSY_CLEAR\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|5|BUSY_CLEAR~combout\ = ( \1|27|5|BUSY_CLR~0_combout\ & ( (!\RESETn~combout\) # ((!\1|27|5|count\(2) & \1|27|5|BUSY_CLR~1_combout\)) ) ) # ( !\1|27|5|BUSY_CLR~0_combout\ & ( !\RESETn~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010111110101010101011111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_RESETn~combout\,
	datac => \1|27|5|ALT_INV_count\(2),
	datad => \1|27|5|ALT_INV_BUSY_CLR~1_combout\,
	dataf => \1|27|5|ALT_INV_BUSY_CLR~0_combout\,
	combout => \1|27|5|BUSY_CLEAR~combout\);

-- Location: LCCOMB_X21_Y17_N16
\1|2|61~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|61~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \1|2|61~feeder_combout\);

-- Location: LCFF_X21_Y17_N17
\1|2|61\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|43~regout\,
	datain => \1|2|61~feeder_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|61~regout\);

-- Location: LCFF_X25_Y18_N29
\1|73|COUNT1[0]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|12|lpm_mux_component|$00009|result_node~clkctrl_outclk\,
	datain => \1|73|COUNT1[0]~0_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|2|61~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|COUNT1\(0));

-- Location: LCCOMB_X25_Y17_N0
\1|73|op_2~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_2~1_sumout\ = SUM(( \1|73|COUNT1\(1) ) + ( \1|73|COUNT1\(0) ) + ( !VCC ))
-- \1|73|op_2~2\ = CARRY(( \1|73|COUNT1\(1) ) + ( \1|73|COUNT1\(0) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011001100110000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \1|73|ALT_INV_COUNT1\(0),
	datad => \1|73|ALT_INV_COUNT1\(1),
	cin => GND,
	sumout => \1|73|op_2~1_sumout\,
	cout => \1|73|op_2~2\);

-- Location: LCFF_X25_Y17_N1
\1|73|COUNT1[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|12|lpm_mux_component|$00009|result_node~combout\,
	datain => \1|73|op_2~1_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|2|61~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|COUNT1\(1));

-- Location: LCCOMB_X25_Y17_N2
\1|73|op_2~5\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_2~5_sumout\ = SUM(( \1|73|COUNT1\(2) ) + ( GND ) + ( \1|73|op_2~2\ ))
-- \1|73|op_2~6\ = CARRY(( \1|73|COUNT1\(2) ) + ( GND ) + ( \1|73|op_2~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_COUNT1\(2),
	cin => \1|73|op_2~2\,
	sumout => \1|73|op_2~5_sumout\,
	cout => \1|73|op_2~6\);

-- Location: LCFF_X25_Y17_N3
\1|73|COUNT1[2]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|12|lpm_mux_component|$00009|result_node~combout\,
	datain => \1|73|op_2~5_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|2|61~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|COUNT1\(2));

-- Location: LCCOMB_X25_Y17_N4
\1|73|op_2~9\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_2~9_sumout\ = SUM(( \1|73|COUNT1\(3) ) + ( GND ) + ( \1|73|op_2~6\ ))
-- \1|73|op_2~10\ = CARRY(( \1|73|COUNT1\(3) ) + ( GND ) + ( \1|73|op_2~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_COUNT1\(3),
	cin => \1|73|op_2~6\,
	sumout => \1|73|op_2~9_sumout\,
	cout => \1|73|op_2~10\);

-- Location: LCFF_X25_Y17_N5
\1|73|COUNT1[3]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|12|lpm_mux_component|$00009|result_node~combout\,
	datain => \1|73|op_2~9_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|2|61~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|COUNT1\(3));

-- Location: LCCOMB_X25_Y17_N6
\1|73|op_2~13\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_2~13_sumout\ = SUM(( \1|73|COUNT1\(4) ) + ( GND ) + ( \1|73|op_2~10\ ))
-- \1|73|op_2~14\ = CARRY(( \1|73|COUNT1\(4) ) + ( GND ) + ( \1|73|op_2~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_COUNT1\(4),
	cin => \1|73|op_2~10\,
	sumout => \1|73|op_2~13_sumout\,
	cout => \1|73|op_2~14\);

-- Location: LCFF_X25_Y17_N7
\1|73|COUNT1[4]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|12|lpm_mux_component|$00009|result_node~combout\,
	datain => \1|73|op_2~13_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|2|61~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|COUNT1\(4));

-- Location: LCCOMB_X21_Y16_N28
\1|6|refg[6]~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|6|refg[6]~0_combout\ = ( \386|CLK_SEL\(1) & ( !\386|CLK_SEL\(0) ) ) # ( !\386|CLK_SEL\(1) & ( \386|CLK_SEL\(0) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \386|ALT_INV_CLK_SEL\(0),
	dataf => \386|ALT_INV_CLK_SEL\(1),
	combout => \1|6|refg[6]~0_combout\);

-- Location: LCFF_X21_Y16_N29
\1|6|refg[6]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~combout\,
	datain => \1|6|refg[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|6|refg\(6));

-- Location: LCCOMB_X25_Y17_N8
\1|73|op_2~17\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_2~17_sumout\ = SUM(( \1|73|COUNT1\(5) ) + ( GND ) + ( \1|73|op_2~14\ ))
-- \1|73|op_2~18\ = CARRY(( \1|73|COUNT1\(5) ) + ( GND ) + ( \1|73|op_2~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_COUNT1\(5),
	cin => \1|73|op_2~14\,
	sumout => \1|73|op_2~17_sumout\,
	cout => \1|73|op_2~18\);

-- Location: LCFF_X25_Y17_N9
\1|73|COUNT1[5]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|12|lpm_mux_component|$00009|result_node~combout\,
	datain => \1|73|op_2~17_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|2|61~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|COUNT1\(5));

-- Location: LCFF_X25_Y17_N11
\1|73|COUNT1[6]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|12|lpm_mux_component|$00009|result_node~combout\,
	datain => \1|73|op_2~21_sumout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	ena => \1|2|61~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|COUNT1\(6));

-- Location: LCCOMB_X21_Y16_N0
\1|6|refg[5]~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|6|refg[5]~1_combout\ = ( !\386|CLK_SEL\(0) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \386|ALT_INV_CLK_SEL\(0),
	combout => \1|6|refg[5]~1_combout\);

-- Location: LCFF_X21_Y16_N1
\1|6|refg[5]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~combout\,
	datain => \1|6|refg[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|6|refg\(5));

-- Location: LCCOMB_X21_Y16_N30
\1|6|_~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|6|_~0_combout\ = ( \386|CLK_SEL\(0) & ( \386|CLK_SEL\(1) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \386|ALT_INV_CLK_SEL\(1),
	dataf => \386|ALT_INV_CLK_SEL\(0),
	combout => \1|6|_~0_combout\);

-- Location: LCFF_X21_Y16_N31
\1|6|refg[7]\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~combout\,
	datain => \1|6|_~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|6|refg\(7));

-- Location: LCCOMB_X22_Y17_N24
\1|73|CLRADD$wire~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLRADD$wire~0_combout\ = ( \1|73|COUNT1\(5) & ( \1|6|refg\(7) & ( (\1|73|COUNT1\(7) & (\1|6|refg\(5) & (!\1|6|refg\(6) $ (\1|73|COUNT1\(6))))) ) ) ) # ( !\1|73|COUNT1\(5) & ( \1|6|refg\(7) & ( (\1|73|COUNT1\(7) & (!\1|6|refg\(5) & (!\1|6|refg\(6) $ 
-- (\1|73|COUNT1\(6))))) ) ) ) # ( \1|73|COUNT1\(5) & ( !\1|6|refg\(7) & ( (!\1|73|COUNT1\(7) & (\1|6|refg\(5) & (!\1|6|refg\(6) $ (\1|73|COUNT1\(6))))) ) ) ) # ( !\1|73|COUNT1\(5) & ( !\1|6|refg\(7) & ( (!\1|73|COUNT1\(7) & (!\1|6|refg\(5) & (!\1|6|refg\(6) 
-- $ (\1|73|COUNT1\(6))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000001000000000000000001000001001000001000000000000000001000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|73|ALT_INV_COUNT1\(7),
	datab => \1|6|ALT_INV_refg\(6),
	datac => \1|73|ALT_INV_COUNT1\(6),
	datad => \1|6|ALT_INV_refg\(5),
	datae => \1|73|ALT_INV_COUNT1\(5),
	dataf => \1|6|ALT_INV_refg\(7),
	combout => \1|73|CLRADD$wire~0_combout\);

-- Location: LCCOMB_X22_Y17_N18
\1|73|CLRADD$wire~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLRADD$wire~1_combout\ = ( !\1|73|COUNT1\(0) & ( !\1|73|COUNT1\(2) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_COUNT1\(2),
	dataf => \1|73|ALT_INV_COUNT1\(0),
	combout => \1|73|CLRADD$wire~1_combout\);

-- Location: LCCOMB_X22_Y17_N14
\1|73|CLRADD$wire\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLRADD$wire~combout\ = LCELL(( \1|73|CLRADD$wire~1_combout\ & ( (!\1|73|COUNT1\(1) & (\1|73|COUNT1\(4) & (\1|73|CLRADD$wire~0_combout\ & !\1|73|COUNT1\(3)))) ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000010000000000000001000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|73|ALT_INV_COUNT1\(1),
	datab => \1|73|ALT_INV_COUNT1\(4),
	datac => \1|73|ALT_INV_CLRADD$wire~0_combout\,
	datad => \1|73|ALT_INV_COUNT1\(3),
	dataf => \1|73|ALT_INV_CLRADD$wire~1_combout\,
	combout => \1|73|CLRADD$wire~combout\);

-- Location: PIN_A10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\INHIBIT~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_INHIBIT,
	combout => \INHIBIT~combout\);

-- Location: LCCOMB_X25_Y17_N22
\1|27|7~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|7~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \1|27|7~feeder_combout\);

-- Location: LCFF_X25_Y17_N23
\1|27|7\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|61~regout\,
	datain => \1|27|7~feeder_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|27|7~regout\);

-- Location: LCCOMB_X17_Y18_N8
\1|27|9\ : stratixii_lcell_comb
-- Equation(s):
-- \1|27|9~combout\ = ( \1|27|7~regout\ ) # ( !\1|27|7~regout\ & ( \INHIBIT~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_INHIBIT~combout\,
	dataf => \1|27|ALT_INV_7~regout\,
	combout => \1|27|9~combout\);

-- Location: LCCOMB_X22_Y17_N28
\1|73|CLR_G~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLR_G~0_combout\ = ( \1|73|COUNT1\(1) & ( !\1|73|COUNT1\(4) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_COUNT1\(4),
	dataf => \1|73|ALT_INV_COUNT1\(1),
	combout => \1|73|CLR_G~0_combout\);

-- Location: LCCOMB_X25_Y17_N30
\1|2|9~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|9~0_combout\ = !\1|2|9~regout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|2|ALT_INV_9~regout\,
	combout => \1|2|9~0_combout\);

-- Location: LCFF_X25_Y17_N31
\1|2|9\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|12|lpm_mux_component|$00009|result_node~combout\,
	datain => \1|2|9~0_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|9~regout\);

-- Location: LCCOMB_X25_Y17_N16
\1|74\ : stratixii_lcell_comb
-- Equation(s):
-- \1|74~combout\ = LCELL(( \1|2|9~regout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \1|2|ALT_INV_9~regout\,
	combout => \1|74~combout\);

-- Location: LCCOMB_X25_Y17_N18
\1|73|CLKD_a~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKD_a~0_combout\ = ( \1|73|COUNT1\(0) & ( (\1|73|COUNT1\(3) & \1|73|COUNT1\(2)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000011110000000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \1|73|ALT_INV_COUNT1\(3),
	datad => \1|73|ALT_INV_COUNT1\(2),
	dataf => \1|73|ALT_INV_COUNT1\(0),
	combout => \1|73|CLKD_a~0_combout\);

-- Location: LCCOMB_X22_Y17_N16
\1|73|CLRG_O\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLRG_O~combout\ = LCELL(( \1|73|CLKD_a~0_combout\ & ( (!\RESETn~combout\) # ((\1|73|CLR_G~0_combout\ & (\1|74~combout\ & \1|73|CLRADD$wire~0_combout\))) ) ) # ( !\1|73|CLKD_a~0_combout\ & ( !\RESETn~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010111010101010101011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_RESETn~combout\,
	datab => \1|73|ALT_INV_CLR_G~0_combout\,
	datac => \1|ALT_INV_74~combout\,
	datad => \1|73|ALT_INV_CLRADD$wire~0_combout\,
	dataf => \1|73|ALT_INV_CLKD_a~0_combout\,
	combout => \1|73|CLRG_O~combout\);

-- Location: LCFF_X21_Y15_N17
\359\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \421~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \359~regout\);

-- Location: LCFF_X21_Y15_N7
\357\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \359~regout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \357~regout\);

-- Location: LCFF_X21_Y15_N25
\397\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \357~regout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \397~regout\);

-- Location: LCFF_X21_Y15_N13
\398\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \397~regout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \398~regout\);

-- Location: LCFF_X22_Y14_N17
\399\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	adatasdata => \398~regout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \399~regout\);

-- Location: LCCOMB_X22_Y14_N16
\400\ : stratixii_lcell_comb
-- Equation(s):
-- \400~combout\ = ( \399~regout\ & ( \359~regout\ ) ) # ( !\399~regout\ & ( \359~regout\ ) ) # ( \399~regout\ & ( !\359~regout\ ) ) # ( !\399~regout\ & ( !\359~regout\ & ( ((\398~regout\) # (\397~regout\)) # (\357~regout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111101111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_357~regout\,
	datab => \ALT_INV_397~regout\,
	datac => \ALT_INV_398~regout\,
	datae => \ALT_INV_399~regout\,
	dataf => \ALT_INV_359~regout\,
	combout => \400~combout\);

-- Location: LCFF_X17_Y18_N9
\352\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \1|27|9~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \352~regout\);

-- Location: LCFF_X17_Y18_N13
\378\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~combout\,
	adatasdata => \352~regout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \378~regout\);

-- Location: PIN_G12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\TRIG~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_TRIG,
	combout => \TRIG~combout\);

-- Location: LCCOMB_X17_Y18_N12
\350\ : stratixii_lcell_comb
-- Equation(s):
-- \350~combout\ = LCELL(( !\378~regout\ & ( \TRIG~combout\ & ( (\386|DILOFC\(3) & (!\386|DILOFC\(0) & (!\386|DILOFC\(2) & \386|DILOFC\(1)))) ) ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000010000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \386|ALT_INV_DILOFC\(3),
	datab => \386|ALT_INV_DILOFC\(0),
	datac => \386|ALT_INV_DILOFC\(2),
	datad => \386|ALT_INV_DILOFC\(1),
	datae => \ALT_INV_378~regout\,
	dataf => \ALT_INV_TRIG~combout\,
	combout => \350~combout\);

-- Location: LCCOMB_X17_Y18_N24
\360~0\ : stratixii_lcell_comb
-- Equation(s):
-- \360~0_combout\ = !\350~combout\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_350~combout\,
	combout => \360~0_combout\);

-- Location: LCFF_X17_Y18_N25
\360\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \360~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \360~regout\);

-- Location: LCFF_X17_Y18_N11
\367\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \360~regout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \367~regout\);

-- Location: LCCOMB_X17_Y18_N10
\368\ : stratixii_lcell_comb
-- Equation(s):
-- \368~combout\ = (!\350~combout\ & (\360~regout\ & \367~regout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001010000000000000101000000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_350~combout\,
	datac => \ALT_INV_360~regout\,
	datad => \ALT_INV_367~regout\,
	combout => \368~combout\);

-- Location: LCCOMB_X23_Y16_N18
\88|FCO[0]~0\ : stratixii_lcell_comb
-- Equation(s):
-- \88|FCO[0]~0_combout\ = ( !\88|RESET_DASY~regout\ & ( !\88|RESET_DASY2~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \88|ALT_INV_RESET_DASY2~regout\,
	dataf => \88|ALT_INV_RESET_DASY~regout\,
	combout => \88|FCO[0]~0_combout\);

-- Location: LCCOMB_X18_Y18_N18
\88|ENIN1~3\ : stratixii_lcell_comb
-- Equation(s):
-- \88|ENIN1~3_combout\ = ( !\88|PED_MEAS~regout\ & ( (!\88|READ_END~regout\ & (!\88|READ_PED~regout\ & !\88|LOAD_PED3~regout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1100000000000000110000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \88|ALT_INV_READ_END~regout\,
	datac => \88|ALT_INV_READ_PED~regout\,
	datad => \88|ALT_INV_LOAD_PED3~regout\,
	dataf => \88|ALT_INV_PED_MEAS~regout\,
	combout => \88|ENIN1~3_combout\);

-- Location: LCCOMB_X23_Y16_N16
\88|ENIN1\ : stratixii_lcell_comb
-- Equation(s):
-- \88|ENIN1~combout\ = ( \88|ENIN1~3_combout\ & ( (!\88|ENIN1~2_combout\) # (!\88|FCO[0]~0_combout\) ) ) # ( !\88|ENIN1~3_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111101110111011101110111011101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_ENIN1~2_combout\,
	datab => \88|ALT_INV_FCO[0]~0_combout\,
	dataf => \88|ALT_INV_ENIN1~3_combout\,
	combout => \88|ENIN1~combout\);

-- Location: LCCOMB_X22_Y16_N16
\1|2|89~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|89~0_combout\ = ( !\1|2|89~regout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000011111111111111110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \1|2|ALT_INV_89~regout\,
	combout => \1|2|89~0_combout\);

-- Location: LCFF_X22_Y16_N17
\1|2|89\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|12|lpm_mux_component|$00009|ALT_INV_result_node~clkctrl_outclk\,
	datain => \1|2|89~0_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|89~regout\);

-- Location: LCCOMB_X22_Y17_N12
\1|73|CLKD~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKD~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \1|73|CLKD~feeder_combout\);

-- Location: LCFF_X22_Y17_N13
\1|73|CLKD\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLKD_a~combout\,
	datain => \1|73|CLKD~feeder_combout\,
	aclr => \1|73|CLRADD$wire~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|CLKD~regout\);

-- Location: LCCOMB_X21_Y17_N0
\1|73|CLKD_O\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKD_O~combout\ = LCELL(( \1|73|CLKD~regout\ & ( !\1|2|89~regout\ ) ) # ( !\1|73|CLKD~regout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|2|ALT_INV_89~regout\,
	dataf => \1|73|ALT_INV_CLKD~regout\,
	combout => \1|73|CLKD_O~combout\);

-- Location: LCCOMB_X21_Y17_N4
\1|85\ : stratixii_lcell_comb
-- Equation(s):
-- \1|85~combout\ = LCELL(( \1|73|CLRG_O~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \1|73|ALT_INV_CLRG_O~combout\,
	combout => \1|85~combout\);

-- Location: LCCOMB_X21_Y17_N2
\1|46\ : stratixii_lcell_comb
-- Equation(s):
-- \1|46~combout\ = ( \1|85~combout\ ) # ( !\1|85~combout\ & ( !\1|73|CLKD_O~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|73|ALT_INV_CLKD_O~combout\,
	dataf => \1|ALT_INV_85~combout\,
	combout => \1|46~combout\);

-- Location: LCCOMB_X21_Y17_N24
\1|2|85~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|85~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \1|2|85~feeder_combout\);

-- Location: LCFF_X21_Y17_N25
\1|2|85\ : stratixii_lcell_ff
PORT MAP (
	clk => \350~clkctrl_outclk\,
	datain => \1|2|85~feeder_combout\,
	aclr => \1|27|5|BUSY_CLEAR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|85~regout\);

-- Location: LCCOMB_X21_Y17_N26
\1|87\ : stratixii_lcell_comb
-- Equation(s):
-- \1|87~combout\ = LCELL((!\CLOCK_IN~combout\) # (!\1|2|85~regout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111101011111010111110101111101011111010111110101111101011111010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_CLOCK_IN~combout\,
	datac => \1|2|ALT_INV_85~regout\,
	combout => \1|87~combout\);

-- Location: LCCOMB_X21_Y17_N12
\1|88\ : stratixii_lcell_comb
-- Equation(s):
-- \1|88~combout\ = LCELL(( \1|87~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \1|ALT_INV_87~combout\,
	combout => \1|88~combout\);

-- Location: LCCOMB_X21_Y17_N14
\1|89\ : stratixii_lcell_comb
-- Equation(s):
-- \1|89~combout\ = LCELL(\1|88~combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|ALT_INV_88~combout\,
	combout => \1|89~combout\);

-- Location: LCCOMB_X21_Y17_N6
\1|2|81~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|81~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \1|2|81~feeder_combout\);

-- Location: LCCOMB_X21_Y17_N18
\1|2|83\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|83~combout\ = ( !\1|2|61~regout\ & ( !\INHIBIT~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \ALT_INV_INHIBIT~combout\,
	dataf => \1|2|ALT_INV_61~regout\,
	combout => \1|2|83~combout\);

-- Location: LCFF_X21_Y17_N7
\1|2|81\ : stratixii_lcell_ff
PORT MAP (
	clk => \350~clkctrl_outclk\,
	datain => \1|2|81~feeder_combout\,
	aclr => \1|73|CLRG_O~combout\,
	ena => \1|2|83~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|81~regout\);

-- Location: LCCOMB_X19_Y17_N16
\1|2|43~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|43~feeder_combout\ = ( \1|2|81~regout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \1|2|ALT_INV_81~regout\,
	combout => \1|2|43~feeder_combout\);

-- Location: LCFF_X19_Y17_N17
\1|2|43\ : stratixii_lcell_ff
PORT MAP (
	clk => \CLOCK_IN~clkctrl_outclk\,
	datain => \1|2|43~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|43~regout\);

-- Location: LCCOMB_X21_Y17_N20
\1|2|49~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \1|2|49~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \1|2|49~feeder_combout\);

-- Location: LCFF_X21_Y17_N21
\1|2|49\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|2|43~regout\,
	datain => \1|2|49~feeder_combout\,
	aclr => \1|73|CLRG_O~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|2|49~regout\);

-- Location: LCCOMB_X23_Y17_N22
\1|73|CLKA~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKA~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \1|73|CLKA~feeder_combout\);

-- Location: LCCOMB_X22_Y17_N20
\1|73|CLR_A~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLR_A~0_combout\ = ( \1|73|CLRADD$wire~0_combout\ & ( (\1|73|COUNT1\(3) & (\1|73|CLRADD$wire~1_combout\ & \1|73|CLR_G~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000001010000000000000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|73|ALT_INV_COUNT1\(3),
	datac => \1|73|ALT_INV_CLRADD$wire~1_combout\,
	datad => \1|73|ALT_INV_CLR_G~0_combout\,
	dataf => \1|73|ALT_INV_CLRADD$wire~0_combout\,
	combout => \1|73|CLR_A~0_combout\);

-- Location: LCFF_X23_Y17_N23
\1|73|CLKA\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLKA_a~combout\,
	datain => \1|73|CLKA~feeder_combout\,
	aclr => \1|73|CLR_A~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|CLKA~regout\);

-- Location: LCCOMB_X21_Y17_N8
\1|73|CLKA_O\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKA_O~combout\ = (\1|73|CLKA~regout\ & !\1|2|89~regout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000000001100110000000000110011000000000011001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \1|73|ALT_INV_CLKA~regout\,
	datad => \1|2|ALT_INV_89~regout\,
	combout => \1|73|CLKA_O~combout\);

-- Location: LCCOMB_X23_Y17_N26
\1|73|CLKG~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKG~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \1|73|CLKG~feeder_combout\);

-- Location: LCCOMB_X22_Y17_N22
\1|73|CLR_G~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLR_G~1_combout\ = ( \1|73|CLRADD$wire~0_combout\ & ( (!\1|73|COUNT1\(3) & (\1|73|CLRADD$wire~1_combout\ & \1|73|CLR_G~0_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \1|73|ALT_INV_COUNT1\(3),
	datac => \1|73|ALT_INV_CLRADD$wire~1_combout\,
	datad => \1|73|ALT_INV_CLR_G~0_combout\,
	dataf => \1|73|ALT_INV_CLRADD$wire~0_combout\,
	combout => \1|73|CLR_G~1_combout\);

-- Location: LCFF_X23_Y17_N27
\1|73|CLKG\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLKG_a~combout\,
	datain => \1|73|CLKG~feeder_combout\,
	aclr => \1|73|CLR_G~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|CLKG~regout\);

-- Location: LCCOMB_X23_Y17_N16
\1|73|CLKG_O\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|CLKG_O~combout\ = LCELL(( \1|73|CLKG~regout\ & ( \1|74~combout\ ) ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \1|ALT_INV_74~combout\,
	dataf => \1|73|ALT_INV_CLKG~regout\,
	combout => \1|73|CLKG_O~combout\);

-- Location: LCCOMB_X19_Y18_N0
\88|WR_FIFO\ : stratixii_lcell_comb
-- Equation(s):
-- \88|WR_FIFO~combout\ = ( \88|DATA_WRITE_ON_FIFO~regout\ ) # ( !\88|DATA_WRITE_ON_FIFO~regout\ & ( (\88|PED_MEAS4~regout\) # (\88|READ_PED4~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101011111010111110101111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_READ_PED4~regout\,
	datac => \88|ALT_INV_PED_MEAS4~regout\,
	dataf => \88|ALT_INV_DATA_WRITE_ON_FIFO~regout\,
	combout => \88|WR_FIFO~combout\);

-- Location: LCCOMB_X18_Y17_N18
\88|RD_FIFO\ : stratixii_lcell_comb
-- Equation(s):
-- \88|RD_FIFO~combout\ = ( \88|DATA_READ~regout\ ) # ( !\88|DATA_READ~regout\ & ( (\88|LOAD_PED1~regout\) # (\88|LOAD_PULSE~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111111111111000011111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \88|ALT_INV_LOAD_PULSE~regout\,
	datad => \88|ALT_INV_LOAD_PED1~regout\,
	dataf => \88|ALT_INV_DATA_READ~regout\,
	combout => \88|RD_FIFO~combout\);

-- Location: LCCOMB_X21_Y15_N4
\425\ : stratixii_lcell_comb
-- Equation(s):
-- \425~combout\ = ( \88|CLEAR_FIFO~4_combout\ & ( \RESETn~combout\ ) ) # ( !\88|CLEAR_FIFO~4_combout\ & ( \RESETn~combout\ & ( ((\386|FC\(1) & (!\88|IDLE~regout\ & \386|FC\(2)))) # (\88|CLEAR_FIFO~3_combout\) ) ) ) # ( \88|CLEAR_FIFO~4_combout\ & ( 
-- !\RESETn~combout\ ) ) # ( !\88|CLEAR_FIFO~4_combout\ & ( !\RESETn~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100001111010011111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \386|ALT_INV_FC\(1),
	datab => \88|ALT_INV_IDLE~regout\,
	datac => \88|ALT_INV_CLEAR_FIFO~3_combout\,
	datad => \386|ALT_INV_FC\(2),
	datae => \88|ALT_INV_CLEAR_FIFO~4_combout\,
	dataf => \ALT_INV_RESETn~combout\,
	combout => \425~combout\);

-- Location: LCCOMB_X22_Y15_N20
\199|DILO10~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|DILO10~0_combout\ = (!\EN_OUT10~combout\ & (\199|DILO9~regout\ & (!\EN_OUT9_IN10~0\))) # (\EN_OUT10~combout\ & (((\199|DILO9~regout\ & !\EN_OUT9_IN10~0\)) # (\199|DILO10~regout\)))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011000001110101001100000111010100110000011101010011000001110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_EN_OUT10~combout\,
	datab => \199|ALT_INV_DILO9~regout\,
	datac => \ALT_INV_EN_OUT9_IN10~0\,
	datad => \199|ALT_INV_DILO10~regout\,
	combout => \199|DILO10~0_combout\);

-- Location: LCCOMB_X22_Y15_N22
\199|READ_STATE~14\ : stratixii_lcell_comb
-- Equation(s):
-- \199|READ_STATE~14_combout\ = ( !\88|RESET_DASY~regout\ & ( !\RESETn~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_RESETn~combout\,
	dataf => \88|ALT_INV_RESET_DASY~regout\,
	combout => \199|READ_STATE~14_combout\);

-- Location: LCFF_X22_Y15_N21
\199|DILO10\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|DILO10~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|DILO10~regout\);

-- Location: LCCOMB_X17_Y18_N26
\417~feeder\ : stratixii_lcell_comb
-- Equation(s):
-- \417~feeder_combout\ = VCC

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	combout => \417~feeder_combout\);

-- Location: LCFF_X17_Y18_N27
\417\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \417~feeder_combout\,
	aclr => \88|RESET_DASY~regout\,
	ena => \350~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \417~regout\);

-- Location: LCCOMB_X23_Y15_N4
\199|IDLE~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|IDLE~0_combout\ = ( \417~regout\ & ( (!\199|DILO10~regout\) # (\EN_OUT10~combout\) ) ) # ( !\417~regout\ & ( (\199|IDLE~regout\ & ((!\199|DILO10~regout\) # (\EN_OUT10~combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110011000000001111001111110011111100111111001111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_EN_OUT10~combout\,
	datac => \199|ALT_INV_DILO10~regout\,
	datad => \199|ALT_INV_IDLE~regout\,
	dataf => \ALT_INV_417~regout\,
	combout => \199|IDLE~0_combout\);

-- Location: LCFF_X23_Y15_N5
\199|IDLE\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|IDLE~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|IDLE~regout\);

-- Location: LCCOMB_X22_Y15_N14
\199|WAITING_STATE~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|WAITING_STATE~0_combout\ = ( \199|WAITING_STATE~regout\ & ( \88|ENIN1~2_combout\ & ( (!\88|FCO[0]~0_combout\) # ((!\88|ENIN1~3_combout\) # ((\417~regout\ & !\199|IDLE~regout\))) ) ) ) # ( !\199|WAITING_STATE~regout\ & ( \88|ENIN1~2_combout\ & ( 
-- (\417~regout\ & !\199|IDLE~regout\) ) ) ) # ( \199|WAITING_STATE~regout\ & ( !\88|ENIN1~2_combout\ ) ) # ( !\199|WAITING_STATE~regout\ & ( !\88|ENIN1~2_combout\ & ( (\417~regout\ & !\199|IDLE~regout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010001000100111111111111111101000100010001001111111111110100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_417~regout\,
	datab => \199|ALT_INV_IDLE~regout\,
	datac => \88|ALT_INV_FCO[0]~0_combout\,
	datad => \88|ALT_INV_ENIN1~3_combout\,
	datae => \199|ALT_INV_WAITING_STATE~regout\,
	dataf => \88|ALT_INV_ENIN1~2_combout\,
	combout => \199|WAITING_STATE~0_combout\);

-- Location: LCFF_X22_Y15_N15
\199|WAITING_STATE\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|WAITING_STATE~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|WAITING_STATE~regout\);

-- Location: LCCOMB_X22_Y15_N8
\199|DILO1~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|DILO1~0_combout\ = ( \199|DILO1~regout\ & ( \88|ENIN1~2_combout\ & ( ((\199|WAITING_STATE~regout\ & (\88|ENIN1~3_combout\ & \88|FCO[0]~0_combout\))) # (\EN_OUT1_IN2~0\) ) ) ) # ( !\199|DILO1~regout\ & ( \88|ENIN1~2_combout\ & ( 
-- (\199|WAITING_STATE~regout\ & (\88|ENIN1~3_combout\ & \88|FCO[0]~0_combout\)) ) ) ) # ( \199|DILO1~regout\ & ( !\88|ENIN1~2_combout\ & ( \EN_OUT1_IN2~0\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000010101010101010100000000000000110101010101010111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_EN_OUT1_IN2~0\,
	datab => \199|ALT_INV_WAITING_STATE~regout\,
	datac => \88|ALT_INV_ENIN1~3_combout\,
	datad => \88|ALT_INV_FCO[0]~0_combout\,
	datae => \199|ALT_INV_DILO1~regout\,
	dataf => \88|ALT_INV_ENIN1~2_combout\,
	combout => \199|DILO1~0_combout\);

-- Location: LCFF_X22_Y15_N9
\199|DILO1\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|DILO1~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|DILO1~regout\);

-- Location: LCCOMB_X22_Y15_N6
\199|DILO2~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|DILO2~0_combout\ = ( \199|DILO1~regout\ & ( (!\EN_OUT1_IN2~0\) # ((\EN_OUT2_IN3~0\ & \199|DILO2~regout\)) ) ) # ( !\199|DILO1~regout\ & ( (\EN_OUT2_IN3~0\ & \199|DILO2~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001110101010101110111010101010111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_EN_OUT1_IN2~0\,
	datab => \ALT_INV_EN_OUT2_IN3~0\,
	datad => \199|ALT_INV_DILO2~regout\,
	dataf => \199|ALT_INV_DILO1~regout\,
	combout => \199|DILO2~0_combout\);

-- Location: LCFF_X22_Y15_N7
\199|DILO2\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|DILO2~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|DILO2~regout\);

-- Location: LCCOMB_X22_Y15_N4
\199|DILO3~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|DILO3~0_combout\ = ( \199|DILO2~regout\ & ( (!\EN_OUT2_IN3~0\) # ((\EN_OUT3_IN4~0\ & \199|DILO3~regout\)) ) ) # ( !\199|DILO2~regout\ & ( (\EN_OUT3_IN4~0\ & \199|DILO3~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111111001100110011111100110011001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_EN_OUT2_IN3~0\,
	datac => \ALT_INV_EN_OUT3_IN4~0\,
	datad => \199|ALT_INV_DILO3~regout\,
	dataf => \199|ALT_INV_DILO2~regout\,
	combout => \199|DILO3~0_combout\);

-- Location: LCFF_X22_Y15_N5
\199|DILO3\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|DILO3~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|DILO3~regout\);

-- Location: LCCOMB_X22_Y15_N0
\199|DILO4~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|DILO4~0_combout\ = ( \199|DILO3~regout\ & ( (!\EN_OUT3_IN4~0\) # ((\EN_OUT4_IN5~0\ & \199|DILO4~regout\)) ) ) # ( !\199|DILO3~regout\ & ( (\EN_OUT4_IN5~0\ & \199|DILO4~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010111110000111101011111000011110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_EN_OUT4_IN5~0\,
	datac => \ALT_INV_EN_OUT3_IN4~0\,
	datad => \199|ALT_INV_DILO4~regout\,
	dataf => \199|ALT_INV_DILO3~regout\,
	combout => \199|DILO4~0_combout\);

-- Location: LCFF_X22_Y15_N1
\199|DILO4\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|DILO4~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|DILO4~regout\);

-- Location: LCCOMB_X22_Y15_N2
\199|DILO5~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|DILO5~0_combout\ = ( \199|DILO4~regout\ & ( (!\EN_OUT4_IN5~0\) # ((\EN_OUT5_IN6~0\ & \199|DILO5~regout\)) ) ) # ( !\199|DILO4~regout\ & ( (\EN_OUT5_IN6~0\ & \199|DILO5~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111110101010101011111010101010101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_EN_OUT4_IN5~0\,
	datac => \ALT_INV_EN_OUT5_IN6~0\,
	datad => \199|ALT_INV_DILO5~regout\,
	dataf => \199|ALT_INV_DILO4~regout\,
	combout => \199|DILO5~0_combout\);

-- Location: LCFF_X22_Y15_N3
\199|DILO5\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|DILO5~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|DILO5~regout\);

-- Location: LCCOMB_X22_Y15_N30
\199|DILO6~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|DILO6~0_combout\ = ( \199|DILO5~regout\ & ( (!\EN_OUT5_IN6~0\) # ((\EN_OUT6_IN7~0\ & \199|DILO6~regout\)) ) ) # ( !\199|DILO5~regout\ & ( (\EN_OUT6_IN7~0\ & \199|DILO6~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000000000011001111110000111100111111000011110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_EN_OUT6_IN7~0\,
	datac => \ALT_INV_EN_OUT5_IN6~0\,
	datad => \199|ALT_INV_DILO6~regout\,
	dataf => \199|ALT_INV_DILO5~regout\,
	combout => \199|DILO6~0_combout\);

-- Location: LCFF_X22_Y15_N31
\199|DILO6\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|DILO6~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|DILO6~regout\);

-- Location: LCCOMB_X22_Y15_N28
\199|DILO7~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|DILO7~0_combout\ = ( \EN_OUT7_IN8~0\ & ( ((!\EN_OUT6_IN7~0\ & \199|DILO6~regout\)) # (\199|DILO7~regout\) ) ) # ( !\EN_OUT7_IN8~0\ & ( (!\EN_OUT6_IN7~0\ & \199|DILO6~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100000011000000110000001100111111110000110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_EN_OUT6_IN7~0\,
	datac => \199|ALT_INV_DILO6~regout\,
	datad => \199|ALT_INV_DILO7~regout\,
	dataf => \ALT_INV_EN_OUT7_IN8~0\,
	combout => \199|DILO7~0_combout\);

-- Location: LCFF_X22_Y15_N29
\199|DILO7\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|DILO7~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|DILO7~regout\);

-- Location: LCCOMB_X22_Y15_N26
\199|DILO8~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|DILO8~0_combout\ = ( \EN_OUT7_IN8~0\ & ( (\EN_OUT8_IN9~0\ & \199|DILO8~regout\) ) ) # ( !\EN_OUT7_IN8~0\ & ( ((\EN_OUT8_IN9~0\ & \199|DILO8~regout\)) # (\199|DILO7~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101011111000011110101111100000000010101010000000001010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_EN_OUT8_IN9~0\,
	datac => \199|ALT_INV_DILO7~regout\,
	datad => \199|ALT_INV_DILO8~regout\,
	dataf => \ALT_INV_EN_OUT7_IN8~0\,
	combout => \199|DILO8~0_combout\);

-- Location: LCFF_X22_Y15_N27
\199|DILO8\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|DILO8~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|DILO8~regout\);

-- Location: LCCOMB_X22_Y15_N24
\199|DILO9~0\ : stratixii_lcell_comb
-- Equation(s):
-- \199|DILO9~0_combout\ = ( \199|DILO8~regout\ & ( (!\EN_OUT8_IN9~0\) # ((\EN_OUT9_IN10~0\ & \199|DILO9~regout\)) ) ) # ( !\199|DILO8~regout\ & ( (\EN_OUT9_IN10~0\ & \199|DILO9~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111000000000000111110101010101011111010101010101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_EN_OUT8_IN9~0\,
	datac => \ALT_INV_EN_OUT9_IN10~0\,
	datad => \199|ALT_INV_DILO9~regout\,
	dataf => \199|ALT_INV_DILO8~regout\,
	combout => \199|DILO9~0_combout\);

-- Location: LCFF_X22_Y15_N25
\199|DILO9\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \199|DILO9~0_combout\,
	aclr => \199|READ_STATE~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \199|DILO9~regout\);

-- Location: LCCOMB_X23_Y15_N16
\199|ADDRESS[3]\ : stratixii_lcell_comb
-- Equation(s):
-- \199|ADDRESS\(3) = ( \199|DILO8~regout\ ) # ( !\199|DILO8~regout\ & ( (\199|DILO10~regout\) # (\199|DILO9~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100111111001111110011111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \199|ALT_INV_DILO9~regout\,
	datac => \199|ALT_INV_DILO10~regout\,
	dataf => \199|ALT_INV_DILO8~regout\,
	combout => \199|ADDRESS\(3));

-- Location: LCCOMB_X23_Y15_N20
\199|ADDRESS[2]\ : stratixii_lcell_comb
-- Equation(s):
-- \199|ADDRESS\(2) = ( \199|DILO4~regout\ ) # ( !\199|DILO4~regout\ & ( ((\199|DILO5~regout\) # (\199|DILO6~regout\)) # (\199|DILO7~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111111111111001111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \199|ALT_INV_DILO7~regout\,
	datac => \199|ALT_INV_DILO6~regout\,
	datad => \199|ALT_INV_DILO5~regout\,
	dataf => \199|ALT_INV_DILO4~regout\,
	combout => \199|ADDRESS\(2));

-- Location: LCCOMB_X23_Y15_N24
\199|ADDRESS[1]\ : stratixii_lcell_comb
-- Equation(s):
-- \199|ADDRESS\(1) = ( \199|DILO6~regout\ ) # ( !\199|DILO6~regout\ & ( (((\199|DILO2~regout\) # (\199|DILO10~regout\)) # (\199|DILO7~regout\)) # (\199|DILO3~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111111111111011111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \199|ALT_INV_DILO3~regout\,
	datab => \199|ALT_INV_DILO7~regout\,
	datac => \199|ALT_INV_DILO10~regout\,
	datad => \199|ALT_INV_DILO2~regout\,
	dataf => \199|ALT_INV_DILO6~regout\,
	combout => \199|ADDRESS\(1));

-- Location: LCCOMB_X23_Y15_N26
\199|ADDRESS[0]\ : stratixii_lcell_comb
-- Equation(s):
-- \199|ADDRESS\(0) = ( \199|DILO1~regout\ ) # ( !\199|DILO1~regout\ & ( (((\199|DILO5~regout\) # (\199|DILO9~regout\)) # (\199|DILO7~regout\)) # (\199|DILO3~regout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111111111111011111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \199|ALT_INV_DILO3~regout\,
	datab => \199|ALT_INV_DILO7~regout\,
	datac => \199|ALT_INV_DILO9~regout\,
	datad => \199|ALT_INV_DILO5~regout\,
	dataf => \199|ALT_INV_DILO1~regout\,
	combout => \199|ADDRESS\(0));

-- Location: CLKCTRL_G12
\1|73|CLKD_O~clkctrl\ : stratixii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock")
-- pragma translate_on
PORT MAP (
	inclk => \1|73|CLKD_O~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \1|73|CLKD_O~clkctrl_outclk\);

-- Location: LCCOMB_X22_Y17_N30
\1|73|ADDRESS[0]~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|ADDRESS[0]~1_combout\ = !\1|73|ADDRESS\(0)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000000111111110000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_ADDRESS\(0),
	combout => \1|73|ADDRESS[0]~1_combout\);

-- Location: LCCOMB_X22_Y17_N10
\1|73|ADDRESS[5]~0\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|ADDRESS[5]~0_combout\ = ( \1|73|CLRADD$wire~combout\ ) # ( !\1|73|CLRADD$wire~combout\ & ( !\RESETn~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_RESETn~combout\,
	dataf => \1|73|ALT_INV_CLRADD$wire~combout\,
	combout => \1|73|ADDRESS[5]~0_combout\);

-- Location: LCFF_X22_Y17_N31
\1|73|ADDRESS[0]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLKD_O~clkctrl_outclk\,
	datain => \1|73|ADDRESS[0]~1_combout\,
	aclr => \1|73|ADDRESS[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|ADDRESS\(0));

-- Location: LCCOMB_X22_Y17_N0
\1|73|op_1~1\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_1~1_sumout\ = SUM(( \1|73|ADDRESS\(1) ) + ( \1|73|ADDRESS\(0) ) + ( !VCC ))
-- \1|73|op_1~2\ = CARRY(( \1|73|ADDRESS\(1) ) + ( \1|73|ADDRESS\(0) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_ADDRESS\(1),
	dataf => \1|73|ALT_INV_ADDRESS\(0),
	cin => GND,
	sumout => \1|73|op_1~1_sumout\,
	cout => \1|73|op_1~2\);

-- Location: LCFF_X22_Y17_N1
\1|73|ADDRESS[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLKD_O~clkctrl_outclk\,
	datain => \1|73|op_1~1_sumout\,
	aclr => \1|73|ADDRESS[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|ADDRESS\(1));

-- Location: LCCOMB_X22_Y17_N2
\1|73|op_1~5\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_1~5_sumout\ = SUM(( \1|73|ADDRESS\(2) ) + ( GND ) + ( \1|73|op_1~2\ ))
-- \1|73|op_1~6\ = CARRY(( \1|73|ADDRESS\(2) ) + ( GND ) + ( \1|73|op_1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_ADDRESS\(2),
	cin => \1|73|op_1~2\,
	sumout => \1|73|op_1~5_sumout\,
	cout => \1|73|op_1~6\);

-- Location: LCFF_X22_Y17_N3
\1|73|ADDRESS[2]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLKD_O~clkctrl_outclk\,
	datain => \1|73|op_1~5_sumout\,
	aclr => \1|73|ADDRESS[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|ADDRESS\(2));

-- Location: LCCOMB_X22_Y17_N4
\1|73|op_1~9\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_1~9_sumout\ = SUM(( \1|73|ADDRESS\(3) ) + ( GND ) + ( \1|73|op_1~6\ ))
-- \1|73|op_1~10\ = CARRY(( \1|73|ADDRESS\(3) ) + ( GND ) + ( \1|73|op_1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_ADDRESS\(3),
	cin => \1|73|op_1~6\,
	sumout => \1|73|op_1~9_sumout\,
	cout => \1|73|op_1~10\);

-- Location: LCFF_X22_Y17_N5
\1|73|ADDRESS[3]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLKD_O~clkctrl_outclk\,
	datain => \1|73|op_1~9_sumout\,
	aclr => \1|73|ADDRESS[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|ADDRESS\(3));

-- Location: LCCOMB_X22_Y17_N6
\1|73|op_1~13\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_1~13_sumout\ = SUM(( \1|73|ADDRESS\(4) ) + ( GND ) + ( \1|73|op_1~10\ ))
-- \1|73|op_1~14\ = CARRY(( \1|73|ADDRESS\(4) ) + ( GND ) + ( \1|73|op_1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_ADDRESS\(4),
	cin => \1|73|op_1~10\,
	sumout => \1|73|op_1~13_sumout\,
	cout => \1|73|op_1~14\);

-- Location: LCFF_X22_Y17_N7
\1|73|ADDRESS[4]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLKD_O~clkctrl_outclk\,
	datain => \1|73|op_1~13_sumout\,
	aclr => \1|73|ADDRESS[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|ADDRESS\(4));

-- Location: LCCOMB_X22_Y17_N8
\1|73|op_1~17\ : stratixii_lcell_comb
-- Equation(s):
-- \1|73|op_1~17_sumout\ = SUM(( \1|73|ADDRESS\(5) ) + ( GND ) + ( \1|73|op_1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \1|73|ALT_INV_ADDRESS\(5),
	cin => \1|73|op_1~14\,
	sumout => \1|73|op_1~17_sumout\);

-- Location: LCFF_X22_Y17_N9
\1|73|ADDRESS[5]\ : stratixii_lcell_ff
PORT MAP (
	clk => \1|73|CLKD_O~clkctrl_outclk\,
	datain => \1|73|op_1~17_sumout\,
	aclr => \1|73|ADDRESS[5]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \1|73|ADDRESS\(5));

-- Location: LCCOMB_X17_Y18_N4
\88|FCO[2]~1\ : stratixii_lcell_comb
-- Equation(s):
-- \88|FCO[2]~1_combout\ = (!\88|FIFO_WR_SEL~2_combout\) # ((!\88|IDLE~regout\) # (\88|CHOOSER~regout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111110011111111111111001111111111111100111111111111110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datac => \88|ALT_INV_IDLE~regout\,
	datad => \88|ALT_INV_CHOOSER~regout\,
	combout => \88|FCO[2]~1_combout\);

-- Location: LCCOMB_X17_Y18_N18
\88|FCO[0]\ : stratixii_lcell_comb
-- Equation(s):
-- \88|FCO\(0) = (!\88|FCO[0]~4_combout\) # (!\88|FCO[0]~3_combout\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110111011101110111011101110111011101110111011101110111011101110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~4_combout\,
	datab => \88|ALT_INV_FCO[0]~3_combout\,
	combout => \88|FCO\(0));

-- Location: PIN_U12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\MACK6_10~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_MACK6_10,
	combout => \MACK6_10~combout\);

-- Location: PIN_V9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\MACK1_5~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_MACK1_5,
	combout => \MACK1_5~combout\);

-- Location: LCCOMB_X23_Y15_N28
\197\ : stratixii_lcell_comb
-- Equation(s):
-- \197~combout\ = ( \MACK1_5~combout\ ) # ( !\MACK1_5~combout\ & ( \MACK6_10~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_MACK6_10~combout\,
	dataf => \ALT_INV_MACK1_5~combout\,
	combout => \197~combout\);

-- Location: LCFF_X23_Y15_N29
\340\ : stratixii_lcell_ff
PORT MAP (
	clk => \ALT_INV_clockext~clkctrl_outclk\,
	datain => \197~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \340~regout\);

-- Location: LCFF_X23_Y15_N31
\371\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \340~regout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \371~regout\);

-- Location: LCCOMB_X23_Y15_N30
\299|lpm_mux_component|$00053|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00053|result_node~0_combout\ = ( \88|FIFO_WR_SEL~2_combout\ & ( (!\88|FCO[0]~3_combout\ & ((\371~regout\))) # (\88|FCO[0]~3_combout\ & (\LOCBUS~0\)) ) ) # ( !\88|FIFO_WR_SEL~2_combout\ & ( \371~regout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000101101011110000010110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \ALT_INV_LOCBUS~0\,
	datad => \ALT_INV_371~regout\,
	dataf => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	combout => \299|lpm_mux_component|$00053|result_node~0_combout\);

-- Location: LCFF_X23_Y15_N17
\414[3]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \199|ADDRESS\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \414\(3));

-- Location: LCCOMB_X23_Y15_N18
\299|lpm_mux_component|$00051|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00051|result_node~0_combout\ = ( \414\(3) & ( (!\88|FIFO_WR_SEL~2_combout\) # ((!\88|FCO[0]~3_combout\) # (\LOCBUS~1\)) ) ) # ( !\414\(3) & ( (\88|FIFO_WR_SEL~2_combout\ & (\LOCBUS~1\ & \88|FCO[0]~3_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010111111111101011111111111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datac => \ALT_INV_LOCBUS~1\,
	datad => \88|ALT_INV_FCO[0]~3_combout\,
	dataf => ALT_INV_414(3),
	combout => \299|lpm_mux_component|$00051|result_node~0_combout\);

-- Location: LCFF_X23_Y15_N21
\414[2]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \199|ADDRESS\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \414\(2));

-- Location: LCCOMB_X23_Y15_N2
\299|lpm_mux_component|$00049|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00049|result_node~0_combout\ = ( \414\(2) & ( (!\88|FCO[0]~3_combout\) # ((!\88|FIFO_WR_SEL~2_combout\) # (\LOCBUS~2\)) ) ) # ( !\414\(2) & ( (\88|FCO[0]~3_combout\ & (\LOCBUS~2\ & \88|FIFO_WR_SEL~2_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010111111111101011111111111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \ALT_INV_LOCBUS~2\,
	datad => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	dataf => ALT_INV_414(2),
	combout => \299|lpm_mux_component|$00049|result_node~0_combout\);

-- Location: LCFF_X23_Y15_N25
\414[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \199|ADDRESS\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \414\(1));

-- Location: LCCOMB_X23_Y15_N0
\299|lpm_mux_component|$00047|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00047|result_node~0_combout\ = ( \LOCBUS~3\ & ( ((\88|FCO[0]~3_combout\ & \88|FIFO_WR_SEL~2_combout\)) # (\414\(1)) ) ) # ( !\LOCBUS~3\ & ( (\414\(1) & ((!\88|FCO[0]~3_combout\) # (!\88|FIFO_WR_SEL~2_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100100010001100110010001000110011011101110011001101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datab => ALT_INV_414(1),
	datad => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	dataf => \ALT_INV_LOCBUS~3\,
	combout => \299|lpm_mux_component|$00047|result_node~0_combout\);

-- Location: LCFF_X23_Y15_N27
\414[0]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	datain => \199|ADDRESS\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \414\(0));

-- Location: LCCOMB_X23_Y15_N22
\299|lpm_mux_component|$00045|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00045|result_node~0_combout\ = ( \414\(0) & ( (!\88|FIFO_WR_SEL~2_combout\) # ((!\88|FCO[0]~3_combout\) # (\LOCBUS~4\)) ) ) # ( !\414\(0) & ( (\88|FIFO_WR_SEL~2_combout\ & (\LOCBUS~4\ & \88|FCO[0]~3_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101000000000000010111111111101011111111111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datac => \ALT_INV_LOCBUS~4\,
	datad => \88|ALT_INV_FCO[0]~3_combout\,
	dataf => ALT_INV_414(0),
	combout => \299|lpm_mux_component|$00045|result_node~0_combout\);

-- Location: LCFF_X23_Y15_N7
\339[17]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~0\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(17));

-- Location: LCCOMB_X23_Y15_N6
\299|lpm_mux_component|$00043|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00043|result_node~0_combout\ = ( \88|FIFO_WR_SEL~2_combout\ & ( (!\88|FCO[0]~3_combout\ & ((\339\(17)))) # (\88|FCO[0]~3_combout\ & (\LOCBUS~5\)) ) ) # ( !\88|FIFO_WR_SEL~2_combout\ & ( \339\(17) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000101101011110000010110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \ALT_INV_LOCBUS~5\,
	datad => ALT_INV_339(17),
	dataf => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	combout => \299|lpm_mux_component|$00043|result_node~0_combout\);

-- Location: LCFF_X23_Y15_N11
\339[16]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~1\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(16));

-- Location: LCCOMB_X23_Y15_N10
\299|lpm_mux_component|$00041|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00041|result_node~0_combout\ = ( \88|FIFO_WR_SEL~2_combout\ & ( (!\88|FCO[0]~3_combout\ & ((\339\(16)))) # (\88|FCO[0]~3_combout\ & (\LOCBUS~6\)) ) ) # ( !\88|FIFO_WR_SEL~2_combout\ & ( \339\(16) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111000000001111111100000101101011110000010110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \ALT_INV_LOCBUS~6\,
	datad => ALT_INV_339(16),
	dataf => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	combout => \299|lpm_mux_component|$00041|result_node~0_combout\);

-- Location: LCFF_X23_Y15_N9
\339[15]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~2\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(15));

-- Location: LCCOMB_X23_Y15_N8
\299|lpm_mux_component|$00039|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00039|result_node~0_combout\ = ( \LOCBUS~7\ & ( ((\88|FCO[0]~3_combout\ & \88|FIFO_WR_SEL~2_combout\)) # (\339\(15)) ) ) # ( !\LOCBUS~7\ & ( (\339\(15) & ((!\88|FCO[0]~3_combout\) # (!\88|FIFO_WR_SEL~2_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111010000000001111101000000101111111110000010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datad => ALT_INV_339(15),
	dataf => \ALT_INV_LOCBUS~7\,
	combout => \299|lpm_mux_component|$00039|result_node~0_combout\);

-- Location: LCFF_X23_Y15_N15
\339[14]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~3\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(14));

-- Location: LCCOMB_X23_Y15_N14
\299|lpm_mux_component|$00037|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00037|result_node~0_combout\ = (!\88|FCO[0]~3_combout\ & (((\339\(14))))) # (\88|FCO[0]~3_combout\ & ((!\88|FIFO_WR_SEL~2_combout\ & ((\339\(14)))) # (\88|FIFO_WR_SEL~2_combout\ & (\LOCBUS~8\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000111101111000000011110111100000001111011110000000111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datab => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datac => \ALT_INV_LOCBUS~8\,
	datad => ALT_INV_339(14),
	combout => \299|lpm_mux_component|$00037|result_node~0_combout\);

-- Location: LCFF_X23_Y15_N13
\339[13]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~4\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(13));

-- Location: LCCOMB_X23_Y15_N12
\299|lpm_mux_component|$00035|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00035|result_node~0_combout\ = ( \LOCBUS~9\ & ( ((\88|FCO[0]~3_combout\ & \88|FIFO_WR_SEL~2_combout\)) # (\339\(13)) ) ) # ( !\LOCBUS~9\ & ( (\339\(13) & ((!\88|FCO[0]~3_combout\) # (!\88|FIFO_WR_SEL~2_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011101110000000001110111000010001111111110001000111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datab => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datad => ALT_INV_339(13),
	dataf => \ALT_INV_LOCBUS~9\,
	combout => \299|lpm_mux_component|$00035|result_node~0_combout\);

-- Location: LCFF_X19_Y15_N3
\339[12]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~5\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(12));

-- Location: LCCOMB_X19_Y15_N2
\299|lpm_mux_component|$00033|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00033|result_node~0_combout\ = (!\88|FIFO_WR_SEL~2_combout\ & (((\339\(12))))) # (\88|FIFO_WR_SEL~2_combout\ & ((!\88|FCO[0]~3_combout\ & ((\339\(12)))) # (\88|FCO[0]~3_combout\ & (\LOCBUS~10\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000111101111000000011110111100000001111011110000000111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datab => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \ALT_INV_LOCBUS~10\,
	datad => ALT_INV_339(12),
	combout => \299|lpm_mux_component|$00033|result_node~0_combout\);

-- Location: LCFF_X19_Y15_N7
\339[11]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~6\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(11));

-- Location: LCCOMB_X19_Y15_N6
\299|lpm_mux_component|$00031|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00031|result_node~0_combout\ = ( \LOCBUS~11\ & ( ((\88|FIFO_WR_SEL~2_combout\ & \88|FCO[0]~3_combout\)) # (\339\(11)) ) ) # ( !\LOCBUS~11\ & ( (\339\(11) & ((!\88|FIFO_WR_SEL~2_combout\) # (!\88|FCO[0]~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011101110000000001110111000010001111111110001000111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datab => \88|ALT_INV_FCO[0]~3_combout\,
	datad => ALT_INV_339(11),
	dataf => \ALT_INV_LOCBUS~11\,
	combout => \299|lpm_mux_component|$00031|result_node~0_combout\);

-- Location: LCFF_X19_Y15_N1
\339[10]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~7\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(10));

-- Location: LCCOMB_X19_Y15_N0
\299|lpm_mux_component|$00029|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00029|result_node~0_combout\ = (!\88|FIFO_WR_SEL~2_combout\ & (((\339\(10))))) # (\88|FIFO_WR_SEL~2_combout\ & ((!\88|FCO[0]~3_combout\ & ((\339\(10)))) # (\88|FCO[0]~3_combout\ & (\LOCBUS~12\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000111101111000000011110111100000001111011110000000111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datab => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \ALT_INV_LOCBUS~12\,
	datad => ALT_INV_339(10),
	combout => \299|lpm_mux_component|$00029|result_node~0_combout\);

-- Location: LCFF_X19_Y15_N5
\339[9]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~8\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(9));

-- Location: LCCOMB_X19_Y15_N4
\299|lpm_mux_component|$00027|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00027|result_node~0_combout\ = ( \LOCBUS~13\ & ( ((\88|FIFO_WR_SEL~2_combout\ & \88|FCO[0]~3_combout\)) # (\339\(9)) ) ) # ( !\LOCBUS~13\ & ( (\339\(9) & ((!\88|FIFO_WR_SEL~2_combout\) # (!\88|FCO[0]~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011101110000000001110111000010001111111110001000111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datab => \88|ALT_INV_FCO[0]~3_combout\,
	datad => ALT_INV_339(9),
	dataf => \ALT_INV_LOCBUS~13\,
	combout => \299|lpm_mux_component|$00027|result_node~0_combout\);

-- Location: LCFF_X21_Y14_N11
\339[8]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~9\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(8));

-- Location: LCCOMB_X21_Y14_N10
\299|lpm_mux_component|$00025|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00025|result_node~0_combout\ = ( \339\(8) & ( \88|FIFO_WR_SEL~2_combout\ & ( (!\88|FCO[0]~3_combout\) # (\LOCBUS~14\) ) ) ) # ( !\339\(8) & ( \88|FIFO_WR_SEL~2_combout\ & ( (\88|FCO[0]~3_combout\ & \LOCBUS~14\) ) ) ) # ( \339\(8) & 
-- ( !\88|FIFO_WR_SEL~2_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000010101011010101011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datad => \ALT_INV_LOCBUS~14\,
	datae => ALT_INV_339(8),
	dataf => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	combout => \299|lpm_mux_component|$00025|result_node~0_combout\);

-- Location: LCFF_X21_Y14_N3
\339[7]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~10\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(7));

-- Location: LCCOMB_X21_Y14_N2
\299|lpm_mux_component|$00023|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00023|result_node~0_combout\ = ( \339\(7) & ( (!\88|FCO[0]~3_combout\) # ((!\88|FIFO_WR_SEL~2_combout\) # (\LOCBUS~15\)) ) ) # ( !\339\(7) & ( (\88|FCO[0]~3_combout\ & (\LOCBUS~15\ & \88|FIFO_WR_SEL~2_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101111111111010111100000000000001011111111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \ALT_INV_LOCBUS~15\,
	datad => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datae => ALT_INV_339(7),
	combout => \299|lpm_mux_component|$00023|result_node~0_combout\);

-- Location: LCFF_X21_Y14_N31
\339[6]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~11\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(6));

-- Location: LCCOMB_X21_Y14_N30
\299|lpm_mux_component|$00021|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00021|result_node~0_combout\ = ( \339\(6) & ( \LOCBUS~16\ ) ) # ( !\339\(6) & ( \LOCBUS~16\ & ( (\88|FCO[0]~3_combout\ & \88|FIFO_WR_SEL~2_combout\) ) ) ) # ( \339\(6) & ( !\LOCBUS~16\ & ( (!\88|FCO[0]~3_combout\) # 
-- (!\88|FIFO_WR_SEL~2_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111010101000000000010101011111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datad => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datae => ALT_INV_339(6),
	dataf => \ALT_INV_LOCBUS~16\,
	combout => \299|lpm_mux_component|$00021|result_node~0_combout\);

-- Location: LCFF_X19_Y15_N9
\339[5]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~12\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(5));

-- Location: LCCOMB_X19_Y15_N8
\299|lpm_mux_component|$00019|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00019|result_node~0_combout\ = (!\88|FIFO_WR_SEL~2_combout\ & (((\339\(5))))) # (\88|FIFO_WR_SEL~2_combout\ & ((!\88|FCO[0]~3_combout\ & ((\339\(5)))) # (\88|FCO[0]~3_combout\ & (\LOCBUS~17\))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000111101111000000011110111100000001111011110000000111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datab => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \ALT_INV_LOCBUS~17\,
	datad => ALT_INV_339(5),
	combout => \299|lpm_mux_component|$00019|result_node~0_combout\);

-- Location: LCFF_X21_Y14_N15
\339[4]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~13\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(4));

-- Location: LCCOMB_X21_Y14_N14
\299|lpm_mux_component|$00017|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00017|result_node~0_combout\ = ( \339\(4) & ( (!\88|FCO[0]~3_combout\) # ((!\88|FIFO_WR_SEL~2_combout\) # (\LOCBUS~18\)) ) ) # ( !\339\(4) & ( (\88|FCO[0]~3_combout\ & (\LOCBUS~18\ & \88|FIFO_WR_SEL~2_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000101111111111010111100000000000001011111111110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \ALT_INV_LOCBUS~18\,
	datad => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datae => ALT_INV_339(4),
	combout => \299|lpm_mux_component|$00017|result_node~0_combout\);

-- Location: LCFF_X21_Y14_N5
\339[3]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~14\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(3));

-- Location: LCCOMB_X21_Y14_N4
\299|lpm_mux_component|$00015|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00015|result_node~0_combout\ = ( \339\(3) & ( \LOCBUS~19\ ) ) # ( !\339\(3) & ( \LOCBUS~19\ & ( (\88|FCO[0]~3_combout\ & \88|FIFO_WR_SEL~2_combout\) ) ) ) # ( \339\(3) & ( !\LOCBUS~19\ & ( (!\88|FCO[0]~3_combout\) # 
-- (!\88|FIFO_WR_SEL~2_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111110101111101000000101000001011111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datae => ALT_INV_339(3),
	dataf => \ALT_INV_LOCBUS~19\,
	combout => \299|lpm_mux_component|$00015|result_node~0_combout\);

-- Location: LCFF_X19_Y15_N11
\339[2]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~15\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(2));

-- Location: LCCOMB_X19_Y15_N10
\299|lpm_mux_component|$00013|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00013|result_node~0_combout\ = ( \LOCBUS~20\ & ( ((\88|FIFO_WR_SEL~2_combout\ & \88|FCO[0]~3_combout\)) # (\339\(2)) ) ) # ( !\LOCBUS~20\ & ( (\339\(2) & ((!\88|FIFO_WR_SEL~2_combout\) # (!\88|FCO[0]~3_combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011101110000000001110111000010001111111110001000111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datab => \88|ALT_INV_FCO[0]~3_combout\,
	datad => ALT_INV_339(2),
	dataf => \ALT_INV_LOCBUS~20\,
	combout => \299|lpm_mux_component|$00013|result_node~0_combout\);

-- Location: LCFF_X21_Y14_N25
\339[1]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~16\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(1));

-- Location: LCCOMB_X21_Y14_N24
\299|lpm_mux_component|$00011|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00011|result_node~0_combout\ = ( \339\(1) & ( \88|FIFO_WR_SEL~2_combout\ & ( (!\88|FCO[0]~3_combout\) # (\LOCBUS~21\) ) ) ) # ( !\339\(1) & ( \88|FIFO_WR_SEL~2_combout\ & ( (\LOCBUS~21\ & \88|FCO[0]~3_combout\) ) ) ) # ( \339\(1) & 
-- ( !\88|FIFO_WR_SEL~2_combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000101000001011111010111110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_LOCBUS~21\,
	datac => \88|ALT_INV_FCO[0]~3_combout\,
	datae => ALT_INV_339(1),
	dataf => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	combout => \299|lpm_mux_component|$00011|result_node~0_combout\);

-- Location: LCFF_X19_Y15_N13
\339[0]\ : stratixii_lcell_ff
PORT MAP (
	clk => \clockext~clkctrl_outclk\,
	adatasdata => \DILOBUS~17\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \339\(0));

-- Location: LCCOMB_X19_Y15_N12
\299|lpm_mux_component|$00009|result_node~0\ : stratixii_lcell_comb
-- Equation(s):
-- \299|lpm_mux_component|$00009|result_node~0_combout\ = ( \339\(0) & ( \LOCBUS~22\ ) ) # ( !\339\(0) & ( \LOCBUS~22\ & ( (\88|FCO[0]~3_combout\ & \88|FIFO_WR_SEL~2_combout\) ) ) ) # ( \339\(0) & ( !\LOCBUS~22\ & ( (!\88|FCO[0]~3_combout\) # 
-- (!\88|FIFO_WR_SEL~2_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111001111110000000011000000111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \88|ALT_INV_FCO[0]~3_combout\,
	datac => \88|ALT_INV_FIFO_WR_SEL~2_combout\,
	datae => ALT_INV_339(0),
	dataf => \ALT_INV_LOCBUS~22\,
	combout => \299|lpm_mux_component|$00009|result_node~0_combout\);

-- Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\LOCBUS[27]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[11]~20_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(27));

-- Location: PIN_L20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\LOCBUS[26]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[10]~21_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(26));

-- Location: PIN_AB8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[25]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[9]~22_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(25));

-- Location: PIN_K2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\LOCBUS[24]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[8]~23_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(24));

-- Location: PIN_A16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LOCBUS[23]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "bidir",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \45|52[7]~24_combout\,
	oe => \45|52[6]~13_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	padio => LOCBUS(23));

-- Location: PIN_A19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\READ_NEXT~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_READ_NEXT);

-- Location: PIN_W12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\STRIN~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \424~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_STRIN);

-- Location: PIN_C5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\CLRADD~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|73|CLRADD$wire~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_CLRADD);

-- Location: PIN_K16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\BUSY~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|27|9~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_BUSY);

-- Location: PIN_K1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\CLRG~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|73|ALT_INV_CLRG_O~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_CLRG);

-- Location: PIN_W10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\RSTn~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \400~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RSTn);

-- Location: PIN_P2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\CLRD~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \359~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_CLRD);

-- Location: PIN_D5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\SUB_COMP~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \386|SUB_COMP~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_SUB_COMP);

-- Location: PIN_J6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\TRIGN~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \368~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_TRIGN);

-- Location: PIN_J2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\EN_IN1~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \88|ENIN1~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_EN_IN1);

-- Location: PIN_V22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\VTEST~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_VTEST);

-- Location: PIN_G22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\CLKD~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|ALT_INV_46~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_CLKD);

-- Location: PIN_L15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\TRIG_DISABLE~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|27|13~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_TRIG_DISABLE);

-- Location: PIN_J19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\DL_IN~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|89~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_DL_IN);

-- Location: PIN_T2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\TRACK_HOLD~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|2|ALT_INV_49~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_TRACK_HOLD);

-- Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\CLOCK~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|2|9~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_CLOCK);

-- Location: PIN_C11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\CLKADC~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|73|CLKA_O~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_CLKADC);

-- Location: PIN_K7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\CLKG~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|73|ALT_INV_CLKG_O~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_CLKG);

-- Location: PIN_K20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\WR_FIFO~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \88|WR_FIFO~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_WR_FIFO);

-- Location: PIN_P19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\WR_CLK~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ALT_INV_clockext~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_WR_CLK);

-- Location: PIN_N16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\RD_FIFO~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \88|RD_FIFO~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RD_FIFO);

-- Location: PIN_T21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\RD_CLK~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \ALT_INV_clockext~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_RD_CLK);

-- Location: PIN_T10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\aclr~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \425~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_aclr);

-- Location: PIN_U2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\ADDR_DILO[3]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \199|ADDRESS\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ADDR_DILO(3));

-- Location: PIN_A8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\ADDR_DILO[2]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \199|ADDRESS\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ADDR_DILO(2));

-- Location: PIN_P17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\ADDR_DILO[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \199|ADDRESS\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ADDR_DILO(1));

-- Location: PIN_Y9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\ADDR_DILO[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \199|ADDRESS\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ADDR_DILO(0));

-- Location: PIN_N15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\ADDRGX[5]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|73|ALT_INV_ADDRESS\(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ADDRGX(5));

-- Location: PIN_U10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\ADDRGX[4]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|73|ALT_INV_ADDRESS\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ADDRGX(4));

-- Location: PIN_C10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\ADDRGX[3]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|73|ALT_INV_ADDRESS\(3),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ADDRGX(3));

-- Location: PIN_K5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\ADDRGX[2]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|73|ALT_INV_ADDRESS\(2),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ADDRGX(2));

-- Location: PIN_T5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\ADDRGX[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|73|ALT_INV_ADDRESS\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ADDRGX(1));

-- Location: PIN_L7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\ADDRGX[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \1|73|ALT_INV_ADDRESS\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_ADDRGX(0));

-- Location: PIN_C9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FC[3]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \88|ENIN1~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FC(3));

-- Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FC[2]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \88|ALT_INV_FCO[2]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FC(2));

-- Location: PIN_AA12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FC[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \88|ALT_INV_FCO[1]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FC(1));

-- Location: PIN_AB16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FC[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \88|FCO\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FC(0));

-- Location: PIN_G19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[22]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00053|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(22));

-- Location: PIN_E15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FFIN[21]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00051|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(21));

-- Location: PIN_R3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[20]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00049|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(20));

-- Location: PIN_AA10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FFIN[19]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00047|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(19));

-- Location: PIN_H3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[18]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00045|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(18));

-- Location: PIN_N8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[17]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00043|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(17));

-- Location: PIN_K6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[16]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00041|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(16));

-- Location: PIN_P3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[15]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00039|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(15));

-- Location: PIN_P20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[14]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00037|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(14));

-- Location: PIN_C6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FFIN[13]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00035|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(13));

-- Location: PIN_B13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FFIN[12]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00033|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(12));

-- Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[11]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00031|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(11));

-- Location: PIN_L16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[10]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00029|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(10));

-- Location: PIN_N21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[9]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00027|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(9));

-- Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FFIN[8]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00025|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(8));

-- Location: PIN_W2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[7]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00023|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(7));

-- Location: PIN_N22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[6]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00021|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(6));

-- Location: PIN_L3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[5]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00019|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(5));

-- Location: PIN_Y11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FFIN[4]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00017|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(4));

-- Location: PIN_T3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[3]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00015|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(3));

-- Location: PIN_AA9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\FFIN[2]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00013|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(2));

-- Location: PIN_K19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00011|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(1));

-- Location: PIN_N2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 12mA
\FFIN[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => \299|lpm_mux_component|$00009|result_node~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_FFIN(0));

-- Location: PIN_Y3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\NBR_GASS[1]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_NBR_GASS(1));

-- Location: PIN_D13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\NBR_GASS[0]~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_NBR_GASS(0));

-- Location: PIN_V16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\spare2~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_spare2);

-- Location: PIN_G7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\spare1~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_spare1);

-- Location: PIN_A5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\START_READ~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_START_READ);

-- Location: PIN_AA5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\s_noAdata2~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_s_noAdata2);

-- Location: PIN_C4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\s_noAdata1~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_s_noAdata1);

-- Location: PIN_E19,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\s_empty1~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_s_empty1);

-- Location: PIN_Y15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\s_almfull1~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_s_almfull1);

-- Location: PIN_N4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\s_empty2~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_s_empty2);

-- Location: PIN_V10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\s_almfull2~I\ : stratixii_io
-- pragma translate_off
GENERIC MAP (
	ddio_mode => "none",
	ddioinclk_input => "negated_inclk",
	dqs_delay_buffer_mode => "none",
	dqs_out_mode => "none",
	inclk_input => "normal",
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none",
	sim_dqs_delay_increment => 0,
	sim_dqs_intrinsic_delay => 0,
	sim_dqs_offset_increment => 0)
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_s_almfull2);
END structure;



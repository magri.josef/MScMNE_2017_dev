-------------------------------------------------------------------------------
-- File : tri_bus.vhd
-- Description : 32 bit Tri-State buffer
-- Designer : Josef Magri (University of Malta CERN-EP/UAI)
-------------------------------------------------------------------------------
--http://www.kynix.com/uploadfiles/pdf/74LVT16245BDGG2b112.pdf
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tri_bus is
   port ( DIR : in    std_logic; 
          OEn : in    std_logic; 
          A   : INOUT std_logic_vector(31 DOWNTO 0);
          B   : INOUT std_logic_vector(31 DOWNTO 0)
          );
end tri_bus;

architecture BEHAVIORAL of tri_bus is

signal temp : std_logic_vector(1 DOWNTO 0);

begin
	
	temp <= (OEn & DIR); -- ?? (not(OEn) & DIR);

process(A,B,temp)
	begin
		case temp is
			when "01" =>
					B <= A;
					--B <= (others => 'Z');
			when "00" =>
					A <= "0000" & B(27 DOWNTO 0);
					--A <= (others => 'Z');
			--when "1X" =>
			--		A <= (others => 'Z');
			--		B <= (others => 'Z');
			when "11" =>
					A <= (others => 'Z');
					B <= (others => 'Z');
			when "10" =>
					A <= (others => 'Z');
					B <= (others => 'Z');
			when others =>
					A <= (others => 'Z');
					B <= (others => 'Z');
		end case ;
	end process;
end Behavioral;

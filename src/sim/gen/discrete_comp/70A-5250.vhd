-------------------------------------------------------------------------------
-- File : 70A-5250.vhd
-- Description : Digital Delay Module 5ns Tap-to-Tap http://www.farnell.com/datasheets/64085.pdf
-- Designer : Josef Magri (University of Malta CERN-EP/UAI)
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity delay_chip is
   port (
         CLK_IN : in  std_logic;
         T1     : out std_logic := '1'
        );
end delay_chip;

architecture delay_chip_sim of delay_chip is

begin

process
	begin
		wait until CLK_IN'event;
			if falling_edge(CLK_IN) and T1 = '1' then
				wait for 5 ns;
				T1 <= '0';
			elsif rising_edge(CLK_IN) and T1 = '0' then
				wait for 5 ns;
				T1 <= '1';
			end if;
	end process;

end delay_chip_sim;

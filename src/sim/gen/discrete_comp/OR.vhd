-------------------------------------------------------------------------------
-- File : tri_bus.vhd
-- Description : 32 bit Tri-State buffer
-- Designer : Josef Magri (University of Malta CERN-EP/UAI)
-------------------------------------------------------------------------------
--http://www.kynix.com/uploadfiles/pdf/74LVT16245BDGG2b112.pdf
library IEEE;
use ieee.std_logic_1164.ALL;
use ieee.std_logic_misc.ALL;

entity ORgate is
   port ( IN_0 : in    std_logic; 
          IN_1 : in    std_logic; 
          IN_2 : in    std_logic; 
          IN_3 : in    std_logic; 
          IN_4 : in    std_logic;
          OUT_0 : out    std_logic
          );
end ORgate;

architecture BEHAVIORAL of ORgate is

begin
process(IN_0, IN_1, IN_2, IN_3, IN_4)
   variable bcat : std_logic_vector(0 to 4);
begin
   bcat := IN_0 & IN_1 & IN_2 & IN_3 & IN_4;
   OUT_0 <= AND_REDUCE(bcat);
end process;

end BEHAVIORAL;

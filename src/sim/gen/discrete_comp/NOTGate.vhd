-------------------------------------------------------------------------------
-- File : NOTGate.vhd
-- Description : 8 in 8 out not gate
-- Designer : Josef Magri (University of Malta CERN-EP/UAI)
-------------------------------------------------------------------------------
--import std_logic from the IEEE library
library ieee;
use ieee.std_logic_1164.all;

--ENTITY DECLARATION: name, inputs, outputs
entity notGate is
   port( 	in_0  : in std_logic;
   			in_1  : in std_logic;
   			in_2  : in std_logic;
   			in_3  : in std_logic;
   			in_4  : in std_logic;
   			in_5  : in std_logic;
   			in_6  : in std_logic;
            in_7  : in std_logic;
         	out_0 : out std_logic;
         	out_1 : out std_logic;
         	out_2 : out std_logic;
         	out_3 : out std_logic;
         	out_4 : out std_logic;
         	out_5 : out std_logic;
            out_6 : out std_logic;
         	out_7 : out std_logic);
end notGate;

--FUNCTIONAL DESCRIPTION: how the Inverter works
architecture func of notGate is
begin
	out_0 <= not in_0;
	out_1 <= not in_1;
	out_2 <= not in_2;
	out_3 <= not in_3;
	out_4 <= not in_4;
	out_5 <= not in_5;
	out_6 <= not in_6;
   out_7 <= not in_7;
end func;
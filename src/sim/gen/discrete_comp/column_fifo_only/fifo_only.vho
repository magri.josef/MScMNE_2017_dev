-- Copyright (C) 1991-2010 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 10.1 Build 153 11/29/2010 SJ Web Edition"

-- DATE "07/05/2017 18:40:49"

-- 
-- Device: Altera EP4CGX22CF19C6 Package FBGA324
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	clmn_fifo IS
    PORT (
	aclr : IN std_logic;
	data : IN std_logic_vector(22 DOWNTO 0);
	rdclk : IN std_logic;
	rdreq : IN std_logic;
	wrclk : IN std_logic;
	wrreq : IN std_logic;
	q : OUT std_logic_vector(22 DOWNTO 0);
	rdempty : OUT std_logic;
	wrfull : OUT std_logic;
	wrusedw : OUT std_logic_vector(9 DOWNTO 0)
	);
END clmn_fifo;

-- Design Ports Information
-- q[0]	=>  Location: PIN_N17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[1]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[2]	=>  Location: PIN_B15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[3]	=>  Location: PIN_G17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[4]	=>  Location: PIN_V13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[5]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[6]	=>  Location: PIN_T11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[7]	=>  Location: PIN_V14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[8]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[9]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[10]	=>  Location: PIN_R10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[11]	=>  Location: PIN_P10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[12]	=>  Location: PIN_J17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[13]	=>  Location: PIN_R12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[14]	=>  Location: PIN_M18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[15]	=>  Location: PIN_T16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[16]	=>  Location: PIN_T12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[17]	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[18]	=>  Location: PIN_R13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[19]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[20]	=>  Location: PIN_U10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[21]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- q[22]	=>  Location: PIN_P13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rdempty	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrfull	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrusedw[0]	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrusedw[1]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrusedw[2]	=>  Location: PIN_T14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrusedw[3]	=>  Location: PIN_N18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrusedw[4]	=>  Location: PIN_M17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrusedw[5]	=>  Location: PIN_U13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrusedw[6]	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrusedw[7]	=>  Location: PIN_K15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrusedw[8]	=>  Location: PIN_D16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrusedw[9]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrreq	=>  Location: PIN_V11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rdreq	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- wrclk	=>  Location: PIN_M10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rdclk	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- aclr	=>  Location: PIN_V12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[0]	=>  Location: PIN_D14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[1]	=>  Location: PIN_V9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[2]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[3]	=>  Location: PIN_L16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[4]	=>  Location: PIN_V17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[5]	=>  Location: PIN_P12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[6]	=>  Location: PIN_T13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[7]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[8]	=>  Location: PIN_T17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[9]	=>  Location: PIN_D17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[10]	=>  Location: PIN_R11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[11]	=>  Location: PIN_P18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[12]	=>  Location: PIN_U12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[13]	=>  Location: PIN_U16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[14]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[15]	=>  Location: PIN_C15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[16]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[17]	=>  Location: PIN_K16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[18]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[19]	=>  Location: PIN_V15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[20]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[21]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data[22]	=>  Location: PIN_U15,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF clmn_fifo IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_aclr : std_logic;
SIGNAL ww_data : std_logic_vector(22 DOWNTO 0);
SIGNAL ww_rdclk : std_logic;
SIGNAL ww_rdreq : std_logic;
SIGNAL ww_wrclk : std_logic;
SIGNAL ww_wrreq : std_logic;
SIGNAL ww_q : std_logic_vector(22 DOWNTO 0);
SIGNAL ww_rdempty : std_logic;
SIGNAL ww_wrfull : std_logic;
SIGNAL ww_wrusedw : std_logic_vector(9 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTADATAIN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTAADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTADATAIN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTAADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTADATAIN_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTAADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTBADDR_bus\ : std_logic_vector(9 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTBDATAOUT_bus\ : std_logic_vector(8 DOWNTO 0);
SIGNAL \aclr~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \rdclk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \wrclk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~3_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~2_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|_~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~3_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~7_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|sub_parity12a1~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|_~9_combout\ : std_logic;
SIGNAL \rdreq~input_o\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|delayed_wrptr_g[9]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[9]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[6]~feeder_combout\ : std_logic;
SIGNAL \wrreq~input_o\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~1_combout\ : std_logic;
SIGNAL \aclr~input_o\ : std_logic;
SIGNAL \aclr~inputclkctrl_outclk\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[0]~9_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[1]~10_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|delayed_wrptr_g[0]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[0]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[0]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|_~5_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[2]~7_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|_~4_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[5]~6_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[4]~5_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[6]~3_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|_~3_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[8]~1_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|_~2_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[9]~2_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|_~6_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|sub_parity12a2~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|_~8_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|sub_parity12a0~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|_~7_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|parity11~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g[5]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[5]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[5]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g[2]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[2]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~5_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g[6]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[6]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~3_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g[7]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[7]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g[4]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[4]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[4]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~4_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~6_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[3]~8_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|delayed_wrptr_g[3]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g[0]~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~7_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~8_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~6_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g[8]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[10]~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|delayed_wrptr_g[10]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[10]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|delayed_wrptr_g[8]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[8]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[8]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~2_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|delayed_wrptr_g[2]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|delayed_wrptr_g[5]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[5]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[5]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~5_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a[7]~4_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[7]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|delayed_wrptr_g[4]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[4]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[4]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~4_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~6_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|valid_rdreq~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~2_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~5_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~10_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~9_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~11_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~8_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|parity6~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g[3]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[3]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~7_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~8_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|valid_wrreq~0_combout\ : std_logic;
SIGNAL \wrclk~input_o\ : std_logic;
SIGNAL \wrclk~inputclkctrl_outclk\ : std_logic;
SIGNAL \rdclk~input_o\ : std_logic;
SIGNAL \rdclk~inputclkctrl_outclk\ : std_logic;
SIGNAL \data[0]~input_o\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ram_address_a[9]~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~_wirecell_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|_~0_combout\ : std_logic;
SIGNAL \data[1]~input_o\ : std_logic;
SIGNAL \data[2]~input_o\ : std_logic;
SIGNAL \data[3]~input_o\ : std_logic;
SIGNAL \data[4]~input_o\ : std_logic;
SIGNAL \data[5]~input_o\ : std_logic;
SIGNAL \data[6]~input_o\ : std_logic;
SIGNAL \data[7]~input_o\ : std_logic;
SIGNAL \data[8]~input_o\ : std_logic;
SIGNAL \data[9]~input_o\ : std_logic;
SIGNAL \data[10]~input_o\ : std_logic;
SIGNAL \data[11]~input_o\ : std_logic;
SIGNAL \data[12]~input_o\ : std_logic;
SIGNAL \data[13]~input_o\ : std_logic;
SIGNAL \data[14]~input_o\ : std_logic;
SIGNAL \data[15]~input_o\ : std_logic;
SIGNAL \data[16]~input_o\ : std_logic;
SIGNAL \data[17]~input_o\ : std_logic;
SIGNAL \data[18]~input_o\ : std_logic;
SIGNAL \data[19]~input_o\ : std_logic;
SIGNAL \data[20]~input_o\ : std_logic;
SIGNAL \data[21]~input_o\ : std_logic;
SIGNAL \data[22]~input_o\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor1~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor0~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[0]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[0]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g[10]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[10]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[8]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdptr_g[9]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[9]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[9]~feeder_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor1~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor0~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~1\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~2_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor2~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor2~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~3\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~4_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor3~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor3~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~5\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~6_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~7\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~8_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor5~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor5~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~9\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~10_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor6~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor6~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~11\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~12_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~13\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~14_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor8~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~15\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~16_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor9~combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~17\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|op_1~18_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|ws_brp|dffe18a\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|fifo_ram|q_b\ : std_logic_vector(22 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|wrptr_gp|counter13a\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a\ : std_logic_vector(2 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|delayed_wrptr_g\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|rdptr_g\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|ws_bwp|dffe18a\ : std_logic_vector(10 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|ALT_INV_valid_rdreq~0_combout\ : std_logic;
SIGNAL \dcfifo_component|auto_generated|rdempty_eq_comp|ALT_INV_aneb_result_wire\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \dcfifo_component|auto_generated|wrfull_eq_comp|ALT_INV_aneb_result_wire\ : std_logic_vector(0 DOWNTO 0);
SIGNAL \ALT_INV_aclr~inputclkctrl_outclk\ : std_logic;

BEGIN

ww_aclr <= aclr;
ww_data <= data;
ww_rdclk <= rdclk;
ww_rdreq <= rdreq;
ww_wrclk <= wrclk;
ww_wrreq <= wrreq;
q <= ww_q;
rdempty <= ww_rdempty;
wrfull <= ww_wrfull;
wrusedw <= ww_wrusedw;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTADATAIN_bus\ <= (\data[8]~input_o\ & \data[7]~input_o\ & \data[6]~input_o\ & \data[5]~input_o\ & \data[4]~input_o\ & \data[3]~input_o\ & \data[2]~input_o\ & \data[1]~input_o\ & \data[0]~input_o\
);

\dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTAADDR_bus\ <= (\dcfifo_component|auto_generated|ram_address_a[9]~0_combout\ & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8) & 
\dcfifo_component|auto_generated|wrptr_gp|counter13a\(7) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4) & 
\dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0));

\dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBADDR_bus\ <= (\dcfifo_component|auto_generated|rdptr_g1p|_~0_combout\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\ & 
\dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\ & 
\dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~_wirecell_combout\);

\dcfifo_component|auto_generated|fifo_ram|q_b\(0) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\(0);
\dcfifo_component|auto_generated|fifo_ram|q_b\(1) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\(1);
\dcfifo_component|auto_generated|fifo_ram|q_b\(2) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\(2);
\dcfifo_component|auto_generated|fifo_ram|q_b\(3) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\(3);
\dcfifo_component|auto_generated|fifo_ram|q_b\(4) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\(4);
\dcfifo_component|auto_generated|fifo_ram|q_b\(5) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\(5);
\dcfifo_component|auto_generated|fifo_ram|q_b\(6) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\(6);
\dcfifo_component|auto_generated|fifo_ram|q_b\(7) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\(7);
\dcfifo_component|auto_generated|fifo_ram|q_b\(8) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\(8);

\dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTADATAIN_bus\ <= (\data[17]~input_o\ & \data[16]~input_o\ & \data[15]~input_o\ & \data[14]~input_o\ & \data[13]~input_o\ & \data[12]~input_o\ & \data[11]~input_o\ & \data[10]~input_o\ & 
\data[9]~input_o\);

\dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTAADDR_bus\ <= (\dcfifo_component|auto_generated|ram_address_a[9]~0_combout\ & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8) & 
\dcfifo_component|auto_generated|wrptr_gp|counter13a\(7) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4) & 
\dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0));

\dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBADDR_bus\ <= (\dcfifo_component|auto_generated|rdptr_g1p|_~0_combout\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\ & 
\dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\ & 
\dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~_wirecell_combout\);

\dcfifo_component|auto_generated|fifo_ram|q_b\(9) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\(0);
\dcfifo_component|auto_generated|fifo_ram|q_b\(10) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\(1);
\dcfifo_component|auto_generated|fifo_ram|q_b\(11) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\(2);
\dcfifo_component|auto_generated|fifo_ram|q_b\(12) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\(3);
\dcfifo_component|auto_generated|fifo_ram|q_b\(13) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\(4);
\dcfifo_component|auto_generated|fifo_ram|q_b\(14) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\(5);
\dcfifo_component|auto_generated|fifo_ram|q_b\(15) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\(6);
\dcfifo_component|auto_generated|fifo_ram|q_b\(16) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\(7);
\dcfifo_component|auto_generated|fifo_ram|q_b\(17) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\(8);

\dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTADATAIN_bus\ <= (gnd & gnd & gnd & gnd & \data[22]~input_o\ & \data[21]~input_o\ & \data[20]~input_o\ & \data[19]~input_o\ & \data[18]~input_o\);

\dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTAADDR_bus\ <= (\dcfifo_component|auto_generated|ram_address_a[9]~0_combout\ & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8) & 
\dcfifo_component|auto_generated|wrptr_gp|counter13a\(7) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4) & 
\dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1) & \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0));

\dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTBADDR_bus\ <= (\dcfifo_component|auto_generated|rdptr_g1p|_~0_combout\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\ & 
\dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\ & 
\dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~_wirecell_combout\);

\dcfifo_component|auto_generated|fifo_ram|q_b\(18) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTBDATAOUT_bus\(0);
\dcfifo_component|auto_generated|fifo_ram|q_b\(19) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTBDATAOUT_bus\(1);
\dcfifo_component|auto_generated|fifo_ram|q_b\(20) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTBDATAOUT_bus\(2);
\dcfifo_component|auto_generated|fifo_ram|q_b\(21) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTBDATAOUT_bus\(3);
\dcfifo_component|auto_generated|fifo_ram|q_b\(22) <= \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTBDATAOUT_bus\(4);

\aclr~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \aclr~input_o\);

\rdclk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \rdclk~input_o\);

\wrclk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \wrclk~input_o\);
\dcfifo_component|auto_generated|ALT_INV_valid_rdreq~0_combout\ <= NOT \dcfifo_component|auto_generated|valid_rdreq~0_combout\;
\dcfifo_component|auto_generated|rdempty_eq_comp|ALT_INV_aneb_result_wire\(0) <= NOT \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire\(0);
\dcfifo_component|auto_generated|wrfull_eq_comp|ALT_INV_aneb_result_wire\(0) <= NOT \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire\(0);
\ALT_INV_aclr~inputclkctrl_outclk\ <= NOT \aclr~inputclkctrl_outclk\;

-- Location: FF_X37_Y24_N3
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[9]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(9));

-- Location: FF_X39_Y24_N11
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(6),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(6));

-- Location: LCCOMB_X39_Y24_N10
\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~3_combout\ = (\dcfifo_component|auto_generated|rdptr_g\(6) & ((\dcfifo_component|auto_generated|rdptr_g\(9) $ (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(9))) # 
-- (!\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(6)))) # (!\dcfifo_component|auto_generated|rdptr_g\(6) & ((\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(6)) # (\dcfifo_component|auto_generated|rdptr_g\(9) $ 
-- (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111101111011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g\(6),
	datab => \dcfifo_component|auto_generated|rdptr_g\(9),
	datac => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(6),
	datad => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(9),
	combout => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~3_combout\);

-- Location: LCCOMB_X41_Y25_N4
\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~2_combout\ = (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(8) & ((\dcfifo_component|auto_generated|wrptr_gp|counter13a\(10) $ 
-- (!\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(10))) # (!\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(8)))) # (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(8) & 
-- ((\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(8)) # (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(10) $ (!\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101111001111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(8),
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(10),
	combout => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~2_combout\);

-- Location: FF_X37_Y24_N19
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|delayed_wrptr_g\(9),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(9));

-- Location: FF_X39_Y25_N23
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[6]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(6));

-- Location: LCCOMB_X38_Y25_N24
\dcfifo_component|auto_generated|wrptr_gp|_~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|_~0_combout\ = (\wrreq~input_o\ & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(1) & !\dcfifo_component|auto_generated|wrptr_gp|counter13a\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \wrreq~input_o\,
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1),
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0),
	combout => \dcfifo_component|auto_generated|wrptr_gp|_~0_combout\);

-- Location: LCCOMB_X41_Y24_N4
\dcfifo_component|auto_generated|rdptr_g1p|_~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~3_combout\ = (\rdreq~input_o\ & (\dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\ & !\dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \rdreq~input_o\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~3_combout\);

-- Location: LCCOMB_X39_Y24_N22
\dcfifo_component|auto_generated|rdptr_g1p|_~7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~7_combout\ = (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\ & (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\ & (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\ & 
-- \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~7_combout\);

-- Location: FF_X37_Y24_N23
\dcfifo_component|auto_generated|delayed_wrptr_g[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|delayed_wrptr_g[9]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(9));

-- Location: FF_X39_Y25_N21
\dcfifo_component|auto_generated|delayed_wrptr_g[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(6));

-- Location: FF_X42_Y25_N7
\dcfifo_component|auto_generated|wrptr_gp|sub_parity12a1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|_~9_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|sub_parity12a1~q\);

-- Location: LCCOMB_X42_Y25_N6
\dcfifo_component|auto_generated|wrptr_gp|_~9\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|_~9_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(7) $ 
-- (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(7),
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5),
	combout => \dcfifo_component|auto_generated|wrptr_gp|_~9_combout\);

-- Location: IOIBUF_X36_Y41_N1
\rdreq~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rdreq,
	o => \rdreq~input_o\);

-- Location: LCCOMB_X37_Y24_N22
\dcfifo_component|auto_generated|delayed_wrptr_g[9]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|delayed_wrptr_g[9]~feeder_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(9)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(9),
	combout => \dcfifo_component|auto_generated|delayed_wrptr_g[9]~feeder_combout\);

-- Location: LCCOMB_X37_Y24_N2
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[9]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[9]~feeder_combout\ = \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(9)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(9),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[9]~feeder_combout\);

-- Location: LCCOMB_X39_Y25_N22
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[6]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[6]~feeder_combout\ = \dcfifo_component|auto_generated|delayed_wrptr_g\(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|delayed_wrptr_g\(6),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[6]~feeder_combout\);

-- Location: IOOBUF_X52_Y16_N2
\q[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(0),
	devoe => ww_devoe,
	o => ww_q(0));

-- Location: IOOBUF_X46_Y0_N16
\q[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(1),
	devoe => ww_devoe,
	o => ww_q(1));

-- Location: IOOBUF_X41_Y41_N2
\q[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(2),
	devoe => ww_devoe,
	o => ww_q(2));

-- Location: IOOBUF_X52_Y27_N9
\q[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(3),
	devoe => ww_devoe,
	o => ww_q(3));

-- Location: IOOBUF_X29_Y0_N2
\q[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(4),
	devoe => ww_devoe,
	o => ww_q(4));

-- Location: IOOBUF_X38_Y41_N9
\q[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(5),
	devoe => ww_devoe,
	o => ww_q(5));

-- Location: IOOBUF_X31_Y0_N16
\q[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(6),
	devoe => ww_devoe,
	o => ww_q(6));

-- Location: IOOBUF_X34_Y0_N9
\q[7]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(7),
	devoe => ww_devoe,
	o => ww_q(7));

-- Location: IOOBUF_X36_Y41_N9
\q[8]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(8),
	devoe => ww_devoe,
	o => ww_q(8));

-- Location: IOOBUF_X31_Y41_N9
\q[9]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(9),
	devoe => ww_devoe,
	o => ww_q(9));

-- Location: IOOBUF_X25_Y0_N2
\q[10]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(10),
	devoe => ww_devoe,
	o => ww_q(10));

-- Location: IOOBUF_X25_Y0_N9
\q[11]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(11),
	devoe => ww_devoe,
	o => ww_q(11));

-- Location: IOOBUF_X52_Y23_N9
\q[12]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(12),
	devoe => ww_devoe,
	o => ww_q(12));

-- Location: IOOBUF_X36_Y0_N9
\q[13]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(13),
	devoe => ww_devoe,
	o => ww_q(13));

-- Location: IOOBUF_X52_Y19_N2
\q[14]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(14),
	devoe => ww_devoe,
	o => ww_q(14));

-- Location: IOOBUF_X46_Y0_N9
\q[15]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(15),
	devoe => ww_devoe,
	o => ww_q(15));

-- Location: IOOBUF_X31_Y0_N9
\q[16]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(16),
	devoe => ww_devoe,
	o => ww_q(16));

-- Location: IOOBUF_X52_Y25_N2
\q[17]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(17),
	devoe => ww_devoe,
	o => ww_q(17));

-- Location: IOOBUF_X36_Y0_N2
\q[18]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(18),
	devoe => ww_devoe,
	o => ww_q(18));

-- Location: IOOBUF_X38_Y41_N2
\q[19]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(19),
	devoe => ww_devoe,
	o => ww_q(19));

-- Location: IOOBUF_X23_Y0_N2
\q[20]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(20),
	devoe => ww_devoe,
	o => ww_q(20));

-- Location: IOOBUF_X34_Y41_N2
\q[21]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(21),
	devoe => ww_devoe,
	o => ww_q(21));

-- Location: IOOBUF_X38_Y0_N2
\q[22]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|fifo_ram|q_b\(22),
	devoe => ww_devoe,
	o => ww_q(22));

-- Location: IOOBUF_X52_Y25_N9
\rdempty~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|rdempty_eq_comp|ALT_INV_aneb_result_wire\(0),
	devoe => ww_devoe,
	o => ww_rdempty);

-- Location: IOOBUF_X52_Y27_N2
\wrfull~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|wrfull_eq_comp|ALT_INV_aneb_result_wire\(0),
	devoe => ww_devoe,
	o => ww_wrfull);

-- Location: IOOBUF_X52_Y31_N9
\wrusedw[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|op_1~0_combout\,
	devoe => ww_devoe,
	o => ww_wrusedw(0));

-- Location: IOOBUF_X52_Y28_N2
\wrusedw[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|op_1~2_combout\,
	devoe => ww_devoe,
	o => ww_wrusedw(1));

-- Location: IOOBUF_X41_Y0_N16
\wrusedw[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|op_1~4_combout\,
	devoe => ww_devoe,
	o => ww_wrusedw(2));

-- Location: IOOBUF_X52_Y16_N9
\wrusedw[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|op_1~6_combout\,
	devoe => ww_devoe,
	o => ww_wrusedw(3));

-- Location: IOOBUF_X52_Y15_N9
\wrusedw[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|op_1~8_combout\,
	devoe => ww_devoe,
	o => ww_wrusedw(4));

-- Location: IOOBUF_X29_Y0_N9
\wrusedw[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|op_1~10_combout\,
	devoe => ww_devoe,
	o => ww_wrusedw(5));

-- Location: IOOBUF_X52_Y15_N2
\wrusedw[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|op_1~12_combout\,
	devoe => ww_devoe,
	o => ww_wrusedw(6));

-- Location: IOOBUF_X52_Y18_N2
\wrusedw[7]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|op_1~14_combout\,
	devoe => ww_devoe,
	o => ww_wrusedw(7));

-- Location: IOOBUF_X46_Y41_N9
\wrusedw[8]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|op_1~16_combout\,
	devoe => ww_devoe,
	o => ww_wrusedw(8));

-- Location: IOOBUF_X43_Y41_N2
\wrusedw[9]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \dcfifo_component|auto_generated|op_1~18_combout\,
	devoe => ww_devoe,
	o => ww_wrusedw(9));

-- Location: IOIBUF_X27_Y0_N8
\wrreq~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wrreq,
	o => \wrreq~input_o\);

-- Location: LCCOMB_X41_Y24_N26
\dcfifo_component|auto_generated|rdptr_g1p|_~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~1_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|parity6~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|parity6~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~1_combout\);

-- Location: IOIBUF_X27_Y0_N1
\aclr~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_aclr,
	o => \aclr~input_o\);

-- Location: CLKCTRL_G18
\aclr~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \aclr~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \aclr~inputclkctrl_outclk\);

-- Location: LCCOMB_X38_Y25_N20
\dcfifo_component|auto_generated|wrptr_gp|counter13a[0]~9\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[0]~9_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0) $ (((!\dcfifo_component|auto_generated|wrptr_gp|parity11~q\ & \dcfifo_component|auto_generated|valid_wrreq~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|parity11~q\,
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0),
	datad => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[0]~9_combout\);

-- Location: FF_X38_Y25_N21
\dcfifo_component|auto_generated|wrptr_gp|counter13a[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[0]~9_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0));

-- Location: LCCOMB_X38_Y25_N8
\dcfifo_component|auto_generated|wrptr_gp|counter13a[1]~10\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[1]~10_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1) $ (((\dcfifo_component|auto_generated|wrptr_gp|parity11~q\ & (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(0) & 
-- \dcfifo_component|auto_generated|valid_wrreq~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|parity11~q\,
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1),
	datad => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[1]~10_combout\);

-- Location: FF_X38_Y25_N9
\dcfifo_component|auto_generated|wrptr_gp|counter13a[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[1]~10_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1));

-- Location: FF_X38_Y25_N23
\dcfifo_component|auto_generated|delayed_wrptr_g[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(1));

-- Location: FF_X38_Y25_N15
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|delayed_wrptr_g\(1),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(1));

-- Location: FF_X38_Y25_N27
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(1),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(1));

-- Location: LCCOMB_X38_Y25_N28
\dcfifo_component|auto_generated|delayed_wrptr_g[0]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|delayed_wrptr_g[0]~feeder_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0),
	combout => \dcfifo_component|auto_generated|delayed_wrptr_g[0]~feeder_combout\);

-- Location: FF_X38_Y25_N29
\dcfifo_component|auto_generated|delayed_wrptr_g[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|delayed_wrptr_g[0]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(0));

-- Location: LCCOMB_X38_Y25_N10
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[0]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[0]~feeder_combout\ = \dcfifo_component|auto_generated|delayed_wrptr_g\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|delayed_wrptr_g\(0),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[0]~feeder_combout\);

-- Location: FF_X38_Y25_N11
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[0]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(0));

-- Location: LCCOMB_X38_Y25_N30
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[0]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[0]~feeder_combout\ = \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(0),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[0]~feeder_combout\);

-- Location: FF_X38_Y25_N31
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[0]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(0));

-- Location: LCCOMB_X38_Y25_N2
\dcfifo_component|auto_generated|wrptr_gp|_~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|_~5_combout\ = (\dcfifo_component|auto_generated|wrptr_gp|parity11~q\ & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(0) & (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(1) & 
-- \dcfifo_component|auto_generated|valid_wrreq~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|parity11~q\,
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1),
	datad => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|_~5_combout\);

-- Location: LCCOMB_X38_Y25_N12
\dcfifo_component|auto_generated|wrptr_gp|counter13a[2]~7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[2]~7_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) $ (\dcfifo_component|auto_generated|wrptr_gp|_~5_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2),
	datad => \dcfifo_component|auto_generated|wrptr_gp|_~5_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[2]~7_combout\);

-- Location: FF_X38_Y25_N13
\dcfifo_component|auto_generated|wrptr_gp|counter13a[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[2]~7_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2));

-- Location: LCCOMB_X42_Y25_N24
\dcfifo_component|auto_generated|wrptr_gp|_~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|_~4_combout\ = (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(4) & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) & 
-- \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3),
	datad => \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|_~4_combout\);

-- Location: LCCOMB_X42_Y25_N18
\dcfifo_component|auto_generated|wrptr_gp|counter13a[5]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[5]~6_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) $ (\dcfifo_component|auto_generated|wrptr_gp|_~4_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5),
	datad => \dcfifo_component|auto_generated|wrptr_gp|_~4_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[5]~6_combout\);

-- Location: FF_X42_Y25_N19
\dcfifo_component|auto_generated|wrptr_gp|counter13a[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[5]~6_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5));

-- Location: LCCOMB_X42_Y25_N16
\dcfifo_component|auto_generated|wrptr_gp|counter13a[4]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[4]~5_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4) $ (((\dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) & 
-- \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4),
	datad => \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[4]~5_combout\);

-- Location: FF_X42_Y25_N17
\dcfifo_component|auto_generated|wrptr_gp|counter13a[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[4]~5_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4));

-- Location: LCCOMB_X41_Y25_N10
\dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\ = (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(4) & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) & 
-- \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3),
	datad => \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\);

-- Location: LCCOMB_X39_Y25_N26
\dcfifo_component|auto_generated|wrptr_gp|counter13a[6]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[6]~3_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) $ (((\dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) & 
-- \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6),
	datad => \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[6]~3_combout\);

-- Location: FF_X39_Y25_N27
\dcfifo_component|auto_generated|wrptr_gp|counter13a[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[6]~3_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6));

-- Location: LCCOMB_X41_Y25_N22
\dcfifo_component|auto_generated|wrptr_gp|_~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|_~3_combout\ = (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(7) & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) & 
-- \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(7),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6),
	datad => \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|_~3_combout\);

-- Location: LCCOMB_X41_Y25_N26
\dcfifo_component|auto_generated|wrptr_gp|counter13a[8]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[8]~1_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8) $ (\dcfifo_component|auto_generated|wrptr_gp|_~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8),
	datad => \dcfifo_component|auto_generated|wrptr_gp|_~3_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[8]~1_combout\);

-- Location: FF_X41_Y25_N27
\dcfifo_component|auto_generated|wrptr_gp|counter13a[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[8]~1_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8));

-- Location: LCCOMB_X41_Y25_N12
\dcfifo_component|auto_generated|wrptr_gp|_~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|_~2_combout\ = (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(7) & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) & 
-- \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(7),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6),
	datad => \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|_~2_combout\);

-- Location: LCCOMB_X41_Y25_N14
\dcfifo_component|auto_generated|wrptr_gp|counter13a[9]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[9]~2_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(9) $ (((\dcfifo_component|auto_generated|wrptr_gp|counter13a\(8) & \dcfifo_component|auto_generated|wrptr_gp|_~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(9),
	datad => \dcfifo_component|auto_generated|wrptr_gp|_~2_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[9]~2_combout\);

-- Location: FF_X41_Y25_N15
\dcfifo_component|auto_generated|wrptr_gp|counter13a[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[9]~2_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(9));

-- Location: LCCOMB_X42_Y25_N26
\dcfifo_component|auto_generated|wrptr_gp|_~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|_~6_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(8) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8),
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(9),
	combout => \dcfifo_component|auto_generated|wrptr_gp|_~6_combout\);

-- Location: FF_X42_Y25_N27
\dcfifo_component|auto_generated|wrptr_gp|sub_parity12a2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|_~6_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|sub_parity12a2~q\);

-- Location: LCCOMB_X42_Y25_N20
\dcfifo_component|auto_generated|wrptr_gp|_~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|_~8_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) $ 
-- (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3),
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1),
	combout => \dcfifo_component|auto_generated|wrptr_gp|_~8_combout\);

-- Location: FF_X42_Y25_N21
\dcfifo_component|auto_generated|wrptr_gp|sub_parity12a0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|_~8_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|sub_parity12a0~q\);

-- Location: LCCOMB_X42_Y25_N30
\dcfifo_component|auto_generated|wrptr_gp|_~7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|_~7_combout\ = \dcfifo_component|auto_generated|wrptr_gp|sub_parity12a1~q\ $ (\dcfifo_component|auto_generated|wrptr_gp|sub_parity12a2~q\ $ (!\dcfifo_component|auto_generated|wrptr_gp|sub_parity12a0~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010100101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|sub_parity12a1~q\,
	datac => \dcfifo_component|auto_generated|wrptr_gp|sub_parity12a2~q\,
	datad => \dcfifo_component|auto_generated|wrptr_gp|sub_parity12a0~q\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|_~7_combout\);

-- Location: FF_X42_Y25_N31
\dcfifo_component|auto_generated|wrptr_gp|parity11\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|_~7_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|parity11~q\);

-- Location: LCCOMB_X38_Y24_N12
\dcfifo_component|auto_generated|rdptr_g[5]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g[5]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g[5]~feeder_combout\);

-- Location: FF_X38_Y24_N13
\dcfifo_component|auto_generated|rdptr_g[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g[5]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(5));

-- Location: LCCOMB_X42_Y24_N20
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[5]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[5]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g\(5),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[5]~feeder_combout\);

-- Location: FF_X42_Y24_N21
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[5]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(5));

-- Location: LCCOMB_X42_Y24_N30
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[5]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[5]~feeder_combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(5),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[5]~feeder_combout\);

-- Location: FF_X42_Y24_N31
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[5]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(5));

-- Location: LCCOMB_X38_Y24_N22
\dcfifo_component|auto_generated|rdptr_g[2]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g[2]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g[2]~feeder_combout\);

-- Location: FF_X38_Y24_N23
\dcfifo_component|auto_generated|rdptr_g[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g[2]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(2));

-- Location: LCCOMB_X42_Y24_N14
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[2]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[2]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g\(2),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[2]~feeder_combout\);

-- Location: FF_X42_Y24_N15
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[2]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(2));

-- Location: FF_X41_Y25_N21
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(2),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(2));

-- Location: LCCOMB_X41_Y25_N20
\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~5_combout\ = (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) & ((\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(2) $ 
-- (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(2))) # (!\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(5)))) # (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) & 
-- ((\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(5)) # (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(2) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110111111110110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5),
	datab => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(5),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(2),
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2),
	combout => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~5_combout\);

-- Location: LCCOMB_X38_Y24_N14
\dcfifo_component|auto_generated|rdptr_g[6]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g[6]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g[6]~feeder_combout\);

-- Location: FF_X38_Y24_N15
\dcfifo_component|auto_generated|rdptr_g[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g[6]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(6));

-- Location: LCCOMB_X42_Y24_N6
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[6]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[6]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g\(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g\(6),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[6]~feeder_combout\);

-- Location: FF_X42_Y24_N7
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[6]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(6));

-- Location: FF_X41_Y25_N9
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(6),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(6));

-- Location: LCCOMB_X41_Y25_N8
\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~3_combout\ = (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(9) & ((\dcfifo_component|auto_generated|wrptr_gp|counter13a\(9)) # 
-- (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(6) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(6))))) # (!\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(9) & 
-- ((\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(6) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(6))) # (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001111111111001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(9),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(9),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(6),
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6),
	combout => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~3_combout\);

-- Location: LCCOMB_X38_Y24_N24
\dcfifo_component|auto_generated|rdptr_g[7]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g[7]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g[7]~feeder_combout\);

-- Location: FF_X38_Y24_N25
\dcfifo_component|auto_generated|rdptr_g[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g[7]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(7));

-- Location: LCCOMB_X42_Y24_N24
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[7]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[7]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g\(7),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[7]~feeder_combout\);

-- Location: FF_X42_Y24_N25
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[7]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(7));

-- Location: FF_X41_Y25_N19
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(7),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(7));

-- Location: LCCOMB_X41_Y24_N10
\dcfifo_component|auto_generated|rdptr_g1p|counter5a4~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\ $ (((!\dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\ & (\dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\ & 
-- \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~0_combout\);

-- Location: FF_X41_Y24_N11
\dcfifo_component|auto_generated|rdptr_g1p|counter5a4\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\);

-- Location: LCCOMB_X38_Y24_N26
\dcfifo_component|auto_generated|rdptr_g[4]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g[4]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g[4]~feeder_combout\);

-- Location: FF_X38_Y24_N27
\dcfifo_component|auto_generated|rdptr_g[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g[4]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(4));

-- Location: LCCOMB_X42_Y24_N18
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[4]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[4]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g\(4),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[4]~feeder_combout\);

-- Location: FF_X42_Y24_N19
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[4]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(4));

-- Location: LCCOMB_X42_Y24_N28
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[4]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[4]~feeder_combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(4),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[4]~feeder_combout\);

-- Location: FF_X42_Y24_N29
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[4]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(4));

-- Location: LCCOMB_X41_Y25_N18
\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~4_combout\ = (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(7) & ((\dcfifo_component|auto_generated|wrptr_gp|counter13a\(4) $ 
-- (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(4))) # (!\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(7)))) # (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(7) & 
-- ((\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(7)) # (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(4) $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111101111011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(7),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(7),
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(4),
	combout => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~4_combout\);

-- Location: LCCOMB_X41_Y25_N6
\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~6_combout\ = (\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~2_combout\) # ((\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~5_combout\) # 
-- ((\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~3_combout\) # (\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~2_combout\,
	datab => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~5_combout\,
	datac => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~3_combout\,
	datad => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~4_combout\,
	combout => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~6_combout\);

-- Location: LCCOMB_X41_Y25_N24
\dcfifo_component|auto_generated|wrptr_gp|_~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\ = (\dcfifo_component|auto_generated|wrptr_gp|_~0_combout\ & (\dcfifo_component|auto_generated|wrptr_gp|parity11~q\ & ((\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~6_combout\) # 
-- (\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|_~0_combout\,
	datab => \dcfifo_component|auto_generated|wrptr_gp|parity11~q\,
	datac => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~6_combout\,
	datad => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~8_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\);

-- Location: LCCOMB_X41_Y25_N16
\dcfifo_component|auto_generated|wrptr_gp|counter13a[3]~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[3]~8_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) $ (((\dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) & \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3),
	datad => \dcfifo_component|auto_generated|wrptr_gp|_~1_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[3]~8_combout\);

-- Location: FF_X41_Y25_N17
\dcfifo_component|auto_generated|wrptr_gp|counter13a[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[3]~8_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3));

-- Location: LCCOMB_X37_Y24_N4
\dcfifo_component|auto_generated|delayed_wrptr_g[3]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|delayed_wrptr_g[3]~feeder_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3),
	combout => \dcfifo_component|auto_generated|delayed_wrptr_g[3]~feeder_combout\);

-- Location: FF_X37_Y24_N5
\dcfifo_component|auto_generated|delayed_wrptr_g[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|delayed_wrptr_g[3]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(3));

-- Location: FF_X37_Y24_N25
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|delayed_wrptr_g\(3),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(3));

-- Location: FF_X39_Y24_N21
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(3),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(3));

-- Location: LCCOMB_X41_Y24_N24
\dcfifo_component|auto_generated|rdptr_g[0]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g[0]~0_combout\ = !\dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g[0]~0_combout\);

-- Location: FF_X41_Y24_N25
\dcfifo_component|auto_generated|rdptr_g[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g[0]~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(0));

-- Location: LCCOMB_X39_Y24_N20
\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~7_combout\ = (\dcfifo_component|auto_generated|rdptr_g\(3) & ((\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(0) $ (\dcfifo_component|auto_generated|rdptr_g\(0))) # 
-- (!\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(3)))) # (!\dcfifo_component|auto_generated|rdptr_g\(3) & ((\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(3)) # (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(0) $ 
-- (\dcfifo_component|auto_generated|rdptr_g\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111101111011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g\(3),
	datab => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(0),
	datac => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(3),
	datad => \dcfifo_component|auto_generated|rdptr_g\(0),
	combout => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~7_combout\);

-- Location: LCCOMB_X39_Y24_N6
\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~8_combout\ = (\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~7_combout\) # (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(1) $ 
-- (\dcfifo_component|auto_generated|rdptr_g\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(1),
	datac => \dcfifo_component|auto_generated|rdptr_g\(1),
	datad => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~7_combout\,
	combout => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~8_combout\);

-- Location: LCCOMB_X39_Y24_N24
\dcfifo_component|auto_generated|rdptr_g1p|_~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\ = (\dcfifo_component|auto_generated|rdptr_g1p|_~3_combout\ & (!\dcfifo_component|auto_generated|rdptr_g1p|parity6~q\ & 
-- ((\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~6_combout\) # (\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~8_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001000100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|_~3_combout\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|parity6~q\,
	datac => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~6_combout\,
	datad => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~8_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\);

-- Location: LCCOMB_X39_Y24_N18
\dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\ = (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\ & (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\ & (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\ & 
-- \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\);

-- Location: LCCOMB_X39_Y24_N28
\dcfifo_component|auto_generated|rdptr_g1p|_~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~6_combout\ = (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\ & (\dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\ & (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\ & 
-- \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~6_combout\);

-- Location: LCCOMB_X39_Y24_N2
\dcfifo_component|auto_generated|rdptr_g1p|counter5a8~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|_~6_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|_~6_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~0_combout\);

-- Location: FF_X39_Y24_N3
\dcfifo_component|auto_generated|rdptr_g1p|counter5a8\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\);

-- Location: LCCOMB_X38_Y24_N18
\dcfifo_component|auto_generated|rdptr_g[8]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g[8]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g[8]~feeder_combout\);

-- Location: FF_X38_Y24_N19
\dcfifo_component|auto_generated|rdptr_g[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g[8]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(8));

-- Location: LCCOMB_X41_Y25_N0
\dcfifo_component|auto_generated|wrptr_gp|counter13a[10]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[10]~0_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10) $ (((!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(8) & \dcfifo_component|auto_generated|wrptr_gp|_~2_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10),
	datad => \dcfifo_component|auto_generated|wrptr_gp|_~2_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[10]~0_combout\);

-- Location: FF_X41_Y25_N1
\dcfifo_component|auto_generated|wrptr_gp|counter13a[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[10]~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10));

-- Location: LCCOMB_X37_Y24_N10
\dcfifo_component|auto_generated|delayed_wrptr_g[10]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|delayed_wrptr_g[10]~feeder_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10),
	combout => \dcfifo_component|auto_generated|delayed_wrptr_g[10]~feeder_combout\);

-- Location: FF_X37_Y24_N11
\dcfifo_component|auto_generated|delayed_wrptr_g[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|delayed_wrptr_g[10]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(10));

-- Location: LCCOMB_X37_Y24_N6
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[10]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[10]~feeder_combout\ = \dcfifo_component|auto_generated|delayed_wrptr_g\(10)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|delayed_wrptr_g\(10),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[10]~feeder_combout\);

-- Location: FF_X37_Y24_N7
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[10]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(10));

-- Location: FF_X39_Y24_N17
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(10),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(10));

-- Location: LCCOMB_X37_Y24_N28
\dcfifo_component|auto_generated|delayed_wrptr_g[8]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|delayed_wrptr_g[8]~feeder_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8),
	combout => \dcfifo_component|auto_generated|delayed_wrptr_g[8]~feeder_combout\);

-- Location: FF_X37_Y24_N29
\dcfifo_component|auto_generated|delayed_wrptr_g[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|delayed_wrptr_g[8]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(8));

-- Location: LCCOMB_X37_Y24_N16
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[8]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[8]~feeder_combout\ = \dcfifo_component|auto_generated|delayed_wrptr_g\(8)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|delayed_wrptr_g\(8),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[8]~feeder_combout\);

-- Location: FF_X37_Y24_N17
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[8]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(8));

-- Location: LCCOMB_X37_Y24_N8
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[8]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[8]~feeder_combout\ = \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(8)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(8),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[8]~feeder_combout\);

-- Location: FF_X37_Y24_N9
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[8]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(8));

-- Location: LCCOMB_X39_Y24_N16
\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~2_combout\ = (\dcfifo_component|auto_generated|rdptr_g\(10) & ((\dcfifo_component|auto_generated|rdptr_g\(8) $ (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(8))) # 
-- (!\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(10)))) # (!\dcfifo_component|auto_generated|rdptr_g\(10) & ((\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(10)) # (\dcfifo_component|auto_generated|rdptr_g\(8) $ 
-- (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(8)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111101111011110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g\(10),
	datab => \dcfifo_component|auto_generated|rdptr_g\(8),
	datac => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(10),
	datad => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(8),
	combout => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~2_combout\);

-- Location: LCCOMB_X37_Y24_N26
\dcfifo_component|auto_generated|delayed_wrptr_g[2]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|delayed_wrptr_g[2]~feeder_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2),
	combout => \dcfifo_component|auto_generated|delayed_wrptr_g[2]~feeder_combout\);

-- Location: FF_X37_Y24_N27
\dcfifo_component|auto_generated|delayed_wrptr_g[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|delayed_wrptr_g[2]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(2));

-- Location: FF_X37_Y24_N15
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|delayed_wrptr_g\(2),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(2));

-- Location: FF_X39_Y24_N15
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(2),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(2));

-- Location: LCCOMB_X39_Y25_N0
\dcfifo_component|auto_generated|delayed_wrptr_g[5]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|delayed_wrptr_g[5]~feeder_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5),
	combout => \dcfifo_component|auto_generated|delayed_wrptr_g[5]~feeder_combout\);

-- Location: FF_X39_Y25_N1
\dcfifo_component|auto_generated|delayed_wrptr_g[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|delayed_wrptr_g[5]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(5));

-- Location: LCCOMB_X39_Y25_N18
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[5]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[5]~feeder_combout\ = \dcfifo_component|auto_generated|delayed_wrptr_g\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|delayed_wrptr_g\(5),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[5]~feeder_combout\);

-- Location: FF_X39_Y25_N19
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[5]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(5));

-- Location: LCCOMB_X39_Y25_N8
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[5]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[5]~feeder_combout\ = \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(5),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[5]~feeder_combout\);

-- Location: FF_X39_Y25_N9
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[5]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(5));

-- Location: LCCOMB_X39_Y24_N14
\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~5_combout\ = (\dcfifo_component|auto_generated|rdptr_g\(5) & ((\dcfifo_component|auto_generated|rdptr_g\(2) $ (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(2))) # 
-- (!\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(5)))) # (!\dcfifo_component|auto_generated|rdptr_g\(5) & ((\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(5)) # (\dcfifo_component|auto_generated|rdptr_g\(2) $ 
-- (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111110110111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g\(5),
	datab => \dcfifo_component|auto_generated|rdptr_g\(2),
	datac => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(2),
	datad => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(5),
	combout => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~5_combout\);

-- Location: LCCOMB_X39_Y25_N4
\dcfifo_component|auto_generated|wrptr_gp|counter13a[7]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_gp|counter13a[7]~4_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(7) $ (((\dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) & (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) & 
-- \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(7),
	datad => \dcfifo_component|auto_generated|wrptr_gp|cntr_cout[5]~0_combout\,
	combout => \dcfifo_component|auto_generated|wrptr_gp|counter13a[7]~4_combout\);

-- Location: FF_X39_Y25_N5
\dcfifo_component|auto_generated|wrptr_gp|counter13a[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_gp|counter13a[7]~4_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(7));

-- Location: FF_X39_Y25_N7
\dcfifo_component|auto_generated|delayed_wrptr_g[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(7),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(7));

-- Location: LCCOMB_X39_Y25_N24
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[7]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[7]~feeder_combout\ = \dcfifo_component|auto_generated|delayed_wrptr_g\(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|delayed_wrptr_g\(7),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[7]~feeder_combout\);

-- Location: FF_X39_Y25_N25
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[7]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(7));

-- Location: FF_X39_Y24_N13
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(7),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(7));

-- Location: LCCOMB_X37_Y24_N0
\dcfifo_component|auto_generated|delayed_wrptr_g[4]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|delayed_wrptr_g[4]~feeder_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4),
	combout => \dcfifo_component|auto_generated|delayed_wrptr_g[4]~feeder_combout\);

-- Location: FF_X37_Y24_N1
\dcfifo_component|auto_generated|delayed_wrptr_g[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|delayed_wrptr_g[4]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|delayed_wrptr_g\(4));

-- Location: LCCOMB_X37_Y24_N12
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[4]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[4]~feeder_combout\ = \dcfifo_component|auto_generated|delayed_wrptr_g\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|delayed_wrptr_g\(4),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[4]~feeder_combout\);

-- Location: FF_X37_Y24_N13
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a[4]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(4));

-- Location: LCCOMB_X37_Y24_N20
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[4]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[4]~feeder_combout\ = \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe16a\(4),
	combout => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[4]~feeder_combout\);

-- Location: FF_X37_Y24_N21
\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a[4]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(4));

-- Location: LCCOMB_X39_Y24_N12
\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~4_combout\ = (\dcfifo_component|auto_generated|rdptr_g\(4) & ((\dcfifo_component|auto_generated|rdptr_g\(7) $ (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(7))) # 
-- (!\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(4)))) # (!\dcfifo_component|auto_generated|rdptr_g\(4) & ((\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(4)) # (\dcfifo_component|auto_generated|rdptr_g\(7) $ 
-- (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111110110111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g\(4),
	datab => \dcfifo_component|auto_generated|rdptr_g\(7),
	datac => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(7),
	datad => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(4),
	combout => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~4_combout\);

-- Location: LCCOMB_X39_Y24_N8
\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~6_combout\ = (\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~3_combout\) # ((\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~2_combout\) # 
-- ((\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~5_combout\) # (\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~3_combout\,
	datab => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~2_combout\,
	datac => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~5_combout\,
	datad => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~4_combout\,
	combout => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~6_combout\);

-- Location: LCCOMB_X39_Y24_N0
\dcfifo_component|auto_generated|valid_rdreq~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|valid_rdreq~0_combout\ = (\rdreq~input_o\ & ((\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~6_combout\) # (\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \rdreq~input_o\,
	datac => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~6_combout\,
	datad => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~8_combout\,
	combout => \dcfifo_component|auto_generated|valid_rdreq~0_combout\);

-- Location: FF_X41_Y24_N27
\dcfifo_component|auto_generated|rdptr_g1p|counter5a0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|_~1_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\);

-- Location: LCCOMB_X41_Y24_N2
\dcfifo_component|auto_generated|rdptr_g1p|_~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~2_combout\ = (\dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\ & (!\dcfifo_component|auto_generated|rdptr_g1p|parity6~q\ & (\dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\ & 
-- \dcfifo_component|auto_generated|valid_rdreq~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|parity6~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\,
	datad => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~2_combout\);

-- Location: LCCOMB_X41_Y24_N14
\dcfifo_component|auto_generated|rdptr_g1p|counter5a2~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|_~2_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|_~2_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~0_combout\);

-- Location: FF_X41_Y24_N15
\dcfifo_component|auto_generated|rdptr_g1p|counter5a2\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\);

-- Location: LCCOMB_X41_Y24_N16
\dcfifo_component|auto_generated|rdptr_g1p|counter5a3~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\ $ (((\dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\ & \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~0_combout\);

-- Location: FF_X41_Y24_N17
\dcfifo_component|auto_generated|rdptr_g1p|counter5a3\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\);

-- Location: LCCOMB_X41_Y24_N6
\dcfifo_component|auto_generated|rdptr_g1p|_~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~5_combout\ = (\dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\ & (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\ & (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\ & 
-- \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|_~4_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~5_combout\);

-- Location: LCCOMB_X41_Y24_N20
\dcfifo_component|auto_generated|rdptr_g1p|counter5a5~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|_~5_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|_~5_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~0_combout\);

-- Location: FF_X41_Y24_N21
\dcfifo_component|auto_generated|rdptr_g1p|counter5a5\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\);

-- Location: LCCOMB_X38_Y24_N16
\dcfifo_component|auto_generated|rdptr_g1p|counter5a6~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\ $ (((\dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\ & 
-- \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~0_combout\);

-- Location: FF_X38_Y24_N17
\dcfifo_component|auto_generated|rdptr_g1p|counter5a6\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\);

-- Location: LCCOMB_X38_Y24_N10
\dcfifo_component|auto_generated|rdptr_g1p|counter5a7~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\ $ (((!\dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\ & (\dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\ & 
-- \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011010011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|cntr_cout[5]~0_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~0_combout\);

-- Location: FF_X38_Y24_N11
\dcfifo_component|auto_generated|rdptr_g1p|counter5a7\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\);

-- Location: LCCOMB_X41_Y24_N18
\dcfifo_component|auto_generated|rdptr_g1p|_~10\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~10_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\ $ 
-- (\dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|counter5a4~q\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a7~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a6~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a5~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~10_combout\);

-- Location: FF_X41_Y24_N19
\dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|_~10_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a\(1));

-- Location: LCCOMB_X39_Y24_N4
\dcfifo_component|auto_generated|rdptr_g1p|counter5a9~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~q\ $ (((\dcfifo_component|auto_generated|rdptr_g1p|_~7_combout\ & \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100001111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|_~7_combout\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~0_combout\);

-- Location: FF_X39_Y24_N5
\dcfifo_component|auto_generated|rdptr_g1p|counter5a9\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~q\);

-- Location: LCCOMB_X39_Y24_N30
\dcfifo_component|auto_generated|rdptr_g1p|counter5a10~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~q\ $ (((\dcfifo_component|auto_generated|rdptr_g1p|_~7_combout\ & !\dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001011010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|_~7_combout\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~0_combout\);

-- Location: FF_X39_Y24_N31
\dcfifo_component|auto_generated|rdptr_g1p|counter5a10\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~q\);

-- Location: LCCOMB_X41_Y24_N8
\dcfifo_component|auto_generated|rdptr_g1p|_~9\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~9_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|counter5a9~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|counter5a10~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a8~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~9_combout\);

-- Location: FF_X41_Y24_N9
\dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|_~9_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a\(2));

-- Location: LCCOMB_X41_Y24_N12
\dcfifo_component|auto_generated|rdptr_g1p|_~11\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~11_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\ $ 
-- (!\dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001101001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a2~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~11_combout\);

-- Location: FF_X41_Y24_N13
\dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|_~11_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a\(0));

-- Location: LCCOMB_X41_Y24_N0
\dcfifo_component|auto_generated|rdptr_g1p|_~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~8_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a\(1) $ (\dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a\(2) $ (!\dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a\(1),
	datac => \dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a\(2),
	datad => \dcfifo_component|auto_generated|rdptr_g1p|sub_parity7a\(0),
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~8_combout\);

-- Location: FF_X41_Y24_N1
\dcfifo_component|auto_generated|rdptr_g1p|parity6\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|_~8_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|parity6~q\);

-- Location: LCCOMB_X41_Y24_N28
\dcfifo_component|auto_generated|rdptr_g1p|counter5a1~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\ $ (((!\dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\ & (!\dcfifo_component|auto_generated|rdptr_g1p|parity6~q\ & 
-- \dcfifo_component|auto_generated|valid_rdreq~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\,
	datab => \dcfifo_component|auto_generated|rdptr_g1p|parity6~q\,
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\,
	datad => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~0_combout\);

-- Location: FF_X41_Y24_N29
\dcfifo_component|auto_generated|rdptr_g1p|counter5a1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\);

-- Location: FF_X39_Y24_N7
\dcfifo_component|auto_generated|rdptr_g[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|rdptr_g1p|counter5a1~q\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(1));

-- Location: FF_X38_Y25_N5
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|rdptr_g\(1),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(1));

-- Location: FF_X41_Y25_N29
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(1),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(1));

-- Location: LCCOMB_X39_Y24_N26
\dcfifo_component|auto_generated|rdptr_g[3]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g[3]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a3~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g[3]~feeder_combout\);

-- Location: FF_X39_Y24_N27
\dcfifo_component|auto_generated|rdptr_g[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g[3]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(3));

-- Location: LCCOMB_X41_Y23_N8
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[3]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[3]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g\(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g\(3),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[3]~feeder_combout\);

-- Location: FF_X41_Y23_N9
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[3]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(3));

-- Location: FF_X41_Y25_N3
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(3),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(3));

-- Location: LCCOMB_X41_Y25_N2
\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~7_combout\ = (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(0) & ((\dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) $ 
-- (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(3))) # (!\dcfifo_component|auto_generated|wrptr_gp|counter13a\(0)))) # (!\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(0) & 
-- ((\dcfifo_component|auto_generated|wrptr_gp|counter13a\(0)) # (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111110110111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(0),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(3),
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0),
	combout => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~7_combout\);

-- Location: LCCOMB_X41_Y25_N28
\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~8_combout\ = (\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~7_combout\) # (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(1) $ 
-- (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(1),
	datad => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~7_combout\,
	combout => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~8_combout\);

-- Location: LCCOMB_X41_Y25_N30
\dcfifo_component|auto_generated|valid_wrreq~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|valid_wrreq~0_combout\ = (\wrreq~input_o\ & ((\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~6_combout\) # (\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~8_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~6_combout\,
	datab => \wrreq~input_o\,
	datad => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~8_combout\,
	combout => \dcfifo_component|auto_generated|valid_wrreq~0_combout\);

-- Location: IOIBUF_X27_Y0_N15
\wrclk~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_wrclk,
	o => \wrclk~input_o\);

-- Location: CLKCTRL_G17
\wrclk~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \wrclk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \wrclk~inputclkctrl_outclk\);

-- Location: IOIBUF_X27_Y0_N22
\rdclk~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rdclk,
	o => \rdclk~input_o\);

-- Location: CLKCTRL_G19
\rdclk~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \rdclk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \rdclk~inputclkctrl_outclk\);

-- Location: IOIBUF_X43_Y41_N8
\data[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(0),
	o => \data[0]~input_o\);

-- Location: LCCOMB_X42_Y25_N4
\dcfifo_component|auto_generated|ram_address_a[9]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ram_address_a[9]~0_combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(9))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10),
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(9),
	combout => \dcfifo_component|auto_generated|ram_address_a[9]~0_combout\);

-- Location: LCCOMB_X41_Y24_N22
\dcfifo_component|auto_generated|rdptr_g1p|counter5a0~_wirecell\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~_wirecell_combout\ = !\dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|counter5a0~_wirecell_combout\);

-- Location: LCCOMB_X41_Y24_N30
\dcfifo_component|auto_generated|rdptr_g1p|_~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g1p|_~0_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~q\ $ (\dcfifo_component|auto_generated|rdptr_g1p|counter5a10~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~q\,
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g1p|_~0_combout\);

-- Location: IOIBUF_X21_Y0_N8
\data[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(1),
	o => \data[1]~input_o\);

-- Location: IOIBUF_X43_Y0_N8
\data[2]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(2),
	o => \data[2]~input_o\);

-- Location: IOIBUF_X52_Y13_N8
\data[3]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(3),
	o => \data[3]~input_o\);

-- Location: IOIBUF_X43_Y0_N1
\data[4]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(4),
	o => \data[4]~input_o\);

-- Location: IOIBUF_X38_Y0_N8
\data[5]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(5),
	o => \data[5]~input_o\);

-- Location: IOIBUF_X41_Y0_N22
\data[6]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(6),
	o => \data[6]~input_o\);

-- Location: IOIBUF_X41_Y41_N22
\data[7]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(7),
	o => \data[7]~input_o\);

-- Location: IOIBUF_X46_Y0_N1
\data[8]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(8),
	o => \data[8]~input_o\);

-- Location: M9K_X40_Y24_N0
\dcfifo_component|auto_generated|fifo_ram|ram_block14a0\ : cycloneiv_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_output_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "dcfifo:dcfifo_component|dcfifo_rcj1:auto_generated|altsyncram_4q31:fifo_ram|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 10,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 9,
	port_a_first_address => 0,
	port_a_first_bit_number => 0,
	port_a_last_address => 1023,
	port_a_logical_ram_depth => 1024,
	port_a_logical_ram_width => 23,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "clear1",
	port_b_address_clock => "clock1",
	port_b_address_width => 10,
	port_b_data_out_clear => "clear1",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 9,
	port_b_first_address => 0,
	port_b_first_bit_number => 0,
	port_b_last_address => 1023,
	port_b_logical_ram_depth => 1024,
	port_b_logical_ram_width => 23,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	portbre => VCC,
	portbaddrstall => \dcfifo_component|auto_generated|ALT_INV_valid_rdreq~0_combout\,
	clk0 => \wrclk~inputclkctrl_outclk\,
	clk1 => \rdclk~inputclkctrl_outclk\,
	ena0 => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	ena1 => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	clr1 => \aclr~inputclkctrl_outclk\,
	portadatain => \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTADATAIN_bus\,
	portaaddr => \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTAADDR_bus\,
	portbaddr => \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \dcfifo_component|auto_generated|fifo_ram|ram_block14a0_PORTBDATAOUT_bus\);

-- Location: IOIBUF_X52_Y31_N1
\data[9]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(9),
	o => \data[9]~input_o\);

-- Location: IOIBUF_X31_Y0_N22
\data[10]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(10),
	o => \data[10]~input_o\);

-- Location: IOIBUF_X52_Y12_N8
\data[11]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(11),
	o => \data[11]~input_o\);

-- Location: IOIBUF_X31_Y0_N1
\data[12]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(12),
	o => \data[12]~input_o\);

-- Location: IOIBUF_X41_Y0_N1
\data[13]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(13),
	o => \data[13]~input_o\);

-- Location: IOIBUF_X52_Y19_N8
\data[14]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(14),
	o => \data[14]~input_o\);

-- Location: IOIBUF_X41_Y41_N8
\data[15]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(15),
	o => \data[15]~input_o\);

-- Location: IOIBUF_X52_Y23_N1
\data[16]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(16),
	o => \data[16]~input_o\);

-- Location: IOIBUF_X52_Y18_N8
\data[17]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(17),
	o => \data[17]~input_o\);

-- Location: M9K_X40_Y23_N0
\dcfifo_component|auto_generated|fifo_ram|ram_block14a9\ : cycloneiv_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_output_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "dcfifo:dcfifo_component|dcfifo_rcj1:auto_generated|altsyncram_4q31:fifo_ram|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 10,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 9,
	port_a_first_address => 0,
	port_a_first_bit_number => 9,
	port_a_last_address => 1023,
	port_a_logical_ram_depth => 1024,
	port_a_logical_ram_width => 23,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "clear1",
	port_b_address_clock => "clock1",
	port_b_address_width => 10,
	port_b_data_out_clear => "clear1",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 9,
	port_b_first_address => 0,
	port_b_first_bit_number => 9,
	port_b_last_address => 1023,
	port_b_logical_ram_depth => 1024,
	port_b_logical_ram_width => 23,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	portbre => VCC,
	portbaddrstall => \dcfifo_component|auto_generated|ALT_INV_valid_rdreq~0_combout\,
	clk0 => \wrclk~inputclkctrl_outclk\,
	clk1 => \rdclk~inputclkctrl_outclk\,
	ena0 => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	ena1 => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	clr1 => \aclr~inputclkctrl_outclk\,
	portadatain => \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTADATAIN_bus\,
	portaaddr => \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTAADDR_bus\,
	portbaddr => \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \dcfifo_component|auto_generated|fifo_ram|ram_block14a9_PORTBDATAOUT_bus\);

-- Location: IOIBUF_X52_Y30_N8
\data[18]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(18),
	o => \data[18]~input_o\);

-- Location: IOIBUF_X34_Y0_N1
\data[19]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(19),
	o => \data[19]~input_o\);

-- Location: IOIBUF_X41_Y41_N15
\data[20]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(20),
	o => \data[20]~input_o\);

-- Location: IOIBUF_X52_Y28_N8
\data[21]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(21),
	o => \data[21]~input_o\);

-- Location: IOIBUF_X41_Y0_N8
\data[22]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data(22),
	o => \data[22]~input_o\);

-- Location: M9K_X40_Y25_N0
\dcfifo_component|auto_generated|fifo_ram|ram_block14a18\ : cycloneiv_ram_block
-- pragma translate_off
GENERIC MAP (
	clk0_core_clock_enable => "ena0",
	clk1_output_clock_enable => "ena1",
	data_interleave_offset_in_bits => 1,
	data_interleave_width_in_bits => 1,
	logical_ram_name => "dcfifo:dcfifo_component|dcfifo_rcj1:auto_generated|altsyncram_4q31:fifo_ram|ALTSYNCRAM",
	mixed_port_feed_through_mode => "dont_care",
	operation_mode => "dual_port",
	port_a_address_clear => "none",
	port_a_address_width => 10,
	port_a_byte_enable_clock => "none",
	port_a_data_out_clear => "none",
	port_a_data_out_clock => "none",
	port_a_data_width => 9,
	port_a_first_address => 0,
	port_a_first_bit_number => 18,
	port_a_last_address => 1023,
	port_a_logical_ram_depth => 1024,
	port_a_logical_ram_width => 23,
	port_a_read_during_write_mode => "new_data_with_nbe_read",
	port_b_address_clear => "clear1",
	port_b_address_clock => "clock1",
	port_b_address_width => 10,
	port_b_data_out_clear => "clear1",
	port_b_data_out_clock => "clock1",
	port_b_data_width => 9,
	port_b_first_address => 0,
	port_b_first_bit_number => 18,
	port_b_last_address => 1023,
	port_b_logical_ram_depth => 1024,
	port_b_logical_ram_width => 23,
	port_b_read_during_write_mode => "new_data_with_nbe_read",
	port_b_read_enable_clock => "clock1",
	ram_block_type => "M9K")
-- pragma translate_on
PORT MAP (
	portawe => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	portbre => VCC,
	portbaddrstall => \dcfifo_component|auto_generated|ALT_INV_valid_rdreq~0_combout\,
	clk0 => \wrclk~inputclkctrl_outclk\,
	clk1 => \rdclk~inputclkctrl_outclk\,
	ena0 => \dcfifo_component|auto_generated|valid_wrreq~0_combout\,
	ena1 => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	clr1 => \aclr~inputclkctrl_outclk\,
	portadatain => \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTADATAIN_bus\,
	portaaddr => \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTAADDR_bus\,
	portbaddr => \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTBADDR_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	portbdataout => \dcfifo_component|auto_generated|fifo_ram|ram_block14a18_PORTBDATAOUT_bus\);

-- Location: LCCOMB_X38_Y25_N26
\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire\(0) = (\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~7_combout\) # ((\dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~6_combout\) # 
-- (\dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(1) $ (\dcfifo_component|auto_generated|rdptr_g\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~7_combout\,
	datab => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire[0]~6_combout\,
	datac => \dcfifo_component|auto_generated|rs_dgwp|dffpipe15|dffe17a\(1),
	datad => \dcfifo_component|auto_generated|rdptr_g\(1),
	combout => \dcfifo_component|auto_generated|rdempty_eq_comp|aneb_result_wire\(0));

-- Location: LCCOMB_X38_Y25_N0
\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire\(0) = (\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~6_combout\) # ((\dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~7_combout\) # 
-- (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(1) $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1),
	datab => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~6_combout\,
	datac => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire[0]~7_combout\,
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(1),
	combout => \dcfifo_component|auto_generated|wrfull_eq_comp|aneb_result_wire\(0));

-- Location: LCCOMB_X42_Y25_N2
\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7~combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(7) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(8) $ 
-- (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(10),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(7),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(8),
	datad => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(9),
	combout => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7~combout\);

-- Location: LCCOMB_X42_Y25_N28
\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4~combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(4) $ 
-- (\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(4),
	datad => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7~combout\,
	combout => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4~combout\);

-- Location: LCCOMB_X42_Y25_N22
\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor1~combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) $ 
-- (\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(1),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3),
	datad => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4~combout\,
	combout => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor1~combout\);

-- Location: LCCOMB_X42_Y25_N12
\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor0~combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0) $ (\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor1~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(0),
	datac => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor1~combout\,
	combout => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor0~combout\);

-- Location: FF_X42_Y25_N13
\dcfifo_component|auto_generated|ws_bwp|dffe18a[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor0~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(0));

-- Location: LCCOMB_X37_Y25_N20
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[0]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[0]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g\(0),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[0]~feeder_combout\);

-- Location: FF_X37_Y25_N21
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[0]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(0));

-- Location: LCCOMB_X37_Y25_N16
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[0]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[0]~feeder_combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(0),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[0]~feeder_combout\);

-- Location: FF_X37_Y25_N17
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[0]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(0));

-- Location: LCCOMB_X38_Y24_N8
\dcfifo_component|auto_generated|rdptr_g[10]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g[10]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|rdptr_g1p|counter5a10~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g[10]~feeder_combout\);

-- Location: FF_X38_Y24_N9
\dcfifo_component|auto_generated|rdptr_g[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g[10]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(10));

-- Location: LCCOMB_X42_Y24_N8
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[10]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[10]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g\(10)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g\(10),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[10]~feeder_combout\);

-- Location: FF_X42_Y24_N9
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[10]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(10));

-- Location: FF_X42_Y24_N1
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(10),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(10));

-- Location: LCCOMB_X42_Y24_N2
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[8]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[8]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g\(8)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g\(8),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[8]~feeder_combout\);

-- Location: FF_X42_Y24_N3
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[8]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(8));

-- Location: FF_X41_Y25_N5
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(8),
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(8));

-- Location: LCCOMB_X38_Y24_N28
\dcfifo_component|auto_generated|rdptr_g[9]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|rdptr_g[9]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~q\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g1p|counter5a9~q\,
	combout => \dcfifo_component|auto_generated|rdptr_g[9]~feeder_combout\);

-- Location: FF_X38_Y24_N29
\dcfifo_component|auto_generated|rdptr_g[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \rdclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|rdptr_g[9]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	ena => \dcfifo_component|auto_generated|valid_rdreq~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|rdptr_g\(9));

-- Location: LCCOMB_X42_Y24_N12
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[9]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[9]~feeder_combout\ = \dcfifo_component|auto_generated|rdptr_g\(9)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|rdptr_g\(9),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[9]~feeder_combout\);

-- Location: FF_X42_Y24_N13
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a[9]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(9));

-- Location: LCCOMB_X42_Y24_N10
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[9]~feeder\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[9]~feeder_combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(9)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe20a\(9),
	combout => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[9]~feeder_combout\);

-- Location: FF_X42_Y24_N11
\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low",
	x_on_violation => "off")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a[9]~feeder_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(9));

-- Location: LCCOMB_X43_Y25_N6
\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7~combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(7) $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(10) $ 
-- (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(8) $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(7),
	datab => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(10),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(8),
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(9),
	combout => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7~combout\);

-- Location: LCCOMB_X43_Y25_N0
\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4~combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(6) $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(4) $ 
-- (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(5) $ (\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(6),
	datab => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(4),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(5),
	datad => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7~combout\,
	combout => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4~combout\);

-- Location: LCCOMB_X38_Y25_N6
\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor1~combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(3) $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(1) $ 
-- (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(2) $ (\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(3),
	datab => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(1),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(2),
	datad => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4~combout\,
	combout => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor1~combout\);

-- Location: LCCOMB_X37_Y25_N18
\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor0~combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(0) $ (\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor1~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(0),
	datad => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor1~combout\,
	combout => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor0~combout\);

-- Location: FF_X37_Y25_N19
\dcfifo_component|auto_generated|ws_brp|dffe18a[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor0~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_brp|dffe18a\(0));

-- Location: LCCOMB_X43_Y25_N12
\dcfifo_component|auto_generated|op_1~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|op_1~0_combout\ = (\dcfifo_component|auto_generated|ws_bwp|dffe18a\(0) & ((GND) # (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(0)))) # (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(0) & 
-- (\dcfifo_component|auto_generated|ws_brp|dffe18a\(0) $ (GND)))
-- \dcfifo_component|auto_generated|op_1~1\ = CARRY((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(0)) # (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(0),
	datab => \dcfifo_component|auto_generated|ws_brp|dffe18a\(0),
	datad => VCC,
	combout => \dcfifo_component|auto_generated|op_1~0_combout\,
	cout => \dcfifo_component|auto_generated|op_1~1\);

-- Location: FF_X38_Y25_N7
\dcfifo_component|auto_generated|ws_brp|dffe18a[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor1~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_brp|dffe18a\(1));

-- Location: FF_X42_Y25_N23
\dcfifo_component|auto_generated|ws_bwp|dffe18a[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor1~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(1));

-- Location: LCCOMB_X43_Y25_N14
\dcfifo_component|auto_generated|op_1~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|op_1~2_combout\ = (\dcfifo_component|auto_generated|ws_brp|dffe18a\(1) & ((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(1) & (!\dcfifo_component|auto_generated|op_1~1\)) # 
-- (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(1) & ((\dcfifo_component|auto_generated|op_1~1\) # (GND))))) # (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(1) & ((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(1) & 
-- (\dcfifo_component|auto_generated|op_1~1\ & VCC)) # (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(1) & (!\dcfifo_component|auto_generated|op_1~1\))))
-- \dcfifo_component|auto_generated|op_1~3\ = CARRY((\dcfifo_component|auto_generated|ws_brp|dffe18a\(1) & ((!\dcfifo_component|auto_generated|op_1~1\) # (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(1)))) # 
-- (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(1) & (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(1) & !\dcfifo_component|auto_generated|op_1~1\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_brp|dffe18a\(1),
	datab => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(1),
	datad => VCC,
	cin => \dcfifo_component|auto_generated|op_1~1\,
	combout => \dcfifo_component|auto_generated|op_1~2_combout\,
	cout => \dcfifo_component|auto_generated|op_1~3\);

-- Location: LCCOMB_X42_Y25_N8
\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor2~combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(2) $ (\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100101100110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3),
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(2),
	datad => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4~combout\,
	combout => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor2~combout\);

-- Location: FF_X42_Y25_N9
\dcfifo_component|auto_generated|ws_bwp|dffe18a[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor2~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(2));

-- Location: LCCOMB_X38_Y25_N16
\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor2~combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(3) $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(2) $ 
-- (\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(3),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(2),
	datad => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4~combout\,
	combout => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor2~combout\);

-- Location: FF_X38_Y25_N17
\dcfifo_component|auto_generated|ws_brp|dffe18a[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor2~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_brp|dffe18a\(2));

-- Location: LCCOMB_X43_Y25_N16
\dcfifo_component|auto_generated|op_1~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|op_1~4_combout\ = ((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(2) $ (\dcfifo_component|auto_generated|ws_brp|dffe18a\(2) $ (\dcfifo_component|auto_generated|op_1~3\)))) # (GND)
-- \dcfifo_component|auto_generated|op_1~5\ = CARRY((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(2) & ((!\dcfifo_component|auto_generated|op_1~3\) # (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(2)))) # 
-- (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(2) & (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(2) & !\dcfifo_component|auto_generated|op_1~3\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(2),
	datab => \dcfifo_component|auto_generated|ws_brp|dffe18a\(2),
	datad => VCC,
	cin => \dcfifo_component|auto_generated|op_1~3\,
	combout => \dcfifo_component|auto_generated|op_1~4_combout\,
	cout => \dcfifo_component|auto_generated|op_1~5\);

-- Location: LCCOMB_X42_Y25_N10
\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor3~combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3) $ (\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(3),
	datad => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4~combout\,
	combout => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor3~combout\);

-- Location: FF_X42_Y25_N11
\dcfifo_component|auto_generated|ws_bwp|dffe18a[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor3~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(3));

-- Location: LCCOMB_X38_Y25_N18
\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor3~combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(3) $ (\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(3),
	datad => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4~combout\,
	combout => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor3~combout\);

-- Location: FF_X38_Y25_N19
\dcfifo_component|auto_generated|ws_brp|dffe18a[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor3~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_brp|dffe18a\(3));

-- Location: LCCOMB_X43_Y25_N18
\dcfifo_component|auto_generated|op_1~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|op_1~6_combout\ = (\dcfifo_component|auto_generated|ws_bwp|dffe18a\(3) & ((\dcfifo_component|auto_generated|ws_brp|dffe18a\(3) & (!\dcfifo_component|auto_generated|op_1~5\)) # 
-- (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(3) & (\dcfifo_component|auto_generated|op_1~5\ & VCC)))) # (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(3) & ((\dcfifo_component|auto_generated|ws_brp|dffe18a\(3) & 
-- ((\dcfifo_component|auto_generated|op_1~5\) # (GND))) # (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(3) & (!\dcfifo_component|auto_generated|op_1~5\))))
-- \dcfifo_component|auto_generated|op_1~7\ = CARRY((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(3) & (\dcfifo_component|auto_generated|ws_brp|dffe18a\(3) & !\dcfifo_component|auto_generated|op_1~5\)) # 
-- (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(3) & ((\dcfifo_component|auto_generated|ws_brp|dffe18a\(3)) # (!\dcfifo_component|auto_generated|op_1~5\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100101001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(3),
	datab => \dcfifo_component|auto_generated|ws_brp|dffe18a\(3),
	datad => VCC,
	cin => \dcfifo_component|auto_generated|op_1~5\,
	combout => \dcfifo_component|auto_generated|op_1~6_combout\,
	cout => \dcfifo_component|auto_generated|op_1~7\);

-- Location: FF_X42_Y25_N29
\dcfifo_component|auto_generated|ws_bwp|dffe18a[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor4~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(4));

-- Location: FF_X43_Y25_N1
\dcfifo_component|auto_generated|ws_brp|dffe18a[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor4~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_brp|dffe18a\(4));

-- Location: LCCOMB_X43_Y25_N20
\dcfifo_component|auto_generated|op_1~8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|op_1~8_combout\ = ((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(4) $ (\dcfifo_component|auto_generated|ws_brp|dffe18a\(4) $ (\dcfifo_component|auto_generated|op_1~7\)))) # (GND)
-- \dcfifo_component|auto_generated|op_1~9\ = CARRY((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(4) & ((!\dcfifo_component|auto_generated|op_1~7\) # (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(4)))) # 
-- (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(4) & (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(4) & !\dcfifo_component|auto_generated|op_1~7\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(4),
	datab => \dcfifo_component|auto_generated|ws_brp|dffe18a\(4),
	datad => VCC,
	cin => \dcfifo_component|auto_generated|op_1~7\,
	combout => \dcfifo_component|auto_generated|op_1~8_combout\,
	cout => \dcfifo_component|auto_generated|op_1~9\);

-- Location: LCCOMB_X43_Y25_N10
\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor5~combout\ = \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7~combout\ $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(5) $ 
-- (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7~combout\,
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(5),
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(6),
	combout => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor5~combout\);

-- Location: FF_X43_Y25_N11
\dcfifo_component|auto_generated|ws_brp|dffe18a[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor5~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_brp|dffe18a\(5));

-- Location: LCCOMB_X42_Y25_N14
\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor5~combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) $ (\dcfifo_component|auto_generated|wrptr_gp|counter13a\(5) $ (\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6),
	datac => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(5),
	datad => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7~combout\,
	combout => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor5~combout\);

-- Location: FF_X42_Y25_N15
\dcfifo_component|auto_generated|ws_bwp|dffe18a[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor5~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(5));

-- Location: LCCOMB_X43_Y25_N22
\dcfifo_component|auto_generated|op_1~10\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|op_1~10_combout\ = (\dcfifo_component|auto_generated|ws_brp|dffe18a\(5) & ((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(5) & (!\dcfifo_component|auto_generated|op_1~9\)) # 
-- (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(5) & ((\dcfifo_component|auto_generated|op_1~9\) # (GND))))) # (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(5) & ((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(5) & 
-- (\dcfifo_component|auto_generated|op_1~9\ & VCC)) # (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(5) & (!\dcfifo_component|auto_generated|op_1~9\))))
-- \dcfifo_component|auto_generated|op_1~11\ = CARRY((\dcfifo_component|auto_generated|ws_brp|dffe18a\(5) & ((!\dcfifo_component|auto_generated|op_1~9\) # (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(5)))) # 
-- (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(5) & (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(5) & !\dcfifo_component|auto_generated|op_1~9\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_brp|dffe18a\(5),
	datab => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(5),
	datad => VCC,
	cin => \dcfifo_component|auto_generated|op_1~9\,
	combout => \dcfifo_component|auto_generated|op_1~10_combout\,
	cout => \dcfifo_component|auto_generated|op_1~11\);

-- Location: LCCOMB_X42_Y25_N0
\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor6~combout\ = \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6) $ (\dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \dcfifo_component|auto_generated|wrptr_gp|counter13a\(6),
	datad => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7~combout\,
	combout => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor6~combout\);

-- Location: FF_X42_Y25_N1
\dcfifo_component|auto_generated|ws_bwp|dffe18a[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor6~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(6));

-- Location: LCCOMB_X43_Y25_N4
\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor6~combout\ = \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7~combout\ $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(6))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7~combout\,
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(6),
	combout => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor6~combout\);

-- Location: FF_X43_Y25_N5
\dcfifo_component|auto_generated|ws_brp|dffe18a[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor6~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_brp|dffe18a\(6));

-- Location: LCCOMB_X43_Y25_N24
\dcfifo_component|auto_generated|op_1~12\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|op_1~12_combout\ = ((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(6) $ (\dcfifo_component|auto_generated|ws_brp|dffe18a\(6) $ (\dcfifo_component|auto_generated|op_1~11\)))) # (GND)
-- \dcfifo_component|auto_generated|op_1~13\ = CARRY((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(6) & ((!\dcfifo_component|auto_generated|op_1~11\) # (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(6)))) # 
-- (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(6) & (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(6) & !\dcfifo_component|auto_generated|op_1~11\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(6),
	datab => \dcfifo_component|auto_generated|ws_brp|dffe18a\(6),
	datad => VCC,
	cin => \dcfifo_component|auto_generated|op_1~11\,
	combout => \dcfifo_component|auto_generated|op_1~12_combout\,
	cout => \dcfifo_component|auto_generated|op_1~13\);

-- Location: FF_X43_Y25_N7
\dcfifo_component|auto_generated|ws_brp|dffe18a[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor7~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_brp|dffe18a\(7));

-- Location: FF_X42_Y25_N3
\dcfifo_component|auto_generated|ws_bwp|dffe18a[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|wrptr_g_gray2bin|xor7~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(7));

-- Location: LCCOMB_X43_Y25_N26
\dcfifo_component|auto_generated|op_1~14\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|op_1~14_combout\ = (\dcfifo_component|auto_generated|ws_brp|dffe18a\(7) & ((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(7) & (!\dcfifo_component|auto_generated|op_1~13\)) # 
-- (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(7) & ((\dcfifo_component|auto_generated|op_1~13\) # (GND))))) # (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(7) & ((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(7) & 
-- (\dcfifo_component|auto_generated|op_1~13\ & VCC)) # (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(7) & (!\dcfifo_component|auto_generated|op_1~13\))))
-- \dcfifo_component|auto_generated|op_1~15\ = CARRY((\dcfifo_component|auto_generated|ws_brp|dffe18a\(7) & ((!\dcfifo_component|auto_generated|op_1~13\) # (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(7)))) # 
-- (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(7) & (!\dcfifo_component|auto_generated|ws_bwp|dffe18a\(7) & !\dcfifo_component|auto_generated|op_1~13\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100100101011",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_brp|dffe18a\(7),
	datab => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(7),
	datad => VCC,
	cin => \dcfifo_component|auto_generated|op_1~13\,
	combout => \dcfifo_component|auto_generated|op_1~14_combout\,
	cout => \dcfifo_component|auto_generated|op_1~15\);

-- Location: LCCOMB_X43_Y25_N8
\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor8\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor8~combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(9) $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(8) $ 
-- (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010101011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(9),
	datac => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(8),
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(10),
	combout => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor8~combout\);

-- Location: FF_X43_Y25_N9
\dcfifo_component|auto_generated|ws_brp|dffe18a[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor8~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_brp|dffe18a\(8));

-- Location: FF_X43_Y25_N29
\dcfifo_component|auto_generated|ws_bwp|dffe18a[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	asdata => \dcfifo_component|auto_generated|wrptr_gp|_~6_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(8));

-- Location: LCCOMB_X43_Y25_N28
\dcfifo_component|auto_generated|op_1~16\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|op_1~16_combout\ = ((\dcfifo_component|auto_generated|ws_brp|dffe18a\(8) $ (\dcfifo_component|auto_generated|ws_bwp|dffe18a\(8) $ (\dcfifo_component|auto_generated|op_1~15\)))) # (GND)
-- \dcfifo_component|auto_generated|op_1~17\ = CARRY((\dcfifo_component|auto_generated|ws_brp|dffe18a\(8) & (\dcfifo_component|auto_generated|ws_bwp|dffe18a\(8) & !\dcfifo_component|auto_generated|op_1~15\)) # 
-- (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(8) & ((\dcfifo_component|auto_generated|ws_bwp|dffe18a\(8)) # (!\dcfifo_component|auto_generated|op_1~15\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011001001101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_brp|dffe18a\(8),
	datab => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(8),
	datad => VCC,
	cin => \dcfifo_component|auto_generated|op_1~15\,
	combout => \dcfifo_component|auto_generated|op_1~16_combout\,
	cout => \dcfifo_component|auto_generated|op_1~17\);

-- Location: FF_X42_Y25_N5
\dcfifo_component|auto_generated|ws_bwp|dffe18a[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ram_address_a[9]~0_combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(9));

-- Location: LCCOMB_X43_Y25_N2
\dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor9\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor9~combout\ = \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(9) $ (\dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(10))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010110101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(9),
	datad => \dcfifo_component|auto_generated|ws_dgrp|dffpipe19|dffe21a\(10),
	combout => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor9~combout\);

-- Location: FF_X43_Y25_N3
\dcfifo_component|auto_generated|ws_brp|dffe18a[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wrclk~inputclkctrl_outclk\,
	d => \dcfifo_component|auto_generated|ws_dgrp_gray2bin|xor9~combout\,
	clrn => \ALT_INV_aclr~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \dcfifo_component|auto_generated|ws_brp|dffe18a\(9));

-- Location: LCCOMB_X43_Y25_N30
\dcfifo_component|auto_generated|op_1~18\ : cycloneiv_lcell_comb
-- Equation(s):
-- \dcfifo_component|auto_generated|op_1~18_combout\ = \dcfifo_component|auto_generated|ws_bwp|dffe18a\(9) $ (\dcfifo_component|auto_generated|op_1~17\ $ (!\dcfifo_component|auto_generated|ws_brp|dffe18a\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101010100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \dcfifo_component|auto_generated|ws_bwp|dffe18a\(9),
	datad => \dcfifo_component|auto_generated|ws_brp|dffe18a\(9),
	cin => \dcfifo_component|auto_generated|op_1~17\,
	combout => \dcfifo_component|auto_generated|op_1~18_combout\);
END structure;



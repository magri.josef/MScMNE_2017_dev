transcript on
vmap altera_mf /home/josef/Documents/MScMNE_2017_dev/src/sim/gen/discrete_comp/column_fifo_only/vhdl_libs/altera_mf
vmap altera /home/josef/Documents/MScMNE_2017_dev/src/sim/gen/discrete_comp/column_fifo_only/vhdl_libs/altera
vmap lpm /home/josef/Documents/MScMNE_2017_dev/src/sim/gen/discrete_comp/column_fifo_only/vhdl_libs/lpm
vmap sgate /home/josef/Documents/MScMNE_2017_dev/src/sim/gen/discrete_comp/column_fifo_only/vhdl_libs/sgate
vmap cycloneiv_hssi /home/josef/Documents/MScMNE_2017_dev/src/sim/gen/discrete_comp/column_fifo_only/vhdl_libs/cycloneiv_hssi
vmap cycloneiv_pcie_hip /home/josef/Documents/MScMNE_2017_dev/src/sim/gen/discrete_comp/column_fifo_only/vhdl_libs/cycloneiv_pcie_hip
vmap cycloneiv /home/josef/Documents/MScMNE_2017_dev/src/sim/gen/discrete_comp/column_fifo_only/vhdl_libs/cycloneiv
if {[file exists gate_work]} {
	vdel -lib gate_work -all
}
vlib gate_work
vmap work gate_work

vcom -93 -work work {fifo_only.vho}


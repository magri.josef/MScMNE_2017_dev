-------------------------------------------------------------------------------
-- File : ANDGate.vhd
-- Description : 5 in 1 out and gate
-- Designer : Josef Magri (University of Malta CERN-EP/UAI)
-------------------------------------------------------------------------------
-- http://www.datasheetcatalog.com/datasheets_pdf/7/4/A/C/74ACT11.shtml
-- The original version is build from multiple 3 input and gates

--import std_logic from the IEEE library
library ieee;
use ieee.std_logic_1164.all;

--ENTITY DECLARATION: name, inputs, outputs
entity andGate is
   port( 	in_0  : in std_logic;
   			in_1  : in std_logic;
   			in_2  : in std_logic;
            in_3  : in std_logic;
            in_5  : in std_logic;
         	out_0 : out std_logic);
end andGate;

--FUNCTIONAL DESCRIPTION: how the Inverter works
architecture func of andGate is
begin
	out_0 <= (in_0 AND in_1 AND in_2 AND in_3 AND in_4 AND in_5);
end func;
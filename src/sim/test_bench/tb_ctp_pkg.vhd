-------------------------------------------------------------------------------
-- File          : tb_ctp_pkg.vhd
-- Authors       : L.Musa, A. Rehman, C. Xiang (CERN-PH/ED)
-- Description   : act as CTP send serialBchannel and L1accept to RCU_TOP.
-- Note          : start by "enctp" pulse.
-- Contains five type of TTC trigger.
-- L0-L1a-L2a, L0-L1a-L2r, L0-L1r, SOD, EOD.
-------------------------------------------------------------------------------

library ieee; 
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE ieee.numeric_std.ALL; 
library std;
use std.textio.all;
LIBRARY tb_modules;
use tb_modules.tb_daq_pkg.all;
use tb_modules.tb_values.all;

package tb_ctp_pkg is
    ---------------------------------------------------------------------------
    -- constants
    ---------------------------------------------------------------------------
	constant T     : time := 25 ns;		
    
        procedure p_CTP(
        signal Channel_B     : out std_logic;    -- Bchannel
        signal Channel_A     : out std_logic;    -- L1Accept
        signal L0_LVDS       : out std_logic;    -- L0
        		suffix       : in string);
end;
    ---------------------------------------------------------------------------
    -- package body
    ---------------------------------------------------------------------------
package body tb_ctp_pkg is

    procedure p_CTP(
        signal Channel_B     : out std_logic;    -- Bchannel
        signal Channel_A     : out std_logic;    -- L1Accept
        signal L0_LVDS       : out std_logic;    -- L0
        		suffix       : in string)
    is
		variable in_line : line;
		variable str_7 : string(1 to 7); 
		file in_file : text open READ_MODE is path_in_ctp & suffix & ".txt";
	begin
		Channel_B<='1';
		Channel_A<='0';
		L0_LVDS<='0';
		wait for T*10;
		while not endfile(in_file) loop			
			readline(in_file,in_line);             
			read(in_line, str_7);  
			if str_7(1)='1' then
				Channel_B<='1';
			else
				Channel_B<='0';
			end if;
			if str_7(4)='1' then
				Channel_A<='1';
			else
				Channel_A<='0';
			end if;
			if str_7(7)='1' then
				L0_LVDS<='1';
			else
				L0_LVDS<='0';
			end if;
			wait for T;
		end loop;
	end p_CTP;
END PACKAGE BODY tb_ctp_pkg;
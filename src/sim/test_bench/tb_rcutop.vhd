-------------------------------------------------------------------------------
-- File        : TB_rcutop.vhd
-- Description : TB for rcu_top
-- Team        : Josef Magri
-- Work used/ code reused from : Johan Alme, Csaba Soos, A.Rehman, Changzhou.Xiang, Luciano Musa (CERN-PH/ED)
-------------------------------------------------------------------------------
-- Commands for text interpreter
-- "TXGAP" is used to delay, format as "TXGAP 100 us"
-- "ENCTP" is used to enable process U_CTP
-- "ENSIU" is used to enable process U_SIU
-------------------------------------------------------------------------------
-- U_SIU act as SIU.
-- "FCTRL" is used to send control command, for example 
-- "FSTRD" is used to read FEC status register and send command to SIU_interface
-- "RDYRX" is used to set signal rdyrx high
-- "EOBTR" is used to end of block transfer
-- "TXGAP" is used to delay, format as "TXGAP 100 us"
-- --"STBWR" is used to load block write --  STBWR 0x44444444   ThrPed01
-------------------------------------------------------------------------------
-- U_CTP act as CTP.
-- "L0L2a" is used to send L0-L1a_l2a sequence.
-- "L0L2r" is used to send L0-L1a_l2r sequence.
-- "L0L1r" is used to send L0-L1r sequence.
-------------------------------------------------------------------------------
-- Format for script
---------------------------------------
-- .......
-- .......
-- -END-
-- ** SIU commands ** 
-- .......
-- .......
-- -END-
-- ** CTP commands ** 
-- .......
-- .......
-- -END-
---------------------------------------
-- Note : "--" use to comment one line.
--        "** SIU means bellow script are for SIU
--        "** CTP means bellow script are for CTP
--        script need one "** SIU and one "** CTP.
--        "-END- means script for SIU/CTP is end. 
-------------------------------------------------------------------------------

-- IEEE standard libraries

library IEEE;
LIBRARY ttcrx_modules;
LIBRARY sim_top_modules;
LIBRARY tb_modules;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE ieee.numeric_std.ALL;

-- file I/O libraries

library std;
use std.textio.all;

-- test bench packages

use tb_modules.tb_daq_pkg.all;
use tb_modules.tb_ctp_pkg.all;
use tb_modules.tb_values.all;

entity test_RCU is
end test_RCU;

-------------------------------------------------------------------------------
architecture a_test of test_RCU is

	signal enctp				: std_logic:='0';
	signal ensiu				: std_logic:='0';
	signal dcs_clk				: std_logic:='0';
	constant Tsiuclk_shift		: time :=    12500 ps;  -- time shift base on siu_clk for SIU 
	constant Tdcsclk_shift		: time :=    3000 ps; --104 ps; -- time shift base on TTCLK for DCS ( CLOCK40DES1 deske)
	-- CLOKC40DES1 Fine delay register allows clock phase to be changed in steps of 104ps between 0 and 25ns

	--Inputs
	SIGNAL TTCLK   :  std_logic := '0';
	SIGNAL reset_n :  std_logic := '1';
	SIGNAL foCLK   :  std_logic := '0';
	SIGNAL fiDIR   :  std_logic := '0';
	SIGNAL fiLF_N  :  std_logic := '0';
	SIGNAL fiBEN_N :  std_logic := '0';
	SIGNAL busy    :  std_logic;

	--BiDirs
	SIGNAL fbD      :  std_logic_vector(31 downto 0):= (others=>'Z');
	SIGNAL fbTEN_N  :  std_logic :='1';
	SIGNAL fbCTRL_N :  std_logic :='1';

	--Outputs
	SIGNAL foBSY_N   :  std_logic;
	SIGNAL siu_clk   :  std_logic;
	SIGNAL Channel_A :  std_logic := '0';
	SIGNAL Channel_B :  std_logic := '1';
	SIGNAL L0_LVDS   :  std_logic := '0';

	Signal TOP_SEG_CLOCK              :  std_logic;
	Signal TOP_SEG_TRIG               :  std_logic;
	Signal TOP_L0_EXT_LVDS            :  std_logic;
	Signal TOP_RESETn_to_SEGMENT      :  std_logic;
	Signal TOP_LOC_RnW                :  std_logic;
	Signal TOP_LOC_CSn                :  std_logic;
	Signal TOP_DATABUS_ADD            :  std_logic_vector(12 DOWNTO 0);
	
	Signal TOP_TRIG_LED               :  std_logic;
	Signal TOP_ACCESS_LED             :  std_logic;

	-- Sim ready signals
	signal dcs_done,siu_done,ctp_done : std_logic := '0';

begin  -- TESTBENCH

UUT_TOP_RCB : entity sim_top_modules.TOP_RCB
	PORT MAP(
		TOP_fbD 		=> fbD,
--from SIU
  		TOP_fiDIR       => fiDIR,   
  		TOP_fiBENn      => fiBEN_N,
  		TOP_fiLFn 		=> fiLF_N,
--to SIU
   		TOP_foCLK 		=> siu_clk,
 		TOP_foBSYn 	    => foBSY_N,
  		TOP_fbCTRLn 	=> fbCTRL_N, 
  		TOP_fbTENn 	    => fbTEN_N,
--to L0 to Column                
  		TOP_SEG_TRIG 	=> TOP_SEG_TRIG,      
--to LTU             
  		TOP_BUSY 		=> busy, 
--from LTU 
  		TOP_L0_EXT_LVDS => L0_LVDS,
--to LEDs                                    
  		TOP_TRIG_LED 	=> TOP_TRIG_LED,               
  		TOP_ACCESS_LED  => TOP_ACCESS_LED,             
--to TTCrx
  		TOP_clk         => TTCLK,                                                                   
  		TOP_Channel_A   => Channel_A,
  		TOP_Channel_B   => Channel_B,
  		TOP_RCB_clk     => dcs_clk,
  		TOP_rst         => reset_n);

	-- Generate system clock, 40 MHz.
	U_CLK : process             
	begin
		TTCLK<='1';
		wait for 12 ns;
		TTCLK<='0';
		wait for 13 ns;
	end process;

	U_SIUCLK : process
	begin
		wait until rising_edge(siu_clk);
		wait for Tsiuclk_shift;
		foclk <= siu_clk; 
		wait until falling_edge(siu_clk);
		wait for Tsiuclk_shift;
		foclk <= siu_clk; 
	end process;

	U_RCUCLK : process
	begin
		wait until rising_edge(TTCLK);
		wait for Tdcsclk_shift;
		dcs_clk <= TTCLK;
		wait until falling_edge(TTCLK);
		wait for Tdcsclk_shift;
		dcs_clk <= TTCLK;
	end process;
	
	-- Generate reset signals, this reset is from DCS broad.
	U_RST : process
	begin
		-- wait for Tclk*10;
		reset_n<='1';
		wait for 1000 ns;
		reset_n<='0';
		wait for 25 ns;
		reset_n<='1';
		wait;
	end process;
	
	U_DCS: process
		file     input_file : text open READ_MODE is path_com;
		variable input_line : line;
		variable b_read_ok  : boolean := false;   
		variable command    : string (1 to 5);    
		variable gap_time   : time := 0 ns;
		variable current    : time := 0 ns;
		variable str_2	 	: string(1 to 2);	-- use to read 2 charactors from the line
		variable str_3	 	: string(1 to 3);	-- use to read 3 charactors from the line
		variable str_4 		: string(1 to 4);	-- use to read 4 charactors from the line
		variable str_8 		: string(1 to 8);	-- use to read 8 charactors from the line
		variable reg_addr 	: std_logic_vector (15 downto 0);	-- register address
		variable reg_data 	: std_logic_vector (31 downto 0);	-- data will be written to register
		variable dcs_active : integer := 1;		-- =1,dcs active; =0,dcs inactive
	begin  
	  	sim_title("Testbench for RCU",            
	         "Simulate from two parts : DAQ and CTP.");
	    while ( (not endfile(input_file)) and (dcs_active=1) ) loop        
			readline(input_file, input_line);     
		    read(input_line, str_2, b_read_ok);
			if str_2/="--" then
				read(input_line, str_3, b_read_ok);
				command(1 to 2):=str_2;
				command(3 to 5):=str_3;
			    case command is      
					when "TXGAP" =>                     
				        current := NOW;                   
				        read(input_line, gap_time, b_read_ok);
				        while gap_time > (NOW - current) loop
				          wait until rising_edge(dcs_clk);       
				        end loop;
					when "ENCTP" =>   				
					  	show("ENABLE CTP");               
				        wait until rising_edge(dcs_clk);      
						enctp<='1';                
						wait for Tclk;                                    
					when "ENSIU" =>   				
					  	show("ENABLE SIU");               
				        wait until rising_edge(dcs_clk);      
						ensiu<='1';                
						wait for Tclk;                                                    
				    when "-END-" =>
					  	show(" Run to the end of DCS script.");
						dcs_active:=0;
						dcs_done<='1';
				    when others =>
				        report "Wrong format data in script.txt" severity FAILURE;
			    end case;
			end if;
		end loop;
		wait;
	end process;

	U_SIU: process
		file     input_file : text open READ_MODE is path_com;
		variable input_line : line;
		variable b_read_ok  : boolean := false;
		variable command    : string (1 to 5);
		variable gap_time   : time := 0 ns;
		variable current    : time := 0 ns;
		variable str_2	 	: string(1 to 2);	-- use to read 2 charactors from the line
		variable str_3	 	: string(1 to 3);	-- use to read 3 charactors from the line
		variable str_4 		: string(1 to 4);	-- use to read 4 charactors from the line
		variable str_8 		: string(1 to 8);	-- use to read 8 charactors from the line
		variable str_8_1 		: string(1 to 8);	-- use to read 8 charactors from the line
		variable reg_addr 	: std_logic_vector (15 downto 0);	-- register address
		variable reg_data 	: std_logic_vector (31 downto 0);	-- data to fbD   
		variable siu_started: integer := 0;
		variable siu_active : integer := 1;		-- =1,siu active.		
		variable st_addr	: std_logic_vector(15 downto 0);
		variable add_data	: std_logic_vector(31 downto 0);
		variable file_in	: string (1 to 8);
		variable bl_length	: std_logic_vector(15 downto 0);
	begin  -- process tb_main    
		while siu_started<2 loop
			readline(input_file, input_line);        
		    read(input_line, str_2, b_read_ok);
			if str_2/="--" then
				read(input_line, str_3, b_read_ok);
				command(1 to 2):=str_2;
				command(3 to 5):=str_3;
				if command="** SI" then
					siu_started := 3;
				end if;
			end if;
			wait for 1 ns;
		end loop;
		wait until ensiu='1';
	    while ( (not endfile(input_file)) and (siu_active=1) )loop
			readline(input_file, input_line);
		    read(input_line, str_2, b_read_ok);
			if str_2/="--" then
				read(input_line, str_3, b_read_ok);
				command(1 to 2):=str_2;
				command(3 to 5):=str_3;
			    case command is
				    when "FCTRL" =>
				        show( "EXECUTING FECTRL COMMAND");
				        read(input_line, str_3, b_read_ok);
				        read(input_line, str_8, b_read_ok);
				        reg_data := str2slv(str_8);
						wait until rising_edge(foCLK);wait for Tdly;
							fiBEN_N<='0';
							fiDIR<='0';
							fiLF_N<='1';
							fbTEN_N<='1';
							fbCTRL_N<='1';
						wait for Tfoclk;
							fbd(31)<='0';
							fbd(30 downto 12)<=reg_data(18 downto 0);
							fbd(11 downto 0)<=X"0C4";
							fbTEN_N<='0';
							fbCTRL_N<='0';
						wait for Tfoclk;
							fbTEN_N<='1';
							fbCTRL_N<='1';
							fbd<=(B"0" & X"000" & reg_data(18 downto 0)); -- SIU send control command and waits for acnowledgment 
						wait for Tfoclk;			          
					when "FSTRD" =>
				        show( "EXECUTING FESTRD COMMAND");
				        read(input_line, str_3, b_read_ok);
				        read(input_line, str_8, b_read_ok);
				        reg_data := str2slv(str_8);
						wait until rising_edge(foCLK);wait for Tdly;
							fiBEN_N<='0';
							fiDIR<='0';
							fiLF_N<='1';
							fbTEN_N<='1';
							fbCTRL_N<='1';
						wait for Tfoclk;
							fbd(31)<='0';
							fbd(30 downto 12)<=reg_data(18 downto 0);
							fbd(11 downto 0)<=X"044";
							fbTEN_N<='0';
							fbCTRL_N<='0';
						wait for Tfoclk;
							fbTEN_N<='1';
							fbCTRL_N<='1';
						wait for Tfoclk;
							fiBEN_N<='1';
							fbTEN_N<='Z';
							fbCTRL_N<='Z';
							fbd<=(others=>'Z');
						wait for Tfoclk*2;
							fiDIR<='1';
						wait for Tfoclk*2;
							fiBEN_N<='0';
						wait for Tfoclk;
					when "RDYRX" =>
						wait until rising_edge(foCLK);wait for Tdly;
							fiBEN_N<='0';
							fiDIR<='0';
							fiLF_N<='1';
							fbTEN_N<='1';
							fbCTRL_N<='1';
						wait for Tfoclk;
							fbd<=X"00000014";
						wait for Tfoclk;
							fbTEN_N<='0';
							fbCTRL_N<='0';
						wait for Tfoclk;
							fbTEN_N<='1';
							fbCTRL_N<='1';
						wait for Tfoclk*3;
							fbTEN_N<='Z';
							fbCTRL_N<='Z';
							fbd<=(others=>'Z');
							fiBEN_N<='1';
						wait for Tfoclk*2;
							fiDIR<='1';
						wait for Tfoclk*2;
							fbTEN_N<='Z';
							fbCTRL_N<='Z';
							fiBEN_N<='0';
				    when "EOBTR" =>
						wait until rising_edge(foCLK);wait for Tdly;
							fiBEN_N<='0';
							fiDIR<='0';
							fiLF_N<='1';
							fbTEN_N<='1';
							fbCTRL_N<='1';
						wait for Tfoclk;
							fbd<=X"000000B4";
							fbTEN_N<='0';
							fbCTRL_N<='0';
						wait for Tfoclk;
							fbTEN_N<='1';
							fbCTRL_N<='1';
							fbd<=(others=>'Z');
						wait for Tfoclk*2;
					--when "STBWR" =>
				 --       read(input_line, str_3, b_read_ok);
				 --       read(input_line, str_8, b_read_ok);
				 --       read(input_line, str_3, b_read_ok);
				 --       read(input_line, str_8_1, b_read_ok);
				 --       add_data:=str2slv(str_8);
				 --       file_in:=str_8_1;
					--	siu_wr(add_data,fbCTRL_N,fbd,fbTEN_N,foclk,foBSY_N,fiBEN_N,fiDIR,fiLF_N,file_in);
				    when "TXGAP" =>
				        current := NOW;
				        read(input_line, gap_time, b_read_ok);
				        while gap_time > (NOW - current) loop
				          wait until rising_edge(foCLK);
				        end loop;
					when "-END-" =>
						show(" Run to the end of SIU script.");
						siu_active:=0;
						siu_done<='1';
					when others =>
				        report "Wrong format data in script.txt" severity FAILURE;
			    end case;
			end if;
		end loop;
		wait;
	end process;
	--------------------------------------------------------------------------------------------------------------------------	 
	
	-- Act as CTP, give serialBchannel and L1Accept to RCU
	U_CTP : process                                              
		file     input_file : text open READ_MODE is path_com;
		variable input_line : line;                                           
		variable b_read_ok  : boolean := false;   
		variable command    : string (1 to 5);    
		variable gap_time   : time := 0 ns;       
		variable current    : time := 0 ns;
		variable ctp_started: boolean := false;   
		variable ctp_active : boolean := true;  	
		variable str_2	 	: string(1 to 2);	-- use to read 2 charactors from the line
		variable str_3	 	: string(1 to 3);	-- use to read 3 charactors from the line
	begin 
		while ctp_started=false loop
			readline(input_file, input_line);        
		    read(input_line, str_2, b_read_ok);
			if str_2/="--" then
				read(input_line, str_3, b_read_ok);
				command(1 to 2):=str_2;
				command(3 to 5):=str_3;
				if command="** CT" then
					ctp_started := true;
				end if;
			end if;
			wait for 1 ns;
		end loop;	
		wait until enctp='1';
		ctp_active:=true;
		while ( (not endfile(input_file)) and (ctp_active=true) )loop        
			readline(input_file, input_line);    
		    read(input_line, str_2, b_read_ok);
			if str_2/="--" then
				read(input_line, str_3, b_read_ok);
				command(1 to 2):=str_2;
				command(3 to 5):=str_3;
			    case command is                       
				    when "L0L2a" =>                       
						show("Start send L0-L1-L2a sequence.");
						p_CTP(Channel_B,Channel_A,L0_LVDS,"L0L1L2a");
						show("Finish send L0-L1-L2a sequence.");
				    when "L0L2r" =>                       
						show("Start send L0-L1-L2r sequence.");
						p_CTP(Channel_B,Channel_A,L0_LVDS,"L0L2r");
						show("Finish send L0-L1-L2r sequence.");
				    when "L0L1r" =>                       
						show("Start send L0-L1r sequence.");
						p_CTP(Channel_B,Channel_A,L0_LVDS,"L0L1Lr");
						show("Finish send L0-L1r sequence.");
				    when "TXGAP" =>                     
				        current := NOW;                   
				        read(input_line, gap_time, b_read_ok);
				        while gap_time > (NOW - current) loop
				          wait until rising_edge(foCLK);       
				        end loop;                         
					when "-END-" =>
						show(" Run to the end of CTP script.");
						ctp_active:=false;
						ctp_done<='1';
					when others =>                      
				        report "Wrong format data in script.txt" severity FAILURE;
			    end case;    
			end if;		
		end loop; 		
	end process;

	-- Record command on SIU bus, write information to RCU log file.
	U_SIU_RECORD : process                    
		variable out_line 	: line;         
		variable out_line1 	: line;
		variable out_trailer 	: line;
		variable out_trailerA 	: line;
		variable out_trailerB 	: line;
		variable st_addr	: std_logic_vector(15 downto 0);	-- block transfer start address
		variable cur_addr	: std_logic_vector(15 downto 0);	-- block transfer current address
		variable bl_size	: std_logic_vector(15 downto 0);	-- block length
		variable cnt_i		: integer;	-- increace counter
		variable cnt_i_i		: integer;	-- increace counter
		variable CDH_cnt_i		: integer;	-- increace counter
		variable HMPID_cnt_i		: integer;	-- increace counter
		variable read_para  : integer:=0; -- read status/error/block_length
	begin
		wait until falling_edge(fbCTRL_N);wait for Tdly;
		if fbD(11 downto 0)=X"014" then			
			show("Detect RDYRX on SIU bus.");
			wait for Tfoclk*10;
			wait until fbTEN_N='0'; wait for 12 ns;
			cnt_i:=0;
			cnt_i_i:=0;
			CDH_cnt_i:=0;
			HMPID_cnt_i:=0;
			while cnt_i<4096 loop
				if fbTEN_N='0' and fbCTRL_N='1' then
					write(out_line,slv2str(fbd(31 downto 0)));
					writeline(f_dcssiudata,out_line);
					-- splitting the received date to new formate for ramp read.
					if fbd(31 downto 0) = X"FFFFFFFF" then
						write(out_line1,string'("CDH Start: "));
						write(out_line1,slv2str(fbd(31 downto 0)));
						write(out_line1,string'("                                                 32 Bit Word count:"));							
						write(out_line1,slv2str(conv_std_logic_vector(cnt_i,32)));
						write(out_line1, string'(";    At: " ));
						write(out_line1, now);             
						write(out_line1, string'("("));    
						write(out_line1, now/Tclk);        
						write(out_line1, string'(" clocks)."));
						writeline(f_tosiudata,out_line1);
						cur_addr:= cur_addr+1;
						cnt_i:=cnt_i+1;
						CDH_cnt_i:=CDH_cnt_i+1;
					elsif CDH_cnt_i < 10 then
						write(out_line1,string'("CDH: "));
						write(out_line1,slv2str(fbd(31 downto 0)));
						write(out_line1,string'("                                                 32 Bit Word count:"));							
						write(out_line1,slv2str(conv_std_logic_vector(cnt_i,32)));
						write(out_line1, string'(";    At: " ));
						write(out_line1, now);             
						write(out_line1, string'("("));    
						write(out_line1, now/Tclk);        
						write(out_line1, string'(" clocks)."));
						writeline(f_tosiudata,out_line1);
						cur_addr:= cur_addr+1;
						cnt_i:=cnt_i+1;
						CDH_cnt_i:=CDH_cnt_i+1;
					elsif HMPID_cnt_i < 5 and CDH_cnt_i >= 10 then
						write(out_line1,string'("HMPID: "));
						write(out_line1,slv2str(fbd(31 downto 0)));
						write(out_line1,string'("                                                 32 Bit Word count:"));							
						write(out_line1,slv2str(conv_std_logic_vector(cnt_i,32)));
						write(out_line1, string'(";    At: " ));
						write(out_line1, now);             
						write(out_line1, string'("("));    
						write(out_line1, now/Tclk);        
						write(out_line1, string'(" clocks)."));
						writeline(f_tosiudata,out_line1);
						cur_addr:= cur_addr+1;
						cnt_i:=cnt_i+1;
						HMPID_cnt_i:=HMPID_cnt_i+1;
					elsif HMPID_cnt_i >= 5 and CDH_cnt_i >= 10 then
-- Coloumn header fbd(31 downto 26) = "000000" and 
					if fbd(15 downto 0) = "0011011010101000"then 
						write(out_line1,string'("   Zero ON No of words in Col: "));
						write(out_line1,slv2str(fbd(25 downto 16)));
						write(out_line1,string'("Coloumn Header"));
						write(out_line1,string'("          32 Bit Word count:"));
						write(out_line1,slv2str(conv_std_logic_vector(cnt_i,32)));
						write(out_line1, string'(";    At: " ));
						write(out_line1, now);             
						write(out_line1, string'("("));    
						write(out_line1, now/Tclk);        
						write(out_line1, string'(" clocks)."));
						writeline(f_tosiudata,out_line1);
						cur_addr:= cur_addr+1;
						cnt_i:=cnt_i+1;
					elsif fbd(15 downto 0) = "0011001010101000"then 
						write(out_line1,string'("   Zero OFF No of words in Col: "));
						write(out_line1,slv2str(fbd(25 downto 16)));
						write(out_line1,string'("    Coloumn Header"));
						write(out_line1,string'("          32 Bit Word count:"));
						write(out_line1,slv2str(conv_std_logic_vector(cnt_i,32)));
						write(out_line1, string'(";    At: " ));
						write(out_line1, now);             
						write(out_line1, string'("("));    
						write(out_line1, now/Tclk);        
						write(out_line1, string'(" clocks)."));
						writeline(f_tosiudata,out_line1);
						cur_addr:= cur_addr+1;
						cnt_i:=cnt_i+1;
-- EOE DILOGIC MARKER
					elsif fbd(31 downto 27) = "00001" then 
						write(out_line1,string'("   Col Add: "));
						write(out_line1,slv2str(fbd(26 downto 22)));
						write(out_line1,string'("   Dilo Add : "));
						write(out_line1,slv2str(fbd(21 downto 18)));
						write(out_line1,string'("   No of words in Dilo: "));
						write(out_line1,slv2str(fbd(6 downto 0)));
						write(out_line1,string'("    EoE DILOGIG"));
						write(out_line1,string'("          32 Bit Word count:"));
						write(out_line1,slv2str(conv_std_logic_vector(cnt_i,32)));
						write(out_line1, string'(";    At: " ));
						write(out_line1, now);             
						write(out_line1, string'("("));    
						write(out_line1, now/Tclk);        
						write(out_line1, string'(" clocks)."));
						writeline(f_tosiudata,out_line1);
						cur_addr:= cur_addr+1;
						cnt_i:=cnt_i+1;
-- Segment marker
					elsif fbd(31 downto 20) = "101010110000" then 
						write(out_line1,string'("   No of words in Seg: "));
						write(out_line1,slv2str(fbd(19 downto 8)));
						write(out_line1,string'("   Seg Add : "));
						write(out_line1,slv2str(fbd(3 downto 0)));
						write(out_line1,string'("    Segment Marker"));
						write(out_line1,string'("          32 Bit Word count:"));
						write(out_line1,slv2str(conv_std_logic_vector(cnt_i,32)));
						write(out_line1, string'(";    At: " ));
						write(out_line1, now);             
						write(out_line1, string'("("));    
						write(out_line1, now/Tclk);        
						write(out_line1, string'(" clocks)."));
						writeline(f_tosiudata,out_line1);
						cur_addr:= cur_addr+1;
						cnt_i:=cnt_i+1;
					elsif fbd(31 downto 27) = "00000" then
						write(out_line1,string'("    Column : "));
						write(out_line1,slv2str(fbd(26 downto 22)));
						write(out_line1,string'("   DILO : "));
						write(out_line1,slv2str(fbd(21 downto 18)));
						write(out_line1,string'("   Ch: "));
						write(out_line1,slv2str(fbd(17 downto 12)));
						write(out_line1,string'("   PH : "));
						write(out_line1,slv2str(fbd(11 downto 0)));
						write(out_line1,string'("          DILO Ch count:"));
						write(out_line1,slv2str(conv_std_logic_vector(cnt_i_i,32)));
						write(out_line1, string'(";    At: " ));
						write(out_line1, now);             
						write(out_line1, string'("("));    
						write(out_line1, now/Tclk);        
						write(out_line1, string'(" clocks)."));
						writeline(f_tosiudata,out_line1);
						cur_addr:= cur_addr+1;
						cnt_i_i:=cnt_i_i+1;
					end if;
				end if;
				elsif fbTEN_N='0' and fbCTRL_N='0' then
					cnt_i:=2048;
				end if;
				wait for Tclk;
			end loop;
		elsif fbD(11 downto 0)=X"044" then
			show("Detect FECSTRD#address on SIU bus.");
			if fbD(28 downto 24)=X"0" then
				read_para:=0;
			elsif fbD(28 downto 24)=X"1" then
				read_para:=1;
			elsif fbD(28 downto 24)=X"2" then
				read_para:=2;
			else
				read_para:=10;
			end if;
			wait until falling_edge(fiBEN_N);
			wait for Tfoclk;
			write(out_line, string'("At    : " ));
			write(out_line, now);             
			write(out_line, string'("("));    
			write(out_line, now/Tclk);        
			write(out_line, string'(" clocks)."));
			if read_para=0 then
				write(out_line,string'("    SIU rd status register=0x"));
			elsif read_para=1 then
				write(out_line,string'("    SIU rd error register=0x"));
			elsif read_para=2 then
				write(out_line,string'("    SIU rd block length register=0x"));
			else				
				write(out_line,string'("    SIU rd bad address register."));
			end if;
			write(out_line,slv2str(fbd(31 downto 12)));
			writeline(rcu_logfile,out_line);					
		elsif fbD(11 downto 0)=X"0B4" then
			show("Detect EOBTR on SIU bus.");				
		elsif fbD(11 downto 0)=X"0C4" then
			show("Detect FECTRL on SIU bus : ");
			if fbD(30 downto 28)="000" then			
				if fbD(12)='1' then
					write(out_line,string'(" Local reset. "));
				end if;
				if fbD(13)='1' then
					write(out_line,string'(" Global reset. "));
				end if;
				if fbD(14)='1' then
					write(out_line,string'(" Reset RCU. "));
				end if;
				if fbD(16)='1' then
					write(out_line,string'(" Reset SIB. "));
				end if;
			elsif fbD(30 downto 28)="001" then
				write(out_line,string'(" Clear error register. "));
			elsif fbD(30 downto 28)="010" then
				write(out_line,string'(" Write block length register. "));
			else	
				write(out_line,string'(" Wrong FECTRL format. "));				
			end if;				
			writeline(rcu_logfile,out_line);			
		--else
			--show(string'("Wrong SIU command. fbD=0x")&slv2str(fbD));
		end if;
	end process;

	stop_simulation :process
		begin
			wait until (dcs_done = '1') and (siu_done = '1') and (ctp_done = '1');
			report "Simulation Ended" severity FAILURE;
		end process ; 

end a_test;
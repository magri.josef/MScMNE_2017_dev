-------------------------------------------------------------------------------
-- DESIGN RCU FIRM WARE V.2.0
-- FILE NAME   : tb_pack.vhd
-- DESIGN TEAM : Luciano Musa, Changzhou Xiang, Attiq ur Rehman
-- DESCRIPTION : RCU TESTBENCH PACKAGE. 
-------------------------------------------------------------------------------
library ieee; 
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE ieee.numeric_std.ALL; 
library std;
use std.textio.all;
library tb_modules;
use tb_modules.tb_values.all;

package tb_daq_pkg is

	---------------------------------------------------------------------------
	-- GIVE SIMULATION NAME AND DESCRIPTION
	-- copy from Johan.
	---------------------------------------------------------------------------
	procedure sim_title(
		constant name : in string;		-- name of current simulation
		constant desc : in string);		-- description of current simulation
	
	---------------------------------------------------------------------------
	-- SHOW TIME AND SHRING ON SCREEN AND rcu_logfile.
	---------------------------------------------------------------------------
	procedure show(
		constant str : in string);		-- information(simulation time and note) shown on screen and rcu_logfile

	---------------------------------------------------------------------------
	-- SHOW SHRING ONLY ON SCREEN AND rcu_logfile.
	---------------------------------------------------------------------------
    procedure showstr(
		constant str : in string);		-- information(note only) shown on screen and rcu_logfile

	---------------------------------------------------------------------------
	--  CONVERT HEX TO STD_LOGIC_VECTOR
	--	COPY FROM Csaba. SOOS
	---------------------------------------------------------------------------
	function str2slv (str : string) return std_logic_vector ;
	
	---------------------------------------------------------------------------
	--  CONVERT STD_LOGIC_VECTOR TO HEX
	--  THE WIDTH OF INPUT(SLV) SHOULD LARGER THAN 4
	---------------------------------------------------------------------------
	function slv2str (slv : std_logic_vector) return string ;

--	---------------------------------------------------------------------------
--	--  SIU INITIALIZE READOUT-LIST MEMORY
--	---------------------------------------------------------------------------
--		procedure siu_wr(
--		constant add_data	: in std_logic_vector(31 downto 0);
--		signal fbctrl_n		: inout std_logic;
--		signal fbd			: inout std_logic_vector(31 downto 0);
--		signal fbten_n		: inout std_logic;
--		signal foclk		: in std_logic;
--		signal foBSY_N		: in std_logic;
--		signal fiBEN_N		: out std_logic;
--		signal fiDIR		: out std_logic;
--		signal fiLF_N		: out std_logic;
--			   suffix       : in string);
end;
    ---------------------------------------------------------------------------
    -- package body
    ---------------------------------------------------------------------------
package body tb_daq_pkg is

	---------------------------------------------------------------------------
	-- GIVE SIMULATION NAME AND DESCRIPTION
	-- copy from Johan Alme.
	---------------------------------------------------------------------------
	procedure sim_title(
		constant name : in string;		-- name of current simulation
		constant desc : in string)		-- description of current simulation
	is
		variable l    : line;			-- show information on
		variable l2   : line;
	begin
		write(l, string'(" "));
		writeline (rcu_logfile, l);
		write(l, string'("********************************************************************************"));
		writeline (rcu_logfile, l);
		write(l, "Section     : " & name);
		writeline (rcu_logfile, l);
		write(l, "Description : " & desc);
		writeline (rcu_logfile, l);
		write(l, string'("Entered at  : " ));
		write(l, now);
		write(l, string'("(")); 
		write(l, now/Tclk); 
		write(l, string'(" clocks)."));
		writeline (rcu_logfile, l);
		write(l, string'("********************************************************************************"));
		writeline (rcu_logfile, l);
		
		write(l2, string'(" "));
		writeline (output, l2);
		write(l2, string'("****************************************"));
		writeline (output, l2);
		write(l2, "Section     : " & name);
		writeline (output, l2);
		write(l2, "Description : " & desc);
		writeline (output, l2);
		write(l2, string'("Entered at  : " ));
		write(l2, now);
		write(l2, string'("(")); 
		write(l2, now/Tclk); 
		write(l2, string'(" clocks)."));
		writeline (output, l2);
		write(l2, string'("****************************************"));
		writeline (output, l2);
	end sim_title;
	
	---------------------------------------------------------------------------
	-- SHOW TIME AND SHRING ON SCREEN AND rcu_logfile.
	---------------------------------------------------------------------------
	procedure show(
		constant str : in string)		-- information(simulation time and note) shown on screen and rcu_logfile
	is
		variable l : line;
		variable l2 : line;
	begin
		write(l, string'("At    : " ));
		write(l, now);
		write(l, string'("(")); 
		write(l, now/Tclk); 
		write(l, string'(" clocks).      "));
		write(l,str);
		writeline(rcu_logfile,l);
		
		write(l2, string'("At    : " ));
		write(l2, now);
		write(l2, string'("(")); 
		write(l2, now/Tclk); 
		write(l2, string'(" clocks).      "));
		write(l2,str);
		writeline(output,l2);
	end show;               

	---------------------------------------------------------------------------
	-- SHOW SHRING ONLY ON SCREEN AND rcu_logfile.
	---------------------------------------------------------------------------
	procedure showstr(
		constant str : in string)		-- information(note only) shown on screen and rcu_logfile
	is
		variable l : line;
		variable l2 : line;
	begin
		write(l,str);
		writeline(rcu_logfile,l);

		write(l2,str);
		writeline(output,l2);
	end showstr;
	
	---------------------------------------------------------------------------
	--  CONVERT HEX TO STD_LOGIC_VECTOR
	--	COPY FROM Csaba. SOOS
	---------------------------------------------------------------------------
	function str2slv (str : string) return std_logic_vector is
      subtype nib is std_logic_vector ( 3 downto 0);
      type    nib_table is array (0 to 15) of nib;
      constant nib_tbl : nib_table := ("0000", "0001", "0010", "0011",
                                       "0100", "0101", "0110", "0111",
                                       "1000", "1001", "1010", "1011",
                                       "1100", "1101", "1110", "1111");
      variable slv    : std_logic_vector ((str'length*4)-1 downto 0);
      variable pos1   : integer;
      variable pos_0  : integer;
      variable pos_9  : integer;
      variable pos_A  : integer;
      variable pos_F  : integer;
      variable pos_sa : integer;
      variable pos_sf : integer;
      variable b_good_number : boolean;
      variable b_good_letter1 : boolean;
      variable b_good_letter2 : boolean;
      variable b_good_char : boolean;
    begin  -- str2slv
      slv := (others => '0');
      for i in str'range loop
        pos1   := character'pos(str(i));
        pos_0  := character'pos('0');
        pos_9  := character'pos('9');
        pos_A  := character'pos('A');
        pos_F  := character'pos('F');
        pos_sa := character'pos('a');
        pos_sf := character'pos('f');
        b_good_number  := (pos1 >= pos_0  and pos1 <= pos_9 );
        b_good_letter1 := (pos1 >= pos_A  and pos1 <= pos_F );
        b_good_letter2 := (pos1 >= pos_sa and pos1 <= pos_sf);
        b_good_char := b_good_number or b_good_letter1 or b_good_letter2;
        assert b_good_char report "WRONG NUMBER" severity FAILURE;
        if b_good_number then
          pos1 := pos1 - pos_0;
        elsif b_good_letter1 then
          pos1 := pos1 - pos_A + 10;
        elsif b_good_letter2 then
          pos1 := pos1 - pos_sa + 10;
        end if;
        slv(((str'length*4) - (i-1)*4 - 1) downto
            ((str'length*4) - (i-1)*4 - 4)) := nib_tbl(pos1);
      end loop;  -- i
      return slv;
    end str2slv;
	
	---------------------------------------------------------------------------
	--  CONVERT STD_LOGIC_VECTOR TO HEX
	--  THE WIDTH OF INPUT(SLV) SHOULD LARGER THAN 4
	---------------------------------------------------------------------------
	function slv2str (slv : std_logic_vector) return string is
		variable str		: string(1 to (slv'length+3)/4);
		variable slv_times4 : std_logic_vector(((slv'length+3))/4*4-1 downto 0);
		variable str_1		: character;
		variable slv_4		: std_logic_vector(3 downto 0);
		variable str_length : integer;
		variable need_add_bits : integer;
	begin	
		str_length:=(slv'length+3)/4;
		need_add_bits:=str_length*4-slv'length;
		if need_add_bits=0 then
			slv_times4:=slv;
		elsif need_add_bits=1 then
			slv_times4:='0'&slv;
		elsif need_add_bits=2 then
			slv_times4:="00"&slv;
		else
			slv_times4:="000"&slv;
		end if;
		for i in 1 to str_length loop
			slv_4:=slv_times4(slv_times4'length-i*4+3 downto slv_times4'length-i*4);
			case slv_4 is
				when "0000" =>  str_1:='0';
				when X"1" =>  str_1:='1';
				when X"2" =>  str_1:='2';
				when X"3" =>  str_1:='3';
				when X"4" =>  str_1:='4';
				when X"5" =>  str_1:='5';
				when X"6" =>  str_1:='6';
				when X"7" =>  str_1:='7';
				when X"8" =>  str_1:='8';
				when X"9" =>  str_1:='9';
				when X"a" =>  str_1:='A';
				when X"b" =>  str_1:='B';
				when X"c" =>  str_1:='C';
				when X"d" =>  str_1:='D';
				when X"e" =>  str_1:='E';
				when X"f" =>  str_1:='F';
				when others => str_1:='?';
			end case;
			str(i):=str_1;
		end loop;
		return str;
	end slv2str;
	
	-----------------------------------------------------------------------------
	----  SIU READOUT- MEMORY
	-----------------------------------------------------------------------------
	--procedure siu_wr(
	--	constant add_data	: in std_logic_vector(31 downto 0);
	--	signal fbctrl_n		: inout std_logic;
	--	signal fbd			: inout std_logic_vector(31 downto 0);
	--	signal fbten_n		: inout std_logic;	
	--	signal foclk		: in std_logic;	
	--	signal foBSY_N		: in std_logic;
	--	signal fiBEN_N		: out std_logic;
	--	signal fiDIR		: out std_logic;
	--	signal fiLF_N		: out std_logic;
 --              suffix       : in string)
	--is		
	--	file in_file  : text open READ_MODE is path_in_daq & suffix & ".dat";
	--	file in_file1 : text open READ_MODE is path_in_daq & suffix & ".dat";
	--	variable str_8		: string(1 to 8);
	--	variable str_2	 	: string(1 to 2);	-- use to read 2 charactors from the line
	--	variable str_3	 	: string(1 to 3);	-- use to read 3 charactors from the line
	--	variable din_hex    : std_logic_vector (31 downto 0);
	--	variable command    : string (1 to 8) := "00000000";
	--	variable bl_size    : integer := 0;
	--	variable in_line 	: line;
	--	variable in_line1 	: line;
	--	variable b_read_ok  : boolean := false;
	--begin
	--	bl_size:=0;
	--	show(string'("SIU write INSTRUCTION MEMORY from:")&suffix);
	--	wait until foclk='0';wait until foclk='1';
	--		fiBEN_N<='0';
	--		fiDIR<='0';
	--		fiLF_N<='1';
	--		fbTEN_N<='1';
	--		fbCTRL_N<='1';
	--	wait for Tfoclk;
	--		fbD(31 downto 28)<=X"0";
	--		fbd(27 downto 8)<=add_data(19 downto 0);
	--		fbd(7 downto 0)<=X"d4";
	--		fbTEN_N<='0';
	--		fbCTRL_N<='0';
	--	wait for Tfoclk;
	--		fbten_n<='1';
	--		fbctrl_n<='1';
	--	wait for Tfoclk*4;
	--		fbten_n<='0';
	--		fbctrl_n<='1';		
	--	while not endfile(in_file) loop
	--		if foBSY_N='0' then
	--			fbTEN_N<='1';
	--			wait for Tfoclk; 
	--			fbTEN_N<='0';
	--		else
	--			readline(in_file1,in_line1);			
	--			writeline(rcu_logfile,in_line1);
	--			readline(in_file,in_line);             
	--			read(in_line, str_2);
	--			read(in_line, str_3);
	--			command(4 to 5):=str_2;
	--			command(6 to 8):=str_3;
	--			din_hex := str2slv(command);
	--			fbd<=din_hex;
	--			wait for Tfoclk; 					
	--			bl_size:=bl_size+1;
	--		end if;
	--	end loop;		
	--		fbten_n<='1';
	--		fbctrl_n<='1';
	--	wait for Tfoclk*4;
	--		fbd<=X"000000b4";
	--		fbten_n<='0';
	--		fbctrl_n<='0';
	--	wait for Tfoclk;
	--		fbd<=(others=>'Z');
	--		fbten_n<='1';
	--		fbctrl_n<='1';		
	--	wait for Tfoclk*4;
	--		if foBSY_N='0' then
	--			wait until foBSY_N='1';
	--		end if;
	--	wait for Tfoclk*10;	
	--end siu_wr;
	
END PACKAGE BODY tb_daq_pkg;

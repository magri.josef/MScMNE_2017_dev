-------------------------------------------------------------------------------
-- DESIGN RCU FIRM WARE V.2.0
-- FILE NAME   : tb_pack.vhd
-- DESIGN TEAM : Luciano Musa, Changzhou Xiang, Attiq ur Rehman
-- DESCRIPTION : RCU TESTBENCH PACKAGE. 
-------------------------------------------------------------------------------

library ieee; 
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE ieee.numeric_std.ALL; 
library std;
use std.textio.all;

package tb_values is
	---------------------------------------------------------------------------
	-- constants
	---------------------------------------------------------------------------
	constant Tclk		: time :=  25000 ps; -- system clock period
	constant Tfoclk		: time :=  25000 ps; -- clock period of foclk of SIU interface
	constant Tdly		: time :=  3000 ps; -- signal delay between pin to block input

	constant path_out    : string := "../../../../src/sim/test_bench/file_IO/OUTPUT/";
	--constant path_in_daq : string := "../../../../src/sim/test_bench/file_IO/INPUT/DAQ/";
	constant path_in_ctp : string := "../../../../src/sim/test_bench/file_IO/INPUT/CTP/";
	constant path_in_com : string := "../../../../src/sim/test_bench/file_IO/COMMAND/";

	file rcu_logfile   : text open WRITE_MODE is path_out & "sim_result.log";		-- logfile of simulate result
	file f_dcssiudata  : text open WRITE_MODE is path_out & "dcs_siu_data.txt";	-- data write to DCS/SIU bus and read from DCS/SIU bus 
	file f_tosiudata   : text open WRITE_MODE is path_out & "data_to_siu.txt";	-- data write to DCS/SIU bus and read from DCS/SIU bus 

	---------------------------------------------------------------------------
	-- constants set by TCL script time_sim_type.tcl
	-- NB: Please do not edit manually !!
	---------------------------------------------------------------------------

    constant SYNTHESIS_TIME_STAMP : std_logic_vector( 47 downto 0 ) := X"17_09_01_16_02_31";
    constant path_com : string := path_in_com & "RCB_1_seg_1_col_no_DILO5.txt";
    -- SYNTHESIS_TIME_STAMP was modified by time_sim_type.tcl on 2017-Sep-01 at 16:02:31.
    -- The constant has the form YY_MM_DD_HH_mm_ss.

	---------------------------------------------------------------------------

END PACKAGE tb_values;

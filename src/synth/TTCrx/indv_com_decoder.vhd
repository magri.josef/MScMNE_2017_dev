-------------------------------------------------------------------------------
-- Title      : Individual Command Decoder
-- Project    : TTCrx Model
-------------------------------------------------------------------------------
-- Description: Taking the recieved individual command in paralell and checking
--              if its intended for this chip and sending it to the registers
--              and the data interface.
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY indv_com_decoder IS
  PORT (reset     : IN std_logic;        -- active high
        indvStrb  : IN std_logic;        -- active high
        indv_com  : IN std_logic_vector(31 DOWNTO 0); 
        data      : OUT std_logic_vector(7 DOWNTO 0);
        subAddr   : OUT std_logic_vector(7 DOWNTO 0);
        intStrb   : OUT std_logic;
        extStrb   : OUT std_logic;
        reset_com : OUT std_logic 
        );
END indv_com_decoder;

ARCHITECTURE arc OF indv_com_decoder IS
  ALIAS in_subaddr: std_logic_vector(7 downto 0) IS indv_com(15 DOWNTO 8);
  ALIAS in_data: std_logic_vector(7 downto 0) IS indv_com(7 DOWNTO 0);
  ALIAS E:std_logic IS indv_com(17);
  -- TTCrxADDR 14bit | E | I | SubAddr 8 bit | Data 8 bit 
  --bit 16 is I ;
  
BEGIN
-------------------------------------------------------------------------------
  comdec: PROCESS(indvStrb,reset)
-------------------------------------------------------------------------------
  BEGIN
    IF reset = '1' THEN
      data <= "00000000";
      subAddr <= "00000000";
      intStrb <= '0';
      extStrb <= '0';
      reset_com <= '0';
    ELSIF indvStrb = '1' THEN
        IF E = '1' THEN
          extStrb <= '1';
        ELSIF E = '0' THEN
          intStrb <= '1';
        END IF;
        data <= in_data;
        subAddr <= in_subaddr;
        IF in_subaddr = "00000110" THEN  -- a reset command
          reset_com <= '1';
        END IF;
      END IF;
  END PROCESS comdec;
END arc;  
    
          
  





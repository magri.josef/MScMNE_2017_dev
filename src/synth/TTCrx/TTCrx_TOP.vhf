--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : TTCrx_TOP.vhf
-- /___/   /\     Timestamp : 09/01/2017 15:15:07
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl /home/josef/Documents/MScMNE_2017_dev/src/synth/TTCrx/TTCrx_TOP.vhf -w /home/josef/Documents/MScMNE_2017_dev/src/synth/TTCrx/TTCrx_TOP.sch
--Design Name: TTCrx_TOP
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity TTCrx_TOP is
   port ( Channel_A : in    std_logic; 
          Channel_B : in    std_logic; 
          clk       : in    std_logic; 
          reset_b   : in    std_logic; 
          BCnt      : out   std_logic_vector (11 downto 0); 
          BCntStrb  : out   std_logic; 
          Dout      : out   std_logic_vector (7 downto 0); 
          DoutStrb  : out   std_logic; 
          DQ        : out   std_logic_vector (3 downto 0); 
          L1Accept  : out   std_logic; 
          SubAddr   : out   std_logic_vector (7 downto 0); 
          TTCReady  : out   std_logic);
end TTCrx_TOP;

architecture BEHAVIORAL of TTCrx_TOP is
   attribute BOX_TYPE   : string ;
   signal BrcstCom_i     : std_logic_vector (7 downto 0);
   signal bunchcount_i   : std_logic_vector (11 downto 0);
   signal coarse_delay_i : std_logic_vector (7 downto 0);
   signal control_i      : std_logic_vector (7 downto 0);
   signal eventcount_i   : std_logic_vector (23 downto 0);
   signal extstrb_i      : std_logic;
   signal fine_delay1_i  : std_logic_vector (7 downto 0);
   signal fine_delay2_i  : std_logic_vector (7 downto 0);
   signal indvStrb_i     : std_logic;
   signal indv_com_i     : std_logic_vector (31 downto 0);
   signal indv_data_i    : std_logic_vector (7 downto 0);
   signal indv_subAddr_i : std_logic_vector (15 downto 8);
   signal intstrb_i      : std_logic;
   signal rst_com_i      : std_logic;
   signal rst_i          : std_logic;
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component L1_output
      port ( clk         : in    std_logic; 
             reset       : in    std_logic; 
             L1          : in    std_logic; 
             L1Accept    : out   std_logic; 
             Coarsedelay : in    std_logic_vector (3 downto 0));
   end component;
   
   component counter_interface
      port ( L1Accept   : in    std_logic; 
             clk        : in    std_logic; 
             reset      : in    std_logic; 
             EventCount : in    std_logic_vector (23 downto 0); 
             BunchCount : in    std_logic_vector (11 downto 0); 
             CntrReg    : in    std_logic_vector (1 downto 0); 
             BCntStrb   : out   std_logic; 
             BCnt       : out   std_logic_vector (11 downto 0));
   end component;
   
   component registers
      port ( clk            : in    std_logic; 
             fine_delay1_o  : out   std_logic_vector (7 downto 0); 
             fine_delay2_o  : out   std_logic_vector (7 downto 0); 
             coarse_delay_o : out   std_logic_vector (7 downto 0); 
             control_o      : out   std_logic_vector (7 downto 0); 
             chip_reset     : in    std_logic; 
             indv_int_strb  : in    std_logic; 
             indv_ext_strb  : in    std_logic; 
             indv_data      : in    std_logic_vector (7 downto 0); 
             indv_subAddr   : in    std_logic_vector (7 downto 0));
   end component;
   
   component reset_cord
      port ( reset_b    : in    std_logic; 
             reset_com  : in    std_logic; 
             chip_reset : out   std_logic; 
             clk        : in    std_logic);
   end component;
   
   component counters
      port ( chip_reset  : in    std_logic; 
             clk         : in    std_logic; 
             L1Accept    : in    std_logic; 
             brdcastbits : in    std_logic_vector (1 downto 0); 
             eventcount  : inout std_logic_vector (23 downto 0); 
             bunchcount  : out   std_logic_vector (11 downto 0));
   end component;
   
   component channelB_converter
      port ( clk        : in    std_logic; 
             IndvStrb   : out   std_logic; 
             reset      : in    std_logic; 
             serial_in  : in    std_logic; 
             Brdcst_com : out   std_logic_vector (7 downto 0); 
             Indv_com   : out   std_logic_vector (31 downto 0));
   end component;
   
   component indv_com_decoder
      port ( reset     : in    std_logic; 
             indvStrb  : in    std_logic; 
             indv_com  : in    std_logic_vector (31 downto 0); 
             intStrb   : out   std_logic; 
             extStrb   : out   std_logic; 
             reset_com : out   std_logic; 
             data      : out   std_logic_vector (7 downto 0); 
             subAddr   : out   std_logic_vector (7 downto 0));
   end component;
   
   component data_interface
      port ( Ext_DataStr : in    std_logic; 
             Int_Datastr : in    std_logic; 
             chip_reset  : in    std_logic; 
             clk         : in    std_logic; 
             FineDReg1   : in    std_logic_vector (7 downto 0); 
             FineDReg2   : in    std_logic_vector (7 downto 0); 
             CoarseDReg  : in    std_logic_vector (7 downto 0); 
             DoutStrb    : out   std_logic; 
             Dout        : out   std_logic_vector (7 downto 0); 
             SubAddr     : out   std_logic_vector (7 downto 0); 
             DQ          : out   std_logic_vector (3 downto 0); 
             Indv_data   : in    std_logic_vector (15 downto 0); 
             ControlReg  : in    std_logic_vector (7 downto 0));
   end component;
   
begin
   XLXI_194 : VCC
      port map (P=>TTCReady);
   
   XLXI_195 : L1_output
      port map (clk=>clk,
                Coarsedelay(3 downto 0)=>coarse_delay_i(3 downto 0),
                L1=>Channel_A,
                reset=>rst_i,
                L1Accept=>L1Accept);
   
   XLXI_198 : counter_interface
      port map (BunchCount(11 downto 0)=>bunchcount_i(11 downto 0),
                clk=>clk,
                CntrReg(1 downto 0)=>control_i(1 downto 0),
                EventCount(23 downto 0)=>eventcount_i(23 downto 0),
                L1Accept=>Channel_A,
                reset=>rst_i,
                BCnt(11 downto 0)=>BCnt(11 downto 0),
                BCntStrb=>BCntStrb);
   
   XLXI_202 : registers
      port map (chip_reset=>rst_i,
                clk=>clk,
                indv_data(7 downto 0)=>indv_data_i(7 downto 0),
                indv_ext_strb=>extstrb_i,
                indv_int_strb=>intstrb_i,
                indv_subAddr(7 downto 0)=>indv_subAddr_i(15 downto 8),
                coarse_delay_o(7 downto 0)=>coarse_delay_i(7 downto 0),
                control_o(7 downto 0)=>control_i(7 downto 0),
                fine_delay1_o(7 downto 0)=>fine_delay1_i(7 downto 0),
                fine_delay2_o(7 downto 0)=>fine_delay2_i(7 downto 0));
   
   XLXI_203 : reset_cord
      port map (clk=>clk,
                reset_b=>reset_b,
                reset_com=>rst_com_i,
                chip_reset=>rst_i);
   
   XLXI_204 : counters
      port map (brdcastbits(1 downto 0)=>BrcstCom_i(1 downto 0),
                chip_reset=>rst_i,
                clk=>clk,
                L1Accept=>Channel_A,
                bunchcount(11 downto 0)=>bunchcount_i(11 downto 0),
                eventcount(23 downto 0)=>eventcount_i(23 downto 0));
   
   XLXI_225 : channelB_converter
      port map (clk=>clk,
                reset=>rst_i,
                serial_in=>Channel_B,
                Brdcst_com(7 downto 0)=>BrcstCom_i(7 downto 0),
                IndvStrb=>indvStrb_i,
                Indv_com(31 downto 0)=>indv_com_i(31 downto 0));
   
   XLXI_226 : indv_com_decoder
      port map (indvStrb=>indvStrb_i,
                indv_com(31 downto 0)=>indv_com_i(31 downto 0),
                reset=>rst_i,
                data(7 downto 0)=>indv_data_i(7 downto 0),
                extStrb=>extstrb_i,
                intStrb=>intstrb_i,
                reset_com=>rst_com_i,
                subAddr(7 downto 0)=>indv_subAddr_i(15 downto 8));
   
   XLXI_231 : data_interface
      port map (chip_reset=>rst_i,
                clk=>clk,
                CoarseDReg(7 downto 0)=>coarse_delay_i(7 downto 0),
                ControlReg(7 downto 0)=>control_i(7 downto 0),
                Ext_DataStr=>extstrb_i,
                FineDReg1(7 downto 0)=>fine_delay1_i(7 downto 0),
                FineDReg2(7 downto 0)=>fine_delay2_i(7 downto 0),
                Indv_data(15 downto 8)=>indv_subAddr_i(15 downto 8),
                Indv_data(7 downto 0)=>indv_data_i(7 downto 0),
                Int_Datastr=>intstrb_i,
                Dout(7 downto 0)=>Dout(7 downto 0),
                DoutStrb=>DoutStrb,
                DQ(3 downto 0)=>DQ(3 downto 0),
                SubAddr(7 downto 0)=>SubAddr(7 downto 0));
   
end BEHAVIORAL;



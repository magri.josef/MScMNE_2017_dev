<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Channel_B" />
        <signal name="clk" />
        <signal name="Channel_A" />
        <signal name="rst_i" />
        <signal name="rst_com_i" />
        <signal name="indvStrb_i" />
        <signal name="indv_subAddr_i(15:8)" />
        <signal name="indv_data_i(7:0)" />
        <signal name="control_i(7:0)" />
        <signal name="coarse_delay_i(7:0)" />
        <signal name="fine_delay1_i(7:0)" />
        <signal name="fine_delay2_i(7:0)" />
        <signal name="extstrb_i" />
        <signal name="intstrb_i" />
        <signal name="BrcstCom_i(1:0)" />
        <signal name="eventcount_i(23:0)" />
        <signal name="bunchcount_i(11:0)" />
        <signal name="BCnt(11:0)" />
        <signal name="BCntStrb" />
        <signal name="control_i(1:0)" />
        <signal name="L1Accept" />
        <signal name="coarse_delay_i(3:0)" />
        <signal name="reset_b" />
        <signal name="indv_com_i(31:0)" />
        <signal name="BrcstCom_i(7:0)" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="indv_subAddr_i(15:8),indv_data_i(7:0)" />
        <signal name="DoutStrb" />
        <signal name="DQ(3:0)" />
        <signal name="Dout(7:0)" />
        <signal name="SubAddr(7:0)" />
        <signal name="XLXN_31" />
        <signal name="TTCReady" />
        <port polarity="Input" name="Channel_B" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="Channel_A" />
        <port polarity="Output" name="BCnt(11:0)" />
        <port polarity="Output" name="BCntStrb" />
        <port polarity="Output" name="L1Accept" />
        <port polarity="Input" name="reset_b" />
        <port polarity="Output" name="DoutStrb" />
        <port polarity="Output" name="DQ(3:0)" />
        <port polarity="Output" name="Dout(7:0)" />
        <port polarity="Output" name="SubAddr(7:0)" />
        <port polarity="Output" name="TTCReady" />
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="L1_output">
            <timestamp>2017-8-29T16:33:1</timestamp>
            <rect width="364" x="64" y="-320" height="264" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="496" y1="-288" y2="-288" x1="428" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="counter_interface">
            <timestamp>2017-8-29T16:38:19</timestamp>
            <rect width="320" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="448" y1="-352" y2="-352" x1="384" />
            <rect width="64" x="384" y="-300" height="24" />
            <line x2="448" y1="-288" y2="-288" x1="384" />
        </blockdef>
        <blockdef name="registers">
            <timestamp>2017-8-29T17:24:5</timestamp>
            <rect width="416" x="64" y="-640" height="392" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <rect width="64" x="480" y="-620" height="24" />
            <line x2="544" y1="-608" y2="-608" x1="480" />
            <rect width="64" x="480" y="-556" height="24" />
            <line x2="544" y1="-544" y2="-544" x1="480" />
            <rect width="64" x="480" y="-492" height="24" />
            <line x2="544" y1="-480" y2="-480" x1="480" />
            <rect width="64" x="480" y="-428" height="24" />
            <line x2="544" y1="-416" y2="-416" x1="480" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
        </blockdef>
        <blockdef name="reset_cord">
            <timestamp>2017-8-29T17:34:7</timestamp>
            <rect width="256" x="64" y="-256" height="184" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="counters">
            <timestamp>2017-8-29T17:45:14</timestamp>
            <rect width="352" x="64" y="-512" height="248" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="416" y="-492" height="24" />
            <line x2="480" y1="-480" y2="-480" x1="416" />
            <rect width="64" x="416" y="-428" height="24" />
            <line x2="480" y1="-416" y2="-416" x1="416" />
        </blockdef>
        <blockdef name="channelB_converter">
            <timestamp>2017-8-29T18:7:58</timestamp>
            <rect width="256" x="64" y="-256" height="184" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="0" y1="-176" y2="-176" x1="64" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <blockdef name="indv_com_decoder">
            <timestamp>2017-8-29T18:19:12</timestamp>
            <rect width="304" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-208" y2="-208" x1="64" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="432" y1="-288" y2="-288" x1="368" />
            <line x2="432" y1="-224" y2="-224" x1="368" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
            <rect width="64" x="368" y="-108" height="24" />
            <line x2="432" y1="-96" y2="-96" x1="368" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="data_interface">
            <timestamp>2017-9-1T9:22:1</timestamp>
            <rect width="336" x="64" y="-576" height="576" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="464" y1="-544" y2="-544" x1="400" />
            <rect width="64" x="400" y="-396" height="24" />
            <line x2="464" y1="-384" y2="-384" x1="400" />
            <rect width="64" x="400" y="-236" height="24" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
            <rect width="64" x="400" y="-76" height="24" />
            <line x2="464" y1="-64" y2="-64" x1="400" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
        </blockdef>
        <block symbolname="registers" name="XLXI_202">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="fine_delay1_i(7:0)" name="fine_delay1_o(7:0)" />
            <blockpin signalname="fine_delay2_i(7:0)" name="fine_delay2_o(7:0)" />
            <blockpin signalname="coarse_delay_i(7:0)" name="coarse_delay_o(7:0)" />
            <blockpin signalname="control_i(7:0)" name="control_o(7:0)" />
            <blockpin signalname="rst_i" name="chip_reset" />
            <blockpin signalname="intstrb_i" name="indv_int_strb" />
            <blockpin signalname="extstrb_i" name="indv_ext_strb" />
            <blockpin signalname="indv_data_i(7:0)" name="indv_data(7:0)" />
            <blockpin signalname="indv_subAddr_i(15:8)" name="indv_subAddr(7:0)" />
        </block>
        <block symbolname="counters" name="XLXI_204">
            <blockpin signalname="rst_i" name="chip_reset" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="Channel_A" name="L1Accept" />
            <blockpin signalname="BrcstCom_i(1:0)" name="brdcastbits(1:0)" />
            <blockpin signalname="eventcount_i(23:0)" name="eventcount(23:0)" />
            <blockpin signalname="bunchcount_i(11:0)" name="bunchcount(11:0)" />
        </block>
        <block symbolname="counter_interface" name="XLXI_198">
            <blockpin signalname="Channel_A" name="L1Accept" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="rst_i" name="reset" />
            <blockpin signalname="eventcount_i(23:0)" name="EventCount(23:0)" />
            <blockpin signalname="bunchcount_i(11:0)" name="BunchCount(11:0)" />
            <blockpin signalname="control_i(1:0)" name="CntrReg(1:0)" />
            <blockpin signalname="BCntStrb" name="BCntStrb" />
            <blockpin signalname="BCnt(11:0)" name="BCnt(11:0)" />
        </block>
        <block symbolname="L1_output" name="XLXI_195">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="rst_i" name="reset" />
            <blockpin signalname="Channel_A" name="L1" />
            <blockpin signalname="L1Accept" name="L1Accept" />
            <blockpin signalname="coarse_delay_i(3:0)" name="Coarsedelay(3:0)" />
        </block>
        <block symbolname="reset_cord" name="XLXI_203">
            <blockpin signalname="reset_b" name="reset_b" />
            <blockpin signalname="rst_com_i" name="reset_com" />
            <blockpin signalname="rst_i" name="chip_reset" />
            <blockpin signalname="clk" name="clk" />
        </block>
        <block symbolname="channelB_converter" name="XLXI_225">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="indvStrb_i" name="IndvStrb" />
            <blockpin signalname="rst_i" name="reset" />
            <blockpin signalname="Channel_B" name="serial_in" />
            <blockpin signalname="BrcstCom_i(7:0)" name="Brdcst_com(7:0)" />
            <blockpin signalname="indv_com_i(31:0)" name="Indv_com(31:0)" />
        </block>
        <block symbolname="indv_com_decoder" name="XLXI_226">
            <blockpin signalname="rst_i" name="reset" />
            <blockpin signalname="indvStrb_i" name="indvStrb" />
            <blockpin signalname="indv_com_i(31:0)" name="indv_com(31:0)" />
            <blockpin signalname="intstrb_i" name="intStrb" />
            <blockpin signalname="extstrb_i" name="extStrb" />
            <blockpin signalname="rst_com_i" name="reset_com" />
            <blockpin signalname="indv_data_i(7:0)" name="data(7:0)" />
            <blockpin signalname="indv_subAddr_i(15:8)" name="subAddr(7:0)" />
        </block>
        <block symbolname="data_interface" name="XLXI_231">
            <blockpin signalname="extstrb_i" name="Ext_DataStr" />
            <blockpin signalname="intstrb_i" name="Int_Datastr" />
            <blockpin signalname="rst_i" name="chip_reset" />
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="fine_delay1_i(7:0)" name="FineDReg1(7:0)" />
            <blockpin signalname="fine_delay2_i(7:0)" name="FineDReg2(7:0)" />
            <blockpin signalname="coarse_delay_i(7:0)" name="CoarseDReg(7:0)" />
            <blockpin signalname="DoutStrb" name="DoutStrb" />
            <blockpin signalname="Dout(7:0)" name="Dout(7:0)" />
            <blockpin signalname="SubAddr(7:0)" name="SubAddr(7:0)" />
            <blockpin signalname="DQ(3:0)" name="DQ(3:0)" />
            <blockpin signalname="indv_subAddr_i(15:8),indv_data_i(7:0)" name="Indv_data(15:0)" />
            <blockpin signalname="control_i(7:0)" name="ControlReg(7:0)" />
        </block>
        <block symbolname="vcc" name="XLXI_194">
            <blockpin signalname="TTCReady" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5382" height="3801">
        <attr value="CM" name="LengthUnitName" />
        <attr value="4" name="GridsPerUnit" />
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="848" y="2464" type="branch" />
            <wire x2="896" y1="2464" y2="2464" x1="848" />
        </branch>
        <branch name="rst_i">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1680" y="2368" type="branch" />
            <wire x2="1712" y1="2368" y2="2368" x1="1680" />
        </branch>
        <branch name="indvStrb_i">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1507" y="2464" type="branch" />
            <wire x2="1696" y1="2464" y2="2464" x1="1280" />
            <wire x2="1712" y1="2448" y2="2448" x1="1696" />
            <wire x2="1696" y1="2448" y2="2464" x1="1696" />
        </branch>
        <branch name="rst_com_i">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="2496" type="branch" />
            <wire x2="2384" y1="2496" y2="2496" x1="2144" />
        </branch>
        <branch name="indv_subAddr_i(15:8)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="2624" type="branch" />
            <wire x2="2384" y1="2624" y2="2624" x1="2144" />
        </branch>
        <branch name="indv_data_i(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="2560" type="branch" />
            <wire x2="2384" y1="2560" y2="2560" x1="2144" />
        </branch>
        <branch name="indv_data_i(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2912" y="2656" type="branch" />
            <wire x2="2944" y1="2656" y2="2656" x1="2912" />
        </branch>
        <branch name="indv_subAddr_i(15:8)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2912" y="2720" type="branch" />
            <wire x2="2944" y1="2720" y2="2720" x1="2912" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="2400" type="branch" />
            <wire x2="2944" y1="2400" y2="2400" x1="2928" />
        </branch>
        <branch name="rst_i">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="2464" type="branch" />
            <wire x2="2944" y1="2464" y2="2464" x1="2928" />
        </branch>
        <instance x="2944" y="3008" name="XLXI_202" orien="R0">
        </instance>
        <branch name="coarse_delay_i(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3536" y="2528" type="branch" />
            <wire x2="3536" y1="2528" y2="2528" x1="3488" />
        </branch>
        <branch name="control_i(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3552" y="2592" type="branch" />
            <wire x2="3552" y1="2592" y2="2592" x1="3488" />
        </branch>
        <branch name="fine_delay1_i(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3536" y="2400" type="branch" />
            <wire x2="3536" y1="2400" y2="2400" x1="3488" />
        </branch>
        <branch name="fine_delay2_i(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3536" y="2464" type="branch" />
            <wire x2="3536" y1="2464" y2="2464" x1="3488" />
        </branch>
        <branch name="rst_i">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="832" y="2064" type="branch" />
            <wire x2="880" y1="2064" y2="2064" x1="832" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="832" y="2128" type="branch" />
            <wire x2="880" y1="2128" y2="2128" x1="832" />
        </branch>
        <branch name="Channel_A">
            <wire x2="880" y1="2192" y2="2192" x1="832" />
        </branch>
        <branch name="BrcstCom_i(1:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="816" y="2256" type="branch" />
            <wire x2="880" y1="2256" y2="2256" x1="816" />
        </branch>
        <instance x="880" y="2544" name="XLXI_204" orien="R0">
        </instance>
        <branch name="eventcount_i(23:0)">
            <wire x2="1712" y1="2064" y2="2064" x1="1360" />
        </branch>
        <branch name="bunchcount_i(11:0)">
            <wire x2="1712" y1="2128" y2="2128" x1="1360" />
        </branch>
        <iomarker fontsize="28" x="832" y="2192" name="Channel_A" orien="R180" />
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1680" y="1936" type="branch" />
            <wire x2="1712" y1="1936" y2="1936" x1="1680" />
        </branch>
        <branch name="Channel_A">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="1872" type="branch" />
            <wire x2="1712" y1="1872" y2="1872" x1="1664" />
        </branch>
        <branch name="BCnt(11:0)">
            <wire x2="2224" y1="1936" y2="1936" x1="2160" />
        </branch>
        <branch name="BCntStrb">
            <wire x2="2224" y1="1872" y2="1872" x1="2160" />
        </branch>
        <branch name="control_i(1:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="2192" type="branch" />
            <wire x2="1712" y1="2192" y2="2192" x1="1664" />
        </branch>
        <branch name="rst_i">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1680" y="2000" type="branch" />
            <wire x2="1712" y1="2000" y2="2000" x1="1680" />
        </branch>
        <instance x="1712" y="2224" name="XLXI_198" orien="R0">
        </instance>
        <iomarker fontsize="28" x="2224" y="1872" name="BCntStrb" orien="R0" />
        <iomarker fontsize="28" x="2224" y="1936" name="BCnt(11:0)" orien="R0" />
        <branch name="rst_i">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1632" y="1520" type="branch" />
            <wire x2="1696" y1="1520" y2="1520" x1="1632" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1632" y="1456" type="branch" />
            <wire x2="1696" y1="1456" y2="1456" x1="1632" />
        </branch>
        <branch name="Channel_A">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1632" y="1584" type="branch" />
            <wire x2="1696" y1="1584" y2="1584" x1="1632" />
        </branch>
        <branch name="L1Accept">
            <wire x2="2208" y1="1456" y2="1456" x1="2192" />
        </branch>
        <branch name="coarse_delay_i(3:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1632" y="1648" type="branch" />
            <wire x2="1696" y1="1648" y2="1648" x1="1632" />
        </branch>
        <instance x="1696" y="1744" name="XLXI_195" orien="R0">
        </instance>
        <iomarker fontsize="28" x="2208" y="1456" name="L1Accept" orien="R0" />
        <branch name="rst_i">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1344" y="1728" type="branch" />
            <wire x2="1344" y1="1728" y2="1728" x1="1312" />
        </branch>
        <branch name="reset_b">
            <wire x2="928" y1="1728" y2="1728" x1="896" />
        </branch>
        <branch name="rst_com_i">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="880" y="1792" type="branch" />
            <wire x2="928" y1="1792" y2="1792" x1="880" />
        </branch>
        <instance x="928" y="1952" name="XLXI_203" orien="R0">
        </instance>
        <branch name="clk">
            <wire x2="928" y1="1856" y2="1856" x1="832" />
        </branch>
        <iomarker fontsize="28" x="896" y="1728" name="reset_b" orien="R180" />
        <iomarker fontsize="28" x="832" y="1856" name="clk" orien="R180" />
        <branch name="rst_i">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="848" y="2512" type="branch" />
            <wire x2="896" y1="2512" y2="2512" x1="848" />
        </branch>
        <iomarker fontsize="28" x="816" y="2560" name="Channel_B" orien="R180" />
        <branch name="Channel_B">
            <wire x2="896" y1="2560" y2="2560" x1="816" />
        </branch>
        <instance x="896" y="2688" name="XLXI_225" orien="R0">
        </instance>
        <branch name="indv_com_i(31:0)">
            <wire x2="1712" y1="2528" y2="2528" x1="1280" />
        </branch>
        <branch name="BrcstCom_i(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1328" y="2592" type="branch" />
            <wire x2="1328" y1="2592" y2="2592" x1="1280" />
        </branch>
        <instance x="1712" y="2656" name="XLXI_226" orien="R0">
        </instance>
        <branch name="DoutStrb">
            <wire x2="3536" y1="1664" y2="1664" x1="3488" />
            <wire x2="3552" y1="1664" y2="1664" x1="3536" />
        </branch>
        <iomarker fontsize="28" x="3552" y="1664" name="DoutStrb" orien="R0" />
        <iomarker fontsize="28" x="3536" y="2144" name="DQ(3:0)" orien="R0" />
        <branch name="DQ(3:0)">
            <wire x2="3536" y1="2144" y2="2144" x1="3488" />
        </branch>
        <branch name="indv_subAddr_i(15:8),indv_data_i(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="2112" type="branch" />
            <wire x2="2992" y1="2112" y2="2112" x1="2976" />
            <wire x2="3024" y1="2112" y2="2112" x1="2992" />
        </branch>
        <branch name="coarse_delay_i(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="2048" type="branch" />
            <wire x2="2992" y1="2048" y2="2048" x1="2976" />
            <wire x2="3024" y1="2048" y2="2048" x1="2992" />
        </branch>
        <branch name="fine_delay2_i(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="1984" type="branch" />
            <wire x2="2976" y1="1984" y2="1984" x1="2960" />
            <wire x2="3024" y1="1984" y2="1984" x1="2976" />
        </branch>
        <branch name="fine_delay1_i(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2960" y="1920" type="branch" />
            <wire x2="2976" y1="1920" y2="1920" x1="2960" />
            <wire x2="3024" y1="1920" y2="1920" x1="2976" />
        </branch>
        <branch name="clk">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="1856" type="branch" />
            <wire x2="2992" y1="1856" y2="1856" x1="2976" />
            <wire x2="3024" y1="1856" y2="1856" x1="2992" />
        </branch>
        <branch name="rst_i">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="1792" type="branch" />
            <wire x2="2992" y1="1792" y2="1792" x1="2976" />
            <wire x2="3024" y1="1792" y2="1792" x1="2992" />
        </branch>
        <branch name="intstrb_i">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2560" y="2368" type="branch" />
            <wire x2="2464" y1="2368" y2="2368" x1="2144" />
            <wire x2="2560" y1="2368" y2="2368" x1="2464" />
            <wire x2="2832" y1="2368" y2="2368" x1="2560" />
            <wire x2="2832" y1="2368" y2="2528" x1="2832" />
            <wire x2="2944" y1="2528" y2="2528" x1="2832" />
            <wire x2="2464" y1="1728" y2="2368" x1="2464" />
            <wire x2="3024" y1="1728" y2="1728" x1="2464" />
        </branch>
        <branch name="extstrb_i">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2304" y="2432" type="branch" />
            <wire x2="2304" y1="2432" y2="2432" x1="2144" />
            <wire x2="2432" y1="2432" y2="2432" x1="2304" />
            <wire x2="2768" y1="2432" y2="2432" x1="2432" />
            <wire x2="2768" y1="2432" y2="2592" x1="2768" />
            <wire x2="2944" y1="2592" y2="2592" x1="2768" />
            <wire x2="2432" y1="1664" y2="2432" x1="2432" />
            <wire x2="3024" y1="1664" y2="1664" x1="2432" />
        </branch>
        <instance x="3024" y="2208" name="XLXI_231" orien="R0">
        </instance>
        <branch name="Dout(7:0)">
            <wire x2="3536" y1="1824" y2="1824" x1="3488" />
        </branch>
        <iomarker fontsize="28" x="3536" y="1824" name="Dout(7:0)" orien="R0" />
        <branch name="SubAddr(7:0)">
            <wire x2="3536" y1="1984" y2="1984" x1="3488" />
        </branch>
        <iomarker fontsize="28" x="3536" y="1984" name="SubAddr(7:0)" orien="R0" />
        <branch name="control_i(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="2176" type="branch" />
            <wire x2="3024" y1="2176" y2="2176" x1="2976" />
        </branch>
        <text style="fontsize:32;fontname:Arial" x="3016" y="1436">In simulation the TTCrx is always ready</text>
        <text style="fontsize:32;fontname:Arial" x="3020" y="1512">Clock40Des1 is moddeled from the test bench </text>
        <branch name="TTCReady">
            <wire x2="2784" y1="1504" y2="1504" x1="2640" />
        </branch>
        <instance x="2576" y="1504" name="XLXI_194" orien="R0" />
        <iomarker fontsize="28" x="2784" y="1504" name="TTCReady" orien="R0" />
    </sheet>
</drawing>
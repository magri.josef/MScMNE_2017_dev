-------------------------------------------------------------------------------
-- Title      : L1 Accept signal outputs
-- Project    : TTCrx Model
-------------------------------------------------------------------------------
-- Description: Handeling the L1 trigger delay
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY L1_output IS
  PORT (clk           : IN std_logic;
        reset         : IN std_logic;
        L1            : IN std_logic;     												-- Level 1 trigger 
        Coarsedelay   : IN std_logic_vector (3 DOWNTO 0) := (others => '0');  -- lower 4 bits of Coarse delay reg.
        L1Accept      : OUT std_logic );   												-- delayed L1 trigger
END L1_output;

ARCHITECTURE arc OF L1_output IS

  SIGNAL delay1  : std_logic;
  SIGNAL delay2  : std_logic;
  SIGNAL delay3  : std_logic;
  SIGNAL delay4  : std_logic;
  SIGNAL delay5  : std_logic;
  SIGNAL delay6  : std_logic;
  SIGNAL delay7  : std_logic;
  SIGNAL delay8  : std_logic;
  SIGNAL delay9  : std_logic;
  SIGNAL delay10 : std_logic;
  SIGNAL delay11 : std_logic;
  SIGNAL delay12 : std_logic;
  SIGNAL delay13 : std_logic;
  SIGNAL delay14 : std_logic;
  SIGNAL delay15 : std_logic;
  
  BEGIN
-- This process puts the L1 signal through a series of flip-flops to create the
-- dealy in steps of 1 clock cycle.
-------------------------------------------------------------------------------
    pipelineWait: PROCESS(clk)
-------------------------------------------------------------------------------
    BEGIN
      IF reset = '1' THEN
        delay1  <= '0';
        delay2  <= '0';
        delay3  <= '0';
        delay4  <= '0';
        delay5  <= '0';
        delay6  <= '0';
        delay7  <= '0';
        delay8  <= '0';
        delay9  <= '0';
        delay10 <= '0';
        delay11 <= '0';
        delay12 <= '0';
        delay13 <= '0';
        delay14 <= '0';
        delay15 <= '0';    
      ELSIF (clk'event AND clk='1') THEN
        delay1  <= L1;
        delay2  <= delay1;
        delay3  <= delay2;
        delay4  <= delay3;
        delay5  <= delay4;
        delay6  <= delay5;
        delay7  <= delay6;
        delay8  <= delay7;
        delay9  <= delay8;
        delay10 <= delay9;
        delay11 <= delay10;
        delay12 <= delay11;
        delay13 <= delay12;
        delay14 <= delay13;
        delay15 <= delay14;                
      END IF;
    END PROCESS pipelineWait;

-- This statement choses after how much delay in the pipelineWait process the 
-- L1 signal is written to the L1Accept output based on the value in the Coarse
-- dealy register's 4 lowest bit. It is effectively a multiplekser.
   WITH Coarsedelay SELECT
      L1Accept <= L1      WHEN "0000",
                  delay1  WHEN "0001",
                  delay2  WHEN "0010",
                  delay3  WHEN "0011",
                  delay4  WHEN "0100",
                  delay5  WHEN "0101",
                  delay6  WHEN "0110",
                  delay7  WHEN "0111",
                  delay8  WHEN "1000",
                  delay9  WHEN "1001",
                  delay10 WHEN "1010",
                  delay11 WHEN "1011",
                  delay12 WHEN "1100",
                  delay13 WHEN "1101",
                  delay14 WHEN "1110",
                  delay15 WHEN "1111",
                  L1      WHEN OTHERS;
END arc;
        
    

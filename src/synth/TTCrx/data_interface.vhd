-------------------------------------------------------------------------------
-- Title      : Data Interface
-- Project    : TTCrx Model
-------------------------------------------------------------------------------
-- File       : data_interface.vhd
-- Author     : Sten Ingvar Solli  <sis@fys.uio.no>
-- Company    : Fysisk Institutt, UiO
-- Last update: 2002/12/20
-- Platform   : 
-------------------------------------------------------------------------------
-- Description: Determening the outputs on Dout and SubAddr pins
-------------------------------------------------------------------------------
-- ASSUMPTION OF FUNCTION:
-- In the sixth cycle of a CRDUMP I assume that the 6 highest bit of the 14 bit
-- chip ID from the ID register are written to the 6 lowest bit of the Dout
-- pins.
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2002/07/31  1.0      stenso	Created
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY data_interface IS
  PORT (FineDReg1   : IN std_logic_vector (7  DOWNTO 0);
        FineDReg2   : IN std_logic_vector (7  DOWNTO 0);
        CoarseDReg  : IN std_logic_vector (7  DOWNTO 0);
        ControlReg  : IN std_logic_vector (7  DOWNTO 0);
        Indv_data   : IN std_logic_vector (15 DOWNTO 0);
        Ext_DataStr : IN std_logic;
        Int_Datastr : IN std_logic;
        chip_reset  : IN std_logic;
        clk         : IN std_logic;
        Dout        : OUT std_logic_vector (7 DOWNTO 0);
        SubAddr     : OUT std_logic_vector (7 DOWNTO 0);
        DQ          : OUT std_logic_vector (3  DOWNTO 0);
        DoutStrb    : OUT std_logic
        );
END data_interface;

ARCHITECTURE arc OF data_interface IS

  TYPE DATA_TRANS_STATE IS (
    default, errdump1, errdump2, errdump3,
    crdump1, crdump2, crdump3, crdump4, crdump5, reset);
    -- at default no dump commando is issued and the individually
    -- adressed data is written on the outputs when recieved.

  SIGNAL data_trans : DATA_TRANS_STATE;

  SIGNAL Dout_i       : std_logic_vector (7 DOWNTO 0);
  SIGNAL SubAddr_i    : std_logic_vector (7 DOWNTO 0);
  SIGNAL DQ_i         : std_logic_vector (3  DOWNTO 0);
  SIGNAL DoutStrb_i   : std_logic;

BEGIN

  -- Outputs can be turned of by bit 5 in the Control register
 Dout <= Dout_i WHEN ControlReg(5) = '1' ELSE (OTHERS => 'Z');
 SubAddr <= SubAddr_i WHEN ControlReg(5) = '1' ELSE (OTHERS => 'Z');
 DQ <= DQ_i WHEN ControlReg(5) = '1' ELSE (OTHERS => 'Z');
 DoutStrb <= DoutStrb_i WHEN ControlReg(5) = '1' ELSE '0';
  
-------------------------------------------------------------------------------
  dataint: PROCESS (clk, chip_reset)
------------------------------------------------------------------------------           
  BEGIN
    IF chip_reset = '1' THEN
      Dout_i <= (OTHERS => '0');  
      SubAddr_i <= (OTHERS => '0');
      DQ_i <= "0000";
      DoutStrb_i <= '0';
    ELSIF (clk'event AND clk = '1') THEN
      DoutStrb_i <= '0';
          IF Ext_DataStr = '1' THEN		 
            Dout_i <= Indv_data(7 downto 0);
            SubAddr_i <= Indv_data(15 downto 8);
            DQ_i <= "0000";
            DoutStrb_i <= '1';
          ELSIF Int_Datastr = '1' THEN
            IF Indv_data(15 downto 8) = "00000100" THEN
              Dout_i <= (OTHERS => '0');
              DQ_i <= "0001";
              DoutStrb_i <= '1';
            ELSIF Indv_data(15 downto 8) = "00000101" THEN
              Dout_i <= FineDReg1;
              DQ_i <= "0101";
              DoutStrb_i <= '1';
            END IF;
          -- else do nothing (DoutStrb_i will be zero)
          END IF;
    END IF;
  END PROCESS dataint;
END arc;

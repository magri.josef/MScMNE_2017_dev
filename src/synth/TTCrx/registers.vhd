-------------------------------------------------------------------------------
-- Title      : The TTCrx registers
-- Project    : TTCrx Model
-------------------------------------------------------------------------------
-- Description: The TTCrx registers are modelled here.
-- They can be written via individually adressed commands. 
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY registers IS
  PORT (clk            : IN std_logic;
        chip_reset     : IN std_logic;
        indv_data      : IN std_logic_vector(7 DOWNTO 0);
        indv_subAddr   : IN std_logic_vector(7 DOWNTO 0);
        indv_int_strb  : IN std_logic;
		  indv_ext_strb  : IN std_logic;
        fine_delay1_o  : OUT std_logic_vector(7 DOWNTO 0);
        fine_delay2_o  : OUT std_logic_vector(7 DOWNTO 0);
        coarse_delay_o : OUT std_logic_vector(7 DOWNTO 0) := (others => '0');
        control_o      : OUT std_logic_vector(7 DOWNTO 0)
		  );                              
END registers;

ARCHITECTURE arc OF registers IS

SIGNAL  fine_delay1   : std_logic_vector(7 DOWNTO 0);
SIGNAL  fine_delay2   : std_logic_vector(7 DOWNTO 0);
SIGNAL  coarse_delay  : std_logic_vector(7 DOWNTO 0);
SIGNAL  control       : std_logic_vector(7 DOWNTO 0);

BEGIN

  fine_delay1_o  <= fine_delay1;
  fine_delay2_o  <= fine_delay2;
  coarse_delay_o <= coarse_delay;
  control_o      <= control;
  
-------------------------------------------------------------------------------
  reg_interface : PROCESS (clk,chip_reset)
-------------------------------------------------------------------------------
  BEGIN
    
    -- Async reset/preset
    IF chip_reset = '1' THEN
      fine_delay1  <= (OTHERS => '0');
      fine_delay2  <= (OTHERS => '0');
      coarse_delay <= (OTHERS => '0');
      control      <= "10010011";
    -- This SM writes in values on the timing and control registers recieved
    -- from individually adressed commands. Takes precedence over the I2C
    -- interface in case it writes at the same time.
    ELSIF indv_int_strb = '1' THEN
	    CASE indv_subAddr( 3 downto 0 ) IS
        WHEN "0000" =>                -- new value to fine delay 1
          fine_delay1 <= indv_data;
        WHEN "0001" =>                -- new value to fine delay 2
          fine_delay2 <= indv_data;
        WHEN "0010" =>                -- new value to coarse delay
          coarse_delay <= indv_data;
        WHEN "0011" =>                -- new value to control reg
          control <= indv_data;
        WHEN OTHERS =>                    -- command not intended for regs
      END CASE; 
    ELSIF indv_ext_strb = '1' THEN
	   --control <= indv_data;
		control(5) <= '1';
    END IF;
  END PROCESS reg_interface;
END arc;

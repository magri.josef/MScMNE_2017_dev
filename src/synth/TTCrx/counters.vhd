-------------------------------------------------------------------------------
-- Title      : counters.vhd
-- Project    : TTCrx Model
-------------------------------------------------------------------------------
-- Description: The different TTCrx counters are simulated here.
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;

ENTITY counters IS
  PORT (brdcastbits  : IN     std_logic_vector (1 DOWNTO 0);
        chip_reset   : IN     std_logic;
        clk          : IN     std_logic;
        L1Accept     : IN     std_logic;
        eventcount   : OUT std_logic_vector (23 DOWNTO 0);
        bunchcount   : OUT std_logic_vector (11 DOWNTO 0)
		  );
END  counters;

ARCHITECTURE arc OF counters IS

  SIGNAL bunchcount_i : std_logic_vector (11 DOWNTO 0) := (others => '0')  ;

BEGIN

-------------------------------------------------------------------------------
  event_incr : PROCESS(clk,L1Accept,chip_reset,brdcastbits(1))
-------------------------------------------------------------------------------
VARIABLE eventcount_v : std_logic_vector (23 DOWNTO 0);  -- to flag if counter is "full"
-- counts up on L1 Accepts and is wrap-around
  BEGIN
    IF (clk'event AND clk = '1') THEN
      IF L1Accept = '1' THEN            -- various L1 can come after eachother
        eventcount_v := eventcount_v + 1;
      END IF;
    END IF;
    -- reset
    IF (chip_reset = '1' OR brdcastbits(1) = '1') THEN
      eventcount_v := (OTHERS => '0');
    END IF;
	 eventcount <= eventcount_v;
  END PROCESS event_incr;

-------------------------------------------------------------------------------
  bunch_incr : PROCESS(clk,chip_reset,brdcastbits(0))
-------------------------------------------------------------------------------
-- counts up on clock (bunch passings) and is wrap-around
  BEGIN
    IF (clk'event AND clk = '1') THEN
      bunchcount_i <= bunchcount_i + 1;
		bunchcount <= bunchcount_i;
    END IF;
    -- reset
    IF (chip_reset = '1' OR brdcastbits(0) = '1') THEN 
      bunchcount <= (OTHERS => '0');
    END IF;
  END PROCESS bunch_incr;

END arc;

-------------------------------------------------------------------------------
-- Title      : Serial to paralell converter of channel B
-- Project    : TTCrx Model
-------------------------------------------------------------------------------
-- Description: Takes the serial input from Channel B and converts it to either
-- a 8 bit broadcast command or a 32 bit (16bit data) indv. adressed command.
-- As we are not using the hamming decoder we'll just ignore those bits.
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY channelB_converter IS
  GENERIC(brdcst_width : positive := 8;
          indv_width   : positive := 32);

  PORT(clk          : IN  std_logic := '0';
       reset        : IN  std_logic := '0';
       serial_in    : IN  std_logic;
       IndvStrb     : OUT std_logic;
       Indv_com     : OUT std_logic_vector(indv_width-1 DOWNTO 0);
       Brdcst_com   : OUT std_logic_vector(brdcst_width-1 DOWNTO 0)
       );
END channelB_converter;

ARCHITECTURE arc OF channelB_converter IS

-- Declare a new type for the states
  TYPE state_type IS (wait_st,reset_st,indv_send,brdcst_send, b_read0, b_read1
                      ,b_read2, b_read3, b_read4, b_read5, b_read6, b_read7,
                      i_read0, i_read1, i_read2, i_read3, i_read4, i_read5,
                      i_read6, i_read7, i_read8, i_read9, i_read10, i_read11,
                      i_read12, i_read13, i_read14, i_read15, i_read16,
                      i_read17, i_read18, i_read19, i_read20, i_read21,
                      i_read22, i_read23, i_read24, i_read25, i_read26,
                      i_read27, i_read28, i_read29, i_read30, i_read31, FMT,
                      b_readx, i_readx);
                    
  SIGNAL state      : state_type;
  SIGNAL indv_reg   : std_logic_vector(indv_width-1 DOWNTO 0);    -- Buffer
  SIGNAL brdcst_reg : std_logic_vector(brdcst_width-1 DOWNTO 0);  -- Buffer

BEGIN
-------------------------------------------------------------------------------
  converter: PROCESS(clk,reset)
-------------------------------------------------------------------------------
  --VARIABLE ihammingbits, bchammingbits, resetwait : integer;      --natural :=0;      := (others => '0')
  VARIABLE ihammingbits  : UNSIGNED(2 downto 0) := (others => '0');
  VARIABLE bchammingbits : UNSIGNED(2 downto 0) := (others => '0');
  VARIABLE resetwait     : UNSIGNED(5 downto 0) := (others => '0');
  
  BEGIN
    IF reset = '1' THEN
      state <= reset_st;
      IndvStrb   <= '0';
      Indv_com   <= (OTHERS => '0');
      Brdcst_com <= (OTHERS => '0');
	 ELSIF rising_edge(clk) THEN
   -- END IF;
   -- IF rising_edge(clk) THEN
      CASE state IS

        WHEN wait_st =>                  -- Waiting for startbit=0
          resetwait := (OTHERS => '0');
          IndvStrb <= '0';
          IF serial_in = '0' THEN
            state <= FMT;
          ELSIF serial_in = '1' THEN
            state <= wait_st;
          END IF;

        WHEN FMT =>                     -- Determining the FMT bit
          IF serial_in = '0' THEN       -- wether it is a broadcast
            state <= b_read0;           -- or an individual command.
          ELSIF serial_in = '1' THEN
            state <= i_read0;
          END IF;

        WHEN reset_st =>                   
          IF serial_in = '1' THEN
            resetwait := resetwait+1;
            IF resetwait = "101000" THEN--40 THEN       -- We can now be sure we're not
              state <= wait_st;           -- inside recieveing a command
            ELSE state <= reset_st;
            END IF;
          ELSIF serial_in = '0' THEN   
            resetwait := (OTHERS => '0');
            state <= reset_st;
          END  IF;

        -- Reciving a broadcast command
          
        WHEN b_read0 =>                -- Reading in this bit
          brdcst_reg(7) <= serial_in;  -- Store input
          state         <= b_read1;    -- Read next bit

        WHEN b_read1 =>                -- Reading in this bit
          brdcst_reg(6) <= serial_in;  -- Store input
          state         <= b_read2;    -- Read next bit
            
        WHEN b_read2 =>                -- Reading in this bit
          brdcst_reg(5) <= serial_in;  -- Store input
          state         <= b_read3;    -- Read next bit

        WHEN b_read3 =>                -- Reading in this bit
          brdcst_reg(4) <= serial_in;  -- Store input
          state         <= b_read4;    -- Read next bit

        WHEN b_read4 =>                -- Reading in this bit
          brdcst_reg(3) <= serial_in;  -- Store input
          state         <= b_read5;    -- Read next bit

        WHEN b_read5 =>                -- Reading in this bit
          brdcst_reg(2) <= serial_in;  -- Store input
          state         <= b_read6;    -- Read next bit

        WHEN b_read6 =>                -- Reading in this bit
          brdcst_reg(1) <= serial_in;  -- Store input
          state         <= b_read7;    -- Read next bit

        WHEN b_read7 =>                -- Reading in this bit
          brdcst_reg(0) <= serial_in;  -- Store input
          state         <= b_readx;    -- Read next bit

        
        WHEN b_readx=>                 -- Just wait til the hamming check
          IF bchammingbits < "101" THEN    -- bits pass
            bchammingbits := bchammingbits+1;
            state <= b_readx;
          ELSE
            Brdcst_com <= brdcst_reg;
            state <= brdcst_send;
          END IF;
            
        WHEN brdcst_send =>             -- Sending bits on output bus
          ASSERT (serial_in = '1')      -- kept on bus until next sending
          REPORT "Frame error in brdcst command. Stop bit error." SEVERITY ERROR;
          bchammingbits := (OTHERS => '0');  
          state <= wait_st;

        -- Reciving an Individually adressed command
          
        WHEN i_read0 =>                -- Reading in this bit
          indv_reg(31)   <= serial_in;  -- Store input
          state         <= i_read1;    -- Read next bit

        WHEN i_read1 =>                -- Reading in this bit
          indv_reg(30)   <= serial_in;  -- Store input
          state         <= i_read2;    -- Read next bit

        WHEN i_read2 =>                -- Reading in this bit
          indv_reg(29)   <= serial_in;  -- Store input
          state         <= i_read3;    -- Read next bit

        WHEN i_read3 =>                -- Reading in this bit
          indv_reg(28)   <= serial_in;  -- Store input
          state         <= i_read4;    -- Read next bit

        WHEN i_read4 =>                -- Reading in this bit
          indv_reg(27)   <= serial_in;  -- Store input
          state         <= i_read5;    -- Read next bit

        WHEN i_read5 =>                -- Reading in this bit
          indv_reg(26)   <= serial_in;  -- Store input
          state         <= i_read6;    -- Read next bit

        WHEN i_read6 =>                -- Reading in this bit
          indv_reg(25)   <= serial_in;  -- Store input
          state         <= i_read7;    -- Read next bit

        WHEN i_read7 =>                -- Reading in this bit
          indv_reg(24)   <= serial_in;  -- Store input
          state         <= i_read8;    -- Read next bit

        WHEN i_read8 =>                -- Reading in this bit
          indv_reg(23)   <= serial_in;  -- Store input
          state         <= i_read9;    -- Read next bit

        WHEN i_read9 =>                -- Reading in this bit
          indv_reg(22)   <= serial_in;  -- Store input
          state         <= i_read10;   -- Read next bit

        WHEN i_read10 =>               -- Reading in this bit
          indv_reg(21)  <= serial_in;  -- Store input
          state         <= i_read11;   -- Read next bit

        WHEN i_read11 =>               -- Reading in this bit
          indv_reg(20)  <= serial_in;  -- Store input
          state         <= i_read12;   -- Read next bit

        WHEN i_read12 =>               -- Reading in this bit
          indv_reg(19)  <= serial_in;  -- Store input
          state         <= i_read13;   -- Read next bit

        WHEN i_read13 =>               -- Reading in this bit
          indv_reg(18)  <= serial_in;  -- Store input
          state         <= i_read14;   -- Read next bit

        WHEN i_read14 =>               -- Reading in this bit
          indv_reg(17)  <= serial_in;  -- Store input
          state         <= i_read15;   -- Read next bit

        
        WHEN i_read15 =>               -- Reading in this bit
          indv_reg(16)  <= serial_in;  -- Store input
          state         <= i_read16;   -- Read next bit

        WHEN i_read16 =>               -- Reading in this bit
          indv_reg(15)  <= serial_in;  -- Store input
          state         <= i_read17;   -- Read next bit

        WHEN i_read17 =>               -- Reading in this bit
          indv_reg(14)  <= serial_in;  -- Store input
          state         <= i_read18;   -- Read next bit

        WHEN i_read18 =>               -- Reading in this bit
          indv_reg(13)  <= serial_in;  -- Store input
          state         <= i_read19;   -- Read next bit

        WHEN i_read19 =>               -- Reading in this bit
          indv_reg(12)  <= serial_in;  -- Store input
          state         <= i_read20;   -- Read next bit

        WHEN i_read20 =>               -- Reading in this bit
          indv_reg(11)  <= serial_in;  -- Store input
          state         <= i_read21;   -- Read next bit

        WHEN i_read21 =>               -- Reading in this bit
          indv_reg(10)  <= serial_in;  -- Store input
          state         <= i_read22;   -- Read next bit

        WHEN i_read22 =>               -- Reading in this bit
          indv_reg(9)  <= serial_in;  -- Store input
          state         <= i_read23;   -- Read next bit

        WHEN i_read23 =>               -- Reading in this bit
          indv_reg(8)  <= serial_in;  -- Store input
          state         <= i_read24;   -- Read next bit

        WHEN i_read24 =>               -- Reading in this bit
          indv_reg(7)  <= serial_in;  -- Store input
          state         <= i_read25;   -- Read next bit

        WHEN i_read25 =>               -- Reading in this bit
          indv_reg(6)  <= serial_in;  -- Store input
          state         <= i_read26;   -- Read next bit

        WHEN i_read26 =>               -- Reading in this bit
          indv_reg(5)  <= serial_in;  -- Store input
          state         <= i_read27;   -- Read next bit

        WHEN i_read27 =>               -- Reading in this bit
          indv_reg(4)  <= serial_in;  -- Store input
          state         <= i_read28;   -- Read next bit

        WHEN i_read28 =>               -- Reading in this bit
          indv_reg(3)  <= serial_in;  -- Store input
          state         <= i_read29;   -- Read next bit

        WHEN i_read29 =>               -- Reading in this bit
          indv_reg(2)  <= serial_in;  -- Store input
          state         <= i_read30;   -- Read next bit

        WHEN i_read30 =>               -- Reading in this bit
          indv_reg(1)  <= serial_in;  -- Store input
          state         <= i_read31;   -- Read next bit

        WHEN i_read31 =>               -- Reading in this bit
          indv_reg(0)  <= serial_in;  -- Store input
          state         <= i_readx;    -- Read next bit


        WHEN i_readx=>                 -- Just wait til the hamming check
          IF ihammingbits < "111" THEN     -- bits pass
            ihammingbits := ihammingbits+1;
            state <= i_readx;
          ELSE
            Indv_com <= indv_reg;
            state <= indv_send;
          END IF;
            
        WHEN indv_send =>               -- Sending bits on bus
          ASSERT (serial_in = '1')      -- kept on bus until next sending
          REPORT "Frame error in indv command. Stop bit error." SEVERITY ERROR;
          --Indv_com <= indv_reg;
          IndvStrb <= '1';
          ihammingbits := (OTHERS => '0');  -- bug fix for hamming reset counter
          state <= wait_st;

        WHEN OTHERS =>
          state <= wait_st;
          
        END CASE;
      END IF;
    END PROCESS converter;
END arc;


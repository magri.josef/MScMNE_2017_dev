-------------------------------------------------------------------------------
-- Title      : Counter Interface
-- Project    : TTCrx Model
-------------------------------------------------------------------------------
-- Description: Determening the outputs on pins BCnt<11:0>, BCntStrb
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY counter_interface IS
  PORT (EventCount : IN  std_logic_vector (23 DOWNTO 0):= (others=>'0');
        BunchCount : IN  std_logic_vector (11 DOWNTO 0):= (others=>'0');
        CntrReg    : IN  std_logic_vector (1 DOWNTO 0); -- bit 1:0 of control reg
        L1Accept   : IN  std_logic;
        clk        : IN  std_logic;
        reset      : IN  std_logic;
        BCnt       : OUT std_logic_vector (11 DOWNTO 0):= (others=>'0');
        BCntStrb   : OUT std_logic
        );
END counter_interface;


ARCHITECTURE arc OF counter_interface IS

  TYPE TRIGGER_CYCLE_STATE IS (
    default, TC1, TC2 );
  -- deafult is when no L1Accept OR the cycle when the L1Accept signal is
  -- activated. TC1 and TC2 is the following cycles after a L1Accept.

  SIGNAL trigger_cycle : TRIGGER_CYCLE_STATE;

BEGIN	
-------------------------------------------------------------------------------
  countint: PROCESS (clk,L1Accept,reset)
-------------------------------------------------------------------------------
  BEGIN

    IF reset = '1' THEN
      BCnt <= (OTHERS => '0');
      BCntStrb  <= '0';
      trigger_cycle <= default;     
    ELSIF rising_edge(clk) THEN
      
      IF (L1Accept = '1' AND trigger_cycle /= default) THEN
        -- a new L1Accept interupts the output cycle and starts over.
        -- we do the same here as in the default cycle.
        BCntStrb          <= '0';
        trigger_cycle <= default;
        IF CntrReg = "00" THEN
          BCnt          <= EventCount(11 DOWNTO 0);
        ELSIF CntrReg = "01" THEN
          BCnt          <= BunchCount;
          BCntStrb      <= '1';
        ELSIF CntrReg = "10" THEN
          BCnt          <= EventCount(11 DOWNTO 0);
          trigger_cycle <= TC1;
        ELSIF CntrReg = "11" THEN
          BCnt          <= BunchCount;
          BCntStrb      <= '1';
          trigger_cycle <= TC1;
        END IF;
		  
		ELSE
		
      CASE trigger_cycle IS
        WHEN default =>
          IF L1Accept = '0' THEN
            BCntStrb          <= '0';
            IF CntrReg = "01" THEN
              BCnt          <= BunchCount;
            ELSE
              BCnt          <= EventCount(11 DOWNTO 0);
            END IF;
          ELSE     -- L1Accept='1'
            IF CntrReg = "00" THEN
              BCnt          <= EventCount(11 DOWNTO 0);
            ELSIF CntrReg = "01" THEN
              BCnt          <= BunchCount;
              BCntStrb      <= '1';
            ELSIF CntrReg = "10" THEN
              BCnt          <= EventCount(11 DOWNTO 0);
              trigger_cycle <= TC1;
            ELSIF CntrReg = "11" THEN
              BCnt          <= BunchCount;
              BCntStrb      <= '1';
              trigger_cycle <= TC1;
            END IF;
          END IF;

        WHEN TC1 =>
				IF rising_edge(clk) THEN
            IF CntrReg = "10" THEN
              BCnt          <= EventCount(23 DOWNTO 12);
              trigger_cycle <= default;
            ELSIF CntrReg = "11" THEN
              BCntStrb <= '0';
              BCnt          <= EventCount(11 DOWNTO 0);
              trigger_cycle <= TC2;
            ELSE
              ASSERT (CntrReg = "10" OR CntrReg = "11") 
                REPORT "Error in cnt_interface state machine" SEVERITY ERROR;
            END IF;
          END IF;

        WHEN TC2 =>
				IF rising_edge(clk) THEN
            ASSERT CntrReg = "11"
              REPORT "Error in cnt_interface state machine" SEVERITY ERROR;
            BCnt          <= EventCount(23 DOWNTO 12);
            trigger_cycle <= default;
          END IF;

        WHEN OTHERS =>
          trigger_cycle <= default;
          
      END CASE;
    END IF;
  END IF;
END PROCESS countint;        
END arc;















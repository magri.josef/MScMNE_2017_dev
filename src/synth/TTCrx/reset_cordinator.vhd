-------------------------------------------------------------------------------
-- Title      : Reset signals cordinator
-- Project    : TTCrx Model
-------------------------------------------------------------------------------
-- Description: To take in the reset signals from different sources and to
--              distrebute a single reset signal to the other enteties.
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY reset_cord IS
  PORT (reset_b     : IN std_logic;     -- Active low. Pin input
        reset_com   : IN std_logic;     -- From indv_com_decoder
        clk         : IN std_logic;
        chip_reset  : OUT std_logic
        );
END reset_cord;

ARCHITECTURE arc OF reset_cord IS
  SIGNAL prolonging : std_logic;        -- used to ensure we get a minimum
                                        -- width of the reset puls of 12.5 ns
  BEGIN

    cord: PROCESS(clk,reset_b,reset_com)
    BEGIN
      IF (reset_b ='0' OR reset_com ='1') THEN
        chip_reset <= '1';
        prolonging <= '1';
      ELSE
        IF prolonging = '1' THEN
          prolonging <= '0';
        ELSE
          chip_reset <= '0';
        END IF;
      END IF;
  END PROCESS cord;
    
END arc;

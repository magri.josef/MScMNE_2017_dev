FUNCTION dcfifo( data[lpm_width-1..0], rdclk, rdreq, wrclk, wrreq, aclr )
    WITH( LPM_WIDTH, LPM_NUMWORDS, 
    	  LPM_SHOWAHEAD, UNDERFLOW_CHECKING, OVERFLOW_CHECKING, USE_EAB, 
    	  DELAY_RDUSEDW, DELAY_WRUSEDW, RDSYNC_DELAYPIPE, WRSYNC_DELAYPIPE, CLOCKS_ARE_SYNCHRONIZED )
    RETURNS( q[lpm_width-1..0], rdempty, rdfull, wrempty, wrfull, rdusedw[ceil(log2(lpm_numwords))-1..0], wrusedw[ceil(log2(lpm_numwords))-1..0]%, db_port[ceil(log2(lpm_numwords))..0]%);


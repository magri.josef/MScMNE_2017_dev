--------------------------------------------------------------------
--
--	Compare to Constant Parameterized Megafunction
--
--	Copyright (C) 1988-2002 Altera Corporation
--	Any megafunction design, and related net list (encrypted or decrypted),
--	support information, device programming or simulation file, and any other
--	associated documentation or information provided by Altera or a partner
--	under Altera's Megafunction Partnership Program may be used only to
--	program PLD devices (but not masked PLD devices) from Altera.  Any other
--	use of such megafunction design, net list, support information, device
--	programming or simulation file, or any other related documentation or
--	information is prohibited for any other purpose, including, but not
--	limited to modification, reverse engineering, de-compiling, or use with
--	any other silicon devices, unless such use is explicitly licensed under
--	a separate agreement with Altera or a megafunction partner.  Title to
--	the intellectual property, including patents, copyrights, trademarks,
--	trade secrets, or maskworks, embodied in any such megafunction design,
--	net list, support information, device programming or simulation file, or
--	any other related documentation or information provided by Altera or a
--	megafunction partner, remains with Altera, the megafunction partner, or
--	their respective licensors.  No other licenses, including any licenses
--	needed under any third party's intellectual property, are provided herein.
--
--	Version 1.0
--
--------------------------------------------------------------------

% This file is a subdesign of many LPM functions %

PARAMETERS
(
	WIDTH,
	CVALUE
);

SUBDESIGN cmpconst
(
	data[WIDTH-1..0]		: INPUT;
	result					: OUTPUT;
)

VARIABLE
	and_cascade[WIDTH-1..0]	: node;

begin

	ASSERT (WIDTH > 0)
			REPORT "Value of WIDTH parameter must be greater than 0"
			SEVERITY ERROR
			HELP_ID CMPCONST_WIDTH;

	FOR ebit IN 0 TO WIDTH-1 GENERATE
		IF ((2^ebit & CVALUE) == 0) GENERATE
			IF (ebit == 0) GENERATE
				and_cascade[ebit] = !data[ebit];
			ELSE GENERATE
				and_cascade[ebit] = !data[ebit] & and_cascade[ebit-1];
			END GENERATE;
		ELSE GENERATE
			IF (ebit == 0) GENERATE
				and_cascade[ebit] = data[ebit];
			ELSE GENERATE
				and_cascade[ebit] = data[ebit] & and_cascade[ebit-1];
			END GENERATE;
		END GENERATE;
	END GENERATE;
	
	result = and_cascade[WIDTH-1];
end;

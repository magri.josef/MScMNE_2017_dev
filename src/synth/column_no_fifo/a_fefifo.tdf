--------------------------------------------------------------------
--
--	Full/Empty/Threshold outputs for FIFO Parameterized Megafunction
--
--	Copyright (C) 1988-1999 Altera Corporation
--	Any megafunction design, and related net list (encrypted or decrypted),
--	support information, device programming or simulation file, and any other
--	associated documentation or information provided by Altera or a partner
--	under Altera's Megafunction Partnership Program may be used only to
--	program PLD devices (but not masked PLD devices) from Altera.  Any other
--	use of such megafunction design, net list, support information, device
--	programming or simulation file, or any other related documentation or
--	information is prohibited for any other purpose, including, but not
--	limited to modification, reverse engineering, de-compiling, or use with
--	any other silicon devices, unless such use is explicitly licensed under
--	a separate agreement with Altera or a megafunction partner.  Title to
--	the intellectual property, including patents, copyrights, trademarks,
--	trade secrets, or maskworks, embodied in any such megafunction design,
--	net list, support information, device programming or simulation file, or
--	any other related documentation or information provided by Altera or a
--	megafunction partner, remains with Altera, the megafunction partner, or
--	their respective licensors.  No other licenses, including any licenses
--	needed under any third party's intellectual property, are provided herein.
--
--	Version 2.0
--
--------------------------------------------------------------------

include "lpm_counter";
include "lpm_compare";

parameters
(
	LPM_WIDTHAD, 
	LPM_NUMWORDS, 
	ALLOW_RWCYCLE_WHEN_FULL = "OFF",
	UNDERFLOW_CHECKING, 
	OVERFLOW_CHECKING,
	USEDW_IN_DELAY = 0
);

constant ALMOSTFULL1 = LPM_NUMWORDS-1;
constant ALMOSTFULL2 = LPM_NUMWORDS-2;
constant ALMOSTFULL3 = LPM_NUMWORDS>3 ? LPM_NUMWORDS-3 : 0;

--------------------------------------------------------------------
subdesign a_fefifo
(
	usedw_in[LPM_WIDTHAD-1..0]		: input = gnd;
	usedw_out[LPM_WIDTHAD-1..0]		: output;

	wreq, rreq						: input = gnd;
	empty							: output;
	full							: output;

	threshlevel[LPM_WIDTHAD-1..0]	: input = gnd;
	threshold						: output;

	clock							: input;
	aclr, sclr						: input = gnd;
)

--------------------------------------------------------------------
variable
	usedw[LPM_WIDTHAD-1..0]		: node;

	if not used(usedw_in) generate
		valid_wreq, valid_rreq: NODE;
	end generate;
	
	if used(rreq) and used(wreq) generate				-- singe-clock FIFO
	
		is_almost_empty	: NODE;
		is_almost_full	: NODE;
		
		sm_emptyfull: MACHINE OF BITS( b_full, b_non_empty )
					  WITH STATES(
							state_empty		= B"00",
							state_middle	= B"01",
							state_full		= B"11");

	else generate										-- dual-clock FIFO
	
		if used(rreq) generate								-- read-side

			if USEDW_IN_DELAY==0 generate
				sm_empty : machine of bits( b_non_empty )
								 with states(
									state_empty		= B"0",
									state_non_empty	= B"1");
			else generate
				sm_empty : machine of bits( b_one, b_non_empty )
								 with states(
									state_empty		= B"00",
									state_emptywait	= B"10",
									state_non_empty	= B"01");

				lrreq: dff;
				valid_rreq, usedw_is_1: node;
			end generate;

		else generate										-- write-side

			if USEDW_IN_DELAY==0 generate
				sm_empty : machine of bits( b_non_empty )
								 with states(
									state_empty		= B"0",
									state_non_empty	= B"1");
			else generate
				sm_empty : machine of bits( b_one, b_non_empty )
								 with states(
									state_empty		= B"00",
									state_one		= B"01",
									state_non_empty	= B"11");
			end generate;
		end generate;
	end generate;

--------------------------------------------------------------------
begin
	assert (UNDERFLOW_CHECKING=="ON") or (UNDERFLOW_CHECKING=="OFF")
			report "UNDERFLOW_CHECKING must be either equal to ON or OFF"
			severity error;

	assert (OVERFLOW_CHECKING=="ON") or (OVERFLOW_CHECKING=="OFF")
			report "OVERFLOW_CHECKING must be either equal to ON or OFF"
			severity error;

	assert (ALLOW_RWCYCLE_WHEN_FULL=="ON") or (ALLOW_RWCYCLE_WHEN_FULL=="OFF")
			report "ALLOW_RWCYCLE_WHEN_FULL must be either equal to ON or OFF"
			severity error;

--------------------------------------------------------------------
	usedw_out[]	= usedw[];
	if used(usedw_in) generate
		usedw[] = usedw_in[];
	else generate

		if UNDERFLOW_CHECKING=="OFF" generate
			valid_rreq = rreq;
		else generate
			valid_rreq = rreq and !empty;
		end generate; 
	
		if OVERFLOW_CHECKING=="OFF" generate
			valid_wreq = wreq;
		else generate 
			if ALLOW_RWCYCLE_WHEN_FULL=="ON" generate
				valid_wreq = wreq and (!full or rreq);
			else generate
				valid_wreq = wreq and !full;
			end generate; 
		end generate;
	
		usedw[] = lpm_counter(.clock = clock, 
							  .updown = valid_wreq, 
							  .cnt_en = valid_wreq xor valid_rreq, 
							  .aclr = aclr, 
							  .sclr = sclr )
				  with( LPM_WIDTH=LPM_WIDTHAD )
				  returns( .q[] );
	end generate;

--------------------------------------------------------------------
	if used(rreq) and used(wreq) generate
	
		is_almost_empty	= (usedw[]==1);
		is_almost_full	= (usedw[]==ALMOSTFULL1);

		case sm_emptyfull IS
			when state_empty =>	if wreq and !sclr then
									sm_emptyfull = state_middle;
								end if;
			when state_middle=> if (is_almost_empty and rreq and !wreq) or sclr then
									sm_emptyfull = state_empty;
								end if;
								if (is_almost_full and wreq and !rreq) and !sclr then
									sm_emptyfull = state_full;
								end if;
			when state_full	 =>	
				if ALLOW_RWCYCLE_WHEN_FULL=="OFF" generate
								if sclr then
									sm_emptyfull = state_empty;
								elsif rreq then
									sm_emptyfull = state_middle;
								end if;
				else generate
								if sclr then
									sm_emptyfull = state_empty;
								elsif rreq and !wreq then
									sm_emptyfull = state_middle;
								end if;
				end generate;
		end case;

		empty = !b_non_empty;
		full = b_full;
		sm_emptyfull.(clk, reset) = (clock, aclr);

	else generate if used(rreq) generate				-- read-side

		if USEDW_IN_DELAY==0 generate

			case sm_empty is
			when state_empty=>		if (usedw[]!=0) then
										sm_empty = state_non_empty;
									end if;
	
			when state_non_empty=>	if (usedw[]==1 and rreq) or usedw[]==0 then
										sm_empty = state_empty;
									end if;
			end case;
		
		else generate

			if UNDERFLOW_CHECKING=="OFF" generate
				valid_rreq = rreq;
			else generate
				valid_rreq = rreq and !empty;
			end generate; 

			lrreq.(d, clk, clrn) = (valid_rreq, clock, !aclr);

--			usedw_is_0 = (usedw[]==0           ) or (usedw[]==1 and lrreq);
			usedw_is_1 = (usedw[]==1 and !lrreq) or (usedw[]==2 and lrreq);
	
			case sm_empty is
			when state_empty=>		if (usedw[]!=0) then
										sm_empty = state_non_empty;
									end if;
	
			when state_non_empty=>	if (usedw_is_1 and rreq) then --or usedw_is_0 then
										sm_empty = state_emptywait;
									end if;

			when state_emptywait=>	if (usedw[]!=0) and (usedw[]!=1) then
										sm_empty = state_non_empty;
									else
										sm_empty = state_empty;
									end if;
			end case;

		end generate;

		empty = !b_non_empty;
		sm_empty.(clk, reset) = (clock, aclr);

		full = dff(	.d	 = lpm_compare( .dataa[]=usedw[], .datab[]=ALMOSTFULL3 ) with(LPM_WIDTH=LPM_WIDTHAD,ONE_INPUT_IS_CONSTANT="ON") returns(.ageb), 
					.clk = clock, 
					.clrn= !aclr
				  );

	else generate										-- write-side

		full = dff(	.d	 = lpm_compare( .dataa[]=usedw[], .datab[]=ALMOSTFULL3 ) with(LPM_WIDTH=LPM_WIDTHAD,ONE_INPUT_IS_CONSTANT="ON") returns(.ageb), 
					.clk = clock, 
					.clrn= !aclr
				  );

		if USEDW_IN_DELAY==0 generate

			case sm_empty is
			when state_empty	 =>	if (usedw[]!=0) then		
										sm_empty=state_non_empty;			
									end if;
	
			when state_non_empty =>	if (usedw[]==0) and !wreq then
										sm_empty=state_empty;
									end if;
			end case;

		else generate
	
			case sm_empty is
			when state_empty	 =>	if wreq then		
										sm_empty = state_one;			
									end if;
	
			when state_one		 =>	if !wreq then
										sm_empty = state_non_empty;
									end if;
	
			when state_non_empty =>	if wreq then
										sm_empty = state_one;
									elsif (usedw[]==0) then
										sm_empty = state_empty;
									end if;
			end case;

		end generate;
		
		empty = !b_non_empty;
		sm_empty.(clk, reset) = (clock, aclr);

	end generate;	end generate;

--------------------------------------------------------------------
-- Threshhold output is: (full) # (usedw>=threshlevel)
--
	threshold = full; 

	if used(threshlevel) generate
		threshold = lpm_compare( .dataa[]=usedw[], .datab[]=threshlevel[] )
					with( LPM_WIDTH=LPM_WIDTHAD )
					returns( .ageb );
	end generate;

end;


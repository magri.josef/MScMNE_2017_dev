FUNCTION a_graycounter( clock, cnt_en, clk_en, updown, aclr, sclr )
WITH( WIDTH, PVALUE, CARRY_CHAIN )
RETURNS( q[width-1..0], qbin[width-1..0] );


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity BmFifoRam is
    Port ( Din     : in  STD_LOGIC; -- From comparator
		     SClk    : in  STD_LOGIC; -- write NOT clock CLK
		     SEna_N  : in  STD_LOGIC; -- from ClockCntr -- Set End event
			  
			  WrSel_N : in  STD_LOGIC; -- write clock CLK
           WrEna_N : in  STD_LOGIC; -- clock cntrl write out flag
           
			  RdSel_N : in  STD_LOGIC; -- read clock StrIn_N
           RdEna_N : in  STD_LOGIC; -- from controller
			  
			  Clr_N 	 : in  STD_LOGIC; -- clear fifo

           Dout  	 : out STD_LOGIC_VECTOR(15 downto 0) := (others => 'Z')-- 16 channels per word
			 );
end BmFifoRam;

architecture Behavioral of BmFifoRam is
   -- originaly stated on datasheet to be (64 downto 0)
   type mem_array is array(3 downto 0) of std_logic_vector(15 downto 0);
   signal BmFifoRam : mem_array := ((others=> (others=>'0')));
   
	signal wr_ptr : unsigned(1 downto 0) := (others => '0');
   signal rd_ptr : unsigned(1 downto 0) := (others => '0');
 --  signal temp_reg : std_logic_vector(15 downto 0):= (others => '0');
  
--   signal i_full  : std_logic := '0';
 --  signal i_empty : std_logic := '1';
begin

process(RdSel_N)
	begin
		if (RdSel_N'event and RdSel_N = '0') then
          if RdEna_N = '0' then --and i_empty = '0' then
              Dout <= BmFifoRam(to_integer(rd_ptr));
						if rd_ptr /= 0 then
							rd_ptr <= rd_ptr - 1;
						end if;
           end if;
		end if;
end process;

process(SClk)--,Clr_N)
 variable temp_cnt :  integer range 0 to 16 := 0;
 variable temp_reg  : std_logic_vector(15 downto 0):= (others => '0');
    begin
--	  	 if (Clr_N = '0') then
----			 Clear BmFifoRam on Reset
--				temp_reg <= (others => '0');
--				temp_cnt <= (others => '0');
--					for i in BmFifoRam'Range loop
--						BmFifoRam(i) <= (others => '0');
--					end loop;SClk
--      if (WrSel_N'event and WrSel_N = '0') then
--            if WrEna_N = '0' then --and i_full = '0' then
		 if (SClk'event and SClk = '1') then
            if WrEna_N = '0' then --and i_full = '0' then
--           	    if temp_cnt /= "10000" then
--								temp_cnt <= temp_cnt + 1;
--								temp_reg(15 downto 1) <= temp_reg(14 downto 0);
--								temp_reg(0) <= Din;
--					elsif temp_cnt = "10000" then
--							temp_cnt <= "00000";
--							BmFifoRam(to_integer(wr_ptr+1)) <= temp_reg;
--              	end if;
					if temp_cnt = 16 then
						temp_cnt := 0;
	--					temp_reg <= (others => '0');
						BmFifoRam(to_integer(wr_ptr)) <= temp_reg;	
			 		wr_ptr <= wr_ptr+1;					
					else
						temp_reg := temp_reg(14 downto 0) & Din;
						temp_cnt := temp_cnt + 1;	
						--temp_reg(0) <= Din;		
					end if;

--				  for i in 0 to 15 loop
--						temp_reg(15 downto 1) <= temp_reg(14 downto 0);
--						temp_reg(0) <= Din;
--						tmp_reg <= temp_reg;
--					end loop;
--							temp_reg <= (others => '0');
--							BmFifoRam(to_integer(wr_ptr)) <= tmp_reg;	
--							wr_ptr <= wr_ptr+1;	

              end if;
			end if;	           	    
    end process;
end Behavioral;

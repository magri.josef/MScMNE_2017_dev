----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:09:17 11/07/2016 
-- Design Name: 
-- Module Name:    Sub - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Sub is
    Port ( S_C_Din : in  STD_LOGIC_VECTOR (17 downto 0);
           Sb      : in  STD_LOGIC_VECTOR (7 downto 0);
			  Ena     : in  STD_LOGIC;
           Result  : out  STD_LOGIC_VECTOR (17 downto 0));
end Sub;

architecture Behavioral of Sub is
begin
	Subtraction:process(Ena, S_C_Din, Sb)
			variable temp_Result_ch : STD_LOGIC_VECTOR (5 downto 0);
		begin
			if (Ena = '1') then 
				temp_Result_ch := S_C_Din(17 downto 12);
				Result <= temp_Result_ch & std_logic_vector(unsigned(S_C_Din(11 downto 0)) - unsigned(("0000" & Sb(7 downto 0))));
			else
				Result <= S_C_Din;
			end if;
		end process;
end Behavioral;


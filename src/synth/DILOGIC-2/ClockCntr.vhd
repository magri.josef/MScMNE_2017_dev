----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:52:11 11/09/2016 
-- Design Name: 
-- Module Name:    EventCntr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ClockCntr is
    Port ( clk : in  STD_LOGIC;
           Clr_N : in  STD_LOGIC;
			  Rng : in  STD_LOGIC_VECTOR(1 downto 0); 
			  EndEvent : OUT  STD_LOGIC;
           BmFifoWr_N : out  STD_LOGIC);
end  ClockCntr;

architecture Behavioral of  ClockCntr is
	signal cnt : integer range 0 to 66; --STD_LOGIC_VECTOR(7 downto 0) := (others => '0'); 
begin   
		BmFifoWr_N <= '0';--clk WHEN endtmp = '0' ELSE '0';

process(clk,cnt,Clr_N)
   begin
      if(rising_edge(clk) and cnt <= 65) then
	       cnt <= cnt + 1;--'1';
			case Rng is
				when "00" => 
					if cnt = 16 then--"00010001" then 
						EndEvent <= '1'; -- 17
					--else
					--	EndEvent <= '0';
					--cnt <= 0;
						end if;
				when "01" => 
					if cnt = 32 then--"00100001" then 
						--EndEvent <= '1'; --33
					else
						--EndEvent <= '0';
					--cnt <= 0;
					end if;
				when "10" => 
					if cnt = 48 then--= "00110001"
						EndEvent <= '1'; -- 49
						--					else
						--EndEvent <= '0';
					--cnt <= 0;
					end if;
				when "11" => 
					if cnt = 64 then --"01000001" then 
						EndEvent <= '1'; -- 65
						--					else
						--EndEvent <= '0';
					--cnt <= 0;
						end if;
				when others => EndEvent <= '0';			
			end case;
--			ASSERT ( endtmp = '0' ) -- if false report string
--				REPORT "End Event Reached" 
--				SEVERITY NOTE;		
		end if;
		
		if Clr_N = '0' then
			cnt      <= 0;
			EndEvent <= '0';
		end if;
	end process;

--process(clk)
--	begin
--		if EndEvent = '0' then 
--			BmFifoWr_N <= clk;
--		end if;

end Behavioral;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity InputMUX is
    Port ( 
           A   : in  STD_LOGIC_VECTOR (17 downto 0);
           B   : in  STD_LOGIC_VECTOR (17 downto 0);
			  SEL : in  STD_LOGIC;
           X   : out STD_LOGIC_VECTOR (17 downto 0));
end InputMUX;

architecture Behavioral of InputMUX is
begin
    X <= A when (SEL = '0') else B; -- if not end event A else B
end Behavioral;
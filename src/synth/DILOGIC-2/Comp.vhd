----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:13:20 11/07/2016 
-- Design Name: 
-- Module Name:    Comp - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Comp is
    Port ( S_C_Din : in  STD_LOGIC_VECTOR (17 downto 0);
           Th      : in  STD_LOGIC_VECTOR (7 downto 0);
           Touch   : out  STD_LOGIC;
			  Ena     : in  STD_LOGIC);
end Comp;

architecture Behavioral of Comp is
-- if subcmp is low (disabled) the amplitude are put in fifo without subtraction
-- if the value of the channel is bigger than the threshold output TOUCH '1'
-- if value is lower than the threshold output '0'
begin
	 COMPARATOR:process(Ena, S_C_Din, Th)
		begin
			if (Ena = '1') then
				if (unsigned(S_C_Din(11 downto 0)) < unsigned(("0000" & Th(7 downto 0)))) then
					Touch <= '0';
				else
					Touch <= '1';
				end if;
			else
				Touch <= '1';
			end if;
		end process;
end Behavioral;


----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    06:55:25 11/09/2016 
-- Design Name: 
-- Module Name:    TOMem - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TOMem is
	Generic (
			DATA_WIDTH		: integer := 18;
			ADDRESS_WIDTH	: integer := 6 );
	Port (	-- Front section
			RdA 				: in  STD_LOGIC_VECTOR (ADDRESS_WIDTH - 1 downto 0);  -- Channel Address and Memory Map
			RdSel 			: in  STD_LOGIC; 							  -- CLK - read clock
			RdClr_N 			: in  STD_LOGIC;							  -- Clr_N -- ?? Clear Full Memory ??
			-- Back section
			CD 				: in  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0); -- Data input Th + Sub
			CSel_N 			: in  STD_LOGIC;							  -- write clock -- StrIN_N
			CRd_N 			: in  STD_LOGIC;							  -- Memory read enable
			CWr_N				: in  STD_LOGIC;							  -- Memory write enable
			CClr_N 			: in  STD_LOGIC;							  -- ?? Clear Full Memory ??

			RdQ 				: out  STD_LOGIC_VECTOR (15 downto 0) := (others => '0');-- to Subtractor and Comparator
			CQ 				: out  STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);--:= (others => 'Z');-- to databus --- read back stored data
			Full 				: out  STD_LOGIC;	-- Memory full flag
			EMPTY				: out  STD_LOGIC ); -- Memory empty flag
end TOMem;

architecture Behavioral of TOMem is
	type Memory_Array is array ((2 ** ADDRESS_WIDTH) - 1 downto 0) of STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0);
	signal ToMemRam : Memory_Array := ((others=> (others=>'0')));
	signal temp		: std_logic_vector(0 to ADDRESS_WIDTH):= (others => '0');
	signal i_full	: std_logic;
	signal i_empty	: std_logic;
	signal wr_ptr	: unsigned((2 ** ADDRESS_WIDTH) - 1 downto 0) := (others => '0');
	signal rd_ptr	: unsigned((2 ** ADDRESS_WIDTH) - 1 downto 0) := (others => '0');
begin
--	Full  <= '1' WHEN (Memory((2 ** ADDRESS_WIDTH) - 1) /= (Memory'Range => '0')) ELSE '0';
--	EMPTY <= '1' WHEN (Memory((2 ** ADDRESS_WIDTH) - 1) = (Memory'REVERSE_RANGE => '0')) ELSE '0';
	Full  <= i_full;
	EMPTY <= i_empty;

flag_proc: process(wr_ptr, rd_ptr)
	begin
		if rd_ptr+1 = "1000000" then 
			i_empty <= '1';
		else
			i_empty <= '0';
		end if;
--elsif (rd_ptr = event_ptr) and (event_ptr /= (event_ptr'range => '0')) then 
        --if wr_ptr+1 = rd_ptr then
		if wr_ptr+1 = "1000000" then 
			i_full <= '1';
		else
			i_full <= '0';
		end if;
	end process;

ToMemRead:process (RdSel)	-- Read process -- send threshold and pedastals 
	variable temp_RdQ : STD_LOGIC_VECTOR (17 downto 0) := (others => '0');
	begin
		if rising_edge(RdSel) then
				if CRd_N = '1' then
					temp_RdQ := ToMemRam(to_integer(unsigned(RdA)));
					RdQ <= temp_RdQ(15 downto 0);
				end if;
		end if;
	end process;

ToMemWrite:process (CSel_N,RdClr_N,CClr_N)	-- Write Threshold and pedistals with channel address as the memory address
	begin
		if ((RdClr_N = '0') or (CClr_N = '0')) then	-- Clear Memory on Reset
				for i in ToMemRam'Range loop
					ToMemRam(i) <= (others => '0');
				end loop;
		elsif rising_edge(CSel_N) then
			if CRd_N = '0' then	-- read enabled from controller Configure Write function
				if i_full = '0' then	-- Store DataIn to Current Memory Address
					ToMemRam(to_integer(unsigned(RdA))) <= CD;
					wr_ptr <= wr_ptr + 1;
				end if;
			elsif CWr_N = '0' then
				if i_empty = '0' then	-- If WriteEn then pass through DIn
					CQ <= ToMemRam(to_integer(unsigned(RdA)));
					rd_ptr <= rd_ptr + 1;
				end if;
			end if;
		end if;
	end process;
end Behavioral;


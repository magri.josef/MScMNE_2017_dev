----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:53:07 11/09/2016 
-- Design Name: 
-- Module Name:    AWordCntr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity AWordCntr is
    Port ( Clk : in  STD_LOGIC;
           Ena : in  STD_LOGIC;
           Clr_N : in  STD_LOGIC;
           Q : out  STD_LOGIC_VECTOR (6 downto 0));
end AWordCntr;

architecture Behavioral of AWordCntr is
   signal temp: std_logic_vector(0 to 6);
begin   
process(clk,Clr_N,Ena)
   begin
      if Clr_N='0' then
         temp <= (others => '0');-- "0000000";
      elsif(falling_edge(clk) and Ena = '1') then
	       temp <= temp + 1;
		end if;
   end process;
   Q <= temp;
end Behavioral;


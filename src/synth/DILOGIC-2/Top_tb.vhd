-- Vhdl test bench created from schematic /home/josef/Documents/RCB_hltb/DILOGIC/DILOGIC-1/Top.sch - Mon Nov 14 06:55:55 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
-------------------------------------------------------------------------------
-- IEEE standard libraries

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;


---------------------------------------

-- file I/O libraries

library std;
use std.textio.all;

---------------------------------------

-- test bench packages

library WORK;

	-- random generator

use     work.MT19937AR.SEED_VECTOR;
use     work.MT19937AR.PSEUDO_RANDOM_NUMBER_GENERATOR_TYPE;
use     work.MT19937AR.NEW_PSEUDO_RANDOM_NUMBER_GENERATOR;
use     work.MT19937AR.GENERATE_RANDOM_STD_LOGIC_VECTOR;
use     work.MT19937AR.GENERATE_RANDOM_REAL2;

-------------------------------------------------------------------------------
ENTITY Top_Top_sch_tb IS
END Top_Top_sch_tb;
ARCHITECTURE behavioral OF Top_Top_sch_tb IS 

   COMPONENT TOP_DILO IS
   PORT( StrIn_N	:	IN	STD_LOGIC; --
          CLK	:	IN	STD_LOGIC; --
          Clr_N	:	IN	STD_LOGIC; --
          EnaIn_N	:	IN	STD_LOGIC; 
          Fcode	:	IN	STD_LOGIC_VECTOR (3 DOWNTO 0); --
          NrofGx	:	IN	STD_LOGIC_VECTOR (1 DOWNTO 0); --
          Rst_N	:	IN	STD_LOGIC; --
          --D_IN	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          DFifoEmpty_N	:	OUT	STD_LOGIC; 
          NoAData_N	:	OUT	STD_LOGIC; 
          Trg	:	IN	STD_LOGIC; 
          Sub_Cmp	:	IN	STD_LOGIC; 
          AC_in	:	IN	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          MAck_N	:	OUT	STD_LOGIC; -- 
          EnaOut_N	:	OUT	STD_LOGIC;
          D	:	INOUT	STD_LOGIC_VECTOR (15 DOWNTO 0) 
          --Din	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0)
			 );
   END COMPONENT;

   SIGNAL StrIn_N	:	STD_LOGIC;
   SIGNAL CLK	:	STD_LOGIC:='0';
   SIGNAL Clr_N	:	STD_LOGIC;
   SIGNAL EnaIn_N	:	STD_LOGIC;
   SIGNAL Fcode	:	STD_LOGIC_VECTOR (3 DOWNTO 0);
   SIGNAL NrofGx	:	STD_LOGIC_VECTOR (1 DOWNTO 0);
   SIGNAL Rst_N	:	STD_LOGIC;
   --SIGNAL D_IN	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL DFifoEmpty_N	:	STD_LOGIC;
   SIGNAL NoAData_N	:	STD_LOGIC;
   SIGNAL Trg	:	STD_LOGIC:='0';
   SIGNAL Sub_Cmp	:	STD_LOGIC;
   SIGNAL AC_in	:	STD_LOGIC_VECTOR (15 DOWNTO 0) := (others => '0');
   SIGNAL MAck_N	:	STD_LOGIC;
   SIGNAL EnaOut_N	:	STD_LOGIC;
   SIGNAL D	:	STD_LOGIC_VECTOR (15 DOWNTO 0); --Din
   --SIGNAL Din	:	STD_LOGIC_VECTOR (15 DOWNTO 0); -- D

-- Simulation Declerations
	signal config_WR				: std_logic:='0'; 
	signal config_WR_done			: std_logic:='0'; 
	signal config_WR_StrIn_N: std_logic:='0'; 
	signal config_WR_StrIn_N_done: std_logic:='0'; 
	
	signal config_RD				: std_logic:='0'; 
	signal config_RD_done			: std_logic:='0'; 
	signal config_RD_StrIn_N: std_logic:='0'; 
	signal config_RD_StrIn_N_done: std_logic:='0'; 
	
	signal config_RST_StrIn_N: std_logic:='0'; 
	signal config_RST_StrIn_N_done: std_logic:='0'; 
	
	signal AN_RD				: std_logic:='0'; 
	signal AN_RD_done			: std_logic:='0'; 
	signal AN_RD_StrIn_N: std_logic:='0'; 
	signal AN_RD_StrIn_N_done: std_logic:='0'; 
	
 	signal RST_Chain				: std_logic:='0'; 
	signal RST_Chain_done			: std_logic:='0'; 
	signal RST_Fifo				: std_logic:='0'; 
	signal Rand_data_gen			: std_logic:='0'; 
	signal Rand_data_gen_done		: std_logic:='0'; 
   CONSTANT clk_period : time := 25 ns;
 signal j : integer;
 
 	--  CONVERT HEX TO STD_LOGIC_VECTOR
	--	COPY FROM Csaba. SOOS
	---------------------------------------------------------------------------
--	function str2slv (str : string) return std_logic_vector ;
 
BEGIN

   UUT: TOP_DILO PORT MAP(
		StrIn_N => StrIn_N, 
		CLK => CLK, 
		Clr_N => Clr_N, 
		EnaIn_N => EnaIn_N, 
		Fcode => Fcode, 
		NrofGx => NrofGx, 
		Rst_N => Rst_N, 
		--D_IN => D_IN, 
		DFifoEmpty_N => DFifoEmpty_N, 
		NoAData_N => NoAData_N, 
		Trg => Trg, 
		Sub_Cmp => Sub_Cmp, 
		AC_in => AC_in, 
		MAck_N => MAck_N, 
		EnaOut_N => EnaOut_N, 
		D => D 
		--Din => Din
   );

--Start_RST:process
--	begin
--	-- Reset Front-end
--		Clr_N <= '0';
--		WAIT FOR clk_period*5;
--		Clr_N <= '1';
--		WAIT FOR clk_period*5;
--		
--	-- Reset Back-end
--		Rst_N <= '0';
--		WAIT FOR clk_period*5;
--		Rst_N <= '1';
--		WAIT FOR clk_period*5;
--		
--		wait; -- wait forever
-- end process;
 

Sim_control:process 
	---------------------------------------------------------------------------
	--  CONVERT HEX TO STD_LOGIC_VECTOR
	--	COPY FROM Csaba. SOOS
	---------------------------------------------------------------------------
	function str2slv (str : string) return std_logic_vector is
      subtype nib is std_logic_vector ( 3 downto 0);
      type    nib_table is array (0 to 15) of nib;
      constant nib_tbl : nib_table := ("0000", "0001", "0010", "0011",
                                       "0100", "0101", "0110", "0111",
                                       "1000", "1001", "1010", "1011",
                                       "1100", "1101", "1110", "1111");
      variable slv    : std_logic_vector ((str'length*4)-1 downto 0);
      variable pos1   : integer;
      variable pos_0  : integer;
      variable pos_9  : integer;
      variable pos_A  : integer;
      variable pos_F  : integer;
      variable pos_sa : integer;
      variable pos_sf : integer;
      variable b_good_number : boolean;
      variable b_good_letter1 : boolean;
      variable b_good_letter2 : boolean;
      variable b_good_char : boolean;
      variable j : integer;
    begin  -- str2slv
      slv := (others => '0');
      for i in str'range loop
        pos1   := character'pos(str(i));
        pos_0  := character'pos('0');
        pos_9  := character'pos('9');
        pos_A  := character'pos('A');
        pos_F  := character'pos('F');
        pos_sa := character'pos('a');
        pos_sf := character'pos('f');
        b_good_number  := (pos1 >= pos_0  and pos1 <= pos_9 );
        b_good_letter1 := (pos1 >= pos_A  and pos1 <= pos_F );
        b_good_letter2 := (pos1 >= pos_sa and pos1 <= pos_sf);
        b_good_char := b_good_number or b_good_letter1 or b_good_letter2;
        assert b_good_char report "WRONG NUMBER" severity FAILURE;
        if b_good_number then
          pos1 := pos1 - pos_0;
        elsif b_good_letter1 then
          pos1 := pos1 - pos_A + 10;
        elsif b_good_letter2 then
          pos1 := pos1 - pos_sa + 10;
        end if;
        slv(((str'length*4) - (i-1)*4 - 1) downto
            ((str'length*4) - (i-1)*4 - 4)) := nib_tbl(pos1);
      end loop;  -- i
      return slv;
    end str2slv;
-- ------------------------------------------------------------------------
--	| Function code	| Description 					 | From Column 
--	|-----------------------------------------------------------------------	
--	| 0000 				| Front-end test mode 		 |
--	| 0001 				| Load "almost full" preset |
--	| 0010...0111 		| No operation 				 | Idle(0011) Data_write(0110) Write FIFO Cylce from controller
-- |						|									 | Data_read(0100) Read FIFO Cylce from controller
--	| 1000 				| Pattern readout           |
--	| 1001 				| Pattern delete 				 |
--	| 1010 				| Analog readout 				 | Ped_MEAS Measurement of the Pedestals
--	| 1011 				| Analog delete 				 |
--	| 1100 				| Reset fifo pointers 		 |
--	| 1101 				| Reset daisy chain 			 |
--	| 1110 				| Configuration write 		 | Load_Ped Loading of pedestals
--	| 1111				| Configuration read 		 | READ_PED
-- -----------------------------------------------
	file     input_file   : text open READ_MODE is "rand.txt";
   variable input_line   : LINE;
	file     input_file_ch_wr   : text open READ_MODE is "channel.txt";
   variable input_line_ch_wr   : LINE;
	file     input_file_ch_rd   : text open READ_MODE is "channel.txt";
   variable input_line_ch_rd   : LINE;
	file     input_file_ch_Awr   : text open READ_MODE is "channel.txt";
	variable input_line_ch_Awr   : LINE;
	file     input_file_Awr   : text open READ_MODE is "rand.txt";
   variable input_line_Awr   : LINE;
	variable str_4 		 : string(1 to 4);	-- use to read 4 charactors from the line
	variable str_4_wr 		 : string(1 to 4);	-- use to read 4 charactors from the line
	variable str_4_rd 		 : string(1 to 4);	-- use to read 4 charactors from the line
	variable shift			 : std_logic_vector(15 downto 0) := (others => '0');
	variable cnt 			 : std_logic_vector(5 downto 0) := "000000";
begin
-----------------------------
--Data Acquisition
-----------------------------
WAIT FOR clk_period*5;
	--Fcode <= "1010";
	--Data_A <= '1';
	--EnaIn_N <= '0';
-- before acquisition reset front and back
	-- Reset Front-end
		Clr_N <= '0';
		WAIT FOR clk_period*5;
		Clr_N <= '1';
		WAIT FOR clk_period*5;
		
	-- Reset Back-end
		Rst_N <= '0';
		WAIT FOR clk_period*5;
		Rst_N <= '1';
		WAIT FOR clk_period*5;
-----------------------------
-- now do config write to load Threshold and Pedestals
-----------------------------
--D. Configure Write
-----------------------------
	WAIT FOR clk_period*5;
	Fcode <= "1110";
	config_WR <= '1';
	EnaIn_N <= '0';
	WAIT until config_WR_StrIn_N = '1';
		j <= 0;
    FOR j in 64 DOWNTO 0 LOOP
		StrIn_N <= '0';
		readline(input_file_ch_wr, input_line_ch_wr);    
		read(input_line_ch_wr, str_4_wr);
		shift := str2slv(str_4_wr);
		AC_in(15 downto 10) <= shift(5 downto 0);
		WAIT FOR clk_period*5;
		readline(input_file, input_line);    
		read(input_line, str_4);
		D <= str2slv(str_4);
		StrIn_N <= '1';
		WAIT FOR clk_period*5;
    END LOOP;
	 shift :=(others => '0');
	 config_WR_StrIn_N_done <= '1';
	WAIT until config_WR_done = '1';
-----------------------------
--G. Reset Daisy Chain - part of Configure Write procedure
-----------------------------
	Fcode <= "1101"; -- Reset Daisy Chain
	RST_Chain <= '1'; -- check if reset was done correctly
	WAIT until config_RST_StrIn_N = '1';
	StrIn_N <= '0';
	WAIT FOR clk_period*5;
	StrIn_N <= '1';
	config_RST_StrIn_N_done <= '1';
	WAIT until RST_Chain_done = '1';
	RST_Chain <= '0';
	config_RST_StrIn_N_done <= '0';
	EnaIn_N <= '1';
-----------------------------
-- Data_read(0100) Read FIFO Cylce from controller
-----------------------------
Fcode <= "0100";
-- Sub_Cmp Set
Sub_Cmp <= '0';   -- low  (disabled) AmpL and ChAddr ara stored without subtraction of the offset
--Sub_Cmp <= '1'; -- High (enabled)  Stored only if amplitude is bigger than the threshold.
-----------------------------
-- Set Number of channels
-- 	NrofGX 	-- Nb. of input data -- Nb. of clocks --
--NrofGx <= "00";-- 16						-- 17
--NrofGx <= "01";-- 32						-- 33
--NrofGx <= "10";-- 48						-- 49
NrofGx <= "11";  -- 64						-- 65
-----------------------------
-- Start acquisition
	WAIT FOR clk_period*5;
	Trg <= '1';
	WAIT FOR clk_period*5;
	Trg <= '0';
	
-- 
-- channel + data
	j <= 0;
    FOR j in 64 DOWNTO 0 LOOP -- channels
		CLK <= '1';
		readline(input_file_ch_Awr, input_line_ch_Awr);    
		read(input_line_ch_Awr, str_4_wr);
		shift := str2slv(str_4_wr);
		AC_in(15 downto 10) <= shift(5 downto 0);
		WAIT FOR clk_period*5;
		readline(input_file_Awr, input_line_Awr);    
		read(input_line, str_4);
		AC_in(9 downto 0) <= str2slv(str_4)(9 downto 0);
		CLK <= '0';
		WAIT FOR clk_period*5;
    END LOOP;
	 shift :=(others => '0');
-- 1 extra clock for end event
	CLK <= '1';
	WAIT FOR clk_period*5;
	CLK <= '0';
-----------------------------
-- Check end event process
-----------------------------
--E. Configure Read
-----------------------------
	WAIT FOR clk_period*5;
	Fcode <= "1111";
	config_RD <= '1';
	EnaIn_N <= '0';
	WAIT until config_RD_StrIn_N = '1';
		j <= 0;
    FOR j in 64 DOWNTO 0 LOOP
		StrIn_N <= '0';
		readline(input_file_ch_rd, input_line_ch_rd);    
		read(input_line_ch_rd, str_4_rd);
		shift := str2slv(str_4_rd);
		AC_in(15 downto 10) <= shift(5 downto 0);
		WAIT FOR clk_period*5;   
		StrIn_N <= '1';
		WAIT FOR clk_period*5;
    END LOOP;
	 shift :=(others => '0');
	 config_RD_StrIn_N_done <= '1';
	WAIT until config_RD_done = '1';
-----------------------------
--G. Reset Daisy Chain
-----------------------------
	Fcode <= "1101"; -- Reset Daisy Chain
	RST_Chain <= '1'; -- check if reset was done correctly
	WAIT until config_RST_StrIn_N = '1';
	StrIn_N <= '0';
	WAIT FOR clk_period*5;
	StrIn_N <= '1';
	config_RST_StrIn_N_done <= '1';
	WAIT until RST_Chain_done = '1';
	RST_Chain <= '0';
	config_RST_StrIn_N_done <= '0';
	EnaIn_N <= '1';
-----------------------------
-- Delay for simulation
--WAIT FOR clk_period*25;
----------------------------- 
-- B. Analog Readout	
	Fcode <= "1010";
	AN_RD <= '1';
	EnaIn_N <= '0';
	WAIT until AN_RD_StrIn_N = '1';
		j <= 0;
    FOR j in 64 DOWNTO 0 LOOP
		StrIn_N <= '0';
		WAIT FOR clk_period*5;   
		StrIn_N <= '1';
		WAIT FOR clk_period*5;
    END LOOP;
	 AN_RD_StrIn_N_done <= '1';
	WAIT until AN_RD_done = '1';
-- check if simulation was sucessfull
-- if finished and EnOut_N low

-----------------------------
--G. Reset Daisy Chain
-----------------------------
	Fcode <= "1101"; -- Reset Daisy Chain
	RST_Chain <= '1'; -- check if reset was done correctly
	WAIT until config_RST_StrIn_N = '1';
	StrIn_N <= '0';
	WAIT FOR clk_period*5;
	StrIn_N <= '1';
	config_RST_StrIn_N_done <= '1';
	WAIT until RST_Chain_done = '1';
	RST_Chain <= '0';
	config_RST_StrIn_N_done <= '0';
	EnaIn_N <= '1';
end process;
 
 AN_Read:process
	
begin
	   wait until AN_RD = '1';
		
	-- Set EnIn_N
		
		WAIT FOR clk_period*5;
		
	-- StrIn_N 64 cycles for each Dilogic
		AN_RD_StrIn_N <= '1';
		
		wait until AN_RD_StrIn_N_done = '1';
	 
	--	Dilogic should set enable outEna
	WAIT FOR clk_period*5;
		ASSERT ( EnaOut_N = '1' ) -- if false report string
			REPORT " Next dilogic will be enabled "
			SEVERITY NOTE;
			
	AN_RD_done <= '1';
	
   END PROCESS;
 
Configuration_Write:process

	------------------------------------------------
	-- D. Configuration Write (1110)
	--    Loading of Threshold and offset Memory
	--
		-- > Front-End Reset ( For sim )
		-- > Back-End Reset ( For sim )
		-- > Set Fcode (1110)
		-- > EnIn_N set low
		-- > StrIn_N cycles first 64 cycles to write first Dilogic
		-- > !! Dilogic should set enable out !! Next Dilogic is loaded
		-- > reset daisy chain applied at end of operation
	------------------------------------------------

	
begin
	   wait until config_WR = '1';
	-- generate a file with random data
		Rand_data_gen <= '1';
		wait until Rand_data_gen_done = '1';
				
	-- Set Fcode
--		temp_fcode := "1110";
--		tempcode <= '1';
--		WAIT FOR clk_period*5;
		
	-- Set EnIn_N
		--EnaIn_N <= '0';
		WAIT FOR clk_period*5;
		
	-- StrIn_N 64 cycles for each Dilogic
		config_WR_StrIn_N <= '1';
		
--	j <= 0;
--    FOR j in 63 DOWNTO 0 LOOP
--		StrIn_N <= '0';
--		WAIT FOR clk_period*5;
--		readline(input_file, input_line);    
--		read(input_line, str_4);
--		D <= str2slv(str_4);
--		StrIn_N <= '1';
--		WAIT FOR clk_period*5;
--    END LOOP;
		wait until config_WR_StrIn_N_done = '1';
	 
	--	Dilogic should set enable outEna
	WAIT FOR clk_period*5;
		ASSERT ( EnaOut_N = '1' ) -- if false report string
			REPORT " Next dilogic will be enabled "
			SEVERITY NOTE;
			
	config_WR_done <= '1';
	wait;
		
	-- reset daisy chain
	--WAIT FOR clk_period*5;
	--	RST_Chain <= '1';	
	--StrIn_N <= '0';
	--	WAIT FOR clk_period*5;
	--StrIn_N <= '1';
	--	WAIT FOR clk_period*5;
   --   WAIT; -- will wait forever
   END PROCESS;

Configuration_Read:process

	------------------------------------------------
	-- D. Configuration Write (1110)
	--    Loading of Threshold and offset Memory
	--
		-- > Set Fcode (1111)
		-- > EnIn_N set low
		-- > StrIn_N cycles first 64 cycles to write first Dilogic
		-- > !! Dilogic should set enable out !! Next Dilogic is loaded
		-- > reset daisy chain applied at end of operation
	------------------------------------------------

	
begin
	   wait until config_RD = '1';
		
	-- Set EnIn_N
		
		WAIT FOR clk_period*5;
		
	-- StrIn_N 64 cycles for each Dilogic
		config_RD_StrIn_N <= '1';
		
		wait until config_RD_StrIn_N_done = '1';
	 
	--	Dilogic should set enable outEna
	WAIT FOR clk_period*5;
		ASSERT ( EnaOut_N = '1' ) -- if false report string
			REPORT " Next dilogic will be enabled "
			SEVERITY NOTE;
			
	config_RD_done <= '1';
	
   END PROCESS;

Reset_Fifo_Pointer: process
	------------------------------------------------
	-- F. Reset Fifo Pointer ( 1100 )	Memory Clear
	--
	--    Address counters in data fifo and 
	--		bitmap fifo memories are reset after
	--		StrIn_N cycle is given
	--
	------------------------------------------------
	begin
		wait until RST_Fifo = '1';
			-- WAIT FOR clk_period*5;
				--ASSERT ( EnaOut_N = '1' ) -- if false report string
					REPORT " Memory Cleared "
					SEVERITY NOTE;				
	end process;

Reset_Daisy_Chain : process
	------------------------------------------------
	-- G. Reset Daisy Chain ( 1101 or 0xxx )
	--
	--    Dilogic will set the state machine that 
	--			contols back-end to start state
	--
	--		Used at end of: 
	--			> Pattern Readout
		--			> Analog Readout
			--			> Configure Write
				--			> Configure Read
	--
	------------------------------------------------
	begin
		wait until RST_Chain = '1';
		RST_Chain_done <= '0';
	--temp_fcode <= "1101";
	--	tempcode <= '1';		
	-- Check if reset was Successful or Not
		config_RST_StrIn_N <= '1';
		wait until config_RST_StrIn_N_done = '1';
		WAIT FOR clk_period*5;
		ASSERT (( EnaOut_N = '0' )AND  ( MAck_N = 'Z' ))-- if false report string
			REPORT " Dilogic Daisy Chain Reset Successful "
			SEVERITY NOTE;
		ASSERT ( EnaOut_N = '1' ) -- if false report string
			REPORT " Dilogic Daisy Chain Reset FAILED !! "
			SEVERITY NOTE;			
		 config_RST_StrIn_N <= '0';
		 --config_RST_StrIn_N_done <= '0';
       RST_Chain_done <= '1';
	end process;
	
Random_Data_Generator:process
        ---------------------------------------------------------------------------
        -- unsigned to decimal string.
        ---------------------------------------------------------------------------
        function TO_DEC_STRING(arg:unsigned;len:integer;space:character) return STRING is
            variable str   : STRING(len downto 1);
            variable value : unsigned(arg'length-1 downto 0);
        begin
            value  := arg;
            for i in str'right to str'left loop
                if (value > 0) then
                    case (to_integer(value mod 10)) is
                        when 0      => str(i) := '0';
                        when 1      => str(i) := '1';
                        when 2      => str(i) := '2';
                        when 3      => str(i) := '3';
                        when 4      => str(i) := '4';
                        when 5      => str(i) := '5';
                        when 6      => str(i) := '6';
                        when 7      => str(i) := '7';
                        when 8      => str(i) := '8';
                        when 9      => str(i) := '9';
                        when others => str(i) := 'X';
                    end case;
                else
                    if (i = str'right) then
                        str(i) := '0';
                    else
                        str(i) := space;
                    end if;
                end if;
                value := value / 10;
            end loop;
            return str;
        end function;
        ---------------------------------------------------------------------------
        -- unsigned to decimal string
        ---------------------------------------------------------------------------
        function TO_DEC_STRING(arg:unsigned;len:integer) return STRING is
        begin
            return  TO_DEC_STRING(arg,len,' ');
        end function;
        ---------------------------------------------------------------------------
        -- real number to decimal string.
        ---------------------------------------------------------------------------
        function TO_DEC_STRING(arg:real;len1,len2:integer) return STRING is
            variable str   : STRING(len2-1 downto 0);
            variable i,n,p : integer;
        begin
            i := integer(arg);
            if    real(i) = arg then
                n := i;
            elsif i > 0 and real(i) > arg then
                n := i-1;  
            elsif i < 0 and real(i) < arg then
                n := i+1;
            else  
                n := i;
            end if;
            p := integer((arg-real(n))*(10.0**len2));
            return  TO_DEC_STRING(to_unsigned(n,len1-len2-1),len1-len2-1,' ') & "." & 
                    TO_DEC_STRING(to_unsigned(p,32),len2,'0');
        end function;
        ---------------------------------------------------------------------------
        -- Seed Numbers for Pseudo Random Number Generator.
        ---------------------------------------------------------------------------
        variable seed      : SEED_VECTOR(0 to 3) := (0 => X"00000123",
                                                     1 => X"00000234",
                                                     2 => X"00000345",
                                                     3 => X"00000456");
        ---------------------------------------------------------------------------
        -- Pseudo Random Number Generator Instance.
        ---------------------------------------------------------------------------
        variable prng      : PSEUDO_RANDOM_NUMBER_GENERATOR_TYPE := NEW_PSEUDO_RANDOM_NUMBER_GENERATOR(seed);
        ---------------------------------------------------------------------------
        -- Random number 
        ---------------------------------------------------------------------------
        variable vec       : std_logic_vector(31 downto 0);
        variable num       : real;
        ---------------------------------------------------------------------------
        -- for display
        ---------------------------------------------------------------------------
        constant TAG       : STRING(1 to 1) := " ";
        constant SPACE     : STRING(1 to 1) := " ";
		  file     OUTPUT : text open WRITE_MODE is "rand.txt";
        variable text_line : LINE;
    begin
			
			wait until Rand_data_gen = '1';
        --WRITE(text_line, TAG & "1000 outputs of genrand_int32()");
        --WRITELINE(OUTPUT, text_line);
        for i in 0 to 999 loop
           GENERATE_RANDOM_STD_LOGIC_VECTOR(prng,vec);
           WRITE(text_line, TO_DEC_STRING(unsigned(vec),6));
			  WRITELINE(OUTPUT, text_line);--
          --  WRITE(text_line, SPACE);
         -- if (i mod 5 = 4) then
         --       WRITELINE(OUTPUT, text_line);
         --   end if;
        end loop;
        WRITELINE(OUTPUT, text_line);
		  file_close(OUTPUT);

       -- assert(false) report TAG & " Run complete..." severity FAILURE;
        Rand_data_gen_done <= '1';
    end process;

END;

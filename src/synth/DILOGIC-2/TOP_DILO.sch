<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan6" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_12" />
        <signal name="XLXN_9" />
        <signal name="XLXN_11" />
        <signal name="B(17:7)" />
        <signal name="B(6:0)" />
        <signal name="B(17:0)" />
        <signal name="XLXN_16" />
        <signal name="StrIn_N" />
        <signal name="XLXN_21" />
        <signal name="CLK" />
        <signal name="Clr_N" />
        <signal name="EnaIn_N" />
        <signal name="Fcode(3:0)" />
        <signal name="NrofGx(1:0)" />
        <signal name="Rst_N" />
        <signal name="NoAData_N" />
        <signal name="Trg" />
        <signal name="OS_TH(15:0)" />
        <signal name="OS_TH(7:0)" />
        <signal name="OS_TH(15:8)" />
        <signal name="Sub_Cmp" />
        <signal name="XLXN_163" />
        <signal name="XLXN_187" />
        <signal name="XLXN_198(15:0)" />
        <signal name="Din(17:0)" />
        <signal name="ChAddr(5:0),AC_in(11:0)" />
        <signal name="XLXN_213(17:0)" />
        <signal name="XLXN_214(17:0)" />
        <signal name="XLXN_216(17:0)" />
        <signal name="XLXN_217(17:0)" />
        <signal name="XLXN_218(17:0)" />
        <signal name="ChAddr(5:0)" />
        <signal name="EnaOut_N" />
        <signal name="MAck_N" />
        <signal name="XLXN_234" />
        <signal name="XLXN_235(1:0)" />
        <signal name="XLXN_236" />
        <signal name="XLXN_237" />
        <signal name="XLXN_238" />
        <signal name="XLXN_239" />
        <signal name="XLXN_240" />
        <signal name="XLXN_241" />
        <signal name="XLXN_242" />
        <signal name="DFifoEmpty_N" />
        <signal name="XLXN_245(15:0)" />
        <signal name="XLXN_246(17:0)" />
        <signal name="XLXN_247(1:0)" />
        <signal name="XLXN_248(15:0)" />
        <signal name="XLXN_249(17:0)" />
        <signal name="XLXN_250(1:0)" />
        <signal name="XLXN_251(15:0)" />
        <signal name="XLXN_252(17:0)" />
        <signal name="XLXN_253(1:0)" />
        <signal name="XLXN_254" />
        <signal name="XLXN_243" />
        <signal name="D(17:0)" />
        <signal name="XLXN_258(17:0)" />
        <signal name="XLXN_259(1:0)" />
        <signal name="XLXN_260(15:0)" />
        <signal name="XLXN_265" />
        <signal name="AC_in(11:0)" />
        <port polarity="Input" name="StrIn_N" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="Clr_N" />
        <port polarity="Input" name="EnaIn_N" />
        <port polarity="Input" name="Fcode(3:0)" />
        <port polarity="Input" name="NrofGx(1:0)" />
        <port polarity="Input" name="Rst_N" />
        <port polarity="Output" name="NoAData_N" />
        <port polarity="Input" name="Trg" />
        <port polarity="Input" name="Sub_Cmp" />
        <port polarity="Input" name="ChAddr(5:0)" />
        <port polarity="Output" name="EnaOut_N" />
        <port polarity="Output" name="MAck_N" />
        <port polarity="Output" name="DFifoEmpty_N" />
        <port polarity="BiDirectional" name="D(17:0)" />
        <port polarity="Input" name="AC_in(11:0)" />
        <blockdef name="AWordCntr">
            <timestamp>2016-11-9T16:10:44</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <blockdef name="ClockCntr">
            <timestamp>2016-11-9T16:11:8</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="nor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="BmFifoRam">
            <timestamp>2016-11-9T17:58:32</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="0" y1="224" y2="224" x1="64" />
            <line x2="0" y1="288" y2="288" x1="64" />
            <line x2="0" y1="352" y2="352" x1="64" />
            <line x2="0" y1="416" y2="416" x1="64" />
            <line x2="0" y1="480" y2="480" x1="64" />
            <rect width="256" x="64" y="-4" height="516" />
            <rect width="64" x="320" y="20" height="24" />
            <line x2="384" y1="32" y2="32" x1="320" />
        </blockdef>
        <blockdef name="DFifoRam">
            <timestamp>2017-7-15T11:37:41</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="96" y2="96" x1="64" />
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="0" y1="224" y2="224" x1="64" />
            <line x2="0" y1="288" y2="288" x1="64" />
            <line x2="0" y1="352" y2="352" x1="64" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <line x2="384" y1="96" y2="96" x1="320" />
            <line x2="384" y1="160" y2="160" x1="320" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-64" height="448" />
        </blockdef>
        <blockdef name="EventCntr">
            <timestamp>2017-7-15T11:42:51</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="InputMUX">
            <timestamp>2017-7-15T11:42:39</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
        </blockdef>
        <blockdef name="Sub">
            <timestamp>2017-7-15T11:53:3</timestamp>
            <rect width="304" x="64" y="-192" height="192" />
            <rect width="64" x="368" y="-172" height="24" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
        </blockdef>
        <blockdef name="Comp">
            <timestamp>2017-7-15T11:55:48</timestamp>
            <rect width="304" x="48" y="-192" height="192" />
            <rect width="64" x="-16" y="-172" height="24" />
            <line x2="-16" y1="-160" y2="-160" x1="48" />
            <rect width="64" x="-16" y="-108" height="24" />
            <line x2="-16" y1="-96" y2="-96" x1="48" />
            <line x2="-16" y1="-32" y2="-32" x1="48" />
            <line x2="416" y1="-160" y2="-160" x1="352" />
        </blockdef>
        <blockdef name="InputRegister">
            <timestamp>2017-7-15T11:58:28</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
        </blockdef>
        <blockdef name="TOMem">
            <timestamp>2017-7-15T12:22:48</timestamp>
            <rect width="256" x="64" y="-512" height="512" />
            <rect width="64" x="320" y="-492" height="24" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-432" y2="-432" x1="64" />
            <line x2="0" y1="-384" y2="-384" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="0" y="-316" height="24" />
            <line x2="0" y1="-304" y2="-304" x1="64" />
            <rect width="64" x="320" y="-220" height="24" />
            <line x2="384" y1="-208" y2="-208" x1="320" />
            <line x2="320" y1="-108" y2="-108" x1="380" />
            <line x2="320" y1="-48" y2="-48" x1="380" />
        </blockdef>
        <blockdef name="IO_BUF_BUS">
            <timestamp>2017-8-17T9:40:21</timestamp>
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-128" height="328" />
            <rect width="64" x="0" y="116" height="24" />
            <line x2="0" y1="128" y2="128" x1="64" />
            <rect width="64" x="0" y="-76" height="24" />
            <line x2="0" y1="-64" y2="-64" x1="64" />
            <rect width="64" x="0" y="-12" height="24" />
            <line x2="0" y1="0" y2="0" x1="64" />
            <rect width="64" x="0" y="52" height="24" />
            <line x2="0" y1="64" y2="64" x1="64" />
            <line x2="0" y1="176" y2="176" x1="64" />
        </blockdef>
        <blockdef name="Controller">
            <timestamp>2017-8-17T9:50:14</timestamp>
            <rect width="304" x="64" y="-640" height="600" />
            <line x2="432" y1="-608" y2="-608" x1="368" />
            <line x2="368" y1="-544" y2="-544" x1="432" />
            <line x2="432" y1="-480" y2="-480" x1="368" />
            <line x2="0" y1="-576" y2="-576" x1="64" />
            <line x2="0" y1="-512" y2="-512" x1="64" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-384" y2="-384" x1="0" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="0" y1="-320" y2="-320" x1="64" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="0" y1="-256" y2="-256" x1="64" />
            <line x2="0" y1="-192" y2="-192" x1="64" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="0" y1="-64" y2="-64" x1="64" />
            <line x2="368" y1="-416" y2="-416" x1="432" />
            <line x2="368" y1="-352" y2="-352" x1="432" />
            <line x2="432" y1="-96" y2="-96" x1="368" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
            <line x2="432" y1="-224" y2="-224" x1="368" />
            <rect width="64" x="368" y="-300" height="24" />
            <line x2="432" y1="-288" y2="-288" x1="368" />
        </blockdef>
        <block symbolname="AWordCntr" name="XLXI_2">
            <blockpin signalname="XLXN_163" name="Clk" />
            <blockpin signalname="XLXN_12" name="Ena" />
            <blockpin signalname="XLXN_7" name="Clr_N" />
            <blockpin signalname="B(6:0)" name="Q(6:0)" />
        </block>
        <block symbolname="ClockCntr" name="XLXI_3">
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="XLXN_7" name="Clr_N" />
            <blockpin signalname="NrofGx(1:0)" name="Rng(1:0)" />
            <blockpin signalname="XLXN_5" name="EndEvent" />
            <blockpin signalname="XLXN_21" name="BmFifoWr_N" />
        </block>
        <block symbolname="nor2" name="XLXI_6">
            <blockpin signalname="XLXN_11" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_8">
            <blockpin signalname="XLXN_11" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_9">
            <blockpin signalname="XLXN_5" name="I" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_10">
            <blockpin signalname="XLXN_9" name="I0" />
            <blockpin signalname="Clr_N" name="I1" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_19">
            <blockpin signalname="CLK" name="I" />
            <blockpin signalname="XLXN_163" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_12">
            <blockpin signalname="Trg" name="I" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="BmFifoRam" name="XLXI_75">
            <blockpin signalname="XLXN_11" name="Din" />
            <blockpin signalname="XLXN_163" name="SClk" />
            <blockpin signalname="XLXN_5" name="SEna_N" />
            <blockpin signalname="CLK" name="WrSel_N" />
            <blockpin signalname="XLXN_21" name="WrEna_N" />
            <blockpin signalname="StrIn_N" name="RdSel_N" />
            <blockpin signalname="XLXN_241" name="RdEna_N" />
            <blockpin signalname="XLXN_187" name="Clr_N" />
            <blockpin signalname="XLXN_198(15:0)" name="Dout(15:0)" />
        </block>
        <block symbolname="DFifoRam" name="XLXI_83">
            <blockpin signalname="XLXN_5" name="EndEvIn" />
            <blockpin signalname="XLXN_163" name="WrSel_N" />
            <blockpin signalname="XLXN_16" name="WrEna_N" />
            <blockpin signalname="StrIn_N" name="RdSel_N" />
            <blockpin signalname="XLXN_242" name="RdEna_N" />
            <blockpin signalname="XLXN_187" name="Clr_N" />
            <blockpin signalname="XLXN_213(17:0)" name="Din(17:0)" />
            <blockpin signalname="NoAData_N" name="NoAData_N" />
            <blockpin signalname="DFifoEmpty_N" name="Empty_N" />
            <blockpin signalname="XLXN_243" name="EndEvOut" />
            <blockpin signalname="XLXN_217(17:0)" name="Dout(17:0)" />
        </block>
        <block symbolname="EventCntr" name="XLXI_85">
            <blockpin signalname="Trg" name="clk" />
            <blockpin signalname="Clr_N" name="Clr_N" />
            <blockpin signalname="B(17:7)" name="Q(10:0)" />
        </block>
        <block symbolname="InputMUX" name="XLXI_86">
            <blockpin signalname="XLXN_5" name="SEL" />
            <blockpin signalname="XLXN_216(17:0)" name="A(17:0)" />
            <blockpin signalname="B(17:0)" name="B(17:0)" />
            <blockpin signalname="XLXN_213(17:0)" name="X(17:0)" />
        </block>
        <block symbolname="Sub" name="XLXI_87">
            <blockpin signalname="Sub_Cmp" name="Ena" />
            <blockpin signalname="XLXN_214(17:0)" name="S_C_Din(17:0)" />
            <blockpin signalname="OS_TH(7:0)" name="Sb(7:0)" />
            <blockpin signalname="XLXN_216(17:0)" name="Result(17:0)" />
        </block>
        <block symbolname="Comp" name="XLXI_88">
            <blockpin signalname="Sub_Cmp" name="Ena" />
            <blockpin signalname="XLXN_214(17:0)" name="S_C_Din(17:0)" />
            <blockpin signalname="OS_TH(15:8)" name="Th(7:0)" />
            <blockpin signalname="XLXN_11" name="Touch" />
        </block>
        <block symbolname="InputRegister" name="XLXI_89">
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="Clr_N" name="Clr_N" />
            <blockpin signalname="ChAddr(5:0),AC_in(11:0)" name="Din(17:0)" />
            <blockpin signalname="XLXN_214(17:0)" name="Dout(17:0)" />
        </block>
        <block symbolname="TOMem" name="XLXI_92">
            <blockpin signalname="CLK" name="RdSel" />
            <blockpin signalname="Clr_N" name="RdClr_N" />
            <blockpin signalname="StrIn_N" name="CSel_N" />
            <blockpin signalname="XLXN_239" name="CRd_N" />
            <blockpin signalname="XLXN_240" name="CWr_N" />
            <blockpin signalname="Rst_N" name="CClr_N" />
            <blockpin signalname="ChAddr(5:0)" name="RdA(5:0)" />
            <blockpin signalname="Din(17:0)" name="CD(17:0)" />
            <blockpin signalname="XLXN_236" name="Full" />
            <blockpin signalname="XLXN_238" name="EMPTY" />
            <blockpin signalname="OS_TH(15:0)" name="RdQ(15:0)" />
            <blockpin signalname="XLXN_218(17:0)" name="CQ(17:0)" />
        </block>
        <block symbolname="Controller" name="XLXI_97">
            <blockpin signalname="XLXN_242" name="DFifoRd_N" />
            <blockpin signalname="XLXN_241" name="BmFifoRd_N" />
            <blockpin signalname="XLXN_187" name="FifoClr_N" />
            <blockpin signalname="XLXN_236" name="TOMemFull" />
            <blockpin signalname="XLXN_238" name="TOMemEmpty" />
            <blockpin signalname="XLXN_239" name="TOMemRd_N" />
            <blockpin signalname="XLXN_240" name="TOMemWr_N" />
            <blockpin signalname="NrofGx(1:0)" name="NrofGx(1:0)" />
            <blockpin signalname="Fcode(3:0)" name="FCode(3:0)" />
            <blockpin signalname="StrIn_N" name="StrIn_N" />
            <blockpin signalname="EnaIn_N" name="EnaIn_N" />
            <blockpin signalname="Rst_N" name="Rst_N" />
            <blockpin signalname="XLXN_243" name="DFifoStop" />
            <blockpin signalname="DFifoEmpty_N" name="DFifoEmpty_N" />
            <blockpin signalname="EnaOut_N" name="EnaOut_N" />
            <blockpin signalname="MAck_N" name="MAck_N" />
            <blockpin signalname="XLXN_234" name="TSEna_N" />
            <blockpin signalname="XLXN_235(1:0)" name="Sel(1:0)" />
        </block>
        <block symbolname="IO_BUF_BUS" name="XLXI_96">
            <blockpin signalname="D(17:0)" name="D(17:0)" />
            <blockpin signalname="Din(17:0)" name="Din(17:0)" />
            <blockpin signalname="XLXN_217(17:0)" name="DOut_1(17:0)" />
            <blockpin signalname="XLXN_198(15:0)" name="DOut_2(15:0)" />
            <blockpin signalname="XLXN_218(17:0)" name="DOut_3(17:0)" />
            <blockpin signalname="XLXN_235(1:0)" name="Sel(1:0)" />
            <blockpin signalname="XLXN_234" name="TSEna_N" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5382" height="3801">
        <attr value="CM" name="LengthUnitName" />
        <attr value="4" name="GridsPerUnit" />
        <instance x="2544" y="1632" name="XLXI_2" orien="R0">
        </instance>
        <instance x="2544" y="2128" name="XLXI_3" orien="R0">
        </instance>
        <instance x="3184" y="1472" name="XLXI_6" orien="R0" />
        <branch name="XLXN_5">
            <wire x2="3120" y1="1968" y2="1968" x1="2928" />
            <wire x2="3536" y1="1968" y2="1968" x1="3120" />
            <wire x2="3120" y1="1824" y2="1824" x1="3072" />
            <wire x2="3120" y1="1824" y2="1968" x1="3120" />
            <wire x2="3136" y1="1248" y2="1248" x1="3120" />
            <wire x2="3120" y1="1248" y2="1344" x1="3120" />
            <wire x2="3184" y1="1344" y2="1344" x1="3120" />
            <wire x2="3120" y1="1344" y2="1824" x1="3120" />
            <wire x2="3776" y1="1184" y2="1184" x1="3536" />
            <wire x2="3536" y1="1184" y2="1824" x1="3536" />
            <wire x2="3536" y1="1824" y2="1968" x1="3536" />
            <wire x2="3776" y1="1824" y2="1824" x1="3536" />
        </branch>
        <instance x="2816" y="1696" name="XLXI_8" orien="R180" />
        <instance x="3072" y="1792" name="XLXI_9" orien="R180" />
        <branch name="XLXN_6">
            <wire x2="2848" y1="1824" y2="1824" x1="2816" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="2304" y1="1568" y2="1600" x1="2304" />
            <wire x2="2304" y1="1600" y2="2032" x1="2304" />
            <wire x2="2544" y1="2032" y2="2032" x1="2304" />
            <wire x2="2544" y1="1600" y2="1600" x1="2304" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="2544" y1="1536" y2="1536" x1="2528" />
            <wire x2="2528" y1="1536" y2="1792" x1="2528" />
            <wire x2="2560" y1="1792" y2="1792" x1="2528" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="2272" y1="1104" y2="1312" x1="2272" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="2192" y1="1392" y2="1392" x1="2144" />
            <wire x2="2192" y1="1392" y2="1696" x1="2192" />
            <wire x2="2880" y1="1696" y2="1696" x1="2192" />
            <wire x2="2880" y1="1696" y2="1760" x1="2880" />
            <wire x2="3168" y1="1696" y2="1696" x1="2880" />
            <wire x2="3776" y1="1696" y2="1696" x1="3168" />
            <wire x2="2880" y1="1760" y2="1760" x1="2816" />
            <wire x2="3184" y1="1408" y2="1408" x1="3168" />
            <wire x2="3168" y1="1408" y2="1696" x1="3168" />
        </branch>
        <branch name="B(17:7)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="1232" type="branch" />
            <wire x2="2976" y1="1232" y2="1232" x1="2928" />
        </branch>
        <branch name="B(6:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2976" y="1472" type="branch" />
            <wire x2="2976" y1="1472" y2="1472" x1="2928" />
        </branch>
        <branch name="B(17:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3104" y="1184" type="branch" />
            <wire x2="3136" y1="1184" y2="1184" x1="3104" />
        </branch>
        <instance x="2208" y="1312" name="XLXI_10" orien="R90" />
        <branch name="XLXN_16">
            <wire x2="3472" y1="1376" y2="1376" x1="3440" />
            <wire x2="3472" y1="1312" y2="1376" x1="3472" />
            <wire x2="3776" y1="1312" y2="1312" x1="3472" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="3568" y1="2096" y2="2096" x1="2928" />
            <wire x2="3568" y1="1952" y2="2096" x1="3568" />
            <wire x2="3776" y1="1952" y2="1952" x1="3568" />
        </branch>
        <branch name="CLK">
            <wire x2="1056" y1="736" y2="736" x1="800" />
            <wire x2="2464" y1="736" y2="736" x1="1056" />
            <wire x2="2464" y1="736" y2="1968" x1="2464" />
            <wire x2="2544" y1="1968" y2="1968" x1="2464" />
            <wire x2="2528" y1="736" y2="736" x1="2464" />
            <wire x2="2528" y1="736" y2="752" x1="2528" />
            <wire x2="3568" y1="736" y2="736" x1="2528" />
            <wire x2="3568" y1="736" y2="1888" x1="3568" />
            <wire x2="3776" y1="1888" y2="1888" x1="3568" />
            <wire x2="1056" y1="736" y2="1184" x1="1056" />
            <wire x2="1136" y1="1184" y2="1184" x1="1056" />
            <wire x2="1056" y1="1184" y2="1680" x1="1056" />
            <wire x2="1088" y1="1680" y2="1680" x1="1056" />
        </branch>
        <branch name="Clr_N">
            <wire x2="1024" y1="864" y2="864" x1="800" />
            <wire x2="2336" y1="864" y2="864" x1="1024" />
            <wire x2="2336" y1="864" y2="1296" x1="2336" />
            <wire x2="2336" y1="1296" y2="1312" x1="2336" />
            <wire x2="2544" y1="1296" y2="1296" x1="2336" />
            <wire x2="1024" y1="864" y2="1248" x1="1024" />
            <wire x2="1136" y1="1248" y2="1248" x1="1024" />
            <wire x2="1024" y1="1248" y2="1728" x1="1024" />
            <wire x2="1088" y1="1728" y2="1728" x1="1024" />
        </branch>
        <branch name="Fcode(3:0)">
            <wire x2="3120" y1="2784" y2="2784" x1="816" />
        </branch>
        <branch name="NrofGx(1:0)">
            <wire x2="2512" y1="2720" y2="2720" x1="816" />
            <wire x2="3120" y1="2720" y2="2720" x1="2512" />
            <wire x2="2544" y1="2096" y2="2096" x1="2512" />
            <wire x2="2512" y1="2096" y2="2720" x1="2512" />
        </branch>
        <branch name="NoAData_N">
            <wire x2="4480" y1="1184" y2="1184" x1="4160" />
        </branch>
        <branch name="Trg">
            <wire x2="2272" y1="800" y2="800" x1="800" />
            <wire x2="2272" y1="800" y2="880" x1="2272" />
            <wire x2="2368" y1="800" y2="800" x1="2272" />
            <wire x2="2368" y1="800" y2="1232" x1="2368" />
            <wire x2="2544" y1="1232" y2="1232" x1="2368" />
        </branch>
        <branch name="OS_TH(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1520" y="1632" type="branch" />
            <wire x2="1520" y1="1632" y2="1632" x1="1472" />
        </branch>
        <branch name="OS_TH(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1632" y="1184" type="branch" />
            <wire x2="1712" y1="1184" y2="1184" x1="1632" />
        </branch>
        <branch name="OS_TH(15:8)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1632" y="1456" type="branch" />
            <wire x2="1712" y1="1456" y2="1456" x1="1632" />
        </branch>
        <branch name="Sub_Cmp">
            <wire x2="1680" y1="1520" y2="1520" x1="800" />
            <wire x2="1712" y1="1520" y2="1520" x1="1680" />
            <wire x2="1712" y1="1248" y2="1248" x1="1680" />
            <wire x2="1680" y1="1248" y2="1520" x1="1680" />
        </branch>
        <branch name="XLXN_163">
            <wire x2="2528" y1="976" y2="1472" x1="2528" />
            <wire x2="2544" y1="1472" y2="1472" x1="2528" />
            <wire x2="3600" y1="976" y2="976" x1="2528" />
            <wire x2="3600" y1="976" y2="1248" x1="3600" />
            <wire x2="3600" y1="1248" y2="1760" x1="3600" />
            <wire x2="3776" y1="1760" y2="1760" x1="3600" />
            <wire x2="3776" y1="1248" y2="1248" x1="3600" />
        </branch>
        <branch name="XLXN_187">
            <wire x2="3744" y1="2560" y2="2560" x1="3552" />
            <wire x2="3776" y1="1504" y2="1504" x1="3744" />
            <wire x2="3744" y1="1504" y2="2144" x1="3744" />
            <wire x2="3776" y1="2144" y2="2144" x1="3744" />
            <wire x2="3744" y1="2144" y2="2560" x1="3744" />
        </branch>
        <instance x="3776" y="1664" name="XLXI_75" orien="R0">
        </instance>
        <branch name="ChAddr(5:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1632" type="branch" />
            <wire x2="1088" y1="1632" y2="1632" x1="992" />
        </branch>
        <branch name="Din(17:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1808" type="branch" />
            <wire x2="1088" y1="1808" y2="1808" x1="992" />
        </branch>
        <instance x="3776" y="1152" name="XLXI_83" orien="R0">
        </instance>
        <instance x="2544" y="1328" name="XLXI_85" orien="R0">
        </instance>
        <instance x="3136" y="1280" name="XLXI_86" orien="R0">
        </instance>
        <instance x="1712" y="1280" name="XLXI_87" orien="R0">
        </instance>
        <instance x="1728" y="1552" name="XLXI_88" orien="R0">
        </instance>
        <iomarker fontsize="28" x="816" y="2976" name="Rst_N" orien="R180" />
        <iomarker fontsize="28" x="816" y="2912" name="EnaIn_N" orien="R180" />
        <iomarker fontsize="28" x="816" y="2720" name="NrofGx(1:0)" orien="R180" />
        <iomarker fontsize="28" x="4480" y="1248" name="DFifoEmpty_N" orien="R0" />
        <iomarker fontsize="28" x="4480" y="1184" name="NoAData_N" orien="R0" />
        <iomarker fontsize="28" x="800" y="1520" name="Sub_Cmp" orien="R180" />
        <branch name="ChAddr(5:0),AC_in(11:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1120" type="branch" />
            <wire x2="1136" y1="1120" y2="1120" x1="992" />
        </branch>
        <branch name="XLXN_213(17:0)">
            <wire x2="3776" y1="1120" y2="1120" x1="3520" />
        </branch>
        <branch name="XLXN_214(17:0)">
            <wire x2="1664" y1="1120" y2="1120" x1="1520" />
            <wire x2="1712" y1="1120" y2="1120" x1="1664" />
            <wire x2="1664" y1="1120" y2="1392" x1="1664" />
            <wire x2="1712" y1="1392" y2="1392" x1="1664" />
        </branch>
        <branch name="XLXN_216(17:0)">
            <wire x2="3136" y1="1120" y2="1120" x1="2144" />
        </branch>
        <branch name="XLXN_217(17:0)">
            <wire x2="4320" y1="1120" y2="1120" x1="4160" />
            <wire x2="4320" y1="1120" y2="2096" x1="4320" />
            <wire x2="4368" y1="2096" y2="2096" x1="4320" />
        </branch>
        <branch name="XLXN_218(17:0)">
            <wire x2="1616" y1="1904" y2="1904" x1="1472" />
            <wire x2="1616" y1="1904" y2="2224" x1="1616" />
            <wire x2="4368" y1="2224" y2="2224" x1="1616" />
        </branch>
        <instance x="1088" y="2112" name="XLXI_92" orien="R0">
        </instance>
        <branch name="Rst_N">
            <wire x2="1072" y1="2976" y2="2976" x1="816" />
            <wire x2="3120" y1="2976" y2="2976" x1="1072" />
            <wire x2="1072" y1="2080" y2="2976" x1="1072" />
            <wire x2="1088" y1="2080" y2="2080" x1="1072" />
        </branch>
        <branch name="EnaIn_N">
            <wire x2="3120" y1="2912" y2="2912" x1="816" />
        </branch>
        <branch name="StrIn_N">
            <wire x2="976" y1="2848" y2="2848" x1="816" />
            <wire x2="2944" y1="2848" y2="2848" x1="976" />
            <wire x2="3120" y1="2848" y2="2848" x1="2944" />
            <wire x2="1088" y1="1888" y2="1888" x1="976" />
            <wire x2="976" y1="1888" y2="2848" x1="976" />
            <wire x2="2944" y1="2016" y2="2848" x1="2944" />
            <wire x2="3648" y1="2016" y2="2016" x1="2944" />
            <wire x2="3776" y1="2016" y2="2016" x1="3648" />
            <wire x2="3648" y1="1376" y2="2016" x1="3648" />
            <wire x2="3776" y1="1376" y2="1376" x1="3648" />
        </branch>
        <branch name="XLXN_198(15:0)">
            <wire x2="4288" y1="1696" y2="1696" x1="4160" />
            <wire x2="4288" y1="1696" y2="2160" x1="4288" />
            <wire x2="4368" y1="2160" y2="2160" x1="4288" />
        </branch>
        <instance x="3120" y="3040" name="XLXI_97" orien="R0">
        </instance>
        <branch name="EnaOut_N">
            <wire x2="4416" y1="2944" y2="2944" x1="3552" />
        </branch>
        <branch name="MAck_N">
            <wire x2="4416" y1="2880" y2="2880" x1="3552" />
        </branch>
        <iomarker fontsize="28" x="4416" y="2944" name="EnaOut_N" orien="R0" />
        <iomarker fontsize="28" x="4416" y="2880" name="MAck_N" orien="R0" />
        <branch name="XLXN_234">
            <wire x2="4320" y1="2816" y2="2816" x1="3552" />
            <wire x2="4368" y1="2336" y2="2336" x1="4320" />
            <wire x2="4320" y1="2336" y2="2816" x1="4320" />
        </branch>
        <branch name="XLXN_235(1:0)">
            <wire x2="4288" y1="2752" y2="2752" x1="3552" />
            <wire x2="4368" y1="2288" y2="2288" x1="4288" />
            <wire x2="4288" y1="2288" y2="2752" x1="4288" />
        </branch>
        <branch name="XLXN_236">
            <wire x2="1584" y1="2000" y2="2000" x1="1472" />
            <wire x2="1584" y1="2000" y2="2464" x1="1584" />
            <wire x2="3120" y1="2464" y2="2464" x1="1584" />
        </branch>
        <branch name="XLXN_238">
            <wire x2="1552" y1="2064" y2="2064" x1="1472" />
            <wire x2="1552" y1="2064" y2="2528" x1="1552" />
            <wire x2="3120" y1="2528" y2="2528" x1="1552" />
        </branch>
        <branch name="XLXN_239">
            <wire x2="1088" y1="1952" y2="1952" x1="1008" />
            <wire x2="1008" y1="1952" y2="2592" x1="1008" />
            <wire x2="3120" y1="2592" y2="2592" x1="1008" />
        </branch>
        <branch name="XLXN_240">
            <wire x2="1088" y1="2016" y2="2016" x1="1040" />
            <wire x2="1040" y1="2016" y2="2656" x1="1040" />
            <wire x2="3120" y1="2656" y2="2656" x1="1040" />
        </branch>
        <branch name="XLXN_241">
            <wire x2="3712" y1="2496" y2="2496" x1="3552" />
            <wire x2="3776" y1="2080" y2="2080" x1="3712" />
            <wire x2="3712" y1="2080" y2="2496" x1="3712" />
        </branch>
        <branch name="XLXN_242">
            <wire x2="3680" y1="2432" y2="2432" x1="3552" />
            <wire x2="3680" y1="1440" y2="2432" x1="3680" />
            <wire x2="3776" y1="1440" y2="1440" x1="3680" />
        </branch>
        <branch name="DFifoEmpty_N">
            <wire x2="4256" y1="2688" y2="2688" x1="3552" />
            <wire x2="4256" y1="1248" y2="1248" x1="4160" />
            <wire x2="4256" y1="1248" y2="2688" x1="4256" />
            <wire x2="4480" y1="1248" y2="1248" x1="4256" />
        </branch>
        <iomarker fontsize="28" x="816" y="2784" name="Fcode(3:0)" orien="R180" />
        <iomarker fontsize="28" x="816" y="2848" name="StrIn_N" orien="R180" />
        <instance x="1136" y="1280" name="XLXI_89" orien="R0">
        </instance>
        <branch name="XLXN_243">
            <wire x2="4224" y1="2624" y2="2624" x1="3552" />
            <wire x2="4224" y1="1312" y2="1312" x1="4160" />
            <wire x2="4224" y1="1312" y2="2624" x1="4224" />
        </branch>
        <branch name="D(17:0)">
            <wire x2="4768" y1="2128" y2="2128" x1="4752" />
            <wire x2="4800" y1="2128" y2="2128" x1="4768" />
        </branch>
        <instance x="4368" y="2160" name="XLXI_96" orien="R0">
        </instance>
        <branch name="Din(17:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4800" y="2064" type="branch" />
            <wire x2="4800" y1="2064" y2="2064" x1="4752" />
        </branch>
        <iomarker fontsize="28" x="4800" y="2128" name="D(17:0)" orien="R0" />
        <instance x="2496" y="752" name="XLXI_19" orien="R90" />
        <instance x="2240" y="880" name="XLXI_12" orien="R90" />
        <iomarker fontsize="28" x="800" y="864" name="Clr_N" orien="R180" />
        <iomarker fontsize="28" x="800" y="800" name="Trg" orien="R180" />
        <iomarker fontsize="28" x="800" y="736" name="CLK" orien="R180" />
        <text style="fontsize:64;fontname:Arial" x="2191" y="669">DILOGIC-2</text>
        <branch name="AC_in(11:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2048" y="1936" type="branch" />
            <wire x2="2048" y1="1936" y2="1936" x1="1936" />
        </branch>
        <branch name="ChAddr(5:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2048" y="2096" type="branch" />
            <wire x2="2048" y1="2096" y2="2096" x1="1936" />
        </branch>
        <text style="fontsize:44;fontname:Arial" x="1747" y="2025">Channel Address ( 5:0 )</text>
        <text style="fontsize:44;fontname:Arial" x="1739" y="1861">Data from ADC ( 11:0 )</text>
        <iomarker fontsize="28" x="1936" y="1936" name="AC_in(11:0)" orien="R180" />
        <iomarker fontsize="28" x="1936" y="2096" name="ChAddr(5:0)" orien="R180" />
    </sheet>
</drawing>
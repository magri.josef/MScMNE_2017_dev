----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    07:52:45 11/10/2016 
-- Design Name: 
-- Module Name:    IO_BUF_BUS - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IO_BUF_BUS is
    Port ( DOut_1 	: in  STD_LOGIC_VECTOR (17 downto 0);
			  DOut_2 	: in  STD_LOGIC_VECTOR (15 downto 0);
			  DOut_3 	: in  STD_LOGIC_VECTOR (17 downto 0);
			  Sel 		: in  STD_LOGIC_VECTOR (1 downto 0);
           TSEna_N 	: in  STD_LOGIC;
           Din 		: out  STD_LOGIC_VECTOR (17 downto 0)	:= (others => 'Z');
           D 			: inout STD_LOGIC_VECTOR (17 downto 0) := (others => 'Z'));
end IO_BUF_BUS;

architecture Behavioral of IO_BUF_BUS is
	begin
	process(TSEna_N,Dout_1,Dout_2,Dout_3,D,Sel)
		begin
			if (TSEna_N = '0') then
				case Sel is when "00" => D <= Dout_1;
								when "01" => D <= "00" & Dout_2;
								when "10" => D <= Dout_3;
								when others =>  D <= (others => 'Z');		
				end case;
			else
				Din <= D(17 downto 0);
			end if;	
	end process;
end Behavioral;


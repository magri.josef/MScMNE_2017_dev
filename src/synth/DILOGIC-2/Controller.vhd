
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Controller is
    Port ( NrofGx : in  STD_LOGIC_VECTOR (1 downto 0);
           FCode : in  STD_LOGIC_VECTOR (3 downto 0);
           StrIn_N : in  STD_LOGIC;
           EnaIn_N : in  STD_LOGIC;
           Rst_N : in  STD_LOGIC;
           TOMemRd_N : out  STD_LOGIC;
           TOMemWr_N : out  STD_LOGIC;
           TOMemFull : in  STD_LOGIC;
			  TOMemEmpty : in  STD_LOGIC;
           DFifoRd_N : out  STD_LOGIC;
           DFifoStop : in  STD_LOGIC;
           DFifoEmpty_N : in  STD_LOGIC;
           BmFifoRd_N : out  STD_LOGIC;
           FifoClr_N : out  STD_LOGIC;
           MAck_N : out  STD_LOGIC := '1';
           TSEna_N : out  STD_LOGIC:= '1';
           EnaOut_N : out  STD_LOGIC;
			  Sel : out  STD_LOGIC_VECTOR (1 downto 0) := "ZZ");
end Controller;

architecture Behavioral of Controller is

-- -----------------------------------------------
--	| Function code	| Description 					
--	|---------------------------------------------- 	
--	| 0000 				| Front-end test mode 		
--	| 0001 				| Load "almost full" preset 
--	| 0010...0111 		| No operation 				
--	| 1000 				| Pattern readout           
--	| 1001 				| Pattern delete 			
--	| 1010 				| Analog readout 
--	| 1011 				| Analog delete 
--	| 1100 				| Reset fifo pointers 
--	| 1101 				| Reset daisy chain 
--	| 1110 				| Configuration write 
--	| 1111				| Configuration read 
-- -----------------------------------------------

signal endev : boolean := false;
signal done  : boolean := false;
signal event_start  : boolean := false;


begin

FifoClr_N <= '0' when (RST_N = '0') else '1';
BmFifoRd_N <='1';

--EnaOut:process(endev,StrIn_N,EnaIn_N)
-- begin
--	if ( endev and ( EnaIn_N = '0') ) then
--		--if (StrIn_N'event and StrIn_N = '0') then
--		if rising_edge(StrIn_N) then
--			EnaOut_N <= '0';
--		elsif EnaIn_N = '1' then
--			EnaOut_N <= '1';
--		end if;
----	else
----	EnaOut_N <= '1';
--end if;
--end process;

EnaOut:process(endev,StrIn_N,EnaIn_N,FCode)
 begin
	if EnaIn_N = '0' then
		if rising_edge(StrIn_N) and FCode = "1010" and endev then
			EnaOut_N <= '0';
			done <= true;
		end if;
		elsif EnaIn_N = '1' then
			EnaOut_N <= '1';
			done <= false;
	end if;
end process;

--StrIn_N_Chk:process(StrIn_N)
-- begin
--		if rising_edge(StrIn_N) and FCode = "1010" then
--			event_start <= true;
--		end if;
--end process;

MAckOut:process(endev,StrIn_N,EnaIn_N,FCode)
variable flag : boolean := false;
--variable done_3  : boolean := false;
 begin
	--if rising_edge(StrIn_N) and FCode = "1010" and done_3 = false and EnaIn_N = '0' then
	--if rising_edge(StrIn_N) and FCode = "1010" and flag = true and EnaIn_N = '0' then
	if EnaIn_N = '0' and rising_edge(StrIn_N) and FCode = "1010" and endev and flag = true then
			MAck_N <= '1';
	end if;
	if DFifoStop = '1' and FCode = "1010" and EnaIn_N = '0' and flag = false then --and done_3 = false then
			MAck_N <= '0';
			flag := true;
--	elsif DFifoStop = '0' and FCode = "1010" and done_3 and EnaIn_N = '0' then
--			MAck_N <= '1'; -- 'Z'
--			--flag := false;
--			done_2 <= true;
--	elsif DFifoStop = '0' and FCode = "1010" and EnaIn_N = '0' and flag = true then
--			MAck_N <= '1'; -- 'Z'
			--done_2 <= false;
			--done_3 := true;
--	elsif DFifoStop = '0' and FCode = "1010" and EnaIn_N = '1' and event_start = true then
--			MAck_N <= 'Z'; -- 'Z'
			--done_2 <= false;
	end if;
	if FCode = "1101" or FCode = "0XXX" then
			MAck_N <= 'Z';
--			done_2 <= false;
			--done_3 := false;
			flag := false;
	end if;

end process;

ControlCode:process(Fcode,StrIn_N,EnaIn_N,DFifoStop,TOMemFull,TOMemEmpty)--,TOMemFull,TOMemEmpty)
begin
	 --if falling_edge(StrIn_N) then
	 --if (StrIn_N'event and StrIn_N = '0') then
		if FCode = "1010" then -- Analog Readout
			 if EnaIn_N = '0' then
				TSEna_N <= '0';
				Sel <= "00";
				DfifoRd_N <= '0';
					if DFifoStop = '1' then
						--MAck_N   <= '0';
						endev <= true;
					else
						--MAck_N   <= '1';
						endev <= false;
					end if;
				elsif( DFifoStop = '1') then
					DfifoRd_N <= '1';
				elsif( done ) then
					TSEna_N <= '1';
				end if;
				
		elsif FCode = "1101" or FCode = "0XXX" then --reset daisy chain 
					--MAck_N   <= 'Z';
					TSEna_N  <= '1';
				
		elsif FCode = "1110" then -- Configure write
			if EnaIn_N = '0' then
				TOMemWr_N <= '1';
				TOMemRd_N <= '0';
				if TOMemFull = '1' then
					endev <= true;
					TOMemRd_N <= '1';
				else
					endev <= false;
				end if;
			elsif( TOMemFull = '1') then
			 TOMemRd_N <= '1';
			end if;
			
		elsif FCode = "1111" then -- Configure read
			if EnaIn_N = '0' then
				TSEna_N <= '0';
				Sel <= "10";			
				TOMemRd_N <= '1';
				TOMemWr_N <= '0';
				if TOMemEmpty = '1' then
					endev <= true;
					TOMemWr_N <= '1';
				else
					endev <= false;
				end if;
			else
			 TSEna_N   <= '1';
			 TOMemWr_N <= '1';
			end if;
		end if;
		
		if done then 
			Sel <= "11";
		end if;
	--end if;
end process;
end Behavioral;


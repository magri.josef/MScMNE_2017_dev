library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity DFifoRam is
    generic (
				DATA_WIDTH: integer := 18;
				DEPTH		 : integer := 9; -- 2**9=512bit
				RWIDTH	 : integer := 9);
    Port (	Din       : in  STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0);--(2**RWIDTH-1 downto 0); -- From multiplexer
				EndEvIn   : in  STD_LOGIC; -- from ClockCntr
				WrSel_N   : in  STD_LOGIC; -- write clock CLK
				WrEna_N   : in  STD_LOGIC; -- end event Or above treshold
				RdSel_N   : in  STD_LOGIC; -- read clock StrIn_N
				RdEna_N   : in  STD_LOGIC; -- from controller
				Clr_N 	 : in  STD_LOGIC; -- clear fifo
				NoAData_N : out STD_LOGIC; -- indicates that only end event are present
				Empty_N   : out STD_LOGIC; -- fifo is completely empty
				Dout      : out STD_LOGIC_VECTOR(DATA_WIDTH - 1 downto 0)  := (others => 'Z');
				EndEvOut  : out  STD_LOGIC);
end DFifoRam;

architecture Behavioral of DFifoRam is
	type mem_array is array(2**DEPTH-1 downto 0) of std_logic_vector(DATA_WIDTH - 1 downto 0);
	--signal i_full  : std_logic := '0';
	signal i_empty     : std_logic := '1';
	signal i_NoAData_N : std_logic := '1';
	signal DFifoRam    : mem_array := ((others=> (others=>'0')));
	signal wr_ptr      : unsigned(2**RWIDTH-1 downto 0) := (others => '0');
	signal rd_ptr      : unsigned(2**RWIDTH-1 downto 0) := (others => '0');
	signal event_ptr   : unsigned(2**RWIDTH-1 downto 0) := (others => '0');
begin

NoAData_N <= '0' when (EndEvIn = '1') and (wr_ptr = (wr_ptr'range => '0')) else '1';

flag_proc: process(wr_ptr, rd_ptr,EndEvIn,event_ptr,Clr_N )
	begin
		if (EndEvIn = '1') and (wr_ptr = (wr_ptr'range => '0')) then
			elsif EndEvIn = '1' then 
				event_ptr <= wr_ptr;
		end if;
		if (rd_ptr = event_ptr + 1) and (event_ptr /= (event_ptr'range => '0')) then 
			EndEvOut <= '1';
			else
				EndEvOut <= '0';
		end if;
	end process;
DataFifoRead:process (WrSel_N,Clr_N)	-- Read process
	begin
		if rising_edge(WrSel_N) then
			if (WrEna_N = '0') then
				DFifoRam(to_integer(wr_ptr)) <= Din;
				if (EndEvIn = '0') then
					wr_ptr<=wr_ptr+1;
				end if;
			end if;
		end if;
		if falling_edge(Clr_N) then
			wr_ptr <= (others => '0');
		end if;
	end process;
DataFifoWrite:process (RdSel_N,Clr_N)	-- Write process
	begin
		if falling_edge(RdSel_N) then
			if (RdEna_N = '0') then
				Dout <= DFifoRam(to_integer(rd_ptr));
				rd_ptr<=rd_ptr+1;
				if wr_ptr > rd_ptr then
					Empty_N  <= '0';
				else
					Empty_N  <= '1';
				end if;
			end if;
		end if;
		if falling_edge(Clr_N) then
			rd_ptr <= (others => '0');
			Dout   <= (others => 'Z');
		end if;
	end process;
end Behavioral;
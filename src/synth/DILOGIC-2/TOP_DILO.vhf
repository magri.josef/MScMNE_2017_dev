--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : TOP_DILO.vhf
-- /___/   /\     Timestamp : 07/15/2017 15:20:02
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan6 -flat -suppress -vhdl /home/josef/Documents/MScMNE_2017_dev/src/synth/DILOGIC-2/TOP_DILO.vhf -w /home/josef/Documents/MScMNE_2017_dev/src/synth/DILOGIC-2/TOP_DILO.sch
--Design Name: TOP_DILO
--Device: spartan6
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity TOP_DILO is
   port ( AC_in        : in    std_logic_vector (11 downto 0); 
          ChAddr       : in    std_logic_vector (5 downto 0); 
          CLK          : in    std_logic; 
          Clr_N        : in    std_logic; 
          EnaIn_N      : in    std_logic; 
          Fcode        : in    std_logic_vector (3 downto 0); 
          NrofGx       : in    std_logic_vector (1 downto 0); 
          Rst_N        : in    std_logic; 
          StrIn_N      : in    std_logic; 
          Sub_Cmp      : in    std_logic; 
          Trg          : in    std_logic; 
          DFifoEmpty_N : out   std_logic; 
          EnaOut_N     : out   std_logic; 
          MAck_N       : out   std_logic; 
          NoAData_N    : out   std_logic; 
          D            : inout std_logic_vector (17 downto 0));
end TOP_DILO;

architecture BEHAVIORAL of TOP_DILO is
   attribute BOX_TYPE   : string ;
   signal B                  : std_logic_vector (17 downto 0);
   signal Din                : std_logic_vector (17 downto 0);
   signal OS_TH              : std_logic_vector (15 downto 0);
   signal XLXN_5             : std_logic;
   signal XLXN_6             : std_logic;
   signal XLXN_7             : std_logic;
   signal XLXN_9             : std_logic;
   signal XLXN_11            : std_logic;
   signal XLXN_12            : std_logic;
   signal XLXN_16            : std_logic;
   signal XLXN_21            : std_logic;
   signal XLXN_163           : std_logic;
   signal XLXN_179           : std_logic;
   signal XLXN_180           : std_logic;
   signal XLXN_181           : std_logic;
   signal XLXN_182           : std_logic;
   signal XLXN_183           : std_logic;
   signal XLXN_187           : std_logic;
   signal XLXN_197           : std_logic;
   signal XLXN_198           : std_logic_vector (15 downto 0);
   signal XLXN_204           : std_logic;
   signal XLXN_205           : std_logic_vector (1 downto 0);
   signal XLXN_211           : std_logic;
   signal XLXN_213           : std_logic_vector (17 downto 0);
   signal XLXN_214           : std_logic_vector (17 downto 0);
   signal XLXN_216           : std_logic_vector (17 downto 0);
   signal XLXN_217           : std_logic_vector (17 downto 0);
   signal XLXN_218           : std_logic_vector (17 downto 0);
   signal DFifoEmpty_N_DUMMY : std_logic;
   component AWordCntr
      port ( Clk   : in    std_logic; 
             Ena   : in    std_logic; 
             Clr_N : in    std_logic; 
             Q     : out   std_logic_vector (6 downto 0));
   end component;
   
   component ClockCntr
      port ( clk        : in    std_logic; 
             Clr_N      : in    std_logic; 
             Rng        : in    std_logic_vector (1 downto 0); 
             EndEvent   : out   std_logic; 
             BmFifoWr_N : out   std_logic);
   end component;
   
   component NOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NOR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component BmFifoRam
      port ( Din     : in    std_logic; 
             SClk    : in    std_logic; 
             SEna_N  : in    std_logic; 
             WrSel_N : in    std_logic; 
             WrEna_N : in    std_logic; 
             RdSel_N : in    std_logic; 
             RdEna_N : in    std_logic; 
             Clr_N   : in    std_logic; 
             Dout    : out   std_logic_vector (15 downto 0));
   end component;
   
   component Controller
      port ( StrIn_N      : in    std_logic; 
             EnaIn_N      : in    std_logic; 
             Rst_N        : in    std_logic; 
             TOMemFull    : in    std_logic; 
             TOMemEmpty   : in    std_logic; 
             DFifoStop    : in    std_logic; 
             DFifoEmpty_N : in    std_logic; 
             NrofGx       : in    std_logic_vector (1 downto 0); 
             FCode        : in    std_logic_vector (3 downto 0); 
             TOMemRd_N    : out   std_logic; 
             TOMemWr_N    : out   std_logic; 
             DFifoRd_N    : out   std_logic; 
             BmFifoRd_N   : out   std_logic; 
             FifoClr_N    : out   std_logic; 
             MAck_N       : out   std_logic; 
             TSEna_N      : out   std_logic; 
             EnaOut_N     : out   std_logic; 
             Sel          : out   std_logic_vector (1 downto 0));
   end component;
   
   component DFifoRam
      port ( EndEvIn   : in    std_logic; 
             WrSel_N   : in    std_logic; 
             WrEna_N   : in    std_logic; 
             RdSel_N   : in    std_logic; 
             RdEna_N   : in    std_logic; 
             Clr_N     : in    std_logic; 
             Din       : in    std_logic_vector (17 downto 0); 
             NoAData_N : out   std_logic; 
             Empty_N   : out   std_logic; 
             EndEvOut  : out   std_logic; 
             Dout      : out   std_logic_vector (17 downto 0));
   end component;
   
   component EventCntr
      port ( clk   : in    std_logic; 
             Clr_N : in    std_logic; 
             Q     : out   std_logic_vector (10 downto 0));
   end component;
   
   component InputMUX
      port ( SEL : in    std_logic; 
             A   : in    std_logic_vector (17 downto 0); 
             B   : in    std_logic_vector (17 downto 0); 
             X   : out   std_logic_vector (17 downto 0));
   end component;
   
   component Sub
      port ( Ena     : in    std_logic; 
             S_C_Din : in    std_logic_vector (17 downto 0); 
             Sb      : in    std_logic_vector (7 downto 0); 
             Result  : out   std_logic_vector (17 downto 0));
   end component;
   
   component Comp
      port ( Ena     : in    std_logic; 
             S_C_Din : in    std_logic_vector (17 downto 0); 
             Th      : in    std_logic_vector (7 downto 0); 
             Touch   : out   std_logic);
   end component;
   
   component InputRegister
      port ( CLK   : in    std_logic; 
             Clr_N : in    std_logic; 
             Din   : in    std_logic_vector (17 downto 0); 
             Dout  : out   std_logic_vector (17 downto 0));
   end component;
   
   component IO_BUF_BUS
      port ( TSEna_N : in    std_logic; 
             DOut_1  : in    std_logic_vector (17 downto 0); 
             DOut_2  : in    std_logic_vector (15 downto 0); 
             DOut_3  : in    std_logic_vector (17 downto 0); 
             Sel     : in    std_logic_vector (1 downto 0); 
             D       : inout std_logic_vector (17 downto 0); 
             Din     : out   std_logic_vector (17 downto 0));
   end component;
   
   component TOMem
      port ( RdSel   : in    std_logic; 
             RdClr_N : in    std_logic; 
             CSel_N  : in    std_logic; 
             CRd_N   : in    std_logic; 
             CWr_N   : in    std_logic; 
             CClr_N  : in    std_logic; 
             RdA     : in    std_logic_vector (5 downto 0); 
             CD      : in    std_logic_vector (17 downto 0); 
             Full    : out   std_logic; 
             EMPTY   : out   std_logic; 
             RdQ     : out   std_logic_vector (15 downto 0); 
             CQ      : out   std_logic_vector (17 downto 0));
   end component;
   
begin
   DFifoEmpty_N <= DFifoEmpty_N_DUMMY;
   XLXI_2 : AWordCntr
      port map (Clk=>XLXN_163,
                Clr_N=>XLXN_7,
                Ena=>XLXN_12,
                Q(6 downto 0)=>B(6 downto 0));
   
   XLXI_3 : ClockCntr
      port map (clk=>CLK,
                Clr_N=>XLXN_7,
                Rng(1 downto 0)=>NrofGx(1 downto 0),
                BmFifoWr_N=>XLXN_21,
                EndEvent=>XLXN_5);
   
   XLXI_6 : NOR2
      port map (I0=>XLXN_11,
                I1=>XLXN_5,
                O=>XLXN_16);
   
   XLXI_8 : AND2
      port map (I0=>XLXN_11,
                I1=>XLXN_6,
                O=>XLXN_12);
   
   XLXI_9 : INV
      port map (I=>XLXN_5,
                O=>XLXN_6);
   
   XLXI_10 : AND2
      port map (I0=>XLXN_9,
                I1=>Clr_N,
                O=>XLXN_7);
   
   XLXI_12 : INV
      port map (I=>Trg,
                O=>XLXN_9);
   
   XLXI_19 : INV
      port map (I=>CLK,
                O=>XLXN_163);
   
   XLXI_75 : BmFifoRam
      port map (Clr_N=>XLXN_187,
                Din=>XLXN_11,
                RdEna_N=>XLXN_211,
                RdSel_N=>StrIn_N,
                SClk=>XLXN_163,
                SEna_N=>XLXN_5,
                WrEna_N=>XLXN_21,
                WrSel_N=>CLK,
                Dout(15 downto 0)=>XLXN_198(15 downto 0));
   
   XLXI_82 : Controller
      port map (DFifoEmpty_N=>DFifoEmpty_N_DUMMY,
                DFifoStop=>XLXN_197,
                EnaIn_N=>EnaIn_N,
                FCode(3 downto 0)=>Fcode(3 downto 0),
                NrofGx(1 downto 0)=>NrofGx(1 downto 0),
                Rst_N=>Rst_N,
                StrIn_N=>StrIn_N,
                TOMemEmpty=>XLXN_181,
                TOMemFull=>XLXN_182,
                BmFifoRd_N=>XLXN_211,
                DFifoRd_N=>XLXN_183,
                EnaOut_N=>EnaOut_N,
                FifoClr_N=>XLXN_187,
                MAck_N=>MAck_N,
                Sel(1 downto 0)=>XLXN_205(1 downto 0),
                TOMemRd_N=>XLXN_179,
                TOMemWr_N=>XLXN_180,
                TSEna_N=>XLXN_204);
   
   XLXI_83 : DFifoRam
      port map (Clr_N=>XLXN_187,
                Din(17 downto 0)=>XLXN_213(17 downto 0),
                EndEvIn=>XLXN_5,
                RdEna_N=>XLXN_183,
                RdSel_N=>StrIn_N,
                WrEna_N=>XLXN_16,
                WrSel_N=>XLXN_163,
                Dout(17 downto 0)=>XLXN_217(17 downto 0),
                Empty_N=>DFifoEmpty_N_DUMMY,
                EndEvOut=>XLXN_197,
                NoAData_N=>NoAData_N);
   
   XLXI_85 : EventCntr
      port map (clk=>Trg,
                Clr_N=>Clr_N,
                Q(10 downto 0)=>B(17 downto 7));
   
   XLXI_86 : InputMUX
      port map (A(17 downto 0)=>XLXN_216(17 downto 0),
                B(17 downto 0)=>B(17 downto 0),
                SEL=>XLXN_5,
                X(17 downto 0)=>XLXN_213(17 downto 0));
   
   XLXI_87 : Sub
      port map (Ena=>Sub_Cmp,
                Sb(7 downto 0)=>OS_TH(7 downto 0),
                S_C_Din(17 downto 0)=>XLXN_214(17 downto 0),
                Result(17 downto 0)=>XLXN_216(17 downto 0));
   
   XLXI_88 : Comp
      port map (Ena=>Sub_Cmp,
                S_C_Din(17 downto 0)=>XLXN_214(17 downto 0),
                Th(7 downto 0)=>OS_TH(15 downto 8),
                Touch=>XLXN_11);
   
   XLXI_89 : InputRegister
      port map (CLK=>CLK,
                Clr_N=>Clr_N,
                Din(17 downto 12)=>ChAddr(5 downto 0),
                Din(11 downto 0)=>AC_in(11 downto 0),
                Dout(17 downto 0)=>XLXN_214(17 downto 0));
   
   XLXI_91 : IO_BUF_BUS
      port map (DOut_1(17 downto 0)=>XLXN_217(17 downto 0),
                DOut_2(15 downto 0)=>XLXN_198(15 downto 0),
                DOut_3(17 downto 0)=>XLXN_218(17 downto 0),
                Sel(1 downto 0)=>XLXN_205(1 downto 0),
                TSEna_N=>XLXN_204,
                Din(17 downto 0)=>Din(17 downto 0),
                D(17 downto 0)=>D(17 downto 0));
   
   XLXI_92 : TOMem
      port map (CClr_N=>Rst_N,
                CD(17 downto 0)=>Din(17 downto 0),
                CRd_N=>XLXN_179,
                CSel_N=>StrIn_N,
                CWr_N=>XLXN_180,
                RdA(5 downto 0)=>ChAddr(5 downto 0),
                RdClr_N=>Clr_N,
                RdSel=>CLK,
                CQ(17 downto 0)=>XLXN_218(17 downto 0),
                EMPTY=>XLXN_181,
                Full=>XLXN_182,
                RdQ(15 downto 0)=>OS_TH(15 downto 0));
   
end BEHAVIORAL;



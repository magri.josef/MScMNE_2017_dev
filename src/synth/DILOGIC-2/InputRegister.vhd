----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:19:26 11/07/2016 
-- Design Name: 
-- Module Name:    InputRegister - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity InputRegister is
    Port ( Din		: in  STD_LOGIC_VECTOR (17 downto 0);
           CLK		: in  STD_LOGIC;
           Clr_N	: in  STD_LOGIC;
           Dout	: out  STD_LOGIC_VECTOR (17 downto 0));
end InputRegister;

architecture Behavioral of InputRegister is
begin
	InputReg:process(CLK, Clr_N)
		begin
			if (Clr_N = '0') then Dout <= (others => '0');
				elsif rising_edge(CLK) then Dout <= Din;
			end if;
		end process;
end Behavioral;


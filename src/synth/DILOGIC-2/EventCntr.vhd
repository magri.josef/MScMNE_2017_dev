----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:52:11 11/09/2016 
-- Design Name: 
-- Module Name:    EventCntr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity EventCntr is
    Port ( clk   : in  STD_LOGIC;
           Clr_N : in  STD_LOGIC;
           Q     : out STD_LOGIC_VECTOR (10 downto 0));
end EventCntr;

architecture Behavioral of EventCntr is
   signal temp: STD_LOGIC_VECTOR (10 downto 0);
	begin   
	process(clk,Clr_N)
		begin
			if Clr_N='0' then
				temp <= (others => '0');--"000000000";
			elsif(rising_edge(clk)) then
				temp <= temp + 1;
			end if;
   end process;
   Q <= temp;
end Behavioral;


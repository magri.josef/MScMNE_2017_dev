# Testbench
prefix_tb = ./src/sim/scripts

sim: 
	cd $(prefix_tb) && $(MAKE)

clean:
	cd $(prefix_tb) && $(MAKE) clean
